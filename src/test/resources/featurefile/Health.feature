Feature: To verify the functionalities of health 

@HealthSanity_1
Scenario: To verify normal policy issuance 

	Given logged to the application with details "UserName :=  85335399 # Password := digit123" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 45 # Spouse := 42 # Child 1 := 2" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 1" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Individual SI" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Child1" are entered in Agent Health family details page 
	And family details for "Self" is entered as "Height := 160 # Weight := 60" in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := Self,Spouse # Alcohol := Self" are selected in Agent Health personal habits page 
	And tobacco consumption forms "Self := Both # Spouse := Smoke" are selected in Agent Health personal habits page 
	And alcohol consumption forms "Self := Daily" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := Self,Spouse # Pre Existing Conditions := None # Ongoing Medications := Spouse" are entered in Agent Health medical questions page 
	And gynaecological problem "Spouse := Yes" is selected in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := REF # Spouse := REF # Child 1 := ACS" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Cheque" are entered in Agent Health summary page 
#	And proceed to the payment in Agent Health summary page 
#	Then verify "Referral" message is displayed in Agent Health success page 
#	And verify the policy has moved to "For Approval" state in Agent transactions page
#	And close the browser 
	
@HealthSanity 
Scenario: To verify normal policy issuance 

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 45 # Spouse := 42 # Child 1 := 2" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 1" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Individual SI # Sum Insured := 6,00,000 # Policy Term := 3 year" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Child1" are entered in Agent Health family details page 
	And family details for "Self" is entered as "Height := 160 # Weight := 60" in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page 
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACS # Spouse := ACS # Child 1 := ACS" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Cheque" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Success" message is displayed in Agent Health success page 
	And verify the policy has moved to "Effective" state in Agent transactions page 
	And close the browser 
	
@HealthSanity_Thyroid 
Scenario: To verify normal policy issuance 

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 45 # Spouse := 42 # Child 1 := 2" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 1" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Individual SI # Sum Insured := 6,00,000 # Policy Term := 3 year" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Child1" are entered in Agent Health family details page 
	And family details for "Self" is entered as "Height := 160 # Weight := 60" in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := Self" are entered in Agent Health medical questions page 
	And pre existing diseases "Self := Thyroid problem" are entered in Agent Health medical questions page 
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page 
	And select "Yes" to answer further medical questions in Agent Health medical questions page 
	And thyroid details for "Self" is entered as "Type of Thyroid := Hypothyroidism # Current Medication := Thyroxine # When was it diagnosed := Less than 5 yrs # Any associated complications := Yes" in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := REF # Spouse := ACS # Child 1 := ACS" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Cheque" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Referral" message is displayed in Agent Health success page 
	And verify the policy has moved to "For Approval" state in Agent transactions page 
	And close the browser 
	
	
@HealthSanity_Diabetes 
Scenario: To verify normal policy issuance 

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 45 # Spouse := 42 # Child 1 := 2" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 1" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Individual SI # Sum Insured := 6,00,000 # Policy Term := 3 year" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Child1" are entered in Agent Health family details page 
	And family details for "Self" is entered as "Height := 160 # Weight := 60" in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := Spouse" are entered in Agent Health medical questions page 
	And pre existing diseases "Spouse := Diabetes" are entered in Agent Health medical questions page 
	And select "Yes" to answer further medical questions in Agent Health medical questions page 
	And diabetes details for "Spouse" is entered as "Type of Diabetes := Non Insulin Dependent # Current Medication := Oral medication # When was it diagnosed := Upto 10 yrs # Last Blood Sugar test Done := Less than 3 months # HbA1c Value := More than 7.5% # Hospitalized Due to Diabetes := Yes # Records of hospitalization := Yes # Any complications for Diabetes := Yes # Records of complications := Yes" in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACS # Spouse := DPM # Child 1 := ACS" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Agent Float" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Success" message is displayed in Agent Health success page 
	And verify the policy has moved to "Effective" state in Agent transactions page 
	And close the browser 
	
@HealthSanity_Asthma 
Scenario: To verify normal policy issuance 

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 45 # Spouse := 42 # Child 1 := 2" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 1" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Individual SI # Sum Insured := 6,00,000 # Policy Term := 3 year" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Child1" are entered in Agent Health family details page 
	And family details for "Self" is entered as "Height := 160 # Weight := 60" in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := Spouse" are entered in Agent Health medical questions page 
	And pre existing diseases "Spouse := Asthma" are entered in Agent Health medical questions page 
	And select "Yes" to answer further medical questions in Agent Health medical questions page 
	And asthma details for "Spouse" is entered as "When was it diagnosed? := Upto 10 yrs # Current Medication := None # Times Suffered := 2 to 5 # Hospitalized due to Asthma := Yes # Records of hospitalization := Yes # Complications due to Asthma := Yes # Records of complications := Yes" in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACS # Spouse := DPM # Child 1 := ACS" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Agent Float" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Success" message is displayed in Agent Health success page 
	And verify the policy has moved to "Effective" state in Agent transactions page 
	And close the browser 
	
@HealthSanity_Hyperlipidemia
Scenario: To verify normal policy issuance 

Application: Agent Portal
Module : Health
	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 45 # Spouse := 42 # Child 1 := 2" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 1" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Individual SI # Sum Insured := 6,00,000 # Policy Term := 3 year" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Child1" are entered in Agent Health family details page 
	And family details for "Self" is entered as "Height := 160 # Weight := 60" in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := Spouse" are entered in Agent Health medical questions page 
	And pre existing diseases "Spouse := High Cholesterol" are entered in Agent Health medical questions page 
	And select "Yes" to answer further medical questions in Agent Health medical questions page 
	And hyperlipidemia details for "Spouse" is entered as "On Medication := Yes # Any complications := Yes # When was it diagnosed := Less than 5yrs" in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACS # Spouse := REF # Child 1 := ACS" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Agent Float" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Referral" message is displayed in Agent Health success page 
	And verify the policy has moved to "For Approval" state in Agent transactions page 
	And close the browser 
	