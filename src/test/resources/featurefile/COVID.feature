Feature: COVID

@Covid
Scenario: df

Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Group Health Master policy creation" is selected
And smart button "New" is clicked in Master policy creation page
And company details "Type of Document := PAN # Document No := Random # Company Name := Random # Address := Random # Pincode := Random # Mobile := Random # Email ID := Random # RM Email ID := Random " are entered in Master policy creation page
And policy details "Who will pay the premium := Send payment link to MPH # Max Sum Insured := 10000 # Count of Lives := 10 # Held Cover Letter Number := Random # Remarks := Random" are entered in Master policy creation page
And special conditions "Terms of the Proposal := Standard # Proposal Type := Employer-Employee" are entered in Master policy creation page
And upload member data collection file in Master policy creation page
And smart button "Proceed" is clicked in Master policy creation page
Then verify priciple values are stored in DB

@Covid_RMNM
Scenario: RMNM

Given logged to the application with details "User Name := RMNM # Password := Digit@456$"
When module "Group Health Master policy creation" is selected
And smart button "New" is clicked in Master policy creation page
And company details "Type of Document := PAN # Document No := Random # IMD Code := 1000295 # Company Name := Random # Address := Random # Pincode := Random # Mobile := Random # Email ID := Random # RM Email ID := Random " are entered in Master policy creation page
And verify dropdown "Who will pay the premium := Send payment link to MPH,Agent Float" in Master policy creation page
And policy details "Who will pay the premium := Send payment link to MPH # Max Sum Insured := 10000 # Count of Lives := 10 # Held Cover Letter Number := Random # Remarks := Random" are entered in Master policy creation page
And special conditions "Terms of the Proposal := Standard # Proposal Type := Employer-Employee" are entered in Master policy creation page
And upload member data collection file in Master policy creation page
And smart button "Proceed" is clicked in Master policy creation page
And verify warning message "DigitDesk Ticket Number is required" is displayed in Master policy creation page

@Covid_RMNM_Agent
Scenario: RMNM

Given logged to the application with details "User Name := RMNM # Password := Digit@456$"
When module "Group Health Master policy creation" is selected
And smart button "New" is clicked in Master policy creation page
And company details "Type of Document := PAN # Document No := Random # IMD Code := 1000295 # Company Name := Random # Address := Random # Pincode := Random # Mobile := Random # Email ID := Random # RM Email ID := Random " are entered in Master policy creation page
And verify dropdown "Who will pay the premium := Send payment link to MPH,Agent Float" in Master policy creation page
And policy details "Who will pay the premium := Agent Float # Max Sum Insured := 10000 # Count of Lives := 10 # Held Cover Letter Number := Random # Remarks := Random" are entered in Master policy creation page
And special conditions "Terms of the Proposal := Standard # Proposal Type := Employer-Employee" are entered in Master policy creation page
And upload member data collection file in Master policy creation page
And smart button "Proceed" is clicked in Master policy creation page
And verify warning message "DigitDesk Ticket Number is required" is displayed in Master policy creation page
Then policy details "Digit Desk Number := Random" are entered in Master policy creation page
And smart button "Proceed" is clicked in Master policy creation page
And verify success message "Data saved successfully" is displayed in Master policy creation page
Then verify priciple values are stored in DB

@Covid_543
Scenario: df

Given logged to the application with details "User Name := 54315087 # Password := Digit@123"
When module "Group Health Master policy creation" is selected
And smart button "New" is clicked in Master policy creation page
And company details "Type of Document := PAN # Document No := Random # Company Name := Random # Address := Random # Pincode := Random # Mobile := Random # Email ID := Random # RM Email ID := Random " are entered in Master policy creation page
#And verify dropdown "Who will pay the premium := Send payment link to MPH,Agent Float" in Master policy creation page
And policy details "Who will pay the premium := Send payment link to MPH # Max Sum Insured := 10000 # Count of Lives := 10 # Held Cover Letter Number := Random # Remarks := Random" are entered in Master policy creation page
And special conditions "Terms of the Proposal := Standard # Proposal Type := Employer-Employee" are entered in Master policy creation page
And upload member data collection file in Master policy creation page
And smart button "Proceed" is clicked in Master policy creation page
And verify success message "Data saved successfully" is displayed in Master policy creation page
Then verify priciple values are stored in DB