Feature: Sanity for GMC Large

Background:
Given Login into the application for GMC Large

@GMC_One @GMC_Sanity
Scenario Outline: Test-Case-1

	When Clicked on module "GMC LARGE" in agent portal
	And create new policy
	And provide insured details for GMC Large from "<gmcSalesId>"
	And provide prospective policy details for GMC Large from "<gmcSalesId>"
	And provide coverage details for GMC Large from "<gmcSalesId>"
	And provide additional coverage details for GMC Large from "<gmcSalesId>"
	And provide premium details for GMC Large from "<gmcSalesId>"
	And provide demography details for GMC Large from "<gmcSalesId>"
	And provide document details for GMC Large from "<gmcSalesId>"
	And click on "Calculate Premium" buttton for "<gmcSalesId>"
	And click on "Forward to Underwriter" buttton for "<gmcSalesId>"
	And close the browser

Examples:
| gmcSalesId |
|	1	|


@GMC_Two @GMC_Sanity
Scenario Outline: Test-Case-2

	When Clicked on module "GMC LARGE" in agent portal
	And create new policy
	And provide insured details for GMC Large from "<gmcSalesId>"
	And provide prospective policy details for GMC Large from "<gmcSalesId>"
	And provide coverage details for GMC Large from "<gmcSalesId>"
	And provide additional coverage details for GMC Large from "<gmcSalesId>"
	And provide premium details for GMC Large from "<gmcSalesId>"
	And provide demography details for GMC Large from "<gmcSalesId>"
	And provide document details for GMC Large from "<gmcSalesId>"
	And click on "Calculate Premium" buttton for "<gmcSalesId>"
	And click on "Forward to Underwriter" buttton for "<gmcSalesId>"
	And close the browser

Examples:
| gmcSalesId |
|	2	|


@GMC_Three @GMC_Sanity
Scenario Outline: Test-Case-3

	When Clicked on module "GMC LARGE" in agent portal
	And create new policy
	And provide insured details for GMC Large from "<gmcSalesId>"
	And provide prospective policy details for GMC Large from "<gmcSalesId>"
	And provide coverage details for GMC Large from "<gmcSalesId>"
	And provide additional coverage details for GMC Large from "<gmcSalesId>"
	And provide premium details for GMC Large from "<gmcSalesId>"
	And provide demography details for GMC Large from "<gmcSalesId>"
	And provide document details for GMC Large from "<gmcSalesId>"
	And click on "Calculate Premium" buttton for "<gmcSalesId>"
	And click on "Forward to Underwriter" buttton for "<gmcSalesId>"
	And close the browser

Examples:
| gmcSalesId |
|	3	|


@GMC_Four @GMC_Sanity
Scenario Outline: Test-Case-4

	When Clicked on module "GMC LARGE" in agent portal
	And create new policy
	And provide insured details for GMC Large from "<gmcSalesId>"
	And provide prospective policy details for GMC Large from "<gmcSalesId>"
	And provide coverage details for GMC Large from "<gmcSalesId>"
	And provide additional coverage details for GMC Large from "<gmcSalesId>"
	And provide premium details for GMC Large from "<gmcSalesId>"
	And provide demography details for GMC Large from "<gmcSalesId>"
	And provide document details for GMC Large from "<gmcSalesId>"
	And click on "Calculate Premium" buttton for "<gmcSalesId>"
	And click on "Forward to Underwriter" buttton for "<gmcSalesId>"
	And close the browser

Examples:
| gmcSalesId |
|	4	|


@GMC_Five @GMC_Sanity
Scenario Outline: Test-Case-5

	When Clicked on module "GMC LARGE" in agent portal
	And create new policy
	And provide insured details for GMC Large from "<gmcSalesId>"
	And provide prospective policy details for GMC Large from "<gmcSalesId>"
	And provide coverage details for GMC Large from "<gmcSalesId>"
	And provide additional coverage details for GMC Large from "<gmcSalesId>"
	And provide premium details for GMC Large from "<gmcSalesId>"
	And provide demography details for GMC Large from "<gmcSalesId>"
	And provide document details for GMC Large from "<gmcSalesId>"
	And click on "Calculate Premium" buttton for "<gmcSalesId>"
	And click on "Forward to Underwriter" buttton for "<gmcSalesId>"
	And close the browser

Examples:
| gmcSalesId |
|	5	|


@GMC_Six @GMC_Sanity
Scenario Outline: Test-Case-6

	When Clicked on module "GMC LARGE" in agent portal
	And create new policy
	And provide insured details for GMC Large from "<gmcSalesId>"
	And provide prospective policy details for GMC Large from "<gmcSalesId>"
	And provide coverage details for GMC Large from "<gmcSalesId>"
	And provide additional coverage details for GMC Large from "<gmcSalesId>"
	And provide premium details for GMC Large from "<gmcSalesId>"
	And provide demography details for GMC Large from "<gmcSalesId>"
	And provide document details for GMC Large from "<gmcSalesId>"
	And click on "Calculate Premium" buttton for "<gmcSalesId>"
	And click on "Forward to Underwriter" buttton for "<gmcSalesId>"
	And close the browser

Examples:
| gmcSalesId |
|	6	|
