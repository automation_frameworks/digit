Feature: Functionality of Arogya Sanjeevani

Background:
Given Logged-in into the application

@Health_ArogyaSanjeevani_Sanity @AS_One @QuickSanity
Scenario Outline: Create New Health Insurance policy for Proposer with Arogya_Sanjeevani where SI:= 1Lakh/Term:-1yr/ProposerAge<45/Ht:-180,Wt:-90/No PED/Online payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self" are entered in Agent Health family details page 
	And details entered for individuals "Self" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Self # Alcohol := Self" are selected in Agent Health personal habits page
	And tobacco consumption forms "Self := Both" are selected in Agent Health personal habits page
	And alcohol consumption forms "Self := Daily" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1441 |
	
@Health_ArogyaSanjeevani_Sanity @AS_Two @QuickSanity
Scenario Outline: Create New Health Insurance policy for Spouse with Arogya_Sanjeevani where SI:= 3Lakh/1yr/ProposerAge<45/Ht:-180,Wt:-90/No PED/Cheque payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Spouse" from provided planId "<planId>"
	And unselect Proposer
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Spouse # Alcohol := Spouse" are selected in Agent Health personal habits page
	And tobacco consumption forms "Spouse := Both" are selected in Agent Health personal habits page
	And alcohol consumption forms "Spouse := Daily" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := Spouse # Pre Existing Conditions := Spouse" from planId "<planId>" are entered in Agent Health medical questions page
	And select "Yes" to answer further medical questions in Agent Health medical questions page
	And answer all further medical questions for "Spouse"
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1450 |
	
@Health_ArogyaSanjeevani_Sanity @AS_Three @QuickSanity
Scenario Outline: Create New Health Insurance policy for Proposer with Arogya_Sanjeevani where SI:= 3Lakh/1yr/ProposerAge<45/Ht:-180,Wt:-90/PED:-HighCholesterol/Send payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self" are entered in Agent Health family details page 
	And details entered for individuals "Self" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Self # Alcohol := Self" are selected in Agent Health personal habits page
	And tobacco consumption forms "Self := Smoke" are selected in Agent Health personal habits page
	And alcohol consumption forms "Self := Once in a week" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := Self # Pre Existing Conditions := Self" from planId "<planId>" are entered in Agent Health medical questions page
	And select "No" to answer further medical questions in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1455 |
	
@Health_ArogyaSanjeevani_Sanity @AS_Four @QuickSanity
Scenario Outline: Create New Health Insurance policy for Proposer,Spouse,Child1 with Arogya_Sanjeevani where SI:= 4.5Lakh/Term:-1yr/ProposerAge<45/Ht:-180,Wt:-90/No PED/Online payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Spouse" from provided planId "<planId>"
	And cover for "Child1" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse,Child1" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse,Child1" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Spouse # Alcohol := Self" are selected in Agent Health personal habits page
	And tobacco consumption forms "Spouse := Smoke" are selected in Agent Health personal habits page
	And alcohol consumption forms "Self := Once in a week" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1461 |
	
@Health_ArogyaSanjeevani_Sanity @AS_Five @QuickSanity
Scenario Outline: Create New Health Insurance policy for Proposer,Spouse,Child1 with Arogya_Sanjeevani where SI:= 4.5Lakh/1yr/ProposerAge<45/Ht:-180,Wt:-95/PED:-Diabetes/Cheque payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Spouse" from provided planId "<planId>"
	And cover for "Child1" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse,Child1" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse,Child1" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Spouse # Alcohol := Self" are selected in Agent Health personal habits page
	And tobacco consumption forms "Spouse := Smoke" are selected in Agent Health personal habits page
	And alcohol consumption forms "Self := Once in a week" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := Child1 # Pre Existing Conditions := Spouse" from planId "<planId>" are entered in Agent Health medical questions page
	And select "Yes" to answer further medical questions in Agent Health medical questions page
	And answer all further medical questions for "Spouse"
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1462 |
	
@Health_ArogyaSanjeevani_Sanity @AS_Six 
Scenario Outline: Create New Health Insurance policy for Proposer,Spouse,Child1 with Arogya_Sanjeevani where SI:= 4.5Lakh/1yr/ProposerAge<45/Ht:-180,Wt:-111/PED:-Hypertension/Generate payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Spouse" from provided planId "<planId>"
	And cover for "Child1" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse,Child1" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse,Child1" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Spouse # Alcohol := Self" are selected in Agent Health personal habits page
	And tobacco consumption forms "Spouse := Smoke" are selected in Agent Health personal habits page
	And alcohol consumption forms "Self := Once in a week" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := Child1" from planId "<planId>" are entered in Agent Health medical questions page
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page
	And select "No" to answer further medical questions in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1464 |
	
@Health_ArogyaSanjeevani_Sanity @AS_Seven
Scenario Outline: Create New Health Insurance policy for Proposer,Spouse,Father with Arogya_Sanjeevani where SI:= 2Lakh/1yr/ProposerAge<45/Ht:-180,Wt:-11/No PED/Online payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Spouse" from provided planId "<planId>"
	And cover for "Father" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And choose plan for parent from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse,Father" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse,Father" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Spouse # Alcohol := Self" are selected in Agent Health personal habits page
	And tobacco consumption forms "Spouse := Smoke" are selected in Agent Health personal habits page
	And alcohol consumption forms "Self := Once in a week" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1469 |
	
@Health_ArogyaSanjeevani_Sanity @AS_Eight
Scenario Outline: Create New Health Insurance policy for Proposer,Spouse,Mother with Arogya_Sanjeevani where SI:= 2Lakh/1yr/ProposerAge<45/Ht:-180,Wt:-110/PED:-Diabetes/Cheque payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Spouse" from provided planId "<planId>"
	And cover for "Mother" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And choose plan for parent from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse,Mother" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse,Mother" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Spouse # Alcohol := Self" are selected in Agent Health personal habits page
	And tobacco consumption forms "Spouse := Smoke" are selected in Agent Health personal habits page
	And alcohol consumption forms "Self := Once in a week" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := Mother # Pre Existing Conditions := Spouse" from planId "<planId>" are entered in Agent Health medical questions page
	And gynaecological problem "Mother := No" is selected in Agent Health medical questions page
	And select "Yes" to answer further medical questions in Agent Health medical questions page
	And answer all further medical questions for "Spouse"
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1470 |
	
@Health_ArogyaSanjeevani_Sanity @AS_Nine
Scenario Outline: Create New Health Insurance policy for Proposer,Spouse,FatherInLaw with Arogya_Sanjeevani where SI:= 2Lakh/1yr/ProposerAge<45/Ht:-180,Wt:-100/PED:-Asthma/Send payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Spouse" from provided planId "<planId>"
	And cover for "Father in Law" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And choose plan for parent from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse,Father in Law" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse,Father in Law" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Spouse # Alcohol := Self" are selected in Agent Health personal habits page
	And tobacco consumption forms "Spouse := Smoke" are selected in Agent Health personal habits page
	And alcohol consumption forms "Self := Once in a week" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := Father in Law" from planId "<planId>" are entered in Agent Health medical questions page
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page
	And select "No" to answer further medical questions in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1471 |
	
	
@Health_ArogyaSanjeevani_Sanity @AS_Ten
Scenario Outline: Create New Health Insurance policy for Proposer with Arogya Sanjeevani Plan/350000/1 year/Online/No BMI

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self" are entered in Agent Health family details page 
	And details entered for individuals "Self" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := Self # Pre Existing Conditions := Self # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And select "No" to answer further medical questions in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	Then accept Terms and Conditions for RetailHealth "<planId>"
	Then premium paid by "Proposer"
	Then mode of payment and its info are entered in Agent Health summary page
	Then proceed to the pay in Agent Health summary page
	Then mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1453 |
	
	
@Health_ArogyaSanjeevani_Sanity @AS_Eleven
Scenario Outline: Create New Health Insurance policy for Proposer with Arogya Sanjeevani Plan/350000/1 year/Online/High Cholestrol/No BMI

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self" are entered in Agent Health family details page 
	And details entered for individuals "Self" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := Self # Pre Existing Conditions := Self # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And select "No" to answer further medical questions in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	Then accept Terms and Conditions for RetailHealth "<planId>"
	Then premium paid by "Proposer"
	Then mode of payment and its info are entered in Agent Health summary page
	Then proceed to the pay in Agent Health summary page
	Then mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1501 |
	
@Health_ArogyaSanjeevani_Sanity @AS_Twelve
Scenario Outline: Create New Health Insurance policy for Proposer,Spouse with Arogya Sanjeevani Plan/500000/1 yearCheque/BMI Loading

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Spouse" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Self,Spouse # Alcohol := None" are selected in Agent Health personal habits page
	And tobacco consumption forms "Self := Smoke # Spouse := Smoke" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1466 |
	