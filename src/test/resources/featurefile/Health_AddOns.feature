Feature: To verify the Add Ons 

@Health_Maternity 
Scenario: To verify Maternity Add Ons 

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 25 # Spouse := 25" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 1" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Individual SI" in Agent Health plan details page 
	And maternity benefit details "Sum Insured := 25000 # Waiting Period := 4 years # SI for second child := 200" are entered in Agent Health plan details page
	And daily hospital cash cover details "Sum Insured := 1000 # Cash Cover Deductible Days := 1 days" are entered in Agent Health plan details page 
	And out patient benefit details "Sum Insured := 5000" are entered in Agent Health plan details page
	And co payment details "Percentage := 10%" are entered in Agent Health plan details page
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse" are entered in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	#And validate alcohol habits are not shown for "Self,Spouse" in Agent Health personal habits page 
	And validate alcohol habits are shown for "Child 1,Child 2,Father" in Agent Health personal habits page
#	And perform "Next" action in Agent Health personal habits page 
#	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page 
#	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page 
#	And perform "Next" action in Agent Health medical questions page 
#	And verify uw status as "Self := ACS # Spouse := ACS" in Agent Health summary page 
#	And accept the declaration in Agent Health summary page 
#	And payment info for "Send Payment" are entered in Agent Health summary page 
#	And proceed to the payment in Agent Health summary page
#	#And make the online payment
#	Then verify "Success" message is displayed in Agent Health success page 
#	And verify the policy has moved to "Effective" state in Agent transactions page 
#	And close the browser