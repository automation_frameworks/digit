Feature: To verify the features of Sfsp is working fine

@SFSPData
Scenario: SFSP ROLLOVER INDIVIDUAL 

Given logged to the application with details "User Name := 54315087 # Password := Digit@123"
When module "SFSP" is selected
And policy details "Policy Type := Rollover # Ownership Type := Corporate" are entered in Agent SFSP Insurance page
And building details "Occupancy Type:= Dwellings # Type of construction:= Pucca # Fire Fighting Arrangements:= Fire Extinguisher  # Is Basement Exposed := No # Floor Number:= 2 # Build up Year:= 2019 # Hypothecated To:= Indian Bank # Address := GM Palya # pincode := 560023" are entered in Agent SFSP Insurance page
And base coverage details "Building := 10000000 # Plinth & Foundation := 10000 # Machinery & Accessories := 10000 # Furniture & Fixtures := 10000 # Other Property := 10000" are entered in Agent SFSP Insurance page
And previous insurer details "Expiring Insurer := Others # Any Past Loss Experience := No" are entered in Agent SFSP Insurance page
And action "Show Premium" is performed in SFSP Insurance page
And insurer details "Insured Name := Insured # Pincode := 560001 # Address := address # Mobile Number := 9876543210 # Email ID := test@godigit.com" are entered in Agent SFSP Insurance page
And action "Save Quote" is performed in SFSP Insurance page
And payment details "Payment By Customer Cheque" are entered in Agent SFSP Insurance page
And action "Proceed To Pay" is performed in SFSP Insurance page
And verify the success page for SFSP Insurance
And verify the policyStatus

@ShopData
Scenario Outline: SHOP 

Given logged to the application with details "User Name := 54315087 # Password := Digit@123"
When module "Shop Insurance" is selected
And ownership type is selected as "Corporate" in Agent Shop Insurance page
And shop details "Shop Category := Art Dealers # Age of Property := 2 # Address of Risk Location := Digit Insurance # Square Feet Area := <Square Feet> # Floor Number := 2 " are entered in Agent Shop Insurance page
And SFSP details "Content Stock := 300000 # Terrorism := Yes" are entered in Agent Shop Insurance page
And burglary details "" are entered in Agent Shop Insurance page
And asset care details "Plate Glass Cover := 1000 # Neon Sign Cover := 1000 # Equipment 1 := Television # Serial Number 1 := KLSDJS9039 # Age 1 := 2 " are entered in Agent Shop Insurance page
And employee compensation details "No Of Employees := 5 # Total Monthly Salary := 10000" are entered in Agent Shop Insurance page
And action "Show Premium" is performed in Shop Insurance page
And insurer details "Insured Name := Insured # Pincode := 560001 # Address := address # Mobile Number := 9876543210 # Email ID := test@godigit.com" are entered in Agent Shop Insurance page
And action "Save Quote" is performed in Shop Insurance page
And payment details "Payment By Customer Cheque" are entered in Agent Shop Insurance page
And action "Proceed To Pay" is performed in Shop Insurance page
And verify the success page for Shop Insurance

Examples:
|Square Feet |
|1000|
|2000|
|3000|
|4000|

@Home
Scenario: HOME 

Given logged to the application with details "User Name := 54315087 # Password := Digit@123"
When module "Home Insurance" is selected
And ownership type is selected as "Corporate" in Agent Home Insurance page
And home details "Age of Property := 2 # Address of Risk Location := Digit Insurance # Square Feet Area := 1000" are entered in Agent Home Insurance page
And SFSP details "Terrorism := Yes" are entered in Agent Home Insurance page
And burglary details "" are entered in Agent Home Insurance page
And asset care details "Frame Number := GHJJH78738 # Cycle Make := SCOTT # Cycle Model := Model # Sum Insured := 10000 # Equipment 1 := Washing Machine # Age 1 := 2 " are entered in Agent Home Insurance page
And action "Show Premium" is performed in Home Insurance page
And insurer details "Insured Name := Insured # Pincode := 560001 # Address := address # Mobile Number := 9876543210 # Email ID := test@godigit.com" are entered in Agent Shop Insurance page
And action "Save Quote" is performed in Home Insurance page
And payment details "Payment By Customer Cheque" are entered in Agent Home Insurance page
And action "Proceed To Pay" is performed in Home Insurance page
And verify the success page for Home Insurance
And verify the policyStatus