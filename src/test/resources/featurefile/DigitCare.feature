@RunAllPreInspection
Feature: To verify the preinspection 

@PrivateCar_Inspection @DigitCare
Scenario: To verify the preinspection of Private Car
Given create a breakin policy for "Private Car" and do the preinspection
Given logged to the digitcare with details "User Name := DIGIT # Password := DiGiT@dm!n" 
When move to tab "Approvals" in digitcare 
 #D300037026 [Policy_Number]
	And search with details "Policy Number := [Policy_Number]  # Activity := Motor-4W-PreInspection-SelfSurvey # Status := Select Status" in Approvals tab 
	#And verify the count of records as "3" in Approvals tab
	And click on the Image search at row "1"
	#And verify the count of images as "16" in the inspection details page
When open the image "Back Side" in Inspection details
Then verify the section of "Back Side" for Private Car 
	And close the image for Private Car
When open the image "RC Book Back" in Inspection details
Then verify the section of "RC Book Back" for Private Car 
	And close the image for Private Car 
When open the image "RC Book Front" in Inspection details 
Then verify the section of "RC Book Front" for Private Car 
	And close the image for Private Car 
When open the image "Chassis Plate" in Inspection details 
Then verify the section of "Chassis Plate" for Private Car
	And close the image for Private Car 
When open the image "Engine Chamber" in Inspection details 
Then verify the section of "Engine Chamber" for Private Car 
	And close the image for Private Car 
When open the image "Wind Screen" in Inspection details 
Then verify the section of "Wind Screen" for Private Car 
	And close the image for Private Car
When open the image "Front Right Corner" in Inspection details 
Then verify the section of "Front Right Corner" for Private Car 
	And close the image for Private Car 
When open the image "Front Left Corner" in Inspection details
Then verify the section of "Front Left Corner" for Private Car 
	And close the image for Private Car 
When open the image "Dashboard" in Inspection details 
Then verify the section of "Dashboard" for Private Car 
	And close the image for Private Car 
When open the image "Back Left Corner" in Inspection details 
Then verify the section of "Back Left Corner" for Private Car 
	And close the image for Private Car
When open the image "Front Side" in Inspection details 
Then verify the section of "Front Side" for Private Car 
	And close the image for Private Car
When open the image "Right Side" in Inspection details 
Then verify the section of "Right Side" for Private Car 
	And close the image for Private Car
When open the image "Back Right Corner" in Inspection details 
Then verify the section of "Back Right Corner" for Private Car 
	And close the image for Private Car
When open the image "Left Side" in Inspection details 
Then verify the section of "Left Side" for Private Car 
	And close the image for Private Car
	And perform "Rule Engine Response" action in the Inspection details screen
	And perform "Approve" action in the Inspection details screen
	#And close the inspection details window
	
	
@GCV_4W @DigitCare
Scenario: To verify the preinspection of GCV 4W

Given create a breakin policy for "GCV 4W" and do the preinspection
Given logged to the digitcare with details "User Name := DIGIT # Password := DiGiT@dm!n" 
When move to tab "Approvals" in digitcare 
 #D300029375 D300022384
	And search with details "Policy Number := [Policy_Number] # Activity := Motor-CV-PreInspection-SelfSurvey # Status := Select Status" in Approvals tab 
	#And verify the count of records as "3" in Approvals tab
	And click on the Image search at row "1"
When open the image "Back Side" in Inspection details 
Then verify the section of "Back Side" of "GCV 4W" for Commercial Vehicle
	And close the image for Private Car 
When open the image "Front Side" in Inspection details 
Then verify the section of "Front Side" of "GCV 4W" for Commercial Vehicle
	And close the image for Private Car
When open the image "Right Side" in Inspection details
Then verify the section of "Right Side" of "GCV 4W" for Commercial Vehicle
	And close the image for Private Car
When open the image "Left Side" in Inspection details 
Then verify the section of "Left Side" of "GCV 4W" for Commercial Vehicle
	And close the image for Private Car
When open the image "RC Book Front" in Inspection details 
Then verify the section of "RC Book Front" of "GCV 4W" for Commercial Vehicle
	And close the image for Private Car
When open the image "RC Book Back" in Inspection details 
Then verify the section of "RC Book Back" of "GCV 4W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Chassis Plate" in Inspection details 
Then verify the section of "Chassis Plate" of "GCV 4W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Wind Screen" in Inspection details 
Then verify the section of "Wind Screen" of "GCV 4W" for Commercial Vehicle
	And close the image for Private Car
When open the image "Engine Chamber" in Inspection details 
Then verify the section of "Engine Chamber" of "GCV 4W" for Commercial Vehicle
	And close the image for Private Car
When open the image "Dashboard" in Inspection details 
Then verify the section of "Dashboard" of "GCV 4W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Front Left Corner" in Inspection details 
Then verify the section of "Front Left Corner" of "GCV 4W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Front Right Corner" in Inspection details 
Then verify the section of "Front Right Corner" of "GCV 4W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Back Left Corner" in Inspection details 
Then verify the section of "Back Left Corner" of "GCV 4W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Back Right Corner" in Inspection details 
Then verify the section of "Back Right Corner" of "GCV 4W" for Commercial Vehicle
	And close the image for Private Car	
	And perform "Rule Engine Response" action in the Inspection details screen
	And perform "Approve" action in the Inspection details screen
	#And close the inspection details window

@GCV_3W
Scenario: To verify the preinspection of GCV 3W

Given create a breakin policy for "GCV 3W" and do the preinspection
Given logged to the digitcare with details "User Name := DIGIT # Password := DiGiT@dm!n" 
When move to tab "Approvals" in digitcare 
 #D300029375 D300022384
	And search with details "Policy Number := [Policy_Number] # Activity := Motor-CV-PreInspection-SelfSurvey # Status := Select Status" in Approvals tab 
	#And verify the count of records as "3" in Approvals tab
	And click on the Image search at row "1"
When open the image "Back Side" in Inspection details 
Then verify the section of "Back Side" of "GCV 3W" for Commercial Vehicle
	And close the image for Private Car 
When open the image "Front Side" in Inspection details 
Then verify the section of "Front Side" of "GCV 3W" for Commercial Vehicle
	And close the image for Private Car
When open the image "Right Side" in Inspection details 
Then verify the section of "Right Side" of "GCV 3W" for Commercial Vehicle
	And close the image for Private Car
When open the image "Left Side" in Inspection details 
Then verify the section of "Left Side" of "GCV 3W" for Commercial Vehicle
	And close the image for Private Car
When open the image "RC Book Front" in Inspection details 
Then verify the section of "RC Book Front" of "GCV 3W" for Commercial Vehicle
	And close the image for Private Car
When open the image "RC Book Back" in Inspection details 
Then verify the section of "RC Book Back" of "GCV 3W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Chassis Plate" in Inspection details 
Then verify the section of "Chassis Plate" of "GCV 3W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Wind Screen" in Inspection details 
Then verify the section of "Wind Screen" of "GCV 3W" for Commercial Vehicle
	And close the image for Private Car
When open the image "Engine Chamber" in Inspection details 
Then verify the section of "Engine Chamber" of "GCV 3W" for Commercial Vehicle
	And close the image for Private Car
When open the image "Dashboard" in Inspection details 
Then verify the section of "Dashboard" of "GCV 3W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Front Left Corner" in Inspection details 
Then verify the section of "Front Left Corner" of "GCV 3W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Front Right Corner" in Inspection details 
Then verify the section of "Front Right Corner" of "GCV 3W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Back Left Corner" in Inspection details 
Then verify the section of "Back Left Corner" of "GCV 3W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Back Right Corner" in Inspection details 
Then verify the section of "Back Right Corner" of "GCV 3W" for Commercial Vehicle
	And close the image for Private Car	
	And perform "Rule Engine Response" action in the Inspection details screen
	And perform "Approve" action in the Inspection details screen
	#And close the inspection details window
	
@PCV_4W
Scenario: To verify the preinspection of PCV 4W

Given create a breakin policy for "PCV 51 Seater" and do the preinspection
Given logged to the digitcare with details "User Name := DIGIT # Password := DiGiT@dm!n" 
When move to tab "Approvals" in digitcare 
 #D300029375 D300022384
	And search with details "Policy Number := [Policy_Number] # Activity := Motor-CV-PreInspection-SelfSurvey # Status := Select Status" in Approvals tab 
	#And verify the count of records as "3" in Approvals tab
	And click on the Image search at row "1"
When open the image "Back Side" in Inspection details 
Then verify the section of "Back Side" of "PCV 4W" for Commercial Vehicle
	And close the image for Private Car 
When open the image "Front Side" in Inspection details 
Then verify the section of "Front Side" of "PCV 4W" for Commercial Vehicle
	And close the image for Private Car
When open the image "Right Side" in Inspection details 
Then verify the section of "Right Side" of "PCV 4W" for Commercial Vehicle
	And close the image for Private Car
When open the image "Left Side" in Inspection details 
Then verify the section of "Left Side" of "PCV 4W" for Commercial Vehicle
	And close the image for Private Car
When open the image "RC Book Front" in Inspection details 
Then verify the section of "RC Book Front" of "PCV 4W" for Commercial Vehicle
	And close the image for Private Car
When open the image "RC Book Back" in Inspection details 
Then verify the section of "RC Book Back" of "PCV 4W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Chassis Plate" in Inspection details 
Then verify the section of "Chassis Plate" of "PCV 4W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Wind Screen" in Inspection details 
Then verify the section of "Wind Screen" of "PCV 4W" for Commercial Vehicle
	And close the image for Private Car
When open the image "Engine Chamber" in Inspection details 
Then verify the section of "Engine Chamber" of "PCV 4W" for Commercial Vehicle
	And close the image for Private Car
When open the image "Dashboard" in Inspection details 
Then verify the section of "Dashboard" of "PCV 4W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Front Left Corner" in Inspection details 
Then verify the section of "Front Left Corner" of "PCV 4W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Front Right Corner" in Inspection details 
Then verify the section of "Front Right Corner" of "PCV 4W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Back Left Corner" in Inspection details 
Then verify the section of "Back Left Corner" of "PCV 4W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Back Right Corner" in Inspection details 
Then verify the section of "Back Right Corner" of "PCV 4W" for Commercial Vehicle
	And close the image for Private Car	
	And perform "Rule Engine Response" action in the Inspection details screen
	And perform "Approve" action in the Inspection details screen
	#And close the inspection details window
	
	
@PCV_3W
Scenario: To verify the preinspection of PCV 4W

Given create a breakin policy for "PCV 3W" and do the preinspection
Given logged to the digitcare with details "User Name := DIGIT # Password := DiGiT@dm!n" 
When move to tab "Approvals" in digitcare 
 #D300029375 D300022384
	And search with details "Policy Number := [Policy_Number] # Activity := Motor-CV-PreInspection-SelfSurvey # Status := Select Status" in Approvals tab 
	#And verify the count of records as "3" in Approvals tab
	And click on the Image search at row "1"
When open the image "Back Side" in Inspection details 
Then verify the section of "Back Side" of "PCV 3W" for Commercial Vehicle
	And close the image for Private Car 
When open the image "Front Side" in Inspection details 
Then verify the section of "Front Side" of "PCV 3W" for Commercial Vehicle
	And close the image for Private Car
When open the image "Right Side" in Inspection details 
Then verify the section of "Right Side" of "PCV 3W" for Commercial Vehicle
	And close the image for Private Car
When open the image "Left Side" in Inspection details 
Then verify the section of "Left Side" of "PCV 3W" for Commercial Vehicle
	And close the image for Private Car
When open the image "RC Book Front" in Inspection details 
Then verify the section of "RC Book Front" of "PCV 3W" for Commercial Vehicle
	And close the image for Private Car
When open the image "RC Book Back" in Inspection details 
Then verify the section of "RC Book Back" of "PCV 3W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Chassis Plate" in Inspection details 
Then verify the section of "Chassis Plate" of "PCV 3W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Wind Screen" in Inspection details 
Then verify the section of "Wind Screen" of "PCV 3W" for Commercial Vehicle
	And close the image for Private Car
When open the image "Engine Chamber" in Inspection details 
Then verify the section of "Engine Chamber" of "PCV 3W" for Commercial Vehicle
	And close the image for Private Car
When open the image "Dashboard" in Inspection details 
Then verify the section of "Dashboard" of "PCV 3W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Front Left Corner" in Inspection details 
Then verify the section of "Front Left Corner" of "PCV 3W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Front Right Corner" in Inspection details 
Then verify the section of "Front Right Corner" of "PCV 3W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Back Left Corner" in Inspection details 
Then verify the section of "Back Left Corner" of "PCV 3W" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Back Right Corner" in Inspection details 
Then verify the section of "Back Right Corner" of "PCV 3W" for Commercial Vehicle
	And close the image for Private Car	
	And assert the preinspection section
	And perform "Rule Engine Response" action in the Inspection details screen
	And perform "Approve" action in the Inspection details screen
	#And close the inspection details window
	And assert the preinspection section
	
@MISC
Scenario: To verify the preinspection of PCV 4W

Given create a breakin policy for "Misc" and do the preinspection
Given logged to the digitcare with details "User Name := DIGIT # Password := DiGiT@dm!n" 
When move to tab "Approvals" in digitcare 
 #D300029375 D300022384
	And search with details "Policy Number := [Policy_Number] # Activity := Motor-CV-PreInspection-SelfSurvey # Status := Select Status" in Approvals tab 
	#And verify the count of records as "3" in Approvals tab
	And click on the Image search at row "1"
When open the image "Back Side" in Inspection details 
Then verify the section of "Back Side" of "MISC" for Commercial Vehicle
	And close the image for Private Car 
When open the image "Front Side" in Inspection details 
Then verify the section of "Front Side" of "MISC" for Commercial Vehicle
	And close the image for Private Car
When open the image "Right Side" in Inspection details 
Then verify the section of "Right Side" of "MISC" for Commercial Vehicle
	And close the image for Private Car
When open the image "Left Side" in Inspection details 
Then verify the section of "Left Side" of "MISC" for Commercial Vehicle
	And close the image for Private Car
When open the image "RC Book Front" in Inspection details 
Then verify the section of "RC Book Front" of "MISC" for Commercial Vehicle
	And close the image for Private Car
When open the image "RC Book Back" in Inspection details 
Then verify the section of "RC Book Back" of "MISC" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Chassis Plate" in Inspection details 
Then verify the section of "Chassis Plate" of "MISC" for Commercial Vehicle
	And close the image for Private Car
When open the image "Wind Screen" in Inspection details 
Then verify the section of "Wind Screen" of "MISC" for Commercial Vehicle
	And close the image for Private Car
When open the image "Engine Chamber" in Inspection details 
Then verify the section of "Engine Chamber" of "MISC" for Commercial Vehicle
	And close the image for Private Car
When open the image "Dashboard" in Inspection details 
Then verify the section of "Dashboard" of "MISC" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Front Left Corner" in Inspection details 
Then verify the section of "Front Left Corner" of "MISC" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Front Right Corner" in Inspection details 
Then verify the section of "Front Right Corner" of "MISC" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Back Left Corner" in Inspection details 
Then verify the section of "Back Left Corner" of "MISC" for Commercial Vehicle
	And close the image for Private Car	
When open the image "Back Right Corner" in Inspection details 
Then verify the section of "Back Right Corner" of "MISC" for Commercial Vehicle
	And close the image for Private Car
#	And assert the preinspection section
#	And perform "Rule Engine Response" action in the Inspection details screen
#	And perform "Approve" action in the Inspection details screen
#	#And close the inspection details window
	And assert the preinspection section
	
@CreatePolicyAndPreInspect
Scenario: To create a policy and do th e Preinspection

#Given create a breakin policy for "Private Car" and do the preinspection
#Given create a "breakin" policy for "Private Car" in "uat" environment and do the preinspection
#Given create a "breakin" policy for "Private Car" in "uat" environment and do the preinspection
Given create a "breakin" policy for "Private Car" in "uat" environment without preinspection
	