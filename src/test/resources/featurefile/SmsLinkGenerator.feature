@LinkGenerator @Regression
Feature: Check SmsLinkGenerator functionality is working fine


Scenario: SmsLinkGenerator

Given user login to the application with details
When module "SMS Link Generator" is selected
And verify the page and enter partner name "Sriram Finanace" partner and contact number "9879879871"
And upload "Lead_Generator" excel sheet
And click on Send Message button
And verify Success Message from toast if message is sent