@Agent_Health
Feature: To verify the functionalities of Health Project

@HealthProject1
Scenario: HEALTH PROJECT LOADING BY WEIGHT 

	Given logged to the application with details "UserName :=  85335399 # Password := digit123" 
	When module "Health Project" is selected 
	And age details are entered as "Proposer := 30 # Spouse := 42 # Child 1 := 2" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "OPTION 3" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Floater SI # Sum Insured := 10,00,000" in Agent Health plan details page
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Child1" are entered in Agent Health family details page 
	And family details for "Self" is entered as "Height := 160 # Weight := 60" in Agent Health family details page 
	And family details for "Spouse" is entered as "Height := 160 # Weight := 50" in Agent Health family details page
	And family details for "Child1" is entered as "Height := 160 # Weight := 60" in Agent Health family details page
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := Self,Spouse # Alcohol := Self" are selected in Agent Health personal habits page 
	And tobacco consumption forms "Self := Both # Spouse := Smoke" are selected in Agent Health personal habits page 
	And alcohol consumption forms "Self := Daily" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page 
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACL # Spouse := ACL # Child 1 := ACS" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Cheque" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Success" message is displayed in Agent Health success page 
	And verify the policy has moved to "Effective" state in Agent transactions page 
	And close the browser 
	
@HealthProject2
Scenario: HEALTH PROJECT REFERRAL PED 

	Given logged to the application with details "UserName :=  85335399 # Password := digit123" 
	When module "Health Project" is selected 
	And unselect the proposer in Agent Health age details page
	And age details are entered as "Proposer := 31 # Spouse := 42 # Child1 := 2" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "OPTION 2" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Floater SI # Sum Insured := 10,00,000 # Policy Term := 1 year" in Agent Health plan details page
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Child1" are entered in Agent Health family details page 
	And family details for "Spouse" is entered as "Height := 160 # Weight := 60" in Agent Health family details page 
	And family details for "Child1" is entered as "Height := 160 # Weight := 50" in Agent Health family details page
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := Spouse # Alcohol := None" are selected in Agent Health personal habits page
	And tobacco consumption forms "Spouse := Smoke" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := Spouse # Pre Existing Conditions := Spouse" are entered in Agent Health medical questions page
	And pre existing diseases "Spouse := Diabetes" are entered in Agent Health medical questions page
	And select "Yes" to answer further medical questions in Agent Health medical questions page
	And diabetes details for "Spouse" is entered as "Type of Diabetes := Non Insulin dependant # Current Medication := Diet control # Hospitalized Due to Diabetes := No # Any complications For Diabetes:= No" in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Spouse := REF # Child1 := ACS" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Agent float" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Referral" message is displayed in Agent Health success page 
	And verify the policy has moved to "For Approval" state in Agent transactions page 
	And close the browser 
	
@HealthProject3
Scenario: HEALTH PROJECT REFERRAL PED

	Given logged to the application with details "UserName :=  85335399 # Password := digit123" 
	When module "Health Project" is selected 
	And unselect the proposer in Agent Health age details page
	And age details are entered as "Proposer := 31 # Spouse := 42 # Child1 := 2" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 1" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Floater SI # Sum Insured := 10,00,000 # Policy Term := 1 year" in Agent Health plan details page
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Child1" are entered in Agent Health family details page 
	And family details for "Spouse" is entered as "Height := 160 # Weight := 60" in Agent Health family details page 
	And family details for "Child1" is entered as "Height := 160 # Weight := 50" in Agent Health family details page
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := Spouse # Alcohol := None" are selected in Agent Health personal habits page
	And tobacco consumption forms "Spouse := Smoke" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := Spouse # Pre Existing Conditions := Spouse" are entered in Agent Health medical questions page
	And pre existing diseases "Spouse := Diabetes" are entered in Agent Health medical questions page
	And select "No" to answer further medical questions in Agent Health medical questions page
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Spouse := TEL # Child1 := TEL" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Agent float" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Referral" message is displayed in Agent Health success page 
	And verify the policy has moved to "For Approval- Telemer Required" state in Agent transactions page 
	And close the browser 
	
@HealthProject4
Scenario: HEALTH PROJECT DECLINED

	Given logged to the application with details "UserName :=  85335399 # Password := digit123" 
	When module "Health Project" is selected 
	And age details are entered as "Proposer := 31 # Spouse := 42 # Child1 := 2" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 3" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Floater SI # Sum Insured := 20,00,000" in Agent Health plan details page
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Child1" are entered in Agent Health family details page 
	And family details for "Self" is entered as "Height := 160 # Weight := 120" in Agent Health family details page
	And family details for "Spouse" is entered as "Height := 160 # Weight := 50" in Agent Health family details page 
	And family details for "Child1" is entered as "Height := 160 # Weight := 60" in Agent Health family details page
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := Self,Spouse # Pre Existing Conditions := Spouse" are entered in Agent Health medical questions page
	And pre existing diseases "Spouse := Diabetes" are entered in Agent Health medical questions page
	And select "Yes" to answer further medical questions in Agent Health medical questions page
	And diabetes details for "Spouse" is entered as "Type of Diabetes := Insulin dependant # Hospitalized Due to Diabetes := Yes # Records of hospitalization := Yes # Any complications For Diabetes:= Yes # Records of complications := Yes " in Agent Health medical questions page
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACL # Spouse := TEL # Child1 := TEL" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Agent float" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Referral" message is displayed in Agent Health success page 
	And verify the policy has moved to "For Approval- Telemer Required" state in Agent transactions page 
	And close the browser 