Feature: To verify the functionality of Claims 

@Claims
Scenario: To create a claim 

Given logged to the digitcare with details "User Name := DIGIT # Password := DiGiT@dm!n" 
When move to tab "Search Policy/Claims" in digitcare 
	And search with details "Policy Number := D300038501" in Search Policy/Claim tab
	And perform "Create" action for row "1" in Search Policy/Claim tab
	# Name of the Caller := Satheesh # Contact Number := 1234567890 # Email := ksdj@ksdjf.com 
	And enter the reporter details as "Reporter of Claim := Policy holder # Mobile Number := 1234567890 # Address := sdjk,sdjf" in Claims creation page
	And enter the loss details as "Date of Loss := 01 # Time of Loss := 2323 # Loss Location City := Chennai # Loss Location Address := ksjdhf387 # Description of Loss := skjdfhsjdfkh # Damaged Parts Details := sdjk,sdjf" in Claims creation page
	And enter the claims details as "Type of Claim := Vehicle Damage # Type of Cover := Motor Own Damage # Type of Loss := Roten bite # Cause of Loss := Accidental Collision Damage # Call Id := d760-5c2fc810-vcall-116848 # Third Party Damage := No # Who was Driving := Third-Party # Driver Name := sjdh # Driver Contact Number := 1234567890 # Driver License Number := sdhf8237" in Claims creation page
	And enter the enquiry details as "Police Has been Intimated := Yes # Police Station Name := jsd # FIR Number := jsdiisdj # Date of FIR := 02" in Claims creation page
	And perform "Register Claim" action in Claims creation page
	And accept the confirmation popup in Claims creation page