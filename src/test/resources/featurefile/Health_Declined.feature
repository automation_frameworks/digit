Feature: To verify the declined cases in Agent Health 

@Declined_1A 
Scenario: To verify declined scenario for 1A - Declined due to High BMI 
	Members : Proposer
Zone : A
Package : Option 1
SI Type : Individual
Sum Insured : 4,00,000
Policy Term : 1 year

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 45" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 1" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Individual SI" in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self" are entered in Agent Health family details page 
	And family details for "Self" is entered as "Height := 180 # Weight := 125" in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := DPM" in Agent Health summary page 
	And close the browser 
	
@Declined_2A 
Scenario: To verify the declined scenario for 2A - Declined due to Low BMI 
	Members : Proposer,Spouse
Zone : B
Package : Option 2
SI Type : Floater
Sum Insured : 3,00,000
Policy Term : 1 year

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 30 # Spouse := 27" in Agent Health age details page 
	And pincode is entered as "380001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 2" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Floater SI" in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse" are entered in Agent Health family details page 
	And family details for "Spouse" is entered as "Height := 60 # Weight := 65" in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := Self # Alcohol := None" are selected in Agent Health personal habits page 
	And tobacco consumption forms "Self := Both" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page 
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACL # Spouse := DPM" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Cheque" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Success" message is displayed in Agent Health success page 
	And verify the policy has moved to "Effective" state in Agent transactions page 
	And close the browser 
	
@Declined_2A2C 
Scenario: 
	To verify the declined scenario for 2A2C - Declined with Low BMI and High BMI 
	Description : BMI decline should be applied for children also if he is more than 18 yrs
	Members : Proposer,Spouse,Child 1,Child 2
Zone : C
Package : Option 3
SI Type : Individual
Sum Insured : 2,00,000
Policy Term : 1 year

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 60 # Spouse := 55 # Child 1 := 25 # Child 2 := 22" in Agent Health age details page 
	And pincode is entered as "160001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 3" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Individual SI" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Child 1,Child 2" are entered in Agent Health family details page 
	And family details for "Child 1" is entered as "Height := 180 # Weight := 125" in Agent Health family details page 
	And family details for "Child 2" is entered as "Height := 60 # Weight := 65" in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page 
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACS # Spouse := ACS # Child 1 := DPM # Child 2 := DPM" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Agent Float" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Success" message is displayed in Agent Health success page 
	And verify the policy has moved to "Effective" state in Agent transactions page 
	And close the browser 
	
@Declined_2A4C 
Scenario: To verify the declined scenario for 2A4C - Declined due to BMI 

	Description : Should be declined due to BMI if children age is more than 18 and should not be declined if children age is less than 18 years 
	Members : Proposer,Spouse,Child 1,Child 2,Child 3,Child 4
Zone : A
Package : Option 4
SI Type : Floater
Sum Insured : 5,00,000
Policy Term : 1 year

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 50 # Spouse := 45 # Child 1 := 25 # Child 2 := 22 # Child 3 := 8 # Child 4 := 4" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 4" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Floater SI # Sum Insured := 5,00,000 # Policy Term := 1 year" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Child 1,Child 2,Child 3,Child 4" are entered in Agent Health family details page 
	And family details for "Child 1" is entered as "Height := 180 # Weight := 125" in Agent Health family details page 
	And family details for "Child 2" is entered as "Height := 60 # Weight := 65" in Agent Health family details page 
	And family details for "Child 3" is entered as "Height := 180 # Weight := 125" in Agent Health family details page 
	And family details for "Child 4" is entered as "Height := 60 # Weight := 65" in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page 
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACS # Spouse := ACS # Child 1 := DPM # Child 2 := DPM # Child 3 := ACS # Child 4 := ACS" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Cheque" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Success" message is displayed in Agent Health success page 
	And verify the policy has moved to "Effective" state in Agent transactions page 
	And close the browser 
	
@Declined_4A 
Scenario: To verify the declined scenario for 4A - Declined with BMI 
	Members : Proposer,Spouse,Father,Mother
Zone : B
Package : Option 5
SI Type : Individual
Sum Insured : 6,00,000
Policy Term : 1 year

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 30 # Spouse := 25 # Father := 50 # Mother := 45" in Agent Health age details page 
	And pincode is entered as "380001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 5" for proposer in Agent Health plan selection page 
	And select "Option 5" for in laws in Agent Health plan selection page 
	And plan details are entered as "SI Type := Individual SI # Sum Insured := 6,00,000" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And plan details are entered as "SI Type := Individual SI # Sum Insured := 6,00,000" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Father,Mother" are entered in Agent Health family details page 
	And family details for "Father" is entered as "Height := 180 # Weight := 125" in Agent Health family details page 
	And family details for "Mother" is entered as "Height := 60 # Weight := 65" in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page 
	And gynaecological problem "Spouse := No # Mother := No" is selected in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACS # Spouse := ACS # Father := DPM # Mother := DPM" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Cheque" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Success" message is displayed in Agent Health success page 
	And verify the policy has moved to "Effective" state in Agent transactions page 
	And close the browser 
	
@Declined_4A4C
Scenario: To verify the declined scenario for 4A4C - Loading and BMI 
	Members : Proposer,Spouse,Child 1,Child 2,Child 3,Child 4,Father,Mother
Zone : A
Package : Option 7
SI Type : Individual
Sum Insured : 8,00,000
Policy Term : 1 year

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 30 # Spouse := 27 # Child 1:= 10 # Child 2 := 8 # Child 3 := 6 # Child 4:= 3 # Father := 60 # Mother := 55" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 7" for proposer in Agent Health plan selection page 
	And select "Option 7" for in laws in Agent Health plan selection page 
	And plan details are entered as "SI Type := Individual SI # Sum Insured := 8,00,000 # Policy Term := 1 year" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And plan details are entered as "SI Type := Individual SI # Sum Insured := 8,00,000 # Policy Term := 1 year" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Child 1,Child 2,Child 3,Child 4,Father,Mother" are entered in Agent Health family details page 
	And family details for "Spouse" is entered as "Height := 180 # Weight := 125" in Agent Health family details page 
	And family details for "Mother" is entered as "Height := 65 # Weight := 60" in Agent Health family details page
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := Self,Mother # Alcohol := None" are selected in Agent Health personal habits page 
	And tobacco consumption forms "Self := Chewable # Mother := Smoke" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page 
	And gynaecological problem "Spouse := No # Mother := No" is selected in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACL # Spouse := DPM # Child 1 := ACS # Child 2 := ACS # Child 3 := ACS # Child 4 := ACS # Father := ACS # Mother := DPM" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Cheque" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Success" message is displayed in Agent Health success page 
	And verify the policy has moved to "Effective" state in Agent transactions page 
	And close the browser