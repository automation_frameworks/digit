Feature: Sanity for Portability in Health Insurance

Background:
Given Logged-in into the application

	@RetailPA_Sanity @RPA_One @QuickSanity
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  1 |

	@RetailPA_Sanity @RPA_Two @QuickSanity
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  2 |

	@RetailPA_Sanity @RPA_Three @QuickSanity
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  3 |

	@RetailPA_Sanity @RPA_Four @QuickSanity
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  4 |

	@RetailPA_Sanity @RPA_Five @QuickSanity
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  5 |

	@RetailPA_Sanity @RPA_Six
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  6 |

	@RetailPA_Sanity @RPA_Seven
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  7 |

	@RetailPA_Sanity @RPA_Eight
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  8 |

	@RetailPA_Sanity @RPA_Nine
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  9 |

	@RetailPA_Sanity @RPA_Ten
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  10 |

	@RetailPA_Sanity @RPA_Eleven
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  11 |

	@RetailPA_Sanity @RPA_Twelve
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  12 |

	@RetailPA_Sanity @RPA_Thirteen
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  13 |

	@RetailPA_Sanity @RPA_Fourteen
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  14 |

	@RetailPA_Sanity @RPA_Fifteen
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  15 |

	@RetailPA_Sanity @RPA_Sixteen
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  16 |

	@RetailPA_Sanity @RPA_Seventeen
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  17 |

	@RetailPA_Sanity @RPA_Eighteen
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  18 |

	@RetailPA_Sanity @RPA_Nineteen
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  19 |

	@RetailPA_Sanity @RPA_Twenty
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  20 |

	@RetailPA_Sanity @RPA_TwentyOne
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  21 |

	@RetailPA_Sanity @RPA_TwentyTwo
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  22 |

	@RetailPA_Sanity @RPA_TwentyThree
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  23 |

	@RetailPA_Sanity @RPA_TwentyFour
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  24 |

	@RetailPA_Sanity @RPA_TwentyFive
	Scenario Outline: RetailPA policy

	When Clicked on module "Retail PA"
	And "Create New Policy" button is clicked
	And IMD code if needed is provided from retailId "<retailId>"
	And cover for individuals from provided retailId "<retailId>"
	And earning member provided from "<retailId>"
	And occupation class entered from "<retailId>"
	And view options for "<retailId>"
	And plan for group selected from "<retailId>"
	And plan details for group provided from "<retailId>"
	And coverage details for group provided from "<retailId>"
	And recalculate and proceed
	And member details entered for individuals from "<retailId>"
	And medic information entered from "<retailId>"
	And verify eligibility for policy creation
	And store the proposal number in database for retailId "<retailId>"
	And download policy PDF
	And accept Terms and Conditions for RetailPA "<retailId>"
	And upload documents if asked
	And premium paid by "Proposer"
	And mode of payment fetched from database from retailId "<retailId>"
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success for retailPA
	And close the browser
	
	Examples:
	| retailId |
	|  25 |
	
