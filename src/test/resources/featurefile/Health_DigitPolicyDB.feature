Feature: Functionality of Health Insurance

Background:
Given Logged-in into the application

@First
Scenario Outline: Create New Health Insurance policy for Proposer with Digit_Gold/3L/1year/Tobacco/Alcohol/No_PED/Online_payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer"
	And Enter "Proposer" pinCode "pinCode"
	And view options "View Options"
	And plan selected with details and planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self" are entered in Agent Health family details page 
	And details entered for individuals "Self" as "Height := FromDatabase # Weight := FromDatabase"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Self # Alcohol := Self" are selected in Agent Health personal habits page
	And tobacco consumption forms "Self := Both" are selected in Agent Health personal habits page
	And alcohol consumption forms "Self := Daily" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And fetch the proposal number
	And download policy PDF
#	And verify uw status as "Self := REF # Spouse := REF" in Agent Health summary page
	Then accept the declaration in Agent Health summary page 
#	And verify Member Details
	Then premium paid by "Proposer"
	Then mode of payment and its info are entered in Agent Health summary page
	Then proceed to the pay in Agent Health summary page
	Then mock payment done
	And close the browser
	
	Examples:
	| planId |
	| 481 |
	
@Second
Scenario Outline: Create New Health Insurance policy for Spouse with Digit_Silver/3L/2year/Tobacco/Alcohol/PED(Diabetes,Gastritis + 1)/Generate_payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Spouse"
	And unselect Proposer
	And Enter "Proposer" pinCode "pinCode"
	And view options "View Options"
	And plan selected with details and planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse" as "Height := FromDatabase # Weight := FromDatabase"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Spouse # Alcohol := Spouse" are selected in Agent Health personal habits page
	And tobacco consumption forms "Spouse := Both" are selected in Agent Health personal habits page
	And alcohol consumption forms "Spouse := Daily" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := Spouse # Pre Existing Conditions := Spouse" are entered in Agent Health medical questions page
	And select "Yes" to answer further medical questions in Agent Health medical questions page
	And answer all further medical questions for "Spouse"
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And fetch the proposal number
	And download policy PDF
	#And verify uw status as "Self := REF # Spouse := REF" in Agent Health summary page
	And accept the declaration in Agent Health summary page 
	#And verify Member Details
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	And close the browser
	
	Examples:
	| planId |
	| 968 |
	
@Third
Scenario Outline: Create New Health Insurance policy for Proposer with Digit_Diamond/4.5L/3year/Tobacco/Alcohol/PED(Diabetes,Gastritis + 1)/Send_payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer"
	And Enter "Proposer" pinCode "pinCode"
	And view options "View Options"
	And plan selected with details and planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self" are entered in Agent Health family details page 
	And details entered for individuals "Self" as "Height := FromDatabase # Weight := FromDatabase"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Self # Alcohol := Self" are selected in Agent Health personal habits page
	And tobacco consumption forms "Self := Smoke" are selected in Agent Health personal habits page
	And alcohol consumption forms "Self := Once in a week" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := Self # Pre Existing Conditions := Self" are entered in Agent Health medical questions page
	And select "No" to answer further medical questions in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And fetch the proposal number
	And download policy PDF
	#And verify uw status as "Self := REF # Spouse := REF" in Agent Health summary page
	And accept the declaration in Agent Health summary page 
	#And verify Member Details
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	And close the browser
	
	Examples:
	| planId |
	| 47 |
	
@Fourth
Scenario Outline: Create New Health Insurance policy for Proposer,Spouse,Child1 with Digit_Silver/3.5L/1year/Tobacco/Alcohol/No_PED/Online_payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer"
	And cover for "Spouse"
	And cover for "Child1"
	And Enter "Proposer" pinCode "pinCode"
	And view options "View Options"
	And plan selected with details and planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse,Child1" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse,Child1" as "Height := FromDatabase # Weight := FromDatabase"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Spouse # Alcohol := Self" are selected in Agent Health personal habits page
	And tobacco consumption forms "Spouse := Smoke" are selected in Agent Health personal habits page
	And alcohol consumption forms "Self := Once in a week" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And fetch the proposal number
	And download policy PDF
	#And verify uw status as "Self := REF # Spouse := REF" in Agent Health summary page
	And accept the declaration in Agent Health summary page 
	#And verify Member Details
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	And close the browser
	
	Examples:
	| planId |
	| 961 |
	
@Fifth
Scenario Outline: Create New Health Insurance policy for Proposer,Spouse,Child1 with Digit_Diamond/5.5L/2year/Tobacco/Alcohol/PED(Diabetes,Gastritis + 1)/Cheque_payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer"
	And cover for "Spouse"
	And cover for "Child1"
	And Enter "Proposer" pinCode "pinCode"
	And view options "View Options"
	And plan selected with details and planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse,Child1" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse,Child1" as "Height := FromDatabase # Weight := FromDatabase"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Spouse # Alcohol := Self" are selected in Agent Health personal habits page
	And tobacco consumption forms "Spouse := Smoke" are selected in Agent Health personal habits page
	And alcohol consumption forms "Self := Once in a week" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := Child1 # Pre Existing Conditions := Spouse" are entered in Agent Health medical questions page
	And select "Yes" to answer further medical questions in Agent Health medical questions page
	And answer all further medical questions for "Spouse"
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And fetch the proposal number
	And download policy PDF
	#And verify uw status as "Self := REF # Spouse := REF" in Agent Health summary page
	And accept the declaration in Agent Health summary page 
	#And verify Member Details
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	And close the browser
	
	Examples:
	| planId |
	| 306 |
	
@Sixth
Scenario Outline: Create New Health Insurance policy for Proposer,Spouse,Child1 with Digit_Gold/7.5L/3year/Tobacco/Alcohol/PED(Diabetes,Gastritis + 1)/Send_payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer"
	And cover for "Spouse"
	And cover for "Child1"
	And Enter "Proposer" pinCode "pinCode"
	And view options "View Options"
	And plan selected with details and planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse,Child1" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse,Child1" as "Height := FromDatabase # Weight := FromDatabase"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Spouse # Alcohol := Self" are selected in Agent Health personal habits page
	And tobacco consumption forms "Spouse := Smoke" are selected in Agent Health personal habits page
	And alcohol consumption forms "Self := Once in a week" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := Child1" are entered in Agent Health medical questions page
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page
	And select "No" to answer further medical questions in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And fetch the proposal number
	And download policy PDF
	#And verify uw status as "Self := REF # Spouse := REF" in Agent Health summary page
	And accept the declaration in Agent Health summary page 
	#And verify Member Details
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	And close the browser
	
	Examples:
	| planId |
	| 839 |
	
@Seventh
Scenario Outline: Create New Health Insurance policy for Proposer,Spouse,Father with Digit_Diamond/6L/3year/Tobacco/Alcohol/PED(Diabetes,Gastritis + 1)/Online_payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer"
	And cover for "Spouse"
	And cover for "Father"
	And Enter "Proposer" pinCode "pinCode"
	And view options "View Options"
	And plan selected with details and planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse,Father" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse,Father" as "Height := FromDatabase # Weight := FromDatabase"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Spouse # Alcohol := Self" are selected in Agent Health personal habits page
	And tobacco consumption forms "Spouse := Smoke" are selected in Agent Health personal habits page
	And alcohol consumption forms "Self := Once in a week" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And fetch the proposal number
	And download policy PDF
	#And verify uw status as "Self := REF # Spouse := REF" in Agent Health summary page
	And accept the declaration in Agent Health summary page 
	#And verify Member Details
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	And close the browser
	
	Examples:
	| planId |
	| 81 |
	
@Eighth
Scenario Outline: Create New Health Insurance policy for Proposer,Spouse,Mother with Digit_Gold/7L/2year/Tobacco/Alcohol/PED(Diabetes,Gastritis + 1)/Cheque_payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer"
	And cover for "Spouse"
	And cover for "Mother"
	And Enter "Proposer" pinCode "pinCode"
	And view options "View Options"
	And plan selected with details and planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse,Mother" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse,Mother" as "Height := FromDatabase # Weight := FromDatabase"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Spouse # Alcohol := Self" are selected in Agent Health personal habits page
	And tobacco consumption forms "Spouse := Smoke" are selected in Agent Health personal habits page
	And alcohol consumption forms "Self := Once in a week" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := Mother # Pre Existing Conditions := Spouse" are entered in Agent Health medical questions page
	And select "Yes" to answer further medical questions in Agent Health medical questions page
	And answer all further medical questions for "Spouse"
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And fetch the proposal number
	And download policy PDF
	#And verify uw status as "Self := REF # Spouse := REF" in Agent Health summary page
	And accept the declaration in Agent Health summary page 
	#And verify Member Details
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	And close the browser
	
	Examples:
	| planId |
	| 822 |
	
@Ninth
Scenario Outline: Create New Health Insurance policy for Proposer,Spouse,FatherInLaw with Digit_Silver/5L/1year/Tobacco/Alcohol/PED(Diabetes,Gastritis + 1)/Send_payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer"
	And cover for "Spouse"
	And cover for "Father in Law"
	And Enter "Proposer" pinCode "pinCode"
	And view options "View Options"
	And plan selected with details and planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse,Father in Law" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse,Father in Law" as "Height := FromDatabase # Weight := FromDatabase"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Spouse # Alcohol := Self" are selected in Agent Health personal habits page
	And tobacco consumption forms "Spouse := Smoke" are selected in Agent Health personal habits page
	And alcohol consumption forms "Self := Once in a week" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := Father in Law" are entered in Agent Health medical questions page
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page
	And select "No" to answer further medical questions in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And fetch the proposal number
	And download policy PDF
	#And verify uw status as "Self := REF # Spouse := REF" in Agent Health summary page
	And accept the declaration in Agent Health summary page 
	#And verify Member Details
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	And close the browser
	
	Examples:
	| planId |
	| 1012 |
	