Feature: To verify whether referral,loading and decline scenarios for Pre Existing diseases 

@Thyroid_TC1 
Scenario: To verify the loading for Hypothyroidism - 1A 
	Description : Loading should be applied for Hypothyroidism > Thyroxine > No associated complications

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 45" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 1" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Individual SI" in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self" are entered in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := Self" are entered in Agent Health medical questions page 
	And pre existing diseases "Self := Thyroid problem" are entered in Agent Health medical questions page 
	And select "Yes" to answer further medical questions in Agent Health medical questions page 
	And thyroid details for "Self" is entered as "Type of Thyroid := Hypothyroidism # Current Medication := Thyroxine # When was it diagnosed := Less than 5 yrs # Any associated complications := No" in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACL" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Cheque" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Success" message is displayed in Agent Health success page 
	And verify the policy has moved to "Effective" state in Agent transactions page 
	And close the browser 
	
@Thyroid_TC2 
Scenario: To verify the loading for Hypothyroidism - 2A 
	Description : Loading should be applied for Hypothyroidism > Eltroxine > No associated complications
			  Referral should be applied for Hypothyroidism > Not Listed Medicines
	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 25 # Spouse := 22" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 1" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Floater SI" in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse" are entered in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := Self,Spouse" are entered in Agent Health medical questions page 
	And pre existing diseases "Self := Thyroid problem # Spouse := Thyroid problem" are entered in Agent Health medical questions page 
	And select "Yes" to answer further medical questions in Agent Health medical questions page 
	And thyroid details for "Self" is entered as "Type of Thyroid := Hypothyroidism # Current Medication := Eltroxine # When was it diagnosed := 5 to 9 yrs # Any associated complications := No" in Agent Health medical questions page 
	And thyroid details for "Spouse" is entered as "Type of Thyroid := Hypothyroidism # Current Medication := Not Listed" in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACL # Spouse := REF" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Cheque" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Referral" message is displayed in Agent Health success page 
	And verify the policy has moved to "For Approval" state in Agent transactions page 
	And close the browser