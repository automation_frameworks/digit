@Travel
Feature: To verify the functionalities of Travel

@Travel_2 @Agent_Sanity
Scenario: TRAVEL

Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Travel Insurance" is selected
And travel plan details "Countries := Italy,France # Departure Date := [TODAY] # No of Journey days := 4" are entered
And traveller details "Travel Type := Self/Group # Policy Holder DOB := 03-05-1993 # Number of Traveller := 1" are entered
#And date of birth for traveller "2" is entered as "30-05-1993"
#And date of birth for traveller "3" is entered as "29-10-1995"
#And date of birth for traveller "4" is entered as "30-05-1999"
#And date of birth for traveller "5" is entered as "30-05-1993"
#And date of birth for traveller "6" is entered as "29-10-1995"
#And date of birth for traveller "7" is entered as "30-05-1999"
#And date of birth for traveller "8" is entered as "30-05-1993"
#And date of birth for traveller "9" is entered as "29-10-1995"
#And date of birth for traveller "10" is entered as "30-05-1999"
And action "Show Plans" is clicked in travel
And travel plan with "Plan Number := 1" is selected 
And personal info for traveller "1" is entered as "Passport Number := AADS121AS # Nationality := Indian # Name := Satheesh # Mobile Number := 9479230928 # Email := sayjj@ksjd.com # Gender := Male # City := Bengaluru # Pincode := 560001 # Address := jjkasjdk # Pre Existing disease := Hypertension,Cancer # Nominee Name := kajskdj # Relationship := Brother"
#And personal info for traveller "2" is entered as "Passport Number := AADS121AS # Nationality := Indian # Name := Satheesha # Mobile Number := 9479230928 # Email := sayjj@ksjd.com # Gender := Male # City := Bengaluru # Pincode := 560001 # Address := jjkasjdk # Pre Existing disease := Hypertension,Cancer # Nominee Name := kajskdj # Relationship := Brother"
#And personal info for traveller "3" is entered as "Passport Number := AADS121AS # Nationality := Indian # Name := Satheeshb # Mobile Number := 239479230928 # Email := sayjj@ksjd.com # Gender := Male # City := Bengaluru # Pincode := 560001 # Address := jjkasjdk # Pre Existing disease := Hypertension,Cancer # Nominee Name := kajskdj # Relationship := Brother"
#And personal info for traveller "4" is entered as "Passport Number := AADS121AS # Nationality := Indian # Name := Satheeshc # Mobile Number := 239479230928 # Email := sayjj@ksjd.com # Gender := Male # City := Bengaluru # Pincode := 560001 # Address := jjkasjdk # Pre Existing disease := Hypertension,Cancer # Nominee Name := kajskdj # Relationship := Brother"
#And personal info for traveller "5" is entered as "Passport Number := AADS121AS # Nationality := Indian # Name := Satheeshd # Mobile Number := 239479230928 # Email := sayjj@ksjd.com # Gender := Male # City := Bengaluru # Pincode := 560001 # Address := jjkasjdk # Pre Existing disease := Hypertension,Cancer # Nominee Name := kajskdj # Relationship := Brother"
#And personal info for traveller "6" is entered as "Passport Number := AADS121AS # Nationality := Indian # Name := Satheeshe # Mobile Number := 239479230928 # Email := sayjj@ksjd.com # Gender := Male # City := Bengaluru # Pincode := 560001 # Address := jjkasjdk # Pre Existing disease := Hypertension,Cancer # Nominee Name := kajskdj # Relationship := Brother"
#And personal info for traveller "7" is entered as "Passport Number := AADS121AS # Nationality := Indian # Name := Satheeshf # Mobile Number := 239479230928 # Email := sayjj@ksjd.com # Gender := Male # City := Bengaluru # Pincode := 560001 # Address := jjkasjdk # Pre Existing disease := Hypertension,Cancer # Nominee Name := kajskdj # Relationship := Brother"
#And personal info for traveller "8" is entered as "Passport Number := AADS121AS # Nationality := Indian # Name := Satheeshg # Mobile Number := 239479230928 # Email := sayjj@ksjd.com # Gender := Male # City := Bengaluru # Pincode := 560001 # Address := jjkasjdk # Pre Existing disease := Hypertension,Cancer # Nominee Name := kajskdj # Relationship := Brother"
#And personal info for traveller "9" is entered as "Passport Number := AADS121AS # Nationality := Indian # Name := Satheeshh # Mobile Number := 239479230928 # Email := sayjj@ksjd.com # Gender := Male # City := Bengaluru # Pincode := 560001 # Address := jjkasjdk # Pre Existing disease := Hypertension,Cancer # Nominee Name := kajskdj # Relationship := Brother"
#And personal info for traveller "10" is entered as "Passport Number := AADS121AS # Nationality := Indian # Name := Satheeshk # Mobile Number := 239479230928 # Email := sayjj@ksjd.com # Gender := Male # City := Bengaluru # Pincode := 560001 # Address := jjkasjdk # Pre Existing disease := Hypertension,Cancer # Nominee Name := kajskdj # Relationship := Brother"
And accept the declaration in travel
And payment details are entered for "Online Payment" 
And action "Issue Policy" is clicked in travel
And perform the Online Payment
And verify policy issuance is successful for Travel
#And action "Download Policy" is clicked in travel

@Travel_3
Scenario: To verify normal policy issuance for 3 people

Given logged to the application with details "User Name := 54315087 # Password := Digit@123$"
When module "Travel Insurance" is selected
And travel plan details "Countries := Italy,France # Departure Date := [TODAY] # No of Journey days := 4" are entered
And traveller details "Travel Type := Self/Group # Policy Holder DOB := 03-05-1993 # Number of Traveller := 4" are entered
And date of birth for traveller "2" is entered as "30-05-1993"
And date of birth for traveller "3" is entered as "29-10-1995"
And date of birth for traveller "4" is entered as "30-05-1999"
And action "Show Plans" is clicked in travel
And travel plan with "Premium := 463" is selected
And personal info for traveller "1-4" is entered as " " 
And accept the declaration in travel
And payment details are entered for "Online Payment" 
And action "Issue Policy" is clicked in travel
And perform the Online Payment
And verify policy issuance is successful for Travel
And action "Download Policy" is clicked in travel