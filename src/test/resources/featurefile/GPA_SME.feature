Feature: Sanity for GPA_SME

Background:
Given Login into the application for GPA_SME

@GPA_SME_One @GPA_SME_Sanity
Scenario Outline: Test-Case-1

	When Clicked on module "GPA SME" in agent portal
	And create new policy
	And provide insured details for GPA_SME from "<GPA_SME_Id>"
	And provide prospective policy details for GPA_SME from "<GPA_SME_Id>"
	And provide coverage details for GPA_SME from "<GPA_SME_Id>"
	And provide premium details for GPA_SME from "<GPA_SME_Id>"
	And click on "Calculate Premium" buttton for "<GPA_SME_Id>"
	And click on "Submit" buttton for "<GPA_SME_Id>"
	And click on "Proceed For Binding Quote" buttton for "<GPA_SME_Id>"
	And provide payment details for GPA_SME from "<GPA_SME_Id>"
	And store policy created for GPA_SME in database
	And click on "Proceed To Pay" buttton for "<GPA_SME_Id>"
	And close the browser
	
Examples:
| GPA_SME_Id |
|	  1		 |