Feature: Sanity for Create New Policy in Health Insurance

Background:
Given Logged-in into the application

@HealthInsurance_Sanity @DP_One @QuickSanity
Scenario Outline: Create New Health Insurance policy for Proposer with Digit_Silver/5L/1year/NoTobacco/NoAlcohol/No_PED/Send_payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self" are entered in Agent Health family details page 
	And details entered for individuals "Self" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := Self # Pre Existing Conditions := Self # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And select "No" to answer further medical questions in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1009 |
	
	
@HealthInsurance_Sanity @DP_Two @QuickSanity
Scenario Outline: Create New Health Insurance policy for Proposer,Spouse with Digit_Silver/7L/2year/NoTobacco/NoAlcohol/No_PED/Send_payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Spouse" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1063 |
	
	
@HealthInsurance_Sanity @DP_Three @QuickSanity
Scenario Outline: Create New Health Insurance policy for Proposer,Spouse,Child1 with Digit_Silver/8L/3year/Tobacco/NoAlcohol/No_PED/Send_payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Spouse" from provided planId "<planId>"
	And cover for "Child1" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse,Child1" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse,Child1" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Self # Alcohol := None" are selected in Agent Health personal habits page
	And tobacco consumption forms "Self := Chewable" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1091 |
	
	
@HealthInsurance_Sanity @DP_Four @QuickSanity
Scenario Outline: Create New Health Insurance policy for Proposer with Digit_Silver/5L/3year/NoTobacco/NoAlcohol/No_PED/Send_payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self" are entered in Agent Health family details page 
	And details entered for individuals "Self" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Self # Alcohol := None" are selected in Agent Health personal habits page
	And tobacco consumption forms "Self := Both" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1019 |
	
	
@HealthInsurance_Sanity @DP_Five @QuickSanity
Scenario Outline: Create New Health Insurance policy for Proposer,Father with Digit_Silver/5L/2year/NoTobacco/NoAlcohol/No_PED/Send_payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Father" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And choose plan for parent from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Father" are entered in Agent Health family details page 
	And details entered for individuals "Self,Father" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1061 |
	
	
@HealthInsurance_Sanity @DP_Six
Scenario Outline: Create New Health Insurance policy for Proposer,Spouse,Father with Digit_Silver/8L/3year/Tobacco/NoAlcohol/No_PED/Send_payment

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Spouse" from provided planId "<planId>"
	And cover for "Father" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And choose plan for parent from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse,Father" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse,Father" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Self # Alcohol := None" are selected in Agent Health personal habits page
	And tobacco consumption forms "Self := Chewable" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1091 |
	
	@HealthInsurance_Sanity @DP_Seven
	Scenario Outline: Create New Health Insurance policy for Proposer,Spouse with Digit Gold/Floater SI/400000/1 year/Online/No BMI/560005

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Spouse" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 505 |
	
	@HealthInsurance_Sanity @DP_Eight
	Scenario Outline: Create New Health Insurance policy for Proposer,Spouse with Digit Gold/600000/1 year/Online/BMI Loading

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Spouse" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 553 |
	
	@HealthInsurance_Sanity @DP_Nine
	Scenario Outline: Create New Health Insurance policy for Proposer,Spouse with Digit Gold/900000/1 year/Online/No BMI

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Spouse" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Self,Spouse # Alcohol := None" are selected in Agent Health personal habits page
	And tobacco consumption forms "Self := Smoke # Spouse := Smoke" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 625 |
	
	@HealthInsurance_Sanity @DP_Ten
	Scenario Outline: Create New Health Insurance policy for Proposer,Spouse with Digit Gold/400000/2 year/Online/Thyroid/No BMI

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Spouse" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Spouse" are entered in Agent Health family details page 
	And details entered for individuals "Self,Spouse" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := Self # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page
	And select "No" to answer further medical questions in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 509 |
	
	
	@HealthInsurance_Sanity @DP_Eleven
	Scenario Outline: Create New Health Insurance policy for Proposer,Mother with Digit Gold/400000/3 year/Online/No BMI

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Mother" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And choose plan for parent from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Mother" are entered in Agent Health family details page 
	And details entered for individuals "Self,Mother" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And gynaecological problem "Mother := No" is selected in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 513 |
	
	
	@HealthInsurance_Sanity @DP_Twelve
	Scenario Outline: Create New Health Insurance policy for Proposer,MotherInLaw with Digit Gold/400000/2 year/Online/Thyroid/BMI Loading

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "MotherInLaw" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And choose plan for parent from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,MotherInLaw" are entered in Agent Health family details page 
	And details entered for individuals "Self,MotherInLaw" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := Self # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And gynaecological problem "MotherInLaw := No" is selected in Agent Health medical questions page
	And select "No" to answer further medical questions in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1497 |
	
	
	@HealthInsurance_Sanity @DP_Thirteen
	Scenario Outline: Create New Health Insurance policy for Proposer,Child1,Mother with Digit Gold/900000/1 year/Online/BMI Loading

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Child1" from provided planId "<planId>"
	And cover for "Mother" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And choose plan for parent from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Child1,Mother" are entered in Agent Health family details page 
	And details entered for individuals "Self,Child1,Mother" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Self # Alcohol := None" are selected in Agent Health personal habits page
	And tobacco consumption forms "Self := Smoke" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And gynaecological problem "Mother := No" is selected in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1498 |
	
	
	@HealthInsurance_Sanity @DP_Fourteen
	Scenario Outline: Create New Health Insurance policy for Proposer,Child1 with Digit Diamond/600000/2 year/Cheque

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Child1" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Child1" are entered in Agent Health family details page 
	And details entered for individuals "Self,Child1" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 318 |
	
	
	@HealthInsurance_Sanity @DP_Fifteen
	Scenario Outline: Create New Health Insurance policy for Proposer,Child1,Child2 with Digit Diamond/800000/2 year/Cheque/BMI Loading

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Child1" from provided planId "<planId>"
	And cover for "Child2" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Child1,Child2" are entered in Agent Health family details page 
	And details entered for individuals "Self,Child1,Child2" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 366 |
	
	
	@HealthInsurance_Sanity @DP_Sixteen
	Scenario Outline: Create New Health Insurance policy for Proposer,Child1,Child2 with Digit Diamond/1000000/2 year/Cheque/No BMI

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Child1" from provided planId "<planId>"
	And cover for "Child2" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Child1,Child2" are entered in Agent Health family details page 
	And details entered for individuals "Self,Child1,Child2" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Self # Alcohol := None" are selected in Agent Health personal habits page
	And tobacco consumption forms "Self := Both" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 414 |
	
	
	@HealthInsurance_Sanity @DP_Seventeen
	Scenario Outline: Create New Health Insurance policy for Proposer,Child1 with Digit Diamond/600000/3 year/Cheque/Hypertension

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Child1" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Child1" are entered in Agent Health family details page 
	And details entered for individuals "Self,Child1" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := Self # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And select "No" to answer further medical questions in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 322 |
	
	
@HealthInsurance_Sanity @DP_Eighteen
Scenario Outline: Create New Health Insurance policy for Proposer,FatherInLaw withDigit Diamond/6000001 year/Cheque

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "FatherInLaw" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And choose plan for parent from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,FatherInLaw" are entered in Agent Health family details page 
	And details entered for individuals "Self,FatherInLaw" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 314 |
	
	
	@HealthInsurance_Sanity @DP_Nineteen
	Scenario Outline: Create New Health Insurance policy for Proposer,Mother with Digit Diamond/600000/3 year/Cheque/Asthma/BMI Loading

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Mother" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And choose plan for parent from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Mother" are entered in Agent Health family details page 
	And details entered for individuals "Self,Mother" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := Self # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And gynaecological problem "Mother := No" is selected in Agent Health medical questions page
	And select "No" to answer further medical questions in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1499 |
	
	
@HealthInsurance_Sanity @DP_Twenty
Scenario Outline: Create New Health Insurance policy for Proposer,FatherInLaw with Digit Diamond/1000000/1 year/Online/Diabetes

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "FatherInLaw" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And choose plan for parent from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,FatherInLaw" are entered in Agent Health family details page 
	And details entered for individuals "Self,FatherInLaw" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := Self # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And select "No" to answer further medical questions in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 409 |
	
	
@HealthInsurance_Sanity @DP_Twentyone
Scenario Outline: Create New Health Insurance policy for Proposer,Child1,Child2 with Digit Diamond/1000000/2 year/Cheque/BMI Loading

	When Clicked on module "Health Insurance"
	And "Create New Policy" button is clicked
	And IMD code "1000523" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And cover for "Child1" from provided planId "<planId>"
	And cover for "Child2" from provided planId "<planId>"
	And cover for "FatherInLaw" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And view options for "<planId>"
	And choose plan for family from planId "<planId>"
	And choose plan for parent from planId "<planId>"
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And plan details selected from planId "<planId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self,Child1,Child2,FatherInLaw" are entered in Agent Health family details page 
	And details entered for individuals "Self,Child1,Child2,FatherInLaw" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And personal habits "Tobacco := Self # Alcohol := None" are selected in Agent Health personal habits page
	And tobacco consumption forms "Self := Both" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for planId "<planId>"
	And download policy PDF
	And accept Terms and Conditions for RetailHealth "<planId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId |
	| 1500 |
	
	
