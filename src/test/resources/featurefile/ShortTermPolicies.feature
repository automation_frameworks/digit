Feature: To verify the functionalities of Commercial Vehicle is working fine

@Commercial_Breakin @Breakin
Scenario: CVOD BREAKIN

Given user login to the application with details
#Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := COMMERCIAL VEHICLE COMPREHENSIVE # Vehicle with Registration := true #Vehicle Category := GOODS CARRYING # Usage Type := National Permit #Vehicle Make := ARJUN # Vehicle Model := PICK UP # Vehicle Variant := Delivery Van (797.0)(Diesel) # Registration Year := 2018 # Vehicle Ownership := INDIVIDUAL" are entered
And previous policy details "Don't know previous Policy details := true" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And button "Save Quote" is clicked
And click on download buttun
And survey details "Survey Type := Self Inspection" are entered
And button "Submit" is clicked
And verify quote issuance is successful for Commercial Vehicle
And button "Download Quote" is clicked
Then verify whether file has downloaded 

Then update the survey comments for the quote created

#Given logged into ABS with details "UserName := testall # Password := digit123" 
#	And select module "Contract" in ABS home page 
#	And search with details "Policy Number := [Policy_Number]" in ABS Search page 
#	And switch to "Questions" tab in ABS Main Page 
#	And complete the Preinspection process
#Given user login to the application with details
And open the policy with "Policy Number := [Policy_Number]"
And button "Recalculate" is clicked
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And button "Generate policy" is clicked
And verify policy issuance is successful for Commercial Vehicle
And verify the policyStatus


@CVOD_ShortTerm 
Scenario: CVOD ROLLOVER
Vehicle Ownership : Corporate

Given user login to the application with details
#Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := COMMERCIAL VEHICLE COMPREHENSIVE # Half Yearly Mode := True # Vehicle with Registration := true #Vehicle Category := GOODS CARRYING # Usage Type := National Permit #Vehicle Make := ARJUN # Vehicle Model := PICK UP # Vehicle Variant := Delivery Van (797.0)(Diesel) # Registration Year := 2018 # Vehicle Ownership := INDIVIDUAL" are entered
And previous policy details "" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And nominee details "Nominee Name := Sathesh # Nominee Relation := Brother" are entered
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And upload the previous policy document
And button "Generate Policy" is clicked
And verify policy issuance is successful for Commercial Vehicle
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded 




@CVTP_FireAndTheft_FullTerm
Scenario: CVTP ROLLOVER

Given user login to the application with details
#Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := CVTP FIRE AND THEFT # Vehicle with Registration := true #Vehicle Category := GOODS CARRYING # Usage Type := National Permit #Vehicle Make := ARJUN # Vehicle Model := PICK UP # Vehicle Variant := Delivery Van (797.0)(Diesel) # Registration Year := 2018  # Registration Month := March # Vehicle Ownership := INDIVIDUAL" are entered
#And previous policy details "" are entered

#And previous policy details "Previous Policy Expiry Date := [TODAY]" are entered
#And previous policy details "Don't know previous Policy details := true" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And nominee details "Nominee Name := Sathesh # Nominee Relation := Brother" are entered
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And button "Generate Policy" is clicked
And verify quote issuance is successful for Commercial Vehicle
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded 



@CVTP_New
Scenario: CVTP NEW

Given user login to the application with details
#Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := COMMERCIAL VEHICLE THIRD PARTY # Brand New Vehicle := true # Vehicle Category := MISCELLANEOUS # Usage Type := Excavators #Vehicle Make := ACE # Vehicle Model := 11 Tons # Vehicle Variant := Mobile Crane (1.0)(Diesel) # Manufacturing Year := 2019 # Vehicle Ownership := CORPORATE" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And button "Generate Policy" is clicked
And verify quote issuance is successful for Commercial Vehicle
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded 
