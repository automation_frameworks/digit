@Property @Agent_Sanity @Regression
Feature: To verify the end-to-end functionality of Employee Compensation module

@EmpComp
Scenario: EMPLOYEE COMPENSATION

Given user login to the application with details
When module "Employee Comp" is selected
And policy details "Policy Type := Renewals/Rollover # Policy Holder := Corporate # Table A/B := Table B # Occupational Disease Extn. := Yes # Contractors Employees Cov := Yes # Medical Expenses Extn. Limit per person := 1,50,000 # Aggregate Limit := 10000" are entered in the Employee Compensation page
And work description details "Work Description := Architects # Sub Category := NA # Short Description of work := Skilled # Wages/Salary Per month Per person := 40000 # Total no. of employees := 20" are entered in the Employee Compensation page
And previous insurer details "Previous Insurer := Acko General Insurance Limited # Previous Claims := No" are entered in the Employee Compensation page
And clicks on the "Show Premium" button in the Employee Compensation page
And enters the discount or loading details "Discount/ Loading := 30" in the Employee Compensation page
And clicks on the "Apply" button in the Employee Compensation page
And the customer details "Name of the Insured := Deepjyoti Barman # Mobile number := 9436145337 # Email ID := deepjyoti.barman@godigit.com" are entered in the Employee Compensation page
And risk location details "Anywhere in india := true" are entered in the in the Employee Compensation page
And communication details "Full Address := Atlantis, 95, 4th B Cross Road, Koramangala, Bengaluru, Karnataka # Pincode := 560095" are entered in the Employee Compensation page
And clicks on the "Save Quote" button in the Employee Compensation page
Then verify the quote number and the quote download option are visible in the footer of the Employee Compensation page
And verify if the quick quote premium for Employee Compensation matches with the create quote premium
When clicks on the "Submit" button in the Employee Compensation page
And selects "Yes" as the answer to the question - 'This update will be the final update for this quote. Do you want to proceed?' in the Employee Compensation page
And payment details "Payment Mode := Agent Float" are entered in the Employee Compensation page
And clicks on the "Proceed to Pay" button in the Employee Compensation page
Then verify the success page for Employee Compensation
And verify the policyStatus