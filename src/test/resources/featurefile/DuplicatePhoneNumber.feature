@DuplicatePhoneNumber
Feature: To verify the whether we get toaster for phone number


@DuplicatePhoneNumber
Scenario Outline: 4W ROLLOVER

Given user login to the application with details
#Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := FOUR WHEELER COMPREHENSIVE # Vehicle with Registration := true # Vehicle Make := BAJAJ # Vehicle Model := QUTE RE60 # Vehicle Variant := PETROL (216.0)(Petrol)(4 str) # Registration Year := 2018 # Vehicle Ownership := CORPORATE" are entered
And previous policy details "Previous Policy Expiry Date := [TODAY]" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk # Mobile Number := <Mobile Number>" are entered
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And upload the previous policy document
And button "Generate Policy" is clicked
And verify policy issuance is successful for Four Wheeler
#And button "Download Policy For Email" is clicked
#And verify the policyStatus
#Then verify whether file has downloaded


Examples:
	|Mobile Number|
	|9879879871|
	|9879879871|
	|9879879871|
	|9879879871|
#	|9879879871|