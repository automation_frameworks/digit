@Regression @FourWheeler_Extra @Motor_Extra
Feature: To verify the policy issuance of various Four Wheeler policy types

@FourWheeler_New_3+3
Scenario: Four Wheeler New 3+3

Given user login to the application with details
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := FOUR WHEELER COMPREHENSIVE # Brand New Vehicle := true # Policy Type := 3 YEAR OD + 3 YEAR TP # RTO Code := KA01 # Vehicle Make := HONDA # Vehicle Model := AMAZE # Vehicle Variant := 1.2 E (1199.0)(Petrol)(5 str) # Manufacturing Year := 2019 # Vehicle Ownership := INDIVIDUAL" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And enter payment details
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And upload the previous policy document
And button "Generate Policy" is clicked
And verify quote issuance is successful for Commercial Vehicle
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded

@FourWheeler_New_1ODOnly
Scenario: Four Wheeler New 1 Year OD Only

Given user login to the application with details
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := FOUR WHEELER COMPREHENSIVE # Brand New Vehicle := true # Policy Type := 1 YEAR OD ONLY # RTO Code := KA01 # Vehicle Make := HONDA # Vehicle Model := AMAZE # Vehicle Variant := 1.2 E (1199.0)(Petrol)(5 str) # Manufacturing Year := 2019 # Vehicle Ownership := CORPORATE" are entered
And third party policy details "" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And enter payment details
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And upload the previous policy document
And button "Generate Policy" is clicked
And verify quote issuance is successful for Commercial Vehicle
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded