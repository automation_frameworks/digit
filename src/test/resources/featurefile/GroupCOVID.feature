Feature: Sanity for GMC Large

Background:
Given Login into the application for GroupCOVID

@GroupCOVID_One @GroupCOVID_Sanity
Scenario Outline: Create New Policy for GMC_Large

	When Clicked on module "Group COVID" in agent portal
	And create new policy for GroupCOVID
	And provide company details from "<covidId>"
	And provide policy details from "<covidId>"
	And provide special conditions from "<covidId>"
	And provide communication details from "<covidId>"
	And click on "Proceed" buttton for "<covidId>"
	
Examples:
|	covidId 	|
|		1		|