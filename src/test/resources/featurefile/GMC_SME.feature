Feature: Sanity for GMC_SME

Background:
Given Login into the application for GMC Large

@GMC_SME_One @GMC_SME_Sanity
Scenario Outline: Test-Case-1

	When Clicked on module "GMC SME" in agent portal
	And create new policy
	And provide insured details for GMC SME from "<GMC_SME_Id>"
	And provide prospective policy details for GMC SME from "<GMC_SME_Id>"
	And provide coverage details for GMC SME from "<GMC_SME_Id>"
	And provide premium details for GMC SME from "<GMC_SME_Id>"
	And click on "Calculate Premium" buttton for "<GMC_SME_Id>"
	And click on "Forward to Underwriter" buttton for "<GMC_SME_Id>"
	And close the browser
	
Examples:
| GMC_SME_Id |
|	  1		 |