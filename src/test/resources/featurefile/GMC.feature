Feature: GMC_Ops

Background:
Given Login into the application for GMC_Ops

@GMC_Ops_Sanity
Scenario Outline: Test_Case_1

	When created new customer for GMC_Ops from "<gmc_Ops_NewCustomer_Id>"
	And created new quote for GMC_Ops from "<gmc_Ops_NewQuote_Id>"
	And opened rap and created master policy for GMC_Ops from "<rapId>"
	And mapped master policy for GMC_Ops from "<rapId>"
	And upload tarrif document for GMC_Ops
	And add child policy for GMC_Ops
	Then close the browser

Examples:
|	gmc_Ops_NewCustomer_Id	|	gmc_Ops_NewQuote_Id	|
|			1				|			1			|
