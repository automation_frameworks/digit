@Property @Agent_Sanity @Regression
Feature: To verify the end-to-end functionality of CPM module

@CPM
Scenario: CPM

Given user login to the application with details
When module "CPM" is selected
And the following policy details are entered in the CPM page
  | policytype        | policyholder |
  | Renewals/Rollover | Corporate    |
And the following machinery details are entered in the CPM page
  | machinerytype   | make | model  | capacity | serialno  | chasisno   | ageofmachinery | machineactivity | isregundermotorvehicleact | isusedonpublicroad | registrationnumber |
  | Aggregate plant | CAT  | 3306 T | 18300    | CD99371DE | 2893482294 | 5              | None            | No                        | No                 |                    |
And the following location of machinery details are entered in the CPM page
  | fulladdress | pincode | anywhereinindia |
  |             |         | true            |
And the following coverages details are entered in the CPM page
  | basesuminsured | escalation | floatercover | terrorism | thirdpartyliability | ownerssurroundingproperty | clearanceandremovalofdebris | additionalcustomduty | expressfreight | airfreight |
  | 1000000        | 15         | true         | true      | 10000               | 10000                     | 10000                       | 10000                | 10000          | 10000      |
And the following previous insurer details are entered in the CPM page
  | policyexpirydate | previousinsurer                | previouspolicynumber | previousclaimhistory | claimamount | claimdescription |
  |                  | Acko General Insurance Limited | ACK0019938494        | No                   |             |                  |
And clicks on the "Show Premium" button in the CPM page
And the following discount/loading detail is entered in the CPM page
  | discountloading |
  | 30              |
And clicks on the "Apply" button in the CPM page
And the following document details are entered in the CPM page
  | documenttype1  | Invoice Copy                                                   |
  | documentname1  | /src/main/resources/documents/invoice.pdf                      |
  | documenttype2  | Previous Insurer Policy Schedule                               |
  | documentname2  | /src/main/resources/documents/prev_insurer_policy_schedule.pdf |
  | documenttype3  | Risk inspection report                                         |
  | documentname3  | /src/main/resources/documents/risk_inspection_report.pdf       |
And the following customer details are entered in the CPM page
  | nameoftheinsured | mobilenumber | emailid                      | gstnumber | fulladdress                                                       | pincode | hypothecatedto |
  | Deepjyoti Barman | 9436145337   | deepjyoti.barman@godigit.com |           | Atlantis, 95, 4th B Cross Road, Koramangala, Bengaluru, Karnataka | 560095  |                |
And clicks on the "Save Quote" button in the CPM page
Then verify the quote number and the quote download option are visible in the footer of the CPM page
And verify if the quick quote premium for CPM matches with the create quote premium
When clicks on the "Submit" button in the CPM page
And selects "Yes" as the answer to the question - 'This update will be the final update for this quote. Do you want to proceed?' in the CPM page
And the folllowing payment details are entered in the CPM page
  | paymentmode            |
  | Agent Float            |
And clicks on the "Proceed to Pay" button in the CPM page
Then verify the success page for CPM
And verify the policyStatus