Feature: API Testing for Motor Products

@ExistingTwoWheelerComprehensive @AgentFloat_1YrOD_1YrTP_E2WC @AllMotorAPI
Scenario: AgentFloat_1YrOD_1YrTP for ExistingTwoWheelerComprehensive

	Given test AgentFloat_OneYrOD_OneYrTP for ExistingTwoWheelerComprehensive

@ExistingTwoWheelerComprehensive @AgentFloat_2YrOD_2YrTP_E2WC @AllMotorAPI
Scenario: AgentFloat_2YrOD_2YrTP for ExistingTwoWheelerComprehensive

	Given test AgentFloat_TwoYrOD_TwoYrTP for ExistingTwoWheelerComprehensive

@ExistingTwoWheelerComprehensive @AgentFloat_3YrOD_3YrTP_E2WC @AllMotorAPI
Scenario: AgentFloat_3YrOD_3YrTP for ExistingTwoWheelerComprehensive

	Given test AgentFloat_ThreeYrOD_ThreeYrTP for ExistingTwoWheelerComprehensive

@ExistingTwoWheelerComprehensive @AgentFloat_1YrOD_with_prevailing_TPP_E2WC @AllMotorAPI
Scenario: AgentFloat_1YrOD_with_prevailing_TPP for ExistingTwoWheelerComprehensive

	Given test AgentFloat_OneYrOD_with_prevailing_TPP for ExistingTwoWheelerComprehensive

@ExistingTwoWheelerComprehensive @AgentFloat_1YrOD_with_TPP_Expired_E2WC @AllMotorAPI
Scenario: AgentFloat_1YrOD_with_TPP_Expired for ExistingTwoWheelerComprehensive

	Given test AgentFloat_OneYrOD_with_TPP_Expired for ExistingTwoWheelerComprehensive

@ExistingTwoWheelerComprehensive @Cheque_E2WC @AllMotorAPI
Scenario: Cheque for ExistingTwoWheelerComprehensive

	Given test Cheque for ExistingTwoWheelerComprehensive

@ExistingTwoWheelerComprehensive @Online_E2WC @AllMotorAPI
Scenario: Online for ExistingTwoWheelerComprehensive

	Given test Online for ExistingTwoWheelerComprehensive

@ExistingTwoWheelerComprehensive @SelfInspection_E2WC @AllMotorAPI
Scenario: SelfInspection for ExistingTwoWheelerComprehensive

	Given test SelfInspection for ExistingTwoWheelerComprehensive

@ExistingTwoWheelerComprehensive @SendPreInspectionLink_E2WC @AllMotorAPI
Scenario: SendPreInspectionLink for ExistingTwoWheelerComprehensive

	Given test SendPreInspectionLink for ExistingTwoWheelerComprehensive

@ExistingTwoWheelerComprehensive @GeneratePaymentLink_E2WC @AllMotorAPI
Scenario: GeneratePaymentLink for ExistingTwoWheelerComprehensive

	Given test GeneratePaymentLink for ExistingTwoWheelerComprehensive







@ExistingTwoWheelerThirdParty @AgentFloat_1YrTP_E2WTP @AllMotorAPI
Scenario: AgentFloat_1YrTP for ExistingTwoWheelerThirdParty

	Given test AgentFloat_OneYrTP for ExistingTwoWheelerThirdParty

@ExistingTwoWheelerThirdParty @AgentFloat_2YrTP_E2WTP @AllMotorAPI
Scenario: AgentFloat_2YrTP for ExistingTwoWheelerThirdParty

	Given test AgentFloat_TwoYrTP for ExistingTwoWheelerThirdParty

@ExistingTwoWheelerThirdParty @AgentFloat_3YrTP_E2WTP @AllMotorAPI
Scenario: AgentFloat_3YrTP FOR ExistingTwoWheelerThirdParty

	Given test AgentFloat_ThreeYrTP for ExistingTwoWheelerThirdParty

@ExistingTwoWheelerThirdParty @Cheque_E2WTP @AllMotorAPI
Scenario: Cheque for ExistingTwoWheelerThirdParty

	Given test Cheque for ExistingTwoWheelerThirdParty

@ExistingTwoWheelerThirdParty @Online_E2WTP @AllMotorAPI
Scenario: Online for ExistingTwoWheelerThirdParty

	Given test Online for ExistingTwoWheelerThirdParty

@ExistingTwoWheelerThirdParty @GeneratePaymentLink_E2WTP @AllMotorAPI
Scenario: GeneratePaymentLink for ExistingTwoWheelerThirdParty

	Given test GeneratePaymentLink for ExistingTwoWheelerThirdParty







@ExistingFourWheelerComprehensive @AgentFloat_1YrOD_1YrTP_E4WC @AllMotorAPI
Scenario: AgentFloat_1YrOD_1YrTP for ExistingFourWheelerComprehensive

	Given test AgentFloat_OneYrOD_OneYrTP for ExistingFourWheelerComprehensive

@ExistingFourWheelerComprehensive @AgentFloat_1YrOD_with_prevailing_TPP_E4WC @AllMotorAPI
Scenario: AgentFloat_1YrOD_with_prevailing_TPP for ExistingFourWheelerComprehensive

	Given test AgentFloat_OneYrOD_with_prevailing_TPP for ExistingFourWheelerComprehensive

@ExistingFourWheelerComprehensive @AgentFloat_1YrOD_with_TPP_Expired_E4WC @AllMotorAPI
Scenario: AgentFloat_1YrOD_with_TPP_Expired for ExistingFourWheelerComprehensive

	Given test AgentFloat_OneYrOD_with_TPP_Expired for ExistingFourWheelerComprehensive

@ExistingFourWheelerComprehensive @Cheque_E4WC @AllMotorAPI
Scenario: Cheque for ExistingFourWheelerComprehensive

	Given test Cheque for ExistingFourWheelerComprehensive

@ExistingFourWheelerComprehensive @Online_E4WC @AllMotorAPI
Scenario: Online for ExistingFourWheelerComprehensive

	Given test Online for ExistingFourWheelerComprehensive

@ExistingFourWheelerComprehensive @SelfInspection_E4WC @AllMotorAPI
Scenario: SelfInspection for ExistingFourWheelerComprehensive

	Given test SelfInspection for ExistingFourWheelerComprehensive

@ExistingFourWheelerComprehensive @SendPreInspectionLink_E4WC @AllMotorAPI
Scenario: SendPreInspectionLink for ExistingFourWheelerComprehensive

	Given test SendPreInspectionLink for ExistingFourWheelerComprehensive

@ExistingFourWheelerComprehensive @GeneratePaymentLink_E4WC @AllMotorAPI
Scenario: GeneratePaymentLink for ExistingFourWheelerComprehensive

	Given test GeneratePaymentLink for ExistingFourWheelerComprehensive







@ExistingFourWheelerThirdParty @AgentFloat_E4WTP @AllMotorAPI
Scenario: AgentFloat for ExistingFourWheelerThirdParty

	Given test AgentFloat for ExistingFourWheelerThirdParty

@ExistingFourWheelerThirdParty @Cheque_E4WTP @AllMotorAPI
Scenario: Cheque for ExistingFourWheelerThirdParty

	Given test Cheque for ExistingFourWheelerThirdParty

@ExistingFourWheelerThirdParty @Online_E4WTP @AllMotorAPI
Scenario: Online for ExistingFourWheelerThirdParty

	Given test Online for ExistingFourWheelerThirdParty

@ExistingFourWheelerThirdParty @GeneratePaymentLink_E4WTP @AllMotorAPI
Scenario: GeneratePaymentLink for ExistingFourWheelerThirdParty

	Given test GeneratePaymentLink for ExistingFourWheelerThirdParty







@ExistingCommercialVehicleComprehensive @AgentFloat_ECVC @AllMotorAPI
Scenario: AgentFloat for ExistingCommercialVehicleComprehensive

	Given test AgentFloat for ExistingCommercialVehicleComprehensive

@ExistingCommercialVehicleComprehensive @Cheque_ECVC @AllMotorAPI
Scenario: Cheque for ExistingCommercialVehicleComprehensive

	Given test Cheque for ExistingCommercialVehicleComprehensive

@ExistingCommercialVehicleComprehensive @Online_ECVC @AllMotorAPI
Scenario: Online for ExistingCommercialVehicleComprehensive

	Given test Online for ExistingCommercialVehicleComprehensive

@ExistingCommercialVehicleComprehensive @SelfInspection_ECVC @AllMotorAPI
Scenario: SelfInspection for ExistingCommercialVehicleComprehensive

	Given test SelfInspection for ExistingCommercialVehicleComprehensive

@ExistingCommercialVehicleComprehensive @SendPreInspectionLink_ECVC @AllMotorAPI
Scenario: SendPreInspectionLink for ExistingCommercialVehicleComprehensive

	Given test SendPreInspectionLink for ExistingCommercialVehicleComprehensive







@ExistingCommercialVehicleThirdParty @AgentFloat_ECVTP @AllMotorAPI
Scenario: AgentFloat for ExistingCommercialVehicleThirdParty

	Given test AgentFloat for ExistingCommercialVehicleThirdParty

@ExistingCommercialVehicleThirdParty @Cheque_ECVTP @AllMotorAPI
Scenario: Cheque for ExistingCommercialVehicleThirdParty

	Given test Cheque for ExistingCommercialVehicleThirdParty

@ExistingCommercialVehicleThirdParty @Online_ECVTP @AllMotorAPI
Scenario: Online for ExistingCommercialVehicleThirdParty

	Given test Online for ExistingCommercialVehicleThirdParty

@ExistingCommercialVehicleThirdParty @GeneratePaymentLink_ECVTP @AllMotorAPI
Scenario: GeneratePaymentLink for ExistingCommercialVehicleThirdParty

	Given test GeneratePaymentLink for ExistingCommercialVehicleThirdParty







@NewTwoWheelerComprehensive @AgentFloat_N2WC @AllMotorAPI
Scenario: AgentFloat for NewTwoWheelerComprehensive

	Given test AgentFloat for NewTwoWheelerComprehensive

@NewTwoWheelerComprehensive @Cheque_N2WC @AllMotorAPI
Scenario: Cheque for NewTwoWheelerComprehensive

	Given test Cheque for NewTwoWheelerComprehensive

@NewTwoWheelerComprehensive @Online_N2WC @AllMotorAPI
Scenario: Online for NewTwoWheelerComprehensive

	Given test Online for NewTwoWheelerComprehensive

@NewTwoWheelerComprehensive @GeneratePaymentLink_N2WC @AllMotorAPI
Scenario: GeneratePaymentLink for NewTwoWheelerComprehensive

	Given test GeneratePaymentLink for NewTwoWheelerComprehensive







@NewTwoWheelerThirdParty @AgentFloat_N2WTP @AllMotorAPI
Scenario: AgentFloat for NewTwoWheelerThirdParty

	Given test AgentFloat for NewTwoWheelerThirdParty

@NewTwoWheelerThirdParty @Online_N2WTP @AllMotorAPI
Scenario: Online for NewTwoWheelerThirdParty

	Given test Online for NewTwoWheelerThirdParty

@NewTwoWheelerThirdParty @GeneratePaymentLink_N2WTP @AllMotorAPI
Scenario: GeneratePaymentLink for NewTwoWheelerThirdParty

	Given test GeneratePaymentLink for NewTwoWheelerThirdParty







@NewFourWheelerComprehensive @AgentFloat_1YrOD_N4WC @AllMotorAPI
Scenario: AgentFloat_1YrOD for NewFourWheelerComprehensive

	Given test AgentFloat_OneYrOD for NewFourWheelerComprehensive
	
@NewFourWheelerComprehensive @AgentFloat_1YrOD_3YrTP_N4WC @AllMotorAPI
Scenario: AgentFloat_1YrOD_3YrTP for NewFourWheelerComprehensive

	Given test AgentFloat_OneYrOD_ThreeYrTP for NewFourWheelerComprehensive
	
@NewFourWheelerComprehensive @Cheque_N4WC @AllMotorAPI
Scenario: Cheque for NewFourWheelerComprehensive

	Given test Cheque for NewFourWheelerComprehensive
	
@NewFourWheelerComprehensive @Online_N4WC @AllMotorAPI
Scenario: Online for NewFourWheelerComprehensive

	Given test Online for NewFourWheelerComprehensive
	
@NewFourWheelerComprehensive @GeneratePaymentLink_N4WC @AllMotorAPI
Scenario: GeneratePaymentLink for NewFourWheelerComprehensive

	Given test GeneratePaymentLink for NewFourWheelerComprehensive







@NewFourWheelerThirdParty @AgentFloat_N4WTP @AllMotorAPI
Scenario: AgentFloat for NewFourWheelerThirdParty

	Given test AgentFloat for NewFourWheelerThirdParty

@NewFourWheelerThirdParty @Cheque_N4WTP @AllMotorAPI
Scenario: Cheque for NewFourWheelerThirdParty

	Given test Cheque for NewFourWheelerThirdParty

@NewFourWheelerThirdParty @Online_N4WTP @AllMotorAPI
Scenario: Online for NewFourWheelerThirdParty

	Given test Online for NewFourWheelerThirdParty

@NewFourWheelerThirdParty @GeneratePaymentLink_N4WTP @AllMotorAPI
Scenario: GeneratePaymentLink for NewFourWheelerThirdParty

	Given test GeneratePaymentLink for NewFourWheelerThirdParty







@NewCommercialVehicleComprehensive @AgentFloat_1YrPolicy_NCVC @AllMotorAPI
Scenario: AgentFloat_1YrPolicy for NewCommercialVehicleComprehensive

	Given test AgentFloat_OneYrPolicy for NewCommercialVehicleComprehensive

@NewCommercialVehicleComprehensive @AgentFloat_ShortTermMode_NCVC @AllMotorAPI
Scenario: AgentFloat_ShortTermMode for NewCommercialVehicleComprehensive

	Given test AgentFloat_ShortTermMode for NewCommercialVehicleComprehensive

@NewCommercialVehicleComprehensive @Cheque_NCVC @AllMotorAPI
Scenario: Cheque for NewCommercialVehicleComprehensive

	Given test Cheque for NewCommercialVehicleComprehensive

@NewCommercialVehicleComprehensive @Online_NCVC @AllMotorAPI
Scenario: Online for NewCommercialVehicleComprehensive

	Given test Online for NewCommercialVehicleComprehensive

@NewCommercialVehicleComprehensive @GeneratePaymentLink_NCVC @AllMotorAPI
Scenario: GeneratePaymentLink for NewCommercialVehicleComprehensive

	Given test GeneratePaymentLink for NewCommercialVehicleComprehensive







@NewCommercialVehicleThirdParty @AgentFloat_1YrPolicy_NCVTP @AllMotorAPI
Scenario: AgentFloat_1YrPolicy for NewCommercialVehicleThirdParty

	Given test AgentFloat_OneYrPolicy for NewCommercialVehicleThirdParty

@NewCommercialVehicleThirdParty @AgentFloat_3Months_NCVTP @AllMotorAPI
Scenario: AgentFloat_3Months for NewCommercialVehicleThirdParty

	Given test AgentFloat_ThreeMonths for NewCommercialVehicleThirdParty

@NewCommercialVehicleThirdParty @Cheque_NCVTP @AllMotorAPI
Scenario: Cheque for NewCommercialVehicleThirdParty

	Given test Cheque for NewCommercialVehicleThirdParty

@NewCommercialVehicleThirdParty @Online_NCVTP @AllMotorAPI
Scenario: Online for NewCommercialVehicleThirdParty

	Given test Online for NewCommercialVehicleThirdParty

@NewCommercialVehicleThirdParty @GeneratePaymentLink_NCVTP @AllMotorAPI
Scenario: GeneratePaymentLink for NewCommercialVehicleThirdParty

	Given test GeneratePaymentLink for NewCommercialVehicleThirdParty
