@Health_Standard
Feature: To verify the STP cases in Agent Health 

Scenario: To verify the standard scenario for 1A 

	Members : Proposer
Zone : A
Package : Option 1
SI Type : Individual
Sum Insured : 4,00,000
Policy Term : 1 year 


	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 45" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 1" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Individual SI # Sum Insured := 4,00,000 # Policy Term := 1 year" in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self" are entered in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACS" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Cheque" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Success" message is displayed in Agent Health success page 
	And verify the policy has moved to "Effective" state in Agent transactions page 
	And close the browser 

@2A
Scenario: To verify the standard scenario for 2A 
	Members : Proposer,Spouse
Zone : B
Package : Option 2
SI Type : Floater
Sum Insured : 3,00,000
Policy Term : 1 year

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 30 # Spouse := 27" in Agent Health age details page 
	And pincode is entered as "380001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 2" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Floater SI # Sum Insured := 3,00,000 # Policy Term := 1 year" in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse" are entered in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page 
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACS # Spouse := ACS" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Cheque" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Success" message is displayed in Agent Health success page 
	And verify the policy has moved to "Effective" state in Agent transactions page 
	And close the browser 

@2A2C
Scenario: To verify the standard scenario for 2A2C 
	Members : Proposer,Spouse,Child 1,Child 2
Zone : C
Package : Option 3
SI Type : Individual
Sum Insured : 2,00,000
Policy Term : 1 year

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 30 # Spouse := 27 # Child 1 := 5 # Child 2 := 2" in Agent Health age details page 
	And pincode is entered as "160001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 3" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Individual SI # Sum Insured := 2,00,000 # Policy Term := 1 year" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Child 1,Child 2" are entered in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page 
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACS # Spouse := ACS # Child 1 := ACS # Child 2 := ACS" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Cheque" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Success" message is displayed in Agent Health success page 
	And verify the policy has moved to "Effective" state in Agent transactions page 
	And close the browser 
	
@2A4C
Scenario: To verify the standard scenario for 2A4C 
	Members : Proposer,Spouse,Child 1,Child 2,Child 3,Child 4
Zone : A
Package : Option 4
SI Type : Floater
Sum Insured : 5,00,000
Policy Term : 1 year

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 50 # Spouse := 45 # Child 1 := 15 # Child 2 := 12 # Child 3 := 8 # Child 4 := 4" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 4" for proposer in Agent Health plan selection page 
	And plan details are entered as "SI Type := Floater SI # Sum Insured := 5,00,000 # Policy Term := 1 year" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Child 1,Child 2,Child 3,Child 4" are entered in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page 
	And gynaecological problem "Spouse := No" is selected in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACS # Spouse := ACS # Child 1 := ACS # Child 2 := ACS # Child 3 := ACS # Child 4 := ACS" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Cheque" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Success" message is displayed in Agent Health success page 
	And verify the policy has moved to "Effective" state in Agent transactions page 
	And close the browser 

@4A
Scenario: To verify the standard scenario for 4A 
	Members : Proposer,Spouse,Father,Mother
Zone : B
Package : Option 5
SI Type : Individual
Sum Insured : 6,00,000
Policy Term : 1 year

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 30 # Spouse := 25 # Father := 50 # Mother := 45" in Agent Health age details page 
	And pincode is entered as "380001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 5" for proposer in Agent Health plan selection page 
	And select "Option 5" for in laws in Agent Health plan selection page 
	And plan details are entered as "SI Type := Individual SI # Sum Insured := 6,00,000 # Policy Term := 1 year" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And plan details are entered as "SI Type := Individual SI # Sum Insured := 6,00,000 # Policy Term := 1 year" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Father,Mother" are entered in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page 
	And gynaecological problem "Spouse := No # Mother := No" is selected in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACS # Spouse := ACS # Father := ACS # Mother := ACS" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Cheque" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Success" message is displayed in Agent Health success page 
	And verify the policy has moved to "Effective" state in Agent transactions page 
	And close the browser 
	
@3A 
Scenario: To verify the standard scenario for 3A 
	Members : Proposer,Spouse,Father,Mother
Zone : C
Package : Option 6
SI Type : Floater
Sum Insured : 7,00,000
Policy Term : 1 year

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 40 # Father In Law:= 60 # Mother In Law := 55" in Agent Health age details page 
	And pincode is entered as "160001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 6" for proposer in Agent Health plan selection page 
	And select "Option 6" for in laws in Agent Health plan selection page 
	And plan details are entered as "SI Type := Floater SI # Sum Insured := 7,00,000 # Policy Term := 1 year" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And plan details are entered as "SI Type := Floater SI # Sum Insured := 7,00,000 # Policy Term := 1 year" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Father In Law,Mother In Law" are entered in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page 
	And gynaecological problem "Mother In Law:= No" is selected in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACS # Father In Law := ACS # Mother In Law:= ACS" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Cheque" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Success" message is displayed in Agent Health success page 
	And verify the policy has moved to "Effective" state in Agent transactions page 
	And close the browser 
	
@4A4C
Scenario: To verify the standard scenario for 4A4C 
	Members : Proposer,Spouse,Child 1,Child 2,Child 3,Child 4,Father,Mother
Zone : A
Package : Option 7
SI Type : Individual
Sum Insured : 8,00,000
Policy Term : 1 year

	Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$" 
	When module "Health Insurance" is selected 
	And select "Create New Policy" option in Agent Health home page 
	And age details are entered as "Proposer := 30 # Spouse := 27 # Child 1:= 10 # Child 2 := 8 # Child 3 := 6 # Child 4:= 3 # Father := 60 # Mother := 55" in Agent Health age details page 
	And pincode is entered as "110001" in Agent Health age details page 
	And perform "View Options" action in Agent Health age details page 
	And select "Option 7" for proposer in Agent Health plan selection page 
	And select "Option 7" for in laws in Agent Health plan selection page 
	And plan details are entered as "SI Type := Individual SI # Sum Insured := 8,00,000 # Policy Term := 1 year" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And plan details are entered as "SI Type := Individual SI # Sum Insured := 8,00,000 # Policy Term := 1 year" in Agent Health plan details page 
	And perform "Recalculate" action in Agent Health plan details page 
	And perform "Proceed" action in Agent Health plan details page 
	And family details for "Self,Spouse,Child 1,Child 2,Child 3,Child 4,Father,Mother" are entered in Agent Health family details page 
	And perform "Next" action in Agent Health family details page 
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page 
	And perform "Next" action in Agent Health personal habits page 
	And medical questions "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" are entered in Agent Health medical questions page 
	And gynaecological problem "Spouse := No # Mother := No" is selected in Agent Health medical questions page 
	And perform "Next" action in Agent Health medical questions page 
	And verify uw status as "Self := ACS # Spouse := ACS # Child 1 := ACS # Child 2 := ACS # Child 3 := ACS # Child 4 := ACS # Father := ACS # Mother := ACS" in Agent Health summary page 
	And accept the declaration in Agent Health summary page 
	And payment info for "Cheque" are entered in Agent Health summary page 
	And proceed to the payment in Agent Health summary page 
	Then verify "Success" message is displayed in Agent Health success page 
	And verify the policy has moved to "Effective" state in Agent transactions page 
	And close the browser