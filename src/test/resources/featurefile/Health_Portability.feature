Feature: Sanity for Portability in Health Insurance

Background:
Given Logged-in into the application

	@Health_Portability_Sanity @Port_One @QuickSanity
	Scenario Outline: Portability for Proposer where PolicyExpiryDate:-Today/DigitDiamond/3lakh/1yr/Age<45/Ht:-180,Wt:-78/No PED/Online payment

	When Clicked on module "Health Insurance"
	And "Portability" button is clicked
	And IMD code "1000044" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And answer existing policy questions from provided portId "<portId>"
	And view options for "<portId>"
	And plan selected from planId "<planId>"
	And plan details selected from planId "<planId>"
	And increaseSI option is fetched from portId "<portId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self" are entered in Agent Health family details page 
	And details entered for individuals "Self" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And all existing policy details are filled from portId "<portId>"
	And perform "Next" action in Agent Health family details page
	And provide detail "Yes" in Port Policy popup from portId "<portId>"
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for portId "<portId>"
	And download policy PDF
	And accept Terms and Conditions for Portability "<portId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId | portId |
	|  1     |    1   |
	
	
	@Health_Portability_Sanity @Port_Two @QuickSanity
	Scenario Outline: Portability for Proposer where PolicyExpiryDate:-Tomorrow/DigitDiamond/3lakh/1yr/Age<45/Ht:-170,Wt:-85/No PED/Cheque payment

	When Clicked on module "Health Insurance"
	And "Portability" button is clicked
	And IMD code "1000495" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And answer existing policy questions from provided portId "<portId>"
	And view options for "<portId>"
	And plan selected from planId "<planId>"
	And plan details selected from planId "<planId>"
	And increaseSI option is fetched from portId "<portId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self" are entered in Agent Health family details page 
	And details entered for individuals "Self" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And all existing policy details are filled from portId "<portId>"
	And perform "Next" action in Agent Health family details page
	And provide detail "Yes" in Port Policy popup from portId "<portId>"
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for portId "<portId>"
	And download policy PDF
	And accept Terms and Conditions for Portability "<portId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId | portId |
	|    2   |    2   |
	
	
	@Health_Portability_Sanity @Port_Three @QuickSanity
	Scenario Outline: Portability for Proposer where PolicyExpiryDate:-Tomorrow/DigitDiamond/3lakh/1yr/Age<45/Ht:-180,Wt:-78/PED:-Diabetes/Cheque payment

	When Clicked on module "Health Insurance"
	And "Portability" button is clicked
	And IMD code "1000495" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And answer existing policy questions from provided portId "<portId>"
	And view options for "<portId>"
	And plan selected from planId "<planId>"
	And plan details selected from planId "<planId>"
	And increaseSI option is fetched from portId "<portId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self" are entered in Agent Health family details page 
	And details entered for individuals "Self" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And all existing policy details are filled from portId "<portId>"
	And perform "Next" action in Agent Health family details page
	And provide detail "Yes" in Port Policy popup from portId "<portId>"
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := Self # Pre Existing Conditions := Self # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And select "Yes" to answer further medical questions in Agent Health medical questions page
	And answer all further medical questions for "Self"
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for portId "<portId>"
	And download policy PDF
	And accept Terms and Conditions for Portability "<portId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId | portId |
	|  1502  |    3   |
	
	
	@Health_Portability_Sanity @Port_Four @QuickSanity
	Scenario Outline: Portability for Proposer where PolicyExpiryDate:-Tomorrow/DigitDiamond/3lakh/1yr/Age<45/Ht:-170,Wt:-85/No PED/Send payment

	When Clicked on module "Health Insurance"
	And "Portability" button is clicked
	And IMD code "1000495" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And answer existing policy questions from provided portId "<portId>"
	And view options for "<portId>"
	And plan selected from planId "<planId>"
	And plan details selected from planId "<planId>"
	And increaseSI option is fetched from portId "<portId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self" are entered in Agent Health family details page 
	And details entered for individuals "Self" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And all existing policy details are filled from portId "<portId>"
	And perform "Next" action in Agent Health family details page
	And provide detail "Yes" in Port Policy popup from portId "<portId>"
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for portId "<portId>"
	And download policy PDF
	And accept Terms and Conditions for Portability "<portId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId | portId |
	|  1503  |    4   |
	
	
	@Health_Portability_Sanity @Port_Five @QuickSanity
	Scenario Outline: Portability for Proposer where PolicyExpiryDate:-Tomorrow/DigitDiamond/15lakh/1yr/Age<45/Ht:-180,Wt:-78/PED:-Hypertension/Online payment

	When Clicked on module "Health Insurance"
	And "Portability" button is clicked
	And IMD code "1000495" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And answer existing policy questions from provided portId "<portId>"
	And view options for "<portId>"
	And plan selected from planId "<planId>"
	And plan details selected from planId "<planId>"
	And increaseSI option is fetched from portId "<portId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self" are entered in Agent Health family details page 
	And details entered for individuals "Self" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And all existing policy details are filled from portId "<portId>"
	And perform "Next" action in Agent Health family details page
	And provide detail "Yes" in Port Policy popup from portId "<portId>"
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := Self # Pre Existing Conditions := Self # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And select "Yes" to answer further medical questions in Agent Health medical questions page
	And answer all further medical questions for "Self"
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for portId "<portId>"
	And download policy PDF
	And accept Terms and Conditions for Portability "<portId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId | portId |
	|   181  |    5   |
		
	@Health_Portability_Sanity @Port_Six
	Scenario Outline: Portability for Proposer where PolicyExpiryDate:-(Tomorrow + 2)/DigitDiamond/9lakh/1yr/Age>45/Ht:-175,Wt:-85/No PED/Cheque payment

	When Clicked on module "Health Insurance"
	And "Portability" button is clicked
	And IMD code "1000495" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And answer existing policy questions from provided portId "<portId>"
	And view options for "<portId>"
	And plan selected from planId "<planId>"
	And plan details selected from planId "<planId>"
	And increaseSI option is fetched from portId "<portId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self" are entered in Agent Health family details page 
	And details entered for individuals "Self" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And all existing policy details are filled from portId "<portId>"
	And perform "Next" action in Agent Health family details page
	And provide detail "Yes" in Port Policy popup from portId "<portId>"
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for portId "<portId>"
	And download policy PDF
	And accept Terms and Conditions for Portability "<portId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId | portId |
	|  146   |    6   |
		
	@Health_Portability_Sanity @Port_Seven
	Scenario Outline: Portability for Proposer where PolicyExpiryDate:-(Tomorrow + 2)/DigitDiamond/9lakh/1yr/Age<45/Ht:-180,Wt:-78/PED:-Asthma/Online payment

	When Clicked on module "Health Insurance"
	And "Portability" button is clicked
	And IMD code "1000495" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And answer existing policy questions from provided portId "<portId>"
	And view options for "<portId>"
	And plan selected from planId "<planId>"
	And plan details selected from planId "<planId>"
	And increaseSI option is fetched from portId "<portId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self" are entered in Agent Health family details page 
	And details entered for individuals "Self" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And all existing policy details are filled from portId "<portId>"
	And perform "Next" action in Agent Health family details page
	And provide detail "Yes" in Port Policy popup from portId "<portId>"
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := Self # Pre Existing Conditions := Self # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And select "Yes" to answer further medical questions in Agent Health medical questions page
	And answer all further medical questions for "Self"
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for portId "<portId>"
	And download policy PDF
	And accept Terms and Conditions for Portability "<portId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId | portId |
	|  1504  |    7   |
	
		
	@Health_Portability_Sanity @Port_Eight
	Scenario Outline: Portability for Proposer where PolicyExpiryDate:-(Tomorrow + 2)/DigitDiamond/20lakh/1yr/Age>45/Ht:-170,Wt:-88/No PED/Online payment

	When Clicked on module "Health Insurance"
	And "Portability" button is clicked
	And IMD code "1000495" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And answer existing policy questions from provided portId "<portId>"
	And view options for "<portId>"
	And plan selected from planId "<planId>"
	And plan details selected from planId "<planId>"
	And increaseSI option is fetched from portId "<portId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self" are entered in Agent Health family details page 
	And details entered for individuals "Self" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And all existing policy details are filled from portId "<portId>"
	And perform "Next" action in Agent Health family details page
	And provide detail "Yes" in Port Policy popup from portId "<portId>"
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := None # Pre Existing Conditions := None # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for portId "<portId>"
	And download policy PDF
	And accept Terms and Conditions for Portability "<portId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId | portId |
	|  193   |    8   |
	
	
	@Health_Portability_Sanity @Port_Nine
	Scenario Outline: Portability for Proposer where PolicyExpiryDate:-(Tomorrow + 2)/DigitDiamond/20lakh/1yr/Age>45/Ht:-180,Wt:-78/PED:-Asthma/Online payment

	When Clicked on module "Health Insurance"
	And "Portability" button is clicked
	And IMD code "1000495" is provided
	And cover for "Proposer" from provided planId "<planId>"
	And Enter the pinCode "<planId>"
	And answer existing policy questions from provided portId "<portId>"
	And view options for "<portId>"
	And plan selected from planId "<planId>"
	And plan details selected from planId "<planId>"
	And increaseSI option is fetched from portId "<portId>"
	And perform "Recalculate" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And perform "Proceed" action in Agent Health plan details page
	And family details for "Self" are entered in Agent Health family details page 
	And details entered for individuals "Self" as "Height := FromDatabase # Weight := FromDatabase" from planId "<planId>"
	And perform "Next" action in Agent Health family details page
	And all existing policy details are filled from portId "<portId>"
	And perform "Next" action in Agent Health family details page
	And provide detail "Yes" in Port Policy popup from portId "<portId>"
	And personal habits "Tobacco := None # Alcohol := None" are selected in Agent Health personal habits page
	And perform "Next" action in Agent Health personal habits page
	And medic details from database "Undiagnosed Symptoms := Self # Pre Existing Conditions := Self # Ongoing Medications := None" from planId "<planId>" are entered in Agent Health medical questions page
	And select "Yes" to answer further medical questions in Agent Health medical questions page
	And answer all further medical questions for "Self"
	And click "Next" in Agent Health medical questions page
	And verify eligibility for policy creation
	And store the proposal number in database for portId "<portId>"
	And download policy PDF
	And accept Terms and Conditions for Portability "<portId>"
	And premium paid by "Proposer"
	And mode of payment and its info are entered in Agent Health summary page
	And proceed to the pay in Agent Health summary page
	And mock payment done
	Then verify success
	And close the browser
	
	Examples:
	| planId | portId |
	|   194  |    9   |