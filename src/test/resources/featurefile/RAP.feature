Feature: To verify the functionalities of RAP 

@RAP
Scenario: To change the pre inspection status 

	Given logged into ABS with details "UserName := 000071 # Password := digit123" 
	And select module "Contract" in ABS home page 
	And search with details "Policy Number := D300052275" in ABS Search page 
	And switch to "Questions" tab in ABS Main Page 
	And complete the Preinspection process