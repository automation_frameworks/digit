package com.cucumber.framework.runner;


import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.utilities.Library;
import com.utilities.ResultWriter;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = { "classpath:featurefile" }, 
				tags = { "@Agent_Sanity" }, glue = {
		"classpath:com.cucumber.framework.stepdefinition", "classpath:com.cucumber.framework.helper",
		"com.cucumber.framework.runner" }, plugin = { "pretty", "json:target/SearchFeatureRunner.json",
				"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html" }, monochrome = true,dryRun=false)

public class TestRunner2 extends AbstractTestNGCucumberTests {
	
	@BeforeClass
	public static void generateReportExcel() throws Throwable
	{
		System.out.println("Result sheet is created");
		//Library library = new Library();
		//library.DriverSetup();
		ResultWriter resultWriter = new ResultWriter();
		resultWriter.createResultSheet();
	}

//	@AfterClass
//	public void writeExtentReport() throws IOException, InterruptedException {
//		Thread.sleep(2000);
//		Date dNow = new Date();
//		SimpleDateFormat ft = new SimpleDateFormat("Eyyyy.MM.dd'_'hh:mm:ssa");
//		String timeStamp = ft.format(dNow).replaceAll(":", "_");
//		Reporter.loadXMLConfig(new File(PropertyFileReader.getReportConfigPath()));
//		String from = ResourceHelper.getBaseResourcePath().substring(1).replace("test-classes/", "")
//				+ "cucumber-reports/report.html";
//		String to = ResourceHelper.getBaseResourcePath().substring(1).replace("test-classes/", "")
//				+ "cucumber-reports/reports/SeleniumReport_"
//				+ timeStamp + ".html";
//		Path src = Paths.get(from);
//		Path dest = Paths.get(to);
//		Files.copy(src.toFile(), dest.toFile());
//}
	
	
//		@AfterClass
//		public static void generateReport() throws Throwable
//		{
//		CucumberResultsOverview results = new CucumberResultsOverview();
//		results.setOutputDirectory(".//target");
//		results.setOutputName(".//target//cucumber-results");
//		results.setSourceFile(".//target//cucumber.json");
//		results.executeFeaturesOverviewReport();
//		}
		
		
		@AfterClass
		public static void tearDown() throws Throwable
		{
			ResultWriter resultWriter = new ResultWriter();
			resultWriter.closeExcel();
			System.out.println("Report sheet is closed");
		}

}