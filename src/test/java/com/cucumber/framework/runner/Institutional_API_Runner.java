package com.cucumber.framework.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
		features = { "classpath:featurefile/Institutional_API.feature" }, 
		glue = {"classpath:com.cucumber.framework.stepdefinition",
				"classpath:com.cucumber.framework.helper"}, 
        plugin = { "pretty" },
//		tags = "@MobileQuoteAndIssue", 
        monochrome = true
        ,        dryRun = true
        )
public class Institutional_API_Runner extends AbstractTestNGCucumberTests {

}
