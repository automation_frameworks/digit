package com.cucumber.framework.runner;

import java.util.LinkedHashMap;

import org.openqa.selenium.NoSuchSessionException;
import org.openqa.selenium.NoSuchWindowException;
import org.testng.SkipException;

import com.utilities.Library;
import com.utilities.PropertyFileReader;
import com.utilities.ResultWriter;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
	public static Scenario scenario;
	static int failureCount = 0;
	public static boolean skipped;


//	@BeforeTest
//	public void beforeTest()
//	{
//		System.out.println();
//		ResultWriter resultWriter = new ResultWriter();
//		resultWriter.createResultSheet();
//	}

		
	
	@Before
	public void before(Scenario s) {
		System.out.println("Screenshot starts 123");
		System.out.println(s.getName());
		Library.staticMap = new LinkedHashMap<>();
		Library.staticMap.put("Scenario", s.getName());
		scenario = s;
		PropertyFileReader prop = new PropertyFileReader();
		if (!prop.getFailThreshold().equals("null"))
			if (failureCount >= Integer.parseInt(prop.getFailThreshold().trim())) {
				skipped = true;
				throw new SkipException("Test is skipped as there are [" + failureCount + "] continuous failures");
			}
	}

	@After
	public void after(Scenario s) {
//		Library library = new Library();
//		if(scenario.isFailed())
//		{
//		library.getDriver().close();
//		library.setDriverNull();
//		}
		Library library = new Library();
		PropertyFileReader prop = new PropertyFileReader();
		if ((!System.getProperty("os.name").contains("Windows") || prop.getBrowserClose().equals("true"))&& null != library.getDriver()) {
			try {
				System.out.println("Closing the browser");
			library.getDriver().close();
			}
			catch(NoSuchSessionException se)
			{
				
			}
			catch(NoSuchWindowException we)
			{
				
			}
		}
		ResultWriter resultWriter = new ResultWriter();
		resultWriter.writeTestResults(s,skipped);
		if(!skipped)
		if (scenario.isFailed()) {
			failureCount++;
		} else
			failureCount = 0;
	}
	
	
	

	public Scenario getScenario() {
		return scenario;
	}

}