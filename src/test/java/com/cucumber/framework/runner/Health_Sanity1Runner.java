package com.cucumber.framework.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
		features = { "classpath:featurefile/Health_Sanity1.feature" }, 
		glue = {"classpath:com.cucumber.framework.stepdefinition",
				"classpath:com.cucumber.framework.helper"}, 
        plugin = { "pretty", "html:target/Health_Sanity1_Report", "rerun:target/rerun.txt" },
		tags = "@HealthInsurance_Sanity", 
        monochrome = true,
        dryRun = false)
public class Health_Sanity1Runner extends AbstractTestNGCucumberTests {

}
