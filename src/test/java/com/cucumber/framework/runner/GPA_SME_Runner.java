package com.cucumber.framework.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
		features = { "classpath:featurefile/GPA_SME.feature" }, 
		glue = {"classpath:com.cucumber.framework.stepdefinition",
				"classpath:com.cucumber.framework.helper"}, 
        plugin = { "pretty", "html:target/GPA_SME_Report", "rerun:target/rerun.txt" },
		tags = "@GPA_SME_One", 
        monochrome = true
//        ,        dryRun = true
        )
public class GPA_SME_Runner extends AbstractTestNGCucumberTests {

}
