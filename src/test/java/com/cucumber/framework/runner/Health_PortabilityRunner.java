package com.cucumber.framework.runner;

import org.junit.runner.RunWith;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@RunWith(ExtendedCucumber.class)

@ExtendedCucumberOptions(jsonReport = "target/cucumber.json",
							retryCount = 2,
							detailedReport = true,
							detailedAggregatedReport = true,
							overviewReport = true,
							coverageReport = true,
							jsonUsageReport = "target/cucumbeer-usage.json",
							usageReport = true,
							toPDF = true,
							includeCoverageTags = {"@Port_Five"},
							outputFolder = "target")

@CucumberOptions(
		features = { "classpath:featurefile/Health_Portability.feature" }, 
		glue = {"classpath:com.cucumber.framework.stepdefinition",
				"classpath:com.cucumber.framework.helper"}, 
        plugin = { "pretty", "html:target/Health_Portability_Report", "rerun:target/rerun.txt" },
//		tags = "@Port_Three",
        monochrome = true,
        dryRun = false)
public class Health_PortabilityRunner extends AbstractTestNGCucumberTests {

//	 @Test(retryAnalyzer = MyRetry.class)
//	  public void test2() {
//	    Assert.fail();
//	  }


}
