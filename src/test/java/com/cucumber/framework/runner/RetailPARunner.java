package com.cucumber.framework.runner;

import org.junit.runner.RunWith;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@RunWith(ExtendedCucumber.class)

@ExtendedCucumberOptions(jsonReport = "target/cucumber.json",
							retryCount = 2,
							detailedReport = true,
							detailedAggregatedReport = true,
							overviewReport = true,
							coverageReport = true,
							jsonUsageReport = "target/cucumbeer-usage.json",
							usageReport = true,
							toPDF = true,
							includeCoverageTags = {"@chrome"},
							outputFolder = "target")

@CucumberOptions(
		features = { "classpath:featurefile/RetailPA_Sanity.feature" }, 
		glue = {"classpath:com.cucumber.framework.stepdefinition",
				"classpath:com.cucumber.framework.helper"}, 
        plugin = { "pretty", "html:target/RetailPA_Sanity_Report", "rerun:target/rerun.txt" },
//		tags = {"@RPA_Two"},
        monochrome = true,
        dryRun = false)
public class RetailPARunner extends AbstractTestNGCucumberTests {

}
