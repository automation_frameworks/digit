package com.cucumber.framework.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
		features = { "classpath:featurefile/GMC.feature" }, 
		glue = {"classpath:com.cucumber.framework.stepdefinition",
				"classpath:com.cucumber.framework.helper"}, 
        plugin = { "pretty", "html:target/GMC_Report", "rerun:target/rerun.txt" },
//		tags = "@GMC_One", 
        monochrome = true
//        ,        dryRun = true
        )
public class GMC_Runner extends AbstractTestNGCucumberTests {

}
