package com.cucumber.framework.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
		features = { "classpath:featurefile/Health_DigitPolicyDB.feature" }, 
		glue = {"classpath:com.cucumber.framework.stepdefinition",
				"classpath:com.cucumber.framework.helper"}, 
        plugin = { "pretty", "html:target/Health_DigitPolicyDB_Report" },
		/* tags = {"@First"}, */ 
        monochrome = true,
        dryRun = false)
public class Health_DigitPolicyDBRunner extends AbstractTestNGCucumberTests {

}
