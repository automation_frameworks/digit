package com.cucumber.framework.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
		features = { "@target/rerun.txt" }, 
		glue = {"classpath:com.cucumber.framework.stepdefinition",
				"classpath:com.cucumber.framework.helper"}, 
        plugin = { "pretty", "html:target/Health_Portability_Report" },
        monochrome = true,
        dryRun = false)
public class FailedScenarios extends AbstractTestNGCucumberTests {

}
