package com.cucumber.framework.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
		features = { "classpath:featurefile/MotorAPI.feature" }, 
		glue = {"classpath:com.cucumber.framework.stepdefinition",
				"classpath:com.cucumber.framework.helper"}, 
        plugin = { "pretty", "html:target/MotorAPI_Report"},
		tags = "@AgentFloat_1YrOD_1YrTP_E2WC", 
        monochrome = true,
        dryRun = false)
public class MotorAPI_Runner extends AbstractTestNGCucumberTests {

}
