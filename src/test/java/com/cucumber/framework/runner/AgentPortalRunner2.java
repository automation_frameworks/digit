/**
 * @author rahul.rathore
 *	
 *	14-Aug-2016
 */
package com.cucumber.framework.runner;

import java.io.File;

import org.junit.AfterClass;

import com.cucumber.listener.Reporter;
import com.utilities.PropertyFileReader;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = { "classpath:featurefile" }, 
				 tags = { "@DigitCare" }, 
				 glue = {"classpath:com.cucumber.framework.stepdefinition", "classpath:com.cucumber.framework.helper" }, 
				 plugin = {"pretty", "json:target/SearchFeatureRunner.json","com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html" },
					monochrome = true)
public class AgentPortalRunner2 extends AbstractTestNGCucumberTests {

	@AfterClass
	public void writeExtentReport() {
		Reporter.loadXMLConfig(new File(PropertyFileReader.getReportConfigPath()));
	}
}
