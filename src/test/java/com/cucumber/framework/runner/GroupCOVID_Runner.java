package com.cucumber.framework.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
		features = { "classpath:featurefile/GroupCOVID.feature" }
		,glue = {"classpath:com.cucumber.framework.stepdefinition",
				"classpath:com.cucumber.framework.helper"}
        ,plugin = { "pretty", "html:target/GroupCOVID_Report", "rerun:target/rerun.txt" }
//		,tags = "@GMC_Seven"
        ,monochrome = true
        ,dryRun = true
        )
public class GroupCOVID_Runner extends AbstractTestNGCucumberTests {

}
