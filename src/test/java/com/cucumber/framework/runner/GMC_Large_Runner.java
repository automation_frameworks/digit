package com.cucumber.framework.runner;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.utilities.ResultWriter;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
		features = { "classpath:featurefile/GMC_Large.feature" }, 
		glue = {"classpath:com.cucumber.framework.stepdefinition",
				"classpath:com.cucumber.framework.helper"}, 
        plugin = { "pretty", "html:target/GMC_Large_Report", "rerun:target/rerun.txt" },
		tags = "@GMC_One", 
        monochrome = true
//        ,        dryRun = true
        )
public class GMC_Large_Runner extends AbstractTestNGCucumberTests {

	@BeforeClass
	public static void generateReportExcel() throws Throwable {
		System.out.println("Result sheet is created");
		ResultWriter resultWriter = new ResultWriter();
		resultWriter.createResultSheet();
	}
	
	@AfterClass
	public static void tearDown() throws Throwable {
		ResultWriter resultWriter = new ResultWriter();
		resultWriter.writeTestResults(Hooks.scenario, Hooks.skipped);
		resultWriter.closeExcel();
		System.out.println("Report sheet is closed");
	}
}
