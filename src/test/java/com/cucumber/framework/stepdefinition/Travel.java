package com.cucumber.framework.stepdefinition;

import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.cucumber.framework.runner.Hooks;

import cucumber.api.java.en.When;
import pom.agent.travel.Details;

public class Travel {

	private static com.utilities.Library library = new com.utilities.Library();

	@When("travel plan details \"([^\"]*)\" are entered")
	public void enterTravelPlanDetails(String details) throws IOException {
		try {
			WebDriver driver = library.getDriver();
			Details detailsobj = PageFactory.initElements(driver, Details.class);
			detailsobj.enterTravelDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("traveller details \"([^\"]*)\" are entered")
	public void enterTravellerDetails(String details) throws IOException {
		try {
			WebDriver driver = library.getDriver();
			Details detailsobj = PageFactory.initElements(driver, Details.class);
			detailsobj.enterTravellerDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("date of birth for traveller \"([^\"]*)\" is entered as \"([^\"]*)\"")
	public void enterDOBDetails(String traveller,String dob) throws IOException {
		try {
			WebDriver driver = library.getDriver();
			Details detailsobj = PageFactory.initElements(driver, Details.class);
			detailsobj.enterDOB(traveller, dob);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("personal info for traveller \"([^\"]*)\" is entered as \"([^\"]*)\"")
	public void enterTravellerDetails(String traveller,String details) throws IOException {
		try {
			WebDriver driver = library.getDriver();
			Details detailsobj = PageFactory.initElements(driver, Details.class);
			detailsobj.enterTravellerInfo(traveller, details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("action \"([^\"]*)\" is clicked in travel")
	public void performAction(String action) throws IOException {
		try {
			WebDriver driver = library.getDriver();
			Details detailsobj = PageFactory.initElements(driver, Details.class);
			detailsobj.performAction(action);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("perform the Online Payment")
	public void performOnlinePayment() throws IOException {
		try {
			WebDriver driver = library.getDriver();
			Details detailsobj = PageFactory.initElements(driver, Details.class);
			detailsobj.performOnlinePayment();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	
	@When("travel plan with \"([^\"]*)\" is selected")
	public void selectTravelPlan(String details) throws IOException {
		try {
			WebDriver driver = library.getDriver();
			Details detailsobj = PageFactory.initElements(driver, Details.class);
			detailsobj.selectPlan(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("verify error message \"([^\"]*)\" is displayed")
	public void verifyErrorMessage(String message) throws IOException {
		try {
			WebDriver driver = library.getDriver();
			Details detailsobj = PageFactory.initElements(driver, Details.class);
			detailsobj.verifyErrorMessage(message);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	
	
	@When("accept the declaration in travel")
	public void acceptDeclaration() throws IOException {
		try {
			WebDriver driver = library.getDriver();
			Details detailsobj = PageFactory.initElements(driver, Details.class);
			detailsobj.acceptDeclaration();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("payment details are entered for \"([^\"]*)\"")
	public void enterPaymentDetails(String paymentType) throws IOException {
		try {
			WebDriver driver = library.getDriver();
			Details detailsobj = PageFactory.initElements(driver, Details.class);
			detailsobj.enterPaymentDetails(paymentType);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("verify policy issuance is successful for Travel")
	public void verifySuccessMessage() throws IOException {
		try {
			WebDriver driver = library.getDriver();
			Details detailsobj = PageFactory.initElements(driver, Details.class);
			detailsobj.verifySuccessPage();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
}
