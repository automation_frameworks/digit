package com.cucumber.framework.stepdefinition;

import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.cucumber.framework.runner.Hooks;
import com.utilities.Library;

import cucumber.api.java.en.When;
import pom.agent.health.AgeDetails;
import pom.agent.health.CreateCustomPlan;
import pom.agent.health.HealthHome;
import pom.agent.health.MedicalQuestions;
import pom.agent.health.PersonalDetails;
import pom.agent.health.PersonalHabits;
import pom.agent.health.PlanDetails;
import pom.agent.health.PlanSelection;
import pom.agent.health.Success;
import pom.agent.health.Summary;

public class Health {
	private static com.utilities.Library library = new com.utilities.Library();

	@When("select \"([^\"]*)\" option in Agent Health home page")
	public void selectModule(String option) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			HealthHome homeobj = PageFactory.initElements(driver, HealthHome.class);
			homeobj.selectOption(option);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("select \"([^\"]*)\" option in Agent Health Option Created page")
	public void selectPackage(String packageName) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			CreateCustomPlan homeobj = PageFactory.initElements(driver, CreateCustomPlan.class);
			homeobj.selectPackage(packageName);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	} 


	@When("create a custom plan with all coverages in Agent Health Options page")
	public void createCustomPlan() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			CreateCustomPlan homeobj = PageFactory.initElements(driver, CreateCustomPlan.class);
			homeobj.createCustomPlan();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	} 

	@When("select the custom plan created in Agent Health Options page")
	public void selectCreatedCustomPlan() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			CreateCustomPlan homeobj = PageFactory.initElements(driver, CreateCustomPlan.class);
			homeobj.selectCreatedCustomPlan();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	} 

	@When("age details are entered as \"([^\"]*)\" in Agent Health age details page")
	public void enterAgeDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			AgeDetails homeobj = PageFactory.initElements(driver, AgeDetails.class);
			homeobj.enterAgeDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}


	@When("unselect the proposer in Agent Health age details page")
	public void unselectProposer() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			AgeDetails homeobj = PageFactory.initElements(driver, AgeDetails.class);
			homeobj.unSelectProposer();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("pincode is entered as \"([^\"]*)\" in Agent Health age details page")
	public void enterPinCode(String pinCode) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			AgeDetails homeobj = PageFactory.initElements(driver, AgeDetails.class);
			homeobj.enterPinCode(pinCode);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("perform \"([^\"]*)\" action in Agent Health age details page")
	public void performActioninAge(String action) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			AgeDetails homeobj = PageFactory.initElements(driver, AgeDetails.class);
			homeobj.performAction(action);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("select \"([^\"]*)\" for proposer in Agent Health plan selection page")
	public void selectProposerPlan(String option) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PlanSelection homeobj = PageFactory.initElements(driver, PlanSelection.class);
			homeobj.selectProposerOption(option);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("select \"([^\"]*)\" for in laws in Agent Health plan selection page")
	public void selectInLawsPlan(String option) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PlanSelection homeobj = PageFactory.initElements(driver, PlanSelection.class);
			homeobj.selectinLawsOption(option);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("plan details are entered as \"([^\"]*)\" in Agent Health plan details page")
	public void enterPlanDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PlanDetails homeobj = PageFactory.initElements(driver, PlanDetails.class);
			Thread.sleep(3000);
			homeobj.enterPlanDetails(details,"");
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}


	@When("select all coverages in Agent Health plan details page")
	public void selectAllCoverages() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PlanDetails homeobj = PageFactory.initElements(driver, PlanDetails.class);
			homeobj.selectAllCoverages();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("maternity benefit details \"([^\"]*)\" are entered in Agent Health plan details page")
	public void enterMaternityBenefitDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PlanDetails homeobj = PageFactory.initElements(driver, PlanDetails.class);
			homeobj.enterMaternityBenefitDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("daily hospital cash cover details \"([^\"]*)\" are entered in Agent Health plan details page")
	public void enterDailyHospitalCashCover(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PlanDetails homeobj = PageFactory.initElements(driver, PlanDetails.class);
			homeobj.enterDailyHospitalCashCoverDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("out patient benefit details \"([^\"]*)\" are entered in Agent Health plan details page")
	public void enterOutPatientBenefit(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PlanDetails homeobj = PageFactory.initElements(driver, PlanDetails.class);
			homeobj.enterOutPatientBenefitDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("co payment details \"([^\"]*)\" are entered in Agent Health plan details page")
	public void enterCoPaymentDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PlanDetails homeobj = PageFactory.initElements(driver, PlanDetails.class);
			homeobj.enterCoPaymentDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("perform \"([^\"]*)\" action in Agent Health plan details page")
	public void performActionInPlanDetail(String action) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PlanDetails homeobj = PageFactory.initElements(driver, PlanDetails.class);
			homeobj.performAction(action);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("family details for \"([^\"]*)\" are entered in Agent Health family details page")
	public void enterFamilyDetails(String familyMembers) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PersonalDetails homeobj = PageFactory.initElements(driver, PersonalDetails.class);
			homeobj.enterFamilyDetails(familyMembers);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("family details for \"([^\"]*)\" is entered as \"([^\"]*)\" in Agent Health family details page")
	public void enterSpecificFamilyDetails(String familyMember, String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PersonalDetails homeobj = PageFactory.initElements(driver, PersonalDetails.class);
			homeobj.enterSpecificFamilyDetails(familyMember, details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("perform \"([^\"]*)\" action in Agent Health family details page")
	public void performActionInFamilyDetails(String action) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PersonalDetails homeobj = PageFactory.initElements(driver, PersonalDetails.class);
			homeobj.performAction(action);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("personal habits \"([^\"]*)\" are selected in Agent Health personal habits page")
	public void enterPersonalHabits(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PersonalHabits homeobj = PageFactory.initElements(driver, PersonalHabits.class);
			homeobj.selectTobaccoAlcoholConsumingPersons(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("validate alcohol habits are not shown for \"([^\"]*)\" in Agent Health personal habits page")
	public void validateAlcoholHabitsNotShown(String members) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PersonalHabits homeobj = PageFactory.initElements(driver, PersonalHabits.class);
			homeobj.checkTobaccoConsideringPersons("alcohol", members, false);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("validate alcohol habits are shown for \"([^\"]*)\" in Agent Health personal habits page")
	public void validateAlcoholHabitsShown(String members) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PersonalHabits homeobj = PageFactory.initElements(driver, PersonalHabits.class);
			homeobj.checkTobaccoConsideringPersons("alcohol", members, true);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("tobacco consumption forms \"([^\"]*)\" are selected in Agent Health personal habits page")
	public void tobaccoConsumptionForms(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PersonalHabits homeobj = PageFactory.initElements(driver, PersonalHabits.class);
			homeobj.enterTobaccoFormDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("alcohol consumption forms \"([^\"]*)\" are selected in Agent Health personal habits page")
	public void alcoholConsumptionForms(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PersonalHabits homeobj = PageFactory.initElements(driver, PersonalHabits.class);
			homeobj.enterAlcoholFormDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("perform \"([^\"]*)\" action in Agent Health personal habits page")
	public void performActionInPersonalHabits(String action) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			PersonalHabits homeobj = PageFactory.initElements(driver, PersonalHabits.class);
			homeobj.performAction(action);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("medical questions \"([^\"]*)\" are entered in Agent Health medical questions page")
	public void enterMedicalQueestion(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MedicalQuestions medobj = PageFactory.initElements(driver, MedicalQuestions.class);
			medobj.selectMedicalQuestions(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("pre existing diseases \"([^\"]*)\" are entered in Agent Health medical questions page")
	public void enterPreexistingDiseases(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MedicalQuestions homeobj = PageFactory.initElements(driver, MedicalQuestions.class);
			homeobj.selectMedicalConditions(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("gynaecological problem \"([^\"]*)\" is selected in Agent Health medical questions page")
	public void enterGynaecologicalProblems(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MedicalQuestions homeobj = PageFactory.initElements(driver, MedicalQuestions.class);
			homeobj.selectGynaecologicalProblems(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("select \"([^\"]*)\" to answer further medical questions in Agent Health medical questions page")
	public void selectFurtherMedicalQuestions(String selection) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MedicalQuestions homeobj = PageFactory.initElements(driver, MedicalQuestions.class);
			homeobj.answerFurtherMedicalQuestions(selection);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("thyroid details for \"([^\"]*)\" is entered as \"([^\"]*)\" in Agent Health medical questions page")
	public void answerThyroidQuestions(String familyMember, String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MedicalQuestions homeobj = PageFactory.initElements(driver, MedicalQuestions.class);
			homeobj.answerFurtherMedicalQuestions("Thyroid", familyMember, details);
		} catch (Exception e) {
			try {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
			}catch(NullPointerException n) {

			}
		}
	}

	@When("diabetes details for \"([^\"]*)\" is entered as \"([^\"]*)\" in Agent Health medical questions page")
	public void answerDiabetesQuestions(String familyMember, String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MedicalQuestions homeobj = PageFactory.initElements(driver, MedicalQuestions.class);
			homeobj.answerFurtherMedicalQuestions("Diabetes", familyMember, details);
		} catch (Exception e) {
			try {
				final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
				Hooks.scenario.embed(screenshot, "image/png");
				Assert.assertTrue(false, e.getMessage());
			}catch(NullPointerException n) {

			}
		}
	}

	@When("asthma details for \"([^\"]*)\" is entered as \"([^\"]*)\" in Agent Health medical questions page")
	public void answerAsthmaQuestions(String familyMember, String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MedicalQuestions homeobj = PageFactory.initElements(driver, MedicalQuestions.class);
			homeobj.answerFurtherMedicalQuestions("Asthma", familyMember, details);
		} catch (Exception e) {
			
				final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
				Hooks.scenario.embed(screenshot, "image/png");
				Assert.assertTrue(false, e.getMessage());
			
		}
	}

	@When("hyperlipidemia details for \"([^\"]*)\" is entered as \"([^\"]*)\" in Agent Health medical questions page")
	public void answerHyperlipidemiaQuestions(String familyMember, String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MedicalQuestions homeobj = PageFactory.initElements(driver, MedicalQuestions.class);
			homeobj.answerFurtherMedicalQuestions("Hyperlipidemia", familyMember, details);
		} catch (Exception e) {
//			try {
				final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
				Hooks.scenario.embed(screenshot, "image/png");
				Assert.assertTrue(false, e.getMessage());
//			}catch(NullPointerException n) {
//
//			}
		}
	}

	@When("perform \"([^\"]*)\" action in Agent Health medical questions page")
	public void performActionInMedicalQuestions(String action) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MedicalQuestions homeobj = PageFactory.initElements(driver, MedicalQuestions.class);
			homeobj.performAction(action);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("verify uw status as \"([^\"]*)\" in Agent Health summary page")
	public void verifyUWStatus(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Summary homeobj = PageFactory.initElements(driver, Summary.class);
			homeobj.verifyUWStatus(details);
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Library.softassert.assertTrue(false, e.getMessage());
			Hooks.scenario.write(e.getMessage());
			Library.softassert.assertAll();
		}
	}

	@When("accept the declaration in Agent Health summary page")
	public void acceptDeclaration() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			library.waitForDigitLoad();
			WebDriver driver = library.getDriver();
			Summary homeobj = PageFactory.initElements(driver, Summary.class);
			homeobj.acceptDeclaration();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("payment info for \"([^\"]*)\" are entered in Agent Health summary page")
	public void enterPaymentDetails(String paymentMethod) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Summary homeobj = PageFactory.initElements(driver, Summary.class);
			homeobj.selectPaymentMethods(paymentMethod);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("proceed to the payment in Agent Health summary page")
	public void proceedToPayment() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Summary homeobj = PageFactory.initElements(driver, Summary.class);
			homeobj.proceedToPayment();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("verify \"([^\"]*)\" message is displayed in Agent Health success page")
	public void validateSuccessPage(String expectedMessage) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Success homeobj = PageFactory.initElements(driver, Success.class);
			homeobj.verifySuccessMessage(expectedMessage);
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
		} catch (Exception e) {
			//Library.softassert.assertAll();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

}
