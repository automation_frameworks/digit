package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.cucumber.framework.runner.Hooks;

import cucumber.api.java.en.When;
import pom.agent.gmc.CreateQuote;

public class GMC_SME {
	
	private static com.utilities.Library library = new com.utilities.Library();

	@When("^provide insured details for GMC SME from \"([^\"]*)\"$")
	public void provide_insured_details_for_GMC_SME_from(String gmc_sme_id) throws Throwable {
	    
		try {
			CreateQuote cq =  PageFactory.initElements(library.getDriver(), CreateQuote.class);
			cq.fillInsuredDetailsGMCSME(gmc_sme_id);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^provide prospective policy details for GMC SME from \"([^\"]*)\"$")
	public void provide_prospective_policy_details_for_GMC_SME_from(String gmc_sme_id) throws Throwable {

		try {
			CreateQuote cq =  PageFactory.initElements(library.getDriver(), CreateQuote.class);
			cq.fillProspectiveDetailsGMCSME(gmc_sme_id);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^provide coverage details for GMC SME from \"([^\"]*)\"$")
	public void provide_coverage_details_for_GMC_SME_from(String gmc_sme_id) throws Throwable {

		try {
			CreateQuote cq =  PageFactory.initElements(library.getDriver(), CreateQuote.class);
			cq.fillCoverageDetailsGMCSME(gmc_sme_id);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^provide premium details for GMC SME from \"([^\"]*)\"$")
	public void provide_premium_details_for_GMC_SME_from(String gmc_sme_id) throws Throwable {

		try {
			CreateQuote cq =  PageFactory.initElements(library.getDriver(), CreateQuote.class);
			cq.fillPremiumDetailsGMCSME(gmc_sme_id);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
