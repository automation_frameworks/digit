package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.cucumber.framework.runner.Hooks;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import pom.agent.groupcovid.CovidMasterPolicyCreation;

public class GroupCOVID {

	private static com.utilities.Library library = new com.utilities.Library();
	
	@Given("^Login into the application for GroupCOVID$")
	public void login_into_the_application_for_GroupCOVID() throws Throwable {
	    
	}

	@When("^create new policy for GroupCOVID$")
	public void create_new_policy_for_GroupCOVID() throws Throwable {
		try {
			CovidMasterPolicyCreation obj =  PageFactory.initElements(library.getDriver(), CovidMasterPolicyCreation.class);
			obj.getNewPolicy();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^provide company details from \"([^\"]*)\"$")
	public void provide_company_details_from(String covidId) throws Throwable {
		try {
			CovidMasterPolicyCreation obj =  PageFactory.initElements(library.getDriver(), CovidMasterPolicyCreation.class);
			obj.fillCompanyDetails(covidId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^provide policy details from \"([^\"]*)\"$")
	public void provide_policy_details_from(String covidId) throws Throwable {
		try {
			CovidMasterPolicyCreation obj =  PageFactory.initElements(library.getDriver(), CovidMasterPolicyCreation.class);
			obj.fillPolicyDetails(covidId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^provide special conditions from \"([^\"]*)\"$")
	public void provide_special_conditions_from(String covidId) throws Throwable {
		try {
			CovidMasterPolicyCreation obj =  PageFactory.initElements(library.getDriver(), CovidMasterPolicyCreation.class);
			obj.fillSpecialConditions(covidId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^provide communication details from \"([^\"]*)\"$")
	public void provide_communication_details_from(String covidId) throws Throwable {
		try {
			CovidMasterPolicyCreation obj =  PageFactory.initElements(library.getDriver(), CovidMasterPolicyCreation.class);
			obj.fillCommunicationDetails(covidId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
