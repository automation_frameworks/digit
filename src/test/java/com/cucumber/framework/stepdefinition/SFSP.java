package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.cucumber.framework.runner.Hooks;

import cucumber.api.java.en.And;
import pom.agent.sfsp.SFSP_Home;

public class SFSP {

	@And("^policy details \"([^\"]*)\" are entered in Agent SFSP Insurance page$")
	public void selectPolicyDetail(String details) {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			SFSP_Home homeObj = PageFactory.initElements(driver, SFSP_Home.class);
			homeObj.selectPolicyDetails(details);
		} catch (Exception e) {
			com.utilities.Library library = new com.utilities.Library();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@And("^building details \"([^\"]*)\" are entered in Agent SFSP Insurance page$")
	public void enterBuildingDetails(String details) {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			SFSP_Home homeObj = PageFactory.initElements(driver, SFSP_Home.class);
			homeObj.enterBuildingDetails(details);
		} catch (Exception e) {
			com.utilities.Library library = new com.utilities.Library();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@And("^base coverage details \"([^\"]*)\" are entered in Agent SFSP Insurance page$")
	public void enterBaseCoverages(String details) {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			SFSP_Home homeObj = PageFactory.initElements(driver, SFSP_Home.class);
			homeObj.enterBaseCoverageDetails(details);
		} catch (Exception e) {
			com.utilities.Library library = new com.utilities.Library();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@And("^previous insurer details \"([^\"]*)\" are entered in Agent SFSP Insurance page$")
	public void enterPreviousInsurerDetails(String details) {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			SFSP_Home homeObj = PageFactory.initElements(driver, SFSP_Home.class);
			homeObj.enterPreviousInsurerDetails(details);
		} catch (Exception e) {
			com.utilities.Library library = new com.utilities.Library();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@And("^claim details \"([^\"]*)\" for claim \"([^\"]*)\" are entered in Agent SFSP Insurance page$")
	public void enterClaimDetails(String details, String claimNumber) {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			SFSP_Home homeObj = PageFactory.initElements(driver, SFSP_Home.class);
			homeObj.enterClaimDetails(details, claimNumber);
		} catch (Exception e) {
			com.utilities.Library library = new com.utilities.Library();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@And("^insurer details \"([^\"]*)\" are entered in Agent SFSP Insurance page$")
	public void enterInsurerDetails(String details) {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			SFSP_Home homeObj = PageFactory.initElements(driver, SFSP_Home.class);
			homeObj.enterCustomerDetails(details);
		} catch (Exception e) {
			com.utilities.Library library = new com.utilities.Library();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@And("^action \"([^\"]*)\" is performed in SFSP Insurance page$")
	public void performAction(String action) {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			SFSP_Home homeObj = PageFactory.initElements(driver, SFSP_Home.class);
			homeObj.performAction(action);
		} catch (Exception e) {
			com.utilities.Library library = new com.utilities.Library();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@And("^payment details \"([^\"]*)\" are entered in Agent SFSP Insurance page$")
	public void enterPaymentDetails(String paymentMode) {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			SFSP_Home homeObj = PageFactory.initElements(driver, SFSP_Home.class);
			homeObj.enterPaymentDetails(paymentMode);
		} catch (Exception e) {
			com.utilities.Library library = new com.utilities.Library();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@And("^verify the success page for SFSP Insurance$")
	public void verifySuccessPage() {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			SFSP_Home homeObj = PageFactory.initElements(driver, SFSP_Home.class);
			homeObj.verifySuccessPage();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
		} catch (Exception e) {
			com.utilities.Library library = new com.utilities.Library();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
