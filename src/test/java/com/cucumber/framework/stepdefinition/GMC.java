package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.cucumber.framework.runner.Hooks;
import com.utilities.Library;
import com.utilities.PropertyFileReader;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import pom.agent.gmc.Home;
import pom.agent.gmc.Login;

public class GMC {

	private static com.utilities.Library library = new com.utilities.Library();
	WebDriver  driver;

	@Given("^Login into the application for GMC_Ops$")
	public void login_into_the_application_for_GMC_Ops() throws Throwable {

		try {
			PropertyFileReader read = new PropertyFileReader();

			driver = library.DriverSetup();
			//Set Environment
			library.setExecutionEnvironment(read.getEnvironment_For_GMC());
			driver.get(Library.staticMap.get("server") + "/#/login");

			Login login = PageFactory.initElements(driver, Login.class);
			login.login();

		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^created new customer for GMC_Ops from \"([^\"]*)\"$")
	public void created_new_customer_for_GMC_Ops_from(String gmc_Ops_NewCustomerId) throws Throwable {

		try {
			Home home = PageFactory.initElements(driver, pom.agent.gmc.Home.class);
			home.createNewCustomer(gmc_Ops_NewCustomerId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^created new quote for GMC_Ops from \"([^\"]*)\"$")
	public void created_new_quote_for_GMC_Ops_from(String gmc_Ops_NewQuoteId) throws Throwable {

		try {
			Home home = PageFactory.initElements(driver, pom.agent.gmc.Home.class);
			home.createNewQuote(gmc_Ops_NewQuoteId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("^opened rap and created master policy for GMC_Ops from \"([^\"]*)\"$")
	public void opened_rap_and_created_master_policy_for_GMC_Ops_from(String gmc_Ops_RAP_Id) throws Throwable {
		
		try {
			Home home = PageFactory.initElements(driver, pom.agent.gmc.Home.class);
			home.createMasterPolicyinRAP(gmc_Ops_RAP_Id);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^mapped master policy for GMC_Ops from \"([^\"]*)\"$")
	public void mapped_master_policy_for_GMC_Ops_from(String id) throws Throwable {

		try {
			Home home = PageFactory.initElements(driver, pom.agent.gmc.Home.class);
			home.mapMasterPolicy(id);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^upload tarrif document for GMC_Ops$")
	public void upload_tarrif_document_for_GMC_Ops() throws Throwable {

		try {
			Home home = PageFactory.initElements(driver, pom.agent.gmc.Home.class);
			home.uploadTarrifDocument();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("^add child policy for GMC_Ops$")
	public void add_child_policy_for_GMC_Ops() throws Throwable {
		
		try {
			Home home = PageFactory.initElements(driver, pom.agent.gmc.Home.class);
			home.addChildPolicy();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
