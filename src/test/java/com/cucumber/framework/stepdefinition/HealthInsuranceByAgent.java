package com.cucumber.framework.stepdefinition;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.cucumber.framework.runner.Hooks;
import com.utilities.DBUtility;
import com.utilities.Library;
import com.utilities.PropertyFileReader;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import pom.agent.health.AgeDetails;
import pom.agent.health.ChoosePaymentMethod;
import pom.agent.health.DigitHealthCarePlusPolicy;
import pom.agent.health.MedicalQuestions;
import pom.agent.health.PersonalDetails;
import pom.agent.health.Summary;
import pom.agent.home.Home;
import pom.agent.home.LoginPage;

public class HealthInsuranceByAgent{
	private static Library library = new Library();
	WebDriver driver;
	public static String flag = "accepted";

	@Before
	public void before(Scenario s) {
		System.out.println(s.getName());
		Library.staticMap = new LinkedHashMap<>();
		Library.staticMap.put("Scenario", s.getName());
		Hooks.scenario = s;
	}
	//
	//	@After
	//	public void after(Scenario s) {
	//		try {
	//			library.getDriver().close();
	//		} catch(Exception e) {
	//			Reporter.log("Browser already closed!!",true);
	//		}
	//	}

	@Given("^Logged-in into the application$")
	public void logged_in_into_the_application() throws Throwable {
		try {
			PropertyFileReader read = new PropertyFileReader();
			String credentials = "UserName:="+read.getHealthUserName()+" # Password:="+read.getHealthPassword();

			driver = library.DriverSetup();

			library.setEnvironment();
			driver.get(Library.staticMap.get("server") + "/DigitPlus/#/login");
			Thread.sleep(2000);
			LoginPage loginPageObj = PageFactory.initElements(driver, LoginPage.class);
			loginPageObj.enterDetails(credentials);

		} catch (Exception e) {
			//			System.out.println("Screenshot starts");
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}

	}

	@When("^Clicked on module \"([^\"]*)\"$")
	public void clicked_on_module(String arg1) throws Throwable {
		
		try {
			Home obj = PageFactory.initElements(driver, Home.class);
			try {
				library.waitForDigitLoad();
				obj.getClosePopup(driver);
			}catch(Exception e) {
				Reporter.log("No popup present this time.",true);
			}System.out.println("Clicking on module: "+arg1);
			obj.selectModule(arg1);
		} catch (Exception e) {
//			Screenshot s=new AShot().shootingStrategy(ShootingStrategies.viewportPasting(2000)).takeScreenshot(driver);
////	        ImageIO.write(s.getImage(),"PNG",new File(Paths.get("").toAbsolutePath().toString()+"/target/ExcelReportRPA/fpshot.png"));
//			ByteArrayOutputStream bos = new ByteArrayOutputStream();
//		      ObjectOutputStream oos = new ObjectOutputStream(bos);
//		      oos.writeObject(s);
//		      oos.flush();
//	        
//	        final byte[] screenshot = bos.toByteArray();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
		}
	}

	@When("^\"([^\"]*)\" button is clicked$")
	public void button_is_clicked(String arg1) throws Throwable {

		try {
			DigitHealthCarePlusPolicy dhcppObj = PageFactory.initElements(driver, DigitHealthCarePlusPolicy.class);
			Thread.sleep(3000);System.out.println("Clicking on button: "+arg1);
			dhcppObj.select(arg1);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^IMD code \"([^\"]*)\" is provided$")
	public void imd_code_is_provided(String arg1) throws Throwable {

		AgeDetails ad = PageFactory.initElements(driver, AgeDetails.class);
		try {
			ad.getIMD(arg1);
		}catch(NoSuchElementException e) {
			Reporter.log("No IMD code for the current user.",true);
		}
	}

	//	@When("^age details are entered as \"([^\"]*)\"$")
	//	public void age_details_are_entered_as(String arg1) throws Throwable {
	//		driver.findElement(By.xpath("//input[@name='dateOfbirth']")).sendKeys("15");
	//		driver.findElement(By.xpath("//input[@name='dateOfbirth']")).sendKeys("12");
	//		driver.findElement(By.xpath("//input[@name='dateOfbirth']")).sendKeys("1990");
	//		DBUtility db = PageFactory.initElements(driver, DBUtility.class);
	//		arg1 = db.getPersonData(arg1);
	//		
	//		AgeDetails ad = PageFactory.initElements(driver, AgeDetails.class);
	//		ad.enterAgeDetails(arg1);
	//	}

	@When("^cover for \"([^\"]*)\" from provided planId \"([^\"]*)\"$")
	public void cover_for_Person_from_provided_planId(String arg1,String planId) throws Throwable {
		try {
			AgeDetails ad = PageFactory.initElements(driver, AgeDetails.class);System.out.println("Providing Cover for: "+arg1);
			ad.coverFor(driver,planId,arg1);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^unselect Proposer$")
	public void unselect_Proposer() throws Throwable {
		AgeDetails ad = PageFactory.initElements(driver, AgeDetails.class);System.out.println("Unselecting Proposer ");
		ad.unSelectProposer();
	}

	@When("^Enter the pinCode \"([^\"]*)\"$")
	public void enter_the_pinCode(String planId) throws Throwable {

		try {
			AgeDetails ad = PageFactory.initElements(driver, AgeDetails.class);
			ad.enterPinCode(driver,planId,"pincode");
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^view options \"([^\"]*)\"$")
	public void view_options(String arg1) throws Throwable {
		try {
			AgeDetails ad = PageFactory.initElements(driver, AgeDetails.class);System.out.println("Clicking on: "+arg1);
			ad.performAction(arg1);
			Thread.sleep(5000);
			
//				System.out.println("URL : "+library.getDriver().getCurrentUrl());
//			
//			Response resp = RestAssured.get(library.getDriver().getCurrentUrl());
//			
//			resp.then().log().all();
//			System.out.println();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^plan details selected from planId \"([^\"]*)\"$")
	public void plan_details_selected_from_planId(String planId) throws Throwable {

		try {
			Thread.sleep(6000);
			DBUtility db = PageFactory.initElements(driver, DBUtility.class);
			db.selectPlanDetailsForHealthFromDB(driver,planId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^choose plan for family from planId \"([^\"]*)\"$")
	public void choose_plan_for_family_from_planId(String arg1) throws Throwable {
		try {
			DBUtility db = PageFactory.initElements(driver, DBUtility.class);
			db.planToBeSelected(driver, arg1);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^choose plan for parent from planId \"([^\"]*)\"$")
	public void choose_plan_for_parent_from_planId(String arg1) throws Throwable {
		try {
			DBUtility db = PageFactory.initElements(driver, DBUtility.class);
			db.planToBeSelected(driver, arg1);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}


	@When("^medic details from database \"([^\"]*)\" from planId \"([^\"]*)\" are entered in Agent Health medical questions page$")
	public void medic_details_from_database_from_planId_are_entered_in_Agent_Health_medical_questions_page(String arg,String planId) throws Throwable {

		try {
			//			com.utilities.Library library = new com.utilities.Library();
			library.waitForDigitLoad();
			MedicalQuestions medobj = PageFactory.initElements(driver, MedicalQuestions.class);
			medobj.selectMedicalQuestionsFromDB(arg,planId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^answer all further medical questions for \"([^\"]*)\"$")
	public void answerFurtherMedicalQuestionsFromDB(String familyMember) throws SQLException, InterruptedException {

		//		try {
		//			String[] s = familyMember.split(",") ;
		//			for (String member : s) {
		//				answerMultipleTimes(member);
		//			}
		//		} catch(Exception e) {
		//			answerMultipleTimes(familyMember);
		//		}

		DBUtility db = PageFactory.initElements(driver, DBUtility.class);

		com.utilities.Library library = new com.utilities.Library();
		WebDriver driver = library.getDriver();

		MedicalQuestions homeobj = PageFactory.initElements(driver, MedicalQuestions.class);
		String details = "";
		try {
			details = db.getDiseaseDataFromDB("Thyroid");
			System.out.println("Thyroid Details: "+details);
			homeobj.answerFurtherMedicalQuestions("Thyroid", familyMember, details);
		} catch (Exception e) {
		}
		Thread.sleep(2000);
		try {
			details = db.getDiseaseDataFromDB("Diabetes");
			System.out.println("Diabetes Details: "+details);
			homeobj.answerFurtherMedicalQuestions("Diabetes", familyMember, details);
		} catch (Exception e) {

			//				final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			//				Hooks.scenario.embed(screenshot, "image/png");
			//				Assert.assertTrue(false, e.getMessage());

		}
		Thread.sleep(2000);
		try {
			details = db.getDiseaseDataFromDB("Asthma");
			System.out.println("Asthma Details: "+details);;
			homeobj.answerFurtherMedicalQuestions("Asthma", familyMember, details);
		} catch (Exception e) {
		}
		Thread.sleep(2000);
		try {
			details = db.getDiseaseDataFromDB("Hypertension");
			System.out.println("Hypertension Details: "+details);
			homeobj.answerFurtherMedicalQuestions("Hypertension", familyMember, details);
		} catch (Exception e) {
		}
		Thread.sleep(2000);
		try {
			details = db.getDiseaseDataFromDB("Hyperlipidemia");
			System.out.println("Hyperlipidemia Details: "+details);
			homeobj.answerFurtherMedicalQuestions("Hyperlipidemia", familyMember, details);
		} catch (Exception e) {
		}
	}

	//	private void answerMultipleTimes(String familyMember) {
	//
	//		
	//	}

	@When("^click \"([^\"]*)\" in Agent Health medical questions page$")
	public void click_action_in_Agent_Health_medical_questions_page(String action) throws Throwable {

		Thread.sleep(3000);

		try {
			//		com.utilities.Library library = new com.utilities.Library();
			//		WebDriver driver = library.getDriver();
			MedicalQuestions homeobj = PageFactory.initElements(driver, MedicalQuestions.class);
			homeobj.performAction(action);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^details entered for individuals \"([^\"]*)\" as \"([^\"]*)\" from planId \"([^\"]*)\"$")
	public void details_entered_for_individual_as(String member,String details,String planId) throws Throwable {

		try {
			//			com.utilities.Library library = new com.utilities.Library();
			//			WebDriver driver = library.getDriver();
			PersonalDetails homeobj = PageFactory.initElements(driver, PersonalDetails.class);
			homeobj.enterSpecificFamilyDetailsFromDB(member,details,planId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^verify eligibility for policy creation$")
	public void verify_eligibility_for_policy_creation() throws Exception {
		library.waitForDigitLoad();
		Thread.sleep(3000);
		try {
			
			List<WebElement> eligibility = driver.findElements(By.xpath("//label[contains(text(),'who can be covered & who')]/../div/label"));
			for (WebElement msg : eligibility) {
				if (msg.getText().toLowerCase().contains("declined")) {
					flag = "declined";
					Thread.sleep(3000);
					break;
				}
			}
			for (WebElement msg : eligibility) {
				if (msg.getText().toLowerCase().contains("referred")) {
					flag = "referred";
					Thread.sleep(3000);
					break;
				}
			}
		}catch(NoSuchElementException e) {
			System.out.println("*****************Couldn't find decline message!!*****************");
		}
	}


	@When("^store the proposal number in database for planId \"([^\"]*)\"$")
	public void store_the_proposal_number_in_database_for_planId(String planId) throws Exception {
		Thread.sleep(5000);
		Summary sum = PageFactory.initElements(driver, Summary.class);
		String policy = sum.getProposalNumber();System.out.println("Policy: "+policy);
		library.captureStringInFile(policy, "/AllResponse/Policies.txt");
		Library.staticMap.put("Policy Number", policy);
		try {
			DBUtility db = PageFactory.initElements(driver, DBUtility.class);
			db.storeResultInNonABS_UAT_DB(policy,planId,flag,"digit_health.health_testdata_plan");
		} catch (Exception e) {
			
		}
	}

	@When("^store the proposal number in database for portId \"([^\"]*)\"$")
	public void store_the_proposal_number_in_database_for_portId(String portId) throws Throwable {
		Thread.sleep(5000);
		Summary sum = PageFactory.initElements(driver, Summary.class);
		String policy = sum.getProposalNumber();
		System.out.println("Policy: "+policy);
		Library.staticMap.put("Policy Number", policy);
		try {
			DBUtility db = PageFactory.initElements(driver, DBUtility.class);
			db.storeResultInNonABS_UAT_DB(policy,portId,flag,"digit_health.health_testdata_portability");
		} catch (Exception e) {
			
		}
	}

	@When("^download policy PDF$")
	public void download_policy_PDF() throws InterruptedException {
		Thread.sleep(3000);
		Summary sum = PageFactory.initElements(driver, Summary.class);
		sum.getDownloadPDF();
		Thread.sleep(10000);
	}
	
	@When("^accept Terms and Conditions for RetailHealth \"([^\"]*)\"$")
	public void accept_Terms_and_Conditions(String retailId) throws Throwable {
		try {
			Summary summ = PageFactory.initElements(library.getDriver(), Summary.class);
			summ.acceptTermsAndConditionsForRetailHealth(retailId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	@When("^accept Terms and Conditions for Portability \"([^\"]*)\"$")
	public void accept_Terms_and_Conditions_for_Portability(String retailId) throws Throwable {
		try {
			Summary summ = PageFactory.initElements(library.getDriver(), Summary.class);
			summ.acceptTermsAndConditionsForPortabilityInRetailHealth(retailId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^premium paid by \"([^\"]*)\"$")
	public void premium_paid_by(String arg1) throws Throwable {

		try {
			Thread.sleep(3000);
			Summary sum = PageFactory.initElements(driver, Summary.class);
			sum.premiumPaidBy(arg1);
		} catch (Exception e) {
			if ((flag.equals("accepted") || flag.equals("referred") ) && !Summary.btnSendOTPIsPresent) {
				final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
				Hooks.scenario.embed(screenshot, "image/png");
				Assert.assertTrue(false, e.getMessage());
			}
		}
	}

	@When("^mode of payment and its info are entered in Agent Health summary page$")
	public void mode_of_payment_and_its_info_are_entered_in_Agent_Health_summary_page() throws Throwable {

		try {
			Thread.sleep(2000);
			Summary summ = PageFactory.initElements(driver, Summary.class);
			summ.selectPaymentMethods(DBUtility.modeOfPayment);
		} catch (Exception e) {
			if ((flag.equals("accepted") || flag.equals("referred")) && !Summary.btnSendOTPIsPresent) {
				final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
				Hooks.scenario.embed(screenshot, "image/png");
				Assert.assertTrue(false, e.getMessage());
			}
		}
	}

	@When("^proceed to the pay in Agent Health summary page$")
	public void proceedToPay()  {
		try {
			//			com.utilities.Library library = new com.utilities.Library();
			//			WebDriver driver = library.getDriver();
			Summary summobj = PageFactory.initElements(driver, Summary.class);
			summobj.getProceedToPay();
		} catch (Exception e) {
			if ((flag.equals("accepted") || flag.equals("referred")) && !Summary.btnSendOTPIsPresent) {
				final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
				Hooks.scenario.embed(screenshot, "image/png");
				Assert.assertTrue(false, e.getMessage());
			}
		}
	}

	@When("^mock payment done$")
	public void mock_payment_done() throws Throwable {
		try {

			library.waitForDigitLoad();
			ChoosePaymentMethod cpmobj = PageFactory.initElements(driver, ChoosePaymentMethod.class);
			cpmobj.makeMockPayment();
			Thread.sleep(10000);

		} catch (Exception e) {
			//			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			//			Hooks.scenario.embed(screenshot, "image/png");
			//			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^verify success$")
	public void verify_success() throws Throwable {
		try {
			library.waitForDigitLoad();

			if (flag.equals("accepted") || flag.equals("referred")) {
				String url = driver.getCurrentUrl();System.out.println("URL : "+url);
				if (url.contains("mockfailure") || url.contains("#/health/success") || url.contains("#/health") || url.contains("#/health-home") || url.contains("#/login") || url.contains("#/health-success")) {
					System.out.println("**********SUCCESS!!***********");
				} else {
					throw new Exception();
				}
			}
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
