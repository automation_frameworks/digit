package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.cucumber.framework.runner.Hooks;
import com.utilities.DBUtility;

import cucumber.api.java.en.When; 
import pom.agent.health.PlanDetails;
import pom.agent.health.PortYourPolicy;

public class Health_Portability {

	com.utilities.Library library = new com.utilities.Library();
	
	@When("^answer existing policy questions from provided portId \"([^\"]*)\"$")
	public void answer_existing_policy_questions_from_provided_portId(String portId) throws Throwable {
	   
		try {
			PortYourPolicy port = PageFactory.initElements(library.getDriver(), PortYourPolicy.class);
			port.answerTheQuestionsFromDB(portId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^increaseSI option is fetched from portId \"([^\"]*)\"$")
	public void increasesi_option_is_fetched_from_portId(String portId) throws Throwable {
	  
		try {
			PlanDetails plan = PageFactory.initElements(library.getDriver(), PlanDetails.class);
			plan.increaseSI(portId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^all existing policy details are filled from portId \"([^\"]*)\"$")
	public void all_existing_policy_details_are_filled_from_portId(String portId) throws Throwable {
	   
		try {
			PortYourPolicy port = PageFactory.initElements(library.getDriver(), PortYourPolicy.class);
			port.fillPolicyDetailsFromDB(portId);
			Thread.sleep(5000);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^provide detail \"([^\"]*)\" in Port Policy popup from portId \"([^\"]*)\"$")
	public void provide_detail_in_Port_Policy_popup_from_portId(String arg1, String portId) throws Throwable {
	    
		try {
			PortYourPolicy port = PageFactory.initElements(library.getDriver(), PortYourPolicy.class);
			port.portPolicyPopup(arg1, portId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("^plan selected from planId \"([^\"]*)\"$")
	public void plan_selected_from_planId(String arg1) throws Throwable {

		try {
			DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
			db.planToBeSelected(library.getDriver(), arg1);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
