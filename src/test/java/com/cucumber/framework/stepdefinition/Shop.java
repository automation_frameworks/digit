package com.cucumber.framework.stepdefinition;

import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.cucumber.framework.runner.Hooks;

import cucumber.api.java.en.When;

public class Shop {

	com.utilities.Library library = new com.utilities.Library();

	@When("ownership type is selected as \"([^\"]*)\" in Agent Shop Insurance page")
	public void enterVehicleDetails(String ownership) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Shop detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Shop.class);
			detailsobj.selectOwnershipType(ownership);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("shop details \"([^\"]*)\" are entered in Agent Shop Insurance page")
	public void enterShopDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Shop detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Shop.class);
			detailsobj.enterShopDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("SFSP details \"([^\"]*)\" are entered in Agent Shop Insurance page")
	public void enterSFSPDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Shop detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Shop.class);
			detailsobj.enterSFSPDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("burglary details \"([^\"]*)\" are entered in Agent Shop Insurance page")
	public void enterBurglaryDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Shop detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Shop.class);
			detailsobj.enterBurglaryDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("asset care details \"([^\"]*)\" are entered in Agent Shop Insurance page")
	public void enterAssetCareDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Shop detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Shop.class);
			detailsobj.enterAssetCareDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("employee compensation details \"([^\"]*)\" are entered in Agent Shop Insurance page")
	public void enterEmployeeCompensationDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Shop detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Shop.class);
			detailsobj.enterEmployeeCompensationDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("insurer details \"([^\"]*)\" are entered in Agent Shop Insurance page")
	public void enterCustomerDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Shop detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Shop.class);
			detailsobj.enterCustomerDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("action \"([^\"]*)\" is performed in Shop Insurance page")
	public void performAction(String action) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Shop detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Shop.class);
			detailsobj.performAction(action);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("payment details \"([^\"]*)\" are entered in Agent Shop Insurance page")
	public void enterPaymentDetails(String paymentMode) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Shop detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Shop.class);
			detailsobj.enterPaymentDetails(paymentMode);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("verify the success page for Shop Insurance")
	public void verifySuccessPage() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Shop detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Shop.class);
			detailsobj.verifySuccessPage();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
