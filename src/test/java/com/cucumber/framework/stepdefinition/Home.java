package com.cucumber.framework.stepdefinition;

import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.cucumber.framework.runner.Hooks;

import cucumber.api.java.en.When;

public class Home {

	com.utilities.Library library = new com.utilities.Library();

	@When("ownership type is selected as \"([^\"]*)\" in Agent Home Insurance page")
	public void enterVehicleDetails(String ownership) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Home detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Home.class);
			detailsobj.selectOwnershipType(ownership);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("home details \"([^\"]*)\" are entered in Agent Home Insurance page")
	public void enterHomeDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Home detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Home.class);
			detailsobj.enterHomeDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("SFSP details \"([^\"]*)\" are entered in Agent Home Insurance page")
	public void enterSFSPDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Home detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Home.class);
			detailsobj.enterSFSPDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("burglary details \"([^\"]*)\" are entered in Agent Home Insurance page")
	public void enterBurglaryDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Home detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Home.class);
			detailsobj.enterBurglaryDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("asset care details \"([^\"]*)\" are entered in Agent Home Insurance page")
	public void enterAssetCareDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Home detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Home.class);
			detailsobj.enterAssetCareDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("action \"([^\"]*)\" is performed in Home Insurance page")
	public void performAction(String action) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Home detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Home.class);
			detailsobj.performAction(action);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("payment details \"([^\"]*)\" are entered in Agent Home Insurance page")
	public void enterPaymentDetails(String paymentMode) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Home detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Home.class);
			detailsobj.enterPaymentDetails(paymentMode);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("verify the success page for Home Insurance")
	public void verifySuccessPage() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			pom.agent.homeshop.Home detailsobj = PageFactory.initElements(driver, pom.agent.homeshop.Home.class);
			detailsobj.verifySuccessPage();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
