package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.agent.gpa_sme.GPA_Master_Policy_Creation;
import com.cucumber.framework.runner.Hooks;
import com.utilities.Library;
import com.utilities.PropertyFileReader;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import pom.agent.home.LoginPage;

public class GPA_SME {

	private static com.utilities.Library library = new com.utilities.Library();
	WebDriver driver;
	
	@Given("^Login into the application for GPA_SME$")
	public void login_into_the_application_for_GPA_SME() throws Throwable {

		try {
			PropertyFileReader read = new PropertyFileReader();

			driver = library.DriverSetup();
			//Set Environment
			library.setExecutionEnvironment(read.getEnvironment_For_GMC_Large());
			driver.get(Library.staticMap.get("server") + "/DigitPlus/#/login");

			String credentials = "UserName:="+read.getGPASMEPreprodHealthPlusUserName()+" # Password:="+read.getGPASMEPreprodHealthPlusPassword();
			LoginPage loginPageObj = PageFactory.initElements(driver, LoginPage.class);
			loginPageObj.enterDetails(credentials);
			
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("^provide insured details for GPA_SME from \"([^\"]*)\"$")
	public void provide_insured_details_for_GPA_SME_from(String gpa_sme_id) throws Throwable {
		try {
			GPA_Master_Policy_Creation gpa =  PageFactory.initElements(driver, GPA_Master_Policy_Creation.class);
			gpa.fillInsuredDetails_GPA_SME(gpa_sme_id);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^provide prospective policy details for GPA_SME from \"([^\"]*)\"$")
	public void provide_prospective_policy_details_for_GPA_SME_from(String gpa_sme_id) throws Throwable {
		try {
			GPA_Master_Policy_Creation gpa =  PageFactory.initElements(driver, GPA_Master_Policy_Creation.class);
			gpa.fillProspectiveDetails_GPA_SME(gpa_sme_id);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^provide coverage details for GPA_SME from \"([^\"]*)\"$")
	public void provide_coverage_details_for_GPA_SME_from(String gpa_sme_id) throws Throwable {
		try {
			GPA_Master_Policy_Creation gpa =  PageFactory.initElements(driver, GPA_Master_Policy_Creation.class);
			gpa.fillCoverageDetails_GPA_SME(gpa_sme_id);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^provide premium details for GPA_SME from \"([^\"]*)\"$")
	public void provide_premium_details_for_GPA_SME_from(String gpa_sme_id) throws Throwable {
		try {
			GPA_Master_Policy_Creation gpa =  PageFactory.initElements(driver, GPA_Master_Policy_Creation.class);
			gpa.fillPremiumDetails_GPA_SME(gpa_sme_id);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("^provide payment details for GPA_SME from \"([^\"]*)\"$")
	public void provide_payment_details_for_GPA_SME_from(String gpa_sme_id) throws Throwable {
		try {
			GPA_Master_Policy_Creation gpa =  PageFactory.initElements(driver, GPA_Master_Policy_Creation.class);
			gpa.fillPaymentDetails_GPA_SME(gpa_sme_id);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("^store policy created for GPA_SME in database$")
	public void store_policy_created_for_GPA_SME_in_database() throws Throwable {
		try {
			GPA_Master_Policy_Creation gpa =  PageFactory.initElements(driver, GPA_Master_Policy_Creation.class);
			gpa.storeDataForGPA_SMEInDB();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
}
