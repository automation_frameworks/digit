package com.cucumber.framework.stepdefinition;

import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.cucumber.framework.runner.Hooks;
import com.utilities.Library;
import com.utilities.PropertyFileReader;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import pom.rap.Home;
import pom.rap.Login;
import pom.rap.MainPage;
import pom.rap.Questions;
import pom.rap.Search;
import pom.rap.person.Detail;
import pom.rap.person.Fleet;

public class ABS {

	private static com.utilities.Library library = new com.utilities.Library();
	
	@Given("logged into ABS with details \"([^\"]*)\"$")
	public static void login(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.DriverSetupForRAP();
			library.setEnvironment();
			PropertyFileReader prop = new PropertyFileReader();
			if(prop.getEnvironment().toString().toLowerCase().contains("uat"))
			driver.get("http://uat-rap.godigit.com/RAP/sb/abs");
			else
				driver.get("http://prod-abs-sanity.godigit.com/RAP/sb/abs");
			Thread.sleep(5000);
			Login loginobj = PageFactory.initElements(driver, Login.class);
			loginobj.login(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	
	@When("select module \"([^\"]*)\" in ABS home page$")
	public static void selectModule(String moduleName) throws IOException {
		try {
			Thread.sleep(15000);
			com.utilities.Library library = new com.utilities.Library();
			Home obj = PageFactory.initElements(library.getDriver(), Home.class);
			obj.selectModule(moduleName);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("search with details \"([^\"]*)\" in ABS Search page$")
	public static void search(String details) throws IOException {
		try {
			Thread.sleep(5000);
			if(details.contains("[Policy_Number]"))
			{
				details = details.replaceAll("\\[Policy_Number\\]", Library.staticMap.get("Policy Number"));
			}
			com.utilities.Library library = new com.utilities.Library();
			Search obj = PageFactory.initElements(library.getDriver(), Search.class);
			obj.search(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("switch to \"([^\"]*)\" tab in ABS Main Page$")
	public static void switchToTab(String tabName) throws IOException {
		try {
			Thread.sleep(5000);
			com.utilities.Library library = new com.utilities.Library();
			MainPage obj = PageFactory.initElements(library.getDriver(), MainPage.class);
			obj.selectTab(tabName);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("complete the Preinspection process$")
	public static void completePreInspection() throws IOException {
		try {
			Thread.sleep(5000);
			com.utilities.Library library = new com.utilities.Library();
			Questions obj = PageFactory.initElements(library.getDriver(), Questions.class);
			obj.completePreInspection();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("search with details \"([^\"]*)\" in ABS Person Search page$")
	public static void searchPerson(String details) throws IOException {
		try {
			details = library.getParamValue(details);
			com.utilities.Library library = new com.utilities.Library();
			pom.rap.person.Search obj = PageFactory.initElements(library.getDriver(), pom.rap.person.Search.class);
			obj.search(details);
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("click \"([^\"]*)\" button in ABS Person Search result page$")
	public static void clickPersonSearch(String action) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			pom.rap.person.Search obj = PageFactory.initElements(library.getDriver(), pom.rap.person.Search.class);
			obj.performAction_SearchResult(action);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("select \"([^\"]*)\" from the menu bar")
	public static void selectMenuItem(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			Detail obj = PageFactory.initElements(library.getDriver(), Detail.class);
			obj.selectMenuItem(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("move to sub tab \"([^\"]*)\" in ABS Natural Person detail page")
	public static void moveToSubTab(String subTabName) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			Detail obj = PageFactory.initElements(library.getDriver(), Detail.class);
			obj.moveToSubTabs(subTabName);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("click \"([^\"]*)\" button in ABS Fleet page")
	public static void clickButton_Fleet(String action) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			Fleet obj = PageFactory.initElements(library.getDriver(), Fleet.class);
			obj.performAction(action);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("move to sub tab \"([^\"]*)\" in ABS Fleet page")
	public static void moveToSubTab_Fleet(String subTab) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			Fleet obj = PageFactory.initElements(library.getDriver(), Fleet.class);
			obj.moveToSubTabs(subTab);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("select \"([^\"]*)\" main class in ABS Fleet page")
	public static void selectMainClass(String mainClass) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			Fleet obj = PageFactory.initElements(library.getDriver(), Fleet.class);
			obj.selectMainClass(mainClass);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("enter class details \"([^\"]*)\" is entered in ABS Fleet page")
	public static void enterClassDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			Fleet obj = PageFactory.initElements(library.getDriver(), Fleet.class);
			obj.enterClassDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
