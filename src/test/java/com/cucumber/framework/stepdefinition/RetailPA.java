package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.cucumber.framework.runner.Hooks;
import com.utilities.DBUtility;
import com.utilities.Library;

import cucumber.api.java.en.When;
import pom.agent.health.AgeDetails;
import pom.agent.health.Summary;
import pom.agent.retail_pa.MedicalQuestionRetailPA;
import pom.agent.retail_pa.MemberDetailsRetailPA;
import pom.agent.retail_pa.PersonalAccidentInsurance;
import pom.agent.retail_pa.PlanDetailsRetailPA;
import pom.agent.retail_pa.PlanList;

public class RetailPA {

	private com.utilities.Library library = new com.utilities.Library();
	
	@When("^IMD code if needed is provided from retailId \"([^\"]*)\"$")
	public void imd_code_if_needed_is_provided_from_retailId(String retailId) throws Throwable {
		AgeDetails ad = PageFactory.initElements(library.getDriver(), AgeDetails.class);
		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		try {
			ad.getIMD(db.getRetailDataFromDB("imdcode", retailId));
		}catch(NoSuchElementException e) {
			Reporter.log("No IMD code for the given environment.",true);
		}
	}
	
	@When("^cover for individuals from provided retailId \"([^\"]*)\"$")
	public void cover_for_individuals_from_provided_retailId(String retailId) throws Throwable {
		try {
			PersonalAccidentInsurance pai = PageFactory.initElements(library.getDriver(), PersonalAccidentInsurance.class);
			DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
			String people = db.getRetailDataFromDB("coverfor", retailId);
			pai.cover(library.getDriver(),people,retailId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("^earning member provided from \"([^\"]*)\"$")
	public void earning_member_provided_from(String retailId) throws Throwable {
	    try {
			PersonalAccidentInsurance pai = PageFactory.initElements(library.getDriver(), PersonalAccidentInsurance.class);
			pai.selectEarningMember(library.getDriver(),retailId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^occupation class entered from \"([^\"]*)\"$")
	public void occupation_class_entered_from(String retailId) throws Throwable {
		try {
			PersonalAccidentInsurance pai = PageFactory.initElements(library.getDriver(), PersonalAccidentInsurance.class);
			pai.selectOccputaionClass(library.getDriver(), retailId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("^view options for \"([^\"]*)\"$")
	public void view_options(String retailId) throws Throwable {
		try {
			PersonalAccidentInsurance pai = PageFactory.initElements(library.getDriver(), PersonalAccidentInsurance.class);
			pai.getViewOptions(library.getDriver(),retailId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("^accept Terms and Conditions for RetailPA \"([^\"]*)\"$")
	public void accept_Terms_and_Conditions(String retailId) throws Throwable {
		try {
			Summary summ = PageFactory.initElements(library.getDriver(), Summary.class);
			summ.acceptTermsAndConditionsForRetailPA(retailId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^plan for group selected from \"([^\"]*)\"$")
	public void plan_for_group_selected_from(String retailId) throws Throwable {
		try {
			PlanList pl = PageFactory.initElements(library.getDriver(), PlanList.class);
			DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
			String group = db.getRetailDataFromDB("planfor", retailId);
			pl.selectPAPlan(library.getDriver(), group, retailId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^plan details for group provided from \"([^\"]*)\"$")
	public void plan_details_for_group_provided_from(String retailId) throws Throwable {
		try {
			PlanDetailsRetailPA pd = PageFactory.initElements(library.getDriver(), PlanDetailsRetailPA.class);
			pd.handlePopup();
			pd.selectSI(library.getDriver(), retailId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	@When("^coverage details for group provided from \"([^\"]*)\"$")
	public void coverage_details_for_group_provided_from(String retailId) throws Throwable {
		try {
			PlanDetailsRetailPA pd = PageFactory.initElements(library.getDriver(), PlanDetailsRetailPA.class);
			pd.getCoverage(library.getDriver(), retailId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("^recalculate and proceed$")
	public void recalculate_and_proceed() throws Throwable {
		try {
			PlanDetailsRetailPA pd = PageFactory.initElements(library.getDriver(), PlanDetailsRetailPA.class);
			pd.recalculateAndProceed();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^member details entered for individuals from \"([^\"]*)\"$")
	public void member_details_entered_for_individuals_from(String retailId) throws Throwable {
		try {
			MemberDetailsRetailPA md = PageFactory.initElements(library.getDriver(), MemberDetailsRetailPA.class);
			DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
			String member = db.getRetailDataFromDB("coverfor", retailId);
			md.enterDetails(member,retailId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^medic information entered from \"([^\"]*)\"$")
	public void medic_information_entered_from(String retailId) throws Throwable {
		try {
			MedicalQuestionRetailPA mq = PageFactory.initElements(library.getDriver(), MedicalQuestionRetailPA.class);
			mq.selectMedicalCondition(library.getDriver(), retailId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("^store the proposal number in database for retailId \"([^\"]*)\"$")
	public void store_the_proposal_number_in_database_for_retailId(String retailId) throws Throwable {
		try {
			Thread.sleep(5000);
			Summary sum = PageFactory.initElements(library.getDriver(), Summary.class);
			String policy = sum.getProposalNumber();
			System.out.println("Policy: "+ policy);
			library.captureStringInFile(policy, "/AllResponse/Policies.txt");
			Library.staticMap.put("Policy Number", policy);
			DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
			db.storeResultInNonABS_UAT_DB(policy,retailId,HealthInsuranceByAgent.flag,"digit_retail_pa.retail_pa_testdata");
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("^upload documents if asked$")
	public void upload_documents_if_asked() throws Throwable {
		try {
			Summary summ = PageFactory.initElements(library.getDriver(), Summary.class);
			summ.uploadDocuments();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("^mode of payment fetched from database from retailId \"([^\"]*)\"$")
	public void mode_of_payment_fetched_from_database_from_retailId(String retailId) throws Throwable {
		try {
			DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
			Thread.sleep(2000);
			Summary sumobj = PageFactory.initElements(library.getDriver(), Summary.class);
			sumobj.selectPaymentMethods(db.getRetailDataFromDB("modeofpayment", retailId));
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("^verify success for retailPA$")
	public void verify_success_for_retailPA() throws Throwable {
		try {
			library.waitForDigitLoad();
			Thread.sleep(10000);
			String successsURL = library.getDriver().getCurrentUrl();
			if (HealthInsuranceByAgent.flag.equals("accepted") || HealthInsuranceByAgent.flag.equals("referred")) {
				System.out.println("Success page URL : "+successsURL);
				if (successsURL.contains("success") || successsURL.contains("Success") || successsURL.contains("mockfailure") || successsURL.contains("login")) {
					System.out.println("Policy created successfully!!");
				}else {
					throw new Exception("Policy creation failed!!");
				}
			}
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
