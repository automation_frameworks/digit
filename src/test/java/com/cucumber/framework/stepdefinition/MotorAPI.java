package com.cucumber.framework.stepdefinition;

import cucumber.api.java.en.Given;
import motor_insurance_api.ExistingCommercialVehicleComprehensive;
import motor_insurance_api.ExistingCommercialVehicleThirdParty;
import motor_insurance_api.ExistingFourWheelerComprehensive;
import motor_insurance_api.ExistingFourWheelerThirdParty;
import motor_insurance_api.ExistingTwoWheelerComprehensive;
import motor_insurance_api.ExistingTwoWheelerThirdParty;
import motor_insurance_api.NewCommercialVehicleComprehensive;
import motor_insurance_api.NewCommercialVehicleThirdParty;
import motor_insurance_api.NewFourWheelerComprehensive;
import motor_insurance_api.NewFourWheelerThirdParty;
import motor_insurance_api.NewTwoWheelerComprehensive;
import motor_insurance_api.NewTwoWheelerThirdParty;

public class MotorAPI {

	
	@Given("^test AgentFloat_OneYrOD_OneYrTP for ExistingTwoWheelerComprehensive$")
	public void test_AgentFloat_OneYrOD_OneYrTP_for_ExistingTwoWheelerComprehensive() throws Throwable {
	    
		ExistingTwoWheelerComprehensive obj = new ExistingTwoWheelerComprehensive();
		obj.agentFloat_1yrOD_1yrTP();
	}

	@Given("^test AgentFloat_TwoYrOD_TwoYrTP for ExistingTwoWheelerComprehensive$")
	public void test_AgentFloat_TwoYrOD_TwoYrTP_for_ExistingTwoWheelerComprehensive() throws Throwable {
	    
		ExistingTwoWheelerComprehensive obj = new ExistingTwoWheelerComprehensive();
		obj.agentFloat_2yrOD_2yrTP();
	}

	@Given("^test AgentFloat_ThreeYrOD_ThreeYrTP for ExistingTwoWheelerComprehensive$")
	public void test_AgentFloat_ThreeYrOD_ThreeYrTP_for_ExistingTwoWheelerComprehensive() throws Throwable {
	    
		ExistingTwoWheelerComprehensive obj = new ExistingTwoWheelerComprehensive();
		obj.agentFloat_3yrOD_3yrTP();
	}

	@Given("^test AgentFloat_OneYrOD_with_prevailing_TPP for ExistingTwoWheelerComprehensive$")
	public void test_AgentFloat_OneYrOD_with_prevailing_TPP_for_ExistingTwoWheelerComprehensive() throws Throwable {
	    
		ExistingTwoWheelerComprehensive obj = new ExistingTwoWheelerComprehensive();
		obj.agentFloat_1yrOD();
	}

	@Given("^test AgentFloat_OneYrOD_with_TPP_Expired for ExistingTwoWheelerComprehensive$")
	public void test_AgentFloat_OneYrOD_with_TPP_Expired_for_ExistingTwoWheelerComprehensive() throws Throwable {
	    
		ExistingTwoWheelerComprehensive obj = new ExistingTwoWheelerComprehensive();
		obj.agentFloat_1yrOD_TPPExpired();
	}

	@Given("^test Cheque for ExistingTwoWheelerComprehensive$")
	public void test_Cheque_for_ExistingTwoWheelerComprehensive() throws Throwable {
	    
		ExistingTwoWheelerComprehensive obj = new ExistingTwoWheelerComprehensive();
		obj.cheque();
	}

	@Given("^test Online for ExistingTwoWheelerComprehensive$")
	public void test_Online_for_ExistingTwoWheelerComprehensive() throws Throwable {
	    
		ExistingTwoWheelerComprehensive obj = new ExistingTwoWheelerComprehensive();
		obj.online();
	}

	@Given("^test SelfInspection for ExistingTwoWheelerComprehensive$")
	public void test_SelfInspection_for_ExistingTwoWheelerComprehensive() throws Throwable {
	    
		ExistingTwoWheelerComprehensive obj = new ExistingTwoWheelerComprehensive();
		obj.selfInspection();
	}

	@Given("^test SendPreInspectionLink for ExistingTwoWheelerComprehensive$")
	public void test_SendPreInspectionLink_for_ExistingTwoWheelerComprehensive() throws Throwable {
	    
		ExistingTwoWheelerComprehensive obj = new ExistingTwoWheelerComprehensive();
		obj.sendPreInspectionLink();
	}

	@Given("^test GeneratePaymentLink for ExistingTwoWheelerComprehensive$")
	public void test_GeneratePaymentLink_for_ExistingTwoWheelerComprehensive() throws Throwable {
	    
		ExistingTwoWheelerComprehensive obj = new ExistingTwoWheelerComprehensive();
		obj.generatePaymentLink();
	}

	@Given("^test AgentFloat_OneYrTP for ExistingTwoWheelerThirdParty$")
	public void test_AgentFloat_OneYrTP_for_ExistingTwoWheelerThirdParty() throws Throwable {
	    
		ExistingTwoWheelerThirdParty obj = new ExistingTwoWheelerThirdParty();
		obj.agentFloat_1yrTP();
	}

	@Given("^test AgentFloat_TwoYrTP for ExistingTwoWheelerThirdParty$")
	public void test_AgentFloat_TwoYrTP_for_ExistingTwoWheelerThirdParty() throws Throwable {
	    
		ExistingTwoWheelerThirdParty obj = new ExistingTwoWheelerThirdParty();
		obj.agentFloat_2yrTP();
	}

	@Given("^test AgentFloat_ThreeYrTP for ExistingTwoWheelerThirdParty$")
	public void test_AgentFloat_ThreeYrTP_for_ExistingTwoWheelerThirdParty() throws Throwable {
	    
		ExistingTwoWheelerThirdParty obj = new ExistingTwoWheelerThirdParty();
		obj.agentFloat_3yrTP();
	}

	@Given("^test Cheque for ExistingTwoWheelerThirdParty$")
	public void test_Cheque_for_ExistingTwoWheelerThirdParty() throws Throwable {
	    
		ExistingTwoWheelerThirdParty obj = new ExistingTwoWheelerThirdParty();
		obj.cheque();
	}

	@Given("^test Online for ExistingTwoWheelerThirdParty$")
	public void test_Online_for_ExistingTwoWheelerThirdParty() throws Throwable {
	    
		ExistingTwoWheelerThirdParty obj = new ExistingTwoWheelerThirdParty();
		obj.online();
	}

	@Given("^test GeneratePaymentLink for ExistingTwoWheelerThirdParty$")
	public void test_GeneratePaymentLink_for_ExistingTwoWheelerThirdParty() throws Throwable {
	    
		ExistingTwoWheelerThirdParty obj = new ExistingTwoWheelerThirdParty();
		obj.generatePaymentLink();
	}

	@Given("^test AgentFloat_OneYrOD_OneYrTP for ExistingFourWheelerComprehensive$")
	public void test_AgentFloat_OneYrOD_OneYrTP_for_ExistingFourWheelerComprehensive() throws Throwable {
	    
		ExistingFourWheelerComprehensive obj = new ExistingFourWheelerComprehensive();
		obj.agentFloat_1yr_1TP();
	}

	@Given("^test AgentFloat_OneYrOD_with_prevailing_TPP for ExistingFourWheelerComprehensive$")
	public void test_AgentFloat_OneYrOD_with_prevailing_TPP_for_ExistingFourWheelerComprehensive() throws Throwable {
	    
		ExistingFourWheelerComprehensive obj = new ExistingFourWheelerComprehensive();
		obj.agentFloat_1yrODWithTPP();
	}

	@Given("^test AgentFloat_OneYrOD_with_TPP_Expired for ExistingFourWheelerComprehensive$")
	public void test_AgentFloat_OneYrOD_with_TPP_Expired_for_ExistingFourWheelerComprehensive() throws Throwable {
	    
		ExistingFourWheelerComprehensive obj = new ExistingFourWheelerComprehensive();
		obj.agentFloat_1yrODWithTPPExpired();
	}

	@Given("^test Cheque for ExistingFourWheelerComprehensive$")
	public void test_Cheque_for_ExistingFourWheelerComprehensive() throws Throwable {
	    
		ExistingFourWheelerComprehensive obj = new ExistingFourWheelerComprehensive();
		obj.cheque();
	}

	@Given("^test Online for ExistingFourWheelerComprehensive$")
	public void test_Online_for_ExistingFourWheelerComprehensive() throws Throwable {
	    
		ExistingFourWheelerComprehensive obj = new ExistingFourWheelerComprehensive();
		obj.online();
	}

	@Given("^test SelfInspection for ExistingFourWheelerComprehensive$")
	public void test_SelfInspection_for_ExistingFourWheelerComprehensive() throws Throwable {
	    
		ExistingFourWheelerComprehensive obj = new ExistingFourWheelerComprehensive();
		obj.selfInspection();
	}

	@Given("^test SendPreInspectionLink for ExistingFourWheelerComprehensive$")
	public void test_SendPreInspectionLink_for_ExistingFourWheelerComprehensive() throws Throwable {
	    
		ExistingFourWheelerComprehensive obj = new ExistingFourWheelerComprehensive();
		obj.sendPreInspectionLink();
	}

	@Given("^test GeneratePaymentLink for ExistingFourWheelerComprehensive$")
	public void test_GeneratePaymentLink_for_ExistingFourWheelerComprehensive() throws Throwable {
	    
		ExistingFourWheelerComprehensive obj = new ExistingFourWheelerComprehensive();
		obj.generatePaymentLink();
	}

	@Given("^test AgentFloat for ExistingFourWheelerThirdParty$")
	public void test_AgentFloat_for_ExistingFourWheelerThirdParty() throws Throwable {
	    
		ExistingFourWheelerThirdParty obj = new ExistingFourWheelerThirdParty();
		obj.agentFloat();
	}

	@Given("^test Cheque for ExistingFourWheelerThirdParty$")
	public void test_Cheque_for_ExistingFourWheelerThirdParty() throws Throwable {
	    
		ExistingFourWheelerThirdParty obj = new ExistingFourWheelerThirdParty();
		obj.cheque();
	}

	@Given("^test Online for ExistingFourWheelerThirdParty$")
	public void test_Online_for_ExistingFourWheelerThirdParty() throws Throwable {
	    
		ExistingFourWheelerThirdParty obj = new ExistingFourWheelerThirdParty();
		obj.online();
	}

	@Given("^test GeneratePaymentLink for ExistingFourWheelerThirdParty$")
	public void test_GeneratePaymentLink_for_ExistingFourWheelerThirdParty() throws Throwable {
	    
		ExistingFourWheelerThirdParty obj = new ExistingFourWheelerThirdParty();
		obj.generatePaymentLink();
	}

	@Given("^test AgentFloat for ExistingCommercialVehicleComprehensive$")
	public void test_AgentFloat_for_ExistingCommercialVehicleComprehensive() throws Throwable {
	    
		ExistingCommercialVehicleComprehensive obj = new ExistingCommercialVehicleComprehensive();
		obj.agentFloat();
	}

	@Given("^test Cheque for ExistingCommercialVehicleComprehensive$")
	public void test_Cheque_for_ExistingCommercialVehicleComprehensive() throws Throwable {
	    
		ExistingCommercialVehicleComprehensive obj = new ExistingCommercialVehicleComprehensive();
		obj.cheque();
	}

	@Given("^test Online for ExistingCommercialVehicleComprehensive$")
	public void test_Online_for_ExistingCommercialVehicleComprehensive() throws Throwable {
	    
		ExistingCommercialVehicleComprehensive obj = new ExistingCommercialVehicleComprehensive();
		obj.online();
	}

	@Given("^test SelfInspection for ExistingCommercialVehicleComprehensive$")
	public void test_SelfInspection_for_ExistingCommercialVehicleComprehensive() throws Throwable {
	    
		ExistingCommercialVehicleComprehensive obj = new ExistingCommercialVehicleComprehensive();
		obj.selfInspection();
	}

	@Given("^test SendPreInspectionLink for ExistingCommercialVehicleComprehensive$")
	public void test_SendPreInspectionLink_for_ExistingCommercialVehicleComprehensive() throws Throwable {
	    
		ExistingCommercialVehicleComprehensive obj = new ExistingCommercialVehicleComprehensive();
		obj.selfInspection();
	}

	@Given("^test AgentFloat for ExistingCommercialVehicleThirdParty$")
	public void test_AgentFloat_for_ExistingCommercialVehicleThirdParty() throws Throwable {
	    
		ExistingCommercialVehicleThirdParty obj = new ExistingCommercialVehicleThirdParty();
		obj.agentFloat();
	}

	@Given("^test Cheque for ExistingCommercialVehicleThirdParty$")
	public void test_Cheque_for_ExistingCommercialVehicleThirdParty() throws Throwable {
	    
		ExistingCommercialVehicleThirdParty obj = new ExistingCommercialVehicleThirdParty();
		obj.cheque();
	}

	@Given("^test Online for ExistingCommercialVehicleThirdParty$")
	public void test_Online_for_ExistingCommercialVehicleThirdParty() throws Throwable {
	    
		ExistingCommercialVehicleThirdParty obj = new ExistingCommercialVehicleThirdParty();
		obj.online();
	}

	@Given("^test GeneratePaymentLink for ExistingCommercialVehicleThirdParty$")
	public void test_GeneratePaymentLink_for_ExistingCommercialVehicleThirdParty() throws Throwable {
	    
		ExistingCommercialVehicleThirdParty obj = new ExistingCommercialVehicleThirdParty();
		obj.generatePaymentLink();
	}

	@Given("^test AgentFloat for NewTwoWheelerComprehensive$")
	public void test_AgentFloat_for_NewTwoWheelerComprehensive() throws Throwable {
	    
		NewTwoWheelerComprehensive obj = new NewTwoWheelerComprehensive();
		obj.agentFloat();
	}

	@Given("^test Cheque for NewTwoWheelerComprehensive$")
	public void test_Cheque_for_NewTwoWheelerComprehensive() throws Throwable {
	    
		NewTwoWheelerComprehensive obj = new NewTwoWheelerComprehensive();
		obj.cheque();
	}

	@Given("^test Online for NewTwoWheelerComprehensive$")
	public void test_Online_for_NewTwoWheelerComprehensive() throws Throwable {
	    
		NewTwoWheelerComprehensive obj = new NewTwoWheelerComprehensive();
		obj.online();
	}

	@Given("^test GeneratePaymentLink for NewTwoWheelerComprehensive$")
	public void test_GeneratePaymentLink_for_NewTwoWheelerComprehensive() throws Throwable {
	    
		NewTwoWheelerComprehensive obj = new NewTwoWheelerComprehensive();
		obj.generatePaymentLink();
	}

	@Given("^test AgentFloat for NewTwoWheelerThirdParty$")
	public void test_AgentFloat_for_NewTwoWheelerThirdParty() throws Throwable {
	    
		NewTwoWheelerThirdParty obj = new NewTwoWheelerThirdParty();
		obj.agentFloat();
	}

	@Given("^test Online for NewTwoWheelerThirdParty$")
	public void test_Online_for_NewTwoWheelerThirdParty() throws Throwable {
	    
		NewTwoWheelerThirdParty obj = new NewTwoWheelerThirdParty();
		obj.online();
	}

	@Given("^test GeneratePaymentLink for NewTwoWheelerThirdParty$")
	public void test_GeneratePaymentLink_for_NewTwoWheelerThirdParty() throws Throwable {
	    
		NewTwoWheelerThirdParty obj = new NewTwoWheelerThirdParty();
		obj.generatePaymentLink();
	}

	@Given("^test AgentFloat_OneYrOD for NewFourWheelerComprehensive$")
	public void test_AgentFloat_OneYrOD_for_NewFourWheelerComprehensive() throws Throwable {
	    
		NewFourWheelerComprehensive obj = new NewFourWheelerComprehensive();
		obj.agentFloat_1YrOD();
	}

	@Given("^test AgentFloat_OneYrOD_ThreeYrTP for NewFourWheelerComprehensive$")
	public void test_AgentFloat_OneYrOD_ThreeYrTP_for_NewFourWheelerComprehensive() throws Throwable {
	    
		NewFourWheelerComprehensive obj = new NewFourWheelerComprehensive();
		obj.agentFloat_1YrOD_3YrTP();
	}

	@Given("^test Cheque for NewFourWheelerComprehensive$")
	public void test_Cheque_for_NewFourWheelerComprehensive() throws Throwable {
	    
		NewFourWheelerComprehensive obj = new NewFourWheelerComprehensive();
		obj.cheque();
	}

	@Given("^test Online for NewFourWheelerComprehensive$")
	public void test_Online_for_NewFourWheelerComprehensive() throws Throwable {
	    
		NewFourWheelerComprehensive obj = new NewFourWheelerComprehensive();
		obj.online();
	}

	@Given("^test GeneratePaymentLink for NewFourWheelerComprehensive$")
	public void test_GeneratePaymentLink_for_NewFourWheelerComprehensive() throws Throwable {
	    
		NewFourWheelerComprehensive obj = new NewFourWheelerComprehensive();
		obj.generatePaymentLink();
	}

	@Given("^test AgentFloat for NewFourWheelerThirdParty$")
	public void test_AgentFloat_for_NewFourWheelerThirdParty() throws Throwable {
	    
		NewFourWheelerThirdParty obj = new NewFourWheelerThirdParty();
		obj.agentFloat();
	}

	@Given("^test Cheque for NewFourWheelerThirdParty$")
	public void test_Cheque_for_NewFourWheelerThirdParty() throws Throwable {
	    
		NewFourWheelerThirdParty obj = new NewFourWheelerThirdParty();
		obj.cheque();
	}

	@Given("^test Online for NewFourWheelerThirdParty$")
	public void test_Online_for_NewFourWheelerThirdParty() throws Throwable {
	    
		NewFourWheelerThirdParty obj = new NewFourWheelerThirdParty();
		obj.online();
	}

	@Given("^test GeneratePaymentLink for NewFourWheelerThirdParty$")
	public void test_GeneratePaymentLink_for_NewFourWheelerThirdParty() throws Throwable {
	    
		NewFourWheelerThirdParty obj = new NewFourWheelerThirdParty();
		obj.generatePaymentLink();
	}

	@Given("^test AgentFloat_OneYrPolicy for NewCommercialVehicleComprehensive$")
	public void test_AgentFloat_OneYrPolicy_for_NewCommercialVehicleComprehensive() throws Throwable {
	    
		NewCommercialVehicleComprehensive obj = new NewCommercialVehicleComprehensive();
		obj.agentFloat_1YrPolicy();
	}

	@Given("^test AgentFloat_ShortTermMode for NewCommercialVehicleComprehensive$")
	public void test_AgentFloat_ShortTermMode_for_NewCommercialVehicleComprehensive() throws Throwable {
	    
		NewCommercialVehicleComprehensive obj = new NewCommercialVehicleComprehensive();
		obj.agentFloat_ShortTermMode();
	}

	@Given("^test Cheque for NewCommercialVehicleComprehensive$")
	public void test_Cheque_for_NewCommercialVehicleComprehensive() throws Throwable {
	    
		NewCommercialVehicleComprehensive obj = new NewCommercialVehicleComprehensive();
		obj.cheque();
	}

	@Given("^test Online for NewCommercialVehicleComprehensive$")
	public void test_Online_for_NewCommercialVehicleComprehensive() throws Throwable {
	    
		NewCommercialVehicleComprehensive obj = new NewCommercialVehicleComprehensive();
		obj.online();
	}

	@Given("^test GeneratePaymentLink for NewCommercialVehicleComprehensive$")
	public void test_GeneratePaymentLink_for_NewCommercialVehicleComprehensive() throws Throwable {
	    
		NewCommercialVehicleComprehensive obj = new NewCommercialVehicleComprehensive();
		obj.generatePaymentLink();
	}

	@Given("^test AgentFloat_OneYrPolicy for NewCommercialVehicleThirdParty$")
	public void test_AgentFloat_OneYrPolicy_for_NewCommercialVehicleThirdParty() throws Throwable {
	    
		NewCommercialVehicleThirdParty obj = new NewCommercialVehicleThirdParty();
		obj.agentFloat_1YrPolicy();
	}

	@Given("^test AgentFloat_ThreeMonths for NewCommercialVehicleThirdParty$")
	public void test_AgentFloat_ThreeMonths_for_NewCommercialVehicleThirdParty() throws Throwable {
	    
		NewCommercialVehicleThirdParty obj = new NewCommercialVehicleThirdParty();
		obj.agentFloat_3MonthsPolicy();
	}

	@Given("^test Cheque for NewCommercialVehicleThirdParty$")
	public void test_Cheque_for_NewCommercialVehicleThirdParty() throws Throwable {
	    
		NewCommercialVehicleThirdParty obj = new NewCommercialVehicleThirdParty();
		obj.cheque();
	}

	@Given("^test Online for NewCommercialVehicleThirdParty$")
	public void test_Online_for_NewCommercialVehicleThirdParty() throws Throwable {
	    
		NewCommercialVehicleThirdParty obj = new NewCommercialVehicleThirdParty();
		obj.online();
	}

	@Given("^test GeneratePaymentLink for NewCommercialVehicleThirdParty$")
	public void test_GeneratePaymentLink_for_NewCommercialVehicleThirdParty() throws Throwable {
	    
		NewCommercialVehicleThirdParty obj = new NewCommercialVehicleThirdParty();
		obj.generatePaymentLink();
	}
}
