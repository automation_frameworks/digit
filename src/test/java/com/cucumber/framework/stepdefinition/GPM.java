package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.agent.gpm.MasterPolicyCreation;
import com.cucumber.framework.runner.Hooks;

import cucumber.api.java.en.When;

public class GPM {
	
	private static com.utilities.Library library = new com.utilities.Library();

	@When("smart button \"([^\"]*)\" is clicked in Master policy creation page")
	public void performAction(String action) throws Exception {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MasterPolicyCreation detailsobj = PageFactory.initElements(driver, MasterPolicyCreation.class);
			detailsobj.performAction(action);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("company details \"([^\"]*)\" are entered in Master policy creation page")
	public void enterCompanyDetails(String details) throws Exception {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MasterPolicyCreation detailsobj = PageFactory.initElements(driver, MasterPolicyCreation.class);
			detailsobj.enterCompanyDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("policy details \"([^\"]*)\" are entered in Master policy creation page")
	public void enterPolicyDetails(String details) throws Exception {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MasterPolicyCreation detailsobj = PageFactory.initElements(driver, MasterPolicyCreation.class);
			detailsobj.enterPolicyDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("special conditions \"([^\"]*)\" are entered in Master policy creation page")
	public void enterSpecialConditions(String details) throws Exception {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MasterPolicyCreation detailsobj = PageFactory.initElements(driver, MasterPolicyCreation.class);
			detailsobj.enterSpecialConditions(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("upload member data collection file in Master policy creation page")
	public void uploadDocument() throws Exception {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MasterPolicyCreation detailsobj = PageFactory.initElements(driver, MasterPolicyCreation.class);
			detailsobj.uploadMemberDataFile();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
//	@When("verify warning message \"([^\"]*)\" is displayed in Master policy creation page")
//	public void verifyErrorMessage(String message) throws Exception {
//		try {
//			com.utilities.Library library = new com.utilities.Library();
//			WebDriver driver = library.getDriver();
//			MasterPolicyCreation detailsobj = PageFactory.initElements(driver, MasterPolicyCreation.class);
//			detailsobj.verifyErrorMessage(message);
//		} catch (Exception e) {
//			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
//			Hooks.scenario.embed(screenshot, "image/png");
//			Assert.assertTrue(false, e.getMessage());
//		}
//	}
//	
//	@When("verify success message \"([^\"]*)\" is displayed in Master policy creation page")
//	public void verifySuccessMessage(String message) throws Exception {
//		try {
//			com.utilities.Library library = new com.utilities.Library();
//			WebDriver driver = library.getDriver();
//			MasterPolicyCreation detailsobj = PageFactory.initElements(driver, MasterPolicyCreation.class);
//			detailsobj.verifySuccessMessage(message);
//		} catch (Exception e) {
//			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
//			Hooks.scenario.embed(screenshot, "image/png");
//			Assert.assertTrue(false, e.getMessage());
//		}
//	}
	
	@When("verify dropdown \"([^\"]*)\" in Master policy creation page")
	public void verifyDropDown(String details) throws Exception {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MasterPolicyCreation detailsobj = PageFactory.initElements(driver, MasterPolicyCreation.class);
			detailsobj.verifyDropDownValues(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("verify priciple values are stored in DB")
	public void verifyPrincipleValues() throws Exception {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MasterPolicyCreation detailsobj = PageFactory.initElements(driver, MasterPolicyCreation.class);
			detailsobj.verifyPrincipleValues();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
