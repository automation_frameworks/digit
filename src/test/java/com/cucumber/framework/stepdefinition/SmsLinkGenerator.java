package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.cucumber.framework.runner.Hooks;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import pom.agent.motor.PaymentPage;
import pom.agent.motor.SmsLinkGeratorPage;

public class SmsLinkGenerator 
{
	private static com.utilities.Library library = new com.utilities.Library();
	
	@And("^click on Send Message button$")
	public void click_on_Send_Message_button() throws Throwable 
	{
		try {
			com.utilities.Library library = new com.utilities.Library();
			SmsLinkGeratorPage linkPageObj = PageFactory.initElements(library.getDriver(), SmsLinkGeratorPage.class);
			linkPageObj.clickButton();
		} catch (Exception e) {
			e.printStackTrace();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@And("^verify the page and enter partner name \"([^\"]*)\" partner and contact number \"([^\"]*)\"$")
	public void enterDetails(String partnerName , String contactNumer)
	{
		com.utilities.Library library = new com.utilities.Library();
		SmsLinkGeratorPage linkPageObj = PageFactory.initElements(library.getDriver(), SmsLinkGeratorPage.class);
		linkPageObj.enterDetails(partnerName,contactNumer);
	}
	
	@When("^verify Success Message from toast if message is sent$")
	public void verify_Success_Message_from_toast() throws Throwable 
	{
		try {
			com.utilities.Library library = new com.utilities.Library();
			SmsLinkGeratorPage linkPageObj = PageFactory.initElements(library.getDriver(), SmsLinkGeratorPage.class);
			linkPageObj.verifySuccessMessageFromToast();
		} catch (Exception e) {
			e.printStackTrace();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}