package com.cucumber.framework.stepdefinition;

import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.agent.myTransactions.MyTransactionsPage;
import com.cucumber.framework.runner.Hooks;
import com.utilities.Library;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pom.agent.home.Home;
import pom.agent.home.LoginPage;
import pom.agent.motor.DetailsPage;

public class MyTransactions {
	
	static String environMentName;
	String text;
	public String actualInsuredName;
	private static com.utilities.Library library = new com.utilities.Library();
	
	@Given("user login to the application with details")
	public static void login() throws IOException, InterruptedException {
		System.out.println("Navigating to login page");
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.DriverSetup();
			environMentName = library.setEnvironment();
			System.out.println(Library.staticMap);
			System.out.println("URL : " + Library.staticMap.get("server") + "/DigitPlus/#/login");
			driver.get(Library.staticMap.get("server"));
			 //+ "/DigitPlus/#/login"
			Thread.sleep(3000);
			LoginPage loginobj = PageFactory.initElements(driver, LoginPage.class);
			loginobj.enterLoginCredentials();
		   } 
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
			if(library.getDriver()!=null)
			{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); }// ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage() + e.getStackTrace().toString());
		}

	}

	
	
	@When("^user selects the \"([^\"]*)\" module$")
	public void user_selects_the_module(String moduleNmae) throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			System.out.println("SessionIDWhenSelecting Module:"+driver);
			Home hp = PageFactory.initElements(driver, Home.class);
			hp.selectModule(moduleNmae);
		    } 
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());

		}
	}
	
	@When("^user keep the cursor on my transaction search box$")
	public void user_keep_the_cursor_on_my_transaction_search_box() throws Throwable {
		try {
		com.utilities.Library library = new com.utilities.Library();
		WebDriver driver = library.getDriver();
		MyTransactionsPage mp=PageFactory.initElements(driver, MyTransactionsPage.class);
		mp.mouseOverOnSearchBox(driver);
		}
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());

		}
	}
	
	@Then("^user select the \"([^\"]*)\" from drop down menu options$")
	public void selentOptionFromDroption(String option) throws Throwable {
		try {
		com.utilities.Library library = new com.utilities.Library();
		WebDriver driver = library.getDriver();
		MyTransactionsPage mp=PageFactory.initElements(driver, MyTransactionsPage.class);
		mp.selectDropDownMenuOptionsFromSerachBox(driver, option);
		}
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());

		}
		
	}


	@When("^user search the policy using policy number \"([^\"]*)\"$")
	public void user_search_the_policy_using_policy_number(String details) throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
            mp.searchDataInMyTransactions(environMentName, details);
            System.out.println("Details : "+details);
		}
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());

		}
	}
	@Then("^user search the Quote using Quote number \"([^\"]*)\"$")
	public void user_search_the_Quote_using_Quote_number(String details) throws Throwable {
		
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
            mp.searchDataInMyTransactions(environMentName, details);
            System.out.println("Details : "+details);
		}
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());

		}
	}
	
	@When("^user search the quote/policy using mobile number \"([^\"]*)\"$")
	public void user_search_the_policy_using_mobile_number(String details) throws Throwable {
		try {
			
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			mp.searchDataInMyTransactions(environMentName, details);
			System.out.println("Details : "+details);
			
		}
		catch (Exception e)
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("^user search the policy using registration number \"([^\"]*)\"$")
	public void user_search_the_policy_using_registration_number(String details) throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			mp.searchDataInMyTransactions(environMentName, details);
			  } 
		catch (Exception e)
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());
		}
	}


@Then("^user search the quote/policy using Engine number \"([^\"]*)\"$")
public void user_search_the_quote_policy_using_Engine_number(String details) throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			mp.searchDataInMyTransactions(environMentName, details);
			  } 
		catch (Exception e)
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());
		}
	}
@Then("^user search the quote/policy using chassis number \"([^\"]*)\"$")
public void user_search_the_quote_policy_using_chassis_number(String details) throws Throwable {
	try {
		com.utilities.Library library = new com.utilities.Library();
		WebDriver driver = library.getDriver();
		MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
		mp.searchDataInMyTransactions(environMentName, details);
		  } 
	catch (Exception e)
	{
		final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
		Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
		Assert.assertTrue(false, e.getMessage());
	}
}

	@Then("^It should display the policy details related to the policy number \"([^\"]*)\"$")
	public void it_should_display_the_policy_details_related_to_the_policy_number(String expPolicyNumber) throws Throwable {
	try {
		com.utilities.Library library = new com.utilities.Library();
		WebDriver driver = library.getDriver();
		MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
		text="Policy Number";
		mp.enterPolicyQuotedetails(environMentName, expPolicyNumber, driver,text);
	}
	  catch (Exception e) {
		  final byte[] screenshot = ((TakesScreenshot)library.getDriver()).getScreenshotAs(OutputType.BYTES);
		  Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
		  Assert.assertTrue(false, e.getMessage());
		  }
		 
	}

	
	@Then("^select the check box of that particular policy number$")
	public void select_the_check_box_of_that_particular_policy_number() throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			mp.selectCheckBox();
			actualInsuredName = mp.getInsuredItem();
			}
		catch (Exception e)
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());

		}
	}

	@Then("^click on download option under Actions for the particular policy number of and clicks on download selected image$")
	public void click_on_download_option_under_Actions_for_the_particular_policy_number_of_and_clicks_on_download_selected_image() throws Throwable {
		try {
		library.deletetheFilesInDownloadFiles();
		com.utilities.Library library = new com.utilities.Library();
		WebDriver driver = library.getDriver();
		MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
		library.deletetheFilesInDownloadFiles();
		mp.clickOnDownloadOption();
		}
		catch (Exception e)
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());

		}
	}
	
	
	
	@Then("^verify whether file has downloaded$")
	public  void verify_whether_file_has_downloaded() throws Throwable
	{
		 //library.verifyTheDownloadedFiles();
	}
	@Then("^select the check box of first policy number$")
	public void select_the_check_box_of_any_policy_number() throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			mp.selectCheckBox();
			actualInsuredName = mp.getInsuredItem();
		}

		catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());

		}
	}

	@Then("^click on download option under Actions for first policy number and clicks on download selected image$")
	public void click_on_download_option_under_Actions_for_the_policy_number_of_and_clicks_on_download_selected_image() throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			library.deletetheFilesInDownloadFiles();
			mp.clickOnDownloadOption();
			}
		catch (Exception e)
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^click on the policy number$")
	public void click_on_the_policy_number(String text) throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			mp.clickOnFirstPolicyNumber(text);
			library.waitForDigitLoad();
			} 
		catch (Exception e)
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@Then("^it should display all the vehicle datails in single page$")
	public void it_should_display_all_the_vehicle_datails_in_single_page() throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			DetailsPage dp = PageFactory.initElements(driver, DetailsPage.class);
			//library.waitForElementVisible(dp.txtRegistrationNumber, 20, 5);
			library.waitForDigitLoad();
			String registrationNumber[] = actualInsuredName.split(",");
			actualInsuredName=registrationNumber[0].replaceAll(" ", "");
			System.out.println("ActaulRegu:"+actualInsuredName);
			String regNo= dp.getRegistartionNumber();
			System.out.println("ActualRegNoIN MY trans actualInsuredName"+registrationNumber);
			Assert.assertTrue(actualInsuredName.trim().equals(actualInsuredName.trim()));
			} 
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@Then("^it should display all the vehicle datails related to the mobile number in single page \"([^\"]*)\"$")
	public void it_should_display_all_the_vehicle_datails_realted_to_the_mobile_number_in_single_page(String details) throws Throwable {
		try {
		com.utilities.Library library = new com.utilities.Library();
		WebDriver driver = library.getDriver();
		MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
		mp.verifyMobileNumber(details,environMentName,driver);
			}
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());
		}

	}
	
	
	@Then("^it should display the policy details related to that registration number \"([^\"]*)\"$")
	public void it_should_display_the_policy_details_related_to_that_registration_number(String details) throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			mp.verifyRegistrationNumber(environMentName,details,driver);
			
		   } 
		catch (Exception e)
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Then("^it should display all the vehicle datails related to the Engine number \"([^\"]*)\"$")
	public void it_should_display_all_the_vehicle_datails_related_to_the_Engine_number(String details) throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			mp.verifyEngineNumber(environMentName,details,driver);
			
		   } 
		catch (Exception e)
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());
		}
		
	}
	
	@Then("^it should display all the vehicle datails related to the chassis number \"([^\"]*)\"$")
	public void it_should_display_all_the_vehicle_datails_related_to_the_chassis_number(String details) throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			mp.verifyChassisNumber(environMentName,details,driver);
			
		   } 
		catch (Exception e)
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());
		}
	}
	

	@When("^clicks on the Quotes$")
	public void clicks_on_the_Quotes() throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			mp.clickOnQuotes();
			}
		catch (Exception e) 
			{
				final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
				Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
				Assert.assertTrue(false, e.getMessage());
			}
}


	@Then("^It should display the quote details related to the Quote number \"([^\"]*)\"$")
	public void it_should_display_the_policy_details_related_to_the_Quote_number(String details) throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			text="Quote Number";
			mp.enterPolicyQuotedetails(environMentName, details, driver,text);
		}
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());
		}
	}
	@Then("^select the check box of that particular quote number$")
	public void select_the_check_box_of_that_particular_quote_number() throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			actualInsuredName = mp.getInsuredItem();
			mp.selectCheckBox();
		}
		catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Then("^click on download option under Actions for first quote number and clicks on download selected image$")
	public void click_on_download_option_under_Actions_for_the_quote_number_of_and_clicks_on_download_selected_image() throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			mp.clickOnDownloadOption();
			}
		catch (Exception e)
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("^click on the \"([^\"]*)\"$")
	public void click_on_the(String text) throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			System.out.println("text:"+text);
			library.waitForDigitLoad();
			mp.clickOnFirstPolicyNumber(text);
			} 
		catch (Exception e)
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());
		}
	}

	
	@Then("^It should display the Quote details related to the Quote number \"([^\"]*)\"$")
	public void it_should_display_the_Quote_details_related_to_the_Quote_number(String details) throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			String text="Quote Number";
			mp.enterPolicyQuotedetails(environMentName, details, driver,text);
			}
		  catch (Exception e) {
			  final byte[] screenshot = ((TakesScreenshot)library.getDriver()).getScreenshotAs(OutputType.BYTES);
			  Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			  Assert.assertTrue(false, e.getMessage());
			  }
	}
	
	@Then("^click on download option under Actions for the first quote number and clicks on download selected image$")
	public void click_on_download_option_under_Actions_for_the_first_quote_number_and_clicks_on_download_selected_image() throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			library.deletetheFilesInDownloadFiles();
			mp.clickOnDownloadOption();
			}
		catch (Exception e)
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
			Assert.assertTrue(false, e.getMessage());
		}
	}
@When("^user search the quote using registration number \"([^\"]*)\"$")
public void user_search_the_quote_using_registration_number(String details) throws Throwable {
	try {
	com.utilities.Library library = new com.utilities.Library();
	WebDriver driver = library.getDriver();
	MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
	mp.searchDataInMyTransactions(environMentName, details);
	System.out.println("Details : "+details);
	}
	catch (Exception e)
	{
		final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
		Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
		Assert.assertTrue(false, e.getMessage());
	}
}
@Then("^user select the date more than five days \"([^\"]*)\"$")
public void user_select_the_date_more_than_five_days(String details) throws Throwable {
	try {
		com.utilities.Library library = new com.utilities.Library();
		WebDriver driver = library.getDriver();
		MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
		mp.selectDate(details,driver);
		System.out.println("Details : "+details);
		}
	
	catch(Exception e)
	{
		System.out.println("The data is not available");
	}
}

@Then("^it should populate the toaster as \"([^\"]*)\"$")
public void it_should_populate_the_toaster_as(String expToasterMsg) throws Throwable {
	try {
	com.utilities.Library library = new com.utilities.Library();
	WebDriver driver = library.getDriver();
	library.waitForDigitLoad();
	MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
	if(!expToasterMsg.equals(mp.toasterMessage.getText()))
	 throw new Exception("The warning toaster message is not populated appropriately");
	}
	catch(Exception e)
	{
		throw new Exception("The warning toaster message is not populated appropriately");
	}
}



}
