package com.cucumber.framework.stepdefinition;

import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.agent.motor.api.ApproveInspection;
import com.agent.myTransactions.MyTransactionsPage;
import com.agent.transactions.MyTransactions;
import com.cucumber.framework.runner.Hooks;
import com.utilities.Library;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pom.agent.home.Home;
import pom.agent.home.LoginPage;
import pom.agent.motor.DetailsPage;
import pom.agent.motor.FloatReplenishment;
import pom.agent.motor.PaymentPage;
import pom.mobile.preinspection.UploadPhotos;

public class AgentPortal {

	private static com.utilities.Library library = new com.utilities.Library();

	@Given("logged to the application with details \"([^\"]*)\"$")
	public static void login(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			boolean flag = false;
			if (library.getDriver() != null) {
				LoginPage loginobj = PageFactory.initElements(library.getDriver(), LoginPage.class);
				flag = loginobj.moveToHomePage();
			}
			if (!flag) {
				WebDriver driver = library.DriverSetup();
				library.setEnvironment();
				driver.get(Library.staticMap.get("server") + "/DigitPlus/#/login");
				Thread.sleep(5000);
				LoginPage loginobj = PageFactory.initElements(driver, LoginPage.class);
				loginobj.enterDetails(details);
			}
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("close the browser")
	public void closetheBrowser() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			library.getDriver().close();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("module \"([^\"]*)\" is selected")
	public void selectModule(String module) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Home homeobj = PageFactory.initElements(driver, Home.class);
			library.waitForDigitLoad();
			Thread.sleep(6000);
			// homeobj.clickOnAsiaPopup();
			homeobj.selectModule(module);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("vehicle details \"([^\"]*)\" are entered")
	public void enterVehicleDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			DetailsPage detailsobj = PageFactory.initElements(driver, DetailsPage.class);
			detailsobj.enterVehicleDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("previous policy details \"([^\"]*)\" are entered")
	public void enterPreviousPolicyDetails(String details) throws Exception {
		// try {
		com.utilities.Library library = new com.utilities.Library();
		WebDriver driver = library.getDriver();
		DetailsPage detailsobj = PageFactory.initElements(driver, DetailsPage.class);
		detailsobj.enterPreviousPolicyDetails(details);
		Thread.sleep(2000);
		/*
		 * } catch (Exception e) { final byte[] screenshot = ((TakesScreenshot)
		 * library.getDriver()).getScreenshotAs(OutputType.BYTES);
		 * Hooks.scenario.embed(screenshot, "image/png"); Assert.assertTrue(false,
		 * e.getMessage()); }
		 */
	}

	@When("third party policy details \"([^\"]*)\" are entered")
	public void enterTPPolicyDetails(String details) throws Exception {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			DetailsPage detailsobj = PageFactory.initElements(driver, DetailsPage.class);
			detailsobj.enterThirdPartyPolicyDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}

	}

	@When("button \"([^\"]*)\" is clicked")
	public void getQuote(String action) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			DetailsPage detailsobj = PageFactory.initElements(driver, DetailsPage.class);
			detailsobj.performAction(action);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@Then("check if the policies are displayed")
	public void check_policies_are_displayed() {
		try {

		}

		catch (Exception e) {

		}
	}

	@When("^click on download buttun$")
	public void click_on_download_buttun() throws Throwable {
		// try {
		// com.utilities.Library library = new com.utilities.Library();
		// WebDriver driver = library.getDriver();
		// library.deletetheFilesInDownloadFiles();
		// DetailsPage detailsobj = PageFactory.initElements(driver, DetailsPage.class);
		// detailsobj.clickOnFileDownloadIcon();
		// library.IsErrorDisplayed();

		// }

		/*
		 * catch (Exception e) { final byte[] screenshot = ((TakesScreenshot)
		 * library.getDriver()).getScreenshotAs(OutputType.BYTES);
		 * Hooks.scenario.embed(screenshot, "image/png"); Assert.assertTrue(false,
		 * e.getMessage()); }
		 */
	}

	@When("select all the covers")
	public void selectAllCovers() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			DetailsPage detailsobj = PageFactory.initElements(driver, DetailsPage.class);
			detailsobj.selectAllCovers();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("customer details \"([^\"]*)\" are entered")
	public void enterCustomerDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			DetailsPage detailsobj = PageFactory.initElements(driver, DetailsPage.class);
			detailsobj.enterCustomerDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("nominee details \"([^\"]*)\" are entered")
	public void enterNomineeDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			DetailsPage detailsobj = PageFactory.initElements(driver, DetailsPage.class);
			detailsobj.enterNomineeDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("survey details \"([^\"]*)\" are entered")
	public void enterSurveyDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			DetailsPage detailsobj = PageFactory.initElements(driver, DetailsPage.class);
			detailsobj.enterSurveyDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("enter payment details")
	public void enterPaymentDetails() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			DetailsPage detailsobj = PageFactory.initElements(library.getDriver(), DetailsPage.class);
			detailsobj.enterPaymentDetails();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("upload the previous policy document")
	public void uploadDocument() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			DetailsPage detailsobj = PageFactory.initElements(library.getDriver(), DetailsPage.class);
			detailsobj.uploadDocument();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("verify (policy|quote) issuance is successful for (Two Wheeler|Four Wheeler|Commercial Vehicle)")
	public void verifySuccessPage(String policyStatus, String vehicleType) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			DetailsPage detailsobj = PageFactory.initElements(library.getDriver(), DetailsPage.class);
			System.out.println("VehicleType :" + vehicleType);
			detailsobj.verifySuccessPage(policyStatus, vehicleType);

			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("upload the preinspection images")
	public void uploadPreInspectionImages() throws IOException {
		try {
			UploadPhotos obj = new UploadPhotos();
			obj.getLinkAndUploadImages();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("open the policy with \"([^\"]*)\"")
	public void openPolicy(String details) throws IOException {
		try {
			if (details.contains("[Policy_Number]")) {
				details = details.replaceAll("\\[Policy_Number\\]", Library.staticMap.get("Policy Number"));
			}
			com.utilities.Library library = new com.utilities.Library();
			MyTransactions detailsobj = PageFactory.initElements(library.getDriver(), MyTransactions.class);
			System.out.println("Method is called");
			detailsobj.openPolicy(details);
			System.out.println("Method is returned");
		} catch (Exception e) {
			e.printStackTrace();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@And("enter \"([^\"]*)\" in amount to be added")
	public void enterAmount(String details) throws Exception {
		try {
			com.utilities.Library library = new com.utilities.Library();
			FloatReplenishment detailsobj = PageFactory.initElements(library.getDriver(), FloatReplenishment.class);
			detailsobj.checkPayBtnIsEnabled();
			detailsobj.enterAmountToBeAdded_AndClickOnPayButton(details);
		} catch (Exception e) {
			e.printStackTrace();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@And("choose payment type as \"([^\\\"]*)\" and enter details")
	public void enterCardDetails(String details) throws Exception {
		try {
			com.utilities.Library library = new com.utilities.Library();
			PaymentPage paymentObj = PageFactory.initElements(library.getDriver(), PaymentPage.class);
			paymentObj.enterCardDetails(library.getDriver());
		} catch (Exception e) {
			e.printStackTrace();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^verify Success Message from toast$")
	public void verify_Success_Message_from_toast() throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			PaymentPage paymentObj = PageFactory.initElements(library.getDriver(), PaymentPage.class);
			paymentObj.verifySuccessMessageFromToast();
		} catch (Exception e) {
			e.printStackTrace();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@And("^verify the policyStatus$")
	public void verify_policy_status() throws Exception {
		try {
			System.out.println("going to verify policy status");
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			library.IsErrorDisplayed();
			MyTransactionsPage mp = PageFactory.initElements(driver, MyTransactionsPage.class);
			mp.getPolicyStatus();
		} catch (Exception e) {
			e.printStackTrace();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	
	@And("^update the survey comments for the quote created$")
	public void updateSurveyComments() throws Exception {
		try {
			ApproveInspection app = new ApproveInspection();
			app.approvePreInspection();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.assertTrue(false, e.getMessage());
		}
	}

	
	@When("verify the policy has moved to \"([^\"]*)\" state in Agent transactions page")
	public void validateSuccessPage(String status) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			MyTransactionsPage homeobj = PageFactory.initElements(driver, MyTransactionsPage.class);
			homeobj.verifyPolicyStatus(status);
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
		} catch (Exception e) {
			Library.softassert.assertAll();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("make the online payment")
	public void makeOnlinePayment() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			library.performOnlinePayment();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
		} catch (Exception e) {
			Library.softassert.assertAll();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
