package com.cucumber.framework.stepdefinition;

import com.cucumber.framework.runner.Hooks;
import com.utilities.Library;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pom.agent.empcomp.EmpCompPage;

public class EmpCompSteps {
    Library library = new Library();
    WebDriver driver = library.getDriver();
    int quickQuotePremium, createUpdateQuotePremium;
    EmpCompPage empcomp = PageFactory.initElements(driver, EmpCompPage.class);

    @When("^policy details \"([^\"]*)\" are entered in the Employee Compensation page$")
    public void policy_details_are_entered_in_Employee_Compensation_page(String policyDetails) throws Throwable {
        try {
            empcomp.enterPolicyDetails(policyDetails);
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @When("^work description details \"([^\"]*)\" are entered in the Employee Compensation page$")
    public void work_description_details_are_entered_in_Employee_Compensation_page(String workDescDetails) throws Throwable {
        try {
            empcomp.enterWorkDescDetails(workDescDetails);
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @When("^previous insurer details \"([^\"]*)\" are entered in the Employee Compensation page$")
    public void previous_insurer_details_are_entered_in_Employee_Compensation_page(String previousInsurerDetails) throws Throwable {
        try {
            empcomp.enterPreviousInsurerDetails(previousInsurerDetails);
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @When("^clicks on the \"([^\"]*)\" button in the Employee Compensation page$")
    public void clicks_on_the_button_in_the_Employee_Compensation_page(String action) throws Throwable {
        try {
            empcomp.performAction(action);

            switch (action.replaceAll(" ", "").toLowerCase()) {
                case "showpremium":
                case "apply":
                    quickQuotePremium = empcomp.getPremium();
                    break;
                case "savequote":
                    createUpdateQuotePremium = empcomp.getPremium();
            }
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @When("^enters the discount or loading details \"([^\"]*)\" in the Employee Compensation page$")
    public void enters_the_discount_or_loading_details_in_the_Employee_Compensation_page(String discount) throws Throwable {
        try {
            empcomp.enterDiscountLoadingDetails(discount);
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @When("^the customer details \"([^\"]*)\" are entered in the Employee Compensation page$")
    public void the_customer_details_are_entered_in_the_Employee_Compensation_page(String customerDetails) throws Throwable {
        try {
            empcomp.enterCustomerDetails(customerDetails);
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @When("^risk location details \"([^\"]*)\" are entered in the in the Employee Compensation page$")
    public void risk_location_details_are_entered_in_the_in_the_Employee_Compensation_page(String riskLocDetails) throws Throwable {
        try {
            empcomp.enterRiskLocationDetails(riskLocDetails);
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @When("^communication details \"([^\"]*)\" are entered in the Employee Compensation page$")
    public void communication_details_are_entered_in_the_Employee_Compensation_page(String commLocDetails) throws Throwable {
        try {
            empcomp.enterCommAddressDetails(commLocDetails);
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @Then("^verify the quote number and the quote download option are visible in the footer of the Employee Compensation page$")
    public void verify_the_quote_number_and_the_quote_download_option_are_visible_in_the_footer_of_the_Employee_Compensation_page() throws Throwable {
        Assert.assertTrue(empcomp.visibilityOfQuoteNo());
        Assert.assertTrue(empcomp.visibilityOfProposalDownload());
    }

    @Then("^verify if the quick quote premium for Employee Compensation matches with the create quote premium$")
    public void verify_if_the_quick_quote_premium_for_Employee_Compensation_matches_with_the_create_quote_premium() throws Throwable {
        Assert.assertEquals(quickQuotePremium, createUpdateQuotePremium, "Premium mismatch observed on creating the quote");
    }

    @When("^selects \"([^\"]*)\" as the answer to the question - 'This update will be the final update for this quote\\. Do you want to proceed\\?' in the Employee Compensation page$")
    public void selects_as_the_answer_to_the_question_This_update_will_be_the_final_update_for_this_quote_Do_you_want_to_proceed_in_the_Employee_Compensation_page(String choice) throws Throwable {
        try {
            choice = choice.trim().toLowerCase();

            switch (choice) {
                case "yes":
                    empcomp.clickOnYesForFinalUpdate();
                    break;
                case "no":
                    empcomp.clickOnNoForFinalUpdate();
                    break;
                default:
                    throw new Exception("Proceed for final update has two options 'Yes' and 'No', given: [" + choice + "]");
            }
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @When("^payment details \"([^\"]*)\" are entered in the Employee Compensation page$")
    public void payment_details_are_entered_in_the_Employee_Compensation_page(String payentDetails) throws Throwable {
        try {
            empcomp.enterPaymentDetails(payentDetails);
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @Then("^verify the success page for Employee Compensation$")
    public void verify_the_success_page_for_Employee_Compensation() throws Throwable {
        Assert.assertTrue(empcomp.verifySuccessPage());
    }
}
