package com.cucumber.framework.stepdefinition;

import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.agent.motor.api.CreateBreakinIntegrated;
import com.agent.motor.api.CreateBreakinPolicy;
import com.cucumber.framework.runner.Hooks;
import com.utilities.Library;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import pom.digitcare.login.Approvals;
import pom.digitcare.login.Claims;
import pom.digitcare.login.Home;
import pom.digitcare.login.Login;

public class DigitCare {

	private static com.utilities.Library library = new com.utilities.Library();
	
	@Given("logged to the digitcare with details \"([^\"]*)\"$")
	public static void login(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.DriverSetup();
			driver.get("http://uat-digitcareui.godigit.com/#/login");
			Thread.sleep(10000);
			Login loginobj = PageFactory.initElements(driver, Login.class);
			loginobj.enterDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("move to tab \"([^\"]*)\" in digitcare")
	public static void moveToTab(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Thread.sleep(5000);
			Home homeObj = PageFactory.initElements(driver, Home.class);
			homeObj.movetoTab(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("search with details \"([^\"]*)\" in Approvals tab")
	public static void searchWithDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			//details = library.getParamValue(details);
			System.out.println(Library.staticMap);
			if(details.contains("[Policy_Number]"))
			{
				details = details.replaceAll("\\[Policy_Number\\]", Library.staticMap.get("Policy Number"));
			}
			WebDriver driver = library.getDriver();
			Thread.sleep(2000);
			Approvals approvalObj = PageFactory.initElements(driver, Approvals.class);
			approvalObj.searchWithDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	
	@When("verify the count of records as \"([^\"]*)\" in Approvals tab")
	public static void verifyCountofRecords(String count) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Thread.sleep(2000);
			Approvals approvalObj = PageFactory.initElements(driver, Approvals.class);
			approvalObj.verifyCountOfRecords(count);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("verify the count of images as \"([^\"]*)\" in the inspection details page")
	public static void verifyCountofImages(String count) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Thread.sleep(2000);
			Approvals approvalObj = PageFactory.initElements(driver, Approvals.class);
			approvalObj.verifyImagesCountInInspection(count);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("click on the Image search at row \"([^\"]*)\"")
	public static void openImageSearch(String rowNumber) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Thread.sleep(2000);
			Approvals approvalObj = PageFactory.initElements(driver, Approvals.class);
			approvalObj.openImageSearch(rowNumber);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("verify the section of \"([^\"]*)\" for Private Car")
	public static void verifyPrivateCarImageStatus(String section) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Thread.sleep(2000);
			Approvals approvalObj = PageFactory.initElements(driver, Approvals.class);
			approvalObj.verifyFourWheelerDamageOptions(section);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	
	@When("verify the section of \"([^\"]*)\" of \"([^\"]*)\" for Commercial Vehicle")
	public static void verifyCommercialImageStatus(String section,String type) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Thread.sleep(2000);
			Approvals approvalObj = PageFactory.initElements(driver, Approvals.class);
			approvalObj.verifyCommercialDamageOptions(section, type);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("open the image \"([^\"]*)\" in Inspection details")
	public static void openImage(String imageName) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Thread.sleep(2000);
			Approvals approvalObj = PageFactory.initElements(driver, Approvals.class);
			approvalObj.openImage(imageName);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	
	@When("close the image for Private Car")
	public static void closeImage() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			//Thread.sleep(2000);
			Approvals approvalObj = PageFactory.initElements(driver, Approvals.class);
			approvalObj.closeImage();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	
	@When("close the inspection details window")
	public static void closeInspectionWindow() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Approvals approvalObj = PageFactory.initElements(driver, Approvals.class);
			approvalObj.closeInspectionWindow();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("assert the preinspection section")
	public static void claims_assertPreinspectionImages() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Approvals approvalObj = PageFactory.initElements(driver, Approvals.class);
			approvalObj.assertErrorList();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("create a breakin policy for \"([^\"]*)\" and do the preinspection")
	public static void createPolicyAndInspect(String vehicleType) throws IOException {
		try {
			CreateBreakinPolicy obj = new CreateBreakinPolicy();
			obj.createBreakinPolicy(vehicleType);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("create a \"([^\"]*)\" policy for \"([^\"]*)\" in \"([^\"]*)\" environment and do the preinspection")
	public static void createPolicyAndInspect(String policyType,String vehicleType,String environment) throws IOException {
		try {
			CreateBreakinIntegrated obj = new CreateBreakinIntegrated();
			obj.createPolicy(policyType, vehicleType, environment,true);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("create a \"([^\"]*)\" policy for \"([^\"]*)\" in \"([^\"]*)\" environment without preinspection")
	public static void createPolicyWithoutInspect(String policyType,String vehicleType,String environment) throws IOException {
		try {
			CreateBreakinIntegrated obj = new CreateBreakinIntegrated();
			obj.createPolicy(policyType, vehicleType, environment,false);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("cre jnkate a \"([^\"]*)\" policy for \"([^\"]*)\" in \"([^\"]*)\" environment")
	public static void createPolicy(String policyType,String vehicleType,String environment) throws IOException {
		try {
			CreateBreakinIntegrated obj = new CreateBreakinIntegrated();
			obj.createPolicy(policyType, vehicleType, environment,false);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("perform \"([^\"]*)\" action in the Inspection details screen")
	public static void performActionInInspectionDetails(String action) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Thread.sleep(2000);
			Approvals approvalObj = PageFactory.initElements(driver, Approvals.class);
			approvalObj.performActionInInspectionDetails(action);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
		      Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("search with details \"([^\"]*)\" in Search Policy/Claim tab")
	public static void searchPolicyClaim(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			if(details.contains("[Policy_Number]"))
			{
				details = details.replaceAll("\\[Policy_Number\\]", Library.staticMap.get("PolicyNumber"));
			}
			WebDriver driver = library.getDriver();
			Claims claimsObj = PageFactory.initElements(driver, Claims.class);
			claimsObj.searchWithDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("perform \"([^\"]*)\" action for row \"([^\"]*)\" in Search Policy/Claim tab")
	public static void claims_performAction(String action,String row) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Claims claimsObj = PageFactory.initElements(driver, Claims.class);
			claimsObj.performActionInSearchTable(action, row);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("enter the reporter details as \"([^\"]*)\" in Claims creation page")
	public static void claims_reporterDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Claims claimsObj = PageFactory.initElements(driver, Claims.class);
			claimsObj.enterReporterDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("enter the loss details as \"([^\"]*)\" in Claims creation page")
	public static void claims_lossDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Claims claimsObj = PageFactory.initElements(driver, Claims.class);
			claimsObj.enterLossDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("enter the claims details as \"([^\"]*)\" in Claims creation page")
	public static void claims_claimsDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Claims claimsObj = PageFactory.initElements(driver, Claims.class);
			claimsObj.enterClaimsDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@When("enter the enquiry details as \"([^\"]*)\" in Claims creation page")
	public static void claims_enquiryDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Claims claimsObj = PageFactory.initElements(driver, Claims.class);
			claimsObj.enterEnquiryDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("perform \"([^\"]*)\" action in Claims creation page")
	public static void claims_performAction(String action) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Claims claimsObj = PageFactory.initElements(driver, Claims.class);
			claimsObj.performAction(action);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	
	@When("accept the confirmation popup in Claims creation page")
	public static void claims_acceptConfirmation() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Claims claimsObj = PageFactory.initElements(driver, Claims.class);
			claimsObj.closeClaimCreationPopup();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	
	@When("approve the preinspection")
	public static void claims_approvePreinspection() throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Approvals obj = PageFactory.initElements( driver,Approvals.class);
			obj.approveTheImages();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());
		}
	}
	

}
