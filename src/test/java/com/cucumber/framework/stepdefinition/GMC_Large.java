package com.cucumber.framework.stepdefinition;

import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.cucumber.framework.runner.Hooks;
import com.utilities.Library;
import com.utilities.PropertyFileReader;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import pom.agent.gmc.CreateQuote;
import pom.agent.home.Home;
import pom.agent.gmc.Login;
import pom.agent.gmc.NewCustomer;
import pom.agent.home.LoginPage;

public class GMC_Large {

	private static com.utilities.Library library = new com.utilities.Library();
	WebDriver driver;

	@Given("^Login into the application for GMC Large$")
	public void login_into_the_application_for_GMC_Large() throws Throwable {

		try {
			PropertyFileReader read = new PropertyFileReader();

			driver = library.DriverSetup();
			//Set Environment
			library.setExecutionEnvironment(read.getEnvironment_For_GMC_Large());
			driver.get(Library.staticMap.get("server") + "/DigitPlus/#/login");

			String credentials = "UserName:="+read.getGMCSalesPreprodHealthPlusUserName()+" # Password:="+read.getGMCSalesPreprodHealthPlusPassword();
			LoginPage loginPageObj = PageFactory.initElements(driver, LoginPage.class);
			loginPageObj.enterDetails(credentials);
			
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^Clicked on module \"([^\"]*)\" in agent portal$")
	public void clicked_on_module_in_agent_portal(String arg1) throws Throwable {
	    
		try {
			Home home = PageFactory.initElements(driver, Home.class);
			try {
				library.waitForDigitLoad();
				home.getClosePopup(library.getDriver());
			}catch(Exception e) {
				System.out.println("No popup present in home page this time.");
			}
			home.selectSubModuleFromAgentPortal(arg1);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^create new policy$")
	public void create_new_policy() throws Throwable {
	   
		try {
			CreateQuote cq =  PageFactory.initElements(library.getDriver(), CreateQuote.class);
			cq.getCreateNewPolicy();
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
		
	}

	@When("^provide insured details for GMC Large from \"([^\"]*)\"$")
	public void provide_insured_details_from(String gmcSalesId) throws Throwable {
	    
		try {
			CreateQuote cq =  PageFactory.initElements(driver, CreateQuote.class);
			cq.fillInsuredDetailsGMCLarge(gmcSalesId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^provide prospective policy details for GMC Large from \"([^\"]*)\"$")
	public void provide_prospective_policy_details_from(String gmcSalesId) throws Throwable {

		try {
			CreateQuote cq =  PageFactory.initElements(driver, CreateQuote.class);
			cq.fillProspectiveDetailsGMCLarge(gmcSalesId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^provide coverage details for GMC Large from \"([^\"]*)\"$")
	public void provide_coverage_details_from(String gmcSalesId) throws Throwable {

		try {
			CreateQuote cq =  PageFactory.initElements(driver, CreateQuote.class);
			cq.fillCoverageDetailsGMCLarge(gmcSalesId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^provide additional coverage details for GMC Large from \"([^\"]*)\"$")
	public void provide_additional_coverage_details_from(String gmcSalesId) throws Throwable {

		try {
			CreateQuote cq =  PageFactory.initElements(driver, CreateQuote.class);
			cq.fillAdditionalCoveargesGMCLarge(gmcSalesId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^provide premium details for GMC Large from \"([^\"]*)\"$")
	public void provide_premium_details_from(String gmcSalesId) throws Throwable {

		try {
			CreateQuote cq =  PageFactory.initElements(driver, CreateQuote.class);
			cq.fillPremiumDetailsGMCLarge(gmcSalesId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^provide demography details for GMC Large from \"([^\"]*)\"$")
	public void provide_demography_details_from(String gmcSalesId) throws Throwable {

		try {
//			CreateQuote cq =  PageFactory.initElements(driver, CreateQuote.class);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^provide document details for GMC Large from \"([^\"]*)\"$")
	public void provide_document_details_from(String gmcSalesId) throws Throwable {

		try {
			CreateQuote cq =  PageFactory.initElements(driver, CreateQuote.class);
			cq.fillDocumentDetailsGMCLarge(gmcSalesId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^click on \"([^\"]*)\" buttton for \"([^\"]*)\"$")
	public void click_on_buttton(String button,String gmcSalesId) throws Throwable {

		try {
			CreateQuote cq =  PageFactory.initElements(library.getDriver(), CreateQuote.class);
			cq.clickOnButton(button,gmcSalesId);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
