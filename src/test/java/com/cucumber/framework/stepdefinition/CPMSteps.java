package com.cucumber.framework.stepdefinition;

import com.cucumber.framework.runner.Hooks;
import com.utilities.Library;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import pom.agent.cpm.CPMPage;

public class CPMSteps {
    Library library = new Library();
    WebDriver driver = library.getDriver();
    int quickQuotePremium, createUpdateQuotePremium;
    CPMPage cpm = PageFactory.initElements(driver, CPMPage.class);

    @When("^the following policy details are entered in the CPM page$")
    public void the_following_policy_details_are_entered_in_the_CPM_page(DataTable policyDetails) throws Throwable {
        try {
            cpm.enterPolicyDetails(policyDetails);
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @When("^the following machinery details are entered in the CPM page$")
    public void the_following_machinery_details_are_entered_in_the_CPM_page(DataTable machineryDetails) throws Throwable {
        try {
            cpm.enterMachineryDetails(machineryDetails);
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @When("^the following location of machinery details are entered in the CPM page$")
    public void the_following_location_of_machinery_details_are_entered_in_the_CPM_page(DataTable locOfMachineryDetails) throws Throwable {
        try {
            cpm.enterLocOfMachineryDetails(locOfMachineryDetails);
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @When("^the following coverages details are entered in the CPM page$")
    public void the_following_coverages_details_are_entered_in_the_CPM_page(DataTable coverDetails) throws Throwable {
        try {
            cpm.enterCoveragesDetails(coverDetails);
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @When("^the following previous insurer details are entered in the CPM page$")
    public void the_following_previous_insurer_details_are_entered_in_the_CPM_page(DataTable previousPolicyDetails) throws Throwable {
        try {
            cpm.enterPrevPolicyDetails(previousPolicyDetails);
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @When("^clicks on the \"([^\"]*)\" button in the CPM page$")
    public void clicks_on_the_button_in_the_cpm_page(String action) throws Throwable {
        try {
            cpm.performAction(action);

            switch (action.replaceAll(" ", "").toLowerCase()) {
                case "showpremium":
                case "apply":
                    quickQuotePremium = cpm.getPremium();
                    break;
                case "savequote":
                    createUpdateQuotePremium = cpm.getPremium();
            }
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @When("^the following discount/loading detail is entered in the CPM page$")
    public void the_following_discount_loading_detail_is_entered_in_the_CPM_page(DataTable disLoadDetails) throws Throwable {
        try {
            cpm.enterDiscountLoadingDetails(disLoadDetails);
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @When("^the following document details are entered in the CPM page$")
    public void the_following_document_details_are_entered_in_the_CPM_page(DataTable docDetails) throws Throwable {
        try {
            cpm.enterDocumentDetails(docDetails);
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @When("^the following customer details are entered in the CPM page$")
    public void the_following_customer_details_are_entered_in_the_CPM_page(DataTable custDetails) throws Throwable {
        try {
            cpm.enterCustomerDetails(custDetails);
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @Then("^verify the quote number and the quote download option are visible in the footer of the CPM page$")
    public void verify_the_quote_number_and_the_quote_download_option_are_visible_in_the_footer_of_the_CPM_page() throws Throwable {
        Assert.assertTrue(cpm.visibilityOfQuoteNo());
        Assert.assertTrue(cpm.visibilityOfProposalDownload());
    }

    @Then("^verify if the quick quote premium for CPM matches with the create quote premium$")
    public void verify_if_the_quick_quote_premium_for_CPM_matches_with_the_create_quote_premium() throws Throwable {
        Assert.assertEquals(quickQuotePremium, createUpdateQuotePremium, "Premium mismatch observed on creating the quote");
    }

    @When("^selects \"([^\"]*)\" as the answer to the question - 'This update will be the final update for this quote\\. Do you want to proceed\\?' in the CPM page$")
    public void selects_as_the_answer_to_the_question_This_update_will_be_the_final_update_for_this_quote_Do_you_want_to_proceed_in_the_CPM_page(String choice) throws Throwable {
        try {
            choice = choice.trim().toLowerCase();

            switch (choice) {
                case "yes":
                    cpm.clickOnYesForFinalUpdate();
                    break;
                case "no":
                    cpm.clickOnNoForFinalUpdate();
                    break;
                default:
                    throw new Exception("Proceed for final update has two options 'Yes' and 'No', given: [" + choice + "]");
            }
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @When("^the folllowing payment details are entered in the CPM page$")
    public void the_folllowing_payment_details_are_entered_in_the_CPM_page(DataTable paymentDetails) throws Throwable {
        try {
            cpm.enterPaymentDetails(paymentDetails);
        } catch (Exception e) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Hooks.scenario.embed(screenshot, "image/png");
            Assert.assertTrue(false, e.getMessage());
        }
    }

    @Then("^verify the success page for CPM$")
    public void verify_the_success_page_for_CPM() throws Throwable {
        Assert.assertTrue(cpm.verifySuccessPage());
    }
}
