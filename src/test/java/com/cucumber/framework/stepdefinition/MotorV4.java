package com.cucumber.framework.stepdefinition;

import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.agent.motorv4.DetailsPage;
import com.cucumber.framework.runner.Hooks;

import cucumber.api.java.en.When;

public class MotorV4 {

	private static com.utilities.Library library = new com.utilities.Library();

	@When("motor v4 vehicle details \"([^\"]*)\" are entered")
	public void enterVehicleDetails(String details) throws IOException {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			DetailsPage detailsobj = PageFactory.initElements(driver, DetailsPage.class);
			detailsobj.enterVehicleDetails(details);
		} catch (Exception e) {
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
