package com.cucumber.framework.stepdefinition;

import java.io.IOException;
import java.util.HashMap;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.agent.jewellery.CustomerDetailsPage;
import com.agent.jewellery.JewelleryDetailaPage;
import com.agent.jewellery.SuccessPage;
import com.cucumber.framework.runner.Hooks;
import com.utilities.Library;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pom.agent.home.Home;
import pom.agent.home.LoginPage;

public class Jwellery {

	static Library library = new Library();
	static WebDriver driver = library.getDriver();
	static JewelleryDetailaPage jp;
	static CustomerDetailsPage cp;
	SuccessPage sp;

	/*
	 * @Given("user login to the application with details") public static void
	 * login() throws IOException, InterruptedException { try {
	 * 
	 * driver = library.DriverSetup(); library.setEnvironment();
	 * System.out.println(Library.staticMap); System.out.println("URL : " +
	 * Library.staticMap.get("server") + "/DigitPlus/#/login");
	 * driver.get(Library.staticMap.get("server") + "/DigitPlus/#/login");
	 * Thread.sleep(5000); LoginPage loginobj = PageFactory.initElements(driver,
	 * LoginPage.class); loginobj.enterLoginCredencials(); } catch (Exception e) {
	 * final byte[] screenshot = ((TakesScreenshot)
	 * library.getDriver()).getScreenshotAs(OutputType.BYTES);
	 * Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the
	 * report. Assert.assertTrue(false, e.getMessage());
	 * 
	 * }
	 * 
	 * }
	 */

	@When("^user selects \"([^\"]*)\" module$")
	public void user_selects_module(String module) throws Throwable {
		try
		{
		Home hp = PageFactory.initElements(driver, Home.class);
		hp.selectModule(module);
		}
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());

		}
	}

	@And("^enters all the details \"([^\"]*)\"$")
	public void enters_all_the_details(String details) throws Throwable {
		try 
		{
		System.out.println("Driver objet is : " + driver);
		jp = PageFactory.initElements(driver, JewelleryDetailaPage.class);
		jp.enterAllThedetails(details);
		}
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());

		}

	}

	@And("^user clicks on addItem button and getQuote button$")
	public void user_clicks_on_addItem_button_and_getQuote_button() throws InterruptedException {
		try {
		jp.getAddItemButton();
		jp.getQuoteButton();
		}
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());

		}
	}

	@Then("^\"([^\"]*)\" popup should be displayed$")
	public void popup_should_be_displayed(String expectedconfirmPaymentHeader) throws Throwable {
		try 
		{
		library.waitForElementVisible(JewelleryDetailaPage.confirmPaymentPopupHeader, 20, 3);
		String actualConfirmPaymentHeader = jp.getconfirmPaymentPopupHeader();
		Assert.assertEquals(actualConfirmPaymentHeader, expectedconfirmPaymentHeader);
		}
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());

		}
	}

	@When("^user clicks on Buy Now option in confirm payment popup$")
	public void user_clicks_on_Buy_Now_option_in_confirm_payment_popup() throws Throwable {
		try 
		{
		jp.clickOnBuyNowBtn();
		}
		catch (Exception e)
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());

		}
	}

	@Then("^\"([^\"]*)\" page should be displayed$")
	public void page_should_be_displayed(String expectedCustomerDEtailsFrameTitle) throws Throwable {
		try 
		{
		cp = PageFactory.initElements(driver, CustomerDetailsPage.class);
		library.waitForElementVisible(CustomerDetailsPage.customerDEtailsFrameTitle, 20, 5);
		String ActualCustomerDEtailsFrameTitle = cp.customerDEtailsFrameTitle();
		System.out.println("ActualCustomerDEtailsFrameTitle:" + ActualCustomerDEtailsFrameTitle);
		Assert.assertEquals(ActualCustomerDEtailsFrameTitle, expectedCustomerDEtailsFrameTitle);
		}
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());

		}

	}

	@And("^user enters all the details \"([^\"]*)\"$")
	public void user_enters_all_the_details(String details) throws Throwable {
		try {
		cp.enterCustomerDetails(details);
		}
		catch (Exception e)
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());

		}

	}

	@And("^Upload Take picture and Upload Invoice Photo and Upload Acknowledgement Photo$")
	public void upload_Upload_Take_picture_and_Upload_Invoice_Photo_and_Upload_Acknowledgement_Photo_and_clicks_on_proceed_button() throws Throwable {
		try 
		{
		cp.uploadJwelleryDocuments();
		}
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());

		}
	}

	@Then("^All the images to be uploaded \"([^\"]*)\"$")
	public void all_the_images_to_be_uploaded(String details) throws Throwable {
		try 
		{
		cp.verifyTheUploadedImages(details);
		}
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());

		}
	}

	@And("^clicks on proceed button$")
	public void clicks_on_proceed_button() {
		try 
		{
		cp.clickOnproceedBtn();
		}
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());

		}

	}

	@Then("^verify the success page with the success message \"([^\"]*)\"$")
	public void policy_should_be_issued(String successMsg) throws Throwable {
		try {
		sp = PageFactory.initElements(driver, SuccessPage.class);
		library.waitForElementVisible(sp.policyNumber, 50, 5);
		String policyNumber = sp.getPolicyNumber();
		Library.staticMap.put("Policy Number", policyNumber);
		System.out.println(sp.getsuccessMsg());
		Assert.assertTrue(sp.getsuccessMsg().contains(successMsg));
		}
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());

		}

	}
	
	@And("^click on download button$")
	public void click_on_download_buttond() throws Throwable {
		try 
		{
		sp.clickOnDownloadPolicyButton();
		}
		catch (Exception e) 
		{
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png"); // ... and embed it in the report.
			Assert.assertTrue(false, e.getMessage());

		}
	}

}
