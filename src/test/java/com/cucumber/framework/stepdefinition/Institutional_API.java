package com.cucumber.framework.stepdefinition;

import cucumber.api.java.en.Given;
import institutional_api.KS_CreateQuoteAndIssuepolicy;
import institutional_api.MobileCreateQuoteAndIssuePolicy;
import institutional_api.PropertyABSQuoteAndIssue;

public class Institutional_API {

	
	@Given("^test Mobile Create Quote and ABS Mobile Issue Policy API$")
	public void test_Mobile_Create_Quote_and_ABS_Mobile_Issue_Policy_API() throws Throwable {
	   
		MobileCreateQuoteAndIssuePolicy obj = new MobileCreateQuoteAndIssuePolicy();
		obj.testAPI();
	}
	
	@Given("^test KS Create Quote and KS Issue Policy API$")
	public void test_KS_Create_Quote_and_KS_Issue_Policy_API() throws Throwable {
	   
		KS_CreateQuoteAndIssuepolicy obj = new KS_CreateQuoteAndIssuepolicy();
		obj.testAPI();
	}

	@Given("^test Property ABS Quote and Property ABS Issue API$")
	public void test_Property_ABS_Quote_and_Property_ABS_Issue_API() throws Throwable {
	    
		PropertyABSQuoteAndIssue obj = new PropertyABSQuoteAndIssue();
		obj.testAPI();
	}
}
