package com.cucumber.framework.stepdefinition;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.cucumber.framework.runner.Hooks;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import pom.agent.motor.LoaderPage;

public class LoaderSD {

	// final static Logger log = Logger.getLogger(Loader.class);

	private static com.utilities.Library library = new com.utilities.Library();

	@When("verify the page and select \"([^\\\"]*)\" from the dropdown")
	public void selectDropdown(String motorType) {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			LoaderPage loaderobj = PageFactory.initElements(driver, LoaderPage.class);
			loaderobj.selectMotorType(motorType);
		} catch (Exception e) {
			e.printStackTrace();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("upload \"([^\"]*)\" excel sheet")
	public void uploadSheet(String sheetType) throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			if(sheetType.equals("Lead_Generator"))
			{
				System.out.println("Uploading sheet");
				LoaderPage loaderobj = PageFactory.initElements(driver, LoaderPage.class);
				loaderobj.uploadExcelSheet(sheetType);
			}
			else {
			LoaderPage loaderobj = PageFactory.initElements(driver, LoaderPage.class);
			loaderobj.uploadExcelSheet(sheetType);
			Thread.sleep(3000);
			loaderobj.scrollDown();
			}
		} catch (Exception e) {
			e.printStackTrace();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@When("^edit changes for \"([^\"]*)\"$")
	public void edit_changes_for(String VehicleType) throws Throwable {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			LoaderPage loaderobj = PageFactory.initElements(driver, LoaderPage.class);
			loaderobj.editVehicleDetails(VehicleType);
		} catch (Exception e) {
			e.printStackTrace();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@And("click on QuickQuote button")
	public void clickQuickQuotebtn() {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			LoaderPage loaderobj = PageFactory.initElements(driver, LoaderPage.class);
			loaderobj.clickQuickQuotebtn();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
		} catch (Exception e) {
			e.printStackTrace();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@And("^click on GetQuote button$")
	public void clickGetQuotebutton() {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			LoaderPage loaderobj = PageFactory.initElements(driver, LoaderPage.class);
			loaderobj.clickGetQuotebtn();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
		} catch (Exception e) {
			e.printStackTrace();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

	@And("^click on IssuePolicy button$")
	public void clickIssuePolicyButton() {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			LoaderPage loaderobj = PageFactory.initElements(driver, LoaderPage.class);
			loaderobj.clickIssuePolicyButton();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
		} catch (Exception e) {
			e.printStackTrace();
			final byte[] screenshot = ((TakesScreenshot) library.getDriver()).getScreenshotAs(OutputType.BYTES);
			Hooks.scenario.embed(screenshot, "image/png");
			Assert.assertTrue(false, e.getMessage());
		}
	}

}
