package pom.rap;

import java.util.HashMap;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;


public class Search {

	@FindBy(xpath="//div[@testid='suche_polnr']/input")
	private WebElement txtPolicyNo;
	
	@FindBy(xpath="//div[@testid='suche_kfzkz']/input")
	private WebElement txtRegistrationNo;
	
	@FindBy(xpath="//div[@testid='suche_vbnr']/input")
	private WebElement txtICNo;
	
	@FindBy(xpath="//div[@testid='suche_polnr_vorsys_vertrag']/input")
	private WebElement txtLegSystemPolicyNo;
	
	@FindBy(xpath="//div[@testid='suche_aktennr']/input")
	private WebElement txtRestronTransno;
	
	@FindBy(xpath="//div[@testid='suche_flottennr']/input")
	private WebElement txtFleetNo;
	
	@FindBy(xpath="//div[@testid='suche_kumulnr']/input")
	private WebElement txtAccumNo;
	
	@FindBy(xpath="//div[@testid='suche_mobileNumber']/input")
	private WebElement txtMobileNumber;
	
	@FindBy(xpath="//div[@testid='suche_frpolnr']/input")
	private WebElement txt3rdptyPolicyNo;
	
	@FindBy(xpath="//div[@testid='suche_darlehensnr']/input")
	private WebElement txtLoanNo;
	
	@FindBy(xpath="//div[text()='Quote/Application']/../div")
	private WebElement chkQuoteApplication;
	
	@FindBy(xpath="//div[text()='Completed app'][span[text()='l']][text()='ic.']/../div")
	private WebElement chkCompletedApplication;
	
	@FindBy(xpath="//div[text()='Contr'][span[text()='a']][text()='ct']/../div")
	private WebElement chkContract;
	
	@FindBy(xpath="//div[text()='e'][span[text()='f']][text()='fective']/../div")
	private WebElement chkEffective;
	
	@FindBy(xpath="//div[text()='c'][span[text()='a']][text()='ncelled']/../div")
	private WebElement chkCancelled;
	
	@FindBy(xpath="//div[text()='fr'][span[text()='e']][text()='e of prem.']/../div")
	private WebElement chkFreeofPrem;
	
	@FindBy(xpath="//div[text()='sus'][span[text()='p']][text()='ended']/../div")
	private WebElement chkSuspended;
	
	@FindBy(xpath="//div[@testid='cb_suchen']")
	private WebElement btnSearch;
	
	@FindBy(xpath="//div[@testid='cb_loesche']")
	private WebElement btnResetSearchCriteria;
	
	@FindBy(xpath="//div[@testid='dw_ergebnisliste_vertrag']/div/div/div")
	private WebElement lblFirstRecord;
	//div[@testid='dw_ergebnisliste_vertrag']/div/div/div
	
	@FindBy(xpath="//div[text()='Search contract <on ALLIANZ>']/..//following-sibling::div/div[contains(@style,'730a841e')]")
	private WebElement btnMaximize;
	
	@FindBy(xpath="//div[@testid='cb_ok']")
	private WebElement btnOK;
	
	@FindBy(xpath="//div[@testid='buttonOk']")
	private WebElement btnOKFileSelection;
	
	
	private com.utilities.Library library = new com.utilities.Library();
	
	public void search(String details) throws Exception
	{
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			library.waitForABSLoading();
			for (String key : hmap.keySet()) {
				switch(key.replaceAll(" ", "").toLowerCase())
				{
				case "policynumber":
					txtPolicyNo.sendKeys(hmap.get(key));
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}
			btnSearch.click();
			btnMaximize.click();
			Thread.sleep(5000);
			lblFirstRecord.click();
			btnOK.click();
			Thread.sleep(1000);
			btnOKFileSelection.click();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the search details : " + e.getMessage());
		}
	}
	
	
}
