package pom.rap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.utilities.Library;

public class MainPage {

	
	@FindBy(xpath="//div[text()='O'][span[text()='b']][text()='ject']/../div")
	private WebElement btnObject;
	
	@FindBy(xpath="//div[text()='Pro'][span[text()='d']][text()='uct']/../div")
	private WebElement btnProduct;
	
	@FindBy(xpath="//div[text()='N.-r'][span[text()='e']][text()='c. stm.']/../div")
	private WebElement btnNrecStm;
	
	@FindBy(xpath="//div[text()='Collect'][span[text()='i']][text()='on']/../div")
	private WebElement btnCollection;
	
	@FindBy(xpath="//div[text()='Bloc'][span[text()='k']]/../div")
	private WebElement btnBlock;
	
	@FindBy(xpath="//div[text()='P'][span[text()='H']]/../div")
	private WebElement btnPH;
	
	@FindBy(xpath="//div[text()='Questio'][span[text()='n']][text()='s']/../div")
	private WebElement btnQuestions;
	
	@FindBy(xpath="//div[text()='Ser'][span[text()='v']][text()='.addr.']/../div")
	private WebElement btnServAddr;
	
	@FindBy(xpath="//div[text()='Con'][span[text()='t']][text()='ract']/../div")
	private WebElement btnContract;
	
	@FindBy(xpath="//div[text()='C'][span[text()='l']][text()='aims']/../div")
	private WebElement btnClaims;
	
	@FindBy(xpath="//div[text()='Assi'][span[text()='g']][text()='nm.']/../div")
	private WebElement btnAssignm;
	
	@FindBy(xpath="//div[text()='Ot'][span[text()='h']][text()='er role']/../div")
	private WebElement btnOtherRole;
	
	Library library = new Library();
	
	public void selectTab(String tabName) throws Exception
	{
		library.waitForABSLoading();
		try {
			switch(tabName.replaceAll(" ", "").toLowerCase())
			{
			case "questions" :
				btnQuestions.click();
				break;
			default:
				throw new Exception("There is no tab : '"+tabName+"'");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while selecting the tab : " + e.getMessage());
		}
	
	}
	
}
