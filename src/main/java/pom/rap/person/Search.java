package pom.rap.person;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Search {

	@FindBy(xpath = "//div[@testid='suche_name']/input")
	private WebElement txtName;

	@FindBy(xpath = "//div[@testid='suche_vorname']/input")
	private WebElement txtFirstName;

	@FindBy(xpath = "//div[@testid='suche_mobileNumber']/input")
	private WebElement txtMobileNumber;

	@FindBy(xpath = "//div[text()='Natural']/preceding-sibling::div")
	private WebElement radNatural;

	@FindBy(xpath = "//div[text()='Legal']/preceding-sibling::div")
	private WebElement radLegal;

	@FindBy(xpath = "//div[text()='Both']/preceding-sibling::div")
	private WebElement radBoth;

	@FindBy(xpath = "//div[@testid='cb_suchen']/div")
	private WebElement btnSearch;

	@FindBy(xpath = "//div[@testid='cb_2']//div[text()='Close']")
	private WebElement btnClose;
	
	@FindBy(xpath="//div[@testid='cb_ok']")
	private WebElement btnOk;
	
	@FindBy(xpath="//div[@testid='cb_neu']")
	private WebElement btnNew;

	private static com.utilities.Library library = new com.utilities.Library();

	public void search(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			library.waitForABSLoading();
			for (String key : hmap.keySet()) {
				System.out.println("Search method is called"+key);
				library.waitForABSLoading();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "name":
					txtName.click();
					Thread.sleep(500);
					txtName.sendKeys(hmap.get(key));
					break;
				case "firstname":
					txtFirstName.click();
					Thread.sleep(500);
					txtFirstName.sendKeys(hmap.get(key));
					break;
				case "type":
					switch (hmap.get(key).toLowerCase()) {
					case "natural":
						radNatural.click();
						break;
					case "legal":
						radLegal.click();
						break;
					case "both":
						radBoth.click();
						break;
					default:
						throw new Exception("There is no such type  : " + hmap.get(key));
					}
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}
			btnSearch.click();
			library.waitForABSLoading();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the search details : " + e.getMessage());
		}
	}

	public void performAction_SearchResult(String action) throws Exception {
		try {
			 library.waitForABSLoading();
			Thread.sleep(1000);
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "ok":
				btnOk.click();
				break;
			case "new":
				btnNew.click();
				break;
			default:
				throw new Exception("Implementation for the field '" + action + "' is not found");
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the search details : " + e.getMessage());
		}
	}
	
	public void verifySearchCount(String count) throws Exception {
		try {
			int actualCount = library.getDriver()
					.findElements(By.xpath("//div[@testid='center']//div[@tabindex]//div[@testid='warenkorb']")).size();
			int expectedCount = Integer.parseInt(count.trim());
			if (actualCount != expectedCount)
				throw new Exception("The expected number of records doesn't match ==> Expected : " + expectedCount
						+ ". Actual : " + actualCount);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while verifying the search count : " + e.getMessage());
		}
	}

}
