package pom.rap.person;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.utilities.Library;

public class Detail {

	@FindBy(xpath = "//div[contains(text(),'person file')]/..")
	private WebElement lblNaturalPersonTab;

	@FindBy(xpath = "//div[contains(@testid,'person')]//div[contains(@style,'730a841e')]")
	private WebElement btnMaximize;

	@FindBy(xpath = "//div[@testid='famname']/input")
	private WebElement txtLastName;

	@FindBy(xpath = "//div[@testid='vorname']/input")
	private WebElement txtFirstName;

	@FindBy(xpath = "//div[@testid='geburtsdatum']/input")
	private WebElement txtDateofBirth;

	@FindBy(xpath = "//div[text()='Fem.']/preceding-sibling::div")
	private WebElement radFemale;

	@FindBy(xpath = "//div[text()='Male']/preceding-sibling::div")
	private WebElement radMale;

	@FindBy(xpath = "//div[@testid='cb_betreuer_zuordnen']")
	private WebElement btnAllocate;

	@FindBy(xpath = "//div[@testid='Search acc.mgr./agent']//div[@testid='cb_suchen']")
	private WebElement btnAccountManagerSearch;

	@FindBy(xpath = "//div[@testid='Search acc.mgr./agent']//div[@testid='cb_ok']")
	private WebElement btnAccountManagerOk;

	@FindBy(xpath = "//div[@testid='cb_neu']")
	private WebElement btnAddressNew;

	@FindBy(xpath = "//div[@testid='cb_aendern']")
	private WebElement btnAddressChange;

	@FindBy(xpath = "//div[@testid='cb_loeschen']")
	private WebElement btnAddressRemove;

	@FindBy(xpath = "//div[@testid='postleitzahl']//input")
	private WebElement txtZIP;

	@FindBy(xpath = "//div[@testid='ortschaft']//input")
	private WebElement txtCity;

	@FindBy(xpath = "//div[@testid='strasse']//input")
	private WebElement txtStreet;

	@FindBy(xpath = "//div[@testid='Edit address']//div[@testid='cb_ok']")
	private WebElement btnEditAddressOk;

	@FindBy(xpath = "//div[@testid='Edit address']//div[@testid='cb_zurueck']")
	private WebElement btnEditAddressCancel;

	@FindBy(xpath = "//div[@testid='Edit address']//div[@testid='cb_suchen']")
	private WebElement btnEditAddressSearch;

	@FindBy(xpath = "//div[@testid='City and street directory']//div[@testid='cb_ok']")
	private WebElement btnCityStreetDirectoryOk;

	@FindBy(xpath = "//div[@testid='City and street directory']//div[@testid='cb_zurueck']")
	private WebElement btnCityStreetDirectoryCancel;

	@FindBy(xpath = "//div[@testid='cb_insert_kommdaten']")
	private WebElement btnCommunicationAdd;

	@FindBy(xpath = "//div[@testid='cb_edit_kommdaten']")
	private WebElement btnCommunicationEdit;

	@FindBy(xpath = "//div[@testid='cb_delete_kommdaten']")
	private WebElement btnCommunicationRemove;

	@FindBy(xpath = "//div[@testid='commType']//input")
	private WebElement txtCommunicationType;

	@FindBy(xpath = "//div[@testid='callSign']//input")
	private WebElement txtCommunicationNumberAddress;

	@FindBy(xpath = "//div[@testid='Edit communication data']//div[@testid='cb_ok']")
	private WebElement btnEditCommunicationOk;

	@FindBy(xpath = "//div[@testid='Edit communication data']//div[@testid='cb_close']")
	private WebElement btnEditCommunicationClose;

	@FindBy(xpath = "//div[contains(@style,'f41e3a2b')]")
	private WebElement btnSaveAndClose;
	
	@FindBy(xpath = "//div[contains(@style,'14febf20')]")
	private WebElement btnEdit;
	
	@FindBy(xpath="//div[contains(@style,'df144c0b')]")
	private WebElement btnRecalculate;
	
	@FindBy(xpath="//div[contains(@style,'27277cee')]")
	private WebElement btnOfferToApplication;
	
	@FindBy(xpath="//div[@testid='dw_header']/div/div/div[3]//div[@class='textOverflowWithEllipsis']")
	private WebElement lblPolicyNumber;
	
	@FindBy(xpath = "//div[text()='ddition'][span[text()='A']]/../div")
	private WebElement tabAddition;
	
	@FindBy(xpath="//div[text()='leet'][span[text()='F']]/../div")
	private WebElement tabFleet;

	private static com.utilities.Library library = new com.utilities.Library();

	public void moveToSubTabs(String subTabName) throws Exception
	{
		try {
			library.waitForABSLoading();
			switch (subTabName.replaceAll(" ", "").toLowerCase()) {
			case "addition":
				tabAddition.click();
				break;
			case "fleet":
				tabFleet.click();
				break;
			default:
				throw new Exception("Implementation for the sub tab '" + subTabName + "' is not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while moving to the sub tab : " + e.getMessage());
		}

	
	}
	
	public void enterDetails(String details) throws Exception {
		try {
			Thread.sleep(2000);
			Actions action = new Actions(library.getDriver());
			action.doubleClick(lblNaturalPersonTab).build().perform();
//			for (int i = 0; i < 10; i++)
//				action.dragAndDropBy(lblNaturalPersonTab, -100, 0).build().perform();
//			Thread.sleep(1000);
//			btnMaximize.click();
			HashMap<String, String> hmap = library.getFieldMap(details);
			library.waitForABSLoading();
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "lastname":
					txtLastName.click();
					txtLastName.clear();
					txtLastName.sendKeys(hmap.get(key));
					Library.staticMap.put("LastName", txtLastName.getAttribute("value"));
					break;
				case "firstname":
					txtFirstName.click();
					txtFirstName.clear();
					Thread.sleep(500);
					txtFirstName.sendKeys(hmap.get(key));
					Library.staticMap.put("FirstName", txtFirstName.getAttribute("value"));
					break;
				case "dateofbirth":
					txtDateofBirth.click();
					txtDateofBirth.clear();
					Thread.sleep(500);
					txtDateofBirth.sendKeys(hmap.get(key));
					break;
				case "gender":
					if (hmap.get(key).equalsIgnoreCase("male"))
						radMale.click();
					else
						radFemale.click();
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}
			library.waitForABSLoading();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the search details : " + e.getMessage());
		}

	}

	public void performAction(String action) throws Exception {

		try {
			library.waitForABSLoading();
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "saveandclose":
				btnSaveAndClose.click();
				Thread.sleep(5000);
				break;
			case "edit" :
				btnEdit.click();
				//Actions actions = new Actions(library.getDriver());
//				for (int i = 0; i < 10; i++)
//					actions.dragAndDropBy(lblNaturalPersonTab, -100, 0).build().perform();
//				btnMaximize.click();
				break;
			case "recalculate" :
				btnRecalculate.click();
				library.waitForABSLoading();
				break;
			case "offertoapplication":
				btnOfferToApplication.click();
				library.waitForABSLoading();
				Library.staticMap.put("Policy Number", lblPolicyNumber.getText().split(" ")[0]);
				break;
			default:
				throw new Exception("Implementation for the field '" + action + "' is not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the search details : " + e.getMessage());
		}

	}

	public void performAddressAction(String action) throws Exception {

		try {
			library.waitForABSLoading();
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "new":
				btnAddressNew.click();
				break;
			case "change":
				btnAddressChange.click();
				break;
			case "remove":
				btnAddressRemove.click();
				break;
			default:
				throw new Exception("Implementation for the field '" + action + "' is not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the search details : " + e.getMessage());
		}

	}

	public void enterAddressDetails(String details, String action) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			library.waitForABSLoading();
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "zip":
					txtZIP.click();
					txtZIP.clear();
					Thread.sleep(500);
					txtZIP.sendKeys(hmap.get(key));
					break;
				case "city":
					txtCity.click();
					txtCity.clear();
					txtCity.sendKeys(hmap.get(key));
					break;
				case "street":
					txtStreet.click();
					txtStreet.clear();
					txtStreet.sendKeys(hmap.get(key));
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "ok":
				btnEditAddressOk.click();
				break;
			case "cancel":
				btnEditAddressCancel.click();
				break;
			case "search":
				btnEditAddressSearch.click();
				break;
			default:
				throw new Exception("Implementation for the field '" + action + "' is not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the search details : " + e.getMessage());
		}

	}

	public void enterCommunicationDetails(String details, String action) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			library.waitForABSLoading();
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "communicationtype":
					txtCommunicationType.click();
					txtCommunicationType.clear();
					txtCommunicationType.sendKeys(hmap.get(key));
					Thread.sleep(500);
					txtCommunicationType.sendKeys(Keys.ARROW_DOWN, Keys.ENTER);
					break;
				case "number/address":
				case "number":
					txtCommunicationNumberAddress.click();
					txtCommunicationNumberAddress.clear();
					txtCommunicationNumberAddress.sendKeys(hmap.get(key));
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "ok":
				btnEditCommunicationOk.click();
				break;
			case "close":
				btnEditCommunicationClose.click();
				break;
			default:
				throw new Exception("Implementation for the field '" + action + "' is not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the search details : " + e.getMessage());
		}

	}

	public void performCityStreetDirectoryAction(String action) throws Exception {
		try {
			library.waitForABSLoading();
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "ok":
				btnCityStreetDirectoryOk.click();
				break;
			case "cancel":
				btnCityStreetDirectoryCancel.click();
				break;
			default:
				throw new Exception("Implementation for the field '" + action + "' is not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the search details : " + e.getMessage());
		}

	}

	public void allocateAccountManager() throws Exception {
		try {
			library.waitForABSLoading();
			btnAllocate.click();
			btnAccountManagerSearch.click();
			library.waitForABSLoading();
			btnAccountManagerOk.click();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the search details : " + e.getMessage());
		}
	}

	public void performCommunicationAction(String action) throws Exception {
		try {
			library.waitForABSLoading();
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "edit":
				btnCommunicationEdit.click();
				break;
			case "add":
				btnCommunicationAdd.click();
				break;
			case "remove":
				btnCommunicationRemove.click();
				break;
			default:
				throw new Exception("Implementation for the field '" + action + "' is not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the search details : " + e.getMessage());
		}

	}
	
	public void selectMenuItem(String details) throws Exception
	{
		try {
			Thread.sleep(2000);
			library.waitForABSLoading();
			String[] menuArray = details.split(">");
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[contains(@testid,'person file')]/div[2]/div/div[text()='"+menuArray[0].trim()+"']")).click();
			//library.getDriver().findElement(By.xpath("//div[contains(@testid,'person file')]/div[2]/div/div[text()='"+menuArray[0].trim()+"']")).click();
			
			for(int i=1;i<menuArray.length;i++)
			{
				Thread.sleep(500);
				library.getDriver().findElement(By.xpath("//div[text()='"+menuArray[i].trim()+"']")).click();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while selecting the menu item : " + e.getMessage());
		}
	}
}
