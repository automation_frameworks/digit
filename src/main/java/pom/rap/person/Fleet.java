package pom.rap.person;

import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Fleet {

	@FindBy(xpath = "//div[@testid='cb_sachneu']")
	private WebElement btnNew;

	@FindBy(xpath = "//div[text()='ain classes'][span[text()='M']]/../div")
	private WebElement tabMainClasses;

	@FindBy(xpath = "//div[text()='Cla'][text()='ses'][span[text()='s']]/../div")
	private WebElement tabClasses;

	@FindBy(xpath = "//div[@testid='cb_sachzusatzneu']")
	private WebElement btnAdd;

	@FindBy(xpath = "//div[@testid='st_zusatz']/following-sibling::div/div[@testid='cb_sachzusatzneu']")
	private WebElement btnAddClass;

	@FindBy(xpath = "//div[@testid='dw_person_flotten_sach_zusatz']//div[@class='emptyComboCell textOverflowWithEllipsis']/../..")
	private WebElement drpMainClass;

	@FindBy(xpath = "//div[@testid='hsp_eigen.control']//input")
	private WebElement txtMainClass;

	@FindBy(xpath = "//div[@testid='sparte_eigen.control']//input")
	private WebElement txtSparte;

	@FindBy(xpath = "//div[text()='MAX. SR']/../following-sibling::div[2]/div")
	private WebElement lblMaxSr;
	
	@FindBy(xpath = "//div[@testid='maxsora.control']//input")
	private WebElement txtMaxSr;

	@FindBy(xpath = "//div[text()='Mind. Pr.']/../following-sibling::div[2]/div")
	private WebElement lblMindPr;
	
	@FindBy(xpath = "//div[@testid='mindpr.control']//input")
	private WebElement txtMindPr;

	private com.utilities.Library library = new com.utilities.Library();

	public void performAction(String action) throws Exception {
		try {
			library.waitForABSLoading();
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "new":
				btnNew.click();
				break;
			case "addmainclass":
				btnAdd.click();
				break;
			case "addclass":
				btnAddClass.click();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while clicking on the button : " + action);
		}
	}

	public void moveToSubTabs(String subTab) throws Exception {
		try {
			library.waitForABSLoading();
			switch (subTab.replaceAll(" ", "").toLowerCase()) {
			case "mainclasses":
				tabMainClasses.click();
				break;
			case "classes":
				tabClasses.click();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while clicking on the button : " + subTab);
		}
	}

	public void selectMainClass(String mainClass) throws Exception {
		try {
			drpMainClass.click();
			library.waitForABSLoading();
			library.selectDropDownValue_ABS(txtMainClass, mainClass);
		} catch (Exception e) {
			throw new Exception("Error while selecting the main class : " + e.getMessage());
		}
	}

	public void enterClassDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "sparte":
					library.selectDropDownValue_ABS(txtSparte, hmap.get(key));
					break;
				case "maxsr":
					lblMaxSr.click();
					txtMaxSr.clear();
					txtMaxSr.sendKeys(hmap.get(key));
					break;
				case "mindpr":
					lblMindPr.click();
					txtMindPr.clear();
					txtMindPr.sendKeys(hmap.get(key));
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not done");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while selecting the main class : " + e.getMessage());
		}
	}

}
