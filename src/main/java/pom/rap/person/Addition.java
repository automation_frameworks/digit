package pom.rap.person;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Addition {

	@FindBy(xpath = "//div[@testid='natpersausweisdaten_button_add']")
	private WebElement btnIDCardAdd;

	@FindBy(xpath = "//div[@testid='mle_1']/textarea")
	private WebElement lblErrorMessageArea;
	
	@FindBy(xpath="//div[@testid='Event display']//div[@testid='cb_2']")
	private WebElement btnErrorMessageClose;

	private static com.utilities.Library library = new com.utilities.Library();

	public void performIDCardAction(String action) throws Exception {
		try {
			library.waitForABSLoading();
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "add":
				btnIDCardAdd.click();
				break;
			default:
				throw new Exception("Implementation for the field '" + action + "' is not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while performing action in ID card section : " + e.getMessage());
		}
	}

	public void enterIDCardDetails(String details, String row) throws Exception {
		try {
			Thread.sleep(1000);
			int rowsPresent = library.getDriver()
					.findElements(By.xpath(
							"//div[@testid='dw_natpersausweisdaten']/div/div/div[contains(@style,'color: rgb')]"))
					.size();
			int rowToBeEntered = Integer.parseInt(row.trim());
			if (rowsPresent < rowToBeEntered)
				throw new Exception("There are only " + rowsPresent
						+ "rows in the page. So unable to enter data for the row " + rowToBeEntered);
			HashMap<String, String> hmap = library.getFieldMap(details);
			library.waitForABSLoading();
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "type":
					Thread.sleep(2000);
					library.getDriver().findElement(By
							.xpath("//div[@testid='dw_natpersausweisdaten']/div/div/div[contains(@style,'color: rgb')]["
									+ rowToBeEntered + "]//div[@class='comboCell textOverflowWithEllipsis']"))
							.click();
					Thread.sleep(1000);
					// library.getDriver().findElement(By
					// .xpath("//div[@testid='dw_natpersausweisdaten']/div/div/div[contains(@style,'color:
					// rgb')]["
					// + rowToBeEntered + "]//div[@class='comboCell textOverflowWithEllipsis']"))
					// .click();
					library.getDriver().findElement(By.xpath("//div[text()='" + hmap.get(key).trim() + "']")).click();
					break;
				case "iddocumentno":
					Thread.sleep(1000);
					library.getDriver().findElement(By.xpath("//div[@testid='ausweisnr.control']/input")).click();
					library.getDriver().findElement(By.xpath("//div[@testid='ausweisnr.control']/input"))
							.sendKeys(hmap.get(key));
					;
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}
			library.waitForABSLoading();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the search details : " + e.getMessage());
		}
	}

	
}
