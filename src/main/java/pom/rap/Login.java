package pom.rap;

import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Login {

	
	@FindBy(name ="j_username")
	private WebElement txtUserName;
	
	@FindBy(name="j_password")
	private WebElement txtPassword;
	
	@FindBy(id="submitbutton")
	private WebElement btnSubmit;
	
	private com.utilities.Library library = new com.utilities.Library();
	
	public void login(String details) throws Exception
	{
		try {
		HashMap<String, String> hmap = library.getFieldMap(details);
		for (String key : hmap.keySet()) {
			switch (key.replaceAll(" ", "").toLowerCase()) {
			case "username":
				txtUserName.sendKeys(hmap.get(key));
				break;
			case "password":
				txtPassword.sendKeys(hmap.get(key));
				break;
			default:
				throw new Exception("Implememntation for the field '" + key + "' is not found");
			}
		}
		btnSubmit.click();
	} catch (Exception e) {
		e.printStackTrace();
		throw new Exception(e.getMessage());
	}
	}
}
