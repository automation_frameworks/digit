package pom.rap;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.utilities.Library;

public class Questions {

	@FindBy(xpath = "//div[@testid='dw_fragen_kategorie']//div[text()='Common Questions']")
	private WebElement lblCommonQuestions;

	@FindBy(xpath = "//div[contains(@style,'14febf20')]")
	private WebElement btnEdit;

	@FindBy(xpath = "//div[@class='textOverflowWithEllipsis'][text()='Pre inspection assesment completed?']")
	private WebElement lblPreInsCompleted;

	@FindBy(xpath = "//div[@class='textOverflowWithEllipsis'][text()='Pre inspection assesment completed?']/..//following-sibling::div[div]/div")
	private WebElement txtPreInsCompleted;

	@FindBy(xpath = "//div[contains(text(),'Vehicle')]/..//following-sibling::div/div[contains(@style,'730a841e')]")
	private WebElement btnMaximize;

	@FindBy(xpath = "//div[@testid='dw_fragen_liste']/div[3]/div[3]")
	private WebElement btnScrollDown;

	@FindBy(xpath = "//div[text()='Yes'][contains(@style,'white-space')]")
	private WebElement btnYes;
	
	@FindBy(xpath="//div[contains(@style,'f41e3a2b')]")
	private WebElement btnSaveAndClose;
	
	@FindBy(xpath="//div[@testid='yes']")
	private WebElement btnPopupYes;
	
	@FindBy(xpath="//div[@testid='no']")
	private WebElement btnPopupNo;
	
	@FindBy(xpath="//div[@testid='cb_2']/div[text()='Close']")
	private WebElement btnClose;

	Library library = new Library();

	public void completePreInspection() throws Exception {
		try {
			lblCommonQuestions.click();
			btnMaximize.click();
			btnEdit.click();
			Thread.sleep(1000);
			library.waitForABSLoading();
			
			// lblPreInsCompleted.click();
			while (true) {
				try {
					lblPreInsCompleted.click();
					break;
				} catch (NoSuchElementException ne) {
					Actions action = new Actions(library.getDriver());
					action.sendKeys(Keys.TAB).build().perform();
					//btnScrollDown.click();
				}
			}
			Thread.sleep(1000);
			Actions action = new Actions(library.getDriver());
			action.sendKeys(Keys.TAB).build().perform();
			Thread.sleep(1000);
			//lblPreInsCompleted.click();
			//btnScrollDown.click();
			txtPreInsCompleted.click();
			Thread.sleep(1000);
			
			//txtPreInsCompleted.sendKeys("Yes");
			Thread.sleep(1000);
			btnYes.click();
			btnSaveAndClose.click();
			btnPopupNo.click();
			library.waitForABSLoading();
			Thread.sleep(1000);
			btnClose.click();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while completing the Pre Inspection : " + e.getMessage());
		}
	}
}
