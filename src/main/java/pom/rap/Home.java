package pom.rap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.utilities.Library;

public class Home {

	
	@FindBy(xpath="//div[text()='ONTRACT'][span[text()='C']]/../div")
	private WebElement lblContract;
	
	@FindBy(xpath="//div[text()='ERSON'][span[text()='P']]/../div")
	private WebElement lblPerson;
	
	Library library = new Library();
	
	
	public void selectModule(String moduleName) throws Exception
	{
		try {
			switch(moduleName.replaceAll(" ", "").toLowerCase())
			{
			case "contract":
				library.waitForABSLoading();
				library.waitForElementToBeClickable(lblContract, 30, 200);
				lblContract.click();
				break;
			case "person":
				library.waitForElementToBeClickable(lblPerson, 30, 200);
				lblPerson.click();
				break;
				default:
					throw new Exception("There is no such module '" + moduleName + "'");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}
}
