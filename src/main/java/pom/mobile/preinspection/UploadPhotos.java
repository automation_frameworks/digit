package pom.mobile.preinspection;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.utilities.Library;


public class UploadPhotos {

	public static void main(String args[]) throws InterruptedException, IOException {
		uploadImages("https://uat-DigitMotorInsurance.godigit.com/DigitMotorInsurance/#/?target=9C8D0345D7F362B5A9D04FC53B8C1D62");
	}
	
	public static void uploadImages(String url) throws InterruptedException
	{
		try{
			System.out.println("Uploading the images");
		Map<String, Object> deviceMetrics = new HashMap<String, Object>();
		deviceMetrics.put("width", 300);
		deviceMetrics.put("height", 500);
		deviceMetrics.put("pixelRatio", 2);
		Map<String, Object> mobileEmulation = new HashMap<String, Object>();
		mobileEmulation.put("deviceName", "Nexus 5");
		System.setProperty("webdriver.chrome.driver", "C:\\ARTEMIS\\references\\chromedriver.exe");
		Map<String, Object> chromeOptions = new HashMap<String, Object>();
		chromeOptions.put("mobileEmulation", mobileEmulation);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		WebDriver driver = new ChromeDriver(capabilities);
		driver.get(url);
		driver.manage().window().maximize();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean twowheeler = false;
		if(driver.findElement(By.xpath("//instruction//p[1]")).getText().contains("two wheeler"))
			twowheeler = true;
		driver.findElement(By.xpath("//button[text()='Let’s get started']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()=' Capture photograph']/following-sibling::input")).sendKeys("D:\\InspectionImages\\FrontSide.jpg");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()=' Capture photograph']/following-sibling::input")).sendKeys("D:\\InspectionImages\\WindScreen.jpg");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()=' Capture photograph']/following-sibling::input")).sendKeys("D:\\InspectionImages\\FrontRightCorner.jpg");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()=' Capture photograph']/following-sibling::input")).sendKeys("D:\\InspectionImages\\RightSide.jpg");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()=' Capture photograph']/following-sibling::input")).sendKeys("D:\\InspectionImages\\BackRightCorner.jpg");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()=' Capture photograph']/following-sibling::input")).sendKeys("D:\\InspectionImages\\BackSide.jpg");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()=' Capture photograph']/following-sibling::input")).sendKeys("D:\\InspectionImages\\BackLeftCorner.jpg");
		Thread.sleep(3000);
		if(twowheeler)
		{
			int counter = 0;
			while(driver.findElements(By.xpath("//p[text()='Uploading...']")).size()>0)
			{
				Thread.sleep(2000);
				counter++;
				if(counter>150)
					break;
			}
			driver.close();
			return;
		}
		driver.findElement(By.xpath("//button[text()=' Capture photograph']/following-sibling::input")).sendKeys("D:\\InspectionImages\\LeftSide.jpg");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()=' Capture photograph']/following-sibling::input")).sendKeys("D:\\InspectionImages\\FrontLeftCorner.jpg");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()=' Capture photograph']/following-sibling::input")).sendKeys("D:\\InspectionImages\\DashBoard.jpg");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()=' Capture photograph']/following-sibling::input")).sendKeys("D:\\InspectionImages\\EngineChamber.jpg");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()=' Capture photograph']/following-sibling::input")).sendKeys("D:\\InspectionImages\\ChassisPlate.jpg");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()=' Capture photograph']/following-sibling::input")).sendKeys("D:\\InspectionImages\\RCBookFront.jpg");
		Thread.sleep(3000);
		try
		{
		driver.findElement(By.xpath("//button[text()=' Capture photograph']/following-sibling::input")).sendKeys("D:\\InspectionImages\\RCBookBack.jpg");
		}catch(Exception e)
		{
			
		}
		Thread.sleep(3000);
		try {
		driver.findElement(By.xpath("//button[text()=' Capture photograph']/following-sibling::input")).sendKeys("D:\\InspectionImages\\RCBookBack.jpg");
		}
		catch(Exception e)
		{
			
		}
		Thread.sleep(3000);
		int counter = 0;
		while(driver.findElements(By.xpath("//p[text()='Uploading...']")).size()>0)
		{
			Thread.sleep(2000);
			counter++;
			if(counter>150)
				break;
		}
		driver.close();
	}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void getLinkAndUploadImages() throws Exception
	{
		try {
			uploadImages(getPreInspectionLink(grant()));
		}
		catch(Exception e)
		{
			throw new Exception("Unable to uplaod the images : "+e.getMessage());
		}
	}
	
	
	private static String getPreInspectionLink(String accessToken) throws Exception {

		try {
			Thread.sleep(8000);
			String url = Library.staticMap.get("server")+"/digitplusservices/getPreInspectionLinkActivityResponse?PolicyNumber="
					+ Library.staticMap.get("Policy Number");
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("GET");
			con.setRequestProperty("Accept", "application/json, text/plain, */*");
			con.setRequestProperty("content-type", "Application/json");
			con.setRequestProperty("Authorization", "Bearer " + accessToken);

			// con.setDoOutput(true);

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
			System.out.println("Response Message : " + con.getResponseMessage());

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());

			JSONObject jsonArray = (JSONObject)new JSONParser().parse(response.toString());
			JSONArray linkArray = (JSONArray) jsonArray.get("receivedActivities");
			JSONObject linkObj = (JSONObject) linkArray.get(0);
			return (String) linkObj.get("value");
		} catch (Exception e) {
			throw new Exception("Unable to get the Pre Inspection Link : "+e.getMessage());
		}

	}
	
	private static String grant() throws Exception
	{
		try {
			String url = Library.staticMap.get("server")+"/digitplusservices/oauth/grant";
			String accessToken;
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept", "application/json, text/plain, */*");
			con.setRequestProperty("content-type", "application/x-www-form-urlencoded");

			String urlParameters = "username=35327650&password=digit123";

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
//			System.out.println("Post parameters : " + urlParameters);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			//System.out.println(response.toString());

			Object jsonObj = new JSONParser().parse(response.toString());

			// typecasting obj to JSONObject
			JSONObject jo = (JSONObject) jsonObj;
			//System.out.println("Access Token : " + jo.get("accessToken"));
			accessToken = jo.get("accessToken").toString();
			return accessToken;
		} catch (Exception e) {
			throw new Exception("Unable to get the Access Token : "+e.getMessage());
		}
	}
	
}