package pom.agent.empcomp;

import com.utilities.Library;
import com.utilities.PropertyFileReader;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.HashMap;

public class EmpCompPage {
    // ------------------------------- Policy Details -------------------------------

    @FindBy(xpath = "//p[text()='Employee Compensation']")
    private WebElement empCompHeaderLabel;

    @FindBy(xpath = "//div[text()='New Business']")
    private WebElement policyTypeNewRadio;

    @FindBy(xpath = "//div[text()='Renewals/Rollover ']")
    private WebElement policyTypeRolloverRadio;

    @FindBy(xpath = "//div[text()='Individual']")
    private WebElement policyHolderIndividualRadio;

    @FindBy(xpath = "//div[text()='Corporate']")
    private WebElement policyHolderCorporateRadio;

    @FindBy(xpath = "//div[text()='Table A']")
    private WebElement tableARadio;

    @FindBy(xpath = "//div[text()='Table B']")
    private WebElement tableBRadio;

    @FindBy(xpath = "//mat-radio-group[@name='occupational']/descendant::div[text()='Yes']")
    private WebElement occDiseaseExtnYesRadio;

    @FindBy(xpath = "//mat-radio-group[@name='occupational']/descendant::div[text()='No']")
    private WebElement getOccDiseaseExtnNoRadio;

    @FindBy(xpath = "//mat-radio-group[@name='contractors']/descendant::div[text()='Yes']")
    private WebElement contractorsEmpCovYesRadio;

    @FindBy(xpath = "//mat-radio-group[@name='contractors']/descendant::div[text()='No']")
    private WebElement contractorsEmpCovNoRadio;

    @FindBy(xpath = "//mat-select[@name='medical_expense_select']")
    private WebElement medicalExpExtnDropdown;

    @FindBy(xpath = "//input[@name='year_limit']")
    private WebElement aggrLimitTextField;

    // ------------------------------- Previous Insurer Details -------------------------------

    @FindBy(xpath = "//p[contains(text(), 'Previous Policy Details')]")
    private WebElement previousPolicyDetailsLabel;

    @FindBy(xpath = "//input[@name='previousInsurer']")
    private WebElement previousInsurerDropdown;

    @FindBy(name = "previousPolicyNumber")
    private WebElement prevPolicyNoTextField;

    @FindBy(xpath = "//mat-radio-group[@name='isClaims']/descendant::div[text()='Yes']")
    private WebElement previousClaimsYesRadio;

    @FindBy(xpath = "//mat-radio-group[@name='isClaims']/descendant::div[text()='No']")
    private WebElement previousClaimsNoRadio;

    @FindBy(name = "claimAmount")
    private WebElement claimAmountTextField;

    @FindBy(name = "claimDes")
    private WebElement claimDescTextField;

    // ------------------------------- Work Description Details -------------------------------

    @FindBy(xpath = "//p[contains(text(), 'Provide work Description')]")
    private WebElement provideWorkDescLabel;

    @FindBy(xpath = "//div[@class='work_des']/input")
    private WebElement workdescDropdown;

    @FindBy(xpath = "//div[@class='sub_cat']/input")
    private WebElement subCatDropdown;

    @FindBy(xpath = "//div[@class=' short_des']/input")
    private WebElement shortDescTextField;

    @FindBy(xpath = "//div[@class='salary']/input")
    private WebElement salaryTextField;

    @FindBy(xpath = "//div[@class='no_emp']/input")
    private WebElement totalEmpTextField;

    @FindBy(xpath = "//span[text()='Show Premium']")
    private WebElement showPremiumButton;

    // ------------------------------- Premium Break-up -------------------------------

    @FindBy(xpath = "//p[text()='Premium Break Up']")
    private WebElement premiumBreakupLabel;

    @FindBy(xpath = "//input[@name='discount']")
    private WebElement discountLoadingTextField;

    @FindBy(xpath = "//p[text()='Apply']")
    private WebElement applyButton;

    // ------------------------------- Customer Details -------------------------------

    @FindBy(xpath = "//p[text()='Customer Details']")
    private WebElement custDetailsHeaderLabel;

    @FindBy(id = "firstName")
    private WebElement nameOfTheInsuredTextField;

    @FindBy(name = "mobileNumber")
    private WebElement mobileNoTextField;

    @FindBy(name = "emailId")
    private WebElement emailIdTextField;

    @FindBy(name = "gstNumber")
    private WebElement gstNumberTextField;

    @FindBy(xpath = "//input[@placeholder='Enter Description']")
    private WebElement workDescTextField;

    // ------------------------------- Risk Location Details -------------------------------

    @FindBy(xpath = "//span[text()='Risk Location1 ']")
    private WebElement riskLocationLabel;

    @FindBy(xpath = "//label[@class='input-label riskLabel' and contains(text(), 'Full Address')]/following-sibling::input")
    private WebElement riskAddrTextField;

    @FindBy(xpath = "//label[@class='input-label riskLabel' and contains(text(), 'Pincode')]/following-sibling::input")
    private WebElement riskPincodeTextField;

    @FindBy(xpath = "//p[contains(text(), 'Anywhere in india')]/../../descendant::input")
    private WebElement anyWhereInIndiaCheckbox;

    @FindBy(xpath = "//p[contains(text(), 'risk address & communication address are same')]/../../descendant::input")
    private WebElement riskCommLocSameCheckbox;

    @FindBy(xpath = "//p[contains(text(), 'Add Risk Location')]")
    private WebElement addRiskLocButton;

    // ------------------------------- Communication Address Details -------------------------------

    @FindBy(xpath = "//p[text()='Communication Address']")
    private WebElement commAddrLabel;

    @FindBy(name = "com_address")
    private WebElement commAddrTextField;

    @FindBy(name = "com_pincode")
    private WebElement commPincodeTextField;

    @FindBy(xpath = "//button[contains(text(), 'Save Quote')]")
    private WebElement saveQuoteButton;

    // ------------------------------- Payment Details -------------------------------

    @FindBy(xpath = "//p[text()='Quote Number']/parent::div/div/p")
    private WebElement quoteNoLabel;

    @FindBy(xpath = "//i[@title='Download quote']")
    private WebElement quoteDownloadButton;

    @FindBy(xpath = "//div[contains(@class, 'footer')]/descendant::p[text()='Total Payable Amount']/following-sibling::p")
    private WebElement totalPayableAmtFooterLabel;

    @FindBy(xpath = "//span[text()='Submit']")
    private WebElement submitButton;

    @FindBy(xpath = "//div[@class='refferal-cta-container']/button[text()='Yes']")
    private WebElement finalSubmitOptionYes;

    @FindBy(xpath = "//div[@class='refferal-cta-container']/button[text()='No']")
    private WebElement finalSubmitOptionNo;

    @FindBy(xpath = "//label[text()='Payment Mode']")
    private WebElement paymentModeLabel;

    @FindBy(xpath = "//label[text()='Payment Mode']/following::mat-select")
    private WebElement paymentModeDropdown;

    @FindBy(xpath = "//label[text()='Cheque Number']")
    private WebElement chequeNoLabel;

    @FindBy(xpath = "//input[@formcontrolname='chequeNumber']")
    private WebElement chequeNoTextField;

    @FindBy(xpath = "//input[@formcontrolname='IFSC']")
    private WebElement ifscTextField;

    @FindBy(xpath = "//span[text()='Proceed to pay']")
    private WebElement proceedToPayButton;

    @FindBy(xpath = "//b[contains(@id, 'Successfully-Issued')]")
    private WebElement successMessageLabel;

    // ------------------------------- Additional global variables -------------------------------

    private Library library = new Library();

    // ------------------------------- Action methods -------------------------------

    public void enterPolicyDetails(String details) throws Exception {
        try {
            HashMap<String, String> hmap = library.getFieldMap(details);

            for (String key : hmap.keySet()) {
                library.waitForDigitLoad();

                switch (key.replaceAll(" ", "").toLowerCase()) {
                    case "policytype":
                        switch (hmap.get(key).toLowerCase()) {
                            case "newbusiness":
                                policyTypeNewRadio.click();
                                break;
                            case "renewals/rollover":
                                policyTypeRolloverRadio.click();
                                break;
                            default:
                                throw new Exception("There is no such policy type: [" + hmap.get(key) + "]");
                        }
                        break;
                    case "policyholder":
                        switch (hmap.get(key).toLowerCase()) {
                            case "individual":
                                policyHolderIndividualRadio.click();
                                break;
                            case "corporate":
                                policyHolderCorporateRadio.click();
                                break;
                            default:
                                throw new Exception("There is no such policy holder: [" + hmap.get(key) + "]");
                        }
                        break;
                    case "tablea/b":
                        switch (hmap.get(key).toLowerCase()) {
                            case "table a":
                                tableARadio.click();
                                break;
                            case "table b":
                                tableBRadio.click();
                                break;
                            default:
                                throw new Exception("There is no such table: [" + hmap.get(key) + "]");
                        }
                        break;
                    case "occupationaldiseaseextn.":
                        switch (hmap.get(key).toLowerCase()) {
                            case "yes":
                                occDiseaseExtnYesRadio.click();
                                break;
                            case "no":
                                getOccDiseaseExtnNoRadio.click();
                                break;
                            default:
                                throw new Exception("Occupational disease extension can be either 'Yes' or 'No', given: [" + hmap.get(key) + "]");
                        }
                        break;
                    case "contractorsemployeescov":
                        switch (hmap.get(key).toLowerCase()) {
                            case "yes":
                                contractorsEmpCovYesRadio.click();
                                break;
                            case "no":
                                contractorsEmpCovNoRadio.click();
                                break;
                            default:
                                throw new Exception("Contractors employees cover can be either 'Yes' or 'No', given: [" + hmap.get(key) + "]");
                        }
                        break;

                    case "medicalexpensesextn.limitperperson":
                        medicalExpExtnDropdown.click();
                        Thread.sleep(1000);
                        library.getDriver()
                                .findElement(
                                        By.xpath("//div[@class='cdk-overlay-pane']/descendant::span[contains(text(), '" + hmap.get(key).trim() + "')]"))
                                .click();
                        break;
                    case "aggregatelimit":
                        aggrLimitTextField.clear();
                        aggrLimitTextField.sendKeys(hmap.get(key));
                        break;
                    default:
                        throw new Exception("There is no such field: [" + key + "]");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while entering the policy details: " + e.getMessage());
        }
    }

    public void enterPreviousInsurerDetails(String details) throws Exception {
        try {
            library.focus(library.getDriver(), previousPolicyDetailsLabel);
            HashMap<String, String> hmap = library.getFieldMap(details);

            for (String key : hmap.keySet()) {
                library.waitForDigitLoad();

                switch (key.replaceAll(" ", "").toLowerCase()) {
                    case "previousinsurer":
                        previousInsurerDropdown.sendKeys(hmap.get(key));
                        previousInsurerDropdown.sendKeys(Keys.TAB);
                        break;
                    case "previouspolicynumber":
                        prevPolicyNoTextField.sendKeys(hmap.get(key));
                        break;
                    case "previousclaims":
                        switch (hmap.get(key).toLowerCase()) {
                            case "yes":
                                previousClaimsYesRadio.click();
                                break;
                            case "no":
                                previousClaimsNoRadio.click();
                                break;
                            default:
                                throw new Exception("Previous claim history can be either 'Yes' or 'No', given: [" + hmap.get(key) + "]");
                        }
                        break;
                    default:
                        throw new Exception("There is no such field: [" + key + "]");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while entering the previous insurer details: " + e.getMessage());
        }
    }

    public void enterWorkDescDetails(String details) throws Exception {
        try {
            library.focus(library.getDriver(), provideWorkDescLabel);
            HashMap<String, String> hmap = library.getFieldMap(details);

            for (String key : hmap.keySet()) {
                library.waitForDigitLoad();
                switch (key.replaceAll(" ", "").toLowerCase()) {
                    case "workdescription":
                        workdescDropdown.sendKeys(hmap.get(key));
                        workdescDropdown.sendKeys(Keys.TAB);
                        break;
                    case "subcategory":
                        subCatDropdown.sendKeys(hmap.get(key));
                        subCatDropdown.sendKeys(Keys.TAB);
                        break;
                    case "shortdescriptionofwork":
                        shortDescTextField.sendKeys(hmap.get(key));
                        break;
                    case "wages/salarypermonthperperson":
                        salaryTextField.clear();
                        salaryTextField.sendKeys(hmap.get(key));
                        break;
                    case "totalno.ofemployees":
                        totalEmpTextField.clear();
                        totalEmpTextField.sendKeys(hmap.get(key));
                        break;
                    default:
                        throw new Exception("There is no such field: [" + key + "]");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while entering the work description details: " + e.getMessage());
        }
    }

    public void performAction(String action) throws Exception {
        try {
            switch (action.replaceAll(" ", "").toLowerCase()) {
                case "showpremium":
                    showPremiumButton.click();
                    Thread.sleep(3000);
                    library.waitForDigitLoad();
                    break;
                case "apply":
                    applyButton.click();
                    Thread.sleep(2000);
                    library.waitForDigitLoad();
                    break;
                case "savequote":
                    saveQuoteButton.click();
                    Thread.sleep(2000);
                    library.waitForDigitLoad();
                    Library.staticMap.put("Policy Number", quoteNoLabel.getText());
                    break;
                case "proceedtopay":
                    proceedToPayButton.click();
                    Thread.sleep(2000);
                    library.waitForDigitLoad();
                    break;
                case "submit":
                    submitButton.click();
                    break;
                default:
                    throw new Exception("Option to perform the action [" + action + "] is unavailable");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while performing action: [" + action + "]" + e.getMessage());
        }
    }

    public void enterDiscountLoadingDetails(String details) throws Exception {
        try {
            library.focus(library.getDriver(), premiumBreakupLabel);
            HashMap<String, String> hmap = library.getFieldMap(details);

            for (String key : hmap.keySet()) {
                library.waitForDigitLoad();

                switch (key.replaceAll(" ", "").toLowerCase()) {
                    case "discount/loading":
                        discountLoadingTextField.clear();
                        discountLoadingTextField.sendKeys((hmap.get(key)));
                        break;
                    default:
                        throw new Exception("There is no such field: [" + key + "]");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while entering the discount/loading details: " + e.getMessage());
        }
    }

    public void enterCustomerDetails(String details) throws Exception {
        try {
            library.focus(library.getDriver(), custDetailsHeaderLabel);
            HashMap<String, String> hmap = library.getFieldMap(details);

            for (String key : hmap.keySet()) {
                library.waitForDigitLoad();

                switch (key.replaceAll(" ", "").toLowerCase()) {
                    case "nameoftheinsured":
                        nameOfTheInsuredTextField.sendKeys(hmap.get(key));
                        break;
                    case "mobilenumber":
                        mobileNoTextField.sendKeys(hmap.get(key));
                        break;
                    case "emailid":
                        emailIdTextField.sendKeys(hmap.get(key));
                        break;
                    case "gstnumber":
                    case "gstnumber(optional)":
                        gstNumberTextField.sendKeys(hmap.get(key));
                        break;
                    case "workdescription":
                    case "workdescriptiontobeprintedonthepolicy":
                        workDescTextField.sendKeys(hmap.get(key));
                        break;
                    default:
                        throw new Exception("There is no such field: [" + key + "]");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while entering the customer details: " + e.getMessage());
        }
    }

    public void enterRiskLocationDetails(String details) throws Exception {
        try {
            library.focus(library.getDriver(), riskLocationLabel);
            HashMap<String, String> hmap = library.getFieldMap(details);

            for (String key : hmap.keySet()) {
                library.waitForDigitLoad();

                switch (key.replaceAll(" ", "").toLowerCase()) {
                    case "fulladdress":
                        riskAddrTextField.sendKeys(hmap.get(key));
                        break;
                    case "pincode":
                        riskPincodeTextField.sendKeys(hmap.get(key));
                        break;
                    case "anywhereinindia":
                        if (hmap.get(key).trim().toLowerCase().equals("true"))
                            anyWhereInIndiaCheckbox.click();
                        break;
                    case "riskandaddresslocationsame":
                    case "sameriskandaddresslocation":
                    case "clickhereifthisriskaddress&communicationaddressaresame":
                        riskCommLocSameCheckbox.click();
                        break;
                    default:
                        throw new Exception("There is no such field: [" + key + "]");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while entering the customer details: " + e.getMessage());
        }
    }

    public void enterCommAddressDetails(String details) throws Exception {
        try {
            if (commAddrLabel.isDisplayed()) {
                library.focus(library.getDriver(), commAddrLabel);
                HashMap<String, String> hmap = library.getFieldMap(details);

                for (String key : hmap.keySet()) {
                    library.waitForDigitLoad();

                    switch (key.replaceAll(" ", "").toLowerCase()) {
                        case "fulladdress":
                            commAddrTextField.sendKeys(hmap.get(key));
                            break;
                        case "pincode":
                            commPincodeTextField.sendKeys(hmap.get(key));
                            break;
                        default:
                            throw new Exception("There is no such field: [" + key + "]");
                    }
                }
            } else {
                throw new Exception("Communication address section is not visible");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while entering the customer details: " + e.getMessage());
        }
    }

    public boolean visibilityOfQuoteNo() {
        return quoteNoLabel.isDisplayed();
    }

    public boolean visibilityOfProposalDownload() {
        return quoteDownloadButton.isDisplayed();
    }

    public void clickOnYesForFinalUpdate() throws Exception {
        finalSubmitOptionYes.click();
        Thread.sleep(3000);
        library.waitForDigitLoad();
    }

    public void clickOnNoForFinalUpdate() throws Exception {
        finalSubmitOptionNo.click();
        Thread.sleep(500);
        library.waitForDigitLoad();
    }

    public void enterPaymentDetails(String details) throws Exception {
        try {
            library.focus(library.getDriver(), paymentModeLabel);
            HashMap<String, String> hmap = library.getFieldMap(details);

            for (String key : hmap.keySet()) {
                library.waitForDigitLoad();

                switch (key.replaceAll(" ", "").toLowerCase()) {
                    case "paymentmode":
                        String mode = hmap.get(key).trim();

                        String environment;
                        if (System.getProperty("environment") == null || System.getProperty("environment").equals("")) {
                            PropertyFileReader prop = new PropertyFileReader();
                            environment = prop.getEnvironment();
                        } else
                            environment = System.getProperty("environment");
                        if (environment.equalsIgnoreCase("prod") || environment.equalsIgnoreCase("plus"))
                            mode = "Generate Payment Link";

                        switch (mode.replaceAll(" ", "").toLowerCase()) {
                            case "agentfloat":
                                paymentModeDropdown.click();
                                Thread.sleep(1000);
                                library.getDriver()
                                        .findElement(By.xpath("//div[@class='cdk-overlay-pane']/descendant::span[contains(text(), 'Agent Float')]"))
                                        .click();
                                break;
                            case "generatepaymentlink":
                                paymentModeDropdown.click();
                                Thread.sleep(1000);
                                library.getDriver()
                                        .findElement(By.xpath("//div[@class='cdk-overlay-pane']/descendant::span[contains(text(), 'Generate Payment Link')]"))
                                        .click();
                                break;
                            case "sendpaymentlink":
                                paymentModeDropdown.click();
                                Thread.sleep(1000);
                                library.getDriver().findElement(By.xpath("//div[@class='cdk-overlay-pane']/descendant::span[contains(text(), 'Send Payment Link')]"))
                                        .click();
                                break;
                            case "paymentbycustomercheque":
                                paymentModeDropdown.click();
                                Thread.sleep(1000);
                                library.getDriver()
                                        .findElement(By.xpath("//div[@class='cdk-overlay-pane']/descendant::span[contains(text(), 'Payment by Customer Cheque')]"))
                                        .click();
                                library.focus(library.getDriver(), chequeNoLabel);
                                chequeNoTextField.sendKeys(library.getRandomValue("[N | 6]"));
                                ifscTextField.sendKeys("560269002");
                                ifscTextField.sendKeys(Keys.TAB);
                                break;
                            case "paymentbypartnercheque":
                                paymentModeDropdown.click();
                                Thread.sleep(1000);
                                library.getDriver()
                                        .findElement(By.xpath("//div[@class='cdk-overlay-pane']/descendant::span[contains(text(), 'Payment by Partner Cheque')]"))
                                        .click();
                                library.focus(library.getDriver(), chequeNoLabel);
                                chequeNoTextField.sendKeys(library.getRandomValue("[N | 6]"));
                                ifscTextField.sendKeys("560269002");
                                ifscTextField.sendKeys(Keys.TAB);
                                break;
                            default:
                                throw new Exception("The payment mode " + mode + " is not configured for automation");
                        }
                        break;
                    default:
                        throw new Exception("There is no such field: [" + key + "]");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while entering the payment details: " + e.getMessage());
        }
    }

    public boolean verifySuccessPage() {
        return successMessageLabel.isDisplayed();
    }

    public int getPremium() throws Exception {
        if (totalPayableAmtFooterLabel.isDisplayed())
            return library.strToNum(totalPayableAmtFooterLabel.getText());
        else
            throw new Exception("Error in retrieving the premium amount, field is not visible");
    }
}
