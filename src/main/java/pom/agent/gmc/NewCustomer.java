package pom.agent.gmc;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.utilities.Library;


public class NewCustomer {
	
	private static com.utilities.Library library = new com.utilities.Library();

	@FindBy(xpath = "//input[@value='corporate']")
	private WebElement radGroupCorporate;

	@FindBy(xpath = "//input[@value='company']")
	private WebElement radCompany;

	@FindBy(xpath = "//label[text()='Group Corporate Name']/following-sibling::input")
	private WebElement txtGroupCorporateName;
	
	@FindBy(xpath = "//label[text()='Industry Type']/following-sibling::select")
	private WebElement drpIndustryType;

	@FindBy(xpath = "//label[text()='Company Name']/following-sibling::input")
	private WebElement txtCompanyName;

	@FindBy(xpath = "//label[text()='Account Manager Code (IMD)']/following-sibling::input[2]")
	private WebElement txtAccountManagerCode;

	@FindBy(xpath = "//label[text()='Mobile Number']/following-sibling::textarea")
	private WebElement txtMobileNumber;

	@FindBy(xpath = "//label[text()='Email']/following-sibling::textarea")
	private WebElement txtEmail;

	@FindBy(xpath = "//label[text()='Address Line 1']/following-sibling::input")
	private WebElement txtAddressLine1;

	@FindBy(xpath = "//label[contains(text(),'Address Line 2')]/following-sibling::input")
	private WebElement txtAddressLine2;

	@FindBy(xpath = "//label[contains(text(),'Pin Code')]/following-sibling::input")
	private WebElement txtPinCode;
	
	@FindBy(xpath="//label[text()='State']/following-sibling::select")
	private WebElement drpState;
	
	@FindBy(xpath = "//label[contains(text(),'PAN Number')]/following-sibling::input")
	private WebElement txtPanNumber;

	@FindBy(xpath = "//button[contains(text(),'Create')]")
	private WebElement btnCreate;
	
	
	public void enterNewCustomerDetails() throws Exception
	{
		try {
			library.waitForGMCLoading();
			txtGroupCorporateName.sendKeys(library.getRandomValue("[A | 15]"));
			Select select = new Select(drpIndustryType);
			select.selectByValue("Insurance");
			txtAccountManagerCode.sendKeys("1000295");
			txtMobileNumber.sendKeys(library.getRandomValue("[N | 10]"));
			txtEmail.sendKeys("test@godigit.com");
			txtAddressLine1.sendKeys("Address 1");
			txtAddressLine2.sendKeys("Address 2");
			txtPinCode.sendKeys("560001");
			txtPinCode.sendKeys(Keys.TAB);
			select = new Select(drpState);
			select.selectByVisibleText(" Karnataka");
			txtPanNumber.sendKeys("PANNO1234Q");
			Library.staticMap.put("CustomerName", txtGroupCorporateName.getAttribute("value"));
			btnCreate.click();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception("Error while entering the new customer details : "+e.getMessage());
		}
	}

}
