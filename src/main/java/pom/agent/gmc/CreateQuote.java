package pom.agent.gmc;

import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.utilities.DBUtility;
import com.utilities.Library;
import com.utilities.PropertyFileReader;

public class CreateQuote {

	@FindBy(xpath = "//a[@class='close']")
	private WebElement linkCloseInitialPopup;

	@FindBy(id = "new")
	private WebElement btnNewPolicy;

	@FindBy(xpath = "//button[text()='Search']")
	private WebElement btnSearch;

	@FindBy(xpath = "//label[text()='Enter Document Number']/..//input")
	private WebElement txtEnterDocumentNumber;

	@FindBy(xpath = "//label[text()='Search By']/..//mat-select")
	private WebElement listSearchBy;

	@FindBys({@FindBy(xpath = "//span[@class='mat-option-text']")})
	private List<WebElement> valuesInListbox;

	@FindBy(xpath = "//span[text()='New Policy']")
	private WebElement headNewPolicy;

	@FindBy(xpath = "//span[text()='Prospective Policy Details']")
	private WebElement headProspectivePolicyDetails;

	@FindBy(xpath = "//label[text()='Document Type']/..//mat-select")
	private WebElement listDocumentType;

	@FindBy(xpath = "//label[text()='Document Number']/..//input")
	private WebElement txtDocumentNumber;

	@FindBy(xpath = "//label[text()='Policy Holder Name']/..//textarea")
	private WebElement txtPolicyHolderName;

	@FindBy(xpath = "//label[text()='Pincode']/..//input")
	private WebElement txtPincode;

	@FindBy(xpath = "//label[text()='IMD Code']/..//input")
	private WebElement txtIMD;

	@FindBy(xpath = "//label[text()='Pincode']")
	private WebElement labelPincode;

	@FindBy(xpath = "//label[text()='Industry']/..//mat-select")
	private WebElement listIndustry;

	@FindBy(xpath = "//label[text()='Address']/..//textarea")
	private WebElement txtAddress;
	
	@FindBy(xpath = "//label[text()='COVID Renewal']/..//input")
	private WebElement chkCovidRenewal;

	@FindBy(xpath = "//label[text()='Policy Type']/..//mat-select")
	private WebElement listPolicyType;

	@FindBy(xpath = "//label[text()='Family Definition']/..//mat-select")
	private WebElement listFamilyDefinition;

	@FindBy(xpath = "//label[text()='Policy Sum Insured Basis']/..//mat-select")
	private WebElement listPolicySumInsuredBasis;

	@FindBy(xpath = "//label[text()='Policy Inception date']/..//button")
	private WebElement iconPolicyInceptionDate;

	@FindBy(xpath = "//label[text()='Group Business Type']/..//mat-select")
	private WebElement listGroupBusinessType;
	
	@FindBy(xpath = "//label[text()='Previous Master Policy Number']/..//input")
	private WebElement txtPreviousMasterPolicyNumber;
	
	@FindBy(xpath = "//label[text()='Credit Link']/..//mat-select")
	private WebElement listCreditLink;

	@FindBy(xpath = "//label[text()='Number Of Lives']/..//input")
	private WebElement txtNumberOfLives;

	@FindBy(xpath = "//label[text()='Parents Premium Paid By']/..//mat-select")
	private WebElement listParentsPremiumPaidBy;

	@FindBy(xpath = "//label[text()='Premium at the start of the Existing policy(without taxes)']/..//input")
	private WebElement txtPremiumAtStart;

	@FindBy(xpath = "//label[text()='Premium At Expiry(without taxes)']/..//input")
	private WebElement txtPremiumAtExpiry;

	@FindBy(xpath = "//label[text()='No. Of lives at inception']/..//input")
	private WebElement txtNumberOfLivesAtInception;

	@FindBy(xpath = "//label[text()='Number Of Lives At Expiry']/..//input")
	private WebElement txtNumberOfLivesAtExpiry;

	@FindBy(xpath = "//label[contains(text(),'As At Date Of Claims Data')]/..//button")
	private WebElement iconAsOfDateClaimsData;

	@FindBy(xpath = "//label[text()='Claims Paid']/..//input")
	private WebElement txtClaimsPaid;

	@FindBy(xpath = "//label[text()='Claims Outstanding']/..//input")
	private WebElement txtClaimsOutstanding;

	@FindBy(xpath = "//label[text()='Expiring Insurer name']/..//mat-select")
	private WebElement listExpiringInsurerName;

	@FindBy(xpath = "//label[text()='Expiring Broker']/..//input")
	private WebElement txtExpiringBroker;

	@FindBy(xpath = "//label[text()='Expiring Policy Number']/..//input")
	private WebElement txtExpiringPolicyNumber;

	@FindBy(xpath = "//label[text()='Coverages selected are on Expiring terms']/..//mat-select")
	private WebElement listCoveragesSelectedOnExpiringTerms;

	@FindBy(xpath = "//label[text()='PED waived']/..//mat-select")
	private WebElement listPEDWaived;

	@FindBy(xpath = "//label[text()='Pre-Post Hospitalization']/..//mat-select")
	private WebElement listPrePostHospitalization;

	@FindBy(xpath = "//label[text()='CO-PAY %']/..//mat-select")
	private WebElement listCoPay;

	@FindBy(xpath = "//label[text()='Specific Illness Waiting Period']/..//mat-select")
	private WebElement listSpecificIllnessWP;

	@FindBy(xpath = "//label[text()='Accommodation/Room Rent Limits']/..//mat-select")
	private WebElement listRoomRentLimit;

	@FindBy(xpath = "//label[text()='Aayush Cover']/..//mat-select")
	private WebElement listAayushCover;

	@FindBy(xpath = "//label[text()='Organ Donor']/..//mat-select")
	private WebElement listOrganDonor;

	@FindBy(xpath = "//label[text()='Additional Loading for 80-20 CD2 %']/..//mat-select")
	private WebElement listAdditionalLoading;

	@FindBy(xpath = "//label[text()='Employee resignation/ termination clause']/../..//input")
	private WebElement chkEmployeeTerminationClause;

	@FindBy(xpath = "//label[text()='PED Waiting Period (3 Years) Applicable']/..//mat-select")
	private WebElement listPEDWaitingPeriod;

	@FindBy(xpath = "//label[text()='Initital Waiting Period Applicable']/..//mat-select")
	private WebElement listInitialWaitingPeriod;

	@FindBy(xpath = "//label[text()='Specific Illness WP - 2 years']/..//mat-select")
	private WebElement listSpecificIllnessWP2yrs;

	@FindBy(xpath = "//label[text()='Room Rent/ICU Rent Restriction']/..//mat-select")
	private WebElement listRoomRentRestriction;

	@FindBy(xpath = "//label[text()='Proportionate Reduction On Room Rent']/..//mat-select")
	private WebElement listProportionateReductionOnRoomRent;

	@FindBy(xpath = "//label[text()='Ambulance (INR)']/..//mat-select")
	private WebElement listAmbulance;

	@FindBy(xpath = "//label[text()='Parental SI Restriction']/..//mat-select")
	private WebElement listParentalSIRestriction;

	@FindBy(xpath = "//label[text()='Pre/ Post Hospitalization Expenses Days']/..//mat-select")
	private WebElement listHospitalizationExpensesDays;

	@FindBy(xpath = "//label[text()='Parental Co-Pay']/..//mat-select")
	private WebElement listParentalCoPay;

	@FindBy(xpath = "//label[text()='Co Payment (%)']/..//mat-select")
	private WebElement listCoPaymentPercent;

	@FindBy(xpath = "//label[text()='Maternity']/..//mat-select")
	private WebElement listMaternity;

	@FindBy(xpath = "//label[text()='Maternity Limit Normal Delivery']/..//mat-select")
	private WebElement listMaternityLimitNormalDelivery;

	@FindBy(xpath = "//label[text()='Maternity Limit C-Sec Delivery']/..//mat-select")
	private WebElement listMaternityLimitCSecDelivery;

	@FindBy(xpath = "//label[text()='Maternity Coverage']/..//mat-select")
	private WebElement listMaternityCoverage;

	@FindBy(xpath = "//label[text()='Pre-Post Natal Limit']/..//mat-select")
	private WebElement listPrePostNatalLimit;

	@FindBy(xpath = "//label[text()='Maternity 9  months waiting Preriod Applicable']/..//mat-select")
	private WebElement listMaternity9MonthsWaitingPreriodApplicable;

	@FindBy(xpath = "//label[text()='Baby Day-One Cover']/..//mat-select")
	private WebElement listBabyDayOneCover;

	@FindBy(xpath = "//label[text()='Non-network Co-pay']/..//mat-select")
	private WebElement listNonNetworkCoPay;

	@FindBy(xpath = "//label[text()='Disease Capping']/..//mat-select")
	private WebElement listDiseaseCapping;

	@FindBy(xpath = "//label[text()='Corporate Buffer']/..//input")
	private WebElement txtCorporateBuffer;

	@FindBy(xpath = "//label[text()='Corporate Buffer Usage']/..//mat-select")
	private WebElement listCorporateBufferUsage;

	@FindBy(xpath = "//label[text()='Corporate buffer per Family Limit']/..//mat-select")
	private WebElement listCorporateBufferPerFamilyLimit;

	@FindBy(xpath = "//h5[text()='Additional Coverages']")
	private WebElement drpdnAdditionalCoverages;

	@FindBy(xpath = "//label[text()='Infertility Treatment(IPD)']/..//mat-select")
	private WebElement listInfertilityTreatment;

	@FindBy(xpath = "//label[text()='Domiciliary Hospitalization']/..//mat-select")
	private WebElement listDomiciliaryHospitalization;

	@FindBy(xpath = "//label[text()='Aayush Treatment']/..//mat-select")
	private WebElement listAayushTreatment;

	@FindBy(xpath = "//label[text()='Organ Donor Expenses']/..//mat-select")
	private WebElement listOrganDonorExpenses;

	@FindBy(xpath = "//label[text()='Psychiaitric Illness Sublimit']/..//mat-select")
	private WebElement listPsychiaitricIllnessSublimit;

	@FindBy(xpath = "//label[text()='Terrorism']/..//mat-select")
	private WebElement listTerrorism;

	@FindBy(xpath = "//label[text()='External Congenital Conditions']/..//mat-select")
	private WebElement listExternalCongenitalConditions;

	@FindBy(xpath = "//label[text()='Lasik Surgery']/..//mat-select")
	private WebElement listLasikSurgery;

	@FindBy(xpath = "//label[text()='Network Hospital Reimbursement Co-pay']/..//mat-select")
	private WebElement listNetworkHospitalReimbursementCoPay;

	@FindBy(xpath = "//label[contains(text(),'Robotic Surgery')]/..//mat-select")
	private WebElement listRoboticSurgery;

	@FindBy(xpath = "//label[contains(text(),'Gamma Knife')]/..//mat-select")
	private WebElement listGammaKnife;

	@FindBy(xpath = "//label[contains(text(),'Cochlear Implant treatment')]/..//mat-select")
	private WebElement listCochlearImplantTreatment;

	@FindBy(xpath = "//label[text()='Claim Intimation Clause']/..//mat-select")
	private WebElement listClaimIntimationClause;

	@FindBy(xpath = "//label[text()='Document Submission Clause']/..//mat-select")
	private WebElement listDocumentSubmissionClause;

	@FindBy(xpath = "//label[text()='OPD (including health check-up) per family']/..//mat-select")
	private WebElement listOPDPerFamily;

	@FindBy(xpath = "//label[text()='OPD CO-PAY']/..//mat-select")
	private WebElement listOPDCoPay;

	@FindBy(xpath = "//label[contains(text(),'CyberKnife treatment')]/..//mat-select")
	private WebElement listCyberKnifeTreatment;

	@FindBy(xpath = "//label[contains(text(),'Brokerage (%)')]/..//mat-select")
	private WebElement listBrokerage;

	@FindBy(xpath = "//label[contains(text(),'Competitor Quote premium')]/..//input")
	private WebElement txtCompetitorQuotePremium;

	@FindBy(xpath = "//label[contains(text(),'Target premium')]/..//input")
	private WebElement txtTargetPremium;

	@FindBy(xpath = "//label[text()='GST Exemption']/..//mat-select")
	private WebElement listGSTExemption;

	@FindBy(xpath = "//label[text()='Payment Frequency']/..//mat-select")
	private WebElement listPaymentFrequency;

	@FindBy(xpath = "//label[text()='TPA']/..//mat-select")
	private WebElement listTPA;

	@FindBy(xpath = "//label[text()='MEMBER DATA COLLECTION FILE']/../..//input")
	private WebElement btnUploadMemberDataCollectionFile;

	@FindBy(xpath = "//label[text()='MEMBER DATA COLLECTION FILE']/../..//label[contains(text(),'Error')]")
	private WebElement msgErrorInExcel;

	@FindBy(xpath = "//h5[text()=' Demography Details ']")
	private WebElement drpdnDemographyDetails;

	@FindBy(xpath = "//th[contains(text(),'Sum Insured')]/../..//td[1]//mat-select")
	private WebElement listSumInsured;

	@FindBy(xpath = "//th[contains(text(),'Age Band')]/../..//td[2]//mat-select")
	private WebElement listAgeBand;

	@FindBy(xpath = "//th[contains(text(),'No of persons')]/../..//input")
	private WebElement txtNumberOfPersons;

	@FindBy(xpath = "//label[text()='Number Of Members']/..//input")
	private WebElement txtNumberOfMembers;

	@FindBy(xpath = "//label[text()='Document Name']/../..//mat-select")
	private WebElement listDocumentName;

	@FindBy(xpath = "//label[text()='Document Name']/../..//button")
	private WebElement btnUploadDocumentName;

	@FindBy(xpath = "//button[contains(text(),'Calculate Premium')]")
	private WebElement btnCalculatePremium;

	@FindBy(xpath = "//button[contains(text(),'Save')]")
	private WebElement btnSave;

	@FindBy(xpath = "//button[contains(text(),'Submit ₹')]")
	private WebElement btnSubmit;

	@FindBy(xpath = "//button[contains(text(),'Proceed For Binding Quote')]")
	private WebElement btnProceedForBindingQuote;

	@FindBy(xpath = "//button[contains(text(),'Download Quick Quote')]")
	private WebElement btnDownloadQuickQuote;

	@FindBy(xpath = "//button[contains(text(),'Download Binding Quote')]")
	private WebElement btnDownloadBindingQuote;

	@FindBy(xpath = "//button[contains(text(),'Proceed to Pay')]")
	private WebElement btnProceedToPay;

	@FindBy(xpath = "//button[text()='Forward to Underwriter']")
	private WebElement btnForwardToUnderwriter;

	@FindBy(xpath = "//button[contains(text(),'Proceed for Binding')]")
	private WebElement btnProceedForBinding;

	@FindBy(xpath = "//span[text()='Document Details']/../../form/div[1]//button/../input[1]")
	private WebElement btnUpload1stDocumentDetail;

	@FindBy(xpath = "//span[text()='Document Details']/../../form/div[2]//button/../input[1]")
	private WebElement btnUpload2ndDocumentDetail;

	@FindBy(xpath = "//span[text()='Document Details']/../../form/div[3]//button/../input[1]")
	private WebElement btnUpload3rdDocumentDetail;

	@FindBy(xpath = "//span[text()='Document Details']/../../form/div[4]//button/../input[1]")
	private WebElement btnUpload4thDocumentDetail;

	@FindBy(xpath = "//button[text()='OK']")
	private WebElement btnOkFromPopup;

	@FindBy(xpath = "//label[text()='RM Email Id']/..//input")
	private WebElement txtRMEmailId;

	@FindBy(xpath = "//label[text()='Message to Underwriter']/..//textarea")
	private WebElement txtMessageToUnderwriter;

	@FindBys({@FindBy(xpath = "//th[text()='Referral Reasons']/../../../tbody//td[2]")})
	private List<WebElement> msgReferralReason;

	@FindBy(xpath = "//button[text()='Submit']")
	private WebElement btnSubmitFromUWReferralPopup;

	@FindBy(xpath = "//h5[contains(text(),'Quote')]/../h5[2]")
	private WebElement lblQuoteNumber;

	@FindBy(xpath = "//h5[contains(text(),'Warning')]/../../../..//button")
	private WebElement btnOkFromWarningPopup;
	

	private static com.utilities.Library library = new com.utilities.Library();

	public void getCreateNewPolicy() throws Exception {

		library.waitForGMCLoading();
		library.waitForDigitLoad();
		try {
			library.waitForElementVisible(linkCloseInitialPopup, 10, 1000);
			linkCloseInitialPopup.click();
		} catch (Exception e) {
			System.out.println("No popup present.");
		}

		library.waitForGMCLoading();
		library.waitForDigitLoad();
		library.waitForElementVisible(btnNewPolicy, 10, 1000);
		btnNewPolicy.click();
		library.waitForGMCLoading();
		library.waitForDigitLoad();
	}

	public void fillInsuredDetailsGMCLarge(String gmcSalesId) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.getGMCLargeDataFromDB(gmcSalesId);
		PropertyFileReader read = new PropertyFileReader();

		clickDesiredValueInListbox(listDocumentType,dbData[2]);
		if (dbData[3]!=null) {
			txtDocumentNumber.sendKeys(dbData[3]);
		} else {
			System.out.println("Value of Document Number in database is null !!");
		}
		if (dbData[4]!=null) {
			txtPolicyHolderName.sendKeys(dbData[4]);
		} else {
			System.out.println("Value of txtPolicyHolderName in database is null !!");
		}
		if (dbData[5]!=null) {
			txtPincode.sendKeys(dbData[5]);
			txtPincode.sendKeys(Keys.TAB);
		} else {
			System.out.println("Value of txtPincode in database is null !!");
		}
		Thread.sleep(2000);

		library.waitForGMCLoading();
		Thread.sleep(2000);
		if (dbData[7]!=null) {
			txtAddress.sendKeys(dbData[7]);
		} else {
			System.out.println("Value of txtAddress in database is null !!");
		}
		Thread.sleep(2000);
		clickDesiredValueInListbox(listIndustry, dbData[6]);
		try {
			txtIMD.sendKeys(read.getIMD_Code_For_GMC());
			labelPincode.click();
		} catch (Exception e) {

		}
		library.waitForDigitLoad();
	}

	public void fillInsuredDetailsGMCSME(String gmcSalesId) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.getGMCLargeDataFromDB(gmcSalesId);
		PropertyFileReader read = new PropertyFileReader();

		clickDesiredValueInListbox(listDocumentType,dbData[2]);
		if (dbData[3]!=null) {
			enterText(txtDocumentNumber, dbData[3]);
		} else {
			System.out.println("Value of Document Number in database is null !!");
		}
		if (dbData[4]!=null) {
			enterText(txtPolicyHolderName, dbData[4]);
		} else {
			System.out.println("Value of txtPolicyHolderName in database is null !!");
		}
		library.focus(library.getDriver(), btnNewPolicy);
		if (dbData[5]!=null) {
			enterText(txtPincode, dbData[5]);
			txtPincode.sendKeys(Keys.TAB);
			library.waitForDigitLoad();
		} else {
			System.out.println("Value of txtPincode in database is null !!");
		}
		Thread.sleep(2000);

		library.waitForGMCLoading();
		Thread.sleep(2000);
		if (dbData[7]!=null) {
			enterText(txtAddress, dbData[7]);
		} else {
			System.out.println("Value of txtAddress in database is null !!");
		}
		Thread.sleep(2000);
		clickDesiredValueInListbox(listIndustry, dbData[6]);
		try {
			enterText(txtIMD, read.getIMD_Code_For_GMC());
			txtIMD.sendKeys(Keys.TAB);
		} catch (Exception e) {

		}
		library.waitForDigitLoad();
	}

	public void fillProspectiveDetailsGMCLarge(String gmcSalesId) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.getGMCLargeDataFromDB(gmcSalesId);

		try {
			clickDesiredValueInListbox(listPolicyType,dbData[8]);
		} catch (Exception e) {
			library.focus(library.getDriver(), txtPincode);
			clickDesiredValueInListbox(listPolicyType,dbData[8]);
		}
		clickDesiredValueInListbox(listPolicyType,dbData[8]);
		clickDesiredValueInListbox(listFamilyDefinition,dbData[9]);
		clickDesiredValueInListbox(listPolicySumInsuredBasis,dbData[10]);
		clickOnDesiredDateInCalendar(iconPolicyInceptionDate,dbData[11]);
		clickDesiredValueInListbox(listGroupBusinessType,dbData[12]);
		library.focus(library.getDriver(), listPolicyType);
		if (dbData[9].replaceAll(" ", "").toLowerCase().contains("parents")) {
			clickDesiredValueInListbox(listParentsPremiumPaidBy, dbData[14]);
		}
		if (dbData[8].replaceAll(" ", "").toLowerCase().equals("rollover")) {
			fillExpiringPolicyDetailsGMCLarge(dbData);
		}
	}

	public void fillProspectiveDetailsGMCSME(String gmcSalesId) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.getGMCLargeDataFromDB(gmcSalesId);

		
		if ((dbData[0].toLowerCase().equals("yes") || dbData[0].toLowerCase().contains("select")) && !chkCovidRenewal.isSelected()) {
			chkCovidRenewal.click();
		}
		clickDesiredValueInListbox(listPolicyType,dbData[8]);
		clickOnDesiredDateInCalendar(iconPolicyInceptionDate,dbData[11]);
		library.focus(library.getDriver(), txtAddress);
		clickDesiredValueInListbox(listFamilyDefinition,dbData[9]);
		enterText(txtNumberOfMembers, dbData[0]);
		clickDesiredValueInListbox(listGroupBusinessType,dbData[12]);
		if (dbData[0].toLowerCase().equals("yes") || dbData[0].toLowerCase().contains("select")) {
			enterText(txtPreviousMasterPolicyNumber, dbData[0]);
		}
		clickDesiredValueInListbox(listCreditLink,dbData[12]);
	}

	private void fillExpiringPolicyDetailsGMCLarge(String[] dbData) throws Exception {

		if (dbData[15]!=null) {
			enterText(txtPremiumAtStart, dbData[15]);
		} else {
			System.out.println("Value of txtPremiumAtStart in database is null !!");
		}
		if (dbData[15]!=null) {
			enterText(txtPremiumAtExpiry, dbData[16]);
		} else {
			System.out.println("Value of txtPremiumAtExpiry in database is null !!");
		}
		if (dbData[17]!=null) {
			enterText(txtNumberOfLivesAtInception, dbData[17]);
		} else {
			System.out.println("Value of txtNumberOfLivesAtInception in database is null !!");
		}
		if (dbData[18]!=null) {
			enterText(txtNumberOfLivesAtExpiry, dbData[18]);
		} else {
			System.out.println("Value of txtNumberOfLivesAtExpiry in database is null !!");
		}
		clickOnDesiredDateInCalendar(iconAsOfDateClaimsData, dbData[19]);
		if (dbData[20]!=null) {
			enterText(txtClaimsPaid, dbData[20]);
		} else {
			System.out.println("Value of txtClaimsPaid in database is null !!");
		}
		if (dbData[21]!=null) {
			enterText(txtClaimsOutstanding, dbData[21]);
		} else {
			System.out.println("Value of txtClaimsOutstanding in database is null !!");
		}
		library.focus(library.getDriver(), iconAsOfDateClaimsData);
		clickDesiredValueInListbox(listExpiringInsurerName, dbData[22]);
		if (dbData[23]!=null) {
			enterText(txtExpiringBroker, dbData[23]);
		} else {
			System.out.println("Value of txtExpiringBroker in database is null !!");
		}
		if (dbData[24]!=null) {
			enterText(txtExpiringPolicyNumber, dbData[24]);
		} else {
			System.out.println("Value of txtExpiringPolicyNumber in database is null !!");
		}
	}

	public void fillCoverageDetailsGMCLarge(String gmcSalesId) throws InterruptedException, SQLException {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.getGMCLargeDataFromDB(gmcSalesId);
		if (dbData[8].replaceAll(" ", "").toLowerCase().equals("rollover")) {
			clickDesiredValueInListbox(listCoveragesSelectedOnExpiringTerms, dbData[25]);
			library.focus(library.getDriver(), txtExpiringBroker);
		}
		if (!dbData[8].replaceAll(" ", "").toLowerCase().equals("rollover")) {
			library.focus(library.getDriver(), iconPolicyInceptionDate);
		}
		try {
			chkEmployeeTerminationClause.click();
		} catch (Exception e1) {

		}
		clickDesiredValueInListbox(listPEDWaitingPeriod,dbData[26]);
		clickDesiredValueInListbox(listInitialWaitingPeriod,dbData[27]);
		clickDesiredValueInListbox(listSpecificIllnessWP2yrs,dbData[28]);
		if (dbData[8].replaceAll(" ", "").toLowerCase().equals("rollover")) {
			library.focus(library.getDriver(), listCoveragesSelectedOnExpiringTerms);
		}
		clickDesiredValueInListbox(listRoomRentRestriction,dbData[29]);
		clickDesiredValueInListbox(listProportionateReductionOnRoomRent,dbData[30]);
		clickDesiredValueInListbox(listAmbulance,dbData[31]);
		try {
			library.focus(library.getDriver(), chkEmployeeTerminationClause);
		} catch (Exception e1) {

		}
		if (dbData[9].replaceAll(" ", "").toLowerCase().contains("parents")) {
			clickDesiredValueInListbox(listParentalSIRestriction,dbData[72]);
			clickDesiredValueInListbox(listParentalCoPay,dbData[33]);
		}
		clickDesiredValueInListbox(listHospitalizationExpensesDays,dbData[32]);
		try {
			clickDesiredValueInListbox(listCoPaymentPercent,dbData[34]);
		} catch (Exception e1) {
			library.focus(library.getDriver(), listRoomRentRestriction);
			clickDesiredValueInListbox(listCoPaymentPercent,dbData[34]);
		}
		clickDesiredValueInListbox(listMaternity,dbData[35]);
		if (dbData[35].replaceAll(" ", "").toLowerCase().equals("covered")) {
			clickDesiredValueInListbox(listMaternityLimitNormalDelivery,dbData[36]);
			clickDesiredValueInListbox(listMaternityLimitCSecDelivery,dbData[37]);
			clickDesiredValueInListbox(listMaternityCoverage,dbData[38]);
			clickDesiredValueInListbox(listPrePostNatalLimit,dbData[39]);
			try {
				clickDesiredValueInListbox(listMaternity9MonthsWaitingPreriodApplicable,dbData[40]);
			} catch (Exception e) {
				library.focus(library.getDriver(), listRoomRentRestriction);
				clickDesiredValueInListbox(listMaternity9MonthsWaitingPreriodApplicable,dbData[40]);
			}
			clickDesiredValueInListbox(listBabyDayOneCover,dbData[41]);
		}
		Thread.sleep(2000);
		library.focus(library.getDriver(), listPEDWaitingPeriod);
		if (dbData[8].replaceAll(" ", "").toLowerCase().equals("rollover") && !dbData[42].replaceAll(" ", "").toLowerCase().equals("0")) {
			txtCorporateBuffer.clear();
			txtCorporateBuffer.sendKeys(dbData[42]);
			txtCorporateBuffer.sendKeys(Keys.TAB);
			library.focus(library.getDriver(), listMaternity);
			clickDesiredValueInListbox(listCorporateBufferUsage,dbData[43]);
			clickDesiredValueInListbox(listCorporateBufferPerFamilyLimit,dbData[44]);
		} else {
			try {
				clickDesiredValueInListbox(listDiseaseCapping, dbData[73]);
			} catch (Exception e) {

			}
		}
		clickDesiredValueInListbox(listNonNetworkCoPay,dbData[45]);
	}

	public void fillCoverageDetailsGMCSME(String gmcSalesId) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.getGMCLargeDataFromDB(gmcSalesId);

		library.focus(library.getDriver(), listGroupBusinessType);
		clickDesiredValueInListbox(listPEDWaitingPeriod, dbData[25]);
		library.waitForDigitLoad();
		btnOkFromWarningPopup.click();
		clickDesiredValueInListbox(listRoomRentLimit, dbData[25]);
		clickDesiredValueInListbox(listPrePostHospitalization, dbData[25]);
		clickDesiredValueInListbox(listCoPay, dbData[25]);
		clickDesiredValueInListbox(listInitialWaitingPeriod, dbData[25]);
		clickDesiredValueInListbox(listSpecificIllnessWP, dbData[25]);
		clickDesiredValueInListbox(listAayushCover, dbData[25]);
		clickDesiredValueInListbox(listOrganDonor, dbData[25]);
		clickDesiredValueInListbox(listDiseaseCapping, dbData[25]);
		clickDesiredValueInListbox(listMaternity, dbData[25]);
	}

	public void fillAdditionalCoveargesGMCLarge(String gmcSalesId) throws InterruptedException, SQLException {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.getGMCLargeDataFromDB(gmcSalesId);
		
		library.focus(library.getDriver(), listNonNetworkCoPay);
		Thread.sleep(3000);
		drpdnAdditionalCoverages.click();
		try {
			library.focus(library.getDriver(), listNonNetworkCoPay);
		} catch (Exception e) {
		}
		Thread.sleep(3000);
		if (dbData[8].replaceAll(" ", "").toLowerCase().equals("rollover")) {
			clickDesiredValueInListbox(listInfertilityTreatment, dbData[75]);
		}
		clickDesiredValueInListbox(listDomiciliaryHospitalization, dbData[46]);
		clickDesiredValueInListbox(listAayushTreatment, dbData[47]);
		clickDesiredValueInListbox(listOrganDonorExpenses, dbData[48]);
		clickDesiredValueInListbox(listPsychiaitricIllnessSublimit, dbData[49]);
		clickDesiredValueInListbox(listTerrorism, dbData[50]);
		library.focus(library.getDriver(), listTerrorism);
		clickDesiredValueInListbox(listExternalCongenitalConditions, dbData[51]);
		clickDesiredValueInListbox(listLasikSurgery, dbData[52]);
		clickDesiredValueInListbox(listNetworkHospitalReimbursementCoPay, dbData[53]);
		if (dbData[8].replaceAll(" ", "").toLowerCase().equals("rollover")) {
			library.focus(library.getDriver(), listOrganDonorExpenses);
		}
		clickDesiredValueInListbox(listRoboticSurgery, dbData[54]);
		library.focus(library.getDriver(), listPsychiaitricIllnessSublimit);
		clickDesiredValueInListbox(listGammaKnife, dbData[55]);
		clickDesiredValueInListbox(listCochlearImplantTreatment, dbData[56]);
		clickDesiredValueInListbox(listClaimIntimationClause, dbData[57]);
		clickDesiredValueInListbox(listDocumentSubmissionClause, dbData[58]);
		if (dbData[8].replaceAll(" ", "").toLowerCase().equals("rollover")) {
			clickDesiredValueInListbox(listOPDPerFamily, dbData[59]);
			if (!dbData[59].replaceAll(" ", "").toLowerCase().equals("notcovered")) {
				clickDesiredValueInListbox(listOPDCoPay, dbData[60]);
			}
		}
		clickDesiredValueInListbox(listCyberKnifeTreatment, dbData[61]);
	}

	public void fillPremiumDetailsGMCLarge(String  gmcSalesId) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.getGMCLargeDataFromDB(gmcSalesId);

		library.focus(library.getDriver(), listCyberKnifeTreatment);
		Thread.sleep(3000);
		try {
			clickDesiredValueInListbox(listBrokerage, dbData[74]);
			txtCompetitorQuotePremium.clear();
			txtCompetitorQuotePremium.sendKeys(dbData[62]);
			txtTargetPremium.clear();
			txtTargetPremium.sendKeys(dbData[63]);
		} catch (Exception e) {
			// This try-catch is provided for handling exceptions for above elements as they behave differently for different login credentials.
		}
		clickDesiredValueInListbox(listGSTExemption, dbData[64]);
		clickDesiredValueInListbox(listPaymentFrequency, dbData[65]);
		clickDesiredValueInListbox(listTPA, dbData[66]);
		library.focus(library.getDriver(), listTPA);
		Thread.sleep(3000);
		switch (dbData[77].replaceAll(" ", "").replaceAll("_", "").toLowerCase()) {
		case "lessthan50memberreferal":
			btnUploadMemberDataCollectionFile.sendKeys(Paths.get("").toAbsolutePath().toString()
					+ "/src/main/resources/documents/Less_than_50_Member_referal.xlsx");
			break;
		case "lessthan50memberreferal(1cr)":
			btnUploadMemberDataCollectionFile.sendKeys(Paths.get("").toAbsolutePath().toString()
					+ "/src/main/resources/documents/Less_than_50_Member_referal(1cr).xlsx");
			break;
		case "234selfmemberdatatemplate":
			btnUploadMemberDataCollectionFile.sendKeys(Paths.get("").toAbsolutePath().toString()
					+ "/src/main/resources/documents/234_Self_Member_data_Template.xlsx");
			break;
		case "2531+5memberdatatemplateparentless45years":
			btnUploadMemberDataCollectionFile.sendKeys(Paths.get("").toAbsolutePath().toString()
					+ "/src/main/resources/documents/253_1+5_Member_data_template_parent_less_45_years.xlsx");
			break;
		case "gmcagereferralconditions":
			btnUploadMemberDataCollectionFile.sendKeys(Paths.get("").toAbsolutePath().toString()
					+ "/src/main/resources/documents/GMC_Age_referral_Conditions.xlsx");
			break;
		case "gmcagereferralconditions(1cr)":
			btnUploadMemberDataCollectionFile.sendKeys(Paths.get("").toAbsolutePath().toString()
					+ "/src/main/resources/documents/GMC_Age_referral_Conditions(1cr).xlsx");
			break;
		} 
		library.waitForDigitLoad();
		library.waitForGMCLoading();
		Thread.sleep(5000);
		try {
			if (msgErrorInExcel.isDisplayed()) {
				throw new Exception("*************************Error In Excel!!!*************************");
			}
		} catch (NoSuchElementException e) {

		}
	}

	public void fillPremiumDetailsGMCSME(String  gmcSalesId) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.getGMCLargeDataFromDB(gmcSalesId);

		library.focus(library.getDriver(), listMaternity);
		clickDesiredValueInListbox(listAdditionalLoading, dbData[74]);
		clickDesiredValueInListbox(listBrokerage, dbData[74]);
		clickDesiredValueInListbox(listGSTExemption, dbData[74]);
		clickDesiredValueInListbox(listTPA, dbData[74]);
		clickDesiredValueInListbox(listSumInsured, dbData[74]);
		clickDesiredValueInListbox(listAgeBand, dbData[74]);
	}

	public void fillDocumentDetailsGMCLarge(String gmcSalesId) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.getGMCLargeDataFromDB(gmcSalesId);

		library.focus(library.getDriver(), drpdnDemographyDetails);
		btnUpload1stDocumentDetail.sendKeys(Paths.get("").toAbsolutePath().toString() + "/src/main/resources/documents/GMC_Large_Demography.xlsx");
		library.waitForDigitLoad();
		try {
			btnUpload2ndDocumentDetail.sendKeys(Paths.get("").toAbsolutePath().toString() + "/src/main/resources/documents/GMC_Large_Demography.xlsx");
		} catch (Exception e) {

		}
		if (dbData[8].replaceAll(" ", "").toLowerCase().equals("rollover")) {
			btnUpload3rdDocumentDetail.sendKeys(Paths.get("").toAbsolutePath().toString() + "/src/main/resources/documents/GMC_Large_Demography.xlsx");
			library.waitForDigitLoad();
			btnUpload4thDocumentDetail.sendKeys(Paths.get("").toAbsolutePath().toString() + "/src/main/resources/documents/GMC_Large_Demography.xlsx");
		}
		library.waitForDigitLoad();
	}

	private void enterText(WebElement textField, String data) throws Exception {
		library.waitForDigitLoad();
		if (data!=null) {
			textField.clear();
			textField.sendKeys(data);
		}
		library.waitForDigitLoad();
	}
	
	public void clickOnButton(String button,String gmcSalesId) throws Exception {

		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		String formattedDate = myDateObj.format(myFormatObj);
		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		boolean referralFlag = false;
		try {
			if (button.replaceAll(" ", "").toLowerCase().equals("forwardtounderwriter")) {
				library.waitForElementVisible(btnOkFromPopup, 10, 1000);
				btnOkFromPopup.click();
			}
		} catch (Exception e) {
			System.out.println("Referral popup is not present yet.");
		}
		switch(button.replaceAll(" ", "").toLowerCase()) {

		case "calculatepremium":
			if (btnCalculatePremium.isEnabled()) {
				btnCalculatePremium.click();
			} else {
				throw new Exception("*********************Button Calculate Premium is Disabled**********************");
			}
			break;
		case "save":
			if (btnSave.isEnabled()) {
				btnSave.click();
			} else {
				throw new Exception("*********************Button Save is Disabled**********************");
			}
			break;
		case "submit":
			if (btnSubmit.isEnabled()) {
				btnSubmit.click();
			} else {
				throw new Exception("*********************Button Submit is Disabled**********************");
			}
			break;
		case "proceedforbindingquote":
			if (btnProceedForBindingQuote.isEnabled()) {
				btnProceedForBindingQuote.click();
			} else {
				throw new Exception("*********************Button Proceed For Binding Quote is Disabled**********************");
			}
			break;
		case "downloadquickquote":
			if (btnDownloadQuickQuote.isEnabled()) {
				btnDownloadQuickQuote.click();
			} else {
				throw new Exception("*********************Button Download Quick Quote is Disabled**********************");
			}
			break;
		case "downloadbindingquote":
			if (btnDownloadBindingQuote.isEnabled()) {
				btnDownloadBindingQuote.click();
			} else {
				throw new Exception("*********************Button Download Binding Quote is Disabled**********************");
			}
			break;
		case "proceedtopay":
			if (btnProceedToPay.isEnabled()) {
				btnProceedToPay.click();
			} else {
				throw new Exception("*********************Button PROCEED TO PAY is Disabled**********************");
			}
			break;
		case "forwardtounderwriter":
			;
			try {
				btnForwardToUnderwriter.click();
				library.waitForDigitLoad();
				txtRMEmailId.clear();

				String[] dbData = db.getGMCLargeDataFromDB(gmcSalesId);
				txtRMEmailId.sendKeys(dbData[76]);
				txtMessageToUnderwriter.sendKeys("This mail is for Automation Testing purpose!!!");
				String referralReason = "";int num = 0;
				for (WebElement reason : msgReferralReason) {
					referralReason  = referralReason + " || "+ (++num) + ". "+ reason.getText();
				}
				db.storeCommentsInDB(formattedDate + "  ---> " + referralReason, gmcSalesId, "comments", "digit_health.t_health_testdata_gmc_large");
				for (WebElement reason : msgReferralReason) {
					if (!dbData[78].replaceAll(" ", "").toLowerCase().contains(reason.getText().replaceAll(" ", "").toLowerCase())) {
						throw new Exception("Validation Failed !!**********Referral Reasons in UI and the ones in Database do not match**********");
					} 
				}
				System.out.println("Referral Reasons Validation Successfull!!");
				btnSubmitFromUWReferralPopup.click();
				library.waitForDigitLoad();
				library.waitForElementVisible(lblQuoteNumber, 10, 1000);
				db.storeCommentsInDB(formattedDate + "  ---> " + lblQuoteNumber.getText(), gmcSalesId, "quotenum", "digit_health.t_health_testdata_gmc_large");
				Library.staticMap.put("Policy Number", lblQuoteNumber.getText());					//Writing data from database into the Excel-Report
				referralFlag = true;
			} finally {
				if (referralFlag) {
					DBUtility dbLatest = PageFactory.initElements(library.getDriver(), DBUtility.class);
					String[] writeInReport = dbLatest.getGMCLargeDataFromDB(gmcSalesId);
					db.storeCommentsInDB(formattedDate+"  ---> Yes", gmcSalesId, "status", "digit_health.t_health_testdata_gmc_large");
					String uiReasons = writeInReport[79].split("--->  ")[1], dbReasons = writeInReport[78];
					Library.staticMap.put("Actual", uiReasons);										//Writing data from database into the Excel-Report
					Library.staticMap.put("Expected", dbReasons);									//Writing data from database into the Excel-Report
				} else {
					db.storeCommentsInDB(formattedDate+"  ---> No", gmcSalesId, "status", "digit_health.t_health_testdata_gmc_large");
				}
			}
			break;
		case "proceedforbindingquote/issuance":
			btnProceedForBinding.click();
			break;
		}
		library.waitForDigitLoad();
	}
	

	private void clickDesiredValueInListbox(WebElement listbox,String value) throws InterruptedException {
		if (value!=null) {
			library.waitForElementVisible(listbox, 10, 1000);
			listbox.click();
			Thread.sleep(1000);
			for (WebElement element : valuesInListbox) {
				if (element.getText().replaceAll(" ", "").replaceAll(",", "").toLowerCase()
						.contains(value.replaceAll(" ", "").replaceAll(",", "").toLowerCase())) {
					element.click();
					break;
				}
			} 
			Thread.sleep(1000);
		}
	}

	private void clickOnDesiredDateInCalendar(WebElement iconCalendar,String date) throws InterruptedException {

		iconCalendar.click();
		String dateTxt = date;
		Calendar cal = Calendar.getInstance(); 
		SimpleDateFormat s = new SimpleDateFormat("d-MM-yyyy"); 
		switch(date.toLowerCase().trim()) {
		case "today":
			date = s.format(cal.getTime());
			library.getDriver().findElement(By.xpath("//div[text()='"+date.split("-")[0]+"']")).click();
			break;
		case "tomorrow":
			cal.add(Calendar.DATE, 1);
			date = s.format(cal.getTime());
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//button[contains(@class , 'mat-calendar-period-button')]")).click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[text()='"+date.split("-")[2]+"']")).click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[text()='"+monthFlag(date)+"']")).click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[text()='"+Integer.parseInt(date.split("-")[0])+"']")).click();
			break;
		default:
			date = date.trim().toLowerCase().split(" ")[0];
			if (dateTxt.toLowerCase().contains("from")) {
				cal.add(Calendar.DATE, Integer.parseInt(date));
			} else {
				cal.add(Calendar.DATE, (-Integer.parseInt(date)-1));
			}
			date = s.format(cal.getTime());
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//button[contains(@class , 'mat-calendar-period-button')]"))
			.click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[text()='" + date.split("-")[2] + "']")).click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[text()='" + monthFlag(date) + "']")).click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[@class='mat-calendar-body-cell-content' and text()='"
					+ Integer.parseInt(date.split("-")[0]) + "']")).click();
			break;
		}
	}

	private String monthFlag(String date) {
		String month = "";
		switch(date.split("-")[1]) {
		case "01":
			month = "JAN";
			break;
		case "02":
			month = "FEB";
			break;
		case "03":
			month = "MAR";
			break;
		case "04":
			month = "APR";
			break;
		case "05":
			month = "MAY";
			break;
		case "06":
			month = "JUN";
			break;
		case "07":
			month = "JUL";
			break;
		case "08":
			month = "AUG";
			break;
		case "09":
			month = "SEPT";
			break;
		case "10":
			month = "OCT";
			break;
		case "11":
			month = "NOV";
			break;
		case "12":
			month = "DEC";
			break;
		}
		return month;
	}
}
