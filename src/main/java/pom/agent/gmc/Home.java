package pom.agent.gmc;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.utilities.DBUtility;
import com.utilities.PropertyFileReader;

public class Home {

	@FindBys({@FindBy(xpath = "//span[@class='mat-option-text']")})
	private List<WebElement> valuesInListbox;

	@FindBy(xpath="//a[contains(text(),'New Customer')]")
	private WebElement linkNewCustomer;

	@FindBy(xpath="//a[contains(text(),'New Quote')]")
	private WebElement linkNewQuote;

	@FindBy(xpath="//a[contains(text(),'New Tariff')]")
	private WebElement linkNewTariff;

	@FindBy(xpath="//button[contains(text(),'Choose Document')]/..//input")
	private WebElement btnUploadTarrifDocument;

	@FindBy(xpath="//button[contains(text(),'Choose Document')]/..//input")
	private WebElement btnUploadMemberDocument;

	@FindBy(xpath="//button[contains(text(),'Next')]")
	private WebElement btnNext;

	@FindBy(xpath="//label[contains(text(),'Sum Insured')]")
	private WebElement labelSumInsured;

	@FindBy(xpath="//span[contains(text(),'Group Corporate')]/..//span")
	private WebElement radGroupCorporate;

	@FindBy(xpath="//span[contains(text(),'Company')]/..//span")
	private WebElement radCompany;

	@FindBy(xpath="//label[contains(text(),'Group Corporate Name')]/..//input")
	private WebElement txtGroupCorporateName;

	@FindBy(id="quoteCustomerName")
	private WebElement txtGroupCorporateOrCompanyName;

	@FindBy(xpath="//label[contains(text(),'No.of Members')]/..//input")
	private WebElement txtNumberOfMembers;

	@FindBy(xpath="//label[contains(text(),'GST (%)')]/..//input")
	private WebElement txtGSTPercentage;

	@FindBy(xpath="//label[contains(text(),'Base Premium (₹)')]/..//input")
	private WebElement txtBasePremium;

	@FindBy(xpath="//label[contains(text(),'Max Age')]/..//input")
	private WebElement txtMaxAge;

	@FindBy(name="industryType")
	private WebElement listIndustry;

	@FindBy(xpath="//select[@formcontrolname='state']")
	private WebElement state;

	@FindBy(xpath="//select[@formcontrolname='state']")
	private WebElement listChildCompany;

	@FindBy(xpath="//select[@formcontrolname='groupType']")
	private WebElement listGroupType;

	@FindBy(xpath="//select[@formcontrolname='creditLink']")
	private WebElement listCreditLink;

	@FindBy(xpath="//select[@formcontrolname='policyType']")
	private WebElement listPolicyType;

	@FindBy(xpath="//select[@formcontrolname='product']")
	private WebElement listProduct;

	@FindBy(xpath="//select[@formcontrolname='famiilyDefinition']")
	private WebElement listFamilyDefinition;

	@FindBy(xpath="//label[contains(text(),'TPA')]/../..//select")
	private WebElement listTPA;

	@FindBy(xpath="//label[contains(text(),'Company Name')]/..//input")
	private WebElement txtCompanyName;

	@FindBy(xpath="//label[contains(text(),'Group Corporate / Company Name')]/..//input")
	private WebElement txtGrpCorpCompanyName;

	@FindBy(xpath="//label[contains(text(),'Account Manager Code (IMD)')]/..//input[2]")
	private WebElement txtAccountManagerCode_IMD;

	@FindBy(xpath="//label[contains(text(),'Mobile Number')]/..//textarea")
	private WebElement txtMobileNumber;

	@FindBy(xpath="//label[contains(text(),'Email')]/..//textarea")
	private WebElement txtEmail;

	@FindBy(xpath="//label[contains(text(),'Address Line 1')]/..//input")
	private WebElement txtAddressLine1;

	@FindBy(xpath="//label[contains(text(),'Pin Code')]/..//input")
	private WebElement txtPincode;

	@FindBy(xpath="//label[contains(text(),'GST Number')]/..//input")
	private WebElement txtGSTNumber;

	@FindBy(xpath="//label[contains(text(),'PAN Number')]/..//input")
	private WebElement txtPAN;

	@FindBy(xpath="//input[@formcontrolname = 'masterPolicyNumber']")
	private WebElement txtMasterPolicyNumber;

	@FindBy(xpath="//input[@formcontrolname = 'fleetId']")
	private WebElement txtFleetID;

	@FindBy(xpath="//button[contains(text(),'Create')]")
	private WebElement btnCreate;

	@FindBy(xpath="//span[@class='rectangle']")
	private WebElement linkProfileImage;

	@FindBy(xpath="//a[contains(text(),'Logout')]")
	private WebElement linkLogout;

	@FindBy(xpath="//span[contains(text(),'Create')]")
	private WebElement btnCreateAlternate;

	@FindBy(xpath="//div[@id='main_page']//div[1]/div[1]/div[1]/div[1]/a/img")
	private WebElement backArrow;

	@FindBy(xpath="//button[contains(text(),'Submit')]")
	private WebElement btnSubmit;

	@FindBy(xpath="//div[contains(text(),'ERSON')]")
	private WebElement linkRAPPerson;

	@FindBy(xpath="//div[@testid='suche_name']/input")
	private WebElement txtRAPName;

	@FindBy(xpath="//div[text()='Legal']")
	private WebElement radRAPLegal;

	@FindBy(xpath="//div[text()='Search']")
	private WebElement btnRAPSearch;
	
	@FindBy(xpath="//div[@testid='cb_suchen']//div[text()='Search']")
	private WebElement btnRAPTPAPersonSearch;

	@FindBy(xpath="//div[@testid='name']/input")
	private WebElement labelRAPCompanyName;

	@FindBy(xpath="//div[text()='Event display']/../../..//div[text()='Close']")
	private WebElement closeEventDisplayPopup;

	@FindBy(xpath="//input[@testid='name']")
	private WebElement labelRAPName;

	@FindBy(xpath="//div[contains(@testid,'ABS-Core')]/following-sibling::div[1]/div[1]/div[1]/div[5]/div")
	private WebElement maxmizeWindowCompanyName;

	@FindBy(xpath="//div[contains(@testid,'Group Business File')]/div[1]/div[1]/div[5]/div")
	private WebElement maxmizeWindowGroupBusinessFile;

	@FindBy(xpath="//div[contains(@testid,'Accounting file')]/div[1]/div[1]/div[5]/div")
	private WebElement maxmizeWindowAccountingFile;

	@FindBy(xpath="//div[text()='File']")
	private WebElement optionFile;

	@FindBy(xpath="//div[text()='Edit']")
	private WebElement optionEdit;

	@FindBy(xpath="//div[text()='Modify']")
	private WebElement optionModify;

	@FindBy(xpath="//div[text()='Contract Group Business']")
	private WebElement optionContractGroupBusiness;

	@FindBy(xpath="//div[text()='Save']")
	private WebElement optionSave;

	@FindBy(xpath="//div[text()='Save + Close']")
	private WebElement optionSaveAndClose;

	@FindBy(xpath="//div[text()='Actions']")
	private WebElement optionAction;

	@FindBy(xpath="//div[text()='Open file search ...']/../..//div[text()='Close']")
	private WebElement optionClose;

	@FindBy(xpath="//div[text()='leet']/span[text()='F']")
	private WebElement fleetTab;

	@FindBy(xpath="//div[text()='New']")
	private WebElement btnRAPNewFleet;

	@FindBy(xpath="(//div[text()='Fleetnr.']/../../..//div[@class='textOverflowWithEllipsis'])[1]")
	private WebElement labelRAPFleetNumber;

	@FindBy(xpath="//div[text()='etail']/span[text()='D']")
	private WebElement labelRAPTabDetail;

	@FindBy(xpath="//div[@testid='begin']/input")
	private WebElement txtRAPFleetStartDate;

	@FindBy(xpath="(//div[@testid='date_picker_popup']//div[text()='1'])[1]")
	private WebElement firstDateOfMonth;

	@FindBy(xpath="//div[text()='ain classes']/span[text()='M']")
	private WebElement labelRAPTabMainClasses;

	@FindBy(xpath="//div[text()='Cla']/span[text()='s']")
	private WebElement labelRAPTabClasses;

	@FindBy(xpath="//div[@testid='cb_sachzusatzneu']/div[text()='Add']")
	private WebElement btnRAPAddMainClasses;

	@FindBy(xpath="//div[@testid='btnAdd']/div[2]")
	private WebElement btnRAPAddObject;
	
	@FindBy(xpath="//div[@testid='btnAdd']/div[1]")
	private WebElement btnRAPAddObjectAlt;

	@FindBy(xpath="//div[@testid='dw_person_flotten_sach_zusatz']/div/div/div/div/div")
	private WebElement listRAPMainClasses;

	@FindBy(xpath="//div[@testid='hsp_eigen.control']//input")
	private WebElement txtRAPMainClasses;

	@FindBy(xpath="//div[text()='Addit.agreement']/../..//div[text()='Add']")
	private WebElement btnRAPAddClasses;

	@FindBy(xpath="//div[@testid='maxsora.control']")
	private WebElement boxRAPMaxSr;

	@FindBy(xpath="//div[@testid='mindpr.control']")
	private WebElement boxRAPMindPr;

	@FindBy(xpath="//div[@testid='sparte_eigen.control']//input")
	private WebElement txtRAPSparte;
	
	@FindBy(xpath="//div[text()='MAX. SR']/../following-sibling::div[2]/div")
	private WebElement fieldRAPMaxSr;

	@FindBy(xpath="//div[@testid='maxsora.control']/input")
	private WebElement txtRAPMaxSr;
	
	@FindBy(xpath="//div[text()='MAX. SR']/../following-sibling::div[4]")
	private WebElement fieldRAPMindPr;

	@FindBy(xpath="//div[@testid='mindpr.control']/input")
	private WebElement txtRAPMindPr;

	@FindBy(xpath="(//div[text()='ccounts']/span[text()='A'])[1]")
	private WebElement labelRAPTabAccounts;

	@FindBy(xpath="//div[@testid = 'kontonr']/../../div/div/div/div[2]/div")
	private WebElement labelRAPCustomerAccount;

	@FindBy(xpath="//div[text()='ollection']/span[text()='C']")
	private WebElement labelRAPTabCollection;

	@FindBy(xpath="//div[text()='Collect. type']/../../div[2]//input")
	private WebElement txtRAPCollectionType;

	@FindBy(xpath="//div[text()='Yes']")
	private WebElement btnRAPCollectionChangeYes;

	@FindBy(xpath="//div[text()='Open file booking ...']")
	private WebElement optionFileBoooking;

	@FindBy(xpath="//div[contains(@testid,'dropDownBookingType')]//input")
	private WebElement txtBookingType;

	@FindBy(xpath="//div[contains(@testid,'textAmountTotal')]//input")
	private WebElement txtBookingAmount;

	@FindBy(xpath="//div[text()='Contract account']")
	private WebElement labelRAPAccountType;

	@FindBy(xpath="//textarea")
	private WebElement txtRAPAccountType;

	@FindBy(xpath="//div[@testid='table']/div[last()]/input")
	private WebElement txtRAPCustomerAccount;

	@FindBy(xpath="//div[@testid='table']/div[last()]/input")
	private WebElement txtRAPBookingAmountInTable;

	@FindBy(xpath="//div[text()='Book']")
	private WebElement btnRAPBook;

	@FindBy(xpath="//div[text()='OK']")
	private WebElement btnRAPPopupOK;

	@FindBy(xpath="//div[text()='Close']")
	private WebElement btnRAPPopupClose;

	@FindBy(xpath="//div[text()='Product']")
	private WebElement labelRAPProductTab;

	@FindBy(xpath="//div[@testid='tableClasses']/../../..//div[text()='Add']")
	private WebElement btnRAPAddInProductPopup;

	@FindBy(xpath="//div[text()='Edit coverage']")
	private WebElement btnRAPEditCoverageInGroupBusinessFile;

	@FindBy(xpath="//div[text()='Add object class']")
	private WebElement btnRAPAddObjectClassInGroupBusinessFile;

	@FindBy(xpath="//div[text()='Add product package']")
	private WebElement btnRAPAddProductPackageInGroupBusinessFile;

	@FindBy(xpath="//div[text()='Add classes']")
	private WebElement btnRAPAddClassesInGroupBusinessFile;

	@FindBy(xpath="//div[@testid='txtDescription']/input")
	private WebElement txtRAPObjectClassDescriptionInGroupBusinessFile;

	@FindBy(xpath="//div[@testid='groupProductPackage']//div[@testid='txtDescription']/input")
	private WebElement txtRAPProductPackageDescriptionInGroupBusinessFile;
	
	@FindBy(xpath="//div[@testid='dropDownObjecttype']//input")
	private WebElement txtObjectType;

	@FindBy(xpath="//div[@testid='suchwert']/input")
	private WebElement txtRAPProductSearch;

	@FindBy(xpath="//div[@testid='zahlweise']//input")
	private WebElement txtRAPPayFrequencyInGroupBusinessFile;

	@FindBy(xpath="//div[@testid='flottennr']//input")
	private WebElement txtRAPFleetNoInGroupBusinessFile;

	@FindBy(xpath="//div[@testid='famliyDefination']//input")
	private WebElement txtRAPFamilyDefInGroupBusinessFile;

	@FindBy(xpath="//div[@testid='dGSTException']//input")
	private WebElement txtRAPGSTExemptionInGroupBusinessFile;

	@FindBy(xpath="//div[@testid='groupType']//input")
	private WebElement txtRAPGroupTypeInGroupBusinessFile;

	@FindBy(xpath="//div[@testid='dropDownObjecttype']//input")
	private WebElement txtRAPObjectTypeInGroupBusinessFile;

	@FindBy(xpath="//div[@testid='tableAssignedObjects']/div/div/div/div[6]/div")
	private WebElement chkRAPObjectTypeInAddObjects;

	@FindBy(xpath="//div[@testid='tableClasses']//img/..")
	private WebElement chkRAPProductInAddObjects;

	@FindBy(xpath="//div[text()='New Instance']/../../div[3]")
	private WebElement chkRAPNewInstanceInpatientHospitalizationCollective;

	@FindBy(xpath="//div[text()='Hospitalization Cover']/../../div[3]")
	private WebElement chkRAPHospitalizationCoverInpatientHospitalizationCollective;

	@FindBy(xpath="//div[text()='Accidental and Illness Hospitalization Cover']/../../div[3]")
	private WebElement chkRAPAccidentalAndIllnessHospitalizationCoverInpatientHospitalizationCollective;

	@FindBy(xpath="(//div[text()='New Instance']/../../../../..//input)[1]")
	private WebElement txtRAPNewInstanceSI;

	@FindBy(xpath="(//div[text()='Accidental and Illness Hospitalization Cover']/../../../../..//input)[1]")
	private WebElement txtRAPAccidentalAndIllnessHospitalizationCoverSI;

	@FindBy(xpath="//div[@testid='center']/div[1]//div[@testid='name']/input")
	private WebElement txtRAPFirstPersonAfterSearchInGroupBusinessFile;

	@FindBy(xpath="//div[@testid='name']/input")
	private WebElement txtRAPContractCharacteristicsNameInGroupBusinessFile;

	@FindBy(xpath="//div[text()='ist']/span[text()='L']")
	private WebElement labelRAPListTabInGroupBusinessFile;

	@FindBy(xpath="//div[text()='PH signature']/../../div[2]/div[1]")
	private WebElement chkPHSignature;

	@FindBy(xpath="//div[@testid = 'status']/div[2]/div[1]")
	private WebElement chkForwardBO;

	@FindBy(xpath="//div[text()='Information']/../following-sibling::div[2]/div")
	private WebElement msgMasterPolicyInfo;

	@FindBy(xpath="//a[contains(text(),'View Child Policy')]")
	private WebElement linkViewChildPolicy;

	@FindBy(xpath="//a[contains(text(),'Add Child Policies')]")
	private WebElement linkAddChildPolicies;

	@FindBy(xpath="//button[contains(text(),'Map Data')]")
	private WebElement btnMapData;

	@FindBy(xpath="//button[contains(text(),'Calculate Premium')]")
	private WebElement btnCalculatePremium;

	@FindBy(xpath="//strong[contains(text(),'Total Summary')]")
	private WebElement labelTotalSummary;

	@FindBy(xpath="//button[contains(text(),'Save and Proceed')]")
	private WebElement btnSaveAndProceed;

	@FindBy(xpath="//div[contains(text(),'Do you want to save and proceed')]/../div[2]/button[contains(text(),'OK')]")
	private WebElement btnOkSaveAndProceed;


	private static com.utilities.Library library = new com.utilities.Library();
	static String groupCorporateOrCompanyName,fleetNumberFromRAP,customerAccountFromRAP,masterPolicyNumber;
	Random random = new Random();

	public void createNewCustomer(String gmc_Ops_NewCustomerId) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.get_GMC_Ops_DataFromDB("New Customer",gmc_Ops_NewCustomerId);


		//library.waitForGMCLoading();
		library.waitForDigitLoad();
		clickOnElement(linkNewCustomer);
		switch(dbData[0].replaceAll(" ", "").toLowerCase()) {

		case "groupcorporate":
			if (!radGroupCorporate.isSelected()) {
				clickOnElement(radGroupCorporate);
			}
			String groupCorporateName = dbData[1].split("Name")[0]+"Name"+random.nextInt(10000)+"XYZ"+random.nextInt(1000);
			enterText(txtGroupCorporateName, groupCorporateName);
			groupCorporateOrCompanyName = groupCorporateName;
			db.storeDataInNonABS_Preprod_DB(groupCorporateName, gmc_Ops_NewCustomerId, "corporatename", "digit_health.t_testdata_gmc_ops_newcustomer");
			db.storeDataInNonABS_Preprod_DB("NA", gmc_Ops_NewCustomerId, "companyname", "digit_health.t_testdata_gmc_ops_newcustomer");
			//			listIndustry.click();waitABitInUI();
			//			for (WebElement option : allIndustries) {
			//				if (option.getText().replaceAll(" ", "").toLowerCase().equals(dbData[2].replaceAll(" ", "").toLowerCase())) {
			//					option.click();
			//					break;
			//				}
			//			}
			clickDesiredValueInListbox(listIndustry, dbData[2]);
			break;
		case "company":
			if (!radCompany.isSelected()) {
				clickOnElement(radCompany);
			}
			String companyName = dbData[3].split("Name")[0]+"Name"+random.nextInt(10000)+"ABC"+random.nextInt(1000);
			enterText(txtCompanyName, companyName);
			groupCorporateOrCompanyName = companyName;
			db.storeDataInNonABS_Preprod_DB(companyName, gmc_Ops_NewCustomerId, "companyname", "digit_health.testdata_gmc_ops_newcustomer");
			db.storeDataInNonABS_Preprod_DB("NA", gmc_Ops_NewCustomerId, "corporatename", "digit_health.testdata_gmc_ops_newcustomer");
			break;
		}

		enterText(txtAccountManagerCode_IMD, dbData[4]);
		txtAccountManagerCode_IMD.sendKeys(Keys.TAB);
		enterText(txtMobileNumber, dbData[5]);
		library.focus(library.getDriver(), txtAccountManagerCode_IMD);
		enterText(txtEmail, dbData[6]);
		enterText(txtAddressLine1, dbData[7]);
		library.focus(library.getDriver(), txtAddressLine1);
		enterText(txtPincode, dbData[8]);
		txtPincode.sendKeys(Keys.TAB);waitABitInUI();
		clickOnElement(txtPincode);
		clickOnElement(state);
		Select select = new Select(state);
		select.selectByIndex(1);
		enterText(txtGSTNumber, dbData[9]);
		enterText(txtPAN, dbData[10]);
		try {
			clickOnElement(btnCreate);
		} catch (Exception e) {
			clickOnElement(btnCreateAlternate);
		}
		//library.waitForGMCLoading();
		library.waitForDigitLoad();
	}
	static String productFlag;
	public void createNewQuote(String gmc_Ops_NewQuoteId) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.get_GMC_Ops_DataFromDB("New Quote",gmc_Ops_NewQuoteId);
		//		Login login = PageFactory.initElements(library.getDriver(), Login.class);
		//		Actions act = new Actions(library.getDriver());
		//		Thread.sleep(2000);
		//		act.moveToElement(linkProfileImage).build().perform();
		//		Thread.sleep(2000);
		//		linkLogout.click();
		//		//library.waitForGMCLoading();
		//		library.waitForDigitLoad();
		//		login.login();Thread.sleep(3000);
		//
		//		//library.waitForGMCLoading();
		library.waitForDigitLoad();
		waitABitInUI();
		clickOnElement(linkNewQuote);
		clickOnElement(txtGroupCorporateOrCompanyName);
		enterText(txtGroupCorporateOrCompanyName, groupCorporateOrCompanyName);
		Thread.sleep(5000);txtGroupCorporateOrCompanyName.click();waitABitInUI();
		txtGroupCorporateOrCompanyName.sendKeys(Keys.ARROW_DOWN);waitABitInUI();
		txtGroupCorporateOrCompanyName.sendKeys(Keys.ENTER);waitABitInUI();
		db.storeDataInNonABS_Preprod_DB(groupCorporateOrCompanyName, gmc_Ops_NewQuoteId, "gcorcompanyname", "digit_health.testdata_gmc_ops_newquote");
		//		clickDesiredValueInListbox(listChildCompany, dbData[1]);
		clickDesiredValueInListbox(listGroupType, dbData[2]);
		if (dbData[2].equals("Non-Employer Employee")) {
			clickDesiredValueInListbox(listCreditLink, dbData[3]);
		}
		clickDesiredValueInListbox(listPolicyType, dbData[4]);
		clickDesiredValueInListbox(listProduct, dbData[5]);
		productFlag = dbData[5];
		enterText(txtNumberOfMembers, dbData[6]);
		enterText(txtGSTPercentage, dbData[7]);
		enterText(txtBasePremium, dbData[8]);
		txtBasePremium.sendKeys(Keys.TAB);
		library.focus(library.getDriver(), txtBasePremium);
		enterText(txtMaxAge, dbData[9]);
		clickDesiredValueInListbox(listFamilyDefinition, dbData[10]);
		Thread.sleep(2000);
		try {
			clickOnElement(btnCreateAlternate);
		} catch (Exception e) {
			clickOnElement(btnCreate);
		}
		//library.waitForGMCLoading();
		library.waitForDigitLoad();
	}

	public void createMasterPolicyinRAP(String gmc_Ops_RAP_Id) throws Exception {

			DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
			String[] dbData = db.get_GMC_Ops_DataFromDB("New Quote","1");
			PropertyFileReader read = new PropertyFileReader();

			Login login = PageFactory.initElements(library.getDriver(), Login.class);
			Actions act = new Actions(library.getDriver());
			Thread.sleep(2000);
			try {
				act.moveToElement(linkProfileImage).build().perform();
				Thread.sleep(2000);
				//		clickOnElementInRAP(linkLogout);
				linkLogout.click();
			} catch (Exception e2) {
				try {
					for (int i = 0; i < 100; i++) {
						enterText(txtGroupCorporateOrCompanyName, groupCorporateOrCompanyName);
						enterText(txtNumberOfMembers, dbData[6]);
						clickOnElementInRAP(btnCreateAlternate);
					}
				} catch (Exception e) {
					library.waitForDigitLoad();
					Thread.sleep(2000);
					act.moveToElement(linkProfileImage).build().perform();
					Thread.sleep(2000);
					linkLogout.click();
				}
			}
			//library.waitForGMCLoading();
			library.waitForDigitLoad();

			Thread.sleep(2000);
			library.getDriver().get(read.getAbsPreprodRAPUrl());
			String flag = "rapLogin";System.out.println(flag);
			login.rapLogin();
			Thread.sleep(3000);
			flag = "linkRAPPerson";System.out.println(flag);
			library.waitForElementVisible(linkRAPPerson, 20, 1000);
			flag = "linkRAPPerson";System.out.println(flag);
			library.waitForElementToBeClickable(linkRAPPerson, 20, 1000);
			flag = "linkRAPPerson";System.out.println(flag);
			clickOnElementInRAP(linkRAPPerson);
			library.waitForABSLoading();
			Thread.sleep(2000);
			try {
				flag = "radRAPLegal";System.out.println(flag);
				clickOnElementInRAP(radRAPLegal);
			} catch (Exception e) {
				flag = "linkRAPPerson";System.out.println(flag);
				clickOnElementInRAP(linkRAPPerson);
				library.waitForABSLoading();
				Thread.sleep(2000);
				flag = "radRAPLegal";System.out.println(flag);
				clickOnElementInRAP(radRAPLegal);
			}


			flag = "txtRAPName";System.out.println(flag);
			txtRAPName.clear();
			flag = "txtRAPName";System.out.println(flag);
			clickOnElementInRAP(txtRAPName);
			flag = "txtRAPName";System.out.println(flag);
			clickOnElementInRAP(txtRAPName);

			flag = "txtRAPName";System.out.println(flag);
			txtRAPName.sendKeys(groupCorporateOrCompanyName);waitABitInRAP();

			flag = "btnRAPSearch";System.out.println(flag);
			clickOnElementInRAP(btnRAPSearch);
			library.waitForABSLoading();
			flag = "labelRAPCompanyName";System.out.println(flag);
			clickOnElementInRAP(labelRAPCompanyName);
			flag = "labelRAPCompanyName";System.out.println(flag);
			labelRAPCompanyName.sendKeys(Keys.ENTER);
			library.waitForABSLoading();waitABitInRAP();
			flag = "maxmizeWindowCompanyName";System.out.println(flag);
			clickOnElementInRAP(maxmizeWindowCompanyName);
			flag = "optionFile";System.out.println(flag);
			clickOnElementInRAP(optionFile);
			flag = "optionModify";System.out.println(flag);
			clickOnElementInRAP(optionModify);
			flag = "fleetTab";System.out.println(flag);
			clickOnElementInRAP(fleetTab);
			flag = "btnRAPNewFleet";System.out.println(flag);
			clickOnElementInRAP(btnRAPNewFleet);
			flag = "labelRAPFleetNumber";System.out.println(flag);
			fleetNumberFromRAP = labelRAPFleetNumber.getText().trim();
			flag = "labelRAPTabDetail";System.out.println(flag);
			clickOnElementInRAP(labelRAPTabDetail);
//		Calendar cal = Calendar.getInstance(); 
//		SimpleDateFormat s = new SimpleDateFormat("MM/dd/yyyy"); 
//		String[] split = s.format(cal.getTime()).split("/");
//		split[1] = "01";
//		String date = split[0]+"/"+split[1]+"/"+split[2];waitABitInRAP();
			flag = "txtRAPFleetStartDate";System.out.println(flag);
			act.doubleClick(txtRAPFleetStartDate).build().perform();waitABitInRAP();waitABitInRAP();
			flag = "firstDateOfMonth";System.out.println(flag);
			firstDateOfMonth.click();waitABitInRAP();






			flag = "labelRAPTabMainClasses";System.out.println(flag);
			clickOnElementInRAP(labelRAPTabMainClasses);
			flag = "btnRAPAddMainClasses";System.out.println(flag);
			clickOnElementInRAP(btnRAPAddMainClasses);
			flag = "listRAPMainClasses";System.out.println(flag);
			clickOnElementInRAP(listRAPMainClasses);
//		enterTextInRAP(txtRAPMainClasses, "Health");waitABitInRAP();
			flag = "txtRAPMainClasses";System.out.println(flag);
			enterTextInRAPWithoutActions(txtRAPMainClasses, "Health");
//			txtRAPMainClasses.sendKeys("Health",Keys.TAB);waitABitInRAP();
//		clickOnElementInRAP(labelRAPTabClasses);
			try {
				flag = "labelRAPTabClasses";System.out.println(flag);
				act.doubleClick(labelRAPTabClasses).build().perform();waitABitInRAP();
				flag = "btnRAPAddClasses";System.out.println(flag);
				clickOnElementInRAP(btnRAPAddClasses);
			} catch (Exception e2) {
				try {
					flag = "closeEventDisplayPopup";System.out.println(flag);
					clickOnElementInRAP(closeEventDisplayPopup);
				} catch (Exception e) {}
				flag = "txtRAPMainClasses";System.out.println(flag);
				txtRAPMainClasses.click();
				flag = "txtRAPMainClasses";System.out.println(flag);
				txtRAPMainClasses.clear();
				flag = "txtRAPMainClasses";System.out.println(flag);
				txtRAPMainClasses.sendKeys("Health",Keys.TAB);waitABitInRAP();
				flag = "labelRAPTabClasses";System.out.println(flag);
				act.doubleClick(labelRAPTabClasses).build().perform();waitABitInRAP();
				flag = "btnRAPAddClasses";System.out.println(flag);
				clickOnElementInRAP(btnRAPAddClasses);
			}
			flag = "txtRAPSparte";System.out.println(flag);
			enterTextInRAPWithActions(txtRAPSparte, "Accidental death");
//		clickOnElementInRAP(txtRAPSparte);
//		txtRAPSparte.clear();
//		txtRAPSparte.sendKeys("Accidental death",Keys.TAB);
			flag = "fieldRAPMaxSr";System.out.println(flag);
			clickOnElementInRAP(fieldRAPMaxSr);
			flag = "txtRAPMaxSr";System.out.println(flag);waitABitInRAP();
			enterTextInRAPWithActions(txtRAPMaxSr, "100");
//		clickOnElementInRAP(txtRAPMaxSr);
//		txtRAPMaxSr.clear();
//		txtRAPMaxSr.sendKeys("100");waitABitInRAP();
//		txtRAPMaxSr.sendKeys(Keys.TAB);waitABitInRAP();
			flag = "fieldRAPMindPr";System.out.println(flag);
			clickOnElementInRAP(fieldRAPMindPr);
			flag = "txtRAPMindPr";System.out.println(flag);
			enterTextInRAPWithActions(txtRAPMindPr, "100");
//		clickOnElementInRAP(txtRAPMindPr);
//		txtRAPMindPr.clear();
//		txtRAPMindPr.sendKeys("100");waitABitInRAP();
//		txtRAPMindPr.sendKeys(Keys.TAB);waitABitInRAP();
			flag = "labelRAPTabAccounts";System.out.println(flag);
			clickOnElementInRAP(labelRAPTabAccounts);
			try {
				flag = "labelRAPCustomerAccount";System.out.println(flag);
				customerAccountFromRAP = labelRAPCustomerAccount.getText().split("/")[2].trim();
			} catch (Exception e4) {
				waitABitInRAP();
				labelRAPTabAccounts.click();
				waitABitInRAP();
			}
			flag = "labelRAPTabCollection";System.out.println(flag);
			clickOnElementInRAP(labelRAPTabCollection);
//			clickOnElementInRAP(txtRAPCollectionType);
			flag = "txtRAPCollectionType";System.out.println(flag);
			txtRAPCollectionType.click();
			flag = "txtRAPCollectionType";System.out.println(flag);
			txtRAPCollectionType.clear();Thread.sleep(1000);
			flag = "txtRAPCollectionType";System.out.println(flag);
			txtRAPCollectionType.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);waitABitInRAP();
			try {
				flag = "btnRAPCollectionChangeYes";System.out.println(flag);
				clickOnElementInRAP(btnRAPCollectionChangeYes);
			} catch (Exception e2) {
				flag = "txtRAPCollectionType";System.out.println(flag);
				txtRAPCollectionType.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);waitABitInRAP();waitABitInRAP();
				flag = "btnRAPCollectionChangeYes";System.out.println(flag);
				clickOnElementInRAP(btnRAPCollectionChangeYes);
			}
			flag = "optionFile";System.out.println(flag);
			clickOnElementInRAP(optionFile);
			flag = "optionSaveAndClose";System.out.println(flag);
			clickOnElementInRAP(optionSaveAndClose);
			flag = "optionAction";System.out.println(flag);
			clickOnElementInRAP(optionAction);
			flag = "optionFileBoooking";System.out.println(flag);
			clickOnElementInRAP(optionFileBoooking);
			flag = "txtBookingType";System.out.println(flag);
			clickOnElementInRAP(maxmizeWindowAccountingFile);
			flag = "";System.out.println(flag);
			enterTextInRAPWithoutActions(txtBookingType, "Cash payment");
//			txtBookingType.sendKeys("Cash payment",Keys.TAB);waitABitInRAP();
			String bookingAmount = ""+random.nextInt(49999);
			flag = "txtBookingAmount";System.out.println(flag);waitABitInRAP();
			enterTextInRAPWithActions(txtBookingAmount, bookingAmount);
//		txtBookingType.sendKeys("Cash payment",Keys.TAB);waitABitInRAP();
//		clickOnElementInRAP(txtBookingAmount);
//		txtBookingAmount.sendKeys(bookingAmount);waitABitInRAP();
//		txtBookingAmount.sendKeys(Keys.TAB);waitABitInRAP();
			flag = "labelRAPAccountType";System.out.println(flag);waitABitInRAP();
			clickOnElementInRAP(labelRAPAccountType);waitABitInRAP();
//			txtRAPAccountType.clear();
//			txtRAPAccountType.sendKeys("Customer account",Keys.TAB);waitABitInRAP();
			flag = "txtRAPAccountType";System.out.println(flag);
			enterTextInRAPWithoutActions(txtRAPAccountType, "Customer account");
//		txtRAPAccountType.clear();waitABitInRAP();
//		txtRAPAccountType.sendKeys("Customer account");waitABitInRAP();
//		txtRAPAccountType.sendKeys(Keys.TAB);waitABitInRAP();
			flag = "txtRAPCustomerAccount";System.out.println(flag);
			enterTextInRAPWithActions(txtRAPCustomerAccount, customerAccountFromRAP);waitABitInRAP();
			enterTextInRAPWithActions(txtRAPCustomerAccount, customerAccountFromRAP);
//		txtRAPCustomerAccount.sendKeys(customerAccountFromRAP);waitABitInRAP();
//		txtRAPCustomerAccount.sendKeys(Keys.TAB);waitABitInRAP();
			flag = "txtRAPBookingAmountInTable";System.out.println(flag);
			enterTextInRAPWithActions(txtRAPBookingAmountInTable, bookingAmount);
//		txtRAPBookingAmountInTable.sendKeys(bookingAmount);waitABitInRAP();
//		txtRAPBookingAmountInTable.sendKeys(Keys.TAB);waitABitInRAP();
			try {
				flag = "btnRAPBook";System.out.println(flag);
				clickOnElementInRAP(btnRAPBook);
				flag = "btnRAPPopupOK";System.out.println(flag);
				clickOnElementInRAP(btnRAPPopupOK);
			} catch (Exception e) {
				Thread.sleep(3000);
				flag = "closeEventDisplayPopup";System.out.println(flag);
				clickOnElementInRAP(closeEventDisplayPopup);
				flag = "txtRAPBookingAmountInTable";System.out.println(flag);
				act.doubleClick(txtRAPBookingAmountInTable).build().perform();waitABitInRAP();
				flag = "txtRAPBookingAmountInTable";System.out.println(flag);
				enterTextInRAPWithActions(txtRAPBookingAmountInTable, bookingAmount);
//			txtRAPBookingAmountInTable.sendKeys(Keys.TAB);waitABitInRAP();
				flag = "btnRAPBook";System.out.println(flag);
				clickOnElementInRAP(btnRAPBook);
				flag = "btnRAPPopupOK";System.out.println(flag);
				clickOnElementInRAP(btnRAPPopupOK);
			}


			flag = "closeEventDisplayPopup";System.out.println(flag);
			clickOnElementInRAP(closeEventDisplayPopup);
			flag = "maxmizeWindowAccountingFile";System.out.println(flag);				//Same maximize xpath closes the window
			clickOnElementInRAP(maxmizeWindowAccountingFile);
			flag = "linkRAPPerson";System.out.println(flag);
			library.waitForElementVisible(linkRAPPerson, 20, 1000);
			flag = "linkRAPPerson";System.out.println(flag);waitABitInRAP();
			clickOnElementInRAP(linkRAPPerson);
			flag = "txtRAPName";System.out.println(flag);
			txtRAPName.clear();
			flag = "txtRAPName";System.out.println(flag);
			clickOnElementInRAP(txtRAPName);
			flag = "txtRAPName";System.out.println(flag);
			clickOnElementInRAP(txtRAPName);
			flag = "groupCorporateOrCompanyName";System.out.println(flag);
			txtRAPName.sendKeys(groupCorporateOrCompanyName);waitABitInRAP();
			flag = "btnRAPSearch";System.out.println(flag);
			clickOnElementInRAP(btnRAPSearch);
			flag = "labelRAPCompanyName";System.out.println(flag);
			clickOnElementInRAP(labelRAPCompanyName);labelRAPCompanyName.sendKeys(Keys.ENTER);waitABitInRAP();
			flag = "optionEdit";System.out.println(flag);
			try {
				clickOnElementInRAP(optionEdit);
			} catch (Exception e3) {
				labelRAPCompanyName.sendKeys(Keys.ENTER);waitABitInRAP();
				clickOnElementInRAP(optionEdit);
			}
			flag = "optionContractGroupBusiness";System.out.println(flag);
			clickOnElementInRAP(optionContractGroupBusiness);
			flag = "txtRAPProductSearch";System.out.println(flag);
			clickOnElementInRAP(txtRAPProductSearch);txtRAPProductSearch.sendKeys("10202",Keys.ENTER);Thread.sleep(2000);waitABitInRAP();
			flag = "maxmizeWindowGroupBusinessFile";System.out.println(flag);
			maxmizeWindowGroupBusinessFile.click();waitABitInRAP();
			flag = "txtRAPPayFrequencyInGroupBusinessFile";System.out.println(flag);
			enterTextInRAPWithoutActions(txtRAPPayFrequencyInGroupBusinessFile, "Yearly");
			flag = "txtRAPFleetNoInGroupBusinessFile";System.out.println(flag);
//		clickOnElementInRAP(txtRAPPayFrequencyInGroupBusinessFile);txtRAPPayFrequencyInGroupBusinessFile.sendKeys("Yearly");waitABitInRAP();txtRAPPayFrequencyInGroupBusinessFile.sendKeys(Keys.TAB);waitABitInRAP();
			clickOnElementInRAP(txtRAPFleetNoInGroupBusinessFile);txtRAPFleetNoInGroupBusinessFile.sendKeys(fleetNumberFromRAP);waitABitInRAP();txtRAPFleetNoInGroupBusinessFile.sendKeys(Keys.ARROW_DOWN,Keys.ENTER);waitABitInRAP();
			flag = "txtRAPFamilyDefInGroupBusinessFile";System.out.println(flag);
			txtRAPFamilyDefInGroupBusinessFile.sendKeys("1A+3C",Keys.ARROW_DOWN,Keys.ARROW_DOWN,Keys.ENTER);
//		txtRAPFamilyDefInGroupBusinessFile.sendKeys("1A+3C",Keys.TAB);waitABitInRAP();
//			enterTextInRAPWithoutActions(txtRAPGSTExemptionInGroupBusinessFile, "SEZ unit with tax");waitABitInRAP();waitABitInRAP();
//		txtRAPGSTExemptionInGroupBusinessFile.sendKeys("SEZ unit with tax",Keys.TAB);waitABitInRAP();
			try {
				flag = "closeEventDisplayPopup";System.out.println(flag);
				clickOnElementInRAP(closeEventDisplayPopup);
			} catch (Exception e1) {

			}
			flag = "txtRAPGroupTypeInGroupBusinessFile";System.out.println(flag);
			enterTextInRAPWithoutActions(txtRAPGroupTypeInGroupBusinessFile, "Non Employer- Employee");
//		txtRAPGroupTypeInGroupBusinessFile.sendKeys("Non Employer- Employee",Keys.TAB,Keys.TAB,Keys.ENTER);waitABitInRAP();
			flag = "btnRAPSearch";System.out.println(flag);
			clickOnElementInRAP(btnRAPSearch);
			try {
				flag = "txtRAPName";System.out.println(flag);
				clickOnElementInRAP(txtRAPName);clickOnElementInRAP(txtRAPName);
			} catch (Exception e2) {
				waitABitInRAP();
				flag = "btnRAPSearch";System.out.println(flag);
				clickOnElementInRAP(btnRAPSearch);
				flag = "txtRAPName";System.out.println(flag);
				clickOnElementInRAP(txtRAPName);clickOnElementInRAP(txtRAPName);
			}
			flag = "txtRAPName";System.out.println(flag);
			txtRAPName.sendKeys("Param*");waitABitInRAP();
			flag = "btnRAPTPAPersonSearch";System.out.println(flag);
			clickOnElementInRAP(btnRAPTPAPersonSearch);
			waitABitInRAP();
			flag = "txtRAPFirstPersonAfterSearchInGroupBusinessFile";System.out.println(flag);
			clickOnElementInRAP(txtRAPFirstPersonAfterSearchInGroupBusinessFile);waitABitInRAP();
			flag = "txtRAPFirstPersonAfterSearchInGroupBusinessFile";System.out.println(flag);
			act.sendKeys(txtRAPFirstPersonAfterSearchInGroupBusinessFile, Keys.ENTER).build().perform();waitABitInRAP();
			try {
				flag = "";System.out.println(flag);
				enterTextInRAPWithActions(txtRAPContractCharacteristicsNameInGroupBusinessFile, "Superman");
//			clickOnElementInRAP(txtRAPContractCharacteristicsNameInGroupBusinessFile);txtRAPContractCharacteristicsNameInGroupBusinessFile.sendKeys("Superman",Keys.TAB);waitABitInRAP();
				flag = "";System.out.println(flag);
				clickOnElementInRAP(labelRAPListTabInGroupBusinessFile);
				flag = "";System.out.println(flag);
				btnRAPAddObjectClassInGroupBusinessFile.click();waitABitInRAP();
			} catch (Exception e) {
				flag = "";System.out.println(flag);
				clickOnElementInRAP(closeEventDisplayPopup);
				flag = "";System.out.println(flag);
				enterTextInRAPWithActions(txtRAPContractCharacteristicsNameInGroupBusinessFile, "Superman");
//			clickOnElementInRAP(txtRAPContractCharacteristicsNameInGroupBusinessFile);txtRAPContractCharacteristicsNameInGroupBusinessFile.sendKeys("Superman",Keys.TAB);waitABitInRAP();
				flag = "";System.out.println(flag);
				clickOnElementInRAP(labelRAPListTabInGroupBusinessFile);
				flag = "";System.out.println(flag);
				btnRAPAddObjectClassInGroupBusinessFile.click();waitABitInRAP();
			}
			flag = "txtRAPObjectClassDescriptionInGroupBusinessFile";System.out.println(flag);
			enterTextInRAPWithActions(txtRAPObjectClassDescriptionInGroupBusinessFile, "IronMan");
//		txtRAPObjectClassDescriptionInGroupBusinessFile.sendKeys("IronMan",Keys.TAB);waitABitInRAP();
			flag = "";System.out.println(flag);
			clickOnElementInRAP(labelRAPListTabInGroupBusinessFile);
			flag = "btnRAPAddProductPackageInGroupBusinessFile";System.out.println(flag);
			clickOnElementInRAP(btnRAPAddProductPackageInGroupBusinessFile);
			flag = "txtRAPProductPackageDescriptionInGroupBusinessFile";System.out.println(flag);
			enterTextInRAPWithActions(txtRAPProductPackageDescriptionInGroupBusinessFile, "Silver");
//		txtRAPProductPackageDescriptionInGroupBusinessFile.sendKeys("Silver",Keys.TAB);waitABitInRAP();
			try {
				flag = "labelRAPListTabInGroupBusinessFile";System.out.println(flag);
				clickOnElementInRAP(labelRAPListTabInGroupBusinessFile);
				flag = "btnRAPAddClassesInGroupBusinessFile";System.out.println(flag);
				clickOnElementInRAP(btnRAPAddClassesInGroupBusinessFile);
			} catch (Exception e) {
				System.out.println("***********Caught Exception***********");
				try {
					waitABitInRAP();
					flag = "labelRAPListTabInGroupBusinessFile";System.out.println(flag);
					labelRAPListTabInGroupBusinessFile.click();waitABitInRAP();
				} catch (Exception e1) {
				}
				flag = "btnRAPAddClassesInGroupBusinessFile";System.out.println(flag);
				clickOnElementInRAP(btnRAPAddClassesInGroupBusinessFile);
			}
			flag = "txtObjectType";System.out.println(flag);waitABitInRAP();
			enterTextInRAPWithoutActions(txtObjectType, "Person");
//		txtRAPObjectTypeInGroupBusinessFile.sendKeys("Person",Keys.TAB);waitABitInRAP();
			try {
				flag = "btnRAPAddObject";System.out.println(flag);
				clickOnElementInRAP(btnRAPAddObject);
			} catch (Exception e) {
				waitABitInRAP();clickOnElementInRAP(btnRAPAddObjectAlt);waitABitInRAP();
			}
			flag = "chkRAPObjectTypeInAddObjects";System.out.println(flag);
			clickOnElementInRAP(chkRAPObjectTypeInAddObjects);
			flag = "labelRAPProductTab";System.out.println(flag);
			clickOnElementInRAP(labelRAPProductTab);
			flag = "btnRAPAddInProductPopup";System.out.println(flag);
			clickOnElementInRAP(chkRAPProductInAddObjects);
			flag = "";System.out.println(flag);
			clickOnElementInRAP(btnRAPAddInProductPopup);
			flag = "btnRAPEditCoverageInGroupBusinessFile";System.out.println(flag);
			clickOnElementInRAP(btnRAPEditCoverageInGroupBusinessFile);
			flag = "chkRAPNewInstanceInpatientHospitalizationCollective";System.out.println(flag);
			clickOnElementInRAP(chkRAPNewInstanceInpatientHospitalizationCollective);
			flag = "txtRAPNewInstanceSI";System.out.println(flag);
			txtRAPNewInstanceSI.clear();txtRAPNewInstanceSI.sendKeys("100000",Keys.TAB,"1",Keys.TAB);
			//		clickOnElementInRAP(chkRAPHospitalizationCoverInpatientHospitalizationCollective);
			//		clickOnElementInRAP(chkRAPAccidentalAndIllnessHospitalizationCoverInpatientHospitalizationCollective);
			//		txtRAPAccidentalAndIllnessHospitalizationCoverSI.clear();
			//		txtRAPAccidentalAndIllnessHospitalizationCoverSI.sendKeys("1",Keys.TAB);
			//		clickOnElementInRAP(chkRAPInitialWaitingPeriod);
			//		txtRAPInitialWaitingPeriod.sendKeys("1",Keys.TAB);
			//		clickOnElementInRAP(chkRAPPEDWaitingPeriod);
			//		txtRAPPEDWaitingPeriod.sendKeys("1",Keys.TAB);
			flag = "optionFile";System.out.println(flag);
			clickOnElementInRAP(optionFile);
			try {
				flag = "optionSave";System.out.println(flag);
				clickOnElementInRAP(optionSave);
			} catch (Exception e) {
				flag = "optionFile";System.out.println(flag);
				clickOnElementInRAP(optionFile);
				flag = "optionSave";System.out.println(flag);
				clickOnElementInRAP(optionSave);
			}
			flag = "btnRAPCollectionChangeYes";System.out.println(flag);
			clickOnElementInRAP(btnRAPCollectionChangeYes);
			flag = "chkPHSignature";System.out.println(flag);
			clickOnElementInRAP(chkPHSignature);
			//		clickOnElementInRAP(chkForwardBO);
			//		library.clickOnElementInRAPJS(chkForwardBO, library.getDriver());
			flag = "optionFile";System.out.println(flag);
			clickOnElementInRAP(optionFile);
			flag = "optionSaveAndClose";System.out.println(flag);
			clickOnElementInRAP(optionSaveAndClose);
			flag = "btnRAPCollectionChangeYes";System.out.println(flag);
			clickOnElementInRAP(btnRAPCollectionChangeYes);
			flag = "msgMasterPolicyInfo";System.out.println(flag);
			masterPolicyNumber = msgMasterPolicyInfo.getText().split("[(]")[1].split("[)]")[0];waitABitInRAP();
			flag = "closeEventDisplayPopup";System.out.println(flag);
			clickOnElementInRAP(closeEventDisplayPopup);
			flag = "optionAction";System.out.println(flag);
			clickOnElementInRAP(optionAction);
			flag = "optionClose";System.out.println(flag);
			clickOnElementInRAP(optionClose);
		
	}




	

	public void mapMasterPolicy(String gmc_Ops_MapPolicy_Id) throws Exception {

		Login login = PageFactory.initElements(library.getDriver(), Login.class);
		library.getDriver().get("https://preprod-digithealth.godigit.com/#/login");
		login.login();Thread.sleep(3000);
		//library.waitForGMCLoading();
		library.waitForDigitLoad();

		Actions act = new Actions(library.getDriver());
		String labelCorporateOrCompany = "//td[contains(text(),'"+groupCorporateOrCompanyName+"')]";
		WebElement corporateOrCompanyName = library.getDriver().findElement(By.xpath(labelCorporateOrCompany));
		act.moveToElement(corporateOrCompanyName).build().perform();
		String drpXpath = labelCorporateOrCompany+"/..//td[last()]//li[last()]/div";
		WebElement dropDown = library.getDriver().findElement(By.xpath(drpXpath));
		act.moveToElement(dropDown).build().perform();
		Thread.sleep(1000);
		library.getDriver().findElement(By.xpath(drpXpath+"//a[contains(text(),'Map Master Policy')]")).click();
		//library.waitForGMCLoading();
		library.waitForDigitLoad();
		enterText(txtMasterPolicyNumber, masterPolicyNumber);
		enterText(txtFleetID, fleetNumberFromRAP);
		clickDesiredValueInListbox(listTPA, "PM");
		try {
			clickOnElement(btnCreate);
		} catch (Exception e) {
			clickOnElement(btnCreateAlternate);
		}
		library.waitForDigitLoad();waitABitInUI();
		try {
			clickOnElement(backArrow);
		} catch (Exception e) {
			enterText(txtMasterPolicyNumber, masterPolicyNumber);
			enterText(txtFleetID, fleetNumberFromRAP);
			clickDesiredValueInListbox(listTPA, "PM");
			try {
				clickOnElement(btnCreate);
			} catch (Exception e1) {
				clickOnElement(btnCreateAlternate);
			}
			library.waitForDigitLoad();waitABitInUI();
		}
		//library.waitForGMCLoading();
		library.waitForDigitLoad();
	}

	public void uploadTarrifDocument() throws Exception {
		String flag = "linkNewTariff";System.out.println(flag);
		try {
			linkNewTariff.click();Thread.sleep(2000);
		} catch (Exception e) {
			enterText(txtMasterPolicyNumber, masterPolicyNumber);
			enterText(txtFleetID, fleetNumberFromRAP);
			clickDesiredValueInListbox(listTPA, "PM");
			try {
				clickOnElement(btnCreate);
			} catch (Exception e1) {
				clickOnElement(btnCreateAlternate);
			}
			library.waitForDigitLoad();waitABitInUI();
			clickOnElement(backArrow);waitABitInUI();
			linkNewTariff.click();Thread.sleep(2000);
		}
		switch (productFlag.toLowerCase()) {
		case "gmc":
			FileInputStream fis = new FileInputStream(Paths.get("").toAbsolutePath().toString()+"/src/main/resources/documents/GMC_Tariff.xlsx");
			Workbook book = WorkbookFactory.create(fis);
			Calendar cal = Calendar.getInstance(); 
			SimpleDateFormat s = new SimpleDateFormat("dd-MMM-yyyy"); 
			String[] split = s.format(cal.getTime()).split("-");
			int extraDays = Integer.parseInt(split[0]);
			split[0] = "01";
			String startDate = split[0]+"-"+split[1]+"-"+split[2];
			cal.add(Calendar.YEAR, 1);
			cal.add(Calendar.DATE, -extraDays);
			String expDate = s.format(cal.getTime());
			Cell cell = null;
			for (int i=1; i<=6; i++) {
				
				
				cell = book.getSheet("Sheet1").getRow(i).getCell(0);
				cell.setCellValue(masterPolicyNumber);
				cell = book.getSheet("Sheet1").getRow(i).getCell(6);
				cell.setCellValue(startDate);
				cell = book.getSheet("Sheet1").getRow(i).getCell(7);
				cell.setCellValue(expDate);
			}
			FileOutputStream fos = new FileOutputStream(Paths.get("").toAbsolutePath().toString()+"/src/main/resources/documents/GMC_Tariff.xlsx");
			book.write(fos);
			fos.flush();
			waitABitInUI();
			fis.close();fos.close();
			waitABitInUI();
			flag = "btnUploadTarrifDocument";System.out.println(flag);
			btnUploadTarrifDocument.sendKeys(Paths.get("").toAbsolutePath().toString()+"/src/main/resources/documents/GMC_Tariff.xlsx");
			break;
		case "gpa":
			btnUploadTarrifDocument.sendKeys(Paths.get("").toAbsolutePath().toString()+"/src/main/resources/documents/GPA_Tariff.xlsx");
			break;

		default:
			break;
		}
		//library.waitForGMCLoading();
		library.waitForDigitLoad();
		flag = "btnSubmit";System.out.println(flag);
		clickOnElement(btnSubmit);waitABitInUI();waitABitInUI();
		//library.waitForGMCLoading();
		library.waitForDigitLoad();
	}

	public void addChildPolicy() throws Exception {

		waitABitInRAP();waitABitInRAP();
		String flag = groupCorporateOrCompanyName;System.out.println("Click on "+flag);
		library.getDriver().findElement(By.xpath("//td[contains(text(),'"+groupCorporateOrCompanyName+"')]")).click();waitABitInUI();
		flag = "linkViewChildPolicy";System.out.println(flag);
		clickOnElement(linkViewChildPolicy);
		flag = "linkAddChildPolicies";System.out.println(flag);
		clickOnElement(linkAddChildPolicies);
		FileInputStream fis = new FileInputStream(Paths.get("").toAbsolutePath().toString()+"/src/main/resources/documents/Child_GMC.xlsx");
		Workbook book = WorkbookFactory.create(fis);
		Calendar cal = Calendar.getInstance(); 
		SimpleDateFormat s = new SimpleDateFormat("dd-MMM-yyyy"); 
		String[] split = s.format(cal.getTime()).split("-");
		split[0] = "01";
		String endorsementDate = split[0]+"-"+split[1]+"-"+split[2];
		Cell cell = null;
		for (int i=1; i<=19; i++) {

			cell = book.getSheet("Sheet11").getRow(i).getCell(18);
			cell.setCellValue(endorsementDate);
		}
		FileOutputStream fos = new FileOutputStream(Paths.get("").toAbsolutePath().toString()+"/src/main/resources/documents/Child_GMC.xlsx");
		book.write(fos);
		fos.flush();
		waitABitInUI();
		fis.close();fos.close();
		waitABitInUI();
		flag = "btnUploadMemberDocument";System.out.println(flag);
		btnUploadMemberDocument.sendKeys(Paths.get("").toAbsolutePath().toString()+"/src/main/resources/documents/Child_GMC.xlsx");waitABitInUI();
		flag = "btnNext";System.out.println(flag);
		clickOnElement(btnNext);
		library.focus(library.getDriver(), btnNext);waitABitInUI();
		library.focus(library.getDriver(), labelSumInsured);waitABitInUI();
		flag = "btnMapData";System.out.println(flag);
		clickOnElement(btnMapData);
//		library.focus(library.getDriver(), btnMapData);waitABitInUI();
		flag = "btnCalculatePremium";System.out.println(flag);
		clickOnElement(btnCalculatePremium);
		library.focus(library.getDriver(), btnCalculatePremium);waitABitInUI();
		library.focus(library.getDriver(), labelTotalSummary);waitABitInUI();
		flag = "btnSaveAndProceed";System.out.println(flag);
		clickOnElement(btnSaveAndProceed); 
		flag = "btnOkSaveAndProceed";System.out.println(flag);
		clickOnElement(btnOkSaveAndProceed); 
		System.out.println("*******************Success******************");
	}


	private void waitABitInUI() throws Exception {
		Thread.sleep(2000);
		//		library.waitForABSLoading();
		library.waitForDigitLoad();
	}
	
	private  void enterTextInRAPWithActions(WebElement textField,String data) throws Exception {
		Actions act = new Actions(library.getDriver());
		act.moveToElement(textField).build().perform();System.out.print("  ---   moveToElement  ---   ");
		act.doubleClick(textField).build().perform();System.out.print("doubleClick  ---   ");
		textField.clear();System.out.print("clear  ---   ");
		act.sendKeys(textField, data).build().perform();System.out.print("sendData  ---   ");
		act.sendKeys(textField, Keys.TAB).build().perform();waitABitInRAP();System.out.println("Press TAB\n");
	}
	
	private  void enterTextInRAPWithoutActions(WebElement textField,String data) throws Exception {
		Actions act = new Actions(library.getDriver());
		act.moveToElement(textField).build().perform();System.out.print("  ---   moveToElement  ---   ");
		act.doubleClick(textField).build().perform();System.out.print("doubleClick  ---   ");
		textField.clear();System.out.print("clear  ---   ");
		textField.sendKeys(data);System.out.print("sendData  ---   ");
		textField.sendKeys(Keys.TAB);waitABitInRAP();System.out.println("Press TAB\n");
	}
	
	private void waitABitInRAP() throws Exception {
		Thread.sleep(1500);
//		library.waitForABSLoading();
		library.waitForDigitLoad();
//		Thread.sleep(1000);
//		library.waitForABSLoading();
//		library.waitForDigitLoad();
//		Thread.sleep(1000);
//		library.waitForABSLoading();
//		library.waitForDigitLoad();
//		Thread.sleep(1000);
//		library.waitForABSLoading();
//		library.waitForDigitLoad();
//		Thread.sleep(1000);
//		library.waitForABSLoading();
//		library.waitForDigitLoad();
	}
	
	private void clickOnElement(WebElement element) throws Exception {
		library.waitForElementVisible(element, 10, 500);
		Actions act = new Actions(library.getDriver());
		act.moveToElement(element).build().perform();System.out.print("  ---   moveToElement  ---   ");//Thread.sleep(2000);
		act.click(element).build().perform();System.out.println("Click\n");
		//				library.waitForElementToBeClickable(element, 10, 500);
//		try {
//			element.click();
//		} catch (Exception e) {
//			library.clickOnElementJS(element, library.getDriver());
//		}
		waitABitInUI();
	}
	
	private void clickOnElementInRAP(WebElement element) throws Exception {
		library.waitForElementVisible(element, 10, 500);waitABitInUI();
		//				library.waitForElementToBeClickable(element, 10, 500);
		Actions act = new Actions(library.getDriver());
		act.moveToElement(element).build().perform();System.out.print("  ---   moveToElement  ---   ");//Thread.sleep(2000);
		act.click(element).build().perform();waitABitInRAP();System.out.println("Click\n");
//			element.click();
			try {
			library.clickOnElementJS(element, library.getDriver());
			} catch (Exception e) {
			}
		waitABitInRAP();
	}

	private void enterText(WebElement textField, String data) throws Exception {
		library.waitForDigitLoad();//library.waitForGMCLoading();
		if (data!=null && !data.equalsIgnoreCase("NA")) {
//			try {
				library.waitForElementVisible(textField, 10, 500);
				library.waitForElementToBeClickable(textField, 10, 500);
				Actions act = new Actions(library.getDriver());
				act.moveToElement(textField).build().perform();Thread.sleep(2000);
				act.click().build().perform();
				textField.clear();
				act.sendKeys(textField,data).build().perform();
//				textField.click();
//				textField.sendKeys(data);
				Thread.sleep(2000);
//			} catch (Exception e) {
//
//			}
			//			JavascriptExecutor jse = (JavascriptExecutor)library.getDriver();
			//			jse.executeScript("arguments[0].value='"+ data +"';", textField);
		}
		library.waitForDigitLoad();//library.waitForGMCLoading();
	}

	private void clickDesiredValueInListbox(WebElement listbox,String value) throws InterruptedException {
		if (value!=null && !value.equalsIgnoreCase("NA")) {
			library.waitForElementVisible(listbox, 10, 1000);
			Select select = new Select(listbox);
			select.selectByValue(value);
		}
	}
}
