package pom.agent.gmc;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.utilities.PropertyFileReader;

public class Login {

	private static com.utilities.Library library = new com.utilities.Library();
	
	@FindBy(xpath = "//div[contains(text(),'Digit Agents')]/..//input")
	private WebElement radDigitAgents;
	
	@FindBy(xpath = "//div[contains(text(),'HR')]/..//input")
	private WebElement radHR;
	
	@FindBy(xpath = "//div[contains(text(),'Employee')]/..//input")
	private WebElement radEmployee;
	
	@FindBy(xpath = "//input[@formcontrolname='userName']")
	private WebElement txtUserName;

	@FindBy(xpath = "//input[@formcontrolname='password']")
	private WebElement txtPassword;

	@FindBy(xpath = "//button[contains(text(),'Continue')]")
	private WebElement btnLogin;
	
	@FindBy(xpath = "//input[@type='text']")
	private WebElement txtRAPUserName;
	
	@FindBy(xpath = "//input[@type='password']")
	private WebElement txtRAPPassword;
	
	@FindBy(id = "submitbutton")
	private WebElement btnRAPLogin;
	
	public void login() throws Exception {
		
		PropertyFileReader read = new PropertyFileReader();
		
		Thread.sleep(3000);
		txtUserName.sendKeys(read.getGMCOpsPreprodDigitHealthUserName());
		Thread.sleep(1000);
		txtPassword.sendKeys(read.getGMCOpsPreprodDigitHealthPassword());
		Thread.sleep(1000);
		btnLogin.click();
		
		library.waitForGMCLoading();
	}
	
	public void rapLogin() throws Exception {
		
		PropertyFileReader read = new PropertyFileReader();
		
		Thread.sleep(3000);
		txtRAPUserName.sendKeys(read.getAbsPreprodRAPUsername());
		Thread.sleep(1000);
		txtRAPPassword.sendKeys(read.getAbsPreprodRAPPassword());
		Thread.sleep(1000);
		btnRAPLogin.click();
		library.waitForABSLoading();
	}
}
