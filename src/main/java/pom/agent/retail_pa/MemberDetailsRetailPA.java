package pom.agent.retail_pa;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.utilities.DBUtility;

public class MemberDetailsRetailPA {

	@FindBy(xpath = "//button[contains(text(),'Next')]")
	private WebElement btnNext;
	
	@FindBy(xpath = "//th[contains(text(),'Type of Loan')]/../..//tr/td[1]//mat-select//div[@class='mat-select-arrow']")
	private WebElement lstLoanType;
	
	@FindBy(xpath = "//th[contains(text(),'Financial Institution')]/../..//tr/td[2]//input")
	private WebElement txtLoanInstitution;
	
	@FindBys({@FindBy(xpath = "//span[@class='mat-option-text']")})
	private List<WebElement> valueInList;

	Random random = new Random();
	public static HashMap hm = new HashMap();
	com.utilities.Library library = new com.utilities.Library();

	public void enterDetails(String familyMembers,String retailId) throws Exception {

		try {
			for (String temp : familyMembers.split(",")) {
				switch (temp.replaceAll(" ", "").toLowerCase()) {
				case "self":
				case "proposer":
					enterPersonalDetails("SELF",retailId);
					break;
				case "spouse":
					enterPersonalDetails("SPOUSE",retailId);
					break;
				case "child1":
					enterPersonalDetails("CHILD 1",retailId);
					break;
				case "child2":
					enterPersonalDetails("CHILD 2",retailId);
					break;
				case "child3":
					enterPersonalDetails("CHILD 3",retailId);
					break;
				case "child4":
					enterPersonalDetails("CHILD 4",retailId);
					break;
				case "father":
					enterPersonalDetails("FATHER",retailId);
					break;
				case "mother":
					enterPersonalDetails("MOTHER",retailId);
					break;
				case "fatherinlaw":
					enterPersonalDetails("FATHER_IN_LAW",retailId);
					break;
				case "motherinlaw":
					enterPersonalDetails("MOTHER_IN_LAW",retailId);
					break;
				}
			}
			if (PlanDetailsRetailPA.sec27iscovered) {

				lstLoanType.click();
				library.clickOnElementJS(lstLoanType, library.getDriver());
				Thread.sleep(1500);
				library.getDriver().findElement(By.xpath("(//span[@class='mat-option-text'])["+(random.nextInt(6)+1)+"]")).click();
				Thread.sleep(1500);
				txtLoanInstitution.sendKeys(library.getRandomValue("[A | 9]"));
//				Thread.sleep(2500);
			} 
//			btnNext.click();
			Thread.sleep(3000);
			if (btnNext.isEnabled()) {
				library.clickOnElementJS(btnNext, library.getDriver());
			} else {
				library.focus(library.getDriver(), library.getDriver().findElement(By.xpath("//td[contains(text(),'First Name')]")));
			}
			library.waitForDigitLoad();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		} finally {
			PlanDetailsRetailPA.sec27iscovered = false;
		}
	}

	private void enterPersonalDetails(String member,String retailId) throws Exception {
		try {

			DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
			library.waitForDigitLoad();
			boolean proposer = true;
			if (library.getDriver().findElements(By.xpath("//th[text()='Member Details']/../th/span")).size() > 0)
				proposer = false;
			String xpathMember = member;
			String firstName = library.getRandomValue("[A | 5]");
			hm.put(xpathMember, firstName);
			library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_firstName']//input"))
			.sendKeys(firstName);
			library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_lastName']//input"))
			.sendKeys(library.getRandomValue("[A | 5]"));
			if(member.equals("SELF") && proposer)
			{

				library.getDriver()
				.findElement(By.xpath("//td[@id='td_" + xpathMember + "_maritalStatus']//mat-select")).click();
				Thread.sleep(1000);
				library.getDriver().findElement(By.xpath("//span[text()='Married'][@class='mat-option-text']")).click();

			}
			if (member.equals("SELF") || proposer || member.contains("FATHER")) {
				library.getDriver()
				.findElement(By.xpath("//td[@id='td_" + xpathMember + "_maritalStatus']//mat-select")).click();
				Thread.sleep(1000);
				library.getDriver().findElement(By.xpath("//span[text()='Married'][@class='mat-option-text']")).click();
			}
			Thread.sleep(1000);
			library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_profession']//mat-select"))
			.click();
			Thread.sleep(1000);
			try {
				library.getDriver().findElement(By.xpath("(//span[@class='mat-option-text'])["+(random.nextInt(10)+1)+"]")).click();
			} catch (Exception e) {
				library.getDriver().findElement(By.xpath("(//span[@class='mat-option-text'])[1]")).click();
			}
			if(member.equals("SELF") || proposer)
			{
				Thread.sleep(2000);
				library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_grossAnnualIncome']//input"))
				.sendKeys((Integer.parseInt(db.getRetailDataFromDB("basesi", retailId)) * Integer.parseInt(db.getRetailDataFromDB("grossannualincome", retailId).split("times")[0].trim()))+"");
			}
			if (PlanDetailsRetailPA.spouseEarning && member.equals("SPOUSE")) {
				Thread.sleep(2000);
				library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_grossAnnualIncome']//input"))
				.sendKeys((Integer.parseInt(db.getRetailDataFromDB("basesi", retailId)) * Integer.parseInt(db.getRetailDataFromDB("grossannualincome", retailId).split("times")[0].trim()))+"");
			}
			if (member.equals("SELF") || member.equals("SPOUSE") || member.equals("CHILD 1") || member.equals("CHILD 2")) {
				try {
					library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_idProofType']//mat-select/div/div[2]/div"))
					.click();
					Thread.sleep(1000);
					library.getDriver().findElement(By.xpath("//span[@class = 'mat-option-text' and text()='Passport']")).click();
					library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_panNumber']//input"))
					.sendKeys(library.getRandomValue("[ A | 10]"));
				} catch (Exception e) {
					
				}
			}
			library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_street']//input")).clear();
			library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_street']//input"))
			.sendKeys(library.getRandomValue("[A | 10]"));
			if (member.equals("SELF")) {
				try {
					library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_pincode']//input"))
					.sendKeys(db.getRetailDataFromDB("pincode", retailId));
				} catch (Exception e) {
					
				}
				library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_email']//input"))
				.sendKeys("test@godigit.com");
				library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_mobile']//input"))
				.sendKeys("9" + library.getRandomValue("[N | 9]"));
			}
			if ((member.equals("SELF") && !proposer) || (member.equals("SPOUSE") && proposer)|| (member.contains("FATHER") && proposer)) {
				library.getDriver()
				.findElement(By.xpath("//td[text()='Nominee Details']/../following-sibling::tr/td[@id='td_"
						+ xpathMember + "_firstName']/input"))
				.sendKeys(library.getRandomValue("[A | 10]"));
				library.getDriver()
				.findElement(By.xpath("//td[text()='Nominee Details']/../following-sibling::tr/td[@id='td_"
						+ xpathMember + "_lastName']/input"))
				.sendKeys(library.getRandomValue("[A | 10]"));
				Thread.sleep(2000);
				library.getDriver()
				.findElement(By.xpath("//td[text()='Nominee Details']/../following-sibling::tr/td[@id='td_"
						+ xpathMember + "_dateOfbirth']//input"))
				.sendKeys("03");
				Thread.sleep(1000);
				library.getDriver()
				.findElement(By.xpath("//td[text()='Nominee Details']/../following-sibling::tr/td[@id='td_"
						+ xpathMember + "_dateOfbirth']//input"))
				.sendKeys("05");
				Thread.sleep(1000);
				library.getDriver()
				.findElement(By.xpath("//td[text()='Nominee Details']/../following-sibling::tr/td[@id='td_"
						+ xpathMember + "_dateOfbirth']//input"))
				.sendKeys("1990");
				Thread.sleep(1000);
				library.getDriver()
				.findElement(By.xpath("//td[contains(text(),'Relationship with Insured')]/following-sibling::td[@id='td_" + xpathMember + "_relationshipStatus']//mat-select"))
				.click();
				Thread.sleep(1000);
				library.getDriver().findElement(By.xpath("//span[text()='Sister'][@class='mat-option-text']")).click();
			}

		} catch (Exception e) {
			throw new Exception("Error while entering the family details : " + e.getMessage());
		} 
	}
}
