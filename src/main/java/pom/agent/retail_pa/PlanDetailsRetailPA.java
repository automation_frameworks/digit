package pom.agent.retail_pa;

import java.sql.SQLException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.utilities.DBUtility;

public class PlanDetailsRetailPA {

	@FindBy(xpath = "//span[contains(text(),'Sum Insured opted')]/../../..//button[text()='Ok']")
	private WebElement btnOkFromBegenningPopup;

	@FindBy(xpath = "//span[contains(text(),'Loss of Income Benefit')]/../../..//button[text()='Ok']")
	private WebElement btnOkFromSec4CoveragePopup;
	
	@FindBy(xpath = "//span[contains(text(),'Maximum EMI amount')]/../../..//button[text()='Ok']")
	private WebElement btnOkFromSec27CoveragePopup;
	
	@FindBy(xpath = "//span[contains(text(),'Permanent Partial Disability SI')]/../../..//button[text()='Ok']")
	private WebElement btnOkFromSec3CoveragePopup;
	
	@FindBy(xpath = "//input[@placeholder = 'Enter amount']")
	private WebElement txtSec27EMIAmount;
	
	@FindBy(xpath = "//div[contains(text(),'Section 27 ')]/../../../td[3]//mat-select/following::mat-select")
	private WebElement lstSec27Months;
	
	@FindBy(xpath = "//label[contains(text(),'Cumulative Bonus Option')]/../..//label/div")
	private WebElement chkBonusOption;
	
	@FindBy(xpath = "//label[contains(text(),'Select One Option')]/..//mat-select")
	private WebElement lstBonusOption;

	@FindBy(xpath = "//label[contains(text(),'SI for Self')]/..//mat-select")
	private WebElement lstSelfSI;

	@FindBy(xpath = "//label[contains(text(),'SI for Spouse')]/..//mat-select")
	private WebElement lstSpouseSI;

	@FindBy(xpath = "//label[contains(text(),'SI for Spouse')]/..//span")
	private WebElement lblSpouseSI;

	@FindBy(xpath = "//label[contains(text(),'SI for Child')]/..//span")
	private WebElement lblChildSI;

	@FindBy(xpath = "//label[contains(text(),'SI for Father')]/..//span")
	private WebElement lblFatherSI;

	@FindBy(xpath = "//label[contains(text(),'SI for Mother')]/..//span")
	private WebElement lblMotherSI;

	@FindBy(xpath = "//label[contains(text(),'Policy Term')]/..//mat-select")
	private WebElement lstPolicyTerm;

	@FindBy(xpath = "//label[contains(text(),'Quote Premium')]/../h4")
	private WebElement quotePremium;

	@FindBys({@FindBy(xpath = "//span[@class='mat-option-text']")})
	private List<WebElement> valueInList;

	@FindBy(xpath = "//button[contains(text(),'Recalculate')]")
	private WebElement btnRecalculate;

	@FindBy(xpath = "//button[contains(text(),'Proceed')]")
	private WebElement btnProceed;
	
	@FindBy(xpath = "//span[text()='Message:']/..")
	private WebElement errorMessage;
	
	@FindBy(xpath = "//span[text()='Transaction Id:']/..")
	private WebElement errorTransactionId;
	
	@FindBy(xpath = "//span[text()='URL:']/..")
	private WebElement errorURL;
	
	@FindBy(xpath = "//span[text()='Status:']/..")
	private WebElement errorStatus;

	public boolean recalculateButtonIsPresent;
	public static boolean sec27iscovered;
	public static boolean spouseEarning;
	public static String basesi;

	com.utilities.Library library = new com.utilities.Library();
	static int oldQuote;
	public static int spouseSI;
	
	
	
	public void handlePopup() {
		btnOkFromBegenningPopup.click();
	}

	public void selectSI(WebDriver driver,String retailId) throws Exception {

		DBUtility db = PageFactory.initElements(driver, DBUtility.class);
		basesi = db.getRetailDataFromDB("basesi", retailId); 
		spouseEarning = false;
		
		//Get the initial quote to compare if premium is recalculated.
		oldQuote = Integer.parseInt(quotePremium.getText().replaceAll(",", "").trim().split(" ")[1]);
		
		//SI for Self
		String si = Integer.parseInt(basesi) * (Integer.parseInt(db.getRetailDataFromDB("selfsi", retailId).replaceAll(" ", "").split("%")[0])/100) + "";
		lstSelfSI.click();
		for (WebElement value : valueInList) {
			if (value.getText().trim().replaceAll(",", "").contains(si)) {
				library.clickOnElementJS(value, driver);Thread.sleep(2000);
				break;
			}
		}

		try {
			if (PersonalAccidentInsurance.spouseIsCovered) {
				spouseSI = Integer.parseInt(basesi) * (Integer.parseInt(
						db.getRetailDataFromDB("spousesi", retailId).replaceAll(" ", "").split("%")[0]) / 100);
				si = spouseSI + "";
				
				///If Spouse is earning, select spouse SI.
				if (PersonalAccidentInsurance.spouseIsEarningMember) {
					spouseEarning = PersonalAccidentInsurance.spouseIsEarningMember;
					lstSpouseSI.click();
					for (WebElement value : valueInList) {
						if (value.getText().trim().replaceAll(",", "").contains(si)) {
							library.clickOnElementJS(value, driver);Thread.sleep(2000);
							break;
						}
					}
				} else {
					if (lblSpouseSI.getText().trim().contains(si) && spouseSI <= 2500000) {
						System.out.println("Spouse SI validation PASS!!");
					} else {
						db.storeResultInNonABS_UAT_DB("SI Rs." + si + " for Spouse is not valid as per logic!!", retailId, "fail",
								"digit_retail_pa.retail_pa_testdata");
						throw new Exception("Spouse SI validation FAIL!!");
					}
				}
			}
			
			//Child validation.
			if (PersonalAccidentInsurance.childIsCovered) {
				int childSI = Integer.parseInt(basesi)
						* (Integer.parseInt(db.getRetailDataFromDB("c1si", retailId).replaceAll(" ", "").split("%")[0])
								/ 100);
				si = childSI + "";
				if (lblChildSI.getText().trim().contains(si) && childSI <= 1000000) {
					System.out.println("Child SI validation PASS!!");
				} else {
					db.storeResultInNonABS_UAT_DB("SI Rs." + si + " for Child is not valid as per logic!!", retailId, "fail",
							"digit_retail_pa.retail_pa_testdata");
					throw new Exception("Child SI validation FAIL!!");
				}
			}
			lstPolicyTerm.click();
			Thread.sleep(1000);
			String term = db.getRetailDataFromDB("familypolicyterm", retailId);
			getValue(term);
			boolean parentFlag = true;
			
			//Father validation.
			if (PersonalAccidentInsurance.fatherIsCovered) {
				getCoverage(library.getDriver(),retailId);
				recalculateAndProceed();
				int fatherSI = Integer.parseInt(basesi) * (Integer.parseInt(
						db.getRetailDataFromDB("fathersi", retailId).replaceAll(" ", "").split("%")[0]) / 100);
				si = fatherSI + "";
				if (lblFatherSI.getText().trim().contains(si) && fatherSI <= 2500000) {
					System.out.println("Father SI validation PASS!!");
				} else {
					db.storeResultInNonABS_UAT_DB("SI Rs." + si + " for Father/Father-In-Law is not valid as per logic!!",
							retailId, "fail", "digit_retail_pa.retail_pa_testdata");
					throw new Exception("Father SI validation FAIL!!");
				}

				term = db.getRetailDataFromDB("familypolicyterm", retailId);
				getValue(term);
				parentFlag = false;
			}
			
			//Mother validation.
			if (PersonalAccidentInsurance.motherIsCovered) {
				if (parentFlag) {
					getCoverage(library.getDriver(),retailId);
					recalculateAndProceed();
				}
				int motherSI = Integer.parseInt(basesi) * (Integer.parseInt(
						db.getRetailDataFromDB("mothersi", retailId).replaceAll(" ", "").split("%")[0]) / 100);
				si = motherSI + "";
				if (lblMotherSI.getText().trim().contains(si) && motherSI <= 2500000) {
					System.out.println("Mother SI validation PASS!!");
				} else {
					db.storeResultInNonABS_UAT_DB("SI Rs." + si + " for Mother/Mother-In-Law is not valid as per logic!!",
							retailId, "fail", "digit_retail_pa.retail_pa_testdata");
					throw new Exception("Mother SI validation FAIL!!");
				}

				if (parentFlag) {
					term = db.getRetailDataFromDB("parentspolicyterm", retailId);
					getValue(term);
				}
			} 
		} finally {
			
			//As all the following are static, in order to defend the logic, all are being reset to their default values.
			PersonalAccidentInsurance.spouseIsCovered = false;
			PersonalAccidentInsurance.childIsCovered = false;
			PersonalAccidentInsurance.fatherIsCovered = false;
			PersonalAccidentInsurance.motherIsCovered = false;
			PersonalAccidentInsurance.spouseIsEarningMember = false;
		}
	}

	public void recalculateAndProceed() throws Exception {

		try {
			Thread.sleep(2000);
			btnRecalculate.click();
			recalculateButtonIsPresent = true;
			library.waitForDigitLoad();Thread.sleep(2000);
		} catch(Exception e) {

		}

		if (recalculateButtonIsPresent) {

			int newQuote = Integer.parseInt(quotePremium.getText().replaceAll(",", "").trim().split(" ")[1]);

			if (newQuote==oldQuote) {
				throw new Exception("Quote Validation FAIL!!");

			}else {
				System.out.println("Quote Validation PASS!!");
			}

			recalculateButtonIsPresent = false;
		}

		btnProceed.click();
		library.waitForDigitLoad();
//		try {
//			library.waitForDigitLoad();
//			DBUtility db = PageFactory.initElements(driver, DBUtility.class);
//			Thread.sleep(3000);
//			db.storeCommentInDB("Message: "+errorMessage.getText()+"\n\nTransaction Id: "+errorTransactionId.getText()+"\n\nURL: "+errorURL.getText()+"\n\nStatus: "+errorStatus.getText(), id, "digit_retail_pa.retail_pa_testdata");
//			System.out.println("Message: "+errorMessage.getText()+"\n\nTransaction Id: "+errorTransactionId.getText()+"\n\nURL: "+errorURL.getText()+"\n\nStatus: "+errorStatus.getText());
//		} catch (Exception e) {
//
//		}
	}

	public void getCoverage(WebDriver driver,String retailId) throws SQLException, InterruptedException {

		DBUtility db = PageFactory.initElements(driver, DBUtility.class);

		String[] coverageFromDB = db.getRetailDataFromDB("coverage", retailId).split(",");

		for (String coverageSection : coverageFromDB) {
				try {
					if (!sectionNumber(coverageSection.split("=")[0]).equals("12")) {
						driver.findElement(By.xpath("//div[contains(text(),'Section "
								+ sectionNumber(coverageSection.split("=")[0]) + " ')]/../../../td[2]//mat-select"))
								.click();
						Thread.sleep(1000);
						getValue(coverageSection.split("=")[1]);
						Thread.sleep(1000);
					}
				} catch (NoSuchElementException ne) {
					System.out.println("Section-"+sectionNumber(coverageSection.split("=")[0])+" is not available.");
				}
			if(sectionNumber(coverageSection.split("=")[0]).equals("4")) {
				try {
					Thread.sleep(2000);
					btnOkFromSec4CoveragePopup.click();
					Thread.sleep(2000);
				} catch (NoSuchElementException e) {
					
				}
			}
			if(sectionNumber(coverageSection.split("=")[0]).equals("3")) {
				try {
					Thread.sleep(2000);
					btnOkFromSec3CoveragePopup.click();
					Thread.sleep(2000);
				} catch (NoSuchElementException e) {
					
				}
			}
			} 
		
		
		String sec27 = db.getRetailDataFromDB("sec27", retailId);				//covered,80000,6months
		driver.findElement(By.xpath("//div[contains(text(),'Section 27 ')]/../../../td[3]//mat-select")).click();
		getValue(sec27.split(",")[0]);
		
		if (sec27.split(",")[0].trim().replaceAll(" ", "").toLowerCase().equals("covered")) {
			Thread.sleep(2000);
			sec27iscovered = true;
			btnOkFromSec27CoveragePopup.click();
			Thread.sleep(2000);
			txtSec27EMIAmount.clear();
			txtSec27EMIAmount.sendKeys(sec27.split(",")[1].trim());
			lstSec27Months.click();
			Thread.sleep(2000);
			getValue(sec27.split(",")[2]);
		}

		String cb = db.getRetailDataFromDB("cb", retailId).trim();
		if ( !cb.equalsIgnoreCase("No") && !chkBonusOption.isSelected()) {
			
			chkBonusOption.click();
			Thread.sleep(2000);
			lstBonusOption.click();
			Thread.sleep(1000);
			for (WebElement element : valueInList) {
				if (element.getText().trim().replaceAll(" ", "").toLowerCase().contains("cb-"+cb.trim().replaceAll(" ", "").toLowerCase())) {
					library.waitForElementToBeClickable(element, 10, 500);
					element.click();Thread.sleep(1000);
					break;
				}
			}
		}
		
	}

	private void getValue(String value) throws InterruptedException {
		for (WebElement element : valueInList) {
			if (element.getText().trim().replaceAll(" ", "").toLowerCase().equals(value.trim().replaceAll(" ", "").toLowerCase())) {
				library.clickOnElementJS(element, library.getDriver());Thread.sleep(1000);
				break;
			}
		}
	}

	private String sectionNumber(String coverage) {
		String[] split = coverage.trim().split("");
		coverage = "";
		for (String ch : split) {
			if ("0123456789".contains(ch)) {
				coverage = coverage + ch;
			}
		}
		return coverage;
	}
}
