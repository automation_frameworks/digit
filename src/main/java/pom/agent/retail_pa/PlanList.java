package pom.agent.retail_pa;

import java.sql.SQLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.utilities.DBUtility;

public class PlanList {

	@FindBy(xpath = "//span[contains(text(),'Option 1')]/../label[text()='Select']")
	private WebElement selectOption1;

	@FindBy(xpath = "//span[contains(text(),'Option 2')]/../label[text()='Select']")
	private WebElement selectOption2;

	@FindBy(xpath = "//span[contains(text(),'Option 3')]/../label[text()='Select']")
	private WebElement selectOption3;

	@FindBy(xpath = "//span[contains(text(),'Option 4')]/../label[text()='Select']")
	private WebElement selectOption4;
	
	
	
	public void selectPAPlan(WebDriver driver,String group,String retailId) throws Exception {
		com.utilities.Library library = new com.utilities.Library();
		library.waitForDigitLoad();
		
		String[] split = group.split(",");
		String column;
		
		for (String part : split) {
			switch(part.replaceAll(" ", "").toLowerCase()) {
			case "family":
				column = "planfamily";
				getPAPlanFromDB(driver,column,retailId);
				break;
			case "parent":
			case "parents":
				column = "planparent";
				getPAPlanFromDB(driver,column,retailId);
				break;
			}
			library.waitForDigitLoad();
		}
	}
	
	public void getPAPlanFromDB(WebDriver driver,String column,String retailId) throws SQLException {
		DBUtility db = PageFactory.initElements(driver, DBUtility.class);
		switch(db.getRetailDataFromDB(column, retailId).replaceAll(" ", "").toLowerCase()) {
		case "option1":
			selectOption1.click();
			break;
		case "option2":
			selectOption2.click();
			break;
		case "option3":
			selectOption3.click();
			break;
		case "option4":
			selectOption4.click();
			break;
		}
	}
}
