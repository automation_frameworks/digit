package pom.agent.retail_pa;

import java.sql.SQLException;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.utilities.DBUtility;

import io.restassured.RestAssured;
import io.restassured.authentication.PreemptiveBasicAuthScheme;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class PersonalAccidentInsurance {

	@FindBy(name = "agentCodeInput")
	private WebElement txtIMD;

	@FindBy(xpath = "//div[contains(text(),'cover')]/../following-sibling::div[1]//span[contains(text(),'Proposer')]/..//input/..")
	private WebElement chkProposer;

	@FindBy(xpath = "//div[contains(text(),'cover')]/../following-sibling::div[1]//span[contains(text(),'Spouse')]/..//input/..")
	private WebElement chkSpouse;

	@FindBy(xpath = "//div[contains(text(),'cover')]/../following-sibling::div[1]//span[contains(text(),'Children')]/..//input/..")
	private WebElement chkChildren;

	@FindBy(xpath = "//div[contains(text(),'cover')]/../following-sibling::div[1]//span[contains(text(),'Father')]/..//input/..")
	private WebElement chkFather;

	@FindBy(xpath = "//div[contains(text(),'cover')]/../following-sibling::div[1]//span[contains(text(),'Mother')]/..//input/..")
	private WebElement chkMother;

	@FindBy(xpath = "//div[contains(text(),'cover')]/../following-sibling::div[1]//span[contains(text(),'Father in Law')]/..//input/..")
	private WebElement chkFatherinLaw;

	@FindBy(xpath = "//div[contains(text(),'cover')]/../following-sibling::div[1]//span[contains(text(),'Mother in Law')]/..//input/..")
	private WebElement chkMotherinLaw;

	@FindBy(xpath = "//button[contains(text(),'+')]")
	private WebElement btnAddChildren;

	@FindBy(xpath = "//label[contains(text(),'Proposer')]/following-sibling::input")
	private WebElement txtProposerAge;

	@FindBy(xpath = "//label[contains(text(),'Spouse')]/following-sibling::input")
	private WebElement txtSpouseAge;

	@FindBy(xpath = "//label[contains(text(),'Child 1')]/following-sibling::input")
	private WebElement txtChild1Age;

	@FindBy(xpath = "//label[contains(text(),'Child 2')]/following-sibling::input")
	private WebElement txtChild2Age;

	@FindBy(xpath = "//label[contains(text(),'Child 3')]/following-sibling::input")
	private WebElement txtChild3Age;

	@FindBy(xpath = "//label[contains(text(),'Child 4')]/following-sibling::input")
	private WebElement txtChild4Age;

	@FindBy(xpath = "//label[contains(text(),'Father')]/following-sibling::input")
	private WebElement txtFatherAge;

	@FindBy(xpath = "//label[contains(text(),'Mother')]/following-sibling::input")
	private WebElement txtMotherAge;

	@FindBy(xpath = "//label[contains(text(),'Father_in_law')]/following-sibling::input")
	private WebElement txtFatherInLawAge;

	@FindBy(xpath = "//label[contains(text(),'Mother_in_law')]/following-sibling::input")
	private WebElement txtMotherInLawAge;
	
	@FindBy(xpath = "//label[contains(text(),'Enter Pincode')]/following-sibling::input")
	private WebElement txtPinCode;

	@FindBy(xpath = "//div[contains(text(),'Earning Member in Family')]/../following-sibling::div[1]//input/..")
	private WebElement chkProposerEarning;

	@FindBy(xpath = "//div[contains(text(),'Earning Member in Family')]/../following-sibling::div[2]//input/..")
	private WebElement chkSpouseEarning;

	@FindBy(xpath = "//div[contains(text(),'occupation class')]/../following-sibling::div[1]//label[text()='Proposer']/..//mat-select")
	private WebElement listProposerOccpnClass;

	@FindBy(xpath = "//div[contains(text(),'occupation class')]/../following-sibling::div[1]//label[contains(text(),'Spouse')]/..//mat-select")
	private WebElement listSpouseOccpnClass;

	@FindBy(xpath = "//div[contains(text(),'occupation class')]/../following-sibling::div[1]//label[text()='Child 1']/..//mat-select")
	private WebElement listChild1OccpnClass;

	@FindBy(xpath = "//div[contains(text(),'occupation class')]/../following-sibling::div[1]//label[text()='Child 2']/..//mat-select")
	private WebElement listChild2OccpnClass;

	@FindBy(xpath = "//div[contains(text(),'occupation class')]/../following-sibling::div[1]//label[text()='Child 3']/..//mat-select")
	private WebElement listChild3OccpnClass;

	@FindBy(xpath = "//div[contains(text(),'occupation class')]/../following-sibling::div[1]//label[text()='Child 4']/..//mat-select")
	private WebElement listChild4OccpnClass;

	@FindBy(xpath = "//div[contains(text(),'occupation class')]/../following-sibling::div[1]//label[text()='Father']/..//mat-select")
	private WebElement listFatherOccpnClass;

	@FindBy(xpath = "//div[contains(text(),'occupation class')]/../following-sibling::div[1]//label[text()='Mother']/..//mat-select")
	private WebElement listMotherOccpnClass;

	@FindBy(xpath = "//div[contains(text(),'occupation class')]/../following-sibling::div[1]//label[text()='Father_in_law']/..//mat-select")
	private WebElement listFatherInLawOccpnClass;

	@FindBy(xpath = "//div[contains(text(),'occupation class')]/../following-sibling::div[1]//label[text()='Mother_in_law']/..//mat-select")
	private WebElement listMotherInLawOccpnClass;

	@FindBys({@FindBy(xpath = "//span[@class='mat-option-text']")})
	private List<WebElement> allRiskClass;

	@FindBys({@FindBy(xpath = "//div[contains(text(),'occupation class')]/../following-sibling::div[1]/div/label")})
	private List<WebElement> allPersons;

	@FindBy(xpath = "//button[contains(text(),'View Options')]")
	private WebElement btnViewOptions;
	
	@FindBy(xpath = "//span[text()='Message:']/..")
	private WebElement errorMessage;
	
	@FindBy(xpath = "//span[text()='Transaction Id:']/..")
	private WebElement errorTransactionId;
	
	@FindBy(xpath = "//span[text()='URL:']/..")
	private WebElement errorURL;
	
	@FindBy(xpath = "//span[text()='Status:']/..")
	private WebElement errorStatus;

	public static boolean spouseIsEarningMember = false;
	public static boolean spouseIsCovered = false;
	public static boolean childIsCovered = false;
	public static boolean fatherIsCovered = false;
	public static boolean motherIsCovered = false;

	com.utilities.Library library = new com.utilities.Library();

	public void selectEarningMember(WebDriver driver,String retailId) throws Exception {

		DBUtility db = PageFactory.initElements(driver, DBUtility.class);
		String dbData = db.getRetailDataFromDB("earning", retailId);
		String[] allEarningMember = dbData.split(",");

		for (String member : allEarningMember) {

			switch(member.replaceAll(" ", "").toLowerCase()) {
			case "proposer":
			case "self":
				chkProposerEarning.click();
				break;
			case "spouse":
				chkSpouseEarning.click();
				spouseIsEarningMember = true;
				break;
			default:
				throw new Exception();
			}
		}
	}

	public void selectOccputaionClass(WebDriver driver,String retailId) throws SQLException, InterruptedException {

		DBUtility db = PageFactory.initElements(driver, DBUtility.class);
		String rc = null;

		for (WebElement person : allPersons) {
			Thread.sleep(2000);

			switch(person.getText().replaceAll(" ", "").toLowerCase()) {
			case "proposer":
				listProposerOccpnClass.click();Thread.sleep(1000);
				rc = db.getRetailDataFromDB("selfrc", retailId);
				for (WebElement riskClass : allRiskClass) {
					if (rc.replaceAll(" ", "").toLowerCase().equals(riskClass.getText().replaceAll(" ", "").toLowerCase())) {
						library.clickOnElementJS(riskClass, driver);Thread.sleep(2000);
						break;
					}
				}
				break;
			case "spouse/partner":
				listSpouseOccpnClass.click();Thread.sleep(1000);
				rc = db.getRetailDataFromDB("spouserc", retailId);
				for (WebElement riskClass : allRiskClass) {
					if (rc.replaceAll(" ", "").toLowerCase().equals(riskClass.getText().replaceAll(" ", "").toLowerCase())) {
						library.clickOnElementJS(riskClass, driver);Thread.sleep(2000);
						//						if (rc.replaceAll(" ", "").toLowerCase().equals("riskclassiii")) {
						//							spouseRC3 = true;
						//						}
						break;
					}
				}
				break;
			case "child1":
				listChild1OccpnClass.click();Thread.sleep(1000);
				rc = db.getRetailDataFromDB("c1rc", retailId);
				for (WebElement riskClass : allRiskClass) {
					if (rc.replaceAll(" ", "").toLowerCase().equals(riskClass.getText().replaceAll(" ", "").toLowerCase())) {
						library.clickOnElementJS(riskClass, driver);Thread.sleep(2000);
						break;
					}
				}
				break;
			case "child2":
				listChild2OccpnClass.click();Thread.sleep(1000);
				rc = db.getRetailDataFromDB("c2rc", retailId);
				for (WebElement riskClass : allRiskClass) {
					if (rc.replaceAll(" ", "").toLowerCase().equals(riskClass.getText().replaceAll(" ", "").toLowerCase())) {
						library.clickOnElementJS(riskClass, driver);Thread.sleep(2000);
						break;
					}
				}
				break;
			case "child3":
				listChild3OccpnClass.click();Thread.sleep(1000);
				rc = db.getRetailDataFromDB("c1rc", retailId);
				for (WebElement riskClass : allRiskClass) {
					if (rc.replaceAll(" ", "").toLowerCase().equals(riskClass.getText().replaceAll(" ", "").toLowerCase())) {
						library.clickOnElementJS(riskClass, driver);Thread.sleep(2000);
						break;
					}
				}
				break;
			case "child4":
				listChild4OccpnClass.click();Thread.sleep(1000);
				rc = db.getRetailDataFromDB("c2rc", retailId);
				for (WebElement riskClass : allRiskClass) {
					if (rc.replaceAll(" ", "").toLowerCase().equals(riskClass.getText().replaceAll(" ", "").toLowerCase())) {
						library.clickOnElementJS(riskClass, driver);Thread.sleep(2000);
						break;
					}
				}
				break;
			case "father":
				listFatherOccpnClass.click();Thread.sleep(1000);
				rc = db.getRetailDataFromDB("fatherrc", retailId);
				for (WebElement riskClass : allRiskClass) {
					if (rc.replaceAll(" ", "").toLowerCase().equals(riskClass.getText().replaceAll(" ", "").toLowerCase())) {
						library.clickOnElementJS(riskClass, driver);Thread.sleep(2000);
						break;
					}
				}
				break;
			case "mother":
				listMotherOccpnClass.click();Thread.sleep(1000);
				rc = db.getRetailDataFromDB("motherrc", retailId);
				for (WebElement riskClass : allRiskClass) {
					if (rc.replaceAll(" ", "").toLowerCase().equals(riskClass.getText().replaceAll(" ", "").toLowerCase())) {
						library.clickOnElementJS(riskClass, driver);Thread.sleep(2000);
						break;
					}
				}
				break;
			case "father_in_law":
				listFatherInLawOccpnClass.click();Thread.sleep(1000);
				rc = db.getRetailDataFromDB("fatherrc", retailId);
				for (WebElement riskClass : allRiskClass) {
					if (rc.replaceAll(" ", "").toLowerCase().equals(riskClass.getText().replaceAll(" ", "").toLowerCase())) {
						library.clickOnElementJS(riskClass, driver);Thread.sleep(2000);
						break;
					}
				}
				break;
			case "mother_in_law":
				listMotherInLawOccpnClass.click();Thread.sleep(1000);
				rc = db.getRetailDataFromDB("motherrc", retailId);
				for (WebElement riskClass : allRiskClass) {
					if (rc.replaceAll(" ", "").toLowerCase().equals(riskClass.getText().replaceAll(" ", "").toLowerCase())) {
						library.clickOnElementJS(riskClass, driver);Thread.sleep(2000);
						break;
					}
				}
				break;

			}
		}
	}


	public void cover(WebDriver driver,String people,String retailId) throws SQLException, InterruptedException {

		DBUtility db = PageFactory.initElements(driver, DBUtility.class);
		String dob; int i; boolean flag = true;
		String[] persons = people.split(",");
		for (String person : persons) {
			try {
				switch (person.replaceAll(" ", "").replaceAll("_", "").replaceAll("-", "").toLowerCase()) {
				case "proposer":
				case "self":
					dob = db.getRetailDataFromDB("selfdob", retailId);
					i = 0;
					while (i < 3) {
						txtProposerAge.sendKeys(dob.split("-")[i++]);
						Thread.sleep(1000);
					}
					flag = true;
					break;
				case "spouse":
				case "partner":
					chkSpouse.click();
					spouseIsCovered = true;
					dob = db.getRetailDataFromDB("spousedob", retailId);
					i = 0;
					while (i < 3) {
						txtSpouseAge.sendKeys(dob.split("-")[i++]);
						Thread.sleep(1000);
					}
					break;
				case "child1":
					chkChildren.click();
					childIsCovered = true;
					dob = db.getRetailDataFromDB("c1dob", retailId);
					i = 0;
					while (i < 3) {
						txtChild1Age.sendKeys(dob.split("-")[i++]);
						Thread.sleep(1000);
					}
					break;
				case "child2":
					btnAddChildren.click();
					dob = db.getRetailDataFromDB("c2dob", retailId);
					i = 0;
					while (i < 3) {
						txtChild2Age.sendKeys(dob.split("-")[i++]);
						Thread.sleep(1000);
					}
					break;
				case "child3":
					btnAddChildren.click();
					dob = db.getRetailDataFromDB("c3dob", retailId);
					i = 0;
					while (i < 3) {
						txtChild3Age.sendKeys(dob.split("-")[i++]);
						Thread.sleep(1000);
					}
					break;
				case "child4":
					btnAddChildren.click();
					dob = db.getRetailDataFromDB("c4dob", retailId);
					i = 0;
					while (i < 3) {
						txtChild4Age.sendKeys(dob.split("-")[i++]);
						Thread.sleep(1000);
					}
					break;
				case "father":
					chkFather.click();
					fatherIsCovered =  true;
					dob = db.getRetailDataFromDB("fatherdob", retailId);
					i = 0;
					while (i < 3) {
						txtFatherAge.sendKeys(dob.split("-")[i++]);
						Thread.sleep(1000);
					}
					break;
				case "mother":
					chkMother.click();
					motherIsCovered =  true;
					dob = db.getRetailDataFromDB("motherdob", retailId);
					i = 0;
					while (i < 3) {
						txtMotherAge.sendKeys(dob.split("-")[i++]);
						Thread.sleep(1000);
					}
					break;
				case "fatherinlaw":
					chkFatherinLaw.click();
					fatherIsCovered =  true;
					dob = db.getRetailDataFromDB("fatherdob", retailId);
					i = 0;
					while (i < 3) {
						txtFatherInLawAge.sendKeys(dob.split("-")[i++]);
						Thread.sleep(1000);
					}
					break;
				case "motherinlaw":
					chkMotherinLaw.click();
					motherIsCovered =  true;
					dob = db.getRetailDataFromDB("motherdob", retailId);
					i = 0;
					while (i < 3) {
						txtMotherInLawAge.sendKeys(dob.split("-")[i++]);
						Thread.sleep(1000);
					}
					break;
				}
			} finally {
				if (flag) {
					dob = db.getRetailDataFromDB("selfdob", retailId);
					i = 0;
					while (i < 3) {
						txtProposerAge.sendKeys(dob.split("-")[i++]);
						Thread.sleep(1000);
					}
				}
			}
		}
		txtPinCode.sendKeys(db.getRetailDataFromDB("pincode", retailId));
	}

	public void getViewOptions(WebDriver driver,String id) throws Exception {

		library.waitForDigitLoad();
//		btnViewOptions.click();
		library.clickOnElementJS(btnViewOptions, driver);

		try {
			library.waitForDigitLoad();
			DBUtility db = PageFactory.initElements(driver, DBUtility.class);
			Thread.sleep(3000);
			db.storeScriptFailCommentInDB("Message: "+errorMessage.getText()+"\n\nTransaction Id: "+errorTransactionId.getText()+"\n\nURL: "+errorURL.getText()+"\n\nStatus: "+errorStatus.getText(), id, "digit_retail_pa.retail_pa_testdata");
			System.out.println("Message: "+errorMessage.getText()+"\n\nTransaction Id: "+errorTransactionId.getText()+"\n\nURL: "+errorURL.getText()+"\n\nStatus: "+errorStatus.getText());
		} catch (Exception e) {

		}
		
	//	RestAssured.baseURI = library.getDriver().getCurrentUrl();
		try {
//			Thread.sleep(5000);
			
			
//			Set<Cookie> c = library.getDriver().manage().getCookies();
//			
//			Cookie cn = library.getDriver().manage().getCookieNamed("quickQuote");
//			
//			System.out.println(c);
//			System.out.println(cn);
			
//			String script = "return window.performance.getEntries();";
//			
//			JavascriptExecutor jse = (JavascriptExecutor)library.getDriver();
//			Object executeScript = jse.executeScript(script);
//			System.out.println("starts");
//			ArrayList l = (ArrayList)executeScript;
//			System.out.println("my data is : " + l);
//			LogEntries les = library.getDriver().manage().logs().get(LogType.PERFORMANCE);
//			
//			for (LogEntry le : les) {
//		        System.out.println(le.getMessage());
//		    }
//			PreemptiveBasicAuthScheme authScheme = new PreemptiveBasicAuthScheme();
//			authScheme.setUserName("login");
//			authScheme.setPassword("password");
//			RestAssured.authentication = authScheme;
//			String bearerToken = "56717940-b94f-4546-aa62-c68c7b6addbe-1614582603565";
//			Response response = (Response) RestAssured.given().headers( "Authorization","Bearer " + bearerToken,"Content-Type",ContentType.JSON,"Accept",ContentType.JSON)
//					.when().post("https://preprod-digitplusservice.godigit.com/digitplusservices/health/quickQuote")
//					.then().contentType(ContentType.JSON).extract().response();
////            
//			System.out.println(response.getBody().toString());
//			System.out.println(response.getBody().asString());
	//		RestAssured.get("https://preprod-digitplusservice.godigit.com/digitplusservices/health/quickQuote").then().log().all();
			
//			Response resp = RestAssured.post("https://preprod-digitplusservice.godigit.com/digitplusservices/health/quickQuote");
//			
//			resp.then().log().all();
//			System.out.println();
//			Object obj = resp.jsonPath().get("");
//			System.out.println(obj);
//			System.out.println(obj.toString());
//			System.out.println((String)obj);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		
//		String url = library.getDriver().getCurrentUrl();
//		URL obj = new URL(url);
//		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
//
//		// add reuqest header
//		con.setRequestMethod("GET");
//		con.setRequestProperty("Accept", "application/json, text/plain, */*");
//
//
//		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
//		String inputLine;
//		StringBuffer response = new StringBuffer();
//
//		while ((inputLine = in.readLine()) != null) {
//			response.append(inputLine);
//		}
//		in.close();
//		
//		try {
//			JsonObject jsonObject = new JsonParser().parse(response).getAsJsonObject();
//			String zone = jsonObject.getAsJsonObject("cityPincodeZone").get("city").getAsString();
//			System.out.println(zone);
//			System.out.println(zone);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		System.out.println("Response XXXXXXXX captured");
	}
}
