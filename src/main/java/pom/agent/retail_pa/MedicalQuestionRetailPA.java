package pom.agent.retail_pa;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.utilities.DBUtility;
import com.utilities.Library;

public class MedicalQuestionRetailPA {
	
	@FindBy(xpath = "//button[contains(text(),'Next')]")
	private WebElement btnNext;
	
	@FindBy(xpath = "//button[text()='Confirm and Proceed']")
	private WebElement btnConfirmAndProceed;
	
	@FindBy(xpath = "//span[contains(text(),'None')]/..//input")
	private WebElement chkNone;
	

	@FindBy(xpath = "//span[text()='Message:']/..")
	private WebElement errorMessage;
	
	@FindBy(xpath = "//span[text()='Transaction Id:']/..")
	private WebElement errorTransactionId;
	
	@FindBy(xpath = "//span[text()='URL:']/..")
	private WebElement errorURL;
	
	@FindBy(xpath = "//span[text()='Status:']/..")
	private WebElement errorStatus;

	public void selectMedicalCondition(WebDriver driver,String retailId) throws Exception {
		
		DBUtility db = PageFactory.initElements(driver, DBUtility.class);
		com.utilities.Library library = new com.utilities.Library();
		
		String[] people = db.getRetailDataFromDB("ped", retailId).split(",");
		for (String person : people) {
			Thread.sleep(2000);
			switch(person.replaceAll(" ", "").replaceAll("-", "").replaceAll("_", "").toLowerCase()) {
			case "none":
				if (!chkNone.isSelected()) {
					driver.findElement(By.xpath(
							"//label[contains(text(),'Does anyone in your family')]/../div//span[text()='None']"))
							.click();
				}
				break;
			case "proposer":
			case "self":
				System.out.println(getFirstLetterOfWordInCaps(MemberDetailsRetailPA.hm.get("SELF")));
				driver.findElement(By.xpath(
						"//label[contains(text(),'Does anyone in your family')]/../div//span[text()='"
								+ getFirstLetterOfWordInCaps(MemberDetailsRetailPA.hm.get("SELF")) + "']"))
				.click();
				break;
			case "spouse":
				driver.findElement(By.xpath(
						"//label[contains(text(),'Does anyone in your family')]/../div//span[text()='"
								+ getFirstLetterOfWordInCaps(MemberDetailsRetailPA.hm.get("SPOUSE")) + "']"))
				.click();
				break;
			case "child1":
				driver.findElement(By.xpath(
						"//label[contains(text(),'Does anyone in your family')]/../div//span[text()='"
								+ getFirstLetterOfWordInCaps(MemberDetailsRetailPA.hm.get("CHILD 1")) + "']"))
				.click();
				break;
			case "child2":
				driver.findElement(By.xpath(
						"//label[contains(text(),'Does anyone in your family')]/../div//span[text()='"
								+ getFirstLetterOfWordInCaps(MemberDetailsRetailPA.hm.get("CHILD 2")) + "']"))
				.click();
				break;
			case "child3":
				driver.findElement(By.xpath(
						"//label[contains(text(),'Does anyone in your family')]/../div//span[text()='"
								+ getFirstLetterOfWordInCaps(MemberDetailsRetailPA.hm.get("CHILD 3")) + "']"))
				.click();
				break;
			case "child4":
				driver.findElement(By.xpath(
						"//label[contains(text(),'Does anyone in your family')]/../div//span[text()='"
								+ getFirstLetterOfWordInCaps(MemberDetailsRetailPA.hm.get("CHILD 4")) + "']"))
				.click();
				break;
			case "father":
				driver.findElement(By.xpath(
						"//label[contains(text(),'Does anyone in your family')]/../div//span[text()='"
								+ getFirstLetterOfWordInCaps(MemberDetailsRetailPA.hm.get("FATHER")) + "']"))
				.click();
				break;
			case "mother":
				driver.findElement(By.xpath(
						"//label[contains(text(),'Does anyone in your family')]/../div//span[text()='"
								+ getFirstLetterOfWordInCaps(MemberDetailsRetailPA.hm.get("MOTHER")) + "']"))
				.click();
				break;
			case "fatherinlaw":
				driver.findElement(By.xpath(
						"//label[contains(text(),'Does anyone in your family')]/../div//span[text()='"
								+ getFirstLetterOfWordInCaps(MemberDetailsRetailPA.hm.get("FATHER_IN_LAW")) + "']"))
				.click();
				break;
			case "motherinlaw":
				driver.findElement(By.xpath(
						"//label[contains(text(),'Does anyone in your family')]/../div//span[text()='"
								+ getFirstLetterOfWordInCaps(MemberDetailsRetailPA.hm.get("MOTHER_IN_LAW")) + "']"))
				.click();
				break;
			}
		}
		Thread.sleep(2000);
		btnNext.click();
		Thread.sleep(2000);
		try {
			btnConfirmAndProceed.click();
		} catch (Exception e) {
		}
		try {
			library.waitForDigitLoad();
			Thread.sleep(3000);
			db.storeScriptFailCommentInDB("Message: "+errorMessage.getText()+"\n\nTransaction Id: "+errorTransactionId.getText()+"\n\nURL: "+errorURL.getText()+"\n\nStatus: "+errorStatus.getText(), retailId, "digit_retail_pa.retail_pa_testdata");
			System.out.println("Message: "+errorMessage.getText()+"\n\nTransaction Id: "+errorTransactionId.getText()+"\n\nURL: "+errorURL.getText()+"\n\nStatus: "+errorStatus.getText());
		} catch (Exception e) {

		}
		library.waitForDigitLoad();
	}
	
	private String getFirstLetterOfWordInCaps(Object obj) {
		String name = obj + "";
		String[] temp = name.split("");
		temp[0] = temp[0].toUpperCase();
		for (int i = 1; i < temp.length; i++) {
			temp[i] = temp[i].toLowerCase();
		}
		name = "";
		for (String string : temp) {
			name = name + string;
		}
		return name;
	}
}
