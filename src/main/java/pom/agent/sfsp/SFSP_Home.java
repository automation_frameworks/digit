package pom.agent.sfsp;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.utilities.Library;
import com.utilities.PropertyFileReader;

public class SFSP_Home {
	@FindBy(xpath = "//div[text()='New Business']")
	private WebElement radNewBusiness;

	@FindBy(xpath = "//div[text()='Rollover ']")
	private WebElement radRollOver;

	@FindBy(xpath = "//mat-radio-button[@ng-reflect-value='INDIVIDUAL']")
	private WebElement radIndividual;

	@FindBy(xpath = "//mat-radio-button[@ng-reflect-value='COMPANY']")
	private WebElement radCorporate;

	@FindBy(xpath = "//label[text()='Occupancy Type']/following-sibling::div//input")
	private WebElement txtOccupanyType;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='constructionType']/mat-radio-button[@value='P']")
	private WebElement radPucca;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='constructionType']/mat-radio-button[@value='K']")
	private WebElement radKutcha;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='basementExposed']/mat-radio-button[@value='J']")
	private WebElement radBasementExposedYes;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='basementExposed']/mat-radio-button[@value='P']")
	private WebElement radBasementExposedOnlyParking;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='basementExposed']/mat-radio-button[@value='N']")
	private WebElement radBasementExposedNo;

	@FindBy(xpath = "//label[text()='Floor Number']/following-sibling::div//input")
	private WebElement txtFloorNumber;

	@FindBy(name = "floorInfo1")
	private WebElement txtNumberofFloorsToBeCovered;

	@FindBy(xpath = "//label[text()='Build Up Year']/following-sibling::div//input")
	private WebElement txtBuiltUpYear;

	@FindBy(xpath = "//label[text()='Hypothecated to']/following-sibling::input")
	private WebElement txtHypothecatedTo;

	@FindBy(xpath = "//label[text()='Address']/following-sibling::input")
	private WebElement txtAddress;

	@FindBy(xpath = "//label[text()='Pincode']/following-sibling::input")
	private WebElement txtPinCode;

	@FindBy(xpath = "//div[@class='display-flex-forchk fire-checkbox-top-pad']/div/mat-checkbox/label/div/following-sibling::span")
	private List<WebElement> fireArrangementsCheckBoxes;

	@FindBy(xpath = "//input[@formcontrolname='address']")
	private WebElement address;

	@FindBy(xpath = "//input[@formcontrolname='pincode']")
	private WebElement pincode;

	@FindBy(xpath = "//input[@formcontrolname='buildingIDV']")
	private WebElement buildingIDV;

	@FindBy(xpath = "//input[@formcontrolname='plinthIDV']")
	private WebElement plinthIDV;

	@FindBy(xpath = "//input[@formcontrolname='machineryIDV']")
	private WebElement machineryIDV;

	@FindBy(xpath = "//input[@formcontrolname='furnitureIDV']")
	private WebElement furnitureIDV;

	@FindBy(xpath = "//input[@formcontrolname='stockProgressIDV']")
	private WebElement stockProgressIDV;

	@FindBy(xpath = "//input[@formcontrolname='stockOtherIDV']")
	private WebElement stockOtherIDV;

	@FindBy(xpath = "//input[@formcontrolname='otherIDV']")
	private WebElement otherIDV;

	@FindBy(xpath = "//input[@formcontrolname='lossOfRentIDV']")
	private WebElement lossOfRentIDV;

	@FindBy(xpath = "//button[span[contains(text(),'Show Premium')]]")
	private WebElement btnShowPremium;

	@FindBy(xpath = "//button[contains(text(),'Save Quote')]")
	private WebElement btnSaveQuote;

	@FindBy(xpath = "//p[text()='Quote Number']/../div/p")
	private WebElement lblQuoteNumber;

	@FindBy(xpath = "//button[span[contains(text(),'Proceed')]]")
	private WebElement btnProceedToPay;

	@FindBy(xpath = "//input[@formcontrolname='insurerCompany']")
	private WebElement txtExpiringInsurer;

	@FindBy(xpath = "//input[@formcontrolname='previousPolicyNumber']")
	private WebElement prevPolicyNoTextField;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='previousLoss']/mat-radio-button[@value='yes']")
	private WebElement radAnyPastLossYes;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='previousLoss']/mat-radio-button[@value='no']")
	private WebElement radAnyPastLossNo;

	@FindBy(xpath = "//label[text()='No. Of Claims']/following-sibling::input")
	private WebElement txtNoOfClaims;

	@FindBy(xpath = "//p[text()='Customer Details']")
	private WebElement customerDetailsLabel;

	@FindBy(id = "firstName")
	private WebElement txtInsuredName;

	@FindBy(xpath = "//section[@class='card policy-card-margin']//input[@formcontrolname='lastName']")
	private WebElement txtLastName;

	@FindBy(xpath = "//section[@class='card policy-card-margin']//input[@formcontrolname='pincode']")
	private WebElement txtCustomerPinCode;

	@FindBy(xpath = "//section[@class='card policy-card-margin']//input[@formcontrolname='address']")
	private WebElement txtCustomerAddress;

	@FindBy(xpath = "//section[@class='card policy-card-margin']//input[@formcontrolname='mobileNumber']")
	private WebElement txtMobileNumber;

	@FindBy(xpath = "//section[@class='card policy-card-margin']//input[@formcontrolname='emailId']")
	private WebElement txtEmailID;

	@FindBy(xpath = "//section[@class='card policy-card-margin']//input[@formcontrolname='aadharNumber']")
	private WebElement txtAadharNumber;

	@FindBy(xpath = "//section[@class='card policy-card-margin']//input[@formcontrolname='panNumber']")
	private WebElement txtPANNumber;

	@FindBy(xpath = "//p[text()='Payment']")
	private WebElement paymentLabel;

	@FindBy(xpath = "//label[text()='Payment Mode']/..//mat-select")
	private WebElement drpPaymentMode;

	@FindBy(xpath = "//input[@formcontrolname='chequeNumber']")
	private WebElement txtChequeNumber;

	@FindBy(xpath = "//input[@formcontrolname='IFSC']")
	private WebElement txtIFSC;

	@FindBy(id = "Successfully-Issued")
	private WebElement lblSuccessMessage;

	@FindBy(xpath = "//div[@id='SFSPrefferalModal']//button[text()='Yes']")
	private WebElement btnWarningYes;

	private com.utilities.Library library = new com.utilities.Library();

	public void selectPolicyDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "policytype":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "new":
					case "newbusiness":
						radNewBusiness.click();
						break;
					case "rollover":
						radRollOver.click();
						break;
					default:
						throw new Exception("There is no policy type [" + hmap.get(key) + "].");
					}
					break;
				case "ownershiptype":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "individual":
						radIndividual.click();
						break;
					case "corporate":
						radCorporate.click();
						break;
					default:
						throw new Exception("There is no ownership type [" + hmap.get(key) + "].");
					}
					break;
				default:
					throw new Exception("There is no such ownership [" + key + "]");
				}
			}
		} catch (Exception e) {
			throw new Exception("Error while selecting the ownership type : " + e.getMessage());
		}
	}

	public void enterBuildingDetails(String details) throws Exception {
		boolean flag = false;
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "occupancytype":
					txtOccupanyType.sendKeys(hmap.get(key));
					Thread.sleep(3000);
					library.getDriver().findElement(By.xpath(
							"//div[contains(@class,'dropdownCSS')]//*[contains(text(),'" + hmap.get(key) + "')]"))
							.click();
					break;
				case "typeofconstruction":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "pucca":
						radPucca.click();
						break;
					case "kutcha":
						radKutcha.click();
						break;
					default:
						throw new Exception("There is no such type of construction : [" + hmap.get(key) + "].");
					}
					break;
				case "isbasementexposed":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "yes":
						radBasementExposedYes.click();
						break;
					case "onlyparking":
						radBasementExposedOnlyParking.click();
						break;
					case "no":
						radBasementExposedNo.click();
						break;
					default:
						throw new Exception("There is no such base exposed option : [" + hmap.get(key) + "].");
					}
					break;
				case "firefightingarrangements":
					for(String temp : hmap.get(key).split(","))
					for (int i = 0; i < fireArrangementsCheckBoxes.size(); i++) {
						if (fireArrangementsCheckBoxes.get(i).getText().equalsIgnoreCase(temp)) {
							fireArrangementsCheckBoxes.get(i).click();
							flag = true;
						}
					}
					if (!flag)
						throw new Exception("The data is not matching:" + hmap.get(key));
					break;
				case "floornumber":
					txtFloorNumber.sendKeys(hmap.get(key));
					break;
				case "buildupyear":
					txtBuiltUpYear.sendKeys(hmap.get(key));
					break;
				case "hypothecatedto":
					txtHypothecatedTo.sendKeys(hmap.get(key));
					break;
				case "address":
					address.sendKeys(hmap.get(key));
					Thread.sleep(1000);
					address.sendKeys(Keys.ARROW_DOWN, Keys.TAB);
					break;
				case "pincode":
					pincode.sendKeys(hmap.get(key));
					break;
				default:
					throw new Exception("There is no such field : " + key);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the building details : " + e.getMessage());
		}
	}

	public void enterBaseCoverageDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "building":
					buildingIDV.sendKeys(hmap.get(key));
					break;
				case "plinth&foundation":
				case "plinth":
					plinthIDV.sendKeys(hmap.get(key));
					break;
				case "machinery&accessories":
				case "machinery":
					machineryIDV.sendKeys(hmap.get(key));
					break;
				case "furniture&fixtures":
				case "furnitures":
					furnitureIDV.sendKeys(hmap.get(key));
					break;
				case "stocksinprogress":
				case "stocks(workinprogress)":
					stockProgressIDV.sendKeys(hmap.get(key));
					break;
				case "stocksothers":
				case "stocks(others)":
					stockOtherIDV.sendKeys(hmap.get(key));
					break;
				case "otherproperty":
					otherIDV.sendKeys(hmap.get(key));
					break;
				case "lossofproperty":
					lossOfRentIDV.sendKeys(hmap.get(key));
					break;
				default:
					throw new Exception("There is no such field : " + key);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the building details : " + e.getMessage());
		}
	}

	public void enterPreviousInsurerDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "expiringinsurer":
					txtExpiringInsurer.sendKeys(hmap.get(key));
					library.getDriver()
							.findElement(By.xpath("//section[@formarrayname='previousInsurer']/descendant::div[contains(@class, 'dropdownCSSYear')]/descendant::*[contains(text(), '" + hmap.get(key) + "')]"))
							.click();
					break;
				case "previouspolicynumber":
					prevPolicyNoTextField.sendKeys(hmap.get(key));
					break;
				case "anypastlossexperience":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "yes":
						radAnyPastLossYes.click();
						break;
					case "no":
						radAnyPastLossNo.click();
						break;
					}
					break;
				case "no.ofclaims":
				case "noofclaims":
					txtNoOfClaims.sendKeys(hmap.get(key));
					break;
				default:
					throw new Exception("There is no such field : " + key);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the previous insurer details : " + e.getMessage());
		}
	}

	public void enterClaimDetails(String details, String claimNumber) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String temp : claimNumber.split(",")) {
				for (String key : hmap.keySet()) {
					library.waitForDigitLoad();
					switch (key.replaceAll(" ", "").toLowerCase()) {
					case "amountclaimed":
						library.getDriver()
								.findElement(By.xpath("//label[text()='Claim " + temp.trim()
										+ "']/..//label[text()='Amount Claimed']/following-sibling::input"))
								.sendKeys(hmap.get(key));
						break;
					case "whatisthereason?":
					case "reason":
						library.getDriver()
								.findElement(By.xpath("//label[text()='Claim " + temp.trim()
										+ "']/..//label[text()='What is the Reason?']/following-sibling::input"))
								.sendKeys(hmap.get(key));
						break;
					default:
						throw new Exception("There is no such field : " + key);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the claim details : " + e.getMessage());
		}
	}

	public void performAction(String action) throws Exception {
		try {
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "showpremium":
				btnShowPremium.click();
				Thread.sleep(2000);
				library.waitForDigitLoad();
				break;
			case "savequote":
				btnSaveQuote.click();
				Thread.sleep(2000);
				library.waitForDigitLoad();
				Library.staticMap.put("Policy Number", lblQuoteNumber.getText());
				break;
			case "proceedtopay":
				library.waitForDigitLoad();
				btnProceedToPay.click();
				try {
					btnWarningYes.click();
				} catch (Exception ne) {

				}
				library.waitForDigitLoad();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while performing action : [" + action + "]" + e.getMessage());
		}
	}

	public void enterCustomerDetails(String details) throws Exception {
		try {
			library.focus(library.getDriver(), customerDetailsLabel);

			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "insuredname":
				case "firstname":
					txtInsuredName.sendKeys(hmap.get(key));
					break;
				case "lastname":
					txtLastName.sendKeys(hmap.get(key));
					break;
				case "pincode":
					txtCustomerPinCode.sendKeys(hmap.get(key));
					break;
				case "address":
					txtCustomerAddress.sendKeys(hmap.get(key));
					break;
				case "mobilenumber":
					txtMobileNumber.sendKeys(hmap.get(key));
					break;
				case "emailid":
					txtEmailID.sendKeys(hmap.get(key));
					break;
				case "aadharnumber":
					txtAadharNumber.sendKeys(hmap.get(key));
					break;
				case "pannumber":
					txtPANNumber.sendKeys(hmap.get(key));
					break;
				default:
					throw new Exception("There is no such field [" + key + "]");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the employee compensation details : " + e.getMessage());
		}
	}

	public void enterPaymentDetails(String paymentMode) throws Exception {
		try {
			library.focus(library.getDriver(), paymentLabel);

			String environment;
			if (System.getProperty("environment") == null || System.getProperty("environment").equals("")) {
				PropertyFileReader prop = new PropertyFileReader();
				environment = prop.getEnvironment().toString();
			} else
				environment = System.getProperty("environment");
			if (environment.equalsIgnoreCase("prod"))
				paymentMode = "Generate Payment Link";

			switch (paymentMode.replaceAll(" ", "").toLowerCase()) {
			case "agentfloat":
				drpPaymentMode.click();
				Thread.sleep(1000);
				library.getDriver().findElement(By.xpath("//mat-option//span[normalize-space()='Agent Float']"))
						.click();
				break;
			case "paymentbycustomercheque":
				drpPaymentMode.click();
				Thread.sleep(1000);
				library.getDriver()
						.findElement(By.xpath("//mat-option//span[normalize-space()='Payment by Customer Cheque']"))
						.click();
				txtChequeNumber.sendKeys(library.getRandomValue("[N | 6]"));
				txtIFSC.sendKeys("ADCB0000002");
				txtIFSC.sendKeys(Keys.TAB);
				break;
			case "paymentbypartnercheque":
				drpPaymentMode.click();
				Thread.sleep(1000);
				library.getDriver()
						.findElement(By.xpath("//mat-option//span[normalize-space()='Payment by Partner Cheque']"))
						.click();
				txtChequeNumber.sendKeys(library.getRandomValue("[N | 6]"));
				txtIFSC.sendKeys("ADCB0000002");
				txtIFSC.sendKeys(Keys.TAB);
				break;
			case "sendpaymentlink":
				drpPaymentMode.click();
				Thread.sleep(1000);
				library.getDriver().findElement(By.xpath("//mat-option//span[normalize-space()='Send Payment Link']"))
						.click();
				break;
			case "generatepaymentlink":
				drpPaymentMode.click();
				Thread.sleep(1000);
				library.getDriver()
						.findElement(By.xpath("//mat-option//span[normalize-space()='Generate Payment Link']")).click();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the payment details : " + e.getMessage());
		}
	}

	public void verifySuccessPage() throws Exception {
		try {
			if (!lblSuccessMessage.isDisplayed()) {
				throw new Exception("Success message is not displayed");
			}
		} catch (Exception e) {
			throw new Exception("Error while verifying the success page : " + e.getMessage());
		}
	}
}
