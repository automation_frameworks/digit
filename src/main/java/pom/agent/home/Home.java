package pom.agent.home;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.utilities.Library;

public class Home {

	
	@FindBy(xpath = "//a[@class='close']")
	private WebElement closePopup;

	@FindBy(xpath="//app-digit-award[@class='ng-star-inserted']/img")
	WebElement asiaPopup;
	
	@FindBy(xpath = "//img[@alt='DigitPlus']")
	private WebElement imgHome;
	
	@FindBy(xpath = "//img[contains(@src,'DigitAward')]")
	private WebElement imgDigitAward;
	
	@FindBy(xpath="//p[text()='Motor Insurance'][1]")
	private WebElement lblMotorInsurance;
	
	public void getClosePopup(WebDriver driver) throws Exception {
		Thread.sleep(5000);
		driver.findElement(By.xpath("//a[@class='close']")).click();
	}
//	public void selectModule(String module) throws Exception {
//		try {
//			Thread.sleep(5000);
	

//	public void selectModule(String module) throws Exception {
//		try {
//			Thread.sleep(5000);
//			com.utilities.Library libraryobj = new com.utilities.Library();
//			libraryobj.waitForDigitLoad();
//			WebDriver driver = libraryobj.getDriver();
//			WebElement element = driver
//					.findElement(By.xpath("//p[text()='" + module.trim() + "']/../.."));
//			try {
//				libraryobj.waitForElementToBeClickable(element, 15, 500);
//				JavascriptExecutor executor = (JavascriptExecutor)driver;
//				executor.executeScript("arguments[0].click();", element);
//				//element.click();
//				System.out.println("Module is clicked");
//			} catch (NoSuchElementException ne) {
//				ne.printStackTrace();
//				Thread.sleep(5000);
////				driver.findElement(By.xpath("//div[@class = 'card']//p[text()='" + module.trim() + "']")).click();
////				imgDigitAward.click();
//				for (int i = 0; i < 2; i++) {					
//					selectModule(module);
//				}
//			} 
////			imgHome.click();
////			libraryobj.waitForDigitLoad();
////			libraryobj.waitForElementVisible(lblMotorInsurance, 20, 500);
////			try {
////				driver.findElement(By.xpath("//div[contains(@class,'forMobileView')]//i/following-sibling::p[text()='" + module.trim() + "']")).click();
////			} catch (NoSuchElementException ne) {
////				System.out.println("Inside exception for selecting the module");
////				Thread.sleep(5000);
////				driver.findElement(By.xpath("//p[text()='" + module.trim() + "']")).click();
////			}
//			libraryobj.waitForDigitLoad();
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new Exception("Error while selecting the module : " + module + " ===> " + e.getMessage());
//		}
//		Thread.sleep(2000);
//	}
	
	
	public void selectModule(String module) throws Exception {
		try {
			Thread.sleep(2000);
			com.utilities.Library libraryobj = new com.utilities.Library();
			libraryobj.waitForDigitLoad();
			WebDriver driver = libraryobj.getDriver();
			try {
				imgDigitAward.click();
			} catch (NoSuchElementException ne) {
			}
			imgHome.click();
			libraryobj.waitForDigitLoad();
			libraryobj.waitForElementVisible(lblMotorInsurance, 20, 500);
			try {
				driver.findElement(By.xpath("//div[contains(@class,'forMobileView')]//i/following-sibling::p[text()='" + module.trim() + "']")).click();
			} catch (NoSuchElementException ne) {
				System.out.println("Inside exception for selecting the module");
				Thread.sleep(5000);
				driver.findElement(By.xpath("//p[text()='" + module.trim() + "']")).click();
			}
			libraryobj.waitForDigitLoad();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while selecting the module : " + module + " ===> " + e.getMessage());
		}
		Thread.sleep(2000);
	}
	
	public void clickOnAsiaPopup()
	{
		asiaPopup.click();
	}
	
	public void selectSubModuleFromAgentPortal(String subModule) throws Exception {
		
		com.utilities.Library library = new com.utilities.Library();
		library.waitForDigitLoad();
		library.waitForElementVisible(By.xpath("//p[@class = 'boxMainTitle' and text()='"+subModule+"']"), 20, 1000);
		Thread.sleep(3000);
		library.getDriver().findElement(By.xpath("//p[@class = 'boxMainTitle' and text()='"+subModule+"']")).click();
		library.waitForDigitLoad();
		library.waitForGMCLoading();
	}
}
