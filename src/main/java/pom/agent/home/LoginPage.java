package pom.agent.home;

import java.util.HashMap;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.utilities.Library;
import com.utilities.PropertyFileReader;

public class LoginPage {

	@FindBy(id = "userName")
	private WebElement txtUserName;

	@FindBy(id = "password")
	private WebElement txtPassword;

	@FindBy(xpath = "//button[contains(text(),'Login')]")
	private WebElement btnLogin;
	
	@FindBy(xpath="//img[@alt='DigitPlus']/..")
	private WebElement btnHome;

	private com.utilities.Library library = new com.utilities.Library();
	private static int loginCount;
	
	public void enterDetails(String details) throws InterruptedException {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "username":
					txtUserName.clear();
					txtUserName.sendKeys(hmap.get(key));
					break;
				case "password":
					Thread.sleep(2000);
					txtPassword.clear();
					txtPassword.sendKeys(hmap.get(key));
					break;
				default:
					System.out.println("Implememntation for the field '" + key + "' is not found");
				}
			}
		} catch (Exception e) {
			while (loginCount++<3) {
				enterDetails(details);
			}
		}
		btnLogin.click();
	}



	public void enterLoginCredentials() throws Exception {
		try{
			System.out.println("Thread wait started");
			//Thread.sleep(25000);
			System.out.println("Thread wait ended");
		PropertyFileReader prob = new PropertyFileReader();
		//library.waitForVisibilityOfTheElement(txtUserName);
		library.waitForElementVisible(txtUserName, 60, 1000);
		//txtUserName.click();
		txtUserName.sendKeys(prob.getUserName());
		System.out.println("Entered username");
		//txtPassword.click();
		Thread.sleep(2000);
		txtPassword.sendKeys(prob.getPassword());
		System.out.println("Entered username");
		btnLogin.click();
		System.out.println("Login action is completed");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	
	public boolean moveToHomePage()
	{
		try {
		btnHome.click();
		return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}
}
