package pom.agent.cpm;

import com.utilities.Library;
import com.utilities.PropertyFileReader;
import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class CPMPage {
    // ------------------------------- Policy Details -------------------------------

    @FindBy(xpath = "//div[text()='New Equipment']")
    private WebElement newEquipmentRadio;

    @FindBy(xpath = "//div[text()='Renewals/Rollover']")
    private WebElement renewalsRolloverRadio;

    @FindBy(xpath = "//div[text()='Break in']")
    private WebElement breakInRadio;

    @FindBy(xpath = "//div[text()='Individual']")
    private WebElement individualRadio;

    @FindBy(xpath = "//div[text()='Corporate']")
    private WebElement corporateRadio;

    // ------------------------------- Machinery Details -------------------------------

    @FindBy(name = "machineType")
    private WebElement machineryTypeDropdown;

    @FindBy(name = "make")
    private WebElement makeTextField;

    @FindBy(name = "model")
    private WebElement modelTextField;

    @FindBy(name = "capacity")
    private WebElement capacityTextField;

    @FindBy(name = "serialNo")
    private WebElement serialNoTextField;

    @FindBy(name = "chassisNo")
    private WebElement chasisNoTextField;

    @FindBy(name = "machineAge")
    private WebElement ageOfMachineryTextField;

    @FindBy(name = "activity")
    private WebElement machineActivityDropdown;

    @FindBy(name = "isRegistered")
    private WebElement isRegisteredDropdown;

    @FindBy(name = "isPublic")
    private WebElement isUsedInPublicDropdown;

    @FindBy(name = "regNumber")
    private WebElement regNumberTextField;

    @FindBy(xpath = "//span[text()='Show Premium']")
    private WebElement showPremiumButton;

    // ------------------------------- Location of Machinery -------------------------------

    @FindBy(xpath = "//p[text()='Location of machinery']")
    private WebElement locationOfMachineryLabel;

    @FindBy(name = "com_address")
    private WebElement machineryAddressTextField;

    @FindBy(name = "com_pincode")
    private WebElement machineryPincodeTextField;

    @FindBy(name = "anywhere")
    private WebElement anywhereInIndiaCheckbox;

    // ------------------------------- Coverages -------------------------------

    @FindBy(xpath = "//p[text()='Coverages']")
    private WebElement coveragesLabel;

    @FindBy(name = "baseSI")
    private WebElement baseSumInsuredTextField;

    @FindBy(name = "escalation")
    private WebElement escalationPctTextField;

    @FindBy(xpath = "//input[@name='floater']")
    private WebElement floatCoverCheckbox;

    @FindBy(name = "terrorism")
    private WebElement terroristCheckbox;

    @FindBy(name = "tpsi")
    private WebElement thirdPartyLiabilityTextField;

    @FindBy(name = "ownersSI")
    private WebElement ownersSurrPropTextField;

    @FindBy(name = "clearanceSI")
    private WebElement clearanceRemovalOfDebrisTextField;

    @FindBy(name = "customDutySI")
    private WebElement additionalCustDutyTextField;

    @FindBy(name = "expressFreight")
    private WebElement expressFrieghtTextField;

    @FindBy(name = "airFreight")
    private WebElement airFreightTextField;

    // ------------------------------- Previous Policy Details -------------------------------

    @FindBy(xpath = "//p[text()='Previous Policy Details']")
    private WebElement prevPolicyDetailsLabel;

    @FindBy(name = "policy_expiryDate")
    private WebElement policyExpDateTextField;

    @FindBy(name = "previousInsurer")
    private WebElement selPrevInsurerDropdown;

    @FindBy(name = "previousPolicyNumber")
    private WebElement prevPolicyNoTextField;

    @FindBy(xpath = "//div[text()='Yes']")
    private WebElement prevClaimsYesRadio;

    @FindBy(xpath = "//div[text()='No']")
    private WebElement prevClaimsNoRadio;

    @FindBy(name = "claimAmount")
    private WebElement enterClaimAmtTextField;

    @FindBy(name = "claimDes")
    private WebElement claimDescTextField;

    @FindBy(name = "policy_startDate")
    private WebElement policyStartDateTextField;

    // ------------------------------- Premium Breakup -------------------------------

    @FindBy(xpath = "//p[text()='Premium Break Up']")
    private WebElement premiumBreakupLabel;

    @FindBy(name = "discount")
    private WebElement discountLoadingTextField;

    @FindBy(xpath = "//p[text()='Apply']")
    private WebElement applyButton;

    // ------------------------------- Upload Documents -------------------------------

    @FindBy(xpath = "//p[text()='Upload documents']")
    private WebElement uploadDocumentsLabel;

    @FindBy(id = "fileUpload")
    private WebElement chooseDocumentButton;

    @FindBy(xpath = "//span[text()='Select']")
    private WebElement selectDocTypeDropdown;

    @FindBy(xpath = "//i[text()='delete']")
    private List<WebElement> deleteDocButtons;

    // ------------------------------- Customer Details -------------------------------

    @FindBy(xpath = "//p[text()='Customer Details']")
    private WebElement custDetailsLabel;

    @FindBy(id = "firstName")
    private WebElement nameOfTheInsuredTextField;

    @FindBy(name = "mobileNumber")
    private WebElement mobileNoTextField;

    @FindBy(name = "emailId")
    private WebElement emailIdTextField;

    @FindBy(name = "gstNumber")
    private WebElement gstNumberTextField;

    @FindBy(xpath = "//input[@formcontrolname='address']")
    private WebElement custAddrTextField;

    @FindBy(xpath = "//input[@formcontrolname='pincode']")
    private WebElement custPincodeTextField;

    @FindBy(name = "hypothecation")
    private WebElement hypotecationTextField;

    @FindBy(xpath = "//div[starts-with(@class, 'footer')]/descendant::p[text()='Total Payable Amount']/following-sibling::p")
    private WebElement totalPayableAmtFooterLabel;

    @FindBy(xpath = "//button[contains(text(), 'Save Quote')]")
    private WebElement saveQuoteButton;

    // ------------------------------- Payment Details -------------------------------

    @FindBy(xpath = "//p[text()='Quote Number']/following::p[starts-with(text(), 'D')]")
    private WebElement quoteNoLabel;

    @FindBy(xpath = "//i[@title='Download quote']")
    private WebElement quoteDownloadButton;

    @FindBy(xpath = "//span[text()='Submit']")
    private WebElement submitButton;

    @FindBy(xpath = "//button[text()='Yes']")
    private WebElement finalSubmitOptionYes;

    @FindBy(xpath = "//button[text()='No']")
    private WebElement finalSubmitOptionNo;

    @FindBy(xpath = "//label[text()='Payment Mode']")
    private WebElement paymentModeLabel;

    @FindBy(xpath = "//label[text()='Payment Mode']/following::mat-select")
    private WebElement paymentModeDropdown;

    @FindBy(xpath = "//label[text()='Cheque Number']")
    private WebElement chequeNoLabel;

    @FindBy(xpath = "//input[@formcontrolname='chequeNumber']")
    private WebElement chequeNoTextField;

    @FindBy(xpath = "//input[@formcontrolname='IFSC']")
    private WebElement ifscTextField;

    @FindBy(xpath = "//span[text()='Proceed to pay']")
    private WebElement proceedToPayButton;

    @FindBy(xpath = "//b[contains(@id, 'Successfully-Issued')]")
    private WebElement successMessageLabel;

    // ------------------------------- Additional global variables -------------------------------

    private Library library = new Library();

    // ------------------------------- Action methods -------------------------------

    public void enterPolicyDetails(DataTable details) throws Exception {
        try {
            List<Map<String, String>> policyDetList = details.asMaps(String.class, String.class);

            Set<String> options = policyDetList.get(0).keySet();
            for (String option : options) {
                switch (option) {
                    case "policytype":
                        switch (policyDetList.get(0).get(option).toLowerCase()) {
                            case "newequipment":
                                newEquipmentRadio.click();
                                break;
                            case "renewals/rollover":
                                renewalsRolloverRadio.click();
                                break;
                            case "breakin":
                            case "break-in":
                                breakInRadio.click();
                                break;
                            default:
                                throw new Exception("There is no such policy type: [" + policyDetList.get(0).get(option) + "]");
                        }
                        break;
                    case "policyholder":
                        switch (policyDetList.get(0).get(option).toLowerCase()) {
                            case "individual":
                                individualRadio.click();
                                break;
                            case "corporate":
                                corporateRadio.click();
                                break;
                            default:
                                throw new Exception("There is no such policy holder: [" + policyDetList.get(0).get(option) + "]");
                        }
                        break;
                    default:
                        throw new Exception("There is no such field: [" + option + "]");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while entering the policy details: " + e.getMessage());
        }
    }

    public void enterMachineryDetails(DataTable details) throws Exception {
        try {
            List<Map<String, String>> machineryDetList = details.asMaps(String.class, String.class);

            Set<String> options = machineryDetList.get(0).keySet();
            for (String option : options) {
                switch (option) {
                    case "machinerytype":
                        machineryTypeDropdown.sendKeys(machineryDetList.get(0).get(option));
                        break;
                    case "make":
                        makeTextField.sendKeys(machineryDetList.get(0).get(option));
                        break;
                    case "model":
                        modelTextField.sendKeys(machineryDetList.get(0).get(option));
                        break;
                    case "capacity":
                        capacityTextField.sendKeys(machineryDetList.get(0).get(option));
                        break;
                    case "serialno":
                        // Adding the empty value check condition assuming in future for some case/flow the following fields may get disabled
                        if (!machineryDetList.get(0).get(option).equals(""))
                            serialNoTextField.sendKeys(machineryDetList.get(0).get(option));
                        break;
                    case "chasisno":
                        if (!machineryDetList.get(0).get(option).equals(""))
                            chasisNoTextField.sendKeys(machineryDetList.get(0).get(option));
                        break;
                    case "ageofmachinery":
                        ageOfMachineryTextField.sendKeys(machineryDetList.get(0).get(option));
                        break;
                    case "machineactivity":
                        machineActivityDropdown.sendKeys(machineryDetList.get(0).get(option));
                        break;
                    case "isregundermotorvehicleact":
                        isRegisteredDropdown.sendKeys(machineryDetList.get(0).get(option));
                        break;
                    case "isusedonpublicroad":
                        isUsedInPublicDropdown.sendKeys(machineryDetList.get(0).get(option));
                        break;
                    case "registrationnumber":
                        // Adding the empty value check condition because this field is disabled if the equipment is not reg under motor vehicle act
                        if (!machineryDetList.get(0).get(option).equals(""))
                            regNumberTextField.sendKeys(machineryDetList.get(0).get(option));
                        break;
                    default:
                        throw new Exception("There is no such field: [" + option + "]");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while entering the machinery details: " + e.getMessage());
        }
    }

    public void enterLocOfMachineryDetails(DataTable details) throws Exception {
        try {
            library.focus(library.getDriver(), locationOfMachineryLabel);
            List<Map<String, String>> locOfMachineryDetList = details.asMaps(String.class, String.class);

            Set<String> options = locOfMachineryDetList.get(0).keySet();
            for (String option : options) {
                switch (option) {
                    case "fulladdress":
                        // Adding the empty value check condition assuming in future for some case/flow the following fields may get disabled
                        if (!locOfMachineryDetList.get(0).get(option).equals(""))
                            machineryAddressTextField.sendKeys(locOfMachineryDetList.get(0).get(option));
                        break;
                    case "pincode":
                        if (!locOfMachineryDetList.get(0).get(option).equals(""))
                            machineryPincodeTextField.sendKeys(locOfMachineryDetList.get(0).get(option));
                        break;
                    case "anywhereinindia":
                        if (locOfMachineryDetList.get(0).get(option).toLowerCase().equals("true"))
                            anywhereInIndiaCheckbox.click();
                        break;
                    default:
                        throw new Exception("There is no such field: [" + option + "]");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while entering the location of machinery details: " + e.getMessage());
        }
    }

    public void enterCoveragesDetails(DataTable details) throws Exception {
        try {
            library.focus(library.getDriver(), coveragesLabel);
            List<Map<String, String>> coveragesDetList = details.asMaps(String.class, String.class);

            Set<String> options = coveragesDetList.get(0).keySet();
            for (String option : options) {
                switch (option) {
                    case "basesuminsured":
                        // Adding the empty value check condition assuming in future for some case/flow the following fields may get disabled
                        if (!coveragesDetList.get(0).get(option).equals(""))
                            baseSumInsuredTextField.sendKeys(coveragesDetList.get(0).get(option));
                        break;
                    case "escalation":
                        if (!coveragesDetList.get(0).get(option).equals(""))
                            escalationPctTextField.sendKeys(coveragesDetList.get(0).get(option));
                        break;
                    case "floatercover":
                        if (floatCoverCheckbox.getAttribute("aria-checked").equals("false"))
                            floatCoverCheckbox.click();
                        break;
                    case "terrorism":
                        terroristCheckbox.click();
                        break;
                    case "thirdpartyliability":
                        if (!coveragesDetList.get(0).get(option).equals(""))
                            thirdPartyLiabilityTextField.sendKeys(coveragesDetList.get(0).get(option));
                        break;
                    case "ownerssurroundingproperty":
                        if (!coveragesDetList.get(0).get(option).equals(""))
                            ownersSurrPropTextField.sendKeys(coveragesDetList.get(0).get(option));
                        break;
                    case "clearanceandremovalofdebris":
                        if (!coveragesDetList.get(0).get(option).equals(""))
                            clearanceRemovalOfDebrisTextField.sendKeys(coveragesDetList.get(0).get(option));
                        break;
                    case "additionalcustomduty":
                        if (!coveragesDetList.get(0).get(option).equals(""))
                            additionalCustDutyTextField.sendKeys(coveragesDetList.get(0).get(option));
                        break;
                    case "expressfreight":
                        if (!coveragesDetList.get(0).get(option).equals(""))
                            expressFrieghtTextField.sendKeys(coveragesDetList.get(0).get(option));
                        break;
                    case "airfreight":
                        if (!coveragesDetList.get(0).get(option).equals(""))
                            airFreightTextField.sendKeys(coveragesDetList.get(0).get(option));
                        break;
                    default:
                        throw new Exception("There is no such field: [" + option + "]");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while entering the coverages details: " + e.getMessage());
        }
    }

    public void enterPrevPolicyDetails(DataTable details) throws Exception {
        try {
            library.focus(library.getDriver(), prevPolicyDetailsLabel);
            List<Map<String, String>> prevPolicyDetList = details.asMaps(String.class, String.class);

            Set<String> options = prevPolicyDetList.get(0).keySet();
            for (String option : options) {
                switch (option) {
                    case "policyexpirydate":
                        // Adding the empty value check condition because the following fields are hidden for new equipments
                        if (!prevPolicyDetList.get(0).get(option).equals(""))
                            policyExpDateTextField.sendKeys(prevPolicyDetList.get(0).get(option));
                        break;
                    case "previousinsurer":
                        if (!prevPolicyDetList.get(0).get(option).equals(""))
                            selPrevInsurerDropdown.sendKeys(prevPolicyDetList.get(0).get(option));
                        break;
                    case "previouspolicynumber":
                        if (!prevPolicyDetList.get(0).get(option).equals(""))
                            prevPolicyNoTextField.sendKeys(prevPolicyDetList.get(0).get(option));
                        break;
                    case "previousclaimhistory":
                        if (!prevPolicyDetList.get(0).get(option).equals("")) {
                            switch (prevPolicyDetList.get(0).get(option).toLowerCase()) {
                                case "yes":
                                    prevClaimsYesRadio.click();
                                    break;
                                case "no":
                                    prevClaimsNoRadio.click();
                                    break;
                                default:
                                    throw new Exception("Previous claim history can be either 'Yes' or 'No', given: [" + prevPolicyDetList.get(0).get(option) + "]");
                            }
                        }
                        break;
                    case "claimamount":
                        if (!prevPolicyDetList.get(0).get(option).equals(""))
                            enterClaimAmtTextField.sendKeys(prevPolicyDetList.get(0).get(option));
                        break;
                    case "claimdescription":
                        if (!prevPolicyDetList.get(0).get(option).equals(""))
                            claimDescTextField.sendKeys(prevPolicyDetList.get(0).get(option));
                        break;
                    default:
                        throw new Exception("There is no such field: [" + option + "]");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while entering the previous policy details: " + e.getMessage());
        }
    }

    public void performAction(String action) throws Exception {
        try {
            switch (action.replaceAll(" ", "").toLowerCase()) {
                case "showpremium":
                    showPremiumButton.click();
                    Thread.sleep(3000);
                    library.waitForDigitLoad();
                    break;
                case "apply":
                    applyButton.click();
                    Thread.sleep(2000);
                    library.waitForDigitLoad();
                    break;
                case "savequote":
                    saveQuoteButton.click();
                    Thread.sleep(2000);
                    library.waitForDigitLoad();
                    Library.staticMap.put("Policy Number", quoteNoLabel.getText());
                    break;
                case "proceedtopay":
                    proceedToPayButton.click();
                    Thread.sleep(2000);
                    library.waitForDigitLoad();
                    break;
                case "submit":
                    submitButton.click();
                    break;
                default:
                    throw new Exception("Option to perform the action [" + action + "] is unavailable");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while performing action: [" + action + "]" + e.getMessage());
        }
    }

    public void enterDiscountLoadingDetails(DataTable details) throws Exception {
        try {
            library.focus(library.getDriver(), premiumBreakupLabel);
            List<Map<String, String>> disLoadDetList = details.asMaps(String.class, String.class);

            Set<String> options = disLoadDetList.get(0).keySet();
            for (String option : options) {
                switch (option) {
                    case "discountloading":
                        // Adding the empty value check condition assuming in future for some case/flow the following fields may get disabled
                        if (!disLoadDetList.get(0).get(option).equals(""))
                            discountLoadingTextField.clear();
                            discountLoadingTextField.sendKeys(disLoadDetList.get(0).get(option));
                        break;
                    default:
                        throw new Exception("There is no such field: [" + option + "]");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while entering the discount/loading details: " + e.getMessage());
        }
    }

    public void enterDocumentDetails(DataTable details) throws Exception {
        try {
            library.focus(library.getDriver(), uploadDocumentsLabel);
            Map<String, String> docDetMap = details.asMap(String.class, String.class);

            // Delete the pre-existing document upload fields
            for (WebElement button : deleteDocButtons)
                button.click();

            Set<String> options = docDetMap.keySet();
            for (String option : options) {
                switch (option) {
                    case "documentname1":
                    case "documentname2":
                    case "documentname3":
                        chooseDocumentButton.sendKeys(System.getProperty("user.dir") + docDetMap.get(option));
                        break;
                    case "documenttype1":
                    case "documenttype2":
                    case "documenttype3":
                        selectDocTypeDropdown.click();
                        Thread.sleep(1000);
                        library.getDriver()
                                .findElement(By.xpath("//mat-option/span[contains(text(), '" + docDetMap.get(option) + "')]"))
                                .click();
                        break;
                    default:
                        throw new Exception("There is no such field: [" + option + "]");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while entering the discount/loading details: " + e.getMessage());
        }
    }

    public void enterCustomerDetails(DataTable details) throws Exception {
        try {
            library.focus(library.getDriver(), custDetailsLabel);
            List<Map<String, String>> custDetList = details.asMaps(String.class, String.class);

            Set<String> options = custDetList.get(0).keySet();
            for (String option : options) {
                switch (option) {
                    case "nameoftheinsured":
                        nameOfTheInsuredTextField.sendKeys(custDetList.get(0).get(option));
                        break;
                    case "mobilenumber":
                    case "mobileno":
                        mobileNoTextField.sendKeys(custDetList.get(0).get(option));
                        break;
                    case "emailid":
                        emailIdTextField.sendKeys(custDetList.get(0).get(option));
                        break;
                    case "gstnumber":
                        if (!custDetList.get(0).get(option).equals(""))
                            gstNumberTextField.sendKeys(custDetList.get(0).get(option));
                        break;
                    case "fulladdress":
                        custAddrTextField.sendKeys(custDetList.get(0).get(option));
                        break;
                    case "pincode":
                        custPincodeTextField.sendKeys(custDetList.get(0).get(option));
                        break;
                    case "hypothecatedto":
                        if (!custDetList.get(0).get(option).equals(""))
                            hypotecationTextField.sendKeys(custDetList.get(0).get(option));
                        break;
                    default:
                        throw new Exception("There is no such field: [" + option + "]");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while entering the customer details: " + e.getMessage());
        }
    }

    public boolean visibilityOfQuoteNo() {
        return quoteNoLabel.isDisplayed();
    }

    public boolean visibilityOfProposalDownload() {
        return quoteDownloadButton.isDisplayed();
    }

    public void clickOnYesForFinalUpdate() throws Exception {
        finalSubmitOptionYes.click();
        Thread.sleep(3000);
        library.waitForDigitLoad();
    }

    public void clickOnNoForFinalUpdate() throws Exception {
        finalSubmitOptionNo.click();
        Thread.sleep(500);
        library.waitForDigitLoad();
    }

    public void enterPaymentDetails(DataTable details) throws Exception {
        try {
            library.focus(library.getDriver(), paymentModeLabel);
            List<Map<String, String>> paymentDetList = details.asMaps(String.class, String.class);

            Set<String> options = paymentDetList.get(0).keySet();
            for (String option : options) {
                switch (option) {
                    case "paymentmode":
                        String mode = paymentDetList.get(0).get(option);

                        String environment;
                        if (System.getProperty("environment") == null || System.getProperty("environment").equals("")) {
                            PropertyFileReader prop = new PropertyFileReader();
                            environment = prop.getEnvironment();
                        } else
                            environment = System.getProperty("environment");
                        if (environment.equalsIgnoreCase("prod") || environment.equalsIgnoreCase("plus"))
                            mode = "Generate Payment Link";

                        switch (mode.replaceAll(" ", "").toLowerCase()) {
                            case "agentfloat":
                                paymentModeDropdown.click();
                                Thread.sleep(1000);
                                library.getDriver()
                                        .findElement(By.xpath("//div[@class='cdk-overlay-container']/descendant::span[contains(text(), 'Agent Float')]"))
                                        .click();
                                break;
                            case "generatepaymentlink":
                                paymentModeDropdown.click();
                                Thread.sleep(1000);
                                library.getDriver()
                                        .findElement(By.xpath("//div[@class='cdk-overlay-container']/descendant::span[contains(text(), 'Generate Payment Link')]"))
                                        .click();
                                break;
                            case "sendpaymentlink":
                                paymentModeDropdown.click();
                                Thread.sleep(1000);
                                library.getDriver().findElement(By.xpath("//div[@class='cdk-overlay-container']/descendant::span[contains(text(), 'Send Payment Link')]"))
                                        .click();
                                break;
                            case "paymentbycustomercheque":
                                paymentModeDropdown.click();
                                Thread.sleep(1000);
                                library.getDriver()
                                        .findElement(By.xpath("//div[@class='cdk-overlay-container']/descendant::span[contains(text(), 'Payment by Customer Cheque')]"))
                                        .click();
                                library.focus(library.getDriver(), chequeNoLabel);
                                chequeNoTextField.sendKeys(library.getRandomValue("[N | 6]"));
                                ifscTextField.sendKeys("560269002");
                                ifscTextField.sendKeys(Keys.TAB);
                                break;
                            case "paymentbypartnercheque":
                                paymentModeDropdown.click();
                                Thread.sleep(1000);
                                library.getDriver()
                                        .findElement(By.xpath("//div[@class='cdk-overlay-container']/descendant::span[contains(text(), 'Payment by Partner Cheque')]"))
                                        .click();
                                library.focus(library.getDriver(), chequeNoLabel);
                                chequeNoTextField.sendKeys(library.getRandomValue("[N | 6]"));
                                ifscTextField.sendKeys("560269002");
                                ifscTextField.sendKeys(Keys.TAB);
                                break;
                            default:
                                throw new Exception("The payment mode " + mode + " is not configured for automation");
                        }
                        break;
                    default:
                        throw new Exception("There is no such field: [" + option + "]");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error while entering the payment details: " + e.getMessage());
        }
    }

    public boolean verifySuccessPage() {
        return successMessageLabel.isDisplayed();
    }

    public int getPremium() throws Exception {
        if (totalPayableAmtFooterLabel.isDisplayed())
            return library.strToNum(totalPayableAmtFooterLabel.getText());
        else
            throw new Exception("Error in retrieving the premium amount, field is not visible");
    }
}