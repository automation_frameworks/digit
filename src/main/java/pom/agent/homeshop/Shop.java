package pom.agent.homeshop;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.WebDriverEventListener;

import com.utilities.Library;
import com.utilities.PropertyFileReader;

public class Shop {

	@FindBy(xpath = "//mat-radio-button[@ng-reflect-value='INDIVIDUAL']")
	private WebElement radIndividual;

	@FindBy(xpath = "//mat-radio-button[@ng-reflect-value='COMPANY']")
	private WebElement radCorporate;

	@FindBy(xpath = "//label[text()='Shop Category']/following-sibling::input")
	private WebElement drpShopCategory;

	@FindBy(xpath = "//mat-select[@formcontrolname='propertyAge']")
	private WebElement drpAgeOfProperty;

	@FindBy(xpath = "//input[@formcontrolname='address']")
	private WebElement txtAddress;

	@FindBy(name = "pincode_1")
	private WebElement txtPincode;

	@FindBy(xpath = "//label[text()='Pincode']/following-sibling::input")
	private WebElement drpPinCode;

	@FindBy(xpath = "//label[contains(text(),'Square Feet Area')]/following-sibling::input")
	private WebElement txtSquareFeetArea;

	@FindBy(xpath = "//label[text()='Hypothecated to']/following-sibling::input")
	private WebElement txtHypothecatedTo;

	@FindBy(xpath = "//label[text()='Floor Number']/following-sibling::input")
	private WebElement txtFloorNumber;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='ownership']/mat-radio-button[@value='O']")
	private WebElement radOwned;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='ownership']/mat-radio-button[@value='R']")
	private WebElement radRented;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='constructionType']/mat-radio-button[@value='P']")
	private WebElement radPucca;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='constructionType']/mat-radio-button[@value='K']")
	private WebElement radKutcha;

	@FindBy(xpath = "//mat-radio-button[@name='reinstatement']")
	private WebElement radReinstatementValueClause;

	@FindBy(xpath = "//mat-radio-button[@name='marketvalue']")
	private WebElement radMarketValueClause;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='basement']/mat-radio-button[@value='J']")
	private WebElement radContentsStoredInBasementYes;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='basement']/mat-radio-button[@value='P']")
	private WebElement radContentsStoredInBasementonlyParking;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='basement']/mat-radio-button[@value='N']")
	private WebElement radContentsStoredInBasementNo;

	@FindBy(id = "buildingSI")
	private WebElement chkBuildingSumInsured;

	@FindBy(xpath = "//p[contains(text(),'Enter Custom')]")
	private WebElement btnEnterCustom;

	@FindBy(xpath = "//label[text()='Enter (Sq.ft.) rate']/following-sibling::input")
	private WebElement txtEnterCustomSqrFeet;

	@FindBy(xpath = "//input[@ng-reflect-name='otherSI']")
	private WebElement txtContent;

	@FindBy(name = "Terrorism")
	private WebElement chkTerrorism;

	@FindBy(xpath = "//span[contains(text(), 'Burglary')]")
	private WebElement chkBurglary;

	@FindBy(xpath = "//label[text()='Cash in Till/Counter']/following-sibling::input")
	private WebElement txtCashInTillCounter;

	@FindBy(name = "Asset_Care")
	private WebElement chkAssetCare;

	@FindBy(name = "policyStartDate")
	private WebElement txtPolicyStartDate;

	@FindBy(name = "glasscover_1")
	private WebElement txtPlateGlassCover;

	@FindBy(name = "neoncover_1")
	private WebElement txtNeonSignCover;

	@FindBy(name = "Employee_Compensation")
	private WebElement chkEmployeeCompensation;

	@FindBy(name = "employeeCount_1")
	private WebElement txtNoOfEmployees;

	@FindBy(name = "Monthly_Wage1")
	private WebElement txtTotalMonthlySalary;

	@FindBy(xpath = "//button[span[contains(text(),'Show Premium')]]")
	private WebElement btnShowPremium;

	@FindBy(xpath = "//button[contains(text(),'Save Quote')]")
	private WebElement btnSaveQuote;

	@FindBy(xpath = "//p[text()='Quote Number']/following::p")
	private WebElement lblQuoteNumber;

	@FindBy(xpath = "//button[span[contains(text(),'Proceed to Pay')]]")
	private WebElement btnProceedToPay;

	@FindBy(xpath = "//p[text()='Customer Details']")
	private WebElement custDetailsLabel;

	@FindBy(id = "firstName")
	private WebElement txtInsuredName;

	@FindBy(id = "lastName")
	private WebElement txtLastName;

	@FindBy(name = "Pincode")
	private WebElement txtCustomerPinCode;

	@FindBy(name = "Address")
	private WebElement txtCustomerAddress;

	@FindBy(name = "mobile_number")
	private WebElement txtMobileNumber;

	@FindBy(name = "email")
	private WebElement txtEmailID;

	@FindBy(name = "GSTIN")
	private WebElement txtGSTIN;

	@FindBy(xpath = "//label[text()='Payment Mode']")
	private WebElement paymentModeLabel;

	@FindBy(xpath = "//mat-select[@formcontrolname='selectedPaymentMethod']")
	private WebElement drpPaymentMode;

	@FindBy(xpath = "//input[@formcontrolname='chequeNumber']")
	private WebElement txtChequeNumber;

	@FindBy(xpath = "//input[@formcontrolname='IFSC']")
	private WebElement txtIFSC;

	@FindBy(id = "Successfully-Issued")
	private WebElement lblSuccessMessage;

	com.utilities.Library library = new com.utilities.Library();

	public void selectOwnershipType(String ownership) throws Exception {
		try {
			library.waitForDigitLoad();
			switch (ownership.replaceAll(" ", "").toLowerCase()) {
			case "individual":
				radIndividual.click();
				break;
			case "corporate":
				radCorporate.click();
				break;
			default:
				throw new Exception("There is no such ownership [" + ownership + "]");
			}
		} catch (Exception e) {
			throw new Exception("Error while selecting the ownership type : " + e.getMessage());
		}
	}

	public void enterShopDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "shopcategory":
					drpShopCategory.sendKeys(hmap.get(key));
					drpShopCategory.sendKeys(Keys.TAB);
					break;
				case "ageofproperty":
					drpAgeOfProperty.click();
					Thread.sleep(1000);
					library.getDriver()
							.findElement(
									By.xpath("//mat-option//span[normalize-space()='" + hmap.get(key).trim() + "']"))
							.click();
					txtPincode.click();
					break;
				case "addressofrisklocation":
					txtAddress.sendKeys(hmap.get(key));
					Thread.sleep(1000);
					txtAddress.sendKeys(Keys.ARROW_DOWN, Keys.TAB);
					break;
				case "pincode":
					txtPincode.sendKeys(hmap.get(key));
					break;
				case "squarefeetarea":
					txtSquareFeetArea.sendKeys(hmap.get(key));
					break;
				case "hypothecatedto":
					txtHypothecatedTo.sendKeys(hmap.get(key));
					break;
				case "floornumber":
					txtFloorNumber.sendKeys(hmap.get(key));
					break;
				case "ownership":
					switch (hmap.get(key).toLowerCase()) {
					case "owned":
						radOwned.click();
						break;
					case "rented":
						radRented.click();
						break;
					default:
						throw new Exception("There is no such ownership : [" + hmap.get(key) + "]");
					}
					break;
				case "typeofconstruction":
					switch (hmap.get(key).toLowerCase()) {
					case "pucca":
						radPucca.click();
						break;
					case "kutcha":
						radKutcha.click();
						break;
					default:
						throw new Exception("There is no such construction type : [" + hmap.get(key) + "]");
					}
					break;
				case "baseforsuminsuredvaluation":
					switch (hmap.get(key).toLowerCase()) {
					case "reinstatementvalueclause":
						radReinstatementValueClause.click();
						break;
					case "marketvalueclause":
						radMarketValueClause.click();
						break;
					default:
						throw new Exception("There is no such base for Sum Insured Value : [" + hmap.get(key) + "]");
					}
					break;
				default:
					throw new Exception("There is no such field : [" + key + "]");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the Shop details : " + e.getMessage());
		}
	}

	public void enterSFSPDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			try {
				txtPolicyStartDate.click();
			} catch (Exception ie) {
			}
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "content":
				case "contentstock":
					txtContent.sendKeys(hmap.get(key));
					break;
				case "terrorism":
					switch (hmap.get(key).toLowerCase()) {
					case "yes":
						chkTerrorism.click();
						break;
					}
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the SFSP details : " + e.getMessage());
		}
	}

	public void enterBurglaryDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			try {
				txtPolicyStartDate.click();
			} catch (Exception ie) {
			}
			chkBurglary.click();
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "cashintill/counter":
					txtCashInTillCounter.clear();
					txtCashInTillCounter.sendKeys(hmap.get(key));
					break;
				default:
					throw new Exception("There is no such field [" + key + "]");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the burglary details : " + e.getMessage());
		}
	}

	public void enterAssetCareDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			System.out.println(hmap);
			try {
				txtPolicyStartDate.click();
			} catch (Exception ie) {
			}
			chkAssetCare.click();
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "plateglasscover":
					txtPlateGlassCover.sendKeys(hmap.get(key));
					break;
				case "neonsigncover":
					txtNeonSignCover.sendKeys(hmap.get(key));
					break;
				default:
					if (key.replaceAll(" ", "").toLowerCase().matches("equipment[0-5]")) {
						library.getDriver()
								.findElement(By.xpath("//label[text()='Equipment 1']/following-sibling::mat-select"))
								.click();
						library.getDriver()
								.findElement(By
										.xpath("//mat-option//span[normalize-space()='" + hmap.get(key).trim() + "']"))
								.click();
					} else if (key.replaceAll(" ", "").toLowerCase().matches("serialnumber[0-5]")) {
						library.getDriver()
								.findElement(By.xpath("//label[text()='Equipment 1']/../following-sibling::div/input"))
								.sendKeys(hmap.get(key));
					} else if (key.replaceAll(" ", "").toLowerCase().matches("age[0-5]")) {
						library.getDriver()
								.findElement(
										By.xpath("//label[text()='Equipment 1']/../following-sibling::div/mat-select"))
								.sendKeys(hmap.get(key));
					} else
						throw new Exception("There is no such field [" + key + "]");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the asset care details : " + e.getMessage());
		}
	}

	public void enterEmployeeCompensationDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			try {
				txtPolicyStartDate.click();
			} catch (Exception ie) {
			}
			chkEmployeeCompensation.click();
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "no.ofemployees":
				case "noofemployees":
					txtNoOfEmployees.sendKeys(hmap.get(key));
					break;
				case "totalmonthlysalary":
					txtTotalMonthlySalary.sendKeys(hmap.get(key));
					break;
				default:
					throw new Exception("There is no such field [" + key + "]");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the employee compensation details : " + e.getMessage());
		}
	}

	public void performAction(String action) throws Exception {
		try {
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "showpremium":
				btnShowPremium.click();
				Thread.sleep(2000);
				library.waitForDigitLoad();
				break;
			case "savequote":
				btnSaveQuote.click();
				Thread.sleep(2000);
				library.waitForDigitLoad();
				Library.staticMap.put("Policy Number", lblQuoteNumber.getText());
				break;
			case "proceedtopay":
				library.waitForDigitLoad();
				btnProceedToPay.click();
				Thread.sleep(2000);
				library.waitForDigitLoad();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while performing action : [" + action + "]" + e.getMessage());
		}
	}

	public void enterCustomerDetails(String details) throws Exception {
		try {
			library.focus(library.getDriver(), custDetailsLabel);

			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "insuredname":
				case "firstname":
					txtInsuredName.sendKeys(hmap.get(key));
					break;
				case "lastname":
					txtLastName.sendKeys(hmap.get(key));
					break;
				case "pincode":
					txtCustomerPinCode.sendKeys(hmap.get(key));
					break;
				case "address":
					txtCustomerAddress.sendKeys(hmap.get(key));
					break;
				case "mobilenumber":
					txtMobileNumber.sendKeys(hmap.get(key));
					break;
				case "emailid":
					txtEmailID.sendKeys(hmap.get(key));
					break;
				default:
					throw new Exception("There is no such field [" + key + "]");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the employee compensation details : " + e.getMessage());
		}
	}

	public void enterPaymentDetails(String paymentMode) throws Exception {
		try {
			library.focus(library.getDriver(), paymentModeLabel);

			String environment;
			if (System.getProperty("environment") == null || System.getProperty("environment").equals("")) {
				PropertyFileReader prop = new PropertyFileReader();
				environment = prop.getEnvironment().toString();
			} else
				environment = System.getProperty("environment");
			if (environment.equalsIgnoreCase("prod"))
				paymentMode = "Generate Payment Link";
			switch (paymentMode.replaceAll(" ", "").toLowerCase()) {
			case "agentfloat":
				drpPaymentMode.click();
				Thread.sleep(1000);
				library.getDriver().findElement(By.xpath("//mat-option//span[normalize-space()='Agent Float']"))
						.click();
				break;
			case "paymentbycustomercheque":
				drpPaymentMode.click();
				Thread.sleep(1000);
				library.getDriver()
						.findElement(By.xpath("//mat-option//span[normalize-space()='Payment by Customer Cheque']"))
						.click();
				txtChequeNumber.sendKeys(library.getRandomValue("[N | 6]"));
				txtIFSC.sendKeys("560269002");
				txtIFSC.sendKeys(Keys.TAB);
				break;
			case "paymentbypartnercheque":
				drpPaymentMode.click();
				Thread.sleep(1000);
				library.getDriver()
						.findElement(By.xpath("//mat-option//span[normalize-space()='Payment by Partner Cheque']"))
						.click();
				txtChequeNumber.sendKeys(library.getRandomValue("[N | 6]"));
				txtIFSC.sendKeys("560269002");
				txtIFSC.sendKeys(Keys.TAB);
				break;
			case "sendpaymentlink":
				drpPaymentMode.click();
				Thread.sleep(1000);
				library.getDriver().findElement(By.xpath("//mat-option//span[normalize-space()='Send Payment Link']"))
						.click();
				break;
			case "generatepaymentlink":
				drpPaymentMode.click();
				Thread.sleep(1000);
				library.getDriver()
						.findElement(By.xpath("//mat-option//span[normalize-space()='Generate Payment Link']")).click();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the payment details : " + e.getMessage());
		}
	}

	public void verifySuccessPage() throws Exception {
		try {
			if (!lblSuccessMessage.isDisplayed()) {
				throw new Exception("Success message is not displayed");
			}
		} catch (Exception e) {
			throw new Exception("Error while verifying the success page : " + e.getMessage());
		}
	}
}
