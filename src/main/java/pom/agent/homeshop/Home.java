package pom.agent.homeshop;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.utilities.Library;
import com.utilities.PropertyFileReader;

public class Home {

	@FindBy(xpath = "//mat-radio-button[@ng-reflect-value='INDIVIDUAL']")
	private WebElement radIndividual;

	@FindBy(xpath = "//mat-radio-button[@ng-reflect-value='COMPANY']")
	private WebElement radCorporate;

	@FindBy(xpath = "//mat-select[@formcontrolname='propertyAge']")
	private WebElement drpAgeOfProperty;

	@FindBy(xpath = "//input[@formcontrolname='address']")
	private WebElement txtAddress;

	@FindBy(name = "pincode_1")
	private WebElement txtPincode;

	@FindBy(xpath = "//label[text()='Pincode']/following-sibling::input")
	private WebElement drpPinCode;

	@FindBy(xpath = "//label[contains(text(),'Square Feet Area')]/following-sibling::input")
	private WebElement txtSquareFeetArea;

	@FindBy(xpath = "//label[text()='Hypothecated to']/following-sibling::input")
	private WebElement txtHypothecatedTo;

	@FindBy(xpath = "//label[text()='Floor Number']/following-sibling::input")
	private WebElement txtFloorNumber;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='ownership']/mat-radio-button[@value='O']")
	private WebElement radOwned;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='ownership']/mat-radio-button[@value='R']")
	private WebElement radRented;

	@FindBy(xpath = "//mat-radio-button[@name='reinstatement']")
	private WebElement radReinstatementValueClause;

	@FindBy(xpath = "//mat-radio-button[@name='marketvalue']")
	private WebElement radMarketValueClause;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='basement']/mat-radio-button[@value='J']")
	private WebElement radContentsStoredInBasementYes;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='basement']/mat-radio-button[@value='P']")
	private WebElement radContentsStoredInBasementonlyParking;

	@FindBy(xpath = "//mat-radio-group[@formcontrolname='basement']/mat-radio-button[@value='N']")
	private WebElement radContentsStoredInBasementNo;

	@FindBy(xpath = "//input[@ng-reflect-name='otherSI']")
	private WebElement txtContent;

	@FindBy(name = "Terrorism")
	private WebElement chkTerrorism;

	@FindBy(xpath = "//span[contains(text(), 'Burglary')]")
	private WebElement chkBurglary;

	@FindBy(xpath = "//label[text()='Cash in Till/Counter']/following-sibling::input")
	private WebElement txtCashInTillCounter;

	@FindBy(name = "Asset_Care")
	private WebElement chkAssetCare;

	@FindBy(name = "Pedal_Cycle")
	private WebElement chkPedalCycle;

	@FindBy(name = "framenumber_1")
	private WebElement txtFrameNumber;

	@FindBy(name = "cycleMake1")
	private WebElement txtCycleMake;

	@FindBy(name = "cycleModel1")
	private WebElement txtCycleModel;

	@FindBy(name = "cycleSI_1")
	private WebElement txtCycleSumInsured;

	@FindBy(name = "policyStartDate")
	private WebElement txtPolicyStartDate;

	@FindBy(xpath = "//button[span[contains(text(),'Show Premium')]]")
	private WebElement btnShowPremium;

	@FindBy(xpath = "//button[contains(text(),'Save Quote')]")
	private WebElement btnSaveQuote;

	@FindBy(xpath = "//p[text()='Quote Number']/../div/p")
	private WebElement lblQuoteNumber;

	@FindBy(xpath = "//button[span[contains(text(),'Proceed to Pay')]]")
	private WebElement btnProceedToPay;

	@FindBy(id = "firstName")
	private WebElement txtInsuredName;

	@FindBy(name = "Pincode")
	private WebElement txtCustomerPinCode;

	@FindBy(name = "Address")
	private WebElement txtCustomerAddress;

	@FindBy(name = "mobile_number")
	private WebElement txtMobileNumber;

	@FindBy(name = "email")
	private WebElement txtEmailID;

	@FindBy(name = "GSTIN")
	private WebElement txtGSTIN;

	@FindBy(xpath = "//mat-select[@formcontrolname='selectedPaymentMethod']")
	private WebElement drpPaymentMode;

	@FindBy(xpath = "//input[@formcontrolname='chequeNumber']")
	private WebElement txtChequeNumber;

	@FindBy(xpath = "//input[@formcontrolname='IFSC']")
	private WebElement txtIFSC;

	@FindBy(id = "Successfully-Issued")
	private WebElement lblSuccessMessage;

	com.utilities.Library library = new com.utilities.Library();

	public void selectOwnershipType(String ownership) throws Exception {
		try {
			library.waitForDigitLoad();
			switch (ownership.replaceAll(" ", "").toLowerCase()) {
			case "individual":
				radIndividual.click();
				break;
			case "corporate":
				radCorporate.click();
				break;
			default:
				throw new Exception("There is no such ownership [" + ownership + "]");
			}
		} catch (Exception e) {
			throw new Exception("Error while selecting the ownership type : " + e.getMessage());
		}
	}

	public void enterHomeDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "ageofproperty":
					drpAgeOfProperty.click();
					Thread.sleep(1000);
					library.getDriver()
							.findElement(
									By.xpath("//mat-option//span[normalize-space()='" + hmap.get(key).trim() + "']"))
							.click();
					txtPincode.click();
					break;
				case "addressofrisklocation":
					txtAddress.sendKeys(hmap.get(key));
					txtAddress.sendKeys(Keys.SPACE);
					Thread.sleep(4000);
					txtAddress.sendKeys(Keys.ARROW_DOWN, Keys.TAB);
					break;
				case "pincode":
					txtPincode.clear();
					txtPincode.sendKeys(hmap.get(key));
					break;
				case "squarefeetarea":
					txtSquareFeetArea.sendKeys(hmap.get(key));
					break;
				case "hypothecatedto":
					txtHypothecatedTo.sendKeys(hmap.get(key));
					break;
				case "ownership":
					switch (hmap.get(key).toLowerCase()) {
					case "owned":
						radOwned.click();
						break;
					case "rented":
						radRented.click();
						break;
					default:
						throw new Exception("There is no such ownership : [" + hmap.get(key) + "]");
					}
					break;
				case "baseforsuminsuredvaluation":
					switch (hmap.get(key).toLowerCase()) {
					case "reinstatementvalueclause":
						radReinstatementValueClause.click();
						break;
					case "marketvalueclause":
						radMarketValueClause.click();
						break;
					default:
						throw new Exception("There is no such base for Sum Insured Value : [" + hmap.get(key) + "]");
					}
					break;
				default:
					throw new Exception("There is no such field : [" + key + "]");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the Shop details : " + e.getMessage());
		}
	}

	public void enterSFSPDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			try {
				txtPolicyStartDate.click();
			} catch (Exception ie) {
			}
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "content":
				case "contentstock":
					txtContent.sendKeys(hmap.get(key));
					break;
				case "terrorism":
					switch (hmap.get(key).toLowerCase()) {
					case "yes":
						chkTerrorism.click();
						break;
					}
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the SFSP details : " + e.getMessage());
		}
	}

	public void enterBurglaryDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			try {
				txtPolicyStartDate.click();
			} catch (Exception ie) {
			}
			chkBurglary.click();
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "cashintill/counter":
					txtCashInTillCounter.clear();
					txtCashInTillCounter.sendKeys(hmap.get(key));
					break;
				default:
					throw new Exception("There is no such field [" + key + "]");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the burglary details : " + e.getMessage());
		}
	}

	public void enterAssetCareDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			System.out.println(hmap);
			try {
				txtPolicyStartDate.click();
			} catch (Exception ie) {
			}
			chkAssetCare.click();
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "framenumber":
					if (!chkPedalCycle.getAttribute("class").contains("checked"))
						chkPedalCycle.click();
					txtFrameNumber.sendKeys(hmap.get(key));
					break;
				case "cyclemake":
					if (!chkPedalCycle.getAttribute("class").contains("checked"))
						chkPedalCycle.click();
					txtCycleMake.sendKeys(hmap.get(key));
					break;
				case "cyclemodel":
					if (!chkPedalCycle.getAttribute("class").contains("checked"))
						chkPedalCycle.click();
					txtCycleModel.sendKeys(hmap.get(key));
					break;
				case "cyclesuminsured":
				case "suminsured":
					if (!chkPedalCycle.getAttribute("class").contains("checked"))
						chkPedalCycle.click();
					txtCycleSumInsured.sendKeys(hmap.get(key));
					break;
				default:
					if (key.replaceAll(" ", "").toLowerCase().matches("equipment[0-5]")) {
						library.getDriver()
								.findElement(By.xpath("//label[text()='Equipment 1']/following-sibling::mat-select"))
								.click();
						Thread.sleep(1000);
						library.getDriver()
								.findElement(By
										.xpath("//mat-option//span[normalize-space()='" + hmap.get(key).trim() + "']"))
								.click();
					} else if (key.replaceAll(" ", "").toLowerCase().matches("age[0-5]")) {
						library.getDriver()
								.findElement(
										By.xpath("//label[text()='Equipment 1']/../following-sibling::div/mat-select"))
								.sendKeys(hmap.get(key));
					} else
						throw new Exception("There is no such field [" + key + "]");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the asset care details : " + e.getMessage());
		}
	}

	public void performAction(String action) throws Exception {
		try {
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "showpremium":
				btnShowPremium.click();
				Thread.sleep(3000);
				library.waitForDigitLoad();
				break;
			case "savequote":
				btnSaveQuote.click();
				library.waitForDigitLoad();
				Library.staticMap.put("Policy Number", lblQuoteNumber.getText());
				break;
			case "proceedtopay":
				library.waitForDigitLoad();
				btnProceedToPay.click();
				library.waitForDigitLoad();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while performing action : [" + action + "]" + e.getMessage());
		}
	}

	public void enterCustomerDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "insuredname":
					txtInsuredName.sendKeys(hmap.get(key));
					break;
				case "pincode":
					txtCustomerPinCode.sendKeys(hmap.get(key));
					break;
				case "address":
					txtCustomerAddress.sendKeys(hmap.get(key));
					break;
				case "mobilenumber":
					txtMobileNumber.sendKeys(hmap.get(key));
					break;
				case "emailid":
					txtEmailID.sendKeys(hmap.get(key));
					break;
				default:
					throw new Exception("There is no such field [" + key + "]");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the customer details : " + e.getMessage());
		}
	}

	public void enterPaymentDetails(String paymentMode) throws Exception {
		try {
			String environment;
			if (System.getProperty("environment") == null || System.getProperty("environment").equals("")) {
				PropertyFileReader prop = new PropertyFileReader();
				environment = prop.getEnvironment().toString();
			} else
				environment = System.getProperty("environment");
			if (environment.equalsIgnoreCase("prod"))
				paymentMode = "Generate Payment Link";
			switch (paymentMode.replaceAll(" ", "").toLowerCase()) {
			case "agentfloat":
				drpPaymentMode.click();
				Thread.sleep(1000);
				library.getDriver().findElement(By.xpath("//mat-option//span[normalize-space()='Agent Float']"))
						.click();
				break;
			case "paymentbycustomercheque":
				drpPaymentMode.click();
				Thread.sleep(1000);
				library.getDriver()
						.findElement(By.xpath("//mat-option//span[normalize-space()='Payment by Customer Cheque']"))
						.click();
				txtChequeNumber.sendKeys(library.getRandomValue("[N | 6]"));
				txtIFSC.sendKeys("ADCB0000002");
				txtIFSC.sendKeys(Keys.TAB);
				break;
			case "paymentbypartnercheque":
				drpPaymentMode.click();
				Thread.sleep(1000);
				library.getDriver()
						.findElement(By.xpath("//mat-option//span[normalize-space()='Payment by Partner Cheque']"))
						.click();
				txtChequeNumber.sendKeys(library.getRandomValue("[N | 6]"));
				txtIFSC.sendKeys("ADCB0000002");
				txtIFSC.sendKeys(Keys.TAB);
				break;
			case "sendpaymentlink":
				drpPaymentMode.click();
				Thread.sleep(1000);
				library.getDriver().findElement(By.xpath("//mat-option//span[normalize-space()='Send Payment Link']"))
						.click();
				break;
			case "generatepaymentlink":
				drpPaymentMode.click();
				Thread.sleep(1000);
				library.getDriver()
						.findElement(By.xpath("//mat-option//span[normalize-space()='Generate Payment Link']")).click();
				break;
			case "onlinepayment":
				drpPaymentMode.click();
				Thread.sleep(1000);
				library.getDriver()
						.findElement(By.xpath("//mat-option//span[normalize-space()='Online Payment']")).click();
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the payment details : " + e.getMessage());
		}
	}

	public void verifySuccessPage() throws Exception {
		try {
			if (!lblSuccessMessage.isDisplayed()) {
				throw new Exception("Success message is not displayed");
			}
		} catch (Exception e) {
			throw new Exception("Error while verifying the success page : " + e.getMessage());
		}
	}
}
