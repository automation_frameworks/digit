package pom.agent.groupcovid;

import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.utilities.DBUtility;

public class CovidMasterPolicyCreation {

	@FindBy(xpath = "//button[contains(text(),'New Policy')]")
	private WebElement btnNewPolicy;

	@FindBy(xpath = "//label[text()='GST No.']/..//input")
	private WebElement txtGSTNum;

	@FindBy(xpath = "//label[text()='PAN No.']/..//input")
	private WebElement txtPANNum;

	@FindBy(xpath = "//label[text()='Company Name']/..//input")
	private WebElement txtCompanyName;

	@FindBy(xpath = "//label[text()='Address']/..//input")
	private WebElement txtAddress;

	@FindBy(xpath = "//label[text()='Pincode']/..//input")
	private WebElement txtPincode;

	@FindBy(xpath = "//label[text()='Mobile No.']/..//input")
	private WebElement txtMobileNum;

	@FindBy(xpath = "//label[text()='Email ID']/..//input")
	private WebElement txtEmail;

	@FindBy(xpath = "//label[text()='RM Email ID']/..//input")
	private WebElement txtRMEmail;

	@FindBy(xpath = "//label[text()='IMD Code']/..//input")
	private WebElement txtIMDCode;

	@FindBy(xpath = "//label[text()='Who will pay the premium?']/..//mat-select")
	private WebElement listWhoWillPayThePremium;

	@FindBy(xpath = "//label[text()='Max Sum Insured (INR)']/..//mat-select")
	private WebElement listMaxSumInsured;

	@FindBy(xpath = "//label[text()='Total Premium Including GST']/..//input")
	private WebElement txtTotalPremiumIncludingGST;

	@FindBy(xpath = "//label[text()='Count of Lives']/..//input")
	private WebElement txtCountOfLives;

	@FindBy(xpath = "//label[text()='Held cover letter number']/..//input")
	private WebElement txtHeldCoverLetterNumber;

	@FindBy(xpath = "//label[text()='DigitDesk ticket number']/..//input")
	private WebElement txtDigitDeskTicketNumber;

	@FindBy(xpath = "//label[text()='Remarks']/..//input")
	private WebElement txtRemarks;

	@FindBy(xpath = "//label[text()='Terms of the proposal']/..//mat-select")
	private WebElement listTermsOfTheProposal;

	@FindBy(xpath = "//label[text()='Proposal type']/..//mat-select")
	private WebElement listProposalType;

	@FindBy(xpath = "//label[text()='GST Exemption']/..//mat-select")
	private WebElement listGSTExemption;

	@FindBy(xpath = "//mat-checkbox//input")
	private WebElement chkVectorBorneDiseases;

	@FindBy(xpath = "//label[text()='Member Data Collection File']/..//button[2]")
	private WebElement btnUploadMemberDataCollectionFile;

	@FindBy(xpath = "//label[text()='Proposal Document upload']/..//button[2]")
	private WebElement btnUploadProposalDocument;

	@FindBy(xpath = "//label[contains(text(),'Master Policy')]/../..//label[text()='Email']/..//mat-select")
	private WebElement listMasterPolicyEmail;

	@FindBy(xpath = "//label[contains(text(),'Master Policy')]/../..//label[text()='SMS']/..//mat-select")
	private WebElement listMasterPolicySMS;

	@FindBy(xpath = "//label[contains(text(),'Child Policy')]/../..//label[text()='Email']/..//mat-select")
	private WebElement listChildPolicyEmail;

	@FindBy(xpath = "//label[contains(text(),'Child Policy')]/../..//label[text()='SMS']/..//mat-select")
	private WebElement listChildPolicySMS;

	@FindBy(xpath = "//span[contains(text(),'I hereby confirm')]/..//input")
	private WebElement chkIConfirm;

	@FindBy(id = "SubmitBtn")
	private WebElement btnProceed;

	@FindBys({@FindBy(xpath = "//span[@class='mat-option-text']")})
	private List<WebElement> valuesInListbox;

	@FindBy(xpath = "//label[contains(text(),'From')]/..//button")
	private WebElement iconPeriodOfInsuranceFrom;

	private static com.utilities.Library library = new com.utilities.Library();
	
	public void getNewPolicy() throws Exception {
		btnNewPolicy.click();
		library.waitForDigitLoad();
	}

	public void fillCompanyDetails(String covidId) throws SQLException {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.getGroupCovidDataFromDB(covidId);
		enterText(txtGSTNum, dbData[0]);
		enterText(txtPANNum, dbData[1]);
		enterText(txtCompanyName, dbData[2]);
		enterText(txtAddress, dbData[3]);
		enterText(txtPincode, dbData[4]);
		enterText(txtMobileNum, dbData[5]);
		enterText(txtEmail, dbData[6]);
		enterText(txtRMEmail, dbData[7]);
	}

	public void fillPolicyDetails(String covidId) throws SQLException, InterruptedException {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.getGroupCovidDataFromDB(covidId);
		enterText(txtIMDCode, dbData[8]);
		clickDesiredValueInListbox(listWhoWillPayThePremium,dbData[9]);
		clickOnDesiredDateInCalendar(iconPeriodOfInsuranceFrom, dbData[10]);
		clickDesiredValueInListbox(listMaxSumInsured, dbData[11]);
		enterText(txtTotalPremiumIncludingGST, dbData[12]);
		enterText(txtCountOfLives, dbData[13]);
		enterText(txtHeldCoverLetterNumber, dbData[14]);
		enterText(txtDigitDeskTicketNumber, dbData[15]);
		enterText(txtRemarks, dbData[16]);
	}
	
	public void fillSpecialConditions(String covidId) throws Exception {
		
		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.getGroupCovidDataFromDB(covidId);
		clickDesiredValueInListbox(listTermsOfTheProposal,dbData[17]);
		clickDesiredValueInListbox(listProposalType,dbData[18]);
		clickDesiredValueInListbox(listGSTExemption, dbData[19]);
		if (dbData[20].toLowerCase().equals("yes") && !chkVectorBorneDiseases.isSelected()) {
			chkVectorBorneDiseases.click();
		}
		upload(btnUploadMemberDataCollectionFile, dbData[25]);
		upload(btnUploadProposalDocument, dbData[26]);
	}
	
	public void fillCommunicationDetails(String covidId) throws Exception {
		
		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.getGroupCovidDataFromDB(covidId);
		clickDesiredValueInListbox(listMasterPolicyEmail,dbData[21]);
		clickDesiredValueInListbox(listMasterPolicySMS,dbData[22]);
		clickDesiredValueInListbox(listChildPolicyEmail,dbData[23]);
		clickDesiredValueInListbox(listChildPolicySMS,dbData[24]);
		if (!chkIConfirm.isSelected()) {
			chkIConfirm.click();
		}
	}

	private void enterText(WebElement textField, String data) {
		textField.clear();
		textField.sendKeys(data);
	}

	private void clickDesiredValueInListbox(WebElement listbox,String value) throws InterruptedException {
		if (value!=null) {
			library.waitForElementVisible(listbox, 10, 1000);
			listbox.click();
			Thread.sleep(2000);
			for (WebElement element : valuesInListbox) {
				if (element.getText().replaceAll(" ", "").replaceAll(",", "").toLowerCase()
						.contains(value.replaceAll(" ", "").replaceAll(",", "").toLowerCase())) {
					element.click();
					break;
				}
			} 
			Thread.sleep(3000);
		}
	}
	
	private void upload(WebElement btnUpload,String file) throws Exception {
		switch (file.replaceAll(" ", "").replaceAll("_", "").toLowerCase()) {
		case "abc":
			btnUpload.sendKeys(Paths.get("").toAbsolutePath().toString() + "/src/main/resources/documents/GMC_Large_Demography.xlsx");
			break;
		case "xyz":
			btnUpload.sendKeys(Paths.get("").toAbsolutePath().toString() + "/src/main/resources/documents/GMC_Large_Demography.xlsx");
			break;
		}
		library.waitForDigitLoad();
	}

	private void clickOnDesiredDateInCalendar(WebElement iconCalendar,String date) throws InterruptedException {

		iconCalendar.click();
		String dateTxt = date;
		Calendar cal = Calendar.getInstance(); 
		SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy"); 
		switch(date.toLowerCase().trim()) {
		case "today":
			date = s.format(cal.getTime());
			library.getDriver().findElement(By.xpath("//div[text()='"+date.split("-")[0]+"']")).click();
			break;
		case "tomorrow":
			cal.add(Calendar.DATE, 1);
			date = s.format(cal.getTime());
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//button[contains(@class , 'mat-calendar-period-button')]")).click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[text()='"+date.split("-")[2]+"']")).click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[text()='"+monthFlag(date)+"']")).click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[text()='"+Integer.parseInt(date.split("-")[0])+"']")).click();
			break;
		default:
			date = date.trim().toLowerCase().split(" ")[0];
			if (dateTxt.toLowerCase().contains("from")) {
				cal.add(Calendar.DATE, Integer.parseInt(date));
			} else {
				cal.add(Calendar.DATE, (-Integer.parseInt(date)-1));
			}
			date = s.format(cal.getTime());
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//button[contains(@class , 'mat-calendar-period-button')]"))
			.click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[text()='" + date.split("-")[2] + "']")).click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[text()='" + monthFlag(date) + "']")).click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[@class='mat-calendar-body-cell-content' and text()='"
					+ Integer.parseInt(date.split("-")[0]) + "']")).click();
			break;
		}
	}

	private String monthFlag(String date) {
		String month = "";
		switch(date.split("-")[1]) {
		case "01":
			month = "JAN";
			break;
		case "02":
			month = "FEB";
			break;
		case "03":
			month = "MAR";
			break;
		case "04":
			month = "APR";
			break;
		case "05":
			month = "MAY";
			break;
		case "06":
			month = "JUN";
			break;
		case "07":
			month = "JUL";
			break;
		case "08":
			month = "AUG";
			break;
		case "09":
			month = "SEPT";
			break;
		case "10":
			month = "OCT";
			break;
		case "11":
			month = "NOV";
			break;
		case "12":
			month = "DEC";
			break;
		}
		return month;
	}
}

