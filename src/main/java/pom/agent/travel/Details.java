package pom.agent.travel;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.utilities.Library;

public class Details {

	@FindBy(xpath = "//input[@aria-label='Enter Country' or @aria-label='Add a new country']")
	private WebElement txtCountriesVisiting;

	@FindBy(id = "setDepartureDate")
	private WebElement txtDepartureDate;

	@FindBy(name = "totalDays")
	private WebElement txtNumberofDays;

	@FindBy(id = "setReturnDate")
	private WebElement txtreturnDate;

	@FindBy(xpath = "//div[text()='Self/Group']/../div/div[2]")
	private WebElement radSelfGroup;

	@FindBy(xpath = "//div[contains(text(),'Family')]/../div/div[2]")
	private WebElement radFamily;

	@FindBy(id = "policyHolderDob")
	private WebElement txtPolicyHolderDOB;

	@FindBy(name = "addTravellerNumber")
	private WebElement txtNumberofTraveller;

	@FindBy(xpath = "//input[@value='Show Plans']")
	private WebElement btnShowPlans;

	@FindBy(id = "setName[0]")
	private WebElement txtTraveller1Name;

	@FindBy(xpath = "//input[@value='Issue Policy']")
	private WebElement btnIssuePolicy;

	@FindBy(id = "setPayment")
	private WebElement drpPaymentType;

	@FindBy(id = "setChequeNumber")
	private WebElement txtChequeNumber;

	@FindBy(id = "setChequeDate")
	private WebElement txtChequeDate;

	@FindBy(id = "setmicr")
	private WebElement txtMICRCode;

	@FindBy(xpath = "//div[@id='toast-container']//span")
	private WebElement lblErrorMessage;

	@FindBy(xpath = "//input[@name='diseaseCheck']/following-sibling::span")
	private WebElement chkDeclaration;

	@FindBy(xpath = "//label[contains(text(),'Policy Number')]/..//b")
	private WebElement lblPolicyNumber;

	@FindBy(xpath = "//button[text()='Download Policy']")
	private WebElement btnDownloadPolicy;

	@FindBy(xpath = "//div[@class='hero bg-primary']/h1")
	private WebElement lblSuccessMessage;

	private com.utilities.Library library = new com.utilities.Library();

	public void enterTravelDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "countries":
					for (String temp : hmap.get(key).split(",")) {
						Thread.sleep(2000);
						txtCountriesVisiting.sendKeys(temp);
						Thread.sleep(2000);
						library.getDriver().findElement(By.xpath("//ng2-menu-item//b[text()='" + temp.trim() + "']"))
								.click();
					}
					break;
				case "departuredate":
					library.enterDate(txtDepartureDate, hmap.get(key));
					break;
				case "noofjourneydays":
				case "noofdays":
					txtNumberofDays.sendKeys(hmap.get(key));
					break;
				case "returndate":
					library.enterDate(txtreturnDate, hmap.get(key));
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the travel details : " + e.getMessage());
		}
	}

	public void enterTravellerDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "traveltype":
					txtNumberofTraveller.sendKeys("");
					if (hmap.get(key).replaceAll(" ", "").toLowerCase().equals("self/group"))
						radSelfGroup.click();
					else if (hmap.get(key).replaceAll(" ", "").toLowerCase().equals("family"))
						radFamily.click();
					break;
				case "policyholderdob":
					library.enterDate(txtPolicyHolderDOB, hmap.get(key));
					break;
				case "numberoftraveller":
					txtNumberofTraveller.sendKeys(hmap.get(key));
					txtNumberofDays.click();
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the traveller details : " + e.getMessage());
		}
	}

	public void enterDOB(String traveller, String dob) throws Exception {
		try {
			library.enterDate(library.getDriver().findElement(By.xpath(
					"//label[text()='Date of birth of traveller " + traveller.trim() + "']/following-sibling::input")),
					dob);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("DOB field is not found for traveller " + traveller
					+ ". Please check the number of Travellers provided");
		}
	}

	public void enterTravellerInfo(String traveller, String details) throws Exception {
		String fieldName = "";
		try {
			int travellerNumber;
			HashMap<String, String> hmap = library.getFieldMap(details);
			int begin, end;
			if (traveller.contains("-")) {
				begin = Integer.parseInt(traveller.split("-")[0]);
				end = Integer.parseInt(traveller.split("-")[1]);
			} else {
				begin = Integer.parseInt(traveller.trim());
				end = Integer.parseInt(traveller.trim());
			}
			for (int i = begin; i <= end; i++) {
				travellerNumber = i - 1;
				traveller = "" + i;
				for (String key : hmap.keySet()) {
					library.waitForDigitLoad();
					fieldName = key;
					try {
						switch (key.replaceAll(" ", "").toLowerCase()) {
						case "passportnumber":
							library.getDriver().findElement(By.id("setPassportNumber[" + travellerNumber + "]"))
									.sendKeys(hmap.get(key));
							break;
						case "nationality":
							library.getDriver().findElement(By.id("setNationality[" + travellerNumber + "]"))
									.sendKeys(hmap.get(key));
							break;
						case "name":
							library.getDriver().findElement(By.id("setName[" + travellerNumber + "]"))
									.sendKeys(hmap.get(key));
							break;
						case "mobilenumber":
							library.getDriver().findElement(By.id("setMobile[" + travellerNumber + "]"))
									.sendKeys(hmap.get(key));
							break;
						case "emailid":
						case "email":
							library.getDriver().findElement(By.id("setEmail[" + travellerNumber + "]"))
									.sendKeys(hmap.get(key));
							break;
						case "gender":
							library.selectDropDownValue(library.getDriver()
									.findElement(By.xpath("//h1[text()='Traveller " + traveller.trim()
											+ "']/..//label[text()='Gender']/..//div[@class='ng-autocomplete-inputs']/input")),
									hmap.get(key));
							break;
						case "pancardnumber":
							library.getDriver()
									.findElement(By.xpath("//h1[text()='Traveller " + traveller.trim()
											+ "']/..//label[text()='Pan Card Number']/..//input"))
									.sendKeys(hmap.get(key));
							break;
						case "city":
							library.selectDropDownValue(library.getDriver()
									.findElement(By.xpath("//h1[text()='Traveller " + traveller.trim()
											+ "']/..//label[text()='City']/..//div[@class='ng-autocomplete-inputs']/input")),
									hmap.get(key));
							library.waitForDigitLoad();
							break;
						case "pincode":
							library.selectDropDownValue(library.getDriver()
									.findElement(By.xpath("//h1[text()='Traveller " + traveller.trim()
											+ "']/..//label[text()='Pincode']/..//div[@class='ng-autocomplete-inputs']/input")),
									hmap.get(key));
							library.waitForDigitLoad();
							break;
						case "address":
							library.getDriver().findElement(By.id("setAddress[" + travellerNumber + "]"))
									.sendKeys(hmap.get(key));
							break;
						case "preexistingdisease":
						case "additionalinformation":
							if (hmap.get(key).equalsIgnoreCase("no")) {
								try {
									library.getDriver().findElement(By.id("setNomineeName[" + travellerNumber + "]"))
											.click();
								} catch (Exception ne) {
								}
								library.getDriver().findElement(By.xpath("//h1[text()='Traveller " + traveller.trim()
										+ "']/..//label[contains(text(),'Pre-existing diseases?')]/../following-sibling::div//div[text()='No']"))
										.click();
							} else {
								try {
									library.getDriver().findElement(By.id("setNomineeName[" + travellerNumber + "]"))
											.click();
								} catch (Exception ne) {
								}
								library.getDriver().findElement(By.xpath("//h1[text()='Traveller " + traveller.trim()
										+ "']/..//label[contains(text(),'Pre-existing diseases?')]/../following-sibling::div//div[text()='Yes']"))
										.click();
								try {
									library.getDriver().findElement(By.id("setNomineeName[" + travellerNumber + "]"))
											.click();
								} catch (Exception ne) {
								}
								for (String temp : hmap.get(key).split(",")) {
									library.getDriver().findElement(By.xpath("//h1[text()='Traveller "
											+ traveller.trim()
											+ "']/..//label[contains(text(),'Pre-existing diseases?')]/../following-sibling::div[2]//div[contains(.,'"
											+ temp.trim() + "')][@class='col-3']/input")).click();
									//// div[contains(text(),'')]
								}

							}

							break;
						case "nomineename":
							library.getDriver().findElement(By.id("setNomineeName[" + travellerNumber + "]"))
									.sendKeys(hmap.get(key));
							break;
						case "relationship":
						case "relationshipwithtraveller":
							library.selectDropDownValue(library.getDriver()
									.findElement(By.xpath("//h1[text()='Traveller " + traveller.trim()
											+ "']/..//label[text()='Relationship with traveller']/..//div[@class='ng-autocomplete-inputs']/input")),
									hmap.get(key));
							break;
						default:
							throw new Exception("Implementation for the field '" + key + "' is not found");
						}
					} catch (NoSuchElementException ne) {
						ne.printStackTrace();
						throw new Exception("Field '" + fieldName + "' is not displayed for the travller " + traveller
								+ " or the locator defined for the field is not correct");
					}
				}
				enterMissingVehicleDetails(traveller, details);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the traveller personal details : " + e.getMessage());
		}
	}

	public void performAction(String action) throws Exception {
		try {
			library.waitForDigitLoad();
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "showplans":
				btnShowPlans.click();
				library.waitForDigitLoad();
				break;
			case "issuepolicy":
				Library.staticMap.put("Insurer Name", txtTraveller1Name.getAttribute("value"));
				btnIssuePolicy.click();
				library.waitForDigitLoad();
				break;
			case "downloadpolicy":
				btnDownloadPolicy.click();
				library.waitForDigitLoad();
				break;
			default:
				throw new Exception("Implementation for the action '" + action + "' is not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while performing action : " + e.getMessage());
		}
	}

	public void selectPlan(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "plannumber":
				case "plan":
					int planNumber = Integer.parseInt(hmap.get(key)) - 1;
					library.getDriver().findElement(By.id("planValue[" + planNumber + "]")).click();
					break;
				case "premium":
					library.getDriver()
							.findElement(
									By.xpath("//div[@id='plans']//span[contains(text(),'Premium:')][contains(text(),'"
											+ hmap.get(key).trim() + "')]/.."))
							.click();
					break;
				case "suminsured":
					library.getDriver()
							.findElement(By
									.xpath("//div[@id='plans']//span[contains(text(),'Sum Insured:')][contains(text(),'"
											+ hmap.get(key).trim() + "')]/.."))
							.click();
					break;
				}
			}
			library.waitForDigitLoad();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while selecting the plan : " + e.getMessage());
		}

	}

	public void verifyErrorMessage(String message) throws Exception {
		try {
			try {
				if (lblErrorMessage.isDisplayed()) {
					if (!lblErrorMessage.getText().contains(message))
						throw new Exception("Expected error message is not displayed. Expected : " + message
								+ " Displayed : " + lblErrorMessage.getText());
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception("Error message is not displayed.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while verifying the Error Message : " + e.getMessage());
		}
	}

	public void acceptDeclaration() throws Exception {
		try {
			JavascriptExecutor jse = (JavascriptExecutor) library.getDriver();
			jse.executeScript("window.scrollBy(0,2500)", "");
			chkDeclaration.click();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while verifying the Error Message : " + e.getMessage());
		}
	}

	public void verifySuccessPage() throws Exception {
		try {
			library.waitForDigitLoad();
			Thread.sleep(5000);
			System.out.println("Policy Number : " + lblPolicyNumber.getText());
			Library.staticMap.put("Policy Number", lblPolicyNumber.getText());
			if (!lblSuccessMessage.isDisplayed())
				throw new Exception("Success message is not displayed!!!");
			String successMessage = lblSuccessMessage.getText();
			if (!successMessage.trim().contains(Library.staticMap.get("Insurer Name") + "’s Travel is now insured!"))
				throw new Exception("Success message is not displayed correctly!!! Expected : "
						+ Library.staticMap.get("Insurer Name") + "’s Travel is now insured!" + ". Actual : "
						+ successMessage.trim());
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while verifying the success page : " + e.getMessage());
		}
	}

	public void enterPaymentDetails(String paymentType) throws Exception {
		try {
			Select select = new Select(drpPaymentType);
			switch (paymentType.replaceAll(" ", "").toLowerCase()) {
			case "agentfloat":
				select.selectByVisibleText("Agent Float");
				break;
			case "paymentbycustomercheque":
				select.selectByVisibleText("Payment by Customer Cheque");
				txtChequeNumber.sendKeys(library.getRandomValue("N | 6"));
				library.enterDate(txtChequeDate, "[TODAY]");
				txtMICRCode.sendKeys("560229002");
				txtChequeNumber.click();
				library.waitForDigitLoad();
				break;
			case "paymentbypartnercheque":
				select.selectByVisibleText("Payment by Partner Cheque");
				txtChequeNumber.sendKeys(library.getRandomValue("N | 6"));
				library.enterDate(txtChequeDate, "[TODAY]");
				txtMICRCode.sendKeys("560229002");
				txtChequeNumber.click();
				library.waitForDigitLoad();
				break;
			case "onlinepayment":
			case "online":
				select.selectByVisibleText("Online Payment");
				break;
			case "generatepaymentlink":
				select.selectByVisibleText("Generate Payment Link");
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the payment details : " + e.getMessage());
		}
	}
	
	
	public void performOnlinePayment() throws Exception {
		try {
			try {
				Thread.sleep(10000L);
			} catch (InterruptedException e) {
			}
			/*String frameLastPart = library.getDriver().findElement(By.xpath("//iframe[@class='card_number_iframe']"))
					.getAttribute("name");
			frameLastPart = frameLastPart.split("_")[3];
			// try {Thread.sleep(15000L);} catch (InterruptedException e) {}
			String cardNumberFrame = "card_number_iframe_" + frameLastPart;
			String cardExpiryMonthFrame = "card_exp_month_iframe_" + frameLastPart;
			String cardExpiryYearFrame = "card_exp_year_iframe_" + frameLastPart;
			String cardCVVFrame = "security_code_iframe_" + frameLastPart;
			String cardHolderNameFrame = "name_on_card_iframe_" + frameLastPart;

			library.getDriver().switchTo().defaultContent();
			library.getDriver().switchTo().frame(cardNumberFrame);
			library.getDriver().findElement(By.id("card_number")).sendKeys("5123456789012346");

			library.getDriver().switchTo().defaultContent();
			library.getDriver().switchTo().frame(cardHolderNameFrame);
			library.getDriver().findElement(By.id("name_on_card")).sendKeys("Test Name");

			library.getDriver().switchTo().defaultContent();
			library.getDriver().switchTo().frame(cardExpiryMonthFrame);
			library.getDriver().findElement(By.id("card_exp_month")).sendKeys("05");

			library.getDriver().switchTo().defaultContent();
			library.getDriver().switchTo().frame(cardExpiryYearFrame);
			library.getDriver().findElement(By.id("card_exp_year")).sendKeys("20");

			library.getDriver().switchTo().defaultContent();
			library.getDriver().switchTo().frame(cardCVVFrame);
			library.getDriver().findElement(By.id("security_code")).sendKeys("123");*/

			library.getDriver().switchTo().defaultContent();
			WebElement btnPayNow = library.getDriver().findElement(By.xpath("//button[contains(text(),'Mock Payment')]"));

			btnPayNow.click();
			try {
				Thread.sleep(30000L);
			} catch (InterruptedException e) {
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while performing the Online Payment : " + e.getMessage());
		}
	}

	private void enterMissingVehicleDetails(String traveller, String details) throws Exception {
		try {
			int travellerNumber = Integer.parseInt(traveller.trim()) - 1;
			HashMap<String, String> hmap = library.getFieldMap(details);
			Set<String> fields = new LinkedHashSet<>();
			boolean email = false;
			boolean preexistingdisease = false;
			boolean relationship = false;
			for (String temp : hmap.keySet())
				fields.add(temp.replaceAll(" ", "").toLowerCase());
			LinkedList<String> mandatoryFields = getMandatoryTravellerFields();
			for (String temp : mandatoryFields) {
				if (!fields.contains(temp.replaceAll(" ", "").toLowerCase())) {
					library.waitForDigitLoad();
					switch (temp) {
					case "passportnumber":
						library.getDriver().findElement(By.id("setPassportNumber[" + travellerNumber + "]"))
								.sendKeys(library.getRandomValue("AN | 10"));
						break;
					case "nationality":
						library.getDriver().findElement(By.id("setNationality[" + travellerNumber + "]"))
								.sendKeys("Indian");
						break;
					case "name":
						library.getDriver().findElement(By.id("setName[" + travellerNumber + "]"))
								.sendKeys(library.getRandomValue("AN | 10"));
						break;
					case "mobilenumber":
						library.getDriver().findElement(By.id("setMobile[" + travellerNumber + "]"))
								.sendKeys(library.getRandomValue("N | 10"));
						break;
					case "email":
						if (fields.contains("emailid"))
							continue;
						if (!email)
							library.getDriver().findElement(By.id("setEmail[" + travellerNumber + "]"))
									.sendKeys(library.getRandomValue("AN | 10") + "@godigit.com");
						email = true;
						break;
					case "emailid":
						if (fields.contains("email"))
							continue;
						if (!email)
							library.getDriver().findElement(By.id("setEmail[" + travellerNumber + "]"))
									.sendKeys(library.getRandomValue("AN | 10") + "@godigit.com");
						email = true;
						break;
					case "gender":
						library.selectDropDownValue(library.getDriver().findElement(By.xpath("//h1[text()='Traveller "
								+ traveller.trim()
								+ "']/..//label[text()='Gender']/..//div[@class='ng-autocomplete-inputs']/input")),
								"Male");
						break;
					case "city":
						library.selectDropDownValue(library.getDriver().findElement(By.xpath("//h1[text()='Traveller "
								+ traveller.trim()
								+ "']/..//label[text()='City']/..//div[@class='ng-autocomplete-inputs']/input")),
								"Bengaluru");
						library.waitForDigitLoad();
						break;
					case "pincode":
						library.selectDropDownValue(library.getDriver().findElement(By.xpath("//h1[text()='Traveller "
								+ traveller.trim()
								+ "']/..//label[text()='Pincode']/..//div[@class='ng-autocomplete-inputs']/input")),
								"560001");
						library.waitForDigitLoad();
						break;
					case "address":
						library.getDriver().findElement(By.id("setAddress[" + travellerNumber + "]"))
								.sendKeys(library.getRandomValue("AN | 15"));
						break;
					case "preexistingdisease":
						if (fields.contains("additionalinformation"))
							continue;
						if (!preexistingdisease) {
							try {
								library.getDriver().findElement(By.id("setNomineeName[" + travellerNumber + "]"))
										.click();
							} catch (Exception ne) {
							}
							library.getDriver().findElement(By.xpath("//h1[text()='Traveller " + traveller.trim()
									+ "']/..//label[contains(text(),'Pre-existing diseases?')]/../following-sibling::div//div[text()='No']"))
									.click();
							preexistingdisease = true;
						}
						break;
					case "additionalinformation":
						if (fields.contains("preexistingdisease"))
							continue;
						if (!preexistingdisease) {
							try {
								library.getDriver().findElement(By.id("setNomineeName[" + travellerNumber + "]"))
										.click();
							} catch (Exception ne) {
							}
							library.getDriver().findElement(By.xpath("//h1[text()='Traveller " + traveller.trim()
									+ "']/..//label[contains(text(),'Pre-existing diseases?')]/../following-sibling::div//div[text()='No']"))
									.click();
							preexistingdisease = true;
						}
						break;
					case "nomineename":
						library.getDriver().findElement(By.id("setNomineeName[" + travellerNumber + "]"))
								.sendKeys(library.getRandomValue("AN | 15"));
						break;
					case "relationship":
						if (fields.contains("relationshipwithtraveller"))
							continue;
						if (!relationship)
							library.selectDropDownValue(library.getDriver()
									.findElement(By.xpath("//h1[text()='Traveller " + traveller.trim()
											+ "']/..//label[text()='Relationship with traveller']/..//div[@class='ng-autocomplete-inputs']/input")),
									"Brother");
						relationship = true;
						break;
					case "relationshipwithtraveller":
						if (fields.contains("relationship"))
							continue;
						if (!relationship)
							library.selectDropDownValue(library.getDriver()
									.findElement(By.xpath("//h1[text()='Traveller " + traveller.trim()
											+ "']/..//label[text()='Relationship with traveller']/..//div[@class='ng-autocomplete-inputs']/input")),
									"Brother");
						relationship = true;
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}
	
	private LinkedList<String> getMandatoryTravellerFields() {
		LinkedList<String> vehicleMandatoryFields = new LinkedList<String>();
		vehicleMandatoryFields.add("passportnumber");
		vehicleMandatoryFields.add("nationality");
		vehicleMandatoryFields.add("name");
		vehicleMandatoryFields.add("mobilenumber");
		vehicleMandatoryFields.add("emailid");
		vehicleMandatoryFields.add("email");
		vehicleMandatoryFields.add("gender");
		vehicleMandatoryFields.add("city");
		vehicleMandatoryFields.add("pincode");
		vehicleMandatoryFields.add("address");
		vehicleMandatoryFields.add("additionalinformation");
		vehicleMandatoryFields.add("nomineename");
		vehicleMandatoryFields.add("relationshipwithtraveller");

		return vehicleMandatoryFields;
	}
}
