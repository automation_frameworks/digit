package pom.agent.motor;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;


public class PaymentPage 
{
	@FindBy(xpath="//iframe[contains(@name,'card_number')]")
	private WebElement frameCardNum;

	@FindBy(xpath="//iframe[contains(@name,'name_on_card')]")
	private WebElement frameNameOnCard;
	
	@FindBy(xpath="//iframe[contains(@name,'card_exp_month')]")
	private WebElement frameExpMonth;
	
	@FindBy(xpath="//iframe[contains(@name,'card_exp_year')]")
	private WebElement frameExpYear;
	
	@FindBy(xpath="//iframe[contains(@name,'security_code')]")
	private WebElement frameCvv;
	
	@FindBy(id = "card_number")
	private WebElement txtCardNum;
	
	@FindBy(id="name_on_card")
	private WebElement txtNameOnCard;
	
	@FindBy(id="card_exp_month")
	private WebElement txtExpmon;
	
	@FindBy(id="card_exp_year")
	private WebElement txtExpYear;
	
	@FindBy(id="security_code")
	private WebElement txtCvvCode;
	
	@FindBy(xpath="//button[contains(text(),'Pay')]")
	private WebElement payBtn;
	
	@FindBy(xpath="//a[text()='Credit / Debit Card']")
	private WebElement txtCard;
	
	@FindBy(xpath= "//div[contains(.,'Choose payment method')][@class='mainHeader']")
	private WebElement header;
	
	@FindBy(xpath="//iframe[@id='juspay-hidden-form1565072952900']")
	private WebElement frame1;
	
	@FindBy(xpath="//h3[text() = 'Add Money to Your Account']")
	private WebElement txtAddMoneyToAcc;
	
	@FindBy(xpath="//div[@id='toast-container' and contains(@style,'position: fixed;')]//span")
	private WebElement toastMsg;

	@FindBy(xpath="id=submit")
	private WebElement submitBtn;
	
	@FindBy(xpath="//button[contains(text(),'Mock Payment')]")
	private WebElement mockPaymentBtn;
	
	com.utilities.Library libraryobj = new com.utilities.Library();
	
	public void enterCardDetails(WebDriver driver) throws Exception
	{
		Thread.sleep(15000);
		
		driver.switchTo().frame(frameCardNum);
		txtCardNum.sendKeys("4012001037141112");
		
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame(frameNameOnCard);
		txtNameOnCard.sendKeys("Vishnu Chandar");
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame(frameExpMonth);
		txtExpmon.sendKeys("12");
		
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame(frameExpYear);
		txtExpYear.sendKeys("20");
		
		driver.switchTo().defaultContent();
		driver.switchTo().frame(frameCvv);
		txtCvvCode.sendKeys("123");
		 
		driver.switchTo().defaultContent();
	       
	    JavascriptExecutor js=(JavascriptExecutor) driver;
	    js.executeScript("arguments[0].click()", mockPaymentBtn);
	}
	
	public void clickSubmitButton(WebDriver driver) throws Exception
	{
		libraryobj.waitForDigitLoad();
		submitBtn.click();
		libraryobj.waitForElementVisible(txtAddMoneyToAcc, 60, 5);
		
		
	}
	
	public void verifySuccessMessageFromToast()
	{
		libraryobj.waitForVisibilityOfTheElement(toastMsg);
		
		String toastMessage =toastMsg.getText();
		System.out.println(toastMessage);
		Assert.assertEquals(toastMessage, "Payment success");
		System.out.println("Payment is succcessfully done");
	}
}
