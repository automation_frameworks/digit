package pom.agent.motor;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class SmsLinkGeratorPage 
{
	@FindBy(xpath="//div[@id='toast-container' and contains(@style,'position: fixed;')]//span")
	private WebElement toastMsg;
	
	@FindBy(xpath="//label[text()='Partner Name']/following-sibling::input[@type='text']")
	private WebElement txtpartnerNme;
	
	@FindBy(xpath="//label[text()='Partner Contact No']/following-sibling::input[@type='text']")
	private WebElement txtpartnerContactNum;
	
	@FindBy(xpath="//button[text()='Send Message']")
	private WebElement btnSendMsg;
	
	public void clickButton()
	{
		btnSendMsg.click();
	}
	
	public void enterDetails(String partnerName, String partnerNum)
	{
		txtpartnerNme.sendKeys(partnerName);
		txtpartnerContactNum.sendKeys(partnerNum);
	}
	
	com.utilities.Library libraryobj = new com.utilities.Library();
	public void verifySuccessMessageFromToast()
	{
		libraryobj.waitForVisibilityOfTheElement(toastMsg);
		String toastMessage =toastMsg.getText();
		System.out.println(toastMessage);
		Assert.assertEquals(toastMessage, "1 Success, 0 Failed");
		System.out.println("Message is sent successfully");
	}
	
}
