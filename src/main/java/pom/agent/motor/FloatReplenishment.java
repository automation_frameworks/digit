package pom.agent.motor;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class FloatReplenishment 
{
	@FindBy(xpath="//input[@id ='Pay Online']")
	private WebElement BthPayOnline;
	
	@FindBy(xpath="//label[text()='Amount to be added']/following-sibling::input")
	private WebElement txtAmount;
	
	@FindBy(xpath="//button[contains(text(),'Pay')]")
	private WebElement BtnPay;
	
	private com.utilities.Library library = new com.utilities.Library();
	
	public void selectPayOnlineButton() {
		try {
			BthPayOnline.isSelected();
		}
		catch(Exception e){
			BthPayOnline.click();
		}
	}
	
	public void enterAmountToBeAdded_AndClickOnPayButton(String amount) throws Exception {
		txtAmount.clear();
		txtAmount.sendKeys(amount);
		Thread.sleep(3000);
		library.waitForElementToBeClickable(BtnPay, 20, 5);
		BtnPay.click();
		library.waitForDigitLoad();
	}
	
	public void checkPayBtnIsEnabled()
	{
		try {
			BtnPay.isEnabled();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
