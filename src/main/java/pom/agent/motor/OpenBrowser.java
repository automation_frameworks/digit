package pom.agent.motor;

import java.util.Date;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;



public class OpenBrowser {

	
	public static void main(String args[])
	{
		
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);

		System.setProperty("webdriver.chrome.driver", "C:\\ARTEMIS\\references\\chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		options.setExperimentalOption("prefs", chromePrefs);
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		 Date date= new Date();
		System.out.println(date.getTime());
		WebDriver driver = new ChromeDriver(options);
		driver.get("https://python.org");
		driver.findElement(By.id("id-search-field")).sendKeys("Python");
		driver.findElement(By.id("submit")).click();
		driver.findElement(By.xpath("//a[text()='Python 2.5.4']")).click();
		driver.findElement(By.xpath("//a[text()='About']")).click();
		driver.findElement(By.xpath("//a[text()='Downloads']")).click();
		driver.findElement(By.xpath("//a[text()='Documentation']")).click();
		driver.findElement(By.xpath("//a[text()='Community']")).click();
		driver.findElement(By.xpath("//a[text()='Success Stories']")).click();
		date= new Date();
		System.out.println(date.getTime());
	}
}
