package pom.agent.motor;

import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.utilities.Library;

public class LoaderPage {

	@FindBy(xpath = "//section[@class='card']//h4[text()='Motor Loader:']")
	private WebElement txtMotorLoader;

	@FindBy(xpath = "//select[@class='ng-untouched ng-pristine ng-valid']")
	private WebElement drpMotorType;
	
	@FindBy(xpath = "(//select)[1]")
	private WebElement drpDownValue;

	@FindBy(xpath = "//input[@type='file']")
	private WebElement btnUploadExcel;
	
	@FindBy(xpath = "//span[text()='Address Line 1']/parent::div/following-sibling::div/input")
	private WebElement text2WAddress1;
	
	@FindBy(xpath = "//span[text()='ADDRESS1']/parent::div/following-sibling::div/input ")
	private WebElement textCVAddress1;

	@FindBy(xpath = "//span[.='Edit']")
	private List<WebElement> editButton;

	@FindBy(xpath = "(//span[.='Edit'])[1]")
	private WebElement editButton1;

	@FindBy(xpath = "//button[.='Save']")
	private WebElement buttonSave;
	
	@FindBy(xpath="//span[text()='Chasis #']/../following-sibling::div/input") 
	private WebElement text2WChasis;
	
	@FindBy(xpath="//span[text()='CHSNO']/../following-sibling::div/input") 
	private WebElement textCVChasis;
	
	@FindBy(xpath="//span[text()='Engine #']/../following-sibling::div/input")
	private WebElement text2WEngine;
	
	@FindBy(xpath="//span[text()='ENGNNO']/../following-sibling::div/input")
	private WebElement textCVEngine;
	
	@FindBy(xpath="//span[text()='Previous Policy Expiry Date']/../following-sibling::div/input")
	private WebElement text2WPolExpDate;
	
	@FindBy(xpath="//span[.='P_PLCY_EXDT']/parent::div/following-sibling::div/input")
	private WebElement textCVPolExpDate;
		
	@FindBy(xpath = "//span[text()='Registration Number']/../following-sibling::div/input")
	private WebElement text2WRegNum;

	@FindBy(xpath = "//div[@class='row']//span[contains(text(),'IDV')]/../following-sibling::div/input")
	private WebElement txtIDV;

	@FindBy(xpath = "//i[text() ='check_box_outline_blank']")
	private List<WebElement> checkBoxField;

	@FindBy(xpath = "//i[text() ='check_box_outline_blank'][1]")
	private WebElement selAllCheckBox;

	@FindBy(xpath = "//button[contains(text(),'Quick Quotes')]")
	private WebElement quickQuoteBtn;

	@FindBy(xpath = "//div[@class ='tableBodyContainer']/div/div/div[6]")
	private List<WebElement> allCommentsText;

	@FindBy(xpath = "//span[.='Policy Start Date']/parent::div/following-sibling::div/input")
	private WebElement text2WPolSrtDate;
	
	@FindBy(xpath="//span[.='P_PLCYSTDT']/parent::div/following-sibling::div/input")
	private WebElement textCVPolSrtDate;
	
	@FindBy(xpath="//span[text()='New/Existing']//parent::div/following-sibling::div/input")
	private WebElement txtNewOrExisting;
	
	@FindBy(xpath="//span[text()='NEW_OR_USED']//parent::div/following-sibling::div/input")
	private WebElement txtCVNewOrExisting;
	
	@FindBy(xpath="//button[contains(text(),'Get Quotes')]")
	private WebElement getQuoteBtn;

	@FindBy(xpath = "//button[contains(text(),'Issue Policy')]")
	private WebElement btnIssuePolicy;

	@FindBy(xpath = "(//div[@class='tableBodyContainer'])/div/div/div[7]/span")
	private List<WebElement> loaderComments;

	@FindBy(xpath = "(//div[@class='tableBodyContainer'])/div/div/div[7]/span")
	private List<WebElement> loaderCommentsAfterGetQuote;

	@FindBy(xpath = "//span[text()='Registration Number']/../following-sibling::div/input")
	private WebElement regNoTxtBox;
	
	@FindBy(xpath="//span[text()='VHCLENOPRFX1']/../following-sibling::div/input")
	private WebElement CV_RegNum_1;
	
	@FindBy(xpath="//span[text()='VHCLENOPRFX2']/../following-sibling::div/input")
	private WebElement CV_RegNum_2;
	
	@FindBy(xpath="//span[text()='VHCLENOPRFX3']/../following-sibling::div/input")
	private WebElement CV_RegNum_3;
	
	@FindBy(xpath="//span[text()='VHCLENOPRFX4']/../following-sibling::div/input")
	private WebElement CV_RegNum_4;
	
	@FindBy(xpath="//span[text()='REG_DATE']/../following-sibling::div/input")
	private WebElement txtCVRegManDate;
	
	@FindBy(xpath = "//span[text()='Registration / Manufacturing Date']/../following-sibling::div/input")
	private WebElement txtRegManDate;

	@FindBy(xpath = "//span[text()='AAA_Scenario']/../following-sibling::div/input")
	private WebElement txtScenario;

	@FindBy(xpath = "((//div[@class='tableBodyContainer'])/div/div/div[6]/span)[1]/ancestor::div[contains(@class,'row')]/div/div/i")
	private WebElement first_comment;
	
	@FindBy(xpath="//p[text()='Motor Loader New Quotes']/..")
	private WebElement lblNewLoaderQuotes;

	String motorType = null;
	private com.utilities.Library library = new com.utilities.Library();
	// private static LinkedHashMap loaderMap;
	static int row=0;

	private static HashMap<String, String> loaderMap = new LinkedHashMap<>();

	public void selectMotorType(String motorType) throws Exception {
		lblNewLoaderQuotes.click();
		Library library = new Library();
		library.waitForDigitLoad();
		this.motorType = motorType;
		Assert.assertEquals(txtMotorLoader.getText(), "Motor Loader:");
		
		library.selectDropDownValue(drpMotorType, motorType);

	}

	public void uploadExcelSheet(String data) throws Throwable {
		try {
		Library library = new Library();
		library.waitForDigitLoad();
		switch(data.toUpperCase())
		{
		case "TWO WHEELER" :
			System.out.println("Uploading 2W Excel Sheet");
			btnUploadExcel.sendKeys(
					Paths.get("").toAbsolutePath().toString() + "/src/main/resources/exceldata/2W-Loader.xlsx");
			break;
		case "PRIVATE CAR":
			System.out.println("Uploading 4W Excel Sheet");
			btnUploadExcel.sendKeys(
					Paths.get("").toAbsolutePath().toString() + "/src/main/resources/exceldata/4W-Loader.xlsx");
			break;
		case "COMMERCIAL VEHICLE":
			System.out.println("Uploading CVOD Excel Sheet");
			btnUploadExcel.sendKeys(
					Paths.get("").toAbsolutePath().toString() + "/src/main/resources/exceldata/CV-Loader.xlsx");
			break;
		case "CVTP":
			System.out.println("Uploading CVTP Excel Sheet");
			btnUploadExcel.sendKeys("D:\\AgentPortal\\AutomationCode\\SeleniumCucumber-master\\src\\main\\resources\\exceldata\\CVTP-Loader.xlsx");
			break;
		case "LEAD_GENERATOR":
			System.out.println("Uploading Lead Generator Excel Sheet");
			btnUploadExcel.sendKeys(
					Paths.get("").toAbsolutePath().toString() + "/src/main/resources/exceldata/Lead_Generator.xlsx");
			break;
			
		}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception("Error while uploading the loader : " + e.getMessage());
		}
	}

	public void scrollDown() throws Throwable {
		JavascriptExecutor js = (JavascriptExecutor) library.getDriver();
		js.executeScript("window.scrollBy(0,500)");
		Thread.sleep(1500);
	}

	public void editVehicleDetails(String vehicleType) throws Throwable {
		try {
		library.waitForDigitLoad();
		int j=1;
		String[] vehType_InsuType = vehicleType.split("_");
		library.scrollElementIntoView(checkBoxField.get(j), library.getDriver());
		checkBoxField.get(j).click();
		library.scrollElementIntoView(editButton.get(row), library.getDriver());
		editButton.get(row).click();
		System.out.println("Editing the data in row : "+row);
		switch(vehType_InsuType[0])
		{
		case "2W LOADER":
			switch(vehType_InsuType[1].toUpperCase())
			{
			case "OD NEW":
				System.out.println("Entering new vehicle data");
				edit_RegNum_EngNum_ChsNum_2W();
				text2WAddress1.clear();
				text2WAddress1.sendKeys("2WOD NEW LOADER");
				text2WPolSrtDate.clear();
				text2WPolSrtDate.sendKeys(library.getDate("[TODAY]", "dd-MMM-YYYY"));
				txtRegManDate.clear();
				txtRegManDate.sendKeys(library.getDate("[TODAY-30]", "dd-MMM-YYYY"));
				txtNewOrExisting.clear();
				txtNewOrExisting.sendKeys("N");
				Thread.sleep(1500);
				regNoTxtBox.clear();
				regNoTxtBox.sendKeys("KA02");
				buttonSave.click();
				Thread.sleep(2000);
				break;
			case "OD ROLLOVER":
				System.out.println("Entering rollover vehicle data");
				edit_RegNum_EngNum_ChsNum_2W();
				text2WAddress1.clear();
				text2WAddress1.sendKeys("2WOD ROLL LOADER");
				text2WPolExpDate.clear();
				text2WPolExpDate.sendKeys(library.getDate("[TODAY+1]", "dd-MMM-YYYY"));
				txtNewOrExisting.clear();
				txtNewOrExisting.sendKeys("U");
				regNoTxtBox.clear();
				regNoTxtBox.sendKeys("KA02ZB"+library.getRandomValue("N | 4"));
				Thread.sleep(1500);
				buttonSave.click();
				Thread.sleep(2000);
				break;
			case "OD BREAKIN":
				System.out.println("Entering breakin vehicle data");
				edit_RegNum_EngNum_ChsNum_2W();
				text2WAddress1.clear();
				text2WAddress1.sendKeys("2WOD BREAKIN LOADER");
				text2WPolExpDate.clear();
				text2WPolExpDate.sendKeys(library.getDate("[TODAY-1]", "dd-MMM-YYYY"));
				txtNewOrExisting.clear();
				txtNewOrExisting.sendKeys("U");
				regNoTxtBox.clear();
				regNoTxtBox.sendKeys("KA02NZ"+library.getRandomValue("N | 4"));
				Thread.sleep(1500);
				buttonSave.click();
				Thread.sleep(2000);
				}
			break;	
		case "4W LOADER":
			switch(vehType_InsuType[1].toUpperCase())
			{
			case "OD NEW":
				System.out.println("Entering new vehicle data");
				edit_RegNum_EngNum_ChsNum_2W();
				text2WAddress1.clear();
				text2WAddress1.sendKeys("4WOD NEW LOADER");
				text2WPolSrtDate.clear();
				text2WPolSrtDate.sendKeys(library.getDate("[TODAY]", "dd-MMM-YYYY"));
				txtRegManDate.clear();
				txtRegManDate.sendKeys(library.getDate("[TODAY-30]", "dd-MMM-YYYY"));
				txtNewOrExisting.clear();
				txtNewOrExisting.sendKeys("N");
				Thread.sleep(1500);
				regNoTxtBox.clear();
				regNoTxtBox.sendKeys("KA02");
				buttonSave.click();
				Thread.sleep(2000);
				break;
			case "OD ROLLOVER":
				System.out.println("Entering rollover vehicle data");
				edit_RegNum_EngNum_ChsNum_2W();
				text2WAddress1.clear();
				text2WAddress1.sendKeys("4WOD ROLL LOADER");
				text2WPolExpDate.clear();
				text2WPolExpDate.sendKeys(library.getDate("[TODAY+1]", "dd-MMM-YYYY"));
				txtNewOrExisting.clear();
				txtNewOrExisting.sendKeys("U");
//				txtIDV.clear();
//				txtIDV.sendKeys("25000");
				regNoTxtBox.clear();
				regNoTxtBox.sendKeys("KA02ZB"+library.getRandomValue("N | 4"));
				Thread.sleep(1500);
				buttonSave.click();
				Thread.sleep(2000);
				break;
			case "OD BREAKIN":
				System.out.println("Entering breakin vehicle data");
				edit_RegNum_EngNum_ChsNum_2W();
				text2WAddress1.clear();
				text2WAddress1.sendKeys("4WOD BREAKIN LOADER");
				text2WPolExpDate.clear();
				text2WPolExpDate.sendKeys(library.getDate("[TODAY-1]", "dd-MMM-YYYY"));
				txtNewOrExisting.clear();
				txtNewOrExisting.sendKeys("U");
//				txtIDV.clear();
//				txtIDV.sendKeys("25000");
				regNoTxtBox.clear();
				regNoTxtBox.sendKeys("KA02NZ"+library.getRandomValue("N | 4"));
				Thread.sleep(1500);
				buttonSave.click();
				Thread.sleep(2000);
				}
			break;	
		case "CV LOADER":
			switch(vehType_InsuType[1].toUpperCase())
			{
			case "OD NEW":
				System.out.println("Entering new vehicle data");
				edit_RegNum_EngNum_ChsNum_CV();
				textCVAddress1.clear();
				textCVAddress1.sendKeys("CVOD NEW LOADER");
				textCVPolSrtDate.clear();
				textCVPolSrtDate.sendKeys(library.getDate("[TODAY]", "dd-MMM-YYYY"));
				txtCVRegManDate.clear();
				txtCVRegManDate.sendKeys(library.getDate("[TODAY-30]", "dd-MMM-YYYY"));
				txtCVNewOrExisting.clear();
				txtCVNewOrExisting.sendKeys("N");
				Thread.sleep(1500);
				CV_RegNum_1.clear();
				CV_RegNum_1.sendKeys("KA");
				CV_RegNum_2.clear();
				CV_RegNum_2.sendKeys("02");
				CV_RegNum_3.clear();
				CV_RegNum_3.sendKeys(library.getRandomValue("A | 2").toUpperCase());
				CV_RegNum_4.clear();
				CV_RegNum_4.sendKeys(library.getRandomValue("N | 4"));
				buttonSave.click();
				Thread.sleep(2000);
				break;
			case "OD ROLLOVER":
				System.out.println("Entering rollover vehicle data");
				edit_RegNum_EngNum_ChsNum_CV();
				textCVAddress1.clear();
				textCVAddress1.sendKeys("CVOD ROLL LOADER");
				textCVPolExpDate.clear();
				textCVPolExpDate.sendKeys(library.getDate("[TODAY+1]", "dd-MMM-YYYY"));
				txtCVNewOrExisting.clear();
				txtCVNewOrExisting.sendKeys("U");
				CV_RegNum_1.clear();
				CV_RegNum_1.sendKeys("KA");
				CV_RegNum_2.clear();
				CV_RegNum_2.sendKeys("04");
				CV_RegNum_3.clear();
				CV_RegNum_3.sendKeys(library.getRandomValue("A | 2").toUpperCase());
				CV_RegNum_4.clear();
				CV_RegNum_4.sendKeys(library.getRandomValue("N | 4"));
				Thread.sleep(1500);
				buttonSave.click();
				Thread.sleep(2000);
				break;
			case "OD BREAKIN":
				System.out.println("Entering breakin vehicle data");
				edit_RegNum_EngNum_ChsNum_CV();
				textCVAddress1.clear();
				textCVAddress1.sendKeys("CVOD BREAKIN LOADER");
				textCVPolExpDate.clear();
				textCVPolExpDate.sendKeys(library.getDate("[TODAY-1]", "dd-MMM-YYYY"));
				txtCVNewOrExisting.clear();
				txtCVNewOrExisting.sendKeys("U");
				CV_RegNum_1.clear();
				CV_RegNum_1.sendKeys("KA");
				CV_RegNum_2.clear();
				CV_RegNum_2.sendKeys("04");
				CV_RegNum_3.clear();
				CV_RegNum_3.sendKeys(library.getRandomValue("A | 2").toUpperCase());
				CV_RegNum_4.clear();
				CV_RegNum_4.sendKeys(library.getRandomValue("N | 4"));
				Thread.sleep(1500);
				buttonSave.click();
				Thread.sleep(2000);
				}
			break;	
		default:
			Reporter.log("Given option is not there in the dropdown");
		}
		row++;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public void edit_RegNum_EngNum_ChsNum_2W() throws Throwable {
		Thread.sleep(3000);

		library.waitForElementVisible(text2WChasis, 20, 5);
		text2WChasis.clear();
		text2WChasis.sendKeys(library.getRandomValue("AN | 17").toUpperCase());

		library.waitForElementVisible(text2WEngine, 20, 5);
		text2WEngine.clear();
		text2WEngine.sendKeys(library.getRandomValue("AN | 15").toUpperCase());

	}
	
	public void edit_RegNum_EngNum_ChsNum_CV() throws Throwable
	{
		Thread.sleep(3000);
		
		library.waitForElementVisible(textCVChasis, 20, 5);
		textCVChasis.clear();
		textCVChasis.sendKeys(library.getRandomValue("AN | 17").toUpperCase());
		
		library.waitForElementVisible(textCVEngine, 20, 5);
		textCVEngine.clear();
		textCVEngine.sendKeys(library.getRandomValue("AN | 15").toUpperCase());
		
	}
	
	public void clickQuickQuotebtn()
	{
		try 
		{
			quickQuoteBtn.click();
			library.waitForLoaderLoading();
			List<WebElement> qqPremium = new LinkedList<>();
			qqPremium = library.getDriver().findElements(
					By.xpath("//div[@class='tableBodyContainer']//div[" + getColumnGrid("Total Premium") + "]"));
			System.out.println("qqPremium size : "+qqPremium.size());
			//for (int i = 1; i <= qqPremium.size(); i++)
				for (int i = 1; i <= row; i++)
				loaderMap.put("QuickQuotePremium_" + i, qqPremium.get(i - 1).getText().trim());
			System.out.println("Premium Map after Quick Quote : " + loaderMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void clickGetQuotebtn() {
		try {
			int total_executed_rows = row;
			System.out.println("Total executed rows is " + total_executed_rows);
			for (int k = 0; k < total_executed_rows; k++) {
				System.out.println(k + "=======>" + loaderComments.get(k).getText().trim());
				switch (loaderComments.get(k).getText().trim()) {
				case "Get Quote Failed":
					break;
				case "Quick Quote Success":
					Library library = new Library();
					WebDriver driver = library.getDriver();
					System.out.println("checking checkbox" + k);
					WebElement element = driver.findElement(
							By.xpath("((//div[@class='tableBodyContainer'])/div/div/div[" + getColumnGrid("Comment")
									+ "]/span)[" + (k + 1) + "]/ancestor::div[contains(@class,'row')]/div/div/i"));
					library.scrollElementIntoView(element, library.getDriver());
					element.click();
					System.out.println("checking checkbox" + k);

					break;
				default:
					System.out.println("Loader comments are " + loaderComments.get(k).getText().trim());
					System.out.println("No options seen " + k);

				}
			}

			getQuoteBtn.click();
			library.waitForLoaderLoading();
			List<WebElement> gqPremium = new LinkedList<>();
			gqPremium = library.getDriver().findElements(
					By.xpath("//div[@class='tableBodyContainer']//div[" + getColumnGrid("Total Premium") + "]"));
			for (int i = 1; i <= row; i++)
				loaderMap.put("GetQuotePremium_" + i, gqPremium.get(i - 1).getText().trim());
			System.out.println("Premium Map after Get Quote : " + loaderMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void clickIssuePolicyButton() throws Exception {
		try {
			int total_executed_rows = row;
			if (loaderCommentsAfterGetQuote.size() > 0) {
				for (int k = 0; k < total_executed_rows; k++) {
					switch (loaderCommentsAfterGetQuote.get(k).getText().trim()) {
					case "Get Quote Success":
						Library library = new Library();
						WebDriver driver = library.getDriver();
						System.out.println("checking checkbox" + k);
						WebElement element = driver.findElement(
								By.xpath("((//div[@class='tableBodyContainer'])/div/div/div[" + getColumnGrid("Comment")
										+ "]/span)[" + (k + 1) + "]/ancestor::div[contains(@class,'row')]/div/div/i"));
						if (driver.findElement(By.xpath("((//div[@class='tableBodyContainer'])/div/div/div["
								+ getColumnGrid("Comment") + "]/span)[" + (k + 1)
								+ "]/ancestor::div[contains(@class,'row')]/div/div/i//ancestor::div[@class='fullWidth']//div[5]"))
								.getText().contains("Break-in"))
							continue;
						library.scrollElementIntoView(element, library.getDriver());
						element.click();
						System.out.println("checking checkbox" + k);

						break;
					default:
						System.out.println("Loader comments are " + loaderComments.get(k).getText().trim());
						System.out.println("No options seen " + k);
					}

				}
				btnIssuePolicy.click();
				library.waitForLoaderLoading();
			}
			List<WebElement> icPremium = new LinkedList<>();
			icPremium = library.getDriver().findElements(
					By.xpath("//div[@class='tableBodyContainer']//div[" + getColumnGrid("Total Premium") + "]"));
			for (int i = 1; i <= row; i++)
				loaderMap.put("IssueContractPremium_" + i, icPremium.get(i - 1).getText().trim());
			System.out.println("Premium Map : " + loaderMap);
			getPolicyDetails();
			row=0;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	private void getPolicyDetails() {
		try {
			int total_executed_rows = row;
			List<WebElement> lstPolicy = new LinkedList<>();
			lstPolicy = library.getDriver().findElements(
					By.xpath("//div[@class='tableBodyContainer']//div[" + getColumnGrid("Quote/Policy Number") + "]"));
			for (int i = 1; i <= total_executed_rows; i++)
				loaderMap.put("PolicyNumber_" + i, lstPolicy.get(i - 1).getText().trim());
			
			String loaderDrpDownValue = library.getDropDownValue(drpDownValue);
			List<WebElement> lstScenario = new LinkedList<>();
			if(loaderDrpDownValue.equals("Two Wheeler") || loaderDrpDownValue.equals("Private Car"))
			{
			lstScenario = library.getDriver().findElements(
					By.xpath("//div[@class='tableBodyContainer']//div[" + getColumnGrid("Address Line 1") + "]"));
			}
			else if(loaderDrpDownValue.equals("Commercial Vehicle Comprehensive"))
			{
				lstScenario = library.getDriver().findElements(
						By.xpath("//div[@class='tableBodyContainer']//div[" + getColumnGrid("ADDRESS1") + "]"));
			}
			for (int i = 1; i <= total_executed_rows; i++)
			{
				loaderMap.put("Scenario_" + i, lstScenario.get(i - 1).getText().trim());
				System.out.println("The Text in the Address field is"+lstScenario.get(i - 1).getText().trim());
			}
			List<WebElement> lstComments = new LinkedList<>();
			lstComments = library.getDriver().findElements(
					By.xpath("//div[@class='tableBodyContainer']//div[" + getColumnGrid("Comment") + "]"));
			for (int i = 1; i <= total_executed_rows; i++)
				loaderMap.put("Comment_" + i, lstComments.get(i - 1).getText().trim());
			List<WebElement> lstErrorSummary = new LinkedList<>();
			lstErrorSummary = library.getDriver().findElements(
					By.xpath("//div[@class='tableBodyContainer']//div[" + getColumnGrid("Error Summary") + "]"));
			for (int i = 1; i <= total_executed_rows; i++)
				loaderMap.put("Error_" + i, lstErrorSummary.get(i - 1).getText().trim());
			int counter = 0;
			for (WebElement temp : library.getDriver().findElements(
					By.xpath("//div[@class='tableBodyContainer']//div[" + getColumnGrid("Comment") + "]")))
				if (!temp.getText().trim().equals(""))
					counter++;
			loaderMap.put("LoaderCount", "" + row);
			// div[@class='tableBodyContainer']//div[12]
			System.out.println("Premium Map : " + loaderMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Integer getColumnGrid(String columnName) {
		int counter = 0;
		for (WebElement temp : library.getDriver()
				.findElements(By.xpath("//div[@class='row margin-none fullWidth']/div/div"))) {
			counter++;
			if (temp.getText().equals(columnName))
				return counter;
		}
		return -1;
	}

	public HashMap<String, String> getLoaderMap() {
		return loaderMap;
	}

}
