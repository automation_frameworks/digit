package pom.agent.motor;

import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.utilities.Library;
import com.utilities.PropertyFileReader;

public class DetailsPage {

	public com.utilities.Library library = new com.utilities.Library();

	@FindBy(id = "vehicleFlow")
	private WebElement drpVehicleType;
	
	@FindBy(xpath="//label[.='Half-Yearly Mode']/parent::div/input[@type='checkbox']")
	private WebElement checkBoxHalfYearly;

	@FindBy(xpath = "//select[@id='vehicleFlow']/../following-sibling::div/select")
	private WebElement drpPolicyType;

	@FindBy(id = "vehicleWithRegistration")
	private WebElement radVehicleWithRegistration;

	@FindBy(id = "brandNewVehicle")
	private WebElement radBrandNewVehicle;

	@FindBy(id = "selectRegistrationNumber")
	public WebElement txtRegistrationNumber;

	@FindBy(xpath = "//label[@for='selectRegistrationNumber']")
	private WebElement lblRegistrationNumber;

	@FindBy(id = "selectedTwoWheelerType")
	private WebElement drpTwoWheelerType;

	@FindBy(id = "vehicleCat")
	private WebElement drpVehicleCategory;

	@FindBy(id = "selectPermitType")
	private WebElement drpPermitType;

	@FindBy(xpath = "//ng-autocomplete[@id='UsageType']//input")
	private WebElement drpUsageType;

	@FindBy(xpath = "//ng-autocomplete[@id='BodyType']/descendant::input")
	private WebElement drpBodyType;

	@FindBy(xpath = "//ng-autocomplete[@id='selectVehicleMake']//input")
	private WebElement drpVehicleMake;

	@FindBy(xpath = "//ng-autocomplete[@id='selectVehicleModel']//input")
	private WebElement drpVehicleModel;

	@FindBy(xpath = "//ng-autocomplete[@id='selectVehicleVariant']//input")
	private WebElement drpVehicleVariant;

	@FindBy(xpath = "//ng-autocomplete[@id='monthYear']//input")
	private WebElement drpRegistrationMonth;

	@FindBy(xpath = "//ng-autocomplete[@id='regYear']//input")
	private WebElement drpRegistrationYear;

	@FindBy(xpath = "//ng-autocomplete[@id='manufactureYear']//input")
	private WebElement drpManufacturingYear;

	@FindBy(xpath = "//ng-autocomplete[@id='residenceCity']//input")
	private WebElement drpResidenceCity;

	@FindBy(xpath = "//ng-autocomplete[@id='pincode']//input")
	private WebElement drpPinCode;

	@FindBy(id = "vehicleOwnership")
	private WebElement drpVehicleOwnership;

	@FindBy(id = "chassisNumber")
	private WebElement txtChassisNumber;

	@FindBy(xpath = "//button[contains(.,'Duplicate print')]")
	private WebElement btnDuplictaePrint;

	public String getTxtChassisNumber() {
		return txtChassisNumber.getText();
	}

	public String getTxtEngineNumber() {
		return txtEngineNumber.getText();
	}

	public String getbtnDuplictaePrint() {
		return btnDuplictaePrint.getText();
	}

	@FindBy(id = "engineNumber")
	private WebElement txtEngineNumber;

	@FindBy(id = "partnerReferenceNumber")
	private WebElement txtPartnerReferenceNumber;

	@FindBy(id = "dontKnowPrevPolicyDetails")
	private WebElement chkDontKnowPrevPolicyDetails;

	@FindBy(name = "policyExpiryDate")
	private WebElement txtpolicyExpiryDate;

	@FindBy(xpath = "//ng-autocomplete[@id='insurerName']//input")
	private WebElement drpPreviousInsurerName;

	@FindBy(id = "policynumber")
	private WebElement txtPreviousPolicyNumber;

	@FindBy(name = "prevPolicyNcb")
	private WebElement drpExistingPolicyNCB;

	@FindBy(id = "currentPolicy")
	private WebElement radAnyClaimYes;

	@FindBy(id = "changeCurrentPolicy")
	private WebElement radAnyClaimNo;

	@FindBy(xpath = "//div[contains(text(),'TP only policy')]/../div")
	private WebElement radTPOnlyPolicy;

	@FindBy(xpath = "//div[contains(text(),'Comprehensive policy without Zero Dep')]/../div")
	private WebElement radCompWithoutZeroDep;

	@FindBy(xpath = "//div[contains(text(),'Comprehensive policy with Zero Dep')]/../div")
	private WebElement radCompWithZeroDep;

	@FindBy(xpath = "//div[contains(text(),'Comprehensive Policy with Zero Dep and EP')]/../div")
	private WebElement radCompWithZeroDepAndEP;

	@FindBy(id = "tpPolicyStatus")
	private WebElement chkTPPolicyStatus;

	@FindBy(name = "thirdPartyPolicyStartDate")
	private WebElement txtTPPolicyStartDate;

	@FindBy(name = "thirdPartyPolicyEndDate")
	private WebElement txtTPPolicyEndDate;

	@FindBy(xpath = "//ng-autocomplete[@id='thirdPartyInsurerName']//input")
	private WebElement drpTPInsurerName;

	@FindBy(id = "tpPolicynumber")
	private WebElement txtTPPolicyNumber;

	@FindBy(xpath = "//button[contains(text(),'Get Quote')]")
	private WebElement btnGetQuote;

	@FindBy(xpath = "//button[contains(text(),'Recalculate')]")
	private WebElement btnRecalculate;

	@FindBy(xpath = "//button[contains(text(),'Save Quote')]")
	private WebElement btnSaveQuote;

	@FindBy(xpath = "//div[@class='col-12 minHeight']//button[contains(text(),'Submit')]")
	private WebElement btnSubmit;

	@FindBy(xpath = "//div[@class='col-12 minHeight']//button[contains(text(),'Generate Policy')]")
	private WebElement btnGeneratePolicy;

	@FindBy(xpath = "//input[@value='Filter']")
	private WebElement filterBtn;

	@FindBy(xpath = "//div[@id='cvodPCVconfirmModal']//button[text()='Yes']")
	private WebElement btnCVODConfirmOK;

	@FindBy(id = "setFirstName")
	private WebElement txtFirstName;

	@FindBy(id = "setCompanyName")
	private WebElement txtCompanyName;

	@FindBy(id = "setLastname")
	private WebElement txtLastName;

	@FindBy(id = "mobileNumber")
	public WebElement txtMobileNumber;

	@FindBy(id = "emailAddress")
	private WebElement txtEmailAddress;

	@FindBy(id = "setAddress1")
	private WebElement txtAddressLine1;

	@FindBy(id = "setAddress2")
	private WebElement txtAddressLine2;

	@FindBy(id = "aadharNumber")
	private WebElement txtAadharNumber;

	@FindBy(id = "gst")
	private WebElement txtGSTIN;

	@FindBy(id = "vehicleColor")
	private WebElement txtVehicleColour;

	@FindBy(id = "hypo")
	private WebElement txtHypothecation;

	@FindBy(id = "additionalDetails")
	private WebElement txtAdditionalDetails;

	@FindBy(id = "nomineeN")
	private WebElement txtNomineeName;

	@FindBy(xpath = "//*[@id='nomineeRela']//input")
	private WebElement drpNomineeRelation;

	@FindBy(name = "nomineeDate")
	private WebElement txtnomineeDate;

	@FindBy(id = "selectPaymentType")
	private WebElement drpPaymentType;

	@FindBy(id = "panNumber")
	private WebElement txtPanNumber;

	@FindBy(id = "setChequeNumber")
	private WebElement txtChequeNumber;

	@FindBy(name = "chequeDate")
	private WebElement txtChequeDate;

	@FindBy(id = "setmicr")
	private WebElement txtIFSCMICRCode;

	@FindBy(id = "selectSurveyType")
	private WebElement drpSurveyType;

	@FindBy(id = "sEmail")
	private WebElement txtSurveyEmail;

	@FindBy(id = "sMobile")
	private WebElement txtSurveyMobileNumber;

	@FindBy(xpath = "//label[text()='Upload previous policy copy']/../input")
	private WebElement inpUploadDocument;

	@FindBy(xpath = "//button[text()='Download Quote']")
	private WebElement btnDownloadQuote;

	@FindBy(xpath = "//div[@class='col-auto hidebuttons ng-star-inserted']//div//i[text()='file_download']")
	private WebElement fileDownoad;

	@FindBy(xpath = "//button[text()='Download Policy for Email']")
	private WebElement btnDownloadPolicyForEmail;

	@FindBy(xpath = "//button[text()='Download Policy for Print']")
	private WebElement btnDownloadPolicyForPrint;

	@FindBy(xpath = "//div[@class='hero bg-primary']//h1")
	private WebElement lblSuccessMessage;

	@FindBy(xpath = "//label[text()='Policy Number' or text()='Quote Number']/following-sibling::input")
	private WebElement txtPolicyNumber;

	@FindBy(xpath = "//span[@class='premium']")
	private WebElement premiumtxt;

	public DetailsPage(WebDriver driver) {
		driver = library.getDriver();
		PageFactory.initElements(driver, this);
	}

	public String getRegistartionNumber() {
		return library.getDriver().findElement(By.id("selectRegistrationNumber")).getAttribute("value");
	}

	public String getMobileNumber() {
		return txtMobileNumber.getAttribute("value");
	}

	public void enterVehicleDetails(String details) throws Exception {
		try {
			library.waitForElementVisible(drpVehicleType, 15, 500);
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "vehicletype":
					System.out.println("Vehicle type : " + hmap.get(key));
					library.selectDropDownValue(drpVehicleType, hmap.get(key));
					break;
				case "policytype":
					System.out.println("Vehicle type : " + hmap.get(key));
					library.selectDropDownValue(drpPolicyType, hmap.get(key));
					break;
				case "vehiclewithregistration":
					if (hmap.get(key).equalsIgnoreCase("true"))
						radVehicleWithRegistration.click();
					break;
				case "brandnewvehicle":
					if (hmap.get(key).equalsIgnoreCase("true"))
						radBrandNewVehicle.click();
					break;
				case "registrationnumber":
				case "rtocode":
					txtRegistrationNumber.sendKeys(hmap.get(key));
					break;
				case "twowheelertype":
					library.selectDropDownValue(drpTwoWheelerType, hmap.get(key));
					break;
				case "vehiclecategory":
					library.selectDropDownValue(drpVehicleCategory, hmap.get(key));
					break;
				case "permittype":
					library.selectDropDownValue(drpPermitType, hmap.get(key));
					break;
				case "usagetype":
					library.selectDropDownValue(drpUsageType, hmap.get(key));
					break;
				case "bodytype":
					library.selectDropDownValue(drpBodyType, hmap.get(key));
					break;
				case "vehiclemake":
					library.selectDropDownValue(drpVehicleMake, hmap.get(key));
					break;
				case "vehiclemodel":
					library.selectDropDownValue(drpVehicleModel, hmap.get(key));
					break;
				case "vehiclevariant":
					Thread.sleep(5000);
					library.waitForDigitLoad();
					library.selectDropDownValue(drpVehicleVariant, hmap.get(key));
					break;
				case "registrationyear":
					library.selectDropDownValue(drpRegistrationYear, hmap.get(key));
					break;
				case "registrationmonth":
					library.selectDropDownValue(drpRegistrationMonth, hmap.get(key));
					break;
				case "manufacturingyear":
					library.selectDropDownValue(drpManufacturingYear, hmap.get(key));
					break;
				case "residencecity":
					library.selectDropDownValue(drpResidenceCity, hmap.get(key));
					break;
				case "pincode":
					library.selectDropDownValue(drpPinCode, hmap.get(key));
					break;
				case "vehicleownership":
					library.selectDropDownValue(drpVehicleOwnership, hmap.get(key));
					break;
				case "chassisnumber":
					library.selectDropDownValue(txtChassisNumber, hmap.get(key));
					txtChassisNumber.sendKeys(Keys.TAB);
					library.waitForDigitLoad();
					break;
				case "enginenumber":
					library.selectDropDownValue(txtEngineNumber, hmap.get(key));
					txtEngineNumber.sendKeys(Keys.TAB);
					library.waitForDigitLoad();
					break;
				case "partnerreferencenumber":
					library.selectDropDownValue(txtPartnerReferenceNumber, hmap.get(key));
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}
			enterMissingVehicleDetails(details);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the vehicle details : " + e.getMessage());
		}
	}

	public void enterPreviousPolicyDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "don'tknowpreviouspolicydetails":
					txtPreviousPolicyNumber.sendKeys("");
					if (hmap.get(key).equalsIgnoreCase("true"))
						chkDontKnowPrevPolicyDetails.click();
					break;
				case "previouspolicyexpirydate":
					library.enterDate(txtpolicyExpiryDate, hmap.get(key));
					break;
				case "previousinsurername":
					library.selectDropDownValue(drpPreviousInsurerName, hmap.get(key));
					break;
				case "previouspolicynumber":
					txtPreviousPolicyNumber.sendKeys(hmap.get(key));
					break;
				case "existingpolicyncb":
					library.selectDropDownValue(drpExistingPolicyNCB, hmap.get(key));
					break;
				case "anyclaimlastyear":
					if (hmap.get(key).equalsIgnoreCase("yes"))
						radAnyClaimYes.click();
					else
						radAnyClaimNo.click();
					break;
				case "previouspolicytype":
					txtPreviousPolicyNumber.sendKeys("");
					library.waitForDigitLoad();
					library.getDriver()
							.findElement(By.xpath("//div[contains(text(),'" + hmap.get(key).trim() + "')]/../div"))
							.click();
					break;
				}
			}
			if (!chkDontKnowPrevPolicyDetails.isSelected())
				enterMissingPreviousPolicyDetails(details);

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the previous policy details : " + e.getMessage());
		}
	}

	public void enterThirdPartyPolicyDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "thirdpartypolicystatus":
				case "tppolicystatus":
					txtTPPolicyStartDate.sendKeys("");
					if (hmap.get(key).equalsIgnoreCase("active"))
						;
					else
						chkTPPolicyStatus.click();
					break;
				case "thirdpartypolicystartdate":
				case "tppolicystartdate":
				case "startdate":
					library.enterDate(txtTPPolicyStartDate, hmap.get(key));
					break;
				case "thirdpartypolicyenddate":
				case "tppolicyenddate":
				case "enddate":
					library.enterDate(txtTPPolicyEndDate, hmap.get(key));
					break;
				case "thirdpartypolicynumber":
				case "tppolicynumber":
					txtTPPolicyNumber.sendKeys(hmap.get(key));
					break;
				case "thirdpartyinsurername":
				case "tpinsurername":
					library.selectDropDownValue(drpTPInsurerName, hmap.get(key));
					break;
				}
			}
			if (chkTPPolicyStatus.isSelected())
				enterMissingTPPolicyDetails(details);

		} catch (Exception e) {
			e.printStackTrace();
			// throw new Exception("Error while entering the previous policy details : " +
			// e.getMessage());
		}
	}

	public void enterCustomerDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "firstname":
					txtFirstName.sendKeys(hmap.get(key));
					Library.staticMap.put("Insurer Name", hmap.get(key));
					break;
				case "lastname":
					txtLastName.sendKeys(hmap.get(key));
					break;
				case "companyname":
					txtCompanyName.sendKeys(hmap.get(key));
					Library.staticMap.put("Insurer Name", hmap.get(key));
					break;
				case "mobilenumber":
					txtMobileNumber.sendKeys(hmap.get(key));
					break;
				case "emailaddress":
					txtEmailAddress.sendKeys(hmap.get(key));
					break;
				case "addressline1":
					txtAddressLine1.sendKeys(hmap.get(key));
					break;
				case "addressline2":
					txtAddressLine2.sendKeys(hmap.get(key));
					break;
				case "aadharnumber":
					txtAadharNumber.sendKeys(hmap.get(key));
					break;
				case "gstin":
					txtGSTIN.sendKeys(hmap.get(key));
					break;
				case "vehiclecolour":
					txtVehicleColour.sendKeys(hmap.get(key));
					break;
				case "hypothecation":
					txtHypothecation.sendKeys(hmap.get(key));
					break;
				case "additionaldetails":
					txtAdditionalDetails.sendKeys(hmap.get(key));
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}
			enterMissingCustomerDetails(details);

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the customer details : " + e.getMessage());
		}
	}

	public void enterNomineeDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "nomineename":
					txtNomineeName.sendKeys(hmap.get(key));
					txtHypothecation.click();
					break;
				case "nomineerelation":
					library.selectDropDownValue(drpNomineeRelation, hmap.get(key));
					break;
				case "nomineedob":
					library.enterDate(txtnomineeDate, hmap.get(key));
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the nominee details : " + e.getMessage());
		}
	}

	public void enterSurveyDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "surveytype":
					library.selectDropDownValue(drpSurveyType, hmap.get(key));
					break;
				case "email":
					txtSurveyEmail.sendKeys(hmap.get(key));
					break;
				case "mobilenumber":
				case "mobile":
					txtSurveyMobileNumber.sendKeys(hmap.get(key));
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the survey details : " + e.getMessage());
		}
	}

	public void enterPaymentDetails() throws Exception {
		String environment;

		if (System.getProperty("environment") == null || System.getProperty("environment").equals("")) {
			PropertyFileReader prop = new PropertyFileReader();
			environment = prop.getEnvironment().toString();
		} else
			environment = System.getProperty("environment");

		if (environment.equalsIgnoreCase("prod")) {
			library.selectDropDownValue(drpPaymentType, "Generate Payment Link");
		} else {
			library.selectDropDownValue(drpPaymentType, "Agent Float");
		}
		try {
			txtPanNumber.sendKeys("BZGPP8820B");
		} catch (Exception e) {

		}
	}

	public void selectAllCovers() throws Exception {
		try {
			library.waitForDigitLoad();
			Thread.sleep(2000);
			List<WebElement> lstCovers = library.getDriver().findElements(
					By.xpath("//h1[text()='Cover Details']/..//div[contains(@class,'coverageHeader')]//input"));
			
			for (WebElement temp : lstCovers) {
				try {
					txtMobileNumber.sendKeys("");
					if (temp.isEnabled() && !temp.isSelected())
						temp.click();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			List<WebElement> lstSubCovers = library.getDriver()
					.findElements(By.xpath("//div[@class='col-7 leftalign1']//input"));
			for (WebElement temp : lstSubCovers) {
				try {
					txtMobileNumber.sendKeys("");
					if (temp.isEnabled() && !temp.isSelected())
						temp.click();
						if(temp.getAttribute("ng-reflect-name").equals("Parts Depreciation Protect_che"))
							library.waitForDigitLoad();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			List<WebElement> lstCountries =  library.getDriver().findElements(
					By.xpath("//input[contains(@id,'subCover_country_checkbox_countryInd')]"));
			for (WebElement temp : lstCountries) {
				try {
					txtMobileNumber.sendKeys("");
					if (temp.isEnabled() && !temp.isSelected())
						temp.click();
						if(temp.getAttribute("ng-reflect-name").equals("Parts Depreciation Protect_che"))
							library.waitForDigitLoad();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while selecting the coverages : " + e.getMessage());
		}
	}

	public void performAction(String action) throws Exception {
		try {
			library.waitForDigitLoad();

			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "getquote":
				btnGetQuote.click();
				WebDriver driver = library.getDriver();
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				int counter = 0;
				while (driver
						.findElements(
								By.xpath("//div[@id='toast-container' and contains(@style,'position: fixed;')]//span"))
						.size() > 0) {
					Thread.sleep(5000);
					try {
						counter++;
						btnGetQuote.click();
						if (counter >= 3)
							break;
					} catch (Exception e) {
					}
				}
				break;
			case "recalculate":
				WebDriverWait wait = new WebDriverWait(library.getDriver(), 30, 1000);
				wait.until(ExpectedConditions.elementToBeClickable(btnRecalculate));
				library.waitForDigitLoad();
				btnRecalculate.click();
				Thread.sleep(3000);
				library.waitForDigitLoad();
				Library.staticMap.put("Recalculated premium", premiumtxt.getText().trim().split(" ")[1]);
				System.out.println("Recalculated premium: " + premiumtxt.getText().trim().split(" ")[1]);
				break;
			case "savequote":
				btnSaveQuote.click();
				Thread.sleep(4000);
				library.waitForDigitLoad();
				Library.staticMap.put("Save Quote premium", premiumtxt.getText().trim().split(" ")[1]);
				System.out.println("Save Quote premium: " + premiumtxt.getText().trim().split(" ")[1]);
				break;
			case "submit":
				btnSubmit.click();
				Thread.sleep(3000);
				break;
			case "generatepolicy":
				btnGeneratePolicy.click();
				try {
					library.waitForElementVisible(btnCVODConfirmOK, 2, 500);
				} catch (TimeoutException te) {
				}
				if (btnCVODConfirmOK.isDisplayed())
					btnCVODConfirmOK.click();
				library.waitForDigitLoad();
				break;
			case "downloadquote":
				btnDownloadQuote.click();
				break;
			case "downloadpolicyforemail":
				if (library.setEnvironment().equalsIgnoreCase("prod")) {
					btnDownloadQuote.click();
					System.out.println("Download quote button is clicked");
					break;
				} else {
					try {
						btnDownloadQuote.click();
					} catch (NoSuchElementException ne) {
						try {
							btnDownloadPolicyForEmail.click();
						} catch (NoSuchElementException ne2) {
							throw new Exception("Neither Download Quote nor Download Policy button is displayed");
						}
					}
					break;
				}
			case "filter":
				filterBtn.click();
				break;
			default:
				throw new Exception("Implementation for the action '" + action + "' is not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while performing action : " + e.getMessage());
		}
	}

	public void uploadDocument() throws Exception {
		try {
			inpUploadDocument.sendKeys(
					Paths.get("").toAbsolutePath().toString() + "/src/main/resources/documents/download.jpeg");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while uploading document : " + e.getMessage());
		}
	}

	public void verifySuccessPage(String policyStatus, String vehicleType) throws Exception {
		try {
			Thread.sleep(4000);
			library.waitForDigitLoad();
			String successMessage = lblSuccessMessage.getText();
			System.out.println("SucessMsg: " + successMessage);
			System.out.println("Policy State: " + policyStatus);
			System.out.println("Policy Number: " + txtPolicyNumber.getAttribute("value"));
			Library.staticMap.put("Policy Number", txtPolicyNumber.getAttribute("value"));
			// if (!library.setEnvironment().equalsIgnoreCase("prod")) {
			if (!lblSuccessMessage.isDisplayed())
				throw new Exception("Success message is not displayed!!!");
			// successMessage = lblSuccessMessage.getText();
			String vehicleString = "";
			switch (vehicleType) {
			case "Four Wheeler":
				vehicleString = "Car";
				break;
			case "Two Wheeler":
				vehicleString = "two wheeler";
				break;
			case "Commercial Vehicle":
				vehicleString = "Vehicle";
				break;
			}
			System.out.println("Expected Succcess message : " + Library.staticMap.get("Insurer Name") + "’s "
					+ vehicleString + " is now insured!");
			if (successMessage.trim().toLowerCase().contains(
					(Library.staticMap.get("Insurer Name") + "’s " + vehicleString + " is now insured!").toLowerCase())
					|| successMessage.trim().equals("Information about your Quote"))
				System.out.println("Success Message is displayed : =====> " + successMessage);
			else
				throw new Exception("Message displayed <<" + successMessage + ">> is not as expected");

			// if (policyStatus.equals("policy"))
			// if (!successMessage.trim().contains(
			// Library.staticMap.get("Insurer Name") + "’s " + vehicleString + " is now
			// insured!"))
			// throw new Exception("Success message is not displayed correctly!!! Expected :
			// "
			// + Library.staticMap.get("Insurer Name") + "’s " + vehicleString + " is now
			// insured!"
			// + ". Actual : " + successMessage.trim());
			// else if (policyStatus.equals("quote"))
			// if (!successMessage.trim().equals("Information about your Quote"))
			// throw new Exception(
			// "Success message is not displayed correctly!!! Expected : Information about
			// your Quote. Actual : "
			// + successMessage);
			// }
			//
			// else if (!successMessage.trim().equals("Information about your Quote"))
			// throw new Exception(
			// "Success message is not displayed correctly!!! Expected : Information about
			// your Quote. Actual : "
			// + successMessage);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	private void enterMissingVehicleDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			Set<String> fields = new LinkedHashSet<>();
			for (String temp : hmap.keySet())
				fields.add(temp.replaceAll(" ", "").toLowerCase());
			LinkedList<String> mandatoryFields = getVehicleMandatoryFields();
			for (String temp : mandatoryFields) {
				if (!fields.contains(temp.replaceAll(" ", "").toLowerCase())) {
					library.waitForDigitLoad();
					switch (temp) {
					case "rtocode":
						if (lblRegistrationNumber.getText().equals("RTO Code"))
							txtRegistrationNumber.sendKeys("KA01");
						break;
					case "registrationnumber":
						if (lblRegistrationNumber.getText().equals("Registration Number"))
							txtRegistrationNumber.sendKeys("MH01ZZ" + library.getRandomValue("N | 4"));
						break;
					case "vehiclemake":
						library.selectDropDownValue(drpVehicleMake, "CHEVROLET");
						break;
					case "vehiclemodel":
						library.selectDropDownValue(drpVehicleModel, "BEAT");
						break;
					case "vehiclevariant":
						library.selectDropDownValue(drpVehicleVariant, "PS (936.0)(Petrol)(5 str)");
						break;
					case "manufacturingyear":
						if(library.getDropDownValue(drpVehicleType).equals("CVTP FIRE AND THEFT"));
						else if (radBrandNewVehicle.isSelected())
							library.selectDropDownValue(drpManufacturingYear, "2018");
						break;
					case "registrationyear":
						if (radVehicleWithRegistration.isSelected())
							library.selectDropDownValue(drpRegistrationYear, "2018");
						break;
					case "registrationmonth":
						if (radVehicleWithRegistration.isSelected())
							library.selectDropDownValue(drpRegistrationMonth, "January");
						break;
					case "residencecity":
						if (drpResidenceCity.isDisplayed())
							library.selectDropDownValue(drpResidenceCity, "Bengaluru");
						break;
					case "pincode":
						if (drpPinCode.isDisplayed())
							library.selectDropDownValue(drpPinCode, "560002");
						break;
					case "chassisnumber":
						txtChassisNumber.sendKeys(library.getRandomValue("AN | 17"));
						txtChassisNumber.sendKeys(Keys.TAB);
						library.waitForDigitLoad();
						break;
					case "enginenumber":
						txtEngineNumber.sendKeys(library.getRandomValue("AN | 15"));
						txtEngineNumber.sendKeys(Keys.TAB);
						library.waitForDigitLoad();
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	private void enterMissingPreviousPolicyDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			Set<String> fields = new LinkedHashSet<>();
			for (String temp : hmap.keySet())
				fields.add(temp.replaceAll(" ", "").toLowerCase());
			LinkedList<String> mandatoryFields = getPreviousPolicyMandatoryFields();
			for (String temp : mandatoryFields) {
				if (!fields.contains(temp.replaceAll(" ", "").toLowerCase())) {
					switch (temp) {
					case "previouspolicyexpirydate":
						library.enterDate(txtpolicyExpiryDate, "[TODAY]");
						break;
					case "previousinsurername":
						library.selectDropDownValue(drpPreviousInsurerName, "Acko General Insurance Limited");
						break;
					case "previouspolicynumber":
						Thread.sleep(500);
						txtPreviousPolicyNumber.sendKeys("ABCD1234");
						break;
					case "previouspolicytype":
						try {
							((JavascriptExecutor) library.getDriver()).executeScript("window.scrollBy(0,1000)");
							((JavascriptExecutor) library.getDriver()).executeScript("arguments[0].scrollIntoView();",
									radCompWithZeroDepAndEP);
							radCompWithZeroDepAndEP.click();
						} catch (Exception e) {
							// e.printStackTrace();
						}
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	private void enterMissingTPPolicyDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			Set<String> fields = new LinkedHashSet<>();
			for (String temp : hmap.keySet())
				fields.add(temp.replaceAll(" ", "").toLowerCase());
			LinkedList<String> mandatoryFields = getTPMandatoryFields();
			for (String temp : mandatoryFields) {
				if (!fields.contains(temp.replaceAll(" ", "").toLowerCase())) {
					switch (temp) {
					case "thirdpartypolicystartdate":
						if (fields.contains("tppolicystartdate") || fields.contains("startdate"))
							continue;
						library.enterDate(txtTPPolicyStartDate, "[TODAY]");
						break;
					case "thirdpartypolicyenddate":
						if (fields.contains("tppolicyenddate") || fields.contains("enddate"))
							continue;
						library.enterDate(txtTPPolicyEndDate, "[TODAY+366]");
						break;
					case "thirdpartypolicynumber":
						if (fields.contains("tppolicynumber"))
							continue;
						txtTPPolicyNumber.sendKeys("ABCD1234");
						break;
					case "thirdpartyinsurername":
						if (fields.contains("tpinsurername"))
							continue;
						library.selectDropDownValue(drpTPInsurerName, "Acko General Insurance Limited");
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	private void enterMissingCustomerDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			Set<String> fields = new LinkedHashSet<>();
			for (String temp : hmap.keySet())
				fields.add(temp.replaceAll(" ", "").toLowerCase());
			LinkedList<String> mandatoryFields = getCustomerMandatoryFields();
			for (String temp : mandatoryFields) {
				if (!fields.contains(temp.replaceAll(" ", "").toLowerCase())) {
					switch (temp) {
					case "firstname":
						try {
							String insurerName = library.getRandomValue("AN | 15");
							txtFirstName.sendKeys(insurerName);
							Library.staticMap.put("Insurer Name", insurerName);
						} catch (NoSuchElementException ne) {
						}
						break;
					case "companyname":
						try {
							String insurerName = library.getRandomValue("AN | 15");
							txtCompanyName.sendKeys(insurerName);
							Library.staticMap.put("Insurer Name", insurerName);
						} catch (NoSuchElementException ne) {
						}
						break;
					case "mobilenumber":
						txtMobileNumber.sendKeys(library.getRandomValue("N | 10"));
						break;
					case "emailaddress":
						txtEmailAddress.sendKeys(library.getRandomValue("A | 10") + "@godigit.com");
						break;
					case "addressline1":
						txtAddressLine1.sendKeys(library.getRandomValue("AN | 15"));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	private LinkedList<String> getVehicleMandatoryFields() {
		LinkedList<String> vehicleMandatoryFields = new LinkedList<String>();
		vehicleMandatoryFields.add("rtocode");
		vehicleMandatoryFields.add("registrationnumber");
		vehicleMandatoryFields.add("chassisnumber");
		vehicleMandatoryFields.add("vehiclemake");
		vehicleMandatoryFields.add("vehiclemodel");
		vehicleMandatoryFields.add("vehiclevariant");
		vehicleMandatoryFields.add("manufacturingyear");
		vehicleMandatoryFields.add("registrationyear");
		vehicleMandatoryFields.add("registrationmonth");
		vehicleMandatoryFields.add("residencecity");
		vehicleMandatoryFields.add("pincode");
		vehicleMandatoryFields.add("vehicleownership");
		vehicleMandatoryFields.add("enginenumber");

		return vehicleMandatoryFields;
	}

	private LinkedList<String> getPreviousPolicyMandatoryFields() {
		LinkedList<String> vehicleMandatoryFields = new LinkedList<String>();
		vehicleMandatoryFields.add("previouspolicytype");
		vehicleMandatoryFields.add("previouspolicyexpirydate");
		vehicleMandatoryFields.add("previousinsurername");
		vehicleMandatoryFields.add("previouspolicynumber");
		return vehicleMandatoryFields;
	}

	private LinkedList<String> getCustomerMandatoryFields() {
		LinkedList<String> vehicleMandatoryFields = new LinkedList<String>();
		vehicleMandatoryFields.add("firstname");
		vehicleMandatoryFields.add("companyname");
		vehicleMandatoryFields.add("mobilenumber");
		vehicleMandatoryFields.add("emailaddress");
		vehicleMandatoryFields.add("addressline1");

		return vehicleMandatoryFields;
	}

	private LinkedList<String> getTPMandatoryFields() {
		LinkedList<String> TPMandatoryFields = new LinkedList<String>();
		TPMandatoryFields.add("thirdpartypolicystartdate");
		TPMandatoryFields.add("thirdpartypolicyenddate");
		TPMandatoryFields.add("thirdpartypolicynumber");
		TPMandatoryFields.add("thirdpartyinsurername");

		return TPMandatoryFields;
	}

	public void clickOnFileDownloadIcon() throws Exception {
		// try {
		library.waitForDigitLoad();
		Thread.sleep(2000);
		fileDownoad.click();
		library.waitForDigitLoad();

		// }
		/*
		 * catch(Exception e) { throw new
		 * Exception("The download quote is not clicked"); }
		 */
	}
}
