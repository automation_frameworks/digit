package pom.agent.reco;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OpenCredits {

	@FindBy(xpath = "//div[@class='mat-tab-labels']//div[contains(text(),'Open Credits')]")
	private WebElement tabOpenCredits;

	@FindBy(xpath = "//i[@title='Add filter']")
	private WebElement btnAddFilter;

	@FindBy(xpath = "//input[@ng-reflect-name='imdCode']")
	private WebElement txtImdCode;

	@FindBy(xpath = "//input[@ng-reflect-name='instrumentNo']")
	private WebElement txtInstrumentNumber;

	@FindBy(xpath = "//button[@text()='Search']")
	private WebElement btnSearch;

	@FindBy(xpath = "//button[@text()='Clear Filter']")
	private WebElement btnClearFilter;

}
