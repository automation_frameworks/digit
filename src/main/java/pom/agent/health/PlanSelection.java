package pom.agent.health;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PlanSelection {

	com.utilities.Library library = new com.utilities.Library();

	@FindBy(xpath = "//p[text()='Option1' or text()='OPTION 3']/../..//button")
	private WebElement btnOption1;

	@FindBy(xpath = "//p[text()='Option2' or text()='OPTION 3']/../..//button")
	private WebElement btnOption2;

	@FindBy(xpath = "//p[text()='Option3' or text()='OPTION 3']/../..//button")
	private WebElement btnOption3;

	@FindBy(xpath = "//p[text()='Option4']/../..//button")
	private WebElement btnOption4;

	@FindBy(xpath = "//p[text()='Option5']/../..//button")
	private WebElement btnOption5;

	@FindBy(xpath = "//p[text()='Option6']/../..//button")
	private WebElement btnOption6;

	@FindBy(xpath = "//p[text()='Option7']/../..//button")
	private WebElement btnOption7;

	@FindBy(xpath = "//p[text()='Option8']/../..//button")
	private WebElement btnOption8;

	@FindBy(xpath = "//p[text()='Option9']/../..//button")
	private WebElement btnOption9;
	
	@FindBy(xpath="//p[text()='Arogya Sanjeevani Plan']/../..//button")
	private WebElement btnArogyaSanjeevani;

	@FindBy(xpath = "//div[text()='Plan for Proposer']")
	private WebElement lblPlanForProposer;

	@FindBy(xpath = "//div[text()='Plan for In-Laws']")
	private WebElement lblPlanForInLaws;

	public void selectProposerOption(String option) throws Exception {
		try {
			library.waitForDigitLoad();
			try {
				lblPlanForProposer.click();
			} catch (NoSuchElementException ie) {
			}
			switch (option.toLowerCase().replaceAll(" ", "")) {
			case "arogyasanjeevaniplan":
			case "arogyasanjeevani":
				btnArogyaSanjeevani.click();
				break;
			case "option1":
				btnOption1.click();
				break;
			case "option2":
				btnOption2.click();
				break;
			case "option3":
				btnOption3.click();
				break;
			case "option4":
				btnOption4.click();
				break;
			case "option5":
				btnOption5.click();
				break;
			case "option6":
				btnOption6.click();
				break;
			case "option7":
				btnOption7.click();
				break;
			case "option8":
				btnOption8.click();
				break;
			case "option9":
				btnOption9.click();
				break;
			default:
				throw new Exception("The given option [" + option + "] is not valid");
			}
		} catch (NoSuchElementException ne) {
			ne.printStackTrace();
			throw new Exception("The given option [" + option + "] is not present");
		} catch (Exception e) {
			throw new Exception("Error while selecting the option");
		}
	}

	public void selectinLawsOption(String option) throws Exception {
		try {
//			library.waitForDigitLoad();
//			lblPlanForInLaws.click();
			library.waitForDigitLoad();
			//Thread.sleep(1000);
			switch (option.toLowerCase().replaceAll(" ", "")) {
			case "arogyasanjeevaniplan":
			case "arogyasanjeevani":
				btnArogyaSanjeevani.click();
				break;
			case "option1":
				btnOption1.click();
				break;
			case "option2":
				btnOption2.click();
				break;
			case "option3":
				btnOption3.click();
				break;
			case "option4":
				btnOption4.click();
				break;
			case "option5":
				btnOption5.click();
				break;
			case "option6":
				btnOption6.click();
				break;
			case "option7":
				btnOption7.click();
				break;
			case "option8":
				btnOption8.click();
				break;
			case "option9":
				btnOption9.click();
				break;
			default:
				throw new Exception("The given option [" + option + "] is not valid");
			}
		} catch (NoSuchElementException ne) {
			ne.printStackTrace();
			throw new Exception("The given option [" + option + "] is not present");
		} catch (Exception e) {
			throw new Exception("Error while selecting the option");
		}
	}

}
