package pom.agent.health;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.utilities.Library;

public class HealthHome {

	com.utilities.Library library = new com.utilities.Library();
	
	@FindBy(xpath = "//button[contains(text(),'Create New Policy')]")
	private WebElement btnCreateNewPolicy;

	@FindBy(xpath = "//input[@value='Your Custom Plans']")
	private WebElement btnYourCustomPlans;

	public void selectOption(String option) throws Exception {
		try {
			library.waitForDigitLoad();
			switch (option.toLowerCase().replaceAll(" ", "")) {
			case "createnewpolicy":
				Library.waitForVisibilityOfTheElement(btnCreateNewPolicy);
				btnCreateNewPolicy.click();
				break;
			case "yourcustomplans":
				btnYourCustomPlans.click();
				break;
			default:
				throw new Exception("The option [" + option + "] expeceted to be clicked is not present");
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}
