package pom.agent.health;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.utilities.DBUtility;
import com.utilities.Library;

public class AgeDetails {

	com.utilities.Library library = new com.utilities.Library();
	
	@FindBy(name = "agentCodeInput")
	private WebElement txtIMD;

	@FindBy(xpath = "//span[contains(text(),'Proposer')]/..")
	private WebElement chkProposer;

	@FindBy(xpath = "//span[contains(text(),'Spouse')]/..")
	private WebElement chkSpouse;

	@FindBy(xpath = "//span[contains(text(),'Children')]/..")
	private WebElement chkChildren;

	@FindBy(xpath = "//span[contains(text(),'Father')]/..")
	private WebElement chkFather;

	@FindBy(xpath = "//span[contains(text(),'Mother')]/..")
	private WebElement chkMother;

	@FindBy(xpath = "//span[contains(text(),'Father in Law')]/..")
	private WebElement chkFatherinLaw;

	@FindBy(xpath = "//span[contains(text(),'Mother in Law')]/..")
	private WebElement chkMotherinLaw;

	@FindBy(xpath = "//button[contains(text(),'+')]")
	private WebElement btnAddChildren;

	@FindBy(xpath = "//label[contains(text(),'Proposer')]/following-sibling::input")
	private WebElement txtProposerAge;

	@FindBy(xpath = "//label[contains(text(),'Spouse')]/following-sibling::input")
	private WebElement txtSpouseAge;

	@FindBy(xpath = "//label[contains(text(),'Child 1')]/following-sibling::input")
	private WebElement txtChild1Age;

	@FindBy(xpath = "//label[contains(text(),'Child 2')]/following-sibling::input")
	private WebElement txtChild2Age;

	@FindBy(xpath = "//label[contains(text(),'Child 3')]/following-sibling::input")
	private WebElement txtChild3Age;

	@FindBy(xpath = "//label[contains(text(),'Child 4')]/following-sibling::input")
	private WebElement txtChild4Age;

	@FindBy(xpath = "//label[contains(text(),'Father')]/following-sibling::input")
	private WebElement txtFatherAge;

	@FindBy(xpath = "//label[contains(text(),'Mother')]/following-sibling::input")
	private WebElement txtMotherAge;

	@FindBy(xpath = "//label[contains(text(),'Father_in_law')]/following-sibling::input")
	private WebElement txtFatherInLawAge;

	@FindBy(xpath = "//label[contains(text(),'Mother_in_law')]/following-sibling::input")
	private WebElement txtMotherInLawAge;

	@FindBy(xpath = "//label[contains(text(),'Enter Pincode')]/following-sibling::input")
	private WebElement txtPinCode;

	@FindBy(xpath = "//button[contains(text(),'View Options')]")
	private WebElement btnViewDetails;
	
	public static boolean proposerIsMoreThan45YrsOfAge;

	public void enterAgeDetails(String details) throws Exception {
		System.out.println("Details: "+details);
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "proposer":
					Library.waitForVisibilityOfTheElement(txtProposerAge);
					cal = Calendar.getInstance();
					cal.add(Calendar.YEAR, -(Integer.parseInt(hmap.get(key).trim())));
					library.enterDate(txtProposerAge, s.format(cal.getTime()));
					txtProposerAge.sendKeys(s.format(cal.getTime()).split("-")[0]);
					break;
				case "spouse":
					chkSpouse.click();
					Library.waitForVisibilityOfTheElement(txtSpouseAge);
					cal = Calendar.getInstance();
					cal.add(Calendar.YEAR, -(Integer.parseInt(hmap.get(key).trim())));
					library.enterDate(txtSpouseAge, s.format(cal.getTime()));
					//txtSpouseAge.sendKeys(s.format(cal.getTime()));
					break;
				case "child1":
					chkChildren.click();
					Library.waitForVisibilityOfTheElement(txtChild1Age);
					cal = Calendar.getInstance();
					cal.add(Calendar.YEAR, -(Integer.parseInt(hmap.get(key).trim())));
					library.enterDate(txtChild1Age, s.format(cal.getTime()));
					//txtChild1Age.sendKeys(s.format(cal.getTime()));
					break;
				case "child2":
					btnAddChildren.click();
					Library.waitForVisibilityOfTheElement(txtChild2Age);
					cal = Calendar.getInstance();
					cal.add(Calendar.YEAR, -(Integer.parseInt(hmap.get(key).trim())));
					library.enterDate(txtChild2Age, s.format(cal.getTime()));
					//txtChild2Age.sendKeys(s.format(cal.getTime()));
					break;
				case "child3":
					btnAddChildren.click();
					Library.waitForVisibilityOfTheElement(txtChild3Age);
					cal = Calendar.getInstance();
					cal.add(Calendar.YEAR, -(Integer.parseInt(hmap.get(key).trim())));
					library.enterDate(txtChild3Age, s.format(cal.getTime()));
					//txtChild3Age.sendKeys(s.format(cal.getTime()));
					break;
				case "child4":
					btnAddChildren.click();
					Library.waitForVisibilityOfTheElement(txtChild4Age);
					cal = Calendar.getInstance();
					cal.add(Calendar.YEAR, -(Integer.parseInt(hmap.get(key).trim())));
					library.enterDate(txtChild4Age, s.format(cal.getTime()));
					//txtChild4Age.sendKeys(s.format(cal.getTime()));
					break;
				case "father":
					chkFather.click();
					Library.waitForVisibilityOfTheElement(txtFatherAge);
					cal = Calendar.getInstance();
					cal.add(Calendar.YEAR, -(Integer.parseInt(hmap.get(key).trim())));
					library.enterDate(txtFatherAge, s.format(cal.getTime()));
					//txtFatherAge.sendKeys(s.format(cal.getTime()));
					break;
				case "mother":
					chkMother.click();
					Library.waitForVisibilityOfTheElement(txtMotherAge);
					cal = Calendar.getInstance();
					cal.add(Calendar.YEAR, -(Integer.parseInt(hmap.get(key).trim())));
					library.enterDate(txtMotherAge, s.format(cal.getTime()));
					//txtMotherAge.sendKeys(s.format(cal.getTime()));
					break;
				case "fatherinlaw":
					chkFatherinLaw.click();
					Library.waitForVisibilityOfTheElement(txtFatherInLawAge);
					cal = Calendar.getInstance();
					cal.add(Calendar.YEAR, -(Integer.parseInt(hmap.get(key).trim())));
					library.enterDate(txtFatherInLawAge, s.format(cal.getTime()));
					//txtFatherInLawAge.sendKeys(s.format(cal.getTime()));
					break;
				case "motherinlaw":
					chkMotherinLaw.click();
					Library.waitForVisibilityOfTheElement(txtMotherInLawAge);
					cal = Calendar.getInstance();
					cal.add(Calendar.YEAR, -(Integer.parseInt(hmap.get(key).trim())));
					library.enterDate(txtMotherInLawAge, s.format(cal.getTime()));
					//txtMotherInLawAge.sendKeys(s.format(cal.getTime()));
					break;
				default:
					throw new Exception(
							"The given field [" + key + "] is not present in the Agent Health Age Details page");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the age details : " + e.getMessage());
		}
	}

	public void unSelectProposer() throws Exception
	{
		try {
			chkProposer.click();
		}
		catch(Exception e)
		{
			throw new Exception("Error while unselecting the proposer == > "+e.getMessage());
		}
	}
	
	public void enterPinCode(String pinCode) throws Exception {
		try {
			txtPinCode.sendKeys(pinCode);
			txtPinCode.sendKeys(Keys.TAB);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the pincode : " + e.getMessage());
		}
	}
	public void enterPinCode(WebDriver driver,String planId,String pinCode) throws Exception {
		
		DBUtility db = PageFactory.initElements(driver, DBUtility.class);
		pinCode = db.getPersonDataFromDB(planId,pinCode);System.out.println("Entering pincode: "+pinCode);
		try {
			txtPinCode.sendKeys(pinCode);
			txtPinCode.sendKeys(Keys.TAB);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the pincode : " + e.getMessage());
		}
	}

	public void performAction(String action) throws Exception {
		try {
			switch (action.toLowerCase().replaceAll(" ", "")) {
			case "viewoptions":
				library.waitForDigitLoad();
				btnViewDetails.click();
//				Thread.sleep(15000);
				break;
			default:
				throw new Exception(
						"The given action [" + action + "] is not present in the Agent Health Age Details page");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while clicking the button : " + e.getMessage());
		}
	}
	
	public void getIMD(String arg) {
		txtIMD.sendKeys(arg);
	}
	
	public String[] dateOfBirth(String person,String arg) throws Exception {
		
		String[] dateFromDb = arg.split(",");			//In Database, dobFromDb = 06-01-1982,14-02-1985,etc
		String[] dob = null;
		
			switch(person.replace(" ", "").toLowerCase()) {
			
			case "proposer":
				dob = dateFromDb[0].split("-");
				break;
			case "spouse":
				dob = dateFromDb[1].split("-");
				break;
			case "child1":
				dob = dateFromDb[2].split("-");
				break;
			case "child2":
				dob = dateFromDb[3].split("-");
				break;
			case "child3":
				dob = dateFromDb[4].split("-");
				break;
			case "child4":
				dob = dateFromDb[5].split("-");
				break;
			case "father":
				dob = dateFromDb[6].split("-");
				break;
			case "mother":
				dob = dateFromDb[6].split("-");
				break;
			case "fatherinlaw":
				dob = dateFromDb[6].split("-");
				break;
			case "motherinlaw":
				dob = dateFromDb[6].split("-");
				break;
			}
		
		return dob;
	}
	
	
	public void coverFor(WebDriver driver,String planId,String person) throws Exception {
		
		DBUtility db = PageFactory.initElements(driver, DBUtility.class);
		boolean flag = true;
		String dob = db.getPersonDataFromDB(planId,"dob");
		
		try {
		switch(person.replace(" ", "").toLowerCase()) {
		
		case "proposer":
			proposerIsMoreThan45YrsOfAge = false;
			txtProposerAge.sendKeys(dateOfBirth(person,dob)[0]);
			Thread.sleep(1000);
			txtProposerAge.sendKeys(dateOfBirth(person,dob)[1]);
			Thread.sleep(1000);
			txtProposerAge.sendKeys(dateOfBirth(person,dob)[2]);
			flag = false;
			proposerIsMoreThan45YrsOfAge = ageFlag(dob);
			break;
		case "spouse":
			chkSpouse.click();
			txtSpouseAge.sendKeys(dateOfBirth(person,dob)[0]);
			Thread.sleep(1000);
			txtSpouseAge.sendKeys(dateOfBirth(person,dob)[1]);
			Thread.sleep(1000);
			txtSpouseAge.sendKeys(dateOfBirth(person,dob)[2]);
			break;
		case "child1":
			chkChildren.click();
			txtChild1Age.sendKeys(dateOfBirth(person,dob)[0]);
			Thread.sleep(1000);
			txtChild1Age.sendKeys(dateOfBirth(person,dob)[1]);
			Thread.sleep(1000);
			txtChild1Age.sendKeys(dateOfBirth(person,dob)[2]);
			break;
		case "child2":
			btnAddChildren.click();
			txtChild2Age.sendKeys(dateOfBirth(person,dob)[0]);
			Thread.sleep(1000);
			txtChild2Age.sendKeys(dateOfBirth(person,dob)[1]);
			Thread.sleep(1000);
			txtChild2Age.sendKeys(dateOfBirth(person,dob)[2]);
			break;
		case "child3":
			btnAddChildren.click();
			txtChild3Age.sendKeys(dateOfBirth(person,dob)[0]);
			Thread.sleep(1000);
			txtChild3Age.sendKeys(dateOfBirth(person,dob)[1]);
			Thread.sleep(1000);
			txtChild3Age.sendKeys(dateOfBirth(person,dob)[2]);
			break;
		case "child4":
			btnAddChildren.click();
			txtChild4Age.sendKeys(dateOfBirth(person,dob)[0]);
			Thread.sleep(1000);
			txtChild4Age.sendKeys(dateOfBirth(person,dob)[1]);
			Thread.sleep(1000);
			txtChild4Age.sendKeys(dateOfBirth(person,dob)[2]);
			break;
		case "father":
			chkFather.click();
			txtFatherAge.sendKeys(dateOfBirth(person,dob)[0]);
			Thread.sleep(1000);
			txtFatherAge.sendKeys(dateOfBirth(person,dob)[1]);
			Thread.sleep(1000);
			txtFatherAge.sendKeys(dateOfBirth(person,dob)[2]);
			break;
		case "mother":
			chkMother.click();
			txtMotherAge.sendKeys(dateOfBirth(person,dob)[0]);
			Thread.sleep(1000);
			txtMotherAge.sendKeys(dateOfBirth(person,dob)[1]);
			Thread.sleep(1000);
			txtMotherAge.sendKeys(dateOfBirth(person,dob)[2]);
			break;
		case "fatherinlaw":
			chkFatherinLaw.click();
			txtFatherInLawAge.sendKeys(dateOfBirth(person,dob)[0]);
			Thread.sleep(1000);
			txtFatherInLawAge.sendKeys(dateOfBirth(person,dob)[1]);
			Thread.sleep(1000);
			txtFatherInLawAge.sendKeys(dateOfBirth(person,dob)[2]);
			break;
		case "motherinlaw":
			chkMotherinLaw.click();
			txtMotherInLawAge.sendKeys(dateOfBirth(person,dob)[0]);
			Thread.sleep(1000);
			txtMotherInLawAge.sendKeys(dateOfBirth(person,dob)[1]);
			Thread.sleep(1000);
			txtMotherInLawAge.sendKeys(dateOfBirth(person,dob)[2]);
			break;
		}
		} finally {
			if (flag) {
				txtProposerAge.sendKeys(dateOfBirth(person,dob)[0]);
				Thread.sleep(1000);
				txtProposerAge.sendKeys(dateOfBirth(person,dob)[1]);
				Thread.sleep(1000);
				txtProposerAge.sendKeys(dateOfBirth(person,dob)[2]);
			}
		}
	}
	
	private boolean ageFlag(String dob) {
		
		
		Calendar cal = Calendar.getInstance(); 
		SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy"); 
		int propAge = Integer.parseInt(s.format(cal.getTime()).split("-")[2]) - Integer.parseInt(dob.split(",")[0].split("-")[2]);
		
		return propAge>45;
	}
	
}
