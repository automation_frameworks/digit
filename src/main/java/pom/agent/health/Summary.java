package pom.agent.health;

import java.awt.AWTException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.utilities.DBUtility;
import com.utilities.Library;

import pom.agent.retail_pa.PlanDetailsRetailPA;

public class Summary {

	com.utilities.Library library = new com.utilities.Library();

	@FindBy(xpath = "(//p)[5]//div[@class='mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin']")
	private WebElement chkDeclaration;

	@FindBy(xpath = "//label[text()='Payment Mode']/..//mat-select")
	private WebElement drpPaymentMethod;

	@FindBy(xpath = "//span[contains(text(),'Agent Float')]")
	private WebElement optionAgentFloat;

	@FindBy(xpath = "//span[contains(text(),'Cheque')]")
	private WebElement optionCheque;

	@FindBy(xpath = "//span[contains(text(),'Online')]")
	private WebElement optionOnline;

	@FindBy(xpath = "//span[contains(text(),'Send Payment')]")
	private WebElement optionSendPayment;

	@FindBy(xpath = "//span[contains(text(),'Generate Payment')]")
	private WebElement optionGeneratePayment;

	@FindBy(xpath = "//label[text()='Cheque Number']/../input")
	private WebElement txtChequeNumber;

	@FindBy(xpath = "//label[text()='Cheque Date']/../input")
	private WebElement txtChequeDate;

	@FindBy(xpath = "//label[text()='IFSC/MICR']/../input")
	private WebElement txtIFSCMICR;

	@FindBy(xpath = "//span[contains(text(),'I hereby declare that')]/../mat-checkbox")
	private WebElement chkChequeDeclaration;
	
	@FindBy(xpath="//label[text()='Confirm Member Details']/../following-sibling::div/a")
	private WebElement btnCloseConfirmMemberDetails;

	@FindBy(xpath = "//button[contains(text(),'Proceed to Pay')]")
	private WebElement btnProceedtoPay;

	@FindBy(xpath = "//button[contains(text(),'Update Quote')]")
	private WebElement btnUpdateQuote;
	
	@FindBy(xpath = "//div[@class = 'mat-select-value']")
	private WebElement drpPaidBy;
	
	@FindAll({ @FindBy(xpath = "//span[@class = 'mat-option-text']") })
	private List<WebElement> listPaidBy;
	
	@FindBy(xpath = "//label[text()='Option for Proposer']/../span")
	private WebElement proposalNum;
	
	@FindBy(xpath = "//span[contains(text(),'Download PDF')]")
	private WebElement lnkDownloadPDF;
	
	@FindBy(xpath = "//p[contains(text(),'Upload Documents for Self')]/../..//label[contains(text(),'Form 16')]/../..//a[contains(text(),'Upload')]/../input")
	private WebElement lnkUploadForm16ForSelf;
	
	@FindBy(xpath = "//p[contains(text(),'Upload Documents for Spouse')]/../..//label[contains(text(),'Form 16')]/../..//a[contains(text(),'Upload')]/../input")
	private WebElement lnkUploadForm16ForSpouse;
	
	@FindBy(xpath = "//p[contains(text(),'Upload Documents for Self')]/../..//label[contains(text(),'Pan')]/../..//a[contains(text(),'Upload')]/../input")
	private WebElement lnkUploadPanCardForSelf;
	
	@FindBy(xpath = "//p[contains(text(),'Upload Documents for Spouse')]/../..//label[contains(text(),'Pan')]/../..//a[contains(text(),'Upload')]/../input")
	private WebElement lnkUploadPanCardForSpouse;
	
	@FindBy(xpath = "//button[text()='Send OTP']")
	private WebElement btnSendOTP;
	
	@FindBy(xpath = "//img[@alt = 'DigitPlus']")
	private WebElement imgDigitPlusLogo;
	
	public static boolean btnSendOTPIsPresent;
	

	public void uploadDocuments() throws Exception {
		if (Integer.parseInt(PlanDetailsRetailPA.basesi)>5000000) {
			lnkUploadForm16ForSelf.sendKeys(Paths.get("").toAbsolutePath().toString() + "/src/main/resources/documents/invoice.pdf");
			library.waitForDigitLoad();
			Thread.sleep(3000);
			lnkUploadPanCardForSelf.sendKeys(Paths.get("").toAbsolutePath().toString() + "/src/main/resources/documents/invoice.pdf");
			library.waitForDigitLoad();
			Thread.sleep(3000);
		}
		
		try {
			if (PlanDetailsRetailPA.spouseEarning && PlanDetailsRetailPA.spouseSI > 5000000) {
				lnkUploadForm16ForSpouse.sendKeys(Paths.get("").toAbsolutePath().toString() + "/src/main/resources/documents/invoice.pdf");
				library.waitForDigitLoad();
				Thread.sleep(3000);
				lnkUploadPanCardForSpouse.sendKeys(Paths.get("").toAbsolutePath().toString() + "/src/main/resources/documents/invoice.pdf");
				library.waitForDigitLoad();
				Thread.sleep(3000);
			} 
		} finally {
			PlanDetailsRetailPA.spouseSI =  0;
		}
	}
	public  void getDownloadPDF() {
		lnkDownloadPDF.click();
	}
	public  String getProposalNumber() {
		
		return proposalNum.getText();
	}
	public void acceptDeclaration() throws Exception {
		try {
			library.waitForDigitLoad();
			chkDeclaration.click();
			try{
			library.waitForElementToBeClickable(btnCloseConfirmMemberDetails, 10, 500);
			Thread.sleep(3000);
			btnCloseConfirmMemberDetails.click();
			}
			catch (Exception e)
			{
				
			}
		} catch (Exception e) {
			throw new Exception("Error while accepting the declaration : " + e.getMessage());
		}
	}
	
	public void acceptTermsAndConditionsForRetailHealth(String id) throws Exception {
		try {
			btnSendOTPIsPresent = false;
			library.waitForDigitLoad();
			chkDeclaration.click();
			try{
				library.waitForElementToBeClickable(btnCloseConfirmMemberDetails, 10, 500);
				Thread.sleep(3000);
				btnCloseConfirmMemberDetails.click();
			}
			catch (Exception e)
			{
				
			}
			try {
				Thread.sleep(2000);
				if (btnSendOTP.isDisplayed()) {
					btnSendOTPIsPresent = true;
					library.focus(library.getDriver(), btnSendOTP);
					System.out.println("Payment cannot be done as \"Send OTP\" can't be automated.");
					DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
					db.storeScriptFailCommentInDB("Reason: Send OTP", id, "digit_health.health_testdata_plan");
				}
			} catch (NoSuchElementException ne) {
				
			}
		} catch (Exception e) {
			throw new Exception("Error while accepting the declaration : " + e.getMessage());
		}
	}
	public void acceptTermsAndConditionsForRetailPA(String id) throws Exception {
		try {
			btnSendOTPIsPresent = false;
			library.waitForDigitLoad();
			chkDeclaration.click();
			try{
				library.waitForElementToBeClickable(btnCloseConfirmMemberDetails, 10, 500);
				Thread.sleep(3000);
				btnCloseConfirmMemberDetails.click();
			}
			catch (Exception e)
			{
				
			}
			try {
				Thread.sleep(2000);
				if (btnSendOTP.isDisplayed()) {
					btnSendOTPIsPresent = true;
					library.focus(library.getDriver(), btnSendOTP);
					System.out.println("Payment cannot be done as \"Send OTP\" can't be automated.");
					DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
					db.storeScriptFailCommentInDB("Reason: Send OTP", id, "digit_retail_pa.retail_pa_testdata");
				} 
			} catch (NoSuchElementException ne) {
				
			}
		} catch (Exception e) {
			throw new Exception("Error while accepting the declaration : " + e.getMessage());
		}
	}
	public void acceptTermsAndConditionsForPortabilityInRetailHealth(String id) throws Exception {
		try {
			btnSendOTPIsPresent = false;
			library.waitForDigitLoad();
			chkDeclaration.click();
			try{
				library.waitForElementToBeClickable(btnCloseConfirmMemberDetails, 10, 500);
				Thread.sleep(3000);
				btnCloseConfirmMemberDetails.click();
			}
			catch (Exception e)
			{
				
			}
			try {
				Thread.sleep(2000);
				if (btnSendOTP.isDisplayed()) {
					btnSendOTPIsPresent = true;
					library.focus(library.getDriver(), btnSendOTP);
					System.out.println("Payment cannot be done as \"Send OTP\" can't be automated.");
					DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
					db.storeScriptFailCommentInDB("Reason: Send OTP", id, "digit_health.health_testdata_portability");
				}
			} catch (NoSuchElementException ne) {
				
			}
		} catch (Exception e) {
			throw new Exception("Error while accepting the declaration : " + e.getMessage());
		}
	}

	public void verifyUWStatus(String details) throws Exception {
		try {
			library.waitForDigitLoad();
			HashMap<String, String> hmap = library.getFieldMap(details);
			String name = "";
			String error = "";
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "proposer":
				case "self":
					name = Library.staticMap.get("SELF_firstName") + " " + Library.staticMap.get("SELF_lastName");
					break;
				case "spouse":
					name = Library.staticMap.get("SPOUSE_firstName") + " " + Library.staticMap.get("SPOUSE_lastName");
					break;
				case "child1":
					name = Library.staticMap.get("CHILD 1_firstName") + " " + Library.staticMap.get("CHILD 1_lastName");
					break;
				case "child2":
					name = Library.staticMap.get("CHILD 2_firstName") + " " + Library.staticMap.get("CHILD 2_lastName");
					break;
				case "child3":
					name = Library.staticMap.get("CHILD 3_firstName") + " " + Library.staticMap.get("CHILD 3_lastName");
					break;
				case "child4":
					name = Library.staticMap.get("CHILD 4_firstName") + " " + Library.staticMap.get("CHILD 4_lastName");
					break;
				case "father":
					name = Library.staticMap.get("FATHER_firstName") + " " + Library.staticMap.get("FATHER_lastName");
					break;
				case "mother":
					name = Library.staticMap.get("MOTHER_firstName") + " " + Library.staticMap.get("MOTHER_lastName");
					break;
				case "fatherinlaw":
					name = Library.staticMap.get("FATHER_IN_LAW_firstName") + " "
							+ Library.staticMap.get("FATHER_IN_LAW_lastName");
					break;
				case "motherinlaw":
					name = Library.staticMap.get("MOTHER_IN_LAW_firstName") + " "
							+ Library.staticMap.get("MOTHER_IN_LAW_lastName");
					break;

				}
				String status = library.getDriver()
						.findElement(By.xpath("// label[contains(text(),'who can be covered')]/../div//label[text()='"
								+ name + "']/following-sibling::span[2]"))
						.getText().split(":")[1].trim();
				if (!status.equals(hmap.get(key)))
					error = error + " | " + key + ">>> Expected : " + hmap.get(key) + " >>> Actual : " + status;
			}
			if (!error.equals(""))
				throw new Exception("There are mismatches in the statuses. " + error);
		} catch (Exception e) {
			throw new Exception("Error in validating the statuses : " + e.getMessage());
		}
	}
	
	public void premiumPaidBy(String person) throws AWTException, InterruptedException {
		try {
			drpPaidBy.click();
			for (WebElement name : listPaidBy) {
				if (name.getText().equalsIgnoreCase(person.trim())) {
					try {
						name.click();
					} catch (Exception e) {
						Thread.sleep(2000);
						name.click();
					}
					break;
				}
			} 
		} catch(NoSuchElementException ne) {
			if (!btnSendOTPIsPresent) {
				library.focus(library.getDriver(), imgDigitPlusLogo);
			}
		}
		
	}

	public void selectPaymentMethods(String paymentMethod) throws Exception {
		try {
			Thread.sleep(1000);
			drpPaymentMethod.click();
			Thread.sleep(1000);
			switch (paymentMethod.replaceAll(" ", "").toLowerCase()) {
			case "agentfloat":
				optionAgentFloat.click();
				break;
			case "cheque":
				optionCheque.click();
				txtChequeNumber.sendKeys(library.getRandomValue("[N | 6]"));
				txtChequeDate.sendKeys(library.getDate("[TODAY]", "dd-MM-yyyy"));
				txtIFSCMICR.sendKeys("ADCB0000002");
				txtChequeNumber.click();
				library.waitForDigitLoad();
				chkChequeDeclaration.click();
				break;
			case "online":
				optionOnline.click();
				break;
			case "sendpayment":
				optionSendPayment.click();
				break;
			case "generatepayment":
				optionGeneratePayment.click();
				break;
			default:
				throw new Exception("The given payment method [" + paymentMethod + "] is not valid");
			}
		} catch (Exception e) {
			throw new Exception("Error while selecting the payment details : " + e.getMessage());
		}
	}

	public void proceedToPayment() throws Exception {
		try {
			library.waitForDigitLoad();
			try {
				if (btnUpdateQuote.isDisplayed())
					btnUpdateQuote.click();
			} catch (NoSuchElementException ne) {
			}
			library.waitForDigitLoad();Thread.sleep(1000);
			btnProceedtoPay.click();
		} catch (Exception e) {
			throw new Exception("Error while proceeding to the payment : " + e.getMessage());
		}
	}
	public void getProceedToPay() {
		btnProceedtoPay.click();
	}
}
