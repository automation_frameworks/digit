package pom.agent.health;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.utilities.Library;

public class PersonalHabits {

	com.utilities.Library library = new com.utilities.Library();

	@FindBy(xpath = "//label[contains(text(),'Who in the family consumes tobacco?')]/../div//span[text()='None']")
	private WebElement chkComsumesTobaccoNone;

	@FindBy(xpath = "//label[contains(text(),'Who in the family consumes alcohol?')]/../div//span[text()='None']")
	private WebElement chkComsumesAlcoholNone;

	@FindBy(xpath = "//button[text()='Next']")
	private WebElement btnNext;

	public void selectTobaccoAlcoholConsumingPersons(String details) throws Exception {
		try {Thread.sleep(2000);
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();Thread.sleep(2000);
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "tobacco":
					for (String member : hmap.get(key).split(",")) {
						switch (member.replaceAll(" ", "").toLowerCase()) {
						case "none":
							chkComsumesTobaccoNone.click();
							break;
						case "proposer":
						case "self":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes tobacco?')]/../div//span[text()='"
											+ Library.staticMap.get("SELF_firstName") + "']"))
									.click();
							break;
						case "spouse":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes tobacco?')]/../div//span[text()='"
											+ Library.staticMap.get("SPOUSE_firstName") + "']"))
									.click();
							break;
						case "child1":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes tobacco?')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 1_firstName") + "']"))
									.click();
							break;
						case "child2":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes tobacco?')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 2_firstName") + "']"))
									.click();
							break;
						case "child3":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes tobacco?')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 3_firstName") + "']"))
									.click();
							break;
						case "child4":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes tobacco?')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 4_firstName") + "']"))
									.click();
							break;
						case "father":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes tobacco?')]/../div//span[text()='"
											+ Library.staticMap.get("FATHER_firstName") + "']"))
									.click();
							break;
						case "mother":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes tobacco?')]/../div//span[text()='"
											+ Library.staticMap.get("MOTHER_firstName") + "']"))
									.click();
							break;
						case "fatherinlaw":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes tobacco?')]/../div//span[text()='"
											+ Library.staticMap.get("FATHER_IN_LAW_firstName") + "']"))
									.click();
							break;
						case "motherinlaw":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes tobacco?')]/../div//span[text()='"
											+ Library.staticMap.get("MOTHER_IN_LAW_firstName") + "']"))
									.click();
							break;
						default:
							throw new Exception("There is no such member [" + member + "].");
						}
					}
					break;
				case "alcohol":
					for (String member : hmap.get(key).split(",")) {
						switch (member.replaceAll(" ", "").toLowerCase()) {
						case "none":
							chkComsumesAlcoholNone.click();
							break;
						case "proposer":
						case "self":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes alcohol?')]/../div//span[text()='"
											+ Library.staticMap.get("SELF_firstName") + "']"))
									.click();
							break;
						case "spouse":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes alcohol?')]/../div//span[text()='"
											+ Library.staticMap.get("SPOUSE_firstName") + "']"))
									.click();
							break;
						case "child1":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes alcohol?')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 1_firstName") + "']"))
									.click();
							break;
						case "child2":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes alcohol?')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 2_firstName") + "']"))
									.click();
							break;
						case "child3":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes alcohol?')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 3_firstName") + "']"))
									.click();
							break;
						case "child4":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes alcohol?')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 4_firstName") + "']"))
									.click();
							break;
						case "father":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes alcohol?')]/../div//span[text()='"
											+ Library.staticMap.get("FATHER_firstName") + "']"))
									.click();
							break;
						case "mother":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes alcohol?')]/../div//span[text()='"
											+ Library.staticMap.get("MOTHER_firstName") + "']"))
									.click();
							break;
						case "fatherinlaw":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes alcohol?')]/../div//span[text()='"
											+ Library.staticMap.get("FATHER_IN_LAW_firstName") + "']"))
									.click();
							break;
						case "motherinlaw":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Who in the family consumes alcohol?')]/../div//span[text()='"
											+ Library.staticMap.get("MOTHER_IN_LAW_firstName") + "']"))
									.click();
							break;
						default:
							throw new Exception("There is no such member [" + member + "].");
						}
					}
					break;
				default:
					throw new Exception("There is no such habit [" + key + "].");
				}
			}
		} catch (Exception e) {
			throw new Exception("Error while selecting the personal habits : " + e.getMessage());
		}
	}

	public void enterTobaccoFormDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			String name;
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "proposer":
				case "self":
					name = Library.staticMap.get("SELF_firstName");
					break;
				case "spouse":
					name = Library.staticMap.get("SPOUSE_firstName");
					break;
				case "child1":
					name = Library.staticMap.get("CHILD 1_firstName");
					break;
				case "child2":
					name = Library.staticMap.get("CHILD 2_firstName");
					break;
				case "child3":
					name = Library.staticMap.get("CHILD 3_firstName");
					break;
				case "child4":
					name = Library.staticMap.get("CHILD 4_firstName");
					break;
				case "father":
					name = Library.staticMap.get("FATHER_firstName");
					break;
				case "mother":
					name = Library.staticMap.get("MOTHER_firstName");
					break;
				case "fatherinlaw":
					name = Library.staticMap.get("FATHER_IN_LAW_firstName");
					break;
				case "motherinlaw":
					name = Library.staticMap.get("MOTHER_IN_LAW_firstName");
					break;
				default:
					throw new Exception("There is no such member [" + key + "].");
				}
				switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
				case "chewable":
					library.getDriver()
							.findElement(By.xpath("//input[@name='RSTB1_" + name + "']/..//div[text()='Chewable']"))
							.click();
					break;
				case "smoke":
					library.getDriver()
							.findElement(By.xpath("//input[@name='RSTB1_" + name + "']/..//div[text()='Smoke']"))
							.click();
					break;
				case "both":
					library.getDriver()
							.findElement(By.xpath("//input[@name='RSTB1_" + name + "']/..//div[text()='Both']"))
							.click();
					break;
				default:
					throw new Exception("There is no such tobacco habit [" + hmap.get(key) + "].");
				}
			}
		} catch (Exception e) {
			throw new Exception("Error while selecting the tobacco consumption form : " + e.getMessage());
		}
	}

	public void enterAlcoholFormDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			String name;
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "proposer":
				case "self":
					name = Library.staticMap.get("SELF_firstName");
					break;
				case "spouse":
					name = Library.staticMap.get("SPOUSE_firstName");
					break;
				case "child1":
					name = Library.staticMap.get("CHILD 1_firstName");
					break;
				case "child2":
					name = Library.staticMap.get("CHILD 2_firstName");
					break;
				case "child3":
					name = Library.staticMap.get("CHILD 3_firstName");
					break;
				case "child4":
					name = Library.staticMap.get("CHILD 4_firstName");
					break;
				case "father":
					name = Library.staticMap.get("FATHER_firstName");
					break;
				case "mother":
					name = Library.staticMap.get("MOTHER_firstName");
					break;
				case "fatherinlaw":
					name = Library.staticMap.get("FATHER_IN_LAW_firstName");
					break;
				case "motherinlaw":
					name = Library.staticMap.get("MOTHER_IN_LAW_firstName");
					break;
				default:
					throw new Exception("There is no such member [" + key + "].");
				}
				switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
				case "occasionalorsocial":
				case "occasional":
				case "social":
					library.getDriver()
							.findElement(By.xpath(
									"//input[@name='RSAL1_" + name + "']/..//div[text()='Occasional or Social']"))
							.click();
					break;
				case "onceinaweek":
					library.getDriver()
							.findElement(
									By.xpath("//input[@name='RSAL1_" + name + "']/..//div[text()='Once in a week']"))
							.click();
					break;
				case "daily":
					library.getDriver()
							.findElement(By.xpath("//input[@name='RSAL1_" + name + "']/..//div[text()='Daily']"))
							.click();
					break;
				default:
					throw new Exception("There is no such alcohol habit [" + hmap.get(key) + "].");
				}
			}
		} catch (Exception e) {
			throw new Exception("Error while selecting the alcohol consumption form : " + e.getMessage());
		}
	}

	public void performAction(String action) throws Exception {
		try {
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "next":
				btnNext.click();
				break;
			default:
				throw new Exception("There is no such action [" + action + "] in Family details page");
			}
		} catch (Exception e) {
			throw new Exception("Error while clicking the button in Family details : " + e.getMessage());
		}
	}

	public void checkTobaccoConsideringPersons(String habit, String members, boolean flag) throws Exception {
		try {
			String memberName = "";
			String errorMessage = "";
			for (String temp : members.split(",")) {
				switch (temp.replaceAll(" ", "").toLowerCase()) {
				case "proposer":
				case "self":
					memberName = Library.staticMap.get("SELF_firstName");
					break;
				case "spouse":
					memberName = Library.staticMap.get("SPOUSE_firstName");
					break;
				case "child1":
					memberName = Library.staticMap.get("CHILD 1_firstName");
					break;
				case "child2":
					memberName = Library.staticMap.get("CHILD 2_firstName");
					break;
				case "child3":
					memberName = Library.staticMap.get("CHILD 3_firstName");
					break;
				case "child4":
					memberName = Library.staticMap.get("CHILD 4_firstName");
					break;
				case "father":
					memberName = Library.staticMap.get("FATHER_firstName");
					break;
				case "mother":
					memberName = Library.staticMap.get("MOTHER_firstName");
					break;
				case "fatherinlaw":
					memberName = Library.staticMap.get("FATHER_IN_LAW_firstName");
					break;
				case "motherinlaw":
					memberName = Library.staticMap.get("MOTHER_IN_LAW_firstName");
					break;
				default:
					throw new Exception("There is no such member [" + temp + "].");
				}
				String habitString = "";

				if (habit.equalsIgnoreCase("alcohol"))
					habitString = "Who in the family consumes alcohol?";
				else if (habit.equalsIgnoreCase("tobacco"))
					habitString = "Who in the family consumes tobacco?";

				if (flag && library.getDriver().findElements(By.xpath(
						"//label[contains(text(),'" + habitString + "')]/../div//span[text()='" + memberName + "']"))
						.size() == 0)
					errorMessage = errorMessage + " | Expected " + temp + ". But not present";
				if (!flag && library.getDriver().findElements(By.xpath(
						"//label[contains(text(),'" + habitString + "')]/../div//span[text()='" + memberName + "']"))
						.size() > 0)
					errorMessage = errorMessage + " | " + temp + " is not expected. But is present";
			}
			System.out.println("Error Message : " + errorMessage);
		} catch (Exception e) {
			throw new Exception("Error while clicking the button in Family details : " + e.getMessage());
		}
	}
}
