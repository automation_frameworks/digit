package pom.agent.health;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.utilities.Library;

public class Success {

	com.utilities.Library library = new com.utilities.Library();

	@FindBy(id = "Successfully-Issued")
	private WebElement lblSuccessMessage;

	@FindBy(xpath = "//span[@class='forPolicyNumber']")
	private WebElement lblPolicyNumber;

	@FindBy(xpath = "//span[text()='Download policy Kit']")
	private WebElement btnDownloadPolicyKit;

	@FindBy(xpath = "//app-health-success//span")
	private WebElement lblReferralMessage;

	public void verifySuccessMessage(String expectedMessage) throws Exception {
		try {
			library.waitForDigitLoad();
			if (lblPolicyNumber.isDisplayed()) {
				String policyNumber = lblPolicyNumber.getText().split(":")[1].trim();
				Library.staticMap.put("Policy Number", policyNumber);
			}
			if (expectedMessage.equalsIgnoreCase("success")) {
				try {
					if (lblSuccessMessage.isDisplayed()) {
						if (!lblSuccessMessage.getText().equals("Successfully Issued the policy"))
							throw new Exception(
									"The expected success message is [Successfully Issued the policy]. But the message displayed is ["
											+ lblSuccessMessage.getText() + "]");
					}
				} catch (NoSuchElementException e) {
					try {
						if (lblReferralMessage.isDisplayed())
							throw new Exception("Success message is expected. But the referral message is displayed");
					} catch (NoSuchElementException ne) {
						throw new Exception("Neither success nor referral message is displayed");
					}
				}
			} else if (expectedMessage.equalsIgnoreCase("referral")) {
				try {
					if (lblReferralMessage.isDisplayed()) {
						if (!lblReferralMessage.getText().equals("Application is referred to our medical team"))
							throw new Exception(
									"The expected success message is [Application is referred to our medical team]. But the message displayed is ["
											+ lblSuccessMessage.getText() + "]");
					}
				} catch (NoSuchElementException e) {
					try {
						if (lblSuccessMessage.isDisplayed())
							throw new Exception("Referral message is expected. But success message is displayed");
					} catch (NoSuchElementException ne) {
						throw new Exception("Neither success nor referral message is displayed");
					}
				}
			}

		} catch (Exception e) {
			throw new Exception("Error while validating the success page : " + e.getMessage());
		}
	}
}
