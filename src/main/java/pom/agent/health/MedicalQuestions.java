package pom.agent.health;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.utilities.DBUtility;
import com.utilities.Library;

public class MedicalQuestions {

	com.utilities.Library library = new com.utilities.Library();

	@FindBy(xpath = "//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='None']/../div/input")
	private WebElement chkUndiagnosedSymptomsNone;

	@FindBy(xpath = "//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='None']/../div/input")
	private WebElement chkPreExistingConditionsNone;

	@FindBy(xpath = "//label[contains(text(),'going through any medications currently')]/../div//span[text()='None']/../div/input")
	private WebElement chkOngoingMedicationsNone;
	
	@FindBy(xpath = "//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='None']/../div")
	private WebElement chkUndiagnosedSymptomsNoneCNP;
	
	@FindBy(xpath = "//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='None']/../div")
	private WebElement chkPreExistingConditionsNoneCNP;
	
	@FindBy(xpath = "//label[contains(text(),'going through any medications currently')]/../div//span[text()='None']/../div")
	private WebElement chkOngoingMedicationsNoneCNP;

	@FindBy(xpath = "//button[text()='Next']")
	private WebElement btnNext;

	@FindBy(xpath = "//button[text()='Confirm and Proceed']")
	private WebElement btnConfirmAndProceed;

	@FindBy(xpath = "//label[text()='Select Medical Conditions']")
	private WebElement lblSelectMedicalConditions;

	@FindBy(xpath = "//i[text()='search']/../..//input")
	private WebElement txtSearchMedicalConditions;

	@FindBy(xpath = "//button[text()='Done']")
	private WebElement btnMedicalConditionsDone;

	@FindBy(xpath = "//span[contains(text(),'Yes, I Will Answer')]")
	private WebElement lblFurtherQuestionsYes;

	@FindBy(xpath = "//span[contains(text(),'No, Let Digit Team Call The Customer')]")
	private WebElement lblFurtherQuestionsNo;


	public void selectMedicalQuestions(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "undiagnosedsymptoms":
					for (String member : hmap.get(key).split(",")) {
						switch (member.replaceAll(" ", "").toLowerCase()) {
						case "none":
							chkUndiagnosedSymptomsNone.click();
							break;
						case "proposer":
						case "self":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("SELF_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("SELF_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "spouse":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("SPOUSE_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("SPOUSE_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "child1":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 1_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("CHILD 1_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "child2":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 2_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("CHILD 2_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "child3":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 3_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("CHILD 3_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "child4":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 4_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("CHILD 4_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "father":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("FATHER_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("FATHER_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "mother":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("MOTHER_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("MOTHER_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "fatherinlaw":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("FATHER_IN_LAW_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("FATHER_IN_LAW_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "motherinlaw":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("MOTHER_IN_LAW_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("MOTHER_IN_LAW_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						default:
							throw new Exception("There is no such member [" + member + "].");
						}
					}
					try {
						library.getDriver()
						.findElement(By.xpath("//input[@name='RPAMD_']/..//div[contains(text(),'Yes')]")).click();
					}
					catch(NoSuchElementException ne) {

					}

					break;
				case "preexistingconditions":
					for (String member : hmap.get(key).split(",")) {
						switch (member.replaceAll(" ", "").toLowerCase()) {
						case "none":
							chkPreExistingConditionsNone.click();
							break;
						case "proposer":
						case "self":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("SELF_firstName") + "']"))
							.click();
							break;
						case "spouse":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("SPOUSE_firstName") + "']"))
							.click();
							break;
						case "child1":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 1_firstName") + "']"))
							.click();
							break;
						case "child2":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 2_firstName") + "']"))
							.click();
							break;
						case "child3":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 3_firstName") + "']"))
							.click();
							break;
						case "child4":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 4_firstName") + "']"))
							.click();
							break;
						case "father":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("FATHER_firstName") + "']"))
							.click();
							break;
						case "mother":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("MOTHER_firstName") + "']"))
							.click();
							break;
						case "fatherinlaw":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("FATHER_IN_LAW_firstName") + "']"))
							.click();
							break;
						case "motherinlaw":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("MOTHER_IN_LAW_firstName") + "']"))
							.click();
							break;
						default:
							throw new Exception("There is no such member [" + member + "].");
						}
					}
					break;
				case "ongoingmedications":
					for (String member : hmap.get(key).split(",")) {
						switch (member.replaceAll(" ", "").toLowerCase()) {
						case "none":
							chkOngoingMedicationsNone.click();
							break;
						case "proposer":
						case "self":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
											+ Library.staticMap.get("SELF_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
									+ Library.staticMap.get("SELF_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "spouse":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
											+ Library.staticMap.get("SPOUSE_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
									+ Library.staticMap.get("SPOUSE_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "child1":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 1_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
									+ Library.staticMap.get("CHILD 1_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "child2":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 2_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
									+ Library.staticMap.get("CHILD 2_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "child3":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 3_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
									+ Library.staticMap.get("CHILD 3_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "child4":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 4_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
									+ Library.staticMap.get("CHILD 4_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "father":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
											+ Library.staticMap.get("FATHER_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
									+ Library.staticMap.get("FATHER_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "mother":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
											+ Library.staticMap.get("MOTHER_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
									+ Library.staticMap.get("MOTHER_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "fatherinlaw":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
											+ Library.staticMap.get("FATHER_IN_LAW_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
									+ Library.staticMap.get("FATHER_IN_LAW_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						case "motherinlaw":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
											+ Library.staticMap.get("MOTHER_IN_LAW_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
									+ Library.staticMap.get("MOTHER_IN_LAW_firstName") + "')]/..//input"))
							.sendKeys(library.getRandomValue("A | 50"));
							break;
						default:
							throw new Exception("There is no such member [" + member + "].");
						}

					}
				}
			}
		} catch (Exception e) {
			throw new Exception("Error while selecting the personal habits : " + e.getMessage());
		}
	}
	public void selectMedicalQuestionsFromDB(String details,String planId) throws Exception {
		try {
			DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				Thread.sleep(2000);
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "undiagnosedsymptoms":
					for (String member : hmap.get(key).split(",")) {
						switch (member.replaceAll(" ", "").toLowerCase()) {
						case "none":
							try {
								if(!chkUndiagnosedSymptomsNone.isSelected()) {
									library.waitForElementToBeClickable(chkUndiagnosedSymptomsNone, 10, 500);
									chkUndiagnosedSymptomsNone.click();
								}
							} catch(Exception e) {
								library.waitForElementToBeClickable(chkUndiagnosedSymptomsNoneCNP, 10, 500);
								chkUndiagnosedSymptomsNoneCNP.click();
							}
							break;
						case "proposer":
						case "self":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("SELF_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("SELF_firstName") + "')]/..//input"))
							.sendKeys(db.getMedicDataFromDB("undiagnosedsymptoms",planId));
							break;
						case "spouse":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("SPOUSE_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("SPOUSE_firstName") + "')]/..//input"))
							.sendKeys(db.getMedicDataFromDB("undiagnosedsymptoms",planId));
							break;
						case "child1":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 1_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("CHILD 1_firstName") + "')]/..//input"))
							.sendKeys(db.getMedicDataFromDB("undiagnosedsymptoms",planId));
							break;
						case "child2":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 2_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("CHILD 2_firstName") + "')]/..//input"))
							.sendKeys(db.getMedicDataFromDB("undiagnosedsymptoms",planId));
							break;
						case "child3":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 3_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("CHILD 3_firstName") + "')]/..//input"))
							.sendKeys(db.getMedicDataFromDB("undiagnosedsymptoms",planId));
							break;
						case "child4":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 4_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("CHILD 4_firstName") + "')]/..//input"))
							.sendKeys(db.getMedicDataFromDB("undiagnosedsymptoms",planId));
							break;
						case "father":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("FATHER_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("FATHER_firstName") + "')]/..//input"))
							.sendKeys(db.getMedicDataFromDB("undiagnosedsymptoms",planId));
							break;
						case "mother":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("MOTHER_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("MOTHER_firstName") + "')]/..//input"))
							.sendKeys(db.getMedicDataFromDB("undiagnosedsymptoms",planId));
							break;
						case "fatherinlaw":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("FATHER_IN_LAW_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("FATHER_IN_LAW_firstName") + "')]/..//input"))
							.sendKeys(db.getMedicDataFromDB("undiagnosedsymptoms",planId));
							break;
						case "motherinlaw":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'chest pain, fatigue, weight loss, dizziness, joint pain')]/../div//span[text()='"
											+ Library.staticMap.get("MOTHER_IN_LAW_firstName") + "']"))
							.click();
							library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'Details of "
									+ Library.staticMap.get("MOTHER_IN_LAW_firstName") + "')]/..//input"))
							.sendKeys(db.getMedicDataFromDB("undiagnosedsymptoms",planId));
							break;
						default:
							throw new Exception("There is no such member [" + member + "].");
						}
					}
					try {
						library.getDriver()
						.findElement(By.xpath("//input[@name='RPAMD_']/..//div[contains(text(),'Yes')]")).click();
					}
					catch(NoSuchElementException ne) {

					}

					break;
				case "preexistingconditions":
					for (String member : hmap.get(key).split(",")) {
						String diseases = "";
						if (!member.replaceAll(" ", "").toLowerCase().equals("none")) {
							diseases = db.getMedicDataFromDB("preexistingconditions",planId);
						}
						switch (member.replaceAll(" ", "").toLowerCase()) {
						case "none":
							try {
							if(!chkPreExistingConditionsNone.isSelected()) {
								chkPreExistingConditionsNone.click();
							}
							} catch(Exception e) {
								chkPreExistingConditionsNoneCNP.click();
							}
							break;
						case "proposer":
						case "self":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("SELF_firstName") + "']"))
							.click();
							Thread.sleep(2000);
							library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "+Library.staticMap.get("SELF_firstName")+" current and past medical conditions or disease?')]/../div//input"))
							.click();
							Thread.sleep(2000);
							getPEDFromDB(diseases);
							btnMedicalConditionsDone.click();
							break;
						case "spouse":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("SPOUSE_firstName") + "']"))
							.click();
							Thread.sleep(2000);
							library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "+Library.staticMap.get("SPOUSE_firstName")+" current and past medical conditions or disease?')]/../div//input"))
							.click();
							Thread.sleep(2000);
							getPEDFromDB(diseases);
							btnMedicalConditionsDone.click();
							break;
						case "child1":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 1_firstName") + "']"))
							.click();
							Thread.sleep(2000);
							library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "+Library.staticMap.get("CHILD 1_firstName")+" current and past medical conditions or disease?')]/../div//input"))
							.click();
							Thread.sleep(2000);
							getPEDFromDB(diseases);
							btnMedicalConditionsDone.click();
							break;
						case "child2":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 2_firstName") + "']"))
							.click();
							Thread.sleep(2000);
							library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "+Library.staticMap.get("CHILD 2_firstName")+" current and past medical conditions or disease?')]/../div//input"))
							.click();
							Thread.sleep(2000);
							getPEDFromDB(diseases);
							btnMedicalConditionsDone.click();
							break;
						case "child3":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 3_firstName") + "']"))
							.click();
							Thread.sleep(2000);
							library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "+Library.staticMap.get("CHILD 3_firstName")+" current and past medical conditions or disease?')]/../div//input"))
							.click();
							Thread.sleep(2000);
							getPEDFromDB(diseases);
							btnMedicalConditionsDone.click();
							break;
						case "child4":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("CHILD 4_firstName") + "']"))
							.click();
							Thread.sleep(2000);
							library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "+Library.staticMap.get("CHILD 4_firstName")+" current and past medical conditions or disease?')]/../div//input"))
							.click();
							Thread.sleep(2000);
							getPEDFromDB(diseases);
							btnMedicalConditionsDone.click();
							break;
						case "father":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("FATHER_firstName") + "']"))
							.click();
							Thread.sleep(2000);
							library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "+Library.staticMap.get("FATHER_firstName")+" current and past medical conditions or disease?')]/../div//input"))
							.click();
							Thread.sleep(2000);
							getPEDFromDB(diseases);
							btnMedicalConditionsDone.click();
							break;
						case "mother":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("MOTHER_firstName") + "']"))
							.click();
							Thread.sleep(2000);
							library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "+Library.staticMap.get("MOTHER_firstName")+" current and past medical conditions or disease?')]/../div//input"))
							.click();
							Thread.sleep(2000);
							getPEDFromDB(diseases);
							btnMedicalConditionsDone.click();
							break;
						case "fatherinlaw":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("FATHER_IN_LAW_firstName") + "']"))
							.click();
							Thread.sleep(2000);
							library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "+Library.staticMap.get("FATHER_IN_LAW_firstName")+" current and past medical conditions or disease?')]/../div//input"))
							.click();
							Thread.sleep(2000);
							getPEDFromDB(diseases);
							btnMedicalConditionsDone.click();
							break;
						case "motherinlaw":
							library.getDriver().findElement(By.xpath(
									"//label[contains(text(),'Do you have any existing condition')]/../div//span[text()='"
											+ Library.staticMap.get("MOTHER_IN_LAW_firstName") + "']"))
							.click();
							Thread.sleep(2000);
							library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "+Library.staticMap.get("MOTHER_IN_LAW_firstName")+" current and past medical conditions or disease?')]/../div//input"))
							.click();
							Thread.sleep(2000);
							getPEDFromDB(diseases);
							btnMedicalConditionsDone.click();
							break;
						default:
							throw new Exception("There is no such member [" + member + "].");
						}
					}
					break;
				case "ongoingmedications":
					try {
						for (String member : hmap.get(key).split(",")) {
							switch (member.replaceAll(" ", "").toLowerCase()) {
							case "none":
								try {
								if(!chkOngoingMedicationsNone.isSelected()) {
									chkOngoingMedicationsNone.click();
								}
								} catch(Exception e) {
									chkOngoingMedicationsNoneCNP.click();
								}
								break;
							case "proposer":
							case "self":
								library.getDriver().findElement(By.xpath(
										"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
												+ Library.staticMap.get("SELF_firstName") + "']"))
								.click();
								library.getDriver()
								.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
										+ Library.staticMap.get("SELF_firstName") + "')]/..//input"))
								.sendKeys(library.getRandomValue("A | 50"));
								break;
							case "spouse":
								library.getDriver().findElement(By.xpath(
										"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
												+ Library.staticMap.get("SPOUSE_firstName") + "']"))
								.click();
								library.getDriver()
								.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
										+ Library.staticMap.get("SPOUSE_firstName") + "')]/..//input"))
								.sendKeys(library.getRandomValue("A | 50"));
								break;
							case "child1":
								library.getDriver().findElement(By.xpath(
										"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
												+ Library.staticMap.get("CHILD 1_firstName") + "']"))
								.click();
								library.getDriver()
								.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
										+ Library.staticMap.get("CHILD 1_firstName") + "')]/..//input"))
								.sendKeys(library.getRandomValue("A | 50"));
								break;
							case "child2":
								library.getDriver().findElement(By.xpath(
										"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
												+ Library.staticMap.get("CHILD 2_firstName") + "']"))
								.click();
								library.getDriver()
								.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
										+ Library.staticMap.get("CHILD 2_firstName") + "')]/..//input"))
								.sendKeys(library.getRandomValue("A | 50"));
								break;
							case "child3":
								library.getDriver().findElement(By.xpath(
										"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
												+ Library.staticMap.get("CHILD 3_firstName") + "']"))
								.click();
								library.getDriver()
								.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
										+ Library.staticMap.get("CHILD 3_firstName") + "')]/..//input"))
								.sendKeys(library.getRandomValue("A | 50"));
								break;
							case "child4":
								library.getDriver().findElement(By.xpath(
										"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
												+ Library.staticMap.get("CHILD 4_firstName") + "']"))
								.click();
								library.getDriver()
								.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
										+ Library.staticMap.get("CHILD 4_firstName") + "')]/..//input"))
								.sendKeys(library.getRandomValue("A | 50"));
								break;
							case "father":
								library.getDriver().findElement(By.xpath(
										"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
												+ Library.staticMap.get("FATHER_firstName") + "']"))
								.click();
								library.getDriver()
								.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
										+ Library.staticMap.get("FATHER_firstName") + "')]/..//input"))
								.sendKeys(library.getRandomValue("A | 50"));
								break;
							case "mother":
								library.getDriver().findElement(By.xpath(
										"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
												+ Library.staticMap.get("MOTHER_firstName") + "']"))
								.click();
								library.getDriver()
								.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
										+ Library.staticMap.get("MOTHER_firstName") + "')]/..//input"))
								.sendKeys(library.getRandomValue("A | 50"));
								break;
							case "fatherinlaw":
								library.getDriver().findElement(By.xpath(
										"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
												+ Library.staticMap.get("FATHER_IN_LAW_firstName") + "']"))
								.click();
								library.getDriver()
								.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
										+ Library.staticMap.get("FATHER_IN_LAW_firstName") + "')]/..//input"))
								.sendKeys(library.getRandomValue("A | 50"));
								break;
							case "motherinlaw":
								library.getDriver().findElement(By.xpath(
										"//label[contains(text(),'going through any medications currently')]/../div//span[text()='"
												+ Library.staticMap.get("MOTHER_IN_LAW_firstName") + "']"))
								.click();
								library.getDriver()
								.findElement(By.xpath("//label[contains(text(),'Name of the medicines taken by "
										+ Library.staticMap.get("MOTHER_IN_LAW_firstName") + "')]/..//input"))
								.sendKeys(library.getRandomValue("A | 50"));
								break;
							default:
								throw new Exception("There is no such member [" + member + "].");
							}

						}
					}catch(Exception e) {

					}
				}
			}
		} catch (Exception e) {
			throw new Exception("Error while selecting the personal habits : " + e.getMessage());
		}
	}

	public void getPEDFromDB(String diseases) throws InterruptedException {
		try {
			for (String disease : diseases.split(",")) {
				library.getDriver().findElement(By.xpath("//span[contains(text(),'"+disease+"')]"))
				.click();
				Thread.sleep(2000);
			}
		} catch (Exception e) {
			library.getDriver().findElement(By.xpath("//span[text()='"+diseases+"']"))
			.click();
			Thread.sleep(2000);
		}
	}

	public void performAction(String action) throws Exception {
		try {
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "next":
				library.focus(library.getDriver(), btnNext);
				library.waitForElementToBeClickable(btnNext, 10, 500);
				btnNext.click();
				library.waitForDigitLoad();
				Thread.sleep(2000);
				try {
					btnConfirmAndProceed.click();
				} catch (NoSuchElementException ie) {
				}
				break;
			default:
				throw new Exception("There is no such action [" + action + "] in Family details page");
			}
		} catch (Exception e) {
			throw new Exception("Error while clicking the button in Family details : " + e.getMessage());
		}
	}

	public void selectGynaecologicalProblems(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			String name = "";
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "proposer":
				case "self":
					name = Library.staticMap.get("SELF_firstName");
					break;
				case "spouse":
					name = Library.staticMap.get("SPOUSE_firstName");
					break;
				case "child1":
					name = Library.staticMap.get("CHILD 1_firstName");
					break;
				case "child2":
					name = Library.staticMap.get("CHILD 2_firstName");
					break;
				case "child3":
					name = Library.staticMap.get("CHILD 3_firstName");
					break;
				case "child4":
					name = Library.staticMap.get("CHILD 4_firstName");
					break;
				case "mother":
					name = Library.staticMap.get("MOTHER_firstName");
					break;
				case "motherinlaw":
					name = Library.staticMap.get("MOTHER_IN_LAW_firstName");
					break;
				default:
					throw new Exception("There is no such member [" + key + "].");
				}
				switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
				case "yes":
					Thread.sleep(2000);
					library.waitForElementToBeClickable(library.getDriver().findElement(By.xpath("//input[@name='RPGYN_" + name + "']/..//div[contains(text(),'Yes')]")), 10, 500);
					library.getDriver()
					.findElement(
							By.xpath("//input[@name='RPGYN_" + name + "']/..//div[contains(text(),'Yes')]"))
					.click();
					library.getDriver()
					.findElement(By.xpath(
							"//label[contains(text(),'what is the name of the " + name + "')]/..//input"))
					.sendKeys(library.getRandomValue("A | 50"));
					break;
				case "no":
					Thread.sleep(2000);
					library.waitForElementToBeClickable(library.getDriver().findElement(By.xpath("//input[@name='RPGYN_" + name + "']/..//div[contains(text(),'No')]")), 10, 500);
					library.getDriver()
					.findElement(By.xpath("//input[@name='RPGYN_" + name + "']/..//div[contains(text(),'No')]"))
					.click();
					break;
				default:
					throw new Exception("Please provide either 'Yes' or 'No' in the data.");
				}
			}
		} catch (Exception e) {
			throw new Exception("Error while selecting the Gynaecological details : " + e.getMessage());
		}
	}

	public void answerFurtherMedicalQuestions(String disease, String familyMember, String details) throws Exception {
		try {
			String member = "";
			switch (familyMember.replaceAll(" ", "").toLowerCase()) {
			case "self":
			case "proposer":
				member = "SELF";
				break;
			case "spouse":
				member = "SPOUSE";
				break;
			case "child1":
				member = "CHILD 1";
				break;
			case "child2":
				member = "CHILD 2";
				break;
			case "child3":
				member = "CHILD 3";
				break;
			case "child4":
				member = "CHILD 4";
				break;
			case "father":
				member = "FATHER";
				break;
			case "mother":
				member = "MOTHER";
				break;
			case "fatherinlaw":
				member = "FATHER_IN_LAW";
				break;
			case "motherinlaw":
				member = "MOTHER_IN_LAW";
				break;
			default:
				throw new Exception("There is no such family member : " + familyMember);
			}
			switch (disease.replaceAll(" ", "").toLowerCase()) {
			case "thyroid":
				answerThyroidQuestions(member, details);
				break;
			case "diabetes":
				answerDiabetesQuestions(member, details);
				break;
			case "asthma":
				answerAsthmaQuestions(member, details);
				break;
			case "hyperlipidemia":
				answerHyperlipidemiaQuestions(member, details);
				break;
			case "hypertension":
				answerHypertensionQuestions(member, details);
				break;
			}
		} catch (Exception e) {
			throw new Exception("Error while answering further medical questions : " + e.getMessage());
		}
	}

	public void selectMedicalConditions(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "proposer":
				case "self":
					library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "
							+ Library.staticMap.get("SELF_firstName") + "')]/..//input")).click();
					break;
				case "spouse":
					library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "
							+ Library.staticMap.get("SPOUSE_firstName") + "')]/..//input")).click();
					break;
				case "child1":
					library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "
							+ Library.staticMap.get("CHILD 1_firstName") + "')]/..//input")).click();
					break;
				case "child2":
					library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "
							+ Library.staticMap.get("CHILD 2_firstName") + "')]/..//input")).click();
					break;
				case "child3":
					library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "
							+ Library.staticMap.get("CHILD 3_firstName") + "')]/..//input")).click();
					break;
				case "child4":
					library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "
							+ Library.staticMap.get("CHILD 4_firstName") + "')]/..//input")).click();
					break;
				case "father":
					library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "
							+ Library.staticMap.get("FATHER_firstName") + "')]/..//input")).click();
					break;
				case "mother":
					library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "
							+ Library.staticMap.get("MOTHER_firstName") + "')]/..//input")).click();
					break;
				case "fatherinlaw":
					library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "
							+ Library.staticMap.get("FATHER_IN_LAW_firstName") + "')]/..//input")).click();
					break;
				case "motherinlaw":
					library.getDriver().findElement(By.xpath("//label[contains(text(),'What are "
							+ Library.staticMap.get("MOTHER_IN_LAW_firstName") + "')]/..//input")).click();
					break;
				}
				Library.waitForVisibilityOfTheElement(lblSelectMedicalConditions);
				for (String temp : hmap.get(key).split(",")) {
					txtSearchMedicalConditions.sendKeys(temp);
					library.getDriver().findElement(By.xpath("//span[text()='" + temp + "']//ancestor::mat-checkbox")).click();
					btnMedicalConditionsDone.click();
					Thread.sleep(500);
				}
			}
		} catch (Exception e) {
			throw new Exception("Error while selecting the medical conditions : " + e.getMessage());
		}
	}

	public void answerFurtherMedicalQuestions(String selection) throws Exception {
		try {
			if (selection.trim().equalsIgnoreCase("yes")) {
				Thread.sleep(2000);
				lblFurtherQuestionsYes.click();
			} else {
				Thread.sleep(2000);
				lblFurtherQuestionsNo.click();
			}
		} catch (Exception e) {
			throw new Exception("Error while selecting Further medical questions : " + e.getMessage());
		}
	}

	private void answerThyroidQuestions(String member, String details) throws Exception {
		try {
			String memberName = Library.staticMap.get(member + "_firstName");
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "typeofthyroid":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "hyperthyroidism":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSTH1_" + memberName + "']/..//div[text()='Hyperthyroidism']"))
						.click();
						break;
					case "hypothyroidism":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSTH1_" + memberName + "']/..//div[text()='Hypothyroidism']"))
						.click();
						break;
					case "notsure":
						library.getDriver()
						.findElement(By
								.xpath("//input[@name='RSTH1_" + memberName + "']/..//div[text()='Not sure']"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Thyroid questions");
					}
					break;
				case "currentmedication":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "thyroxine":
						library.getDriver()
						.findElement(By
								.xpath("//input[@name='RSTH2_" + memberName + "']/..//div[text()='Thyroxine']"))
						.click();
						break;
					case "eltroxine":
						library.getDriver()
						.findElement(By
								.xpath("//input[@name='RSTH2_" + memberName + "']/..//div[text()='Eltroxine']"))
						.click();
						break;
					case "notlisted":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSTH2_" + memberName + "']/..//div[text()='Not listed']"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Thyroid questions");
					}
					break;
				case "whenwasitdiagnosed":
				case "whenwasitdiagnosed?":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "10yrsormore":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSTH3_" + memberName + "']/..//div[text()='10yrs or more']"))
						.click();
						break;
					case "lessthan5years":
					case "lessthan5yrs":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSTH3_" + memberName + "']/..//div[text()='Less than 5yrs']"))
						.click();
						break;
					case "5to9yrs":
					case "5to9years":
						library.getDriver()
						.findElement(By
								.xpath("//input[@name='RSTH3_" + memberName + "']/..//div[text()='5 to 9yrs']"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Thyroid questions");
					}
					break;
				case "anyassociatedcomplications":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "yes":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSTH4_" + memberName + "']/..//div[contains(text(),'Yes')]"))
						.click();
						break;
					case "no":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSTH4_" + memberName + "']/..//div[contains(text(),'No')]"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Thyroid questions");
					}
					break;
				default:
					throw new Exception("There is no such question [" + key + "] under Thyroid questions");
				}
			}
		} catch (Exception e) {
			throw new Exception("Error while answering the thyroid conditions : " + e.getMessage());
		}
	}

	private void answerDiabetesQuestions(String member, String details) throws Exception {
		try {
			String memberName = Library.staticMap.get(member + "_firstName");
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "typeofdiabetes":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "insulindependant":
						library.getDriver().findElement(By
								.xpath("//input[@name='RSDB1_" + memberName + "']/..//div[text()='Insulin dependant']"))
						.click();
						break;
					case "noninsulindependant":
						System.out.println(
								"//input[@name='RSDB1_" + memberName + "']/..//div[text()='Non Insulin dependant']");
						library.getDriver().findElement(By.xpath(
								"//input[@name='RSDB1_" + memberName + "']/..//div[text()='Non Insulin dependant']"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Diabetes questions");
					}
					break;
				case "currentmedication":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "insulinandoralmedicines":
						library.getDriver()
						.findElement(
								By.xpath("//input[@name='RSDB2_" + memberName + "']/..//div[text()='Insulin and Oral medicines']"))
						.click();
						break;
					case "dietcontrol":
						library.getDriver()
						.findElement(
								By.xpath("//input[@name='RSDB2_" + memberName + "']/..//div[text()='Diet control']"))
						.click();
						break;
					case "othermedication":
						library.getDriver()
						.findElement(
								By.xpath("//input[@name='RSDB2_" + memberName + "']/..//div[text()='Other medication']"))
						.click();
						break;
					case "insulin":
						library.getDriver()
						.findElement(
								By.xpath("//input[@name='RSDB2_" + memberName + "']/..//div[text()='Insulin']"))
						.click();
						break;
					case "oralmedication":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSDB2_" + memberName + "']/..//div[text()='Oral medication']"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Diabetes questions");
					}
					break;
				case "whenwasitdiagnosed":
				case "whenwasitdiagnosed?":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "morethan10yrs":
					case "morethan10years":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSDB3_" + memberName + "']/..//div[text()='More than 10yrs']"))
						.click();
						break;
					case "upto10years":
					case "upto10yrs":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSDB3_" + memberName + "']/..//div[text()='Upto 10yrs']"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Diabetes questions");
					}
					break;
				case "lastbloodsugartestdone":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "notdoneinlast6months":
						library.getDriver().findElement(By.xpath("//input[@name='RSDB4_" + memberName
								+ "']/..//div[text()='Not done in last 6 months']")).click();
						break;
					case "lessthan3months":
						library.getDriver().findElement(By.xpath(
								"//input[@name='RSDB4_" + memberName + "']/..//div[text()='Less than 3 months']"))
						.click();
						break;
					case "morethan3months":
						library.getDriver().findElement(By.xpath(
								"//input[@name='RSDB4_" + memberName + "']/..//div[text()='More than 3 months']"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Diabetes questions");
					}
					break;
				case "hba1cvalue":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "upto7.5%":
						library.getDriver()
						.findElement(By
								.xpath("//input[@name='RSDB9_" + memberName + "']/..//div[text()='upto 7.5%']"))
						.click();
						break;
					case "morethan7.5%":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSDB9_" + memberName + "']/..//div[text()='More than 7.5%']"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Diabetes questions");
					}
					break;
				case "hospitalizedduetodiabetes":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "yes":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSDB5_" + memberName + "']/..//div[contains(text(),'Yes')]"))
						.click();
						break;
					case "no":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSDB5_" + memberName + "']/..//div[contains(text(),'No')]"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Diabetes questions");
					}
					break;
				case "recordsofhospitalization":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "yes":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSDB6_" + memberName + "']/..//div[contains(text(),'Yes')]"))
						.click();
						break;
					case "no":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSDB6_" + memberName + "']/..//div[contains(text(),'No')]"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Diabetes questions");
					}
					break;
				case "anycomplicationsfordiabetes":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "yes":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSDB7_" + memberName + "']/..//div[contains(text(),'Yes')]"))
						.click();
						break;
					case "no":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSDB7_" + memberName + "']/..//div[contains(text(),'No')]"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Diabetes questions");
					}
					break;
				case "recordsofcomplications":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "yes":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSDB8_" + memberName + "']/..//div[contains(text(),'Yes')]"))
						.click();
						break;
					case "no":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSDB8_" + memberName + "']/..//div[contains(text(),'No')]"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Diabetes questions");
					}
					break;
				default:
					throw new Exception("There is no such question [" + key + "] under Diabetes questions");
				}
			}
		} catch (Exception e) {
			throw new Exception("Error while answering the Diabetes conditions : " + e.getMessage());
		}
	}

	private void answerAsthmaQuestions(String member, String details) throws Exception {
		try {
			String memberName = Library.staticMap.get(member + "_firstName");
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "currentmedication":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "none":
						library.getDriver()
						.findElement(
								By.xpath("//input[@name='RSAS2_" + memberName + "']/..//div[text()='None']"))
						.click();
						break;
					case "nonsteroids":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSAS2_" + memberName + "']/..//div[text()='Non Steroids']"))
						.click();
						break;
					case "steroids":
						library.getDriver()
						.findElement(By
								.xpath("//input[@name='RSAS2_" + memberName + "']/..//div[text()='Steroids']"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Asthma questions");
					}
					break;
				case "whenwasitdiagnosed":
				case "whenwasitdiagnosed?":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "morethan10yrs":
					case "morethan10years":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSAS1_" + memberName + "']/..//div[text()='More than 10yrs']"))
						.click();
						break;
					case "upto10years":
					case "upto10yrs":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSAS1_" + memberName + "']/..//div[text()='Upto 10yrs']"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Asthma questions");
					}
					break;
				case "timessufferedwithasthma":
				case "timessuffered":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "lessthan2":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSAS3_" + memberName + "']/..//div[text()='Less than 2']"))
						.click();
						break;
					case "morethan5":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSAS3_" + memberName + "']/..//div[text()='More than 5']"))
						.click();
						break;
					case "2to5":
						library.getDriver()
						.findElement(
								By.xpath("//input[@name='RSAS3_" + memberName + "']/..//div[text()='2 to 5']"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Asthma questions");
					}
					break;
				case "hospitalizedduetoasthma":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "yes":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSAS4_" + memberName + "']/..//div[contains(text(),'Yes')]"))
						.click();
						break;
					case "no":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSAS4_" + memberName + "']/..//div[contains(text(),'No')]"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Asthma questions");
					}
					break;
				case "recordsofhospitalization":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "yes":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSAS5_" + memberName + "']/..//div[contains(text(),'Yes')]"))
						.click();
						break;
					case "no":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSAS5_" + memberName + "']/..//div[contains(text(),'No')]"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Asthma questions");
					}
					break;
				case "anycomplicationsduetoasthma":
				case "complicationsduetoasthma":
				case "complicationsforasthma":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "yes":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSAS6_" + memberName + "']/..//div[contains(text(),'Yes')]"))
						.click();
						break;
					case "no":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSAS6_" + memberName + "']/..//div[contains(text(),'No')]"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Asthma questions");
					}
					break;
				case "recordsofcomplications":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "yes":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSAS7_" + memberName + "']/..//div[contains(text(),'Yes')]"))
						.click();
						break;
					case "no":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSAS7_" + memberName + "']/..//div[contains(text(),'No')]"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Asthma questions");
					}
					break;
				default:
					throw new Exception("There is no such question [" + key + "] under Asthma questions");
				}
			}
		} catch (Exception e) {
			throw new Exception("Error while answering the Asthma conditions : " + e.getMessage());
		}
	}

	private void answerHypertensionQuestions(String member, String details) throws Exception {
		try {
			String memberName = Library.staticMap.get(member + "_firstName");
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {

				case "whenwasitdiagnosed":
				case "whenwasitdiagnosed?":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "morethan10yrs":
					case "morethan10years":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP1_" + memberName + "']/..//div[text()='More than 10yrs']"))
						.click();
						break;
					case "upto10years":
					case "upto10yrs":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP1_" + memberName + "']/..//div[text()='Upto 10yrs']"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Hypertension questions");
					}
					break;
				case "no.oftablets":
				case "nooftablets":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "twoorless":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP2_" + memberName + "']/..//div[text()='two or less']"))
						.click();
						break;
					case "morethantwo":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP2_" + memberName + "']/..//div[text()='More than two']"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Hypertension questions");
					}
					break;
				case "systolicreading":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "130mmofhgorless":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP3_" + memberName + "']/..//div[text()='130 mm of Hg or less']"))
						.click();
						break;
					case "morethan130to149mmofhg":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP3_" + memberName + "']/..//div[text()='More than 130 to 149 mm of Hg']"))
						.click();
						break;
					case "150mmofhgormore":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP3_" + memberName + "']/..//div[text()='150 mm of Hg or more']"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Hypertension questions");
					}
					break;
				case "diastolicreading":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "90mmofhgorless":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP4_" + memberName + "']/..//div[text()='90 mm of Hg or less']"))
						.click();
						break;
					case "90to95mmofhg":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP4_" + memberName + "']/..//div[text()='90 to 95 mm of Hg']"))
						.click();
						break;
					case "morethan95mmofhg":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP4_" + memberName + "']/..//div[text()='More than 95 mm of Hg']"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Hypertension questions");
					}
					break;
				case "hospitalizedduetohypertension":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "yes":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP8_" + memberName + "']/..//div[contains(text(),'Yes')]"))
						.click();
						break;
					case "no":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP8_" + memberName + "']/..//div[contains(text(),'No')]"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Hypertension questions");
					}
					break;
				case "recordsofhospitalization":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "yes":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP5_" + memberName + "']/..//div[contains(text(),'Yes')]"))
						.click();
						break;
					case "no":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP5_" + memberName + "']/..//div[contains(text(),'No')]"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Hypertension questions");
					}
					break;
				case "anycomplicationsrelatedtohypertension":
				case "complicationsrelatedtohypertension":
				case "complicationsforhypertension":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "yes":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP6_" + memberName + "']/..//div[contains(text(),'Yes')]"))
						.click();
						break;
					case "no":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP6_" + memberName + "']/..//div[contains(text(),'No')]"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Hypertension questions");
					}
					break;
				case "recordsofcomplications":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "yes":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP7_" + memberName + "']/..//div[contains(text(),'Yes')]"))
						.click();
						break;
					case "no":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSHP7_" + memberName + "']/..//div[contains(text(),'No')]"))
						.click();
						break;
					default:
						throw new Exception(
								"The given option [" + hmap.get(key) + "] is not available under Hypertension questions");
					}
					break;
				default:
					throw new Exception("There is no such question [" + key + "] under Hypertension questions");
				}
			}
		} catch (Exception e) {
			throw new Exception("Error while answering the Hypertension conditions : " + e.getMessage());
		}
	}

	private void answerHyperlipidemiaQuestions(String member, String details) throws Exception {
		try {
			String memberName = Library.staticMap.get(member + "_firstName");
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "onmedication":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "yes":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSLD1_" + memberName + "']/..//div[contains(text(),'Yes')]"))
						.click();
						break;
					case "no":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSLD1_" + memberName + "']/..//div[contains(text(),'No')]"))
						.click();
						break;
					default:
						throw new Exception("The given option [" + hmap.get(key)
						+ "] is not available under Hyperlipidemia questions");
					}
					break;
				case "anycomplicationswithhighlipids":
				case "anycomplications":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "yes":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSLD2_" + memberName + "']/..//div[contains(text(),'Yes')]"))
						.click();
						break;
					case "no":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSLD2_" + memberName + "']/..//div[contains(text(),'No')]"))
						.click();
						break;
					default:
						throw new Exception("The given option [" + hmap.get(key)
						+ "] is not available under Hyperlipidemia questions");
					}
					break;
				case "whenwasitdiagnosed":
				case "whenwasitdiagnosed?":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "5to9years":
					case "5to9yrs":
						library.getDriver()
						.findElement(By
								.xpath("//input[@name='RSLD3_" + memberName + "']/..//div[text()='5 to 9yrs']"))
						.click();
						break;
					case "lessthan5yrs":
					case "lessthan5years":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSLD3_" + memberName + "']/..//div[text()='Less than 5yrs']"))
						.click();
						break;
					case "10yearsormore":
					case "10yrsormore":
						library.getDriver()
						.findElement(By.xpath(
								"//input[@name='RSLD3_" + memberName + "']/..//div[text()='10yrs or more']"))
						.click();
						break;
					default:
						throw new Exception("The given option [" + hmap.get(key)
						+ "] is not available under Hyperlipidemia questions");
					}
					break;
				}
			}
		} catch (Exception e) {
			throw new Exception("Error while answering the Hyperlipidemia conditions : " + e.getMessage());
		}
	}
}
