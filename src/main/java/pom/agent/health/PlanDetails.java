package pom.agent.health;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.utilities.DBUtility;

public class PlanDetails {

	com.utilities.Library library = new com.utilities.Library();

	@FindBy(xpath = "//div[contains(text(),'Floater SI')]/../..")
	private WebElement radFloater;

	@FindBy(xpath = "//div[contains(text(),'Individual SI')]/../..")
	private WebElement radIndividual;

	@FindBy(xpath = "//label[contains(text(),'Family Sum Insured')]/..//mat-select")
	private WebElement drpFamilySumInsured;

	@FindAll({ @FindBy(xpath = "//mat-option/span") })
	public List<WebElement> drpFamilySumInsuredOptions;

	@FindBy(xpath = "//label[contains(text(),'SI for')]/..//mat-select")
	private WebElement drpIndividualSumInsured;
	
	@FindBy(xpath = "(//label[contains(text(),'Increase SI')]/..//div)[1]")
	private WebElement switchIncreaseSI;
	
	@FindBy(xpath = "//div[text()='25000']")
	private WebElement radio25k;
	
	@FindBy(xpath = "//div[text()='50000']")
	private WebElement radio50k;
	
	

	// @FindAll({ @FindBy(xpath = "//mat-option/span") })
	// public List<WebElement> drpFamilySumInsuredOptions;

	@FindBy(xpath = "//label[text()='Policy Term']/..//mat-select")
	private WebElement drpPolicyTerm;

	@FindBy(xpath = "//button[contains(text(),'Recalculate')]")
	private WebElement btnRecalculate;

	@FindBy(xpath = "//button[contains(text(),'Proceed')]")
	private WebElement btnProceed;

	@FindBy(xpath = "//label[text()='Maternity Benefit & New Born Baby Cover']/../..//mat-checkbox")
	private WebElement chkMaternityBenefit;

	@FindBy(xpath = "//mat-radio-group[contains(@ng-reflect-name,'Maternity Benefit & New Born B')]//mat-radio-button[@ng-reflect-value='INR 20000.00']")
	private WebElement radMaternitySumInsured20000;

	@FindBy(xpath = "//mat-radio-group[contains(@ng-reflect-name,'Maternity Benefit & New Born B')]//mat-radio-button[@ng-reflect-value='INR 25000.00']")
	private WebElement radMaternitySumInsured25000;

	@FindBy(xpath = "//mat-radio-group[contains(@ng-reflect-name,'Waiting Period ')]//mat-radio-button[@ng-reflect-value='INR 2.00']")
	private WebElement radWaitingPeriod2years;

	@FindBy(xpath = "//mat-radio-group[contains(@ng-reflect-name,'Waiting Period ')]//mat-radio-button[@ng-reflect-value='INR 3.00']")
	private WebElement radWaitingPeriod3years;

	@FindBy(xpath = "//mat-radio-group[contains(@ng-reflect-name,'Waiting Period ')]//mat-radio-button[@ng-reflect-value='INR 4.00']")
	private WebElement radWaitingPeriod4years;

	@FindBy(xpath = "//mat-radio-group[contains(@ng-reflect-name,'Maternity SI for the second ch')]//mat-radio-button[@ng-reflect-value='INR 100.00']")
	private WebElement radSIforSecondChild100;

	@FindBy(xpath = "//mat-radio-group[contains(@ng-reflect-name,'Maternity SI for the second ch')]//mat-radio-button[@ng-reflect-value='INR 200.00']")
	private WebElement radSIforSecondChild200;

	@FindBy(xpath = "//label[text()='Daily Hospital Cash Cover']/../..//mat-checkbox")
	private WebElement chkDailyHospitalCashCover;

	@FindBy(xpath = "//mat-radio-group[contains(@ng-reflect-name,'Daily Hospital Cash Cover')]//mat-radio-button[@ng-reflect-value='INR 500.00']")
	private WebElement radDailyHospitalCashCover500;

	@FindBy(xpath = "//mat-radio-group[contains(@ng-reflect-name,'Daily Hospital Cash Cover')]//mat-radio-button[@ng-reflect-value='INR 1000.00']")
	private WebElement radDailyHospitalCashCover1000;

	@FindBy(xpath = "//mat-radio-group[contains(@ng-reflect-name,'Daily Hospital Cash Cover Days')]//mat-radio-button[@ng-reflect-value='INR 30.00']")
	private WebElement radCashCoverDays30;

	@FindBy(xpath = "//mat-radio-group[contains(@ng-reflect-name,'Daily Hospital Cash Cover Dedu')]//mat-radio-button[@ng-reflect-value='INR 1.00']")
	private WebElement radCashCoverDeductibleDays1days;

	@FindBy(xpath = "//label[text()='Out-Patient (OPD) Benefit']/../..//mat-checkbox")
	private WebElement chkOutPatientBenefit;

	@FindBy(xpath = "//mat-radio-group[contains(@ng-reflect-name,'Out-Patient (OPD) Benefit')]//mat-radio-button[@ng-reflect-value='INR 2500.00']")
	private WebElement radOutPatientBenefit2500;

	@FindBy(xpath = "//mat-radio-group[contains(@ng-reflect-name,'Out-Patient (OPD) Benefit')]//mat-radio-button[@ng-reflect-value='INR 5000.00']")
	private WebElement radOutPatientBenefit5000;

	@FindBy(xpath = "//mat-radio-group[contains(@ng-reflect-name,'Co-payment')]//mat-radio-button[@ng-reflect-value='INR 0.00']")
	private WebElement radCoPaymentPercentage0;

	@FindBy(xpath = "//mat-radio-group[contains(@ng-reflect-name,'Co-payment')]//mat-radio-button[@ng-reflect-value='INR 10.00']")
	private WebElement radCoPaymentPercentage10;
	
	@FindBy(xpath = "//label[contains(text(),'Quote Premium')]/../h4")
	private WebElement quotePremium;
	
	static int oldQuote;
	
	

	public void enterPlanDetails(String details,String planId) throws Exception {
		library.waitForDigitLoad();
		oldQuote = Integer.parseInt(quotePremium.getText().replaceAll(",", "").trim().split(" ")[1]);
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "sitype":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "floater":
					case "floatersi":
						try {
							radFloater.click();
						}catch(Exception e) {

						}
						break;
					case "individual":
					case "individualsi":
						library.waitForElementToBeClickable(radIndividual, 10, 500);
						radIndividual.click();
						break;
					}
					break;
				case "suminsured":
					int i = 1;
					if (radFloater.getAttribute("class").contains("mat-radio-checked")) {
						library.waitForElementToBeClickable(drpFamilySumInsured, 5, 500);
						drpFamilySumInsured.click();
						for (WebElement temp : drpFamilySumInsuredOptions) {
							try {
								while(i==1) {
									i++;
									DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
									String minSIBasedOnCityZoneFromDB = db.getMinSI_BasedOnCityZoneFromDB(planId).replace(",", "");
									if (DBUtility.cityZoneFlag.equals("A") || DBUtility.cityZoneFlag.equals("B")) {
										if (!AgeDetails.proposerIsMoreThan45YrsOfAge) {
											if (temp.getText().replaceAll(",", "").replaceAll("₹", "").replaceAll(" ", "").equals(minSIBasedOnCityZoneFromDB)) {
												System.out.println("~~~~~~~~~~~Validation Successful for minSI based on city zone~~~~~~~~~~~~~");
											} else {
												throw new Exception("xxxxxxxxxxx	Validation Failed for minSI based on city zone	 xxxxxxxxxxxxxx");
											}
										} else {
											if (!temp.getText().replaceAll(",", "").replaceAll("₹", "").replaceAll(" ", "").equals(minSIBasedOnCityZoneFromDB)) {
												System.out.println("~~~~~~~~~~~Validation Successful for minSI based on city zone~~~~~~~~~~~~~");
											} else {
												throw new Exception("xxxxxxxxxxx	Validation Failed for minSI based on city zone	 xxxxxxxxxxxxxx");
											}
										}
									} else if(DBUtility.cityZoneFlag.equals("C")) {
										if (temp.getText().replaceAll(",", "").replaceAll("₹", "").replaceAll(" ", "").equals(minSIBasedOnCityZoneFromDB)) {
											System.out.println("~~~~~~~~~~~Validation Successful for minSI based on city zone~~~~~~~~~~~~~");
										} else {
											throw new Exception("xxxxxxxxxxx	Validation Failed for minSI based on city zone	 xxxxxxxxxxxxxx");
										}
									}
								}
							} catch (Exception e1) {
								
							}
							if (temp.getText().replaceAll(",", "").replaceAll("₹", "").replaceAll(" ", "").equals(
									hmap.get(key).replaceAll(",", "").replaceAll("₹", "").replaceAll(" ", ""))) {
								try {
									temp.click();
								} catch (ElementClickInterceptedException e) {
									Thread.sleep(2000);library.waitForElementToBeClickable(temp, 5, 500);
									temp.click();
								}
								break;
							}
						}
					} else {
						library.waitForElementToBeClickable(drpIndividualSumInsured, 5, 500);
						drpIndividualSumInsured.click();
						for (WebElement temp : drpFamilySumInsuredOptions) {
							try {
								while(i==1) {
									i++;
									DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
									String minSIBasedOnCityZoneFromDB = db.getMinSI_BasedOnCityZoneFromDB(planId).replace(",", "");
									if (DBUtility.cityZoneFlag.equals("A") || DBUtility.cityZoneFlag.equals("B")) {
										if (!AgeDetails.proposerIsMoreThan45YrsOfAge) {
											if (temp.getText().replaceAll(",", "").replaceAll("₹", "").replaceAll(" ", "").equals(minSIBasedOnCityZoneFromDB)) {
												System.out.println("~~~~~~~~~~~Validation Successful for minSI based on city zone~~~~~~~~~~~~~");
											} else {
												throw new Exception("xxxxxxxxxxx	Validation Failed for minSI based on city zone	 xxxxxxxxxxxxxx");
											}
										} else {
											if (!temp.getText().replaceAll(",", "").replaceAll("₹", "").replaceAll(" ", "").equals(minSIBasedOnCityZoneFromDB)) {
												System.out.println("~~~~~~~~~~~Validation Successful for minSI based on city zone~~~~~~~~~~~~~");
											} else {
												throw new Exception("xxxxxxxxxxx	Validation Failed for minSI based on city zone	 xxxxxxxxxxxxxx");
											}
										}
									} else if(DBUtility.cityZoneFlag.equals("C")) {
										if (temp.getText().replaceAll(",", "").replaceAll("₹", "").replaceAll(" ", "").equals(minSIBasedOnCityZoneFromDB)) {
											System.out.println("~~~~~~~~~~~Validation Successful for minSI based on city zone~~~~~~~~~~~~~");
										} else {
											throw new Exception("xxxxxxxxxxx	Validation Failed for minSI based on city zone	 xxxxxxxxxxxxxx");
										}
									}
								}
							} catch (Exception e1) {
								
							}
							if (temp.getText().replaceAll(",", "").replaceAll("₹", "").replaceAll(" ", "").equals(
									hmap.get(key).replaceAll(",", "").replaceAll("₹", "").replaceAll(" ", ""))) {
								try {
									temp.click();
								} catch (ElementClickInterceptedException e) {
									Thread.sleep(2000);library.waitForElementToBeClickable(temp, 5, 500);
									temp.click();
								}
								break;
							}
						}
					}
					break;
				case "policyterm":
					drpPolicyTerm.click();
					Thread.sleep(500);
					for (WebElement temp : drpFamilySumInsuredOptions) {
						if (temp.getText().replaceAll(" ", "").equalsIgnoreCase(hmap.get(key).replaceAll(" ", "")))
							try {
								temp.click();
							} catch (ElementClickInterceptedException e) {
								Thread.sleep(2000);library.waitForElementToBeClickable(temp, 5, 500);
								temp.click();
							}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the plan details : " + e.getMessage());
		}
	}

	public void performAction(String action) throws Exception {
		try {
			try {
				switch (action.replaceAll(" ", "").toLowerCase()) {
				case "recalculate":
					btnRecalculate.click();
					library.waitForDigitLoad();
					int newQuote = Integer.parseInt(quotePremium.getText().replaceAll(",", "").trim().split(" ")[1]);

					if (newQuote==oldQuote) {
						throw new Exception("Quote Validation FAIL!!");

					}else {
						System.out.println("Quote Validation PASS!!");
					}
					break;
				case "proceed":
					library.waitForDigitLoad();
					btnProceed.click();
					library.waitForDigitLoad();
					break;
				case "default":
					throw new Exception("There is no such action [" + action + "] in  the plan details page");
				}

			} catch (NoSuchElementException e) {

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the plan details : " + e.getMessage());
		}
	}

	public void enterMaternityBenefitDetails(String details) throws Exception {
		try {
			if (!chkMaternityBenefit.getAttribute("class").contains("mat-checkbox-checked"))
				chkMaternityBenefit.click();
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "suminsured":
					switch (hmap.get(key).replaceAll(" ", "")) {
					case "20000":
						radMaternitySumInsured20000.click();
						break;
					case "25000":
						radMaternitySumInsured25000.click();
						break;
					default:
						throw new Exception("The given Sum Insured [" + hmap.get(key) + "] is not valid.");
					}
					break;
				case "waitingperiod":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "2years":
						radWaitingPeriod2years.click();
						break;
					case "3years":
						radWaitingPeriod3years.click();
						break;
					case "4years":
						radWaitingPeriod4years.click();
						break;
					default:
						throw new Exception("The given Waiting Period [" + hmap.get(key) + "] is not valid.");
					}
					break;
				case "siforsecondchild":
				case "maternitysiforthesecondchild":
				case "maternitysiforsecondchild":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "100%":
					case "100":
						radSIforSecondChild100.click();
						break;
					case "200%":
					case "200":
						radSIforSecondChild200.click();
						break;
					default:
						throw new Exception("The given SI for Second Child [" + hmap.get(key) + "] is not valid.");
					}
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the Maternity Benefit details : " + e.getMessage());
		}

	}

	public void enterDailyHospitalCashCoverDetails(String details) throws Exception {
		try {
			if (!chkDailyHospitalCashCover.getAttribute("class").contains("mat-checkbox-checked"))
				chkDailyHospitalCashCover.click();
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "suminsured":
					switch (hmap.get(key).replaceAll(" ", "")) {
					case "500":
						radDailyHospitalCashCover500.click();
						break;
					case "1000":
						radDailyHospitalCashCover1000.click();
						break;
					default:
						throw new Exception("The given Sum Insured [" + hmap.get(key) + "] is not valid.");
					}
					break;
				case "cashcoverdayslimit":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "30days":
						radCashCoverDays30.click();
						break;
					default:
						throw new Exception("The given Cash Cover Days Limit [" + hmap.get(key) + "] is not valid.");
					}
					break;
				case "cashcoverdeductibledays":
					switch (hmap.get(key).replaceAll(" ", "").toLowerCase()) {
					case "1days":
					case "1day":
						radCashCoverDeductibleDays1days.click();
						break;
					default:
						throw new Exception(
								"The given Cash Cover Deductible Days [" + hmap.get(key) + "] is not valid.");
					}
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the Daily Hospital Cash Cover details : " + e.getMessage());
		}
	}

	public void enterOutPatientBenefitDetails(String details) throws Exception {
		try {
			if (!chkOutPatientBenefit.getAttribute("class").contains("mat-checkbox-checked"))
				chkOutPatientBenefit.click();
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "suminsured":
					switch (hmap.get(key).replaceAll(" ", "")) {
					case "2500":
						radOutPatientBenefit2500.click();
						break;
					case "5000":
						radOutPatientBenefit2500.click();
						break;
					default:
						throw new Exception("The given Sum Insured [" + hmap.get(key) + "] is not valid.");
					}
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the Out Patient details : " + e.getMessage());
		}
	}

	public void enterCoPaymentDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "percentage":
					switch (hmap.get(key).replaceAll(" ", "")) {
					case "0%":
					case "0":
						radCoPaymentPercentage0.click();
						break;
					case "10%":
					case "10":
						radCoPaymentPercentage10.click();
						break;
					default:
						throw new Exception("The given Percentage [" + hmap.get(key) + "] is not valid.");
					}
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the Co Payment details : " + e.getMessage());
		}
	}

	public void selectAllCoverages() throws Exception
	{
		try {
			chkDailyHospitalCashCover.click();
			chkOutPatientBenefit.click();
		}
		catch(Exception e)
		{
			throw new Exception("Error while selecting all the coverages ==> "+e.getMessage());
		}
	}

	public void increaseSI(String portId) throws Exception {
		
		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String value = db.getPortabilityDataFromDB("increasesiby", portId);
		
		
			try {
				switch(value.toLowerCase()) {
				case "none":
					switchIncreaseSI.click();
					break;
				case "25000":
					radio25k.click();
					break;
				case "50000":
					radio50k.click();
					break;
				default:
					throw new Exception("There is no "+value+" present in UI");
				}
			} catch (NoSuchElementException e) {
				
			}
		
	}
}

