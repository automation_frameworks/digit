package pom.agent.health;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.utilities.Library;

public class DigitHealthCarePlusPolicy {

	@FindBy(xpath = "//button[contains(text(),'Create New Policy')]")
	private WebElement btnCreateNewPolicy;

	@FindBy(xpath = "//input[@value = 'Your Custom Plans']")
	private WebElement btnCustomPlan;

	@FindBy(xpath = "//input[@value = 'Portability']")
	private WebElement btnPortability;

	@FindBy(xpath = "//h4[text() = 'Download Material']/..//li[1]/a")
	private WebElement download_RH_Zone_A_Link;

	@FindBy(xpath = "//h4[text() = 'Download Material']/..//li[2]/a")
	private WebElement download_RH_Zone_B_Link;

	@FindBy(xpath = "//h4[text() = 'Download Material']/..//li[3]/a")
	private WebElement download_RH_Zone_C_Link;

	@FindBy(xpath = "//h4[text() = 'Download Material']/..//li[4]/a")
	private WebElement download_HCP_PolicyWordingLink;

	@FindBy(xpath = "//h4[text() = 'Download Material']/..//li[5]/a")
	private WebElement downloadHealthDocumentLink;

	@FindBy(xpath = "//h4[text() = 'Download Material']/..//li[6]/a")
	private WebElement download_AS_Prospectus_Link;

	@FindBy(xpath = "//h4[text() = 'Download Material']/..//li[7]/a")
	private WebElement download_AS_Wording_Link;

	@FindBy(xpath = "//h4[text() = 'Download Material']/..//li[8]/a")
	private WebElement download_AS_Policy_Link;


	public void select(String option) throws Exception {
		try {
			com.utilities.Library library = new com.utilities.Library();
			library.waitForDigitLoad();
			switch (option.toLowerCase().replaceAll(" ", "")) {
			case "createnewpolicy":
				Library.waitForVisibilityOfTheElement(btnCreateNewPolicy);
				btnCreateNewPolicy.click();
				break;
			case "yourcustomplans":
				Library.waitForVisibilityOfTheElement(btnCustomPlan);
				btnCustomPlan.click();
				break;
			case "portability":
				Library.waitForVisibilityOfTheElement(btnPortability);
				btnPortability.click();
				break;
			case "retailhealthzonea":
				Library.waitForVisibilityOfTheElement(download_RH_Zone_A_Link);
				download_RH_Zone_A_Link.click();
				break;
			case "retailhealthzoneb":
				Library.waitForVisibilityOfTheElement(download_RH_Zone_B_Link);
				download_RH_Zone_B_Link.click();
				break;
			case "retailhealthzonec":
				Library.waitForVisibilityOfTheElement(download_RH_Zone_C_Link);
				download_RH_Zone_C_Link.click();
				break;
			case "healthcarepluspolicywording":
				Library.waitForVisibilityOfTheElement(download_HCP_PolicyWordingLink);
				download_HCP_PolicyWordingLink.click();
				break;
			case "healthdocumentdownloadinstruction":
				Library.waitForVisibilityOfTheElement(downloadHealthDocumentLink);
				downloadHealthDocumentLink.click();
				break;
			case "arogyasanjeevani-prospectus":
				Library.waitForVisibilityOfTheElement(download_AS_Prospectus_Link);
				download_AS_Prospectus_Link.click();
				break;
			case "arogyasanjeevani-wording":
				Library.waitForVisibilityOfTheElement(download_AS_Wording_Link);
				download_AS_Wording_Link.click();
				break;
			case "arogyasanjeevani-policy_brochre":
				Library.waitForVisibilityOfTheElement(download_AS_Policy_Link);
				download_AS_Policy_Link.click();
				break;
			default:
				throw new Exception("The option [" + option + "] expeceted to be clicked is not present");
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	/*
	 * public WebElement createNewPolicy() { return btnCreateNewPolicy; }
	 * 
	 * public WebElement yourCustomPlan() { return btnCustomPlan; }
	 * 
	 * public WebElement portability() { return btnPortability; }
	 * 
	 * public WebElement getRetailHealthZoneA() { return download_RH_Zone_A_Link; }
	 * 
	 * public WebElement getRetailHealthZoneB() { return download_RH_Zone_B_Link; }
	 * 
	 * public WebElement getRetailHealthZoneC() { return download_RH_Zone_C_Link; }
	 * 
	 * public WebElement getHealthCarePlusPolicyWording() { return
	 * download_HCP_PolicyWordingLink; }
	 * 
	 * public WebElement getHealthDocumentDownloadInstruction() { return
	 * downloadHealthDocumentLink; }
	 * 
	 * public WebElement getArogyaSanjeevaniProspectus() { return
	 * download_AS_Prospectus_Link; }
	 * 
	 * public WebElement getArogyaSanjeevaniWording() { return
	 * download_AS_Wording_Link; }
	 * 
	 * public WebElement getArogyaSanjeevaniPolicyBrochre() { return
	 * download_AS_Policy_Link; }
	 */

}
