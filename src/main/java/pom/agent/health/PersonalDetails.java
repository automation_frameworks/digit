package pom.agent.health;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.utilities.DBUtility;
import com.utilities.Library;

public class PersonalDetails {

	com.utilities.Library library = new com.utilities.Library();

	@FindBy(xpath = "//button[text()='Next']")
	private WebElement btnNext;

	@FindAll({ @FindBy(xpath = "//tbody[@class='results']/tr[1]/td") })
	private List<WebElement> lstFirsttName;

	@FindAll({ @FindBy(xpath = "//tbody[@class='results']/tr[1]/td/input") })
	private List<WebElement> lstFirsttNameInput;

	@FindAll({ @FindBy(xpath = "//tbody[@class='results']/tr[3]/td") })
	private List<WebElement> lstLastName;

	@FindAll({ @FindBy(xpath = "//tbody[@class='results']/tr[3]/td/input") })
	private List<WebElement> lstLastNameInput;

	public void enterSpecificFamilyDetails(String familyMember, String details) throws Exception {
		try {
			String memberKey = "";
			for (String temp : familyMember.split(",")) {
				switch (temp.replaceAll(" ", "").toLowerCase()) {
				case "self":
					memberKey = "SELF";
					break;
				case "spouse":
					memberKey = "SPOUSE";
					break;
				case "child1":
					memberKey = "CHILD 1";
					break;
				case "child2":
					memberKey = "CHILD 2";
					break;
				case "child3":
					memberKey = "CHILD 3";
					break;
				case "child4":
					memberKey = "CHILD 4";
					break;
				case "father":
					memberKey = "FATHER";
					break;
				case "mother":
					memberKey = "MOTHER";
					break;
				case "fatherinlaw":
					memberKey = "FATHER_IN_LAW";
					break;
				case "motherinlaw":
					memberKey = "MOTHER_IN_LAW";
					break;
				default:
					throw new Exception("There is no such family member : [" + temp + "]");
				}
				HashMap<String, String> hmap = library.getFieldMap(details);
				for (String key : hmap.keySet()) {
					library.waitForDigitLoad();
					switch (key.replaceAll(" ", "").toLowerCase()) {
					case "gender":
						library.getDriver().findElement(By.xpath("//td[@id='td_" + memberKey + "_gender']//mat-select"))
						.click();
						Thread.sleep(500);
						library.getDriver()
						.findElement(
								By.xpath("//span[text()='" + hmap.get(key) + "'][@class='mat-option-text']"))
						.click();
						break;
					case "height":
						library.getDriver().findElement(By.xpath("//td[@id='td_" + memberKey + "_height']//input"))
						.clear();
						library.getDriver().findElement(By.xpath("//td[@id='td_" + memberKey + "_height']//input"))
						.sendKeys(hmap.get(key));
						break;
					case "weight":
						library.getDriver().findElement(By.xpath("//td[@id='td_" + memberKey + "_weight']//input"))
						.clear();
						library.getDriver().findElement(By.xpath("//td[@id='td_" + memberKey + "_weight']//input"))
						.sendKeys(hmap.get(key));
						break;
					}
				}
			}
		} catch (Exception e) {
			throw new Exception("Error while entering the family details : " + e.getMessage());
		}
	}

	public void enterSpecificFamilyDetailsFromDB(String familyMember, String details,String planId)throws Exception {		 /* Created By Sidharth Mishra */

		try {
			String memberKey = "";
			for (String temp : familyMember.split(",")) {
				switch (temp.replaceAll(" ", "").toLowerCase()) {
				case "self":
					memberKey = "SELF";
					break;
				case "spouse":
					memberKey = "SPOUSE";
					break;
				case "child1":
					memberKey = "CHILD 1";
					break;
				case "child2":
					memberKey = "CHILD 2";
					break;
				case "child3":
					memberKey = "CHILD 3";
					break;
				case "child4":
					memberKey = "CHILD 4";
					break;
				case "father":
					memberKey = "FATHER";
					break;
				case "mother":
					memberKey = "MOTHER";
					break;
				case "fatherinlaw":
					memberKey = "FATHER_IN_LAW";
					break;
				case "motherinlaw":
					memberKey = "MOTHER_IN_LAW";
					break;
				default:
					throw new Exception("There is no such family member : [" + temp + "]");
				}
				HashMap<String, String> hmap = library.getFieldMap(details);
				DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
				for (String key : hmap.keySet()) {
					library.waitForDigitLoad();
					try {
						switch (key.replaceAll(" ", "").toLowerCase()) {
						case "gender":
							library.getDriver().findElement(By.xpath("//td[@id='td_" + memberKey + "_gender']//mat-select"))
							.click();
							Thread.sleep(500);
							library.getDriver()
							.findElement(
									By.xpath("//span[text()='" + hmap.get(key) + "'][@class='mat-option-text']"))
							.click();
							break;
						case "height":
							library.getDriver().findElement(By.xpath("//td[@id='td_" + memberKey + "_height']//input"))
							.clear();
							library.getDriver().findElement(By.xpath("//td[@id='td_" + memberKey + "_height']//input"))
							.sendKeys(db.getPersonDataFromDB(planId,"height"));
							break;
						case "weight":
							library.getDriver().findElement(By.xpath("//td[@id='td_" + memberKey + "_weight']//input"))
							.clear();
							library.getDriver().findElement(By.xpath("//td[@id='td_" + memberKey + "_weight']//input"))
							.sendKeys(db.getPersonDataFromDB(planId,"weight"));
							break;
						}
					}catch(Exception e) {

					}
				}
			}
		} catch (Exception e) {
			throw new Exception("Error while entering the family details : " + e.getMessage());
		}
	}

	public void enterFamilyDetails(String familyMembers) throws Exception {
		try {
			for (String temp : familyMembers.split(",")) {
				switch (temp.replaceAll(" ", "").toLowerCase()) {
				case "self":
					enterPersonDetails("SELF");
					break;
				case "spouse":
					enterPersonDetails("SPOUSE");
					break;
				case "child1":
					enterPersonDetails("CHILD 1");
					break;
				case "child2":
					enterPersonDetails("CHILD 2");
					break;
				case "child3":
					enterPersonDetails("CHILD 3");
					break;
				case "child4":
					enterPersonDetails("CHILD 4");
					break;
				case "father":
					enterPersonDetails("FATHER");
					break;
				case "mother":
					enterPersonDetails("MOTHER");
					break;
				case "fatherinlaw":
					enterPersonDetails("FATHER_IN_LAW");
					break;
				case "motherinlaw":
					enterPersonDetails("MOTHER_IN_LAW");
					break;
				}
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	private void enterPersonDetails(String member) throws Exception {
		try {
			library.waitForDigitLoad();
			boolean proposer = true;
			if (library.getDriver().findElements(By.xpath("//th[text()='Member Details']/../th/span")).size() > 0)
				proposer = false;
			String xpathMember = member;
			library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_firstName']//input"))
			.sendKeys(library.getRandomValue("[A | 5]"));
			library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_lastName']//input"))
			.sendKeys(library.getRandomValue("[A | 5]"));
			if(member.equals("SELF") && proposer)
			{

				library.getDriver()
				.findElement(By.xpath("//td[@id='td_" + xpathMember + "_maritalStatus']//mat-select")).click();
				Thread.sleep(1000);
				library.getDriver().findElement(By.xpath("//span[text()='Married'][@class='mat-option-text']")).click();

			}System.out.println("Member: "+member);
			if (member.contains("FATHER") || member.contains("MOTHER")) {
				library.getDriver()
				.findElement(By.xpath("//td[@id='td_" + xpathMember + "_maritalStatus']//mat-select")).click();
				Thread.sleep(1000);
				library.getDriver().findElement(By.xpath("//span[text()='Married'][@class='mat-option-text']")).click();
			}
			library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_profession']//mat-select"))
			.click();
			Thread.sleep(1000);
			library.getDriver().findElement(By.xpath("//span[text()='Student'][@class='mat-option-text']")).click();
			if(!member.equals("SELF") || proposer)
			{
				library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_height']//input"))
				.sendKeys("180");
				library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_weight']//input"))
				.sendKeys("80");
			}
			if (member.equals("SELF")) {
				library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_panNumber']//input"))
				.sendKeys("");
				library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_idProofType']//mat-select"))
				.click();
				Thread.sleep(1000);
				library.getDriver().findElement(By.xpath("//span[text()='Passport']")).click();
				library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_panNumber']//input"))
				.sendKeys(library.getRandomValue("[ A | 10]"));
			}
			library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_street']//input"))
			.sendKeys(library.getRandomValue("[A | 10]"));
			if (member.equals("SELF")) {
				library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_email']//input"))
				.sendKeys("test@godigit.com");
				library.getDriver().findElement(By.xpath("//td[@id='td_" + xpathMember + "_mobile']//input"))
				.sendKeys("9" + library.getRandomValue("[N | 9]"));
			}
			if ((member.equals("SELF") && proposer) || (member.equals("SPOUSE") && !proposer)|| member.contains("FATHER") || member.contains("MOTHER")) {
				library.getDriver()
				.findElement(By.xpath("//td[text()='Nominee Details']/../following-sibling::tr/td[@id='td_"
						+ xpathMember + "_firstName']/input"))
				.sendKeys(library.getRandomValue("[A | 10]"));
				library.getDriver()
				.findElement(By.xpath("//td[text()='Nominee Details']/../following-sibling::tr/td[@id='td_"
						+ xpathMember + "_lastName']/input"))
				.sendKeys(library.getRandomValue("[A | 10]"));
				Thread.sleep(2000);
				library.getDriver()
				.findElement(By.xpath("//td[text()='Nominee Details']/../following-sibling::tr/td[@id='td_"
						+ xpathMember + "_dateOfbirth']//input"))
				.sendKeys("03");
				Thread.sleep(1000);
				library.getDriver()
				.findElement(By.xpath("//td[text()='Nominee Details']/../following-sibling::tr/td[@id='td_"
						+ xpathMember + "_dateOfbirth']//input"))
				.sendKeys("05");
				Thread.sleep(1000);
				library.getDriver()
				.findElement(By.xpath("//td[text()='Nominee Details']/../following-sibling::tr/td[@id='td_"
						+ xpathMember + "_dateOfbirth']//input"))
				.sendKeys("1999");
				Thread.sleep(1000);
				library.getDriver()
				.findElement(By.xpath("//td[contains(text(),'Relationship with Insured')]/following-sibling::td[@id='td_" + xpathMember + "_relationshipStatus']//mat-select"))
				.click();
				Thread.sleep(1000);
				library.getDriver().findElement(By.xpath("//span[text()='Sister'][@class='mat-option-text']")).click();
			}

		} catch (Exception e) {
			throw new Exception("Error while entering the family details : " + e.getMessage());
		}
	}

	public void performAction(String action) throws Exception {
		try {
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "next":
				saveMemberDetails();
				library.waitForDigitLoad();
				btnNext.click();
				library.waitForDigitLoad();
				break;
			default:
				throw new Exception("There is no such action [" + action + "] in Family details page");
			}
		} catch (Exception e) {
			throw new Exception("Error while clicking the button in Family details : " + e.getMessage());
		}
	}

	private void saveMemberDetails() throws Exception {
		try {
			for (int i = 1; i < lstFirsttName.size(); i++)
				Library.staticMap.put(lstFirsttName.get(i).getAttribute("id").replace("td_", ""),
						lstFirsttNameInput.get(i - 1).getAttribute("ng-reflect-model"));
			for (int i = 1; i < lstLastName.size(); i++)
				Library.staticMap.put(lstLastName.get(i).getAttribute("id").replace("td_", ""),
						lstLastNameInput.get(i - 1).getAttribute("ng-reflect-model"));
			System.out.println(Library.staticMap);
		} catch (Exception e) {
			throw new Exception("Error while saving the Family details : " + e.getMessage());
		}
	}
}
