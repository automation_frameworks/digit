package pom.agent.health;

import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.utilities.DBUtility;
import com.utilities.Library;


public class PortYourPolicy {

	com.utilities.Library library = new com.utilities.Library();


	@FindBy(xpath = "//label[contains(text(),'Since how')]/../mat-select")
	private WebElement drpYears;

	@FindBys(@FindBy(xpath = "//span[@class='mat-option-text']"))
	private List<WebElement> listOfElements;

	@FindBy(xpath = "//label[contains(text(),'Inception Date')]/../input")
	private WebElement txtInceptionDate;

	@FindBy(xpath = "//label[contains(text(),'Expiry Date')]/../input")
	private WebElement txtExpiryDate;

	@FindBy(xpath = "//span[contains(text(),'Sum Insured')]/../input")
	private WebElement txtSI;

	@FindBy(xpath = "//span[contains(text(),'Accured CB')]/../input")
	private WebElement txtAccuredCB;

	@FindBy(xpath = "//input[@type = 'checkbox']/..")
	private WebElement chkTnC;

	@FindBys(@FindBy(xpath = "//span[@class='mat-option-text']"))
	private List<WebElement> listOfCompanies;
	//span[@class = 'mat-option-text']
	@FindBy(xpath = "//label[contains(text(),'Upload')]/../input")
	private WebElement lnkUpload;

	@FindBy(xpath = "//button[text() = 'Next']")
	private WebElement btnNext;

	@FindBy(xpath = "//button[text() = 'Submit']")
	private WebElement btnSubmit;

	@FindBy(xpath = "//th[contains(text(),'Person Name')]/../../../tbody//td[2]//mat-select")
	private WebElement drpPersonName;

	@FindBy(xpath = "//label[contains(text(),'Reason')]/..//input")
	private WebElement txtReasonForPort;

	@FindBy(xpath = "//label[contains(text(),'PED exclusions')]/..//div[contains(text(),'Yes')]")
	private WebElement radYesInPopup;

	@FindBy(xpath = "//label[contains(text(),'PED exclusions')]/..//div[contains(text(),'No')]")
	private WebElement radNoInPopup;

	@FindBy(xpath = "//label//mat-select")
	private WebElement drpWaitPeriod;

	static String startDate;

	public void selectElementFromDropDown(String item) {

		for (WebElement element : listOfElements) {

			if (element.getText().replace(" ", "").toLowerCase().contains(item.replace(" ", "").toLowerCase())) {
				element.click();
				break;
			}
		}

	}

	public void answerTheQuestionsFromDB(String portId) throws Exception {
		//The following commented code is to fetch start/end date from "digit_health.health_testdata_portability" table of database.

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class); 
		String data = db.getPortabilityDataFromDB("expirydate", portId); 
		String value = getPolicyExpiryDate(data); 
		int i = 0; 
		while(i<3) {
			txtExpiryDate.sendKeys(dateInPortability(value)[i++]); Thread.sleep(2000);
		}

		value = db.getPortabilityDataFromDB("policyterm", portId).split(" ")[0]; 
		startDate = getPolicyInceptionDate(data,value);

		i = 0; 
		while(i<3){ 
			txtInceptionDate.sendKeys(dateInPortability(startDate)[i++]); Thread.sleep(2000);
		}

		try {
			txtSI.sendKeys(db.getPortabilityDataFromDB("basesi", portId));
			txtAccuredCB.sendKeys(db.getPortabilityDataFromDB("accuredcb", portId)); }
		catch (Exception e) {

		}
	}

	public String getPolicyExpiryDate(String value) {

		Calendar cal = Calendar.getInstance(); 
		SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy"); 
		int temp;
		switch(value.toLowerCase()) {
		case "today":
			value = s.format(cal.getTime());
			break;
		case "tomorrow":
			cal.add(Calendar.DATE, 1);
			value = s.format(cal.getTime());
			break;
		default:
			if (value.toLowerCase().contains("today+")) {
				temp = Integer.parseInt(value.charAt(value.length()-1)+"");
				cal.add(Calendar.DATE, temp);
				value = s.format(cal.getTime());
				break;
			}
			if (value.toLowerCase().contains("tomorrow+")) {
				temp = Integer.parseInt(value.charAt(value.length()-1)+"")+1;
				cal.add(Calendar.DATE, temp);
				value = s.format(cal.getTime());
				break;
			}
		}
		return value;
	}

	public String getPolicyInceptionDate(String data,String term) {

		Calendar cal = Calendar.getInstance(); 
		SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy"); 

		int temp;
		switch(data.toLowerCase()) {
		case "today":
			cal.add(Calendar.DATE, 1);
			data = s.format(cal.getTime()).split("-")[0]+"-"+
					s.format(cal.getTime()).split("-")[1]+"-"+
					(Integer.parseInt(s.format(cal.getTime()).split("-")[2])-Integer.parseInt(term));
			break;
		case "tomorrow":
			cal.add(Calendar.DATE, 2);
			data = s.format(cal.getTime()).split("-")[0]+"-"+
					s.format(cal.getTime()).split("-")[1]+"-"+
					(Integer.parseInt(s.format(cal.getTime()).split("-")[2])-Integer.parseInt(term));
			break;
		default:
			if (data.toLowerCase().contains("today+")) {
				temp = Integer.parseInt(data.charAt(data.length()-1)+"")+1;
				cal.add(Calendar.DATE, temp);
				data = s.format(cal.getTime()).split("-")[0]+"-"+
						s.format(cal.getTime()).split("-")[1]+"-"+
						(Integer.parseInt(s.format(cal.getTime()).split("-")[2])-Integer.parseInt(term));
				break;
			}
			if (data.toLowerCase().contains("tomorrow+")) {
				temp = Integer.parseInt(data.charAt(data.length()-1)+"")+2;
				cal.add(Calendar.DATE, temp);
				data = s.format(cal.getTime()).split("-")[0]+"-"+
						s.format(cal.getTime()).split("-")[1]+"-"+
						(Integer.parseInt(s.format(cal.getTime()).split("-")[2])-Integer.parseInt(term));
				break;
			}
		}
		return data;
	}

	public void fillPolicyDetailsFromDB(String portId) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		int numOfYears;String value;

		value = db.getPortabilityDataFromDB("policyterm", portId);
		drpYears.click();Thread.sleep(2000);
		selectElementFromDropDown(value);Thread.sleep(1000);

		value = value.split(" ")[0];
		numOfYears = Integer.parseInt(value);

		for (int i = 2; i <= numOfYears+1; i++) {

			//			Policy duration
			//			String text = library.getDriver().findElement(By.xpath("(//tr)["+i+"]/td[1]/b")).getText();

			library.getDriver().findElement(By.xpath("(//tr)["+i+"]/td[2]//div[@class='mat-select-value']")).click();
			Thread.sleep(2000);

			selectElementFromDropDown(db.getPortabilityDataFromDB("company", portId));

			Thread.sleep(2000);

			//Base SI
			library.getDriver().findElement(By.xpath("(//tr)["+i+"]/td[3]/input")).sendKeys(db.getPortabilityDataFromDB("basesi", portId));
			//CB%
			library.getDriver().findElement(By.xpath("(//tr)["+i+"]/td[4]/input")).sendKeys(db.getPortabilityDataFromDB("cb_percent", portId));
			//CB Amount
			library.getDriver().findElement(By.xpath("(//tr)["+i+"]/td[5]/input")).sendKeys(db.getPortabilityDataFromDB("accuredcb", portId));
			//			library.getDriver().findElement(By.xpath("(//tr)["+i+"]/td[6]/input")).sendKeys("ToBeFilled");
			//Product Name
			library.getDriver().findElement(By.xpath("(//tr)["+i+"]/td[7]/input")).sendKeys(library.getRandomValue("[ A | 10]"));
			//Policy Number
			library.getDriver().findElement(By.xpath("(//tr)["+i+"]/td[8]/input")).sendKeys(library.getRandomValue("[ A | 6]"));
			
			WebElement date = library.getDriver().findElement(By.xpath("(//tr)["+i+"]/td[9]/input"));
			date.clear();
			int j = 0;
			while(j<3){ 
				date.sendKeys(dateInPortability(startDate)[j++]); Thread.sleep(2000);
			}
			library.getDriver().findElement(By.xpath("(//tr)["+i+"]/td[10]/input")).click();
			//Upload File
			getUpload();
			//Any Claims
			library.getDriver().findElement(By.xpath("(//tr)["+i+"]/td[12]/mat-select")).click();
			String anyClaims = db.getPortabilityDataFromDB("anyclaims", portId);
			selectElementFromDropDown(anyClaims);

			if(anyClaims.equalsIgnoreCase("yes")) {
				existingPolicyClaims(portId);
			}
		}
		library.focus(library.getDriver(), drpYears);
		chkTnC.click();
	}

	public void getUpload() throws Exception {
		Thread.sleep(2000);
		lnkUpload.sendKeys(Paths.get("").toAbsolutePath().toString() + "/src/main/resources/documents/invoice.pdf");
		library.waitForDigitLoad();
		Thread.sleep(5000);
	}

	public void existingPolicyClaims(String portId) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String date = db.getPortabilityDataFromDB("cl_date", portId);
		String amount = db.getPortabilityDataFromDB("cl_amount", portId);

		library.getDriver().findElement(By.xpath("(//tr)[last()]/td[2]//div[@class= 'mat-select-arrow']")).click();
		library.getDriver().findElement(By.xpath("//span[text()='"+Library.staticMap.get("SELF_firstName")+"']")).click();Thread.sleep(1000);

		library.getDriver().findElement(By.xpath("(//tr)[last()]/td[3]/input")).sendKeys(library.getRandomValue("[ A | 6]"));Thread.sleep(1000);

		library.getDriver().findElement(By.xpath("(//tr)[last()]/td[4]/input")).sendKeys(dateInPortability(date)[0]);Thread.sleep(2000);
		library.getDriver().findElement(By.xpath("(//tr)[last()]/td[4]/input")).sendKeys(dateInPortability(date)[1]);Thread.sleep(2000);
		library.getDriver().findElement(By.xpath("(//tr)[last()]/td[4]/input")).sendKeys(dateInPortability(date)[2]);Thread.sleep(2000);

		library.getDriver().findElement(By.xpath("(//tr)[last()]/td[5]/input")).sendKeys(amount);Thread.sleep(2000);
		library.getDriver().findElement(By.xpath("(//tr)[last()]/td[6]/input")).sendKeys(library.getRandomValue("[ A | 6]"));Thread.sleep(2000);
		library.getDriver().findElement(By.xpath("(//tr)[last()]/td[7]//div[@class= 'mat-select-arrow']")).click();Thread.sleep(2000);
		selectElementFromDropDown(db.getPortabilityDataFromDB("cl_status", portId));

	}

	public String[] dateInPortability(String dateFromDB) {

		String[] value = dateFromDB.split("-");
		return value;
	}


	public void portPolicyPopup(String select,String portId) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		library.waitForDigitLoad();
		txtReasonForPort.sendKeys(library.getRandomValue("[ A | 15]"));

		switch(select.toLowerCase()) {

		case "yes":
			radYesInPopup.click();
			drpWaitPeriod.click();
			selectElementFromDropDown(db.getPortabilityDataFromDB("waitingperiod", portId));
			btnSubmit.click();
			break;
		case "no":
			radNoInPopup.click();
			btnSubmit.click();
			break;
		}
	}
}
