package pom.agent.health;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.utilities.DBUtility;
import com.utilities.Library;

public class OptionsRecomendedByDoctor {

	@FindBy(xpath = "//span[contains(text(),'Arogya Sanjeevani Plan')]/../label[text()='Select']")
	private WebElement selectArogyaSanjeevaniPlan;
	
	@FindBy(xpath = "//span[contains(text(),'Arogya Sanjeevani Plan')]/../label[contains(text(),'₹')]")
	private WebElement labelArogyaSanjeevaniPrice;
	
	@FindBy(xpath = "//span[contains(text(),'Plan')]/../label[text()='Select']")
	private WebElement selectPortabilityPlan;
	
	@FindBy(xpath = "//span[contains(text(),'Plan')]/../label[contains(text(),'₹')]")
	private WebElement labelPortabilityPlanPrice;

	@FindBy(xpath = "//span[contains(text(),'Diamond')]/../label[text()='Select']")
	private WebElement selectDiamond;
	
	@FindBy(xpath = "//span[contains(text(),'Diamond')]/../label[contains(text(),'₹')]")
	private WebElement labelDiamondPrice;

	@FindBy(xpath = "//span[contains(text(),'Gold')]/../label[text()='Select']")
	private WebElement selectGold;
	
	@FindBy(xpath = "//span[contains(text(),'Gold')]/../label[contains(text(),'₹')]")
	private WebElement labelGoldPrice;

	@FindBy(xpath = "//span[contains(text(),'Silver')]/../label[text()='Select']")
	private WebElement selectSilver;
	
	@FindBy(xpath = "//span[contains(text(),'Silver')]/../label[contains(text(),'₹')]")
	private WebElement labelSilverPrice;
	
	@FindBy(xpath = "//span[text()='Bosch ']/../label[2]")
	private WebElement selectBosch;
	
	@FindBy(xpath = "//span[text()='Digit Standalone Benefit Plans ']/../label[2]")
	private WebElement selectDigitStandaloneBenefitPlans ;
	
	@FindBy(xpath = "//span[text()='IIFL Plan 1 ']/../label[2]")
	private WebElement selectIIFLPlan1;
	
	@FindBy(xpath = "//span[text()='IIFL Plan 2 ']/../label[2]")
	private WebElement selectIIFLPlan2;
	
	@FindBy(xpath = "//span[text()='IIFL Plan 3 ']/../label[2]")
	private WebElement selectIIFLPlan3;
	
	@FindBy(xpath = "//span[text()='SILVER ']/../label[2]")
	private WebElement selectSILVER;
	
	@FindBy(xpath = "//button[contains(text(),'Show')]")
	private WebElement btnShowAllPlans;
	
	@FindBy(xpath = "//span[text()='Message:']/..")
	private WebElement errorMessage;
	
	@FindBy(xpath = "//span[text()='Transaction Id:']/..")
	private WebElement errorTransactionId;
	
	@FindBy(xpath = "//span[text()='URL:']/..")
	private WebElement errorURL;
	
	@FindBy(xpath = "//span[text()='Status:']/..")
	private WebElement errorStatus;
	
	public void selectPlan(String plan,String planId) throws Exception {
		Thread.sleep(3000);
		try {
			com.utilities.Library library = new com.utilities.Library();
			library.waitForDigitLoad();Thread.sleep(3000);
			boolean priceFlag = true;
			switch (plan.replaceAll(" ", "").toLowerCase()) {
			case "arogyasanjeevaniplan":
				try {
					Library.waitForVisibilityOfTheElement(btnShowAllPlans);
					if (!labelArogyaSanjeevaniPrice.getText().replaceAll(" ", "").replaceAll(".00", "").replaceAll("₹", "").replaceAll(",", "").equals("0")) {
						System.out.println("******************Plan-Price validation successfull!!******************");
						selectArogyaSanjeevaniPlan.click();
					} else {
						priceFlag = false;
						throw new Exception("****************Selected plan-price is ZERO !!***************");
					}
				} catch (Exception e) {
					if (priceFlag) {
						btnShowAllPlans.click();
						Library.waitForVisibilityOfTheElement(selectArogyaSanjeevaniPlan);
						if (!labelArogyaSanjeevaniPrice.getText().replaceAll(" ", "").replaceAll(".00", "")
								.replaceAll("₹", "").replaceAll(",", "").equals("0")) {
							System.out.println("******************Plan-Price validation successfull!!******************");
							selectArogyaSanjeevaniPlan.click();
						} else {
							throw new Exception("****************Selected plan-price is ZERO !!***************");
						} 
					}
				}
				getErrorNotificationMessage(planId);
				break;
			case "digitdiamond":
				try {
					Library.waitForVisibilityOfTheElement(btnShowAllPlans);
					if (!labelDiamondPrice.getText().replaceAll(" ", "").replaceAll(".00", "").replaceAll("₹", "").replaceAll(",", "").equals("0")) {
						System.out.println("******************Plan-Price validation successfull!!******************");
						selectDiamond.click();
					} else {
						priceFlag = false;
						throw new Exception("****************Selected plan-price is ZERO !!***************");
					}
				} catch (Exception e) {
					if (priceFlag) {
						btnShowAllPlans.click();
						Library.waitForVisibilityOfTheElement(selectDiamond);
						if (!labelDiamondPrice.getText().replaceAll(" ", "").replaceAll(".00", "").replaceAll("₹", "")
								.replaceAll(",", "").equals("0")) {
							System.out.println("******************Plan-Price validation successfull!!******************");
							selectDiamond.click();
						} else {
							throw new Exception("****************Selected plan-price is ZERO !!***************");
						} 
					}
				}
				getErrorNotificationMessage(planId);
				break;
			case "digitgold":
				try {
					Library.waitForVisibilityOfTheElement(btnShowAllPlans);
					if (!labelGoldPrice.getText().replaceAll(" ", "").replaceAll(".00", "").replaceAll("₹", "").replaceAll(",", "").equals("0")) {
						System.out.println("******************Plan-Price validation successfull!!******************");
						selectGold.click();
					} else {
						priceFlag = false;
						throw new Exception("****************Selected plan-price is ZERO !!***************");
					}
				} catch (Exception e) {
					if (priceFlag) {
						btnShowAllPlans.click();
						Library.waitForVisibilityOfTheElement(selectGold);
						if (!labelGoldPrice.getText().replaceAll(" ", "").replaceAll(".00", "").replaceAll("₹", "")
								.replaceAll(",", "").equals("0")) {
							System.out.println("******************Plan-Price validation successfull!!******************");
							selectGold.click();
						} else {
							throw new Exception("****************Selected plan-price is ZERO !!***************");
						} 
					}
				}
				getErrorNotificationMessage(planId);
				break;
			case "digitsilver":
				try {
					Library.waitForVisibilityOfTheElement(btnShowAllPlans);
					if (!labelSilverPrice.getText().replaceAll(" ", "").replaceAll(".00", "").replaceAll("₹", "").replaceAll(",", "").equals("0")) {
						System.out.println("******************Plan-Price validation successfull!!******************");
						selectSilver.click();
					} else {
						priceFlag = false;
						throw new Exception("****************Selected plan-price is ZERO !!***************");
					}
				} catch (Exception e) {
					if (priceFlag) {
						btnShowAllPlans.click();
						Library.waitForVisibilityOfTheElement(selectSilver);
						if (!labelSilverPrice.getText().replaceAll(" ", "").replaceAll(".00", "").replaceAll("₹", "")
								.replaceAll(",", "").equals("0")) {
							System.out.println("******************Plan-Price validation successfull!!******************");
							selectSilver.click();
						} else {
							throw new Exception("****************Selected plan-price is ZERO !!***************");
						} 
					}
				}
				getErrorNotificationMessage(planId);
				break;
			case "portabilityplan":
				try {
					Library.waitForVisibilityOfTheElement(btnShowAllPlans);
					if (!labelPortabilityPlanPrice.getText().replaceAll(" ", "").replaceAll(".00", "").replaceAll("₹", "").replaceAll(",", "").equals("0")) {
						System.out.println("******************Plan-Price validation successfull!!******************");
						selectPortabilityPlan.click();
					} else {
						priceFlag = false;
						throw new Exception("****************Selected plan-price is ZERO !!***************");
					}
				} catch (Exception e) {
					if (priceFlag) {
						btnShowAllPlans.click();
						Library.waitForVisibilityOfTheElement(selectSilver);
						if (!labelPortabilityPlanPrice.getText().replaceAll(" ", "").replaceAll(".00", "")
								.replaceAll("₹", "").replaceAll(",", "").equals("0")) {
							System.out.println("******************Plan-Price validation successfull!!******************");
							selectPortabilityPlan.click();
						} else {
							throw new Exception("****************Selected plan-price is ZERO !!***************");
						} 
					}
				}
				getErrorNotificationMessage(planId);
				break;
			case "selectBosch":
				try {
					Library.waitForVisibilityOfTheElement(btnShowAllPlans);
					btnShowAllPlans.click();
					selectBosch.click();
				} catch (Exception e) {
					btnShowAllPlans.click();
					selectBosch.click();
					Library.waitForVisibilityOfTheElement(selectBosch);
				}
				getErrorNotificationMessage(planId);
				break;
			case "selectDigitStandaloneBenefitPlans":
				try {
					Library.waitForVisibilityOfTheElement(btnShowAllPlans);
					btnShowAllPlans.click();
					selectDigitStandaloneBenefitPlans.click();
				} catch (Exception e) {
					btnShowAllPlans.click();
					Library.waitForVisibilityOfTheElement(selectDigitStandaloneBenefitPlans);
					selectDigitStandaloneBenefitPlans.click();
				}
				getErrorNotificationMessage(planId);
				break;
			case "selectIIFLPlan1":
				try {
					Library.waitForVisibilityOfTheElement(btnShowAllPlans);
					btnShowAllPlans.click();
					selectIIFLPlan1.click();
				} catch (Exception e) {
					btnShowAllPlans.click();
					Library.waitForVisibilityOfTheElement(selectIIFLPlan1);
					selectIIFLPlan1.click();
				}
				getErrorNotificationMessage(planId);
				break;
			case "selectIIFLPlan2":
				try {
					Library.waitForVisibilityOfTheElement(btnShowAllPlans);
					btnShowAllPlans.click();
					selectIIFLPlan2.click();
				} catch (Exception e) {
					btnShowAllPlans.click();
					Library.waitForVisibilityOfTheElement(selectIIFLPlan2);
					selectIIFLPlan2.click();
				}
				getErrorNotificationMessage(planId);
				break;
			case "selectIIFLPlan3":
				try {
					Library.waitForVisibilityOfTheElement(btnShowAllPlans);
					btnShowAllPlans.click();
					selectIIFLPlan3.click();
				} catch (Exception e) {
					btnShowAllPlans.click();
					Library.waitForVisibilityOfTheElement(selectIIFLPlan3);
					selectIIFLPlan3.click();
				}
				getErrorNotificationMessage(planId);
				break;
			case "selectSILVER":
				try {
					Library.waitForVisibilityOfTheElement(btnShowAllPlans);
					btnShowAllPlans.click();
					selectSILVER.click();
				} catch (Exception e) {
					btnShowAllPlans.click();
					Library.waitForVisibilityOfTheElement(selectSILVER);
					selectSILVER.click();
				}
				getErrorNotificationMessage(planId);
				break;
			
			default:
				throw new Exception("The option [" + plan + "] expeceted to be clicked is not present");
			}
			library.waitForDigitLoad();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	public void getShowAllOptions(String button) {
		
		button = button.replaceAll(" ", "").toLowerCase();
		if (button == "showallplans" || button == "showrecommendedplans") {
			
			btnShowAllPlans.click();
		}
	}
	
	private void getErrorNotificationMessage(String id) throws Exception {
		
		try {
			com.utilities.Library library = new com.utilities.Library();
			DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
			library.waitForDigitLoad();
			Thread.sleep(3000);
			db.storeScriptFailCommentInDB("Message: "+errorMessage.getText()+"\n\nTransaction Id: "+errorTransactionId.getText()+"\n\nURL: "+errorURL.getText()+"\n\nStatus: "+errorStatus.getText(), id, "digit_retail_pa.retail_pa_testdata");
			System.out.println("Message: "+errorMessage.getText()+"\n\nTransaction Id: "+errorTransactionId.getText()+"\n\nURL: "+errorURL.getText()+"\n\nStatus: "+errorStatus.getText());
		} catch (Exception e) {
			
		}
	}

}