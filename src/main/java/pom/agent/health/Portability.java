package pom.agent.health;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Portability {

	@FindBy(id = "firstYearRID")
	private WebElement txtFYPolicyInceptionDate;
	
	@FindBy(id = "currentExpiry")
	private WebElement txtCYPolicyExpiryDate;
	
	@FindBy(xpath = "//div[@class = 'mat-select-value']")
	private WebElement drpPolicyType;
	
	@FindBy(xpath = "(//span[text()='Floater'])[1]")
	private WebElement selectFloater;
	
	@FindBy(xpath = "(//span[text()='Individual'])[1]")
	private WebElement selectIndividual;
	
	@FindBy(xpath = "//span[contains(text(),'Sum Insured of expiring policy')]/../input")
	private WebElement txtSIOfExpiringPolicy;
	
	@FindBy(xpath = "//span[contains(text(),'Accrued CB')]/../input")
	private WebElement txtAccuredCB;
	
	@FindBy(xpath = "//div[contains(text(),'merge the existing CB')]/..//mat-radio-button//div[text()='Yes']")
	private WebElement radioMergeCBYes;
	
	@FindBy(xpath = "//div[contains(text(),'merge the existing CB')]/..//mat-radio-button//div[text()='No']")
	private WebElement radioMergeCBNo;
	
	@FindBy(xpath = "//div[contains(text(),'medical history')]/..//mat-radio-button//div[text()='Yes']")
	private WebElement radioAnyHistoryYes;
	
	@FindBy(xpath = "//div[contains(text(),'medical history')]/..//mat-radio-button//div[text()='No']")
	private WebElement radioAnyHistoryNo;
	
	@FindBy(xpath = "//div[contains(text(),'Any claims')]/..//mat-radio-button//div[text()='Yes']")
	private WebElement radioAnyClaimsYes;
	
	@FindBy(xpath = "//div[contains(text(),'Any claims')]/..//mat-radio-button//div[text()='No']")
	private WebElement radioAnyClaimsNo;
	
	
}
