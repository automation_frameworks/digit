package pom.agent.health;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class ChoosePaymentMethod {

	com.utilities.Library library = new com.utilities.Library();
			
	@FindBy(xpath = "//button[contains(text(),'Mock Payment')]")
	private WebElement btnMockPayment;
	
	public void makeMockPayment() throws Exception {
		library.waitForDigitLoad();
		library.waitForElementVisible(btnMockPayment, 10, 1000);
		library.waitForElementToBeClickable(btnMockPayment, 10, 1000);
		Thread.sleep(3000);
		Actions act = new Actions(library.getDriver());
		
		act.moveToElement(btnMockPayment).click().build().perform();
		
		try {
			library.clickOnElementJS(btnMockPayment, library.getDriver());
		} catch (Exception e) {
		}
		
	}
}
