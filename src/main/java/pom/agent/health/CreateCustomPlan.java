package pom.agent.health;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.utilities.Library;

public class CreateCustomPlan {

	@FindBy(xpath = "//button[text()='Create New Package']")
	private WebElement btnCreateNewPackage;

	@FindBy(xpath = "//span[text()='Create Custom Plan']/..//input")
	private WebElement txtCustomPlanName;

	@FindBy(xpath = "//button[text()='Save']")
	private WebElement btnSave;

	Library library = new com.utilities.Library();

	public void selectPackage(String packageName) throws Exception {
		try {
			library.waitForDigitLoad();
			switch (packageName.replaceAll(" ", "").toLowerCase()) {
			case "createnewpackage":
				btnCreateNewPackage.click();
				break;
			default:
				throw new Exception("The given option [" + packageName + "] is not available");
			}
		} catch (Exception e) {
			throw new Exception("Error while selecting the packageName : " + e.getMessage());
		}
	}

	public void createCustomPlan() throws Exception {
		try {
			library.waitForDigitLoad();
			txtCustomPlanName.sendKeys(library.getRandomValue("[AN | 10]").toUpperCase());
			Library.staticMap.put("CustomPlan", txtCustomPlanName.getAttribute("value"));
			Thread.sleep(1000);
			btnSave.click();
		} catch (Exception e) {
			throw new Exception("Error while creating the Custom Plan : " + e.getMessage());
		}
	}

	public void selectCreatedCustomPlan() throws Exception {
		try {
			Thread.sleep(10000);
			library.waitForDigitLoad();
			Thread.sleep(5000);
			WebElement element = library.getDriver().findElement(By.xpath("//label[text()='"+Library.staticMap.get("CustomPlan")+"']/../../following-sibling::div//span[text()='Select']"));
			Actions action = new Actions(library.getDriver());
			action.moveToElement(element);
			Thread.sleep(1000);
			element.click();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while selecting the custom plan : " + e.getMessage());
		}
	}
}
