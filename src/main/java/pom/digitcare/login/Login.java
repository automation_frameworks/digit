package pom.digitcare.login;

import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Login {

	@FindBy(id = "username")
	private WebElement txtUserName;

	@FindBy(id = "password")
	private WebElement txtPassword;

	@FindBy(xpath = "//button[text()='Login']")
	private WebElement btnLogin;

	private com.utilities.Library library = new com.utilities.Library();

	public void enterDetails(String details) {
		HashMap<String, String> hmap = library.getFieldMap(details);
		for (String key : hmap.keySet()) {
			switch (key.replaceAll(" ", "").toLowerCase()) {
			case "username":
				txtUserName.sendKeys(hmap.get(key));
				break;
			case "password":
				txtPassword.sendKeys(hmap.get(key));
				break;
			default:
				System.out.println("Implememntation for the field '" + key + "' is not found");
			}
		}
		btnLogin.click();
	}
}
