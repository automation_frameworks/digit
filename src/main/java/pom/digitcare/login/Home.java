package pom.digitcare.login;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Home {

	@FindBy(xpath = "//div[@id='myNavbar']/a[text()='Search Activity']")
	private WebElement lnkSearchActivity;

	@FindBy(xpath = "//div[@id='myNavbar']/a[text()='Search Policy/Claim']")
	private WebElement lnkSearchPolicyClaim;

	@FindBy(xpath = "//div[@id='myNavbar']/a[text()='Approvals']")
	private WebElement lnkApprovals;

	@FindBy(xpath = "//div[@id='myNavbar']/a[text()='Digit Onboarding']")
	private WebElement lnkDigitOnboarding;

	public void movetoTab(String tabName) throws Exception {
		switch (tabName.toLowerCase().replaceAll(" ", "")) {
		case "searchactivity":
			lnkSearchActivity.click();
			break;
		case "searchpolicy/claim":
		case "searchpolicyclaim":
		case "searchpolicy/claims":
		case "searchpolicyclaims":
			lnkSearchPolicyClaim.click();
			break;
		case "approvals":
			lnkApprovals.click();
			break;
		case "digitonboarding":
			lnkDigitOnboarding.click();
			break;
		default:
			throw new Exception("There is no tab called '" + tabName + "' in the page");
		}
	}

}
