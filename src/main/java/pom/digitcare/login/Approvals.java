package pom.digitcare.login;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Approvals {

	@FindBy(name = "imdCode")
	private WebElement txtPartner;

	@FindBy(id = "policyNo")
	private WebElement txtPolicyNumber;

	@FindBy(id = "imeiReg")
	private WebElement txtRegistrationNumber;

	@FindBy(id = "activity")
	private WebElement drpActivity;

	@FindBy(id = "product")
	private WebElement drpProduct;

	@FindBy(id = "vin")
	private WebElement txtVINNumber;

	@FindBy(id = "status")
	private WebElement drpStatus;

	@FindBy(xpath = "//button[contains(text(),'Search')]")
	private WebElement btnSearch;

	@FindBy(xpath = "//button[contains(text(),'Clear')]")
	private WebElement btnClear;

	@FindBy(xpath = "//table//tbody/tr[1]//i")
	private WebElement lnkFirstImageSearch;

	@FindBy(xpath = "//input[@id='enum_answer_No Damage']")
	private WebElement radNoDamage;

	@FindBy(xpath = "//i[@class='ngx-gallery-icon-content ngx-gallery-close fa fa-times-circle']")
	private WebElement btnCloseImage;

	@FindBy(xpath = "//button[text()='Approve']")
	private WebElement btnApprove;

	@FindBy(xpath = "//button[text()='Reject']")
	private WebElement btnReject;

	@FindBy(xpath = "//button[text()='Reinspection']")
	private WebElement btnReInspection;

	@FindBy(xpath = "//button[text()='PI-Pending']")
	private WebElement btnPIPending;

	@FindBy(xpath = "//button[text()='Save']")
	private WebElement btnSave;

	@FindBy(xpath = "//button[text()='Rule Engine Response']")
	private WebElement btnRuleEngineResponse;

	@FindBy(xpath = "//button[text()='Display Quote']")
	private WebElement btnDisplayQuote;

	@FindBy(xpath = "//div[@id='toast-container']//div[@class='toast-title']")
	private WebElement lblInformationMessage;

	String radImageNotValid = "enum_answer_Image not valid";

	String radImageNotClear = "enum_answer_Image not clear";

	String radNoDmage = "enum_answer_No Damage";

	String radExcludingExistingDamages = "enum_answer_Excluding existing damages";

	String radMoreThan4PanelsDamaged = "enum_answer_More than 4 panels damaged rejected";

	String radVehicleInGoodCondition = "enum_answer_Vehicle is in good condition";

	private com.utilities.Library library = new com.utilities.Library();

	static LinkedList<String> errorMessages = new LinkedList<>();

	public void searchWithDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "partner":
					txtPartner.sendKeys(hmap.get(key));
					break;
				case "policynumber":
					txtPolicyNumber.sendKeys(hmap.get(key));
					break;
				case "registrationnumber":
					txtRegistrationNumber.sendKeys(hmap.get(key));
					break;
				case "activity":
					library.selectDropDownValue(drpActivity, hmap.get(key));
					break;
				case "product":
					library.selectDropDownValue(drpProduct, hmap.get(key));
					break;
				case "vinnumber":
					txtVINNumber.sendKeys(hmap.get(key));
					break;
				case "status":
					library.selectDropDownValue(drpStatus, hmap.get(key));
					break;
				default:
					System.out.println("Implememntation for the field '" + key + "' is not found");
				}
			}
			btnSearch.click();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public void verifyCountOfRecords(String count) throws Exception {

		int rowsPresent = library.getDriver().findElements(By.xpath("//table//tbody/tr")).size();
		System.out.println("Rows Present : " + rowsPresent);
		if (!(rowsPresent == Integer.parseInt(count.trim())))
			throw new Exception("Rows expected : " + count + ". But rows present : " + rowsPresent);
	}

	public void verifyImagesCountInInspection(String number) throws Exception {
		int imagesCount = library.getDriver().findElements(By.xpath("//div[@class='ngx-gallery-thumbnails']//a"))
				.size();
		System.out.println("Rows Present : " + imagesCount);
		if (!(imagesCount == Integer.parseInt(number.trim())))
			throw new Exception("Images expected : " + number + ". But images present : " + imagesCount);
	}

	public void verifyFourWheelerDamageOptions(String section) {
		try {
			String backSidePartsExpected = library.getTestDataProperties("PRIVATECAR_BACKSIDE_PARTS");
			String backSideDamagesExpected = library.getTestDataProperties("PRIVATECAR_BACKSIDE_DAMAGES");

			switch (section.toLowerCase().replaceAll(" ", "")) {
			case "backside":
				backSidePartsExpected = library.getTestDataProperties("PRIVATECAR_BACKSIDE_PARTS");
				backSideDamagesExpected = library.getTestDataProperties("PRIVATECAR_BACKSIDE_DAMAGES");
				break;
			case "rcbookback":
				backSidePartsExpected = library.getTestDataProperties("PRIVATECAR_RCBOOKBACK_PARTS");
				backSideDamagesExpected = library.getTestDataProperties("PRIVATECAR_RCBOOKBACK_DAMAGES");
				break;
			case "rcbookfront":
				backSidePartsExpected = library.getTestDataProperties("PRIVATECAR_RCBOOKFRONT_PARTS");
				backSideDamagesExpected = library.getTestDataProperties("PRIVATECAR_RCBOOKFRONT_DAMAGES");
				break;
			case "chassisplate":
				backSidePartsExpected = library.getTestDataProperties("PRIVATECAR_CHASSISPLATE_PARTS");
				backSideDamagesExpected = library.getTestDataProperties("PRIVATECAR_CHASSISPLATE_DAMAGES");
				break;

			case "enginechamber":
				backSidePartsExpected = library.getTestDataProperties("PRIVATECAR_ENGINECHAMBER_PARTS");
				backSideDamagesExpected = library.getTestDataProperties("PRIVATECAR_ENGINECHAMBER_DAMAGES");
				break;
			case "windscreen":
				backSidePartsExpected = library.getTestDataProperties("PRIVATECAR_WINDSCREEN_PARTS");
				backSideDamagesExpected = library.getTestDataProperties("PRIVATECAR_WINDSCREEN_DAMAGES");
				break;
			case "frontrightcorner":
				backSidePartsExpected = library.getTestDataProperties("PRIVATECAR_FRONTRIGHTCORNER_PARTS");
				backSideDamagesExpected = library.getTestDataProperties("PRIVATECAR_FRONTRIGHTCORNER_DAMAGES");
				break;
			case "frontleftcorner":
				backSidePartsExpected = library.getTestDataProperties("PRIVATECAR_FRONTLEFTCORNER_PARTS");
				backSideDamagesExpected = library.getTestDataProperties("PRIVATECAR_FRONTLEFTCORNER_DAMAGES");
				break;
			case "dashboard":
				backSidePartsExpected = library.getTestDataProperties("PRIVATECAR_DASHBOARD_PARTS");
				backSideDamagesExpected = library.getTestDataProperties("PRIVATECAR_DASHBOARD_DAMAGES");
				break;
			case "backleftcorner":
				backSidePartsExpected = library.getTestDataProperties("PRIVATECAR_BACKLEFTCORNER_PARTS");
				backSideDamagesExpected = library.getTestDataProperties("PRIVATECAR_BACKLEFTCORNER_DAMAGES");
				break;
			case "frontside":
				backSidePartsExpected = library.getTestDataProperties("PRIVATECAR_FRONTSIDE_PARTS");
				backSideDamagesExpected = library.getTestDataProperties("PRIVATECAR_FRONTSIDE_DAMAGES");
				break;
			case "rightside":
				backSidePartsExpected = library.getTestDataProperties("PRIVATECAR_RIGHTSIDE_PARTS");
				backSideDamagesExpected = library.getTestDataProperties("PRIVATECAR_RIGHTSIDE_DAMAGES");
				break;
			case "backrightcorner":
				backSidePartsExpected = library.getTestDataProperties("PRIVATECAR_BACKRIGHTCORNER_PARTS");
				backSideDamagesExpected = library.getTestDataProperties("PRIVATECAR_BACKRIGHTCORNER_DAMAGES");
				break;
			case "leftside":
				backSidePartsExpected = library.getTestDataProperties("PRIVATECAR_LEFTSIDE_PARTS");
				backSideDamagesExpected = library.getTestDataProperties("PRIVATECAR_LEFTSIDE_DAMAGES");
				break;

			}

			verifyDamageOptions(backSidePartsExpected, backSideDamagesExpected, section);

			radNoDamage.click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void verifyCommercialDamageOptions(String section, String type) {
		try {
			String prefix = "";
			switch (type.toLowerCase().replaceAll(" ", "")) {
			case "gcv4w":
				prefix = "GCV4W";
				break;
			case "gcv3w":
				prefix = "GCV3W";
				break;
			case "pcv4w":
				prefix = "PCV4W";
				break;
			case "pcv3w":
				prefix = "PCV3W";
				break;
			case "misc":
				prefix = "MISC";
				break;
			default:
				prefix = "MISC";
			}
			String backSidePartsExpected = "";
			String backSideDamagesExpected = "";
			// System.out.println("Verifying "+section);
			switch (section.toLowerCase().replaceAll(" ", "")) {
			case "backside":
				backSidePartsExpected = library.getTestDataProperties(prefix + "_BACKSIDE_PARTS");
				backSideDamagesExpected = library.getTestDataProperties(prefix + "_BACKSIDE_DAMAGES");
				break;
			case "frontside":
				backSidePartsExpected = library.getTestDataProperties(prefix + "_FRONTSIDE_PARTS");
				backSideDamagesExpected = library.getTestDataProperties(prefix + "_FRONTSIDE_DAMAGES");
				break;
			case "rightside":
				backSidePartsExpected = library.getTestDataProperties(prefix + "_RIGHTSIDE_PARTS");
				backSideDamagesExpected = library.getTestDataProperties(prefix + "_RIGHTSIDE_DAMAGES");
				break;
			case "leftside":
				backSidePartsExpected = library.getTestDataProperties(prefix + "_LEFTSIDE_PARTS");
				backSideDamagesExpected = library.getTestDataProperties(prefix + "_LEFTSIDE_DAMAGES");
				break;
			case "rcbookback":
				backSidePartsExpected = library.getTestDataProperties(prefix + "_RCBOOKBACK_PARTS");
				backSideDamagesExpected = library.getTestDataProperties(prefix + "_RCBOOKBACK_DAMAGES");
				break;
			case "rcbookfront":
				backSidePartsExpected = library.getTestDataProperties(prefix + "_RCBOOKFRONT_PARTS");
				backSideDamagesExpected = library.getTestDataProperties(prefix + "_RCBOOKFRONT_DAMAGES");
				break;
			case "chassisplate":
				backSidePartsExpected = library.getTestDataProperties(prefix + "_CHASSISPLATE_PARTS");
				backSideDamagesExpected = library.getTestDataProperties(prefix + "_CHASSISPLATE_DAMAGES");
				break;
			case "enginechamber":
				backSidePartsExpected = library.getTestDataProperties(prefix + "_ENGINECHAMBER_PARTS");
				backSideDamagesExpected = library.getTestDataProperties(prefix + "_ENGINECHAMBER_DAMAGES");
				break;
			case "windscreen":
				backSidePartsExpected = library.getTestDataProperties(prefix + "_WINDSCREEN_PARTS");
				backSideDamagesExpected = library.getTestDataProperties(prefix + "_WINDSCREEN_DAMAGES");
				break;
			case "frontrightcorner":
				backSidePartsExpected = library.getTestDataProperties(prefix + "_FRONTRIGHTCORNER_PARTS");
				backSideDamagesExpected = library.getTestDataProperties(prefix + "_FRONTRIGHTCORNER_DAMAGES");
				break;
			case "frontleftcorner":
				backSidePartsExpected = library.getTestDataProperties(prefix + "_FRONTLEFTCORNER_PARTS");
				backSideDamagesExpected = library.getTestDataProperties(prefix + "_FRONTLEFTCORNER_DAMAGES");
				break;
			case "dashboard":
				backSidePartsExpected = library.getTestDataProperties(prefix + "_DASHBOARD_PARTS");
				backSideDamagesExpected = library.getTestDataProperties(prefix + "_DASHBOARD_DAMAGES");
				break;
			case "backleftcorner":
				backSidePartsExpected = library.getTestDataProperties(prefix + "_BACKLEFTCORNER_PARTS");
				backSideDamagesExpected = library.getTestDataProperties(prefix + "_BACKLEFTCORNER_DAMAGES");
				break;
			case "backrightcorner":
				backSidePartsExpected = library.getTestDataProperties(prefix + "_BACKRIGHTCORNER_PARTS");
				backSideDamagesExpected = library.getTestDataProperties(prefix + "_BACKRIGHTCORNER_DAMAGES");
				break;
			}
			verifyDamageOptions(backSidePartsExpected, backSideDamagesExpected, section);
			try {
				radNoDamage.click();
			} catch (Exception e) {

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void verifyDamageOptions(String backSidePartsExpected, String backSideDamagesExpected, String section) {
		try {
			int size = library.getDriver().findElements(By.id(radImageNotValid)).size();
			if (size != 1)
				errorMessages
						.add("INV Radio button is displayed '" + size + "' times for the section : " + section);
			size = library.getDriver().findElements(By.id(radImageNotClear)).size();
			if (size != 1)
				errorMessages
						.add("INC Radio button is displayed '" + size + "' times for the section : " + section);
			size = library.getDriver().findElements(By.id(radNoDmage)).size();
			if (size != 1)
				errorMessages
						.add("ND Radio button is displayed '" + size + "' times for the section : " + section);
			size = library.getDriver().findElements(By.id(radExcludingExistingDamages)).size();
			if (size != 1)
				errorMessages
						.add("EED Radio button is displayed '" + size + "' times for the section : " + section);
			size = library.getDriver().findElements(By.id(radMoreThan4PanelsDamaged)).size();
			if (size != 1)
				errorMessages
						.add("4+PDE Radio button is displayed '" + size + "' times for the section : " + section);
			size = library.getDriver().findElements(By.id(radVehicleInGoodCondition)).size();
			if (size != 1)
				errorMessages
						.add("VGC Radio button is displayed '" + size + "' times for the section : " + section);
			String[] partsArrayExpected = backSidePartsExpected.split(",");
			ArrayList<String> partsExpectedAL = new ArrayList<String>(Arrays.asList(partsArrayExpected));

			String[] damagesArrayExpected = backSideDamagesExpected.split(",");
			ArrayList<String> damagesExpectedAL = new ArrayList<String>(Arrays.asList(damagesArrayExpected));
			ArrayList<String> partsArrayActual = new ArrayList<>();
			for (WebElement temp : library.getDriver().findElements(By.xpath("//div[@class='pointer-cursor']/div"))) {
				partsArrayActual.add(temp.getText().trim());
			}
			ArrayList<String> presentExtra = (ArrayList<String>) partsArrayActual.clone();
			ArrayList<String> missed = (ArrayList<String>) partsExpectedAL.clone();
			presentExtra.removeAll(partsExpectedAL);
			missed.removeAll(partsArrayActual);
			if (!presentExtra.isEmpty()) {
				errorMessages.add("=====>" + section + "==>Parts displayed extra : " + presentExtra);
				System.out.println("=====>" + section + "==>Parts displayed extra : " + presentExtra);
			}
			if (!missed.isEmpty() || missed.size() > 0) {
				errorMessages.add("=====>" + section + "==>Parts not displayed : " + missed);
				System.out.println("=====>" + section + "==>Parts not displayed : " + missed);
			}

			int partsCount = library.getDriver().findElements(By.xpath("//div[@class='checkox-align']/div[2]")).size();
			for (int i = 1; i <= partsCount; i++) {
				library.getDriver().findElement(By.xpath("//div[@class='checkox-align'][" + i + "]/div")).click();
				String partName = library.getDriver()
						.findElement(By.xpath("//div[@class='checkox-align'][" + i + "]/div[1]/div")).getText().trim();
				ArrayList<String> damagesArrayActual = new ArrayList<>();
				for (WebElement temp : library.getDriver()
						.findElements(By.xpath("//div[@class='checkox-align'][" + i + "]/div[2]/label"))) {
					damagesArrayActual.add(temp.getText().trim());
				}
				// if (backSideDamagesExpectedAL.equals(backSideDamagesArrayActual)) {
				// } else
				// System.out.println("Arrays are not matched for part : " + partName + ".
				// Expected : "
				// + backSideDamagesExpectedAL + " | Actual : " + backSideDamagesArrayActual);

				// System.out.println("Damages present in the UI : "+damagesArrayActual);
				// System.out.println("Damages expected in the UI : "+damagesExpectedAL);

				// presentExtra = damagesArrayActual;
				// missed = damagesExpectedAL;
				presentExtra = (ArrayList<String>) damagesArrayActual.clone();
				missed = (ArrayList<String>) damagesExpectedAL.clone();

				presentExtra.removeAll(damagesExpectedAL);
				// System.out.println(" weDamages present in the UI : "+damagesArrayActual);
				// System.out.println("weDamages expected in the UI : "+damagesExpectedAL);
				missed.removeAll(damagesArrayActual);
				if (!presentExtra.isEmpty()) {
					errorMessages.add(
							"=====>" + section + "===>" + partName + "==>Damages displayed extra : " + presentExtra);
					System.out.println(
							"=====>" + section + "===>" + partName + "==>Damages displayed extra : " + presentExtra);
				}
				if (!missed.isEmpty()) {
					errorMessages.add("=====>" + section + "===>" + partName + "==>Damages not displayed : " + missed);
					System.out.println("=====>" + section + "===>" + partName + "==>Damages not displayed : " + missed);
				}
				library.getDriver().findElement(By.xpath("//div[@class='checkox-align'][" + i + "]/div")).click();

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void openImage(String imageName) throws InterruptedException {
		int imagesCount = library.getDriver().findElements(By.xpath("//div[@class='ngx-gallery-thumbnails']//a"))
				.size();
		boolean flag = false;
		for (int i = 1; i <= imagesCount; i++) {

			library.getDriver().findElement(By.xpath("//div[@class='ngx-gallery-thumbnails']//a[" + i + "]")).click();
			Thread.sleep(1000);
			String presentImage = library.getDriver()
					.findElement(By
							.xpath("//div[contains(@class,'ngx-gallery-active')]/div[@class='ngx-gallery-image-text']"))
					.getText();
			if (presentImage.toLowerCase().replaceAll(" ", "").replaceAll("\r\n", "")
					.equals(imageName.toLowerCase().replaceAll(" ", ""))) {
				Thread.sleep(1000);
				flag = true;
				library.getDriver().findElement(By.xpath("//div[contains(@class,'ngx-gallery-image-wrapper')]"))
						.click();
				break;
			}
		}
		if (!flag)
			System.out.println("Image '" + imageName + "' is not present. Skipping...");
	}

	public void openImageSearch(String rowNumber) throws Exception {
		library.waitforDigitCareLoading();
		library.getDriver().findElement(By.xpath("//table//tbody/tr[" + rowNumber.trim() + "]//i")).click();
		Thread.sleep(3000);
		errorMessages.clear();
	}

	public void closeImage() throws InterruptedException, AWTException {
		try {
			Actions action = new Actions(library.getDriver());
			action.moveToElement(btnCloseImage).build().perform();

			btnCloseImage.click();
		} catch (Exception e) {

		}
	}

	public void closeInspectionWindow() throws InterruptedException, AWTException {
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_PAGE_UP);
		robot.keyRelease(KeyEvent.VK_PAGE_UP);
		Thread.sleep(1000);
		library.getDriver().findElement(By.xpath("//div[@id='imageModal']//span[@class='close']")).click();

	}

	public void performActionInInspectionDetails(String action) throws Exception {
		try {
			switch (action.toLowerCase().replaceAll(" ", "")) {
			case "approve":
				btnApprove.click();
				WebDriverWait wait = new WebDriverWait(library.getDriver(), 20);
				try {
					wait.until(ExpectedConditions.textToBePresentInElement(lblInformationMessage,
							"Approved successfully"));
				} catch (TimeoutException te) {
					throw new Exception(
							"'Approved successfully' message is not displayed after clicking the Approve button");
				}
				break;
			case "reject":
				btnReject.click();
				break;
			case "reinspection":
				btnReInspection.click();
				break;
			case "pi-pending":
			case "pipending":
				btnPIPending.click();
				break;
			case "ruleengineresponse":
				btnRuleEngineResponse.click();
				break;
			case "displayquote":
				btnDisplayQuote.click();
				break;
			case "save":
				btnSave.click();
				break;
			default:
				throw new Exception("The specified action '" + action
						+ "' is not recognised or not a valid action for the page 'Inspection details'");
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void assertErrorList() throws Exception {
		if (errorMessages.size() > 0)
			throw new Exception("Pre Inspection image section has the following issues : " + errorMessages);
	}
	
	public void approveTheImages() throws Exception
	{
		try {
			int imagesCount = library.getDriver().findElements(By.xpath("//div[@class='ngx-gallery-thumbnails']//a"))
					.size();
			for (int i = 1; i <= imagesCount; i++) {
				library.getDriver().findElement(By.xpath("//div[@class='ngx-gallery-thumbnails']//a[" + i + "]")).click();
				Thread.sleep(1000);
					library.getDriver().findElement(By.xpath("//div[contains(@class,'ngx-gallery-image-wrapper')]"))
							.click();
					radNoDamage.click();
					Actions action = new Actions(library.getDriver());
					action.moveToElement(btnCloseImage).build().perform();
					Thread.sleep(1000);
					try {
					btnCloseImage.click();
					}
					catch(Exception e)
					{
						Thread.sleep(3000);
						btnCloseImage.click();
					}
				}
		}
		catch (Exception e)
		{
			throw new Exception("Error while trying to aprove the images : " + e.getMessage());
		}
	}
}
