package pom.digitcare.login;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import java.io.FileReader; 
import java.util.Iterator; 
import java.util.Map; 
  
import org.json.simple.JSONArray; 
import org.json.simple.JSONObject; 
import org.json.simple.parser.*;

public class APICall {

	private final String USER_AGENT = "Mozilla/5.0";

	public static void main(String[] args) throws Exception {

		APICall http = new APICall();

		System.out.println("Testing 1 - Send Http GET request");
		http.sendGet();
		
		System.out.println("\nTesting 2 - Send Http POST request");
		http.sendPost();

	}

	// HTTP GET request
	private void sendGet() throws Exception {

		String url = "http://www.google.com/search?q=mkyong";
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());

	}
	
	// HTTP POST request
	private void sendPost() throws Exception {

		String url = "http://preprod-plus1.godigit.com/digitplusservices/createQuote?isUserSpecialDiscountOpted=false&isDownloadQuote=false";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("Accept","application/json, text/plain, */*");
		con.setRequestProperty("content-type", "application/x-www-form-urlencoded");
		con.setRequestProperty("Authorization", "Bearer 7440bb1d-7f2c-4e7e-8920-d66e3de47217-1554975614409");

		String urlParameters = "{\r\n" + 
				"  \"enquiryId\": \"DP-20101-krgvdj29y3\",\r\n" + 
				"  \"vehicle\": {\r\n" + 
				"    \"isVehicleNew\": false,\r\n" + 
				"    \"vehicleMaincode\": \"1110710302\",\r\n" + 
				"    \"licensePlateNumber\": \"KA01A1234\",\r\n" + 
				"    \"vehicleIdentificationNumber\": \"JSGJHGFHSADFG\",\r\n" + 
				"    \"engineNumber\": \"SJHJASKDFHJAKFHJASD\",\r\n" + 
				"    \"manufactureDate\": \"0001-01-01\",\r\n" + 
				"    \"registrationDate\": \"2017-03-01\",\r\n" + 
				"    \"vehicleIDV\": {\r\n" + 
				"      \"idv\": 402859,\r\n" + 
				"      \"defaultIdv\": 402859,\r\n" + 
				"      \"minimumIdv\": 322288,\r\n" + 
				"      \"maximumIdv\": 483431\r\n" + 
				"    },\r\n" + 
				"    \"bodyType\": null,\r\n" + 
				"    \"vehicleType\": \"KFZP10\",\r\n" + 
				"    \"usageType\": null,\r\n" + 
				"    \"permitType\": null,\r\n" + 
				"    \"trailers\": null,\r\n" + 
				"    \"odometerReading\": null,\r\n" + 
				"    \"annualMileage\": null,\r\n" + 
				"    \"batteryNumber\": null,\r\n" + 
				"    \"chargerNumber\": null,\r\n" + 
				"    \"make\": \"CHEVROLET\",\r\n" + 
				"    \"model\": \"BEAT\",\r\n" + 
				"    \"maximumPermissibleWeight\": null\r\n" + 
				"  },\r\n" + 
				"  \"previousInsurer\": {\r\n" + 
				"    \"previousInsurerCode\": \"\",\r\n" + 
				"    \"previousPolicyNumber\": \"\",\r\n" + 
				"    \"previousPolicyExpiryDate\": \"2019-01-11\",\r\n" + 
				"    \"isClaimInLastYear\": true,\r\n" + 
				"    \"previousPolicyType\": null,\r\n" + 
				"    \"previousNoClaimBonus\": \"ZERO\",\r\n" + 
				"    \"previousNoClaimBonusValue\": null\r\n" + 
				"  },\r\n" + 
				"  \"contract\": {\r\n" + 
				"    \"insuranceProductCode\": \"20101\",\r\n" + 
				"    \"subInsuranceProductCode\": \"PB\",\r\n" + 
				"    \"productUniqueIndentifier\": null,\r\n" + 
				"    \"startDate\": \"2019-04-12\",\r\n" + 
				"    \"endDate\": \"2020-04-11\",\r\n" + 
				"    \"policyTerm\": 1,\r\n" + 
				"    \"policyNumber\": null,\r\n" + 
				"    \"externalPolicyNumber\": \"\",\r\n" + 
				"    \"coverages\": [\r\n" + 
				"      {\r\n" + 
				"        \"coverType\": \"THIRD_PARTY\",\r\n" + 
				"        \"name\": \"Motor Third Party Liability Insurance\",\r\n" + 
				"        \"coverCode\": \"THIRD_PARTY\",\r\n" + 
				"        \"componentNumber\": null,\r\n" + 
				"        \"selection\": true,\r\n" + 
				"        \"coverTerm\": null,\r\n" + 
				"        \"status\": null,\r\n" + 
				"        \"coPay\": null,\r\n" + 
				"        \"coverAvailability\": \"MANDATORY\",\r\n" + 
				"        \"value\": {\r\n" + 
				"          \"valueType\": null,\r\n" + 
				"          \"insuredValue\": 0,\r\n" + 
				"          \"minValue\": 0,\r\n" + 
				"          \"maxValue\": 0,\r\n" + 
				"          \"validValues\": null\r\n" + 
				"        },\r\n" + 
				"        \"voluntaryDeductible\": null,\r\n" + 
				"        \"compulsoryDeductible\": null,\r\n" + 
				"        \"discount\": null,\r\n" + 
				"        \"surcharge\": null,\r\n" + 
				"        \"subCovers\": [\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Property Damage\",\r\n" + 
				"            \"coverCode\": null,\r\n" + 
				"            \"componentNumber\": \"10061\",\r\n" + 
				"            \"selection\": true,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"MANDATORY\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": \"SUM_INSURED\",\r\n" + 
				"              \"insuredValue\": 750000,\r\n" + 
				"              \"minValue\": 0,\r\n" + 
				"              \"maxValue\": 0,\r\n" + 
				"              \"validValues\": [\r\n" + 
				"                6000,\r\n" + 
				"                750000\r\n" + 
				"              ]\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 1850.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"CNG Kit - IMT 25\",\r\n" + 
				"            \"coverCode\": \"ACCESSORIES_NEW\",\r\n" + 
				"            \"componentNumber\": \"10134\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"NOT_AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": null,\r\n" + 
				"              \"insuredValue\": 0,\r\n" + 
				"              \"minValue\": 0,\r\n" + 
				"              \"maxValue\": 0,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 0.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          }\r\n" + 
				"        ],\r\n" + 
				"        \"plan\": null,\r\n" + 
				"        \"tyreType\": null,\r\n" + 
				"        \"dailyConveyanceType\": null,\r\n" + 
				"        \"netPremium\": \"INR 0.00\",\r\n" + 
				"        \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"      },\r\n" + 
				"      {\r\n" + 
				"        \"coverType\": \"OWN_DAMAGE\",\r\n" + 
				"        \"name\": \"Motor Own Damage\",\r\n" + 
				"        \"coverCode\": \"OWN_DAMAGE\",\r\n" + 
				"        \"componentNumber\": null,\r\n" + 
				"        \"selection\": true,\r\n" + 
				"        \"coverTerm\": null,\r\n" + 
				"        \"status\": null,\r\n" + 
				"        \"coPay\": null,\r\n" + 
				"        \"coverAvailability\": \"MANDATORY\",\r\n" + 
				"        \"value\": {\r\n" + 
				"          \"valueType\": \"SUM_INSURED\",\r\n" + 
				"          \"insuredValue\": 402859,\r\n" + 
				"          \"minValue\": 322288,\r\n" + 
				"          \"maxValue\": 483431,\r\n" + 
				"          \"validValues\": null\r\n" + 
				"        },\r\n" + 
				"        \"voluntaryDeductible\": null,\r\n" + 
				"        \"compulsoryDeductible\": null,\r\n" + 
				"        \"discount\": {\r\n" + 
				"          \"userSpecialDiscountPercent\": 0,\r\n" + 
				"          \"defaultSpecialDiscountPercent\": 0,\r\n" + 
				"          \"impliedDiscountPercent\": 0,\r\n" + 
				"          \"effectiveSpecialDiscountPercent\": 0,\r\n" + 
				"          \"minSpecialDiscountPercent\": 0,\r\n" + 
				"          \"maxSpecialDiscountPercent\": 0,\r\n" + 
				"          \"isUWViolated\": false,\r\n" + 
				"          \"agentType\": null,\r\n" + 
				"          \"userDefaultCommissionPercent\": 0,\r\n" + 
				"          \"newUserCommissionPercent\": 0,\r\n" + 
				"          \"effectiveIncrementalCommissionPercent\": 0,\r\n" + 
				"          \"discounts\": [],\r\n" + 
				"          \"status\": 204\r\n" + 
				"        },\r\n" + 
				"        \"surcharge\": null,\r\n" + 
				"        \"subCovers\": [\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"IMT23\",\r\n" + 
				"            \"coverCode\": null,\r\n" + 
				"            \"componentNumber\": \"47860\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"NOT_AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": null,\r\n" + 
				"              \"insuredValue\": 0,\r\n" + 
				"              \"minValue\": 0,\r\n" + 
				"              \"maxValue\": 0,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 0.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"IMT 47 (Overturning Cover Exclusion)\",\r\n" + 
				"            \"coverCode\": null,\r\n" + 
				"            \"componentNumber\": \"47857\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"NOT_AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": null,\r\n" + 
				"              \"insuredValue\": 0,\r\n" + 
				"              \"minValue\": 0,\r\n" + 
				"              \"maxValue\": 0,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 0.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Compulsary Deductible - IMT 21\",\r\n" + 
				"            \"coverCode\": null,\r\n" + 
				"            \"componentNumber\": \"47862\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"NOT_AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": null,\r\n" + 
				"              \"insuredValue\": 0,\r\n" + 
				"              \"minValue\": 0,\r\n" + 
				"              \"maxValue\": 0,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 0.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Compulsary Deductible - IMT 22\",\r\n" + 
				"            \"coverCode\": null,\r\n" + 
				"            \"componentNumber\": \"10159\",\r\n" + 
				"            \"selection\": true,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"MANDATORY\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": null,\r\n" + 
				"              \"insuredValue\": 0,\r\n" + 
				"              \"minValue\": 0,\r\n" + 
				"              \"maxValue\": 0,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 0.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"CNG Kit - IMT 25\",\r\n" + 
				"            \"coverCode\": \"ACCESSORIES_NEW\",\r\n" + 
				"            \"componentNumber\": \"10134\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": \"SUM_INSURED\",\r\n" + 
				"              \"insuredValue\": 15000,\r\n" + 
				"              \"minValue\": 15000,\r\n" + 
				"              \"maxValue\": 80000,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 0.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Electrical Accessories - IMT 24\",\r\n" + 
				"            \"coverCode\": \"ACCESSORIES_NEW\",\r\n" + 
				"            \"componentNumber\": \"10132\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": \"SUM_INSURED\",\r\n" + 
				"              \"insuredValue\": 40,\r\n" + 
				"              \"minValue\": 40,\r\n" + 
				"              \"maxValue\": 60429,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 0.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Electrical Accessories - IMT 24\",\r\n" + 
				"            \"coverCode\": \"ACCESSORIES_NEW\",\r\n" + 
				"            \"componentNumber\": \"47800\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"NOT_AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": null,\r\n" + 
				"              \"insuredValue\": 0,\r\n" + 
				"              \"minValue\": 0,\r\n" + 
				"              \"maxValue\": 0,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 0.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Non Electrical Accessories - IMT 24\",\r\n" + 
				"            \"coverCode\": \"ACCESSORIES_NEW\",\r\n" + 
				"            \"componentNumber\": \"10133\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": \"SUM_INSURED\",\r\n" + 
				"              \"insuredValue\": 40,\r\n" + 
				"              \"minValue\": 40,\r\n" + 
				"              \"maxValue\": 40286,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 0.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Non Electrical Accessories - IMT 24\",\r\n" + 
				"            \"coverCode\": \"ACCESSORIES_NEW\",\r\n" + 
				"            \"componentNumber\": \"47801\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"NOT_AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": null,\r\n" + 
				"              \"insuredValue\": 0,\r\n" + 
				"              \"minValue\": 0,\r\n" + 
				"              \"maxValue\": 0,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 0.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          }\r\n" + 
				"        ],\r\n" + 
				"        \"plan\": \"PLAN_D\",\r\n" + 
				"        \"tyreType\": null,\r\n" + 
				"        \"dailyConveyanceType\": null,\r\n" + 
				"        \"netPremium\": \"INR 12597.00\",\r\n" + 
				"        \"tariffPremium\": \"INR 12597.00\"\r\n" + 
				"      },\r\n" + 
				"      {\r\n" + 
				"        \"coverType\": \"ADDONS\",\r\n" + 
				"        \"name\": \"Motor Own Damage -  Add Ons\",\r\n" + 
				"        \"coverCode\": \"ADDONS\",\r\n" + 
				"        \"componentNumber\": null,\r\n" + 
				"        \"selection\": false,\r\n" + 
				"        \"coverTerm\": null,\r\n" + 
				"        \"status\": null,\r\n" + 
				"        \"coPay\": null,\r\n" + 
				"        \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"        \"value\": {\r\n" + 
				"          \"valueType\": \"SUM_INSURED\",\r\n" + 
				"          \"insuredValue\": 402859,\r\n" + 
				"          \"minValue\": 322288,\r\n" + 
				"          \"maxValue\": 483431,\r\n" + 
				"          \"validValues\": null\r\n" + 
				"        },\r\n" + 
				"        \"voluntaryDeductible\": null,\r\n" + 
				"        \"compulsoryDeductible\": null,\r\n" + 
				"        \"discount\": null,\r\n" + 
				"        \"surcharge\": null,\r\n" + 
				"        \"subCovers\": [\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Breakdown Assistance\",\r\n" + 
				"            \"coverCode\": \"RSA_ADDON\",\r\n" + 
				"            \"componentNumber\": null,\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": null,\r\n" + 
				"              \"insuredValue\": 0,\r\n" + 
				"              \"minValue\": 0,\r\n" + 
				"              \"maxValue\": 0,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 109.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Tyre Protect\",\r\n" + 
				"            \"coverCode\": \"OTHER_ADDON\",\r\n" + 
				"            \"componentNumber\": \"10035\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": null,\r\n" + 
				"              \"insuredValue\": 0,\r\n" + 
				"              \"minValue\": 0,\r\n" + 
				"              \"maxValue\": 0,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 119.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Parts Depreciation Protect\",\r\n" + 
				"            \"coverCode\": \"OTHER_ADDON\",\r\n" + 
				"            \"componentNumber\": \"10031\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"NOT_AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": null,\r\n" + 
				"              \"insuredValue\": 0,\r\n" + 
				"              \"minValue\": 0,\r\n" + 
				"              \"maxValue\": 0,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [\r\n" + 
				"              {\r\n" + 
				"                \"coverType\": \"Cover\",\r\n" + 
				"                \"name\": \"Cover only 1 claims per year\",\r\n" + 
				"                \"coverCode\": null,\r\n" + 
				"                \"componentNumber\": \"10143\",\r\n" + 
				"                \"selection\": true,\r\n" + 
				"                \"coverTerm\": null,\r\n" + 
				"                \"status\": null,\r\n" + 
				"                \"coPay\": null,\r\n" + 
				"                \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"                \"value\": {\r\n" + 
				"                  \"valueType\": null,\r\n" + 
				"                  \"insuredValue\": 0,\r\n" + 
				"                  \"minValue\": 0,\r\n" + 
				"                  \"maxValue\": 0,\r\n" + 
				"                  \"validValues\": null\r\n" + 
				"                },\r\n" + 
				"                \"voluntaryDeductible\": null,\r\n" + 
				"                \"compulsoryDeductible\": null,\r\n" + 
				"                \"discount\": null,\r\n" + 
				"                \"surcharge\": null,\r\n" + 
				"                \"subCovers\": [],\r\n" + 
				"                \"plan\": null,\r\n" + 
				"                \"tyreType\": null,\r\n" + 
				"                \"dailyConveyanceType\": null,\r\n" + 
				"                \"netPremium\": \"INR 0.00\",\r\n" + 
				"                \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"              },\r\n" + 
				"              {\r\n" + 
				"                \"coverType\": \"Cover\",\r\n" + 
				"                \"name\": \"Cover only 2 claims per year\",\r\n" + 
				"                \"coverCode\": null,\r\n" + 
				"                \"componentNumber\": \"10144\",\r\n" + 
				"                \"selection\": false,\r\n" + 
				"                \"coverTerm\": null,\r\n" + 
				"                \"status\": null,\r\n" + 
				"                \"coPay\": null,\r\n" + 
				"                \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"                \"value\": {\r\n" + 
				"                  \"valueType\": null,\r\n" + 
				"                  \"insuredValue\": 0,\r\n" + 
				"                  \"minValue\": 0,\r\n" + 
				"                  \"maxValue\": 0,\r\n" + 
				"                  \"validValues\": null\r\n" + 
				"                },\r\n" + 
				"                \"voluntaryDeductible\": null,\r\n" + 
				"                \"compulsoryDeductible\": null,\r\n" + 
				"                \"discount\": null,\r\n" + 
				"                \"surcharge\": null,\r\n" + 
				"                \"subCovers\": [],\r\n" + 
				"                \"plan\": null,\r\n" + 
				"                \"tyreType\": null,\r\n" + 
				"                \"dailyConveyanceType\": null,\r\n" + 
				"                \"netPremium\": \"INR 0.00\",\r\n" + 
				"                \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"              },\r\n" + 
				"              {\r\n" + 
				"                \"coverType\": \"Cover\",\r\n" + 
				"                \"name\": \"Cover Unlimited Claims\",\r\n" + 
				"                \"coverCode\": null,\r\n" + 
				"                \"componentNumber\": \"10145\",\r\n" + 
				"                \"selection\": false,\r\n" + 
				"                \"coverTerm\": null,\r\n" + 
				"                \"status\": null,\r\n" + 
				"                \"coPay\": null,\r\n" + 
				"                \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"                \"value\": {\r\n" + 
				"                  \"valueType\": null,\r\n" + 
				"                  \"insuredValue\": 0,\r\n" + 
				"                  \"minValue\": 0,\r\n" + 
				"                  \"maxValue\": 0,\r\n" + 
				"                  \"validValues\": null\r\n" + 
				"                },\r\n" + 
				"                \"voluntaryDeductible\": null,\r\n" + 
				"                \"compulsoryDeductible\": null,\r\n" + 
				"                \"discount\": null,\r\n" + 
				"                \"surcharge\": null,\r\n" + 
				"                \"subCovers\": [],\r\n" + 
				"                \"plan\": null,\r\n" + 
				"                \"tyreType\": null,\r\n" + 
				"                \"dailyConveyanceType\": null,\r\n" + 
				"                \"netPremium\": \"INR 0.00\",\r\n" + 
				"                \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"              }\r\n" + 
				"            ],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 0.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Consumable cover\",\r\n" + 
				"            \"coverCode\": \"OTHER_ADDON\",\r\n" + 
				"            \"componentNumber\": \"10038\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": null,\r\n" + 
				"              \"insuredValue\": 0,\r\n" + 
				"              \"minValue\": 0,\r\n" + 
				"              \"maxValue\": 0,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 282.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Engine and Gear Box Protect\",\r\n" + 
				"            \"coverCode\": \"OTHER_ADDON\",\r\n" + 
				"            \"componentNumber\": \"10032\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": null,\r\n" + 
				"              \"insuredValue\": 0,\r\n" + 
				"              \"minValue\": 0,\r\n" + 
				"              \"maxValue\": 0,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 410.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Return to Invoice\",\r\n" + 
				"            \"coverCode\": \"OTHER_ADDON\",\r\n" + 
				"            \"componentNumber\": \"10034\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": null,\r\n" + 
				"              \"insuredValue\": 0,\r\n" + 
				"              \"minValue\": 0,\r\n" + 
				"              \"maxValue\": 0,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 1802.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          }\r\n" + 
				"        ],\r\n" + 
				"        \"plan\": null,\r\n" + 
				"        \"tyreType\": null,\r\n" + 
				"        \"dailyConveyanceType\": null,\r\n" + 
				"        \"netPremium\": \"INR 0.00\",\r\n" + 
				"        \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"      },\r\n" + 
				"      {\r\n" + 
				"        \"coverType\": \"PA_OWNER\",\r\n" + 
				"        \"name\": \"PA Owner Driver - IMT 15\",\r\n" + 
				"        \"coverCode\": \"PA_OWNER\",\r\n" + 
				"        \"componentNumber\": null,\r\n" + 
				"        \"selection\": true,\r\n" + 
				"        \"coverTerm\": null,\r\n" + 
				"        \"status\": null,\r\n" + 
				"        \"coPay\": null,\r\n" + 
				"        \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"        \"value\": {\r\n" + 
				"          \"valueType\": \"SUM_INSURED\",\r\n" + 
				"          \"insuredValue\": 1500000,\r\n" + 
				"          \"minValue\": 0,\r\n" + 
				"          \"maxValue\": 0,\r\n" + 
				"          \"validValues\": null\r\n" + 
				"        },\r\n" + 
				"        \"voluntaryDeductible\": null,\r\n" + 
				"        \"compulsoryDeductible\": null,\r\n" + 
				"        \"discount\": null,\r\n" + 
				"        \"surcharge\": null,\r\n" + 
				"        \"subCovers\": [\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Personal Accident\",\r\n" + 
				"            \"coverCode\": \"PA_OWNER\",\r\n" + 
				"            \"componentNumber\": \"10028\",\r\n" + 
				"            \"selection\": true,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"MANDATORY\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": \"SUM_INSURED\",\r\n" + 
				"              \"insuredValue\": 1500000,\r\n" + 
				"              \"minValue\": 0,\r\n" + 
				"              \"maxValue\": 0,\r\n" + 
				"              \"validValues\": [\r\n" + 
				"                1500000\r\n" + 
				"              ]\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 330.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Personal Accident\",\r\n" + 
				"            \"coverCode\": \"PA_OWNER\",\r\n" + 
				"            \"componentNumber\": \"10174\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"NOT_AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": null,\r\n" + 
				"              \"insuredValue\": 0,\r\n" + 
				"              \"minValue\": 0,\r\n" + 
				"              \"maxValue\": 0,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 0.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Term Plan\",\r\n" + 
				"            \"coverCode\": \"PA_OWNER\",\r\n" + 
				"            \"componentNumber\": \"48081\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"NOT_AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": \"TERM_COUNT\",\r\n" + 
				"              \"insuredValue\": 0,\r\n" + 
				"              \"minValue\": 0,\r\n" + 
				"              \"maxValue\": 0,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 0.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          }\r\n" + 
				"        ],\r\n" + 
				"        \"plan\": null,\r\n" + 
				"        \"tyreType\": null,\r\n" + 
				"        \"dailyConveyanceType\": null,\r\n" + 
				"        \"netPremium\": \"INR 0.00\",\r\n" + 
				"        \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"      },\r\n" + 
				"      {\r\n" + 
				"        \"coverType\": \"PA_UNNAMED\",\r\n" + 
				"        \"name\": \"Unnamed PA Cover\",\r\n" + 
				"        \"coverCode\": \"PA_UNNAMED\",\r\n" + 
				"        \"componentNumber\": null,\r\n" + 
				"        \"selection\": false,\r\n" + 
				"        \"coverTerm\": null,\r\n" + 
				"        \"status\": null,\r\n" + 
				"        \"coPay\": null,\r\n" + 
				"        \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"        \"value\": {\r\n" + 
				"          \"valueType\": \"SUM_INSURED\",\r\n" + 
				"          \"insuredValue\": 100000,\r\n" + 
				"          \"minValue\": 10000,\r\n" + 
				"          \"maxValue\": 200000,\r\n" + 
				"          \"validValues\": [\r\n" + 
				"            10000,\r\n" + 
				"            20000,\r\n" + 
				"            30000,\r\n" + 
				"            40000,\r\n" + 
				"            50000,\r\n" + 
				"            60000,\r\n" + 
				"            70000,\r\n" + 
				"            80000,\r\n" + 
				"            90000,\r\n" + 
				"            100000,\r\n" + 
				"            110000,\r\n" + 
				"            120000,\r\n" + 
				"            130000,\r\n" + 
				"            140000,\r\n" + 
				"            150000,\r\n" + 
				"            160000,\r\n" + 
				"            170000,\r\n" + 
				"            180000,\r\n" + 
				"            190000,\r\n" + 
				"            200000\r\n" + 
				"          ]\r\n" + 
				"        },\r\n" + 
				"        \"voluntaryDeductible\": null,\r\n" + 
				"        \"compulsoryDeductible\": null,\r\n" + 
				"        \"discount\": null,\r\n" + 
				"        \"surcharge\": null,\r\n" + 
				"        \"subCovers\": [\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"PA cover for Unnamed Passenger - IMT 16\",\r\n" + 
				"            \"coverCode\": null,\r\n" + 
				"            \"componentNumber\": \"47722\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": \"SUM_INSURED\",\r\n" + 
				"              \"insuredValue\": 100000,\r\n" + 
				"              \"minValue\": 10000,\r\n" + 
				"              \"maxValue\": 200000,\r\n" + 
				"              \"validValues\": [\r\n" + 
				"                10000,\r\n" + 
				"                20000,\r\n" + 
				"                30000,\r\n" + 
				"                40000,\r\n" + 
				"                50000,\r\n" + 
				"                60000,\r\n" + 
				"                70000,\r\n" + 
				"                80000,\r\n" + 
				"                90000,\r\n" + 
				"                100000,\r\n" + 
				"                110000,\r\n" + 
				"                120000,\r\n" + 
				"                130000,\r\n" + 
				"                140000,\r\n" + 
				"                150000,\r\n" + 
				"                160000,\r\n" + 
				"                170000,\r\n" + 
				"                180000,\r\n" + 
				"                190000,\r\n" + 
				"                200000\r\n" + 
				"              ]\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [\r\n" + 
				"              {\r\n" + 
				"                \"coverType\": \"Cover\",\r\n" + 
				"                \"name\": \"Number of Unnamed Passenger\",\r\n" + 
				"                \"coverCode\": null,\r\n" + 
				"                \"componentNumber\": \"47716\",\r\n" + 
				"                \"selection\": true,\r\n" + 
				"                \"coverTerm\": null,\r\n" + 
				"                \"status\": null,\r\n" + 
				"                \"coPay\": null,\r\n" + 
				"                \"coverAvailability\": \"MANDATORY\",\r\n" + 
				"                \"value\": {\r\n" + 
				"                  \"valueType\": \"INSURED_COUNT\",\r\n" + 
				"                  \"insuredValue\": 5,\r\n" + 
				"                  \"minValue\": 1,\r\n" + 
				"                  \"maxValue\": 1,\r\n" + 
				"                  \"validValues\": null\r\n" + 
				"                },\r\n" + 
				"                \"voluntaryDeductible\": null,\r\n" + 
				"                \"compulsoryDeductible\": null,\r\n" + 
				"                \"discount\": null,\r\n" + 
				"                \"surcharge\": null,\r\n" + 
				"                \"subCovers\": [],\r\n" + 
				"                \"plan\": null,\r\n" + 
				"                \"tyreType\": null,\r\n" + 
				"                \"dailyConveyanceType\": null,\r\n" + 
				"                \"netPremium\": \"INR 0.00\",\r\n" + 
				"                \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"              }\r\n" + 
				"            ],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 250.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"PA cover for Paid Driver - IMT 17\",\r\n" + 
				"            \"coverCode\": null,\r\n" + 
				"            \"componentNumber\": \"47721\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": null,\r\n" + 
				"              \"insuredValue\": 200000,\r\n" + 
				"              \"minValue\": 100000,\r\n" + 
				"              \"maxValue\": 200000,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [\r\n" + 
				"              {\r\n" + 
				"                \"coverType\": \"Cover\",\r\n" + 
				"                \"name\": \"Number of Paid Driver\",\r\n" + 
				"                \"coverCode\": null,\r\n" + 
				"                \"componentNumber\": \"47713\",\r\n" + 
				"                \"selection\": true,\r\n" + 
				"                \"coverTerm\": null,\r\n" + 
				"                \"status\": null,\r\n" + 
				"                \"coPay\": null,\r\n" + 
				"                \"coverAvailability\": \"MANDATORY\",\r\n" + 
				"                \"value\": {\r\n" + 
				"                  \"valueType\": \"INSURED_COUNT\",\r\n" + 
				"                  \"insuredValue\": 1,\r\n" + 
				"                  \"minValue\": 1,\r\n" + 
				"                  \"maxValue\": 1,\r\n" + 
				"                  \"validValues\": null\r\n" + 
				"                },\r\n" + 
				"                \"voluntaryDeductible\": null,\r\n" + 
				"                \"compulsoryDeductible\": null,\r\n" + 
				"                \"discount\": null,\r\n" + 
				"                \"surcharge\": null,\r\n" + 
				"                \"subCovers\": [],\r\n" + 
				"                \"plan\": null,\r\n" + 
				"                \"tyreType\": null,\r\n" + 
				"                \"dailyConveyanceType\": null,\r\n" + 
				"                \"netPremium\": \"INR 0.00\",\r\n" + 
				"                \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"              }\r\n" + 
				"            ],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 100.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          }\r\n" + 
				"        ],\r\n" + 
				"        \"plan\": null,\r\n" + 
				"        \"tyreType\": null,\r\n" + 
				"        \"dailyConveyanceType\": null,\r\n" + 
				"        \"netPremium\": \"INR 0.00\",\r\n" + 
				"        \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"      },\r\n" + 
				"      {\r\n" + 
				"        \"coverType\": \"LEGAL_LIABILITY\",\r\n" + 
				"        \"name\": \"Legal Liability Cover\",\r\n" + 
				"        \"coverCode\": \"LEGAL_LIABILITY\",\r\n" + 
				"        \"componentNumber\": null,\r\n" + 
				"        \"selection\": false,\r\n" + 
				"        \"coverTerm\": null,\r\n" + 
				"        \"status\": null,\r\n" + 
				"        \"coPay\": null,\r\n" + 
				"        \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"        \"value\": {\r\n" + 
				"          \"valueType\": null,\r\n" + 
				"          \"insuredValue\": 0,\r\n" + 
				"          \"minValue\": 0,\r\n" + 
				"          \"maxValue\": 0,\r\n" + 
				"          \"validValues\": null\r\n" + 
				"        },\r\n" + 
				"        \"voluntaryDeductible\": null,\r\n" + 
				"        \"compulsoryDeductible\": null,\r\n" + 
				"        \"discount\": null,\r\n" + 
				"        \"surcharge\": null,\r\n" + 
				"        \"subCovers\": [\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Legal Liability to Paid Driver - IMT 28\",\r\n" + 
				"            \"coverCode\": null,\r\n" + 
				"            \"componentNumber\": \"10181\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": null,\r\n" + 
				"              \"insuredValue\": 1,\r\n" + 
				"              \"minValue\": 1,\r\n" + 
				"              \"maxValue\": 1,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 50.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          },\r\n" + 
				"          {\r\n" + 
				"            \"coverType\": \"Cover\",\r\n" + 
				"            \"name\": \"Legal Liability to Employees - IMT 29\",\r\n" + 
				"            \"coverCode\": null,\r\n" + 
				"            \"componentNumber\": \"47749\",\r\n" + 
				"            \"selection\": false,\r\n" + 
				"            \"coverTerm\": null,\r\n" + 
				"            \"status\": null,\r\n" + 
				"            \"coPay\": null,\r\n" + 
				"            \"coverAvailability\": \"AVAILABLE\",\r\n" + 
				"            \"value\": {\r\n" + 
				"              \"valueType\": null,\r\n" + 
				"              \"insuredValue\": 1,\r\n" + 
				"              \"minValue\": 1,\r\n" + 
				"              \"maxValue\": 1,\r\n" + 
				"              \"validValues\": null\r\n" + 
				"            },\r\n" + 
				"            \"voluntaryDeductible\": null,\r\n" + 
				"            \"compulsoryDeductible\": null,\r\n" + 
				"            \"discount\": null,\r\n" + 
				"            \"surcharge\": null,\r\n" + 
				"            \"subCovers\": [],\r\n" + 
				"            \"plan\": null,\r\n" + 
				"            \"tyreType\": null,\r\n" + 
				"            \"dailyConveyanceType\": null,\r\n" + 
				"            \"netPremium\": \"INR 50.00\",\r\n" + 
				"            \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"          }\r\n" + 
				"        ],\r\n" + 
				"        \"plan\": null,\r\n" + 
				"        \"tyreType\": null,\r\n" + 
				"        \"dailyConveyanceType\": null,\r\n" + 
				"        \"netPremium\": \"INR 0.00\",\r\n" + 
				"        \"tariffPremium\": \"INR 0.00\"\r\n" + 
				"      }\r\n" + 
				"    ],\r\n" + 
				"    \"policyHolderType\": \"INDIVIDUAL\",\r\n" + 
				"    \"currentNoClaimBonus\": null,\r\n" + 
				"    \"isNCBTransfer\": null,\r\n" + 
				"    \"purchaseDate\": null,\r\n" + 
				"    \"quotationDate\": null,\r\n" + 
				"    \"agentCode\": \"\",\r\n" + 
				"    \"isRenewalAvailable\": null\r\n" + 
				"  },\r\n" + 
				"  \"persons\": [\r\n" + 
				"    {\r\n" + 
				"      \"personType\": \"INDIVIDUAL\",\r\n" + 
				"      \"partyId\": \"\",\r\n" + 
				"      \"addresses\": [\r\n" + 
				"        {\r\n" + 
				"          \"addressType\": \"PRIMARY_RESIDENCE\",\r\n" + 
				"          \"flatNumber\": null,\r\n" + 
				"          \"streetNumber\": null,\r\n" + 
				"          \"street\": \"asdfbn\",\r\n" + 
				"          \"district\": \"\",\r\n" + 
				"          \"state\": \"9\",\r\n" + 
				"          \"city\": \"Agra\",\r\n" + 
				"          \"country\": \"IN\",\r\n" + 
				"          \"pincode\": \"282002\",\r\n" + 
				"          \"geoCode\": null,\r\n" + 
				"          \"zone\": null\r\n" + 
				"        }\r\n" + 
				"      ],\r\n" + 
				"      \"communications\": [\r\n" + 
				"        {\r\n" + 
				"          \"communicationType\": \"MOBILE\",\r\n" + 
				"          \"communicationId\": \"1234567890\",\r\n" + 
				"          \"isPrefferedCommunication\": true\r\n" + 
				"        },\r\n" + 
				"        {\r\n" + 
				"          \"communicationType\": \"EMAIL\",\r\n" + 
				"          \"communicationId\": \"satheesh.kumar@godigit.com\",\r\n" + 
				"          \"isPrefferedCommunication\": true\r\n" + 
				"        }\r\n" + 
				"      ],\r\n" + 
				"      \"identificationDocuments\": [],\r\n" + 
				"      \"isPolicyHolder\": true,\r\n" + 
				"      \"isPayer\": null,\r\n" + 
				"      \"isVehicleOwner\": true,\r\n" + 
				"      \"title\": null,\r\n" + 
				"      \"firstName\": \"SATSAY\",\r\n" + 
				"      \"middleName\": null,\r\n" + 
				"      \"lastName\": \".\",\r\n" + 
				"      \"dateOfBirth\": \"1901-01-01\",\r\n" + 
				"      \"gender\": \"MALE\",\r\n" + 
				"      \"relation\": null,\r\n" + 
				"      \"isDriver\": true,\r\n" + 
				"      \"isInsuredPerson\": true\r\n" + 
				"    }\r\n" + 
				"  ],\r\n" + 
				"  \"dealer\": null,\r\n" + 
				"  \"motorQuestions\": {\r\n" + 
				"    \"furtherAgreement\": null,\r\n" + 
				"    \"selfInspection\": true,\r\n" + 
				"    \"financer\": null\r\n" + 
				"  },\r\n" + 
				"  \"nominee\": null,\r\n" + 
				"  \"payment\": null,\r\n" + 
				"  \"documents\": []\r\n" + 
				"}";
		
		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		
		//print result
		System.out.println(response.toString());
		
		
		 Object jsonObj = new JSONParser().parse(response.toString()); 
         
	        // typecasting obj to JSONObject 
	        JSONObject jo = (JSONObject) jsonObj; 
	        System.out.println("Application ID : "+jo.get("applicationId"));
	}

}