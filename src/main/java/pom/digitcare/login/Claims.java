package pom.digitcare.login;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import com.utilities.Library;

public class Claims {

	@FindBy(id = "fname")
	private WebElement txtFirstName;

	@FindBy(id = "lname")
	private WebElement txtLastName;

	@FindBy(id = "email")
	private WebElement txtEmailAddress;

	@FindBy(id = "mobile")
	private WebElement txtMobileNumber;

	@FindBy(id = "claimNo")
	private WebElement txtClaimNumber;

	@FindBy(id = "policyNo")
	private WebElement txtPolicyNumber;

	@FindBy(id = "product")
	private WebElement drpProduct;

	@FindBy(xpath = "//button[text()='Search']")
	private WebElement btnSearch;

	@FindBy(xpath = "//input[@id='myonoffswitch']/../label/span[2]")
	private WebElement chkPolicyClaim;

	@FindBy(id = "reporterOfClaim")
	private WebElement drpReporterOfClaim;

	@FindBy(id = "nameOfReporter")
	private WebElement txtNameofCaller;

	@FindBy(id = "phOfReporter")
	private WebElement txtContactNumberofCaller;

	@FindBy(id = "emailOfReporter")
	private WebElement txtEmailofCaller;

	@FindBy(id = "customerMobile")
	private WebElement txtCustomerMobileNumber;

	@FindBy(id = "customerAdd")
	private WebElement txtCustomerAddress;

	@FindBy(id = "dateOfLoss")
	private WebElement txtDateofLoss;

	@FindBy(id = "timeOfLoss")
	private WebElement txtTimeofLoss;

	@FindBy(name = "lossLocation")
	private WebElement txtLossLocationCity;

	@FindBy(id = "lossLocationList")
	private WebElement lstLossLocationCity;

	@FindBy(id = "losslocationAddress")
	private WebElement txtLossLocationAddress;

	@FindBy(id = "descriptionOfTheft")
	private WebElement txtDescriptionofLoss;

	@FindBy(id = "damagedPartsDetails")
	private WebElement txtDamagedPartsDetails;

	@FindBy(id = "typeOfClaim")
	private WebElement drpTypeofClaim;

	@FindBy(id = "typeOfCoverCodes")
	private WebElement drpTypeofCover;

	@FindBy(id = "typeOfLoss")
	private WebElement drpTypeofLoss;

	@FindBy(id = "causeOfLoss")
	private WebElement drpCauseofLoss;

	@FindBy(id = "callerid")
	private WebElement txtCallerId;

	@FindBy(id = "thirdPartyDamage")
	private WebElement drpThirdPartyDamage;

	@FindBy(id = "whoWasDriving")
	private WebElement drpWhoWasDriving;

	@FindBy(id = "driverDetails1")
	private WebElement txtDriverName;

	@FindBy(id = "driverDetails2")
	private WebElement txtDriverContactNumber;

	@FindBy(id = "driverDetails3")
	private WebElement txtDriverLicenseNumber;

	@FindBy(id = "vehicleInspectionAddress")
	private WebElement txtVehicleInspectionAddress;

	@FindBy(xpath = "//label[@for='policeIntimated']/following-sibling::label[text()='Yes']")
	private WebElement radPoliceIntimatedYes;

	@FindBy(xpath = "//label[@for='policeIntimated']/following-sibling::label[text()='No']")
	private WebElement radPoliceIntimatedNo;

	@FindBy(id = "stationName")
	private WebElement txtStationName;

	@FindBy(id = "firNumber")
	private WebElement txtFIRNumber;

	@FindBy(id = "numOfInjuries")
	private WebElement txtNumberofInjuries;

	@FindBy(id = "dateOfLossForPolice")
	private WebElement txtDateofFIR;

	@FindBy(id = "reasonForPolice")
	private WebElement txtReasonforPoliceNotIntimated;

	@FindAll(value = { @FindBy(xpath = "//div[@class='row']//table/tbody//tr") })
	private List<WebElement> lstSearchRows;

	@FindBy(xpath = "//button[text()='Register Claim']")
	private WebElement btnRegisterClaim;

	@FindBy(xpath = "//div[@id='modalClaimNumber']//b")
	private WebElement lblClaimNumber;

	@FindBy(id = "closeDownloadModal")
	private WebElement btnClaimNumberDone;

	private com.utilities.Library library = new com.utilities.Library();

	public void searchWithDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "firstname":
					txtFirstName.sendKeys(hmap.get(key));
					break;
				case "lastname":
					txtLastName.sendKeys(hmap.get(key));
					break;
				case "email":
				case "emailaddress":
					txtEmailAddress.sendKeys(hmap.get(key));
					break;
				case "mobilenumber":
				case "mobile":
					txtMobileNumber.sendKeys(hmap.get(key));
					break;
				case "claimnumber":
					txtClaimNumber.sendKeys(hmap.get(key));
					break;
				case "policynumber":
					txtPolicyNumber.sendKeys(hmap.get(key));
					break;
				case "product":
					library.selectDropDownValue(drpProduct, hmap.get(key));
					break;
				case "policy/claim":
					chkPolicyClaim.click();
					break;
				default:
					System.out.println("Implememntation for the field '" + key + "' is not found");
				}
			}
			btnSearch.click();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public void performActionInSearchTable(String action, String row) throws Exception {
		try {
			library.waitforDigitCareLoading();
			int rowsPresent = lstSearchRows.size();
			int rowInput = 0;
			try {
				rowInput = Integer.parseInt(row);
			} catch (NumberFormatException ne) {
				throw new Exception("The given value '" + row + "' is not a valid number");
			}
			if (rowsPresent < rowInput)
				throw new Exception("The total number of rows present in the table is " + rowsPresent
						+ ". Unable to perform '" + action + "' for row " + row);
			switch (action.toLowerCase().replaceAll(" ", "")) {
			case "create":
				lstSearchRows.get(rowInput - 1).findElement(By.xpath("//i[text()='create']")).click();
				break;
			case "email":
			case "mail":
				lstSearchRows.get(rowInput - 1).findElement(By.xpath("//i[text()='email']")).click();
				break;
			case "fileupload":
			case "upload":
			case "uploaddocument":
				lstSearchRows.get(rowInput - 1).findElement(By.xpath("//i[text()='file_upload']")).click();
				break;
			default:
				throw new Exception("The specified action '" + action + "' is not valid for Search Policy/Claim");
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public void enterReporterDetails(String details) throws Exception {
		library.waitforDigitCareLoading();
		HashMap<String, String> hmap = library.getFieldMap(details);
		try {
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "reporter":
				case "reporterofclaim":
					library.selectDropDownValue(drpReporterOfClaim, hmap.get(key));
					break;
				case "nameofthecaller":
				case "nameofcaller":
					txtNameofCaller.sendKeys(hmap.get(key));
					break;
				case "contactnumberofthecaller":
				case "contactnumberofcaller":
				case "contactnumber":
					txtContactNumberofCaller.sendKeys(hmap.get(key));
					break;
				case "emailofthecaller":
				case "emailofcaller":
				case "email":
					txtEmailofCaller.sendKeys(hmap.get(key));
					break;
				case "customernumber":
				case "customermobilenumber":
				case "mobilenumber":
					txtCustomerMobileNumber.sendKeys(hmap.get(key));
					break;
				case "customeraddress":
				case "address":
					txtCustomerAddress.sendKeys(hmap.get(key));
					break;
				default:
					System.out.println("Implementation for the field '" + key + "' is not found");
				}
			}

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void enterLossDetails(String details) throws Exception {
		library.waitforDigitCareLoading();
		HashMap<String, String> hmap = library.getFieldMap(details);
		try {
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "lossdate":
				case "dateofloss":
					txtDateofLoss.sendKeys(hmap.get(key));
					break;
				case "losstime":
				case "timeofloss":
					txtTimeofLoss.sendKeys(hmap.get(key));
					break;
				case "losslocationcity":
				case "damagelocationcity":
					txtLossLocationCity.sendKeys(hmap.get(key));
					break;
				case "losslocationaddress":
				case "damagelocationaddress":
					txtLossLocationAddress.sendKeys(hmap.get(key));
					break;
				case "descriptionofloss":
					txtDescriptionofLoss.sendKeys(hmap.get(key));
					break;
				case "damagedetails":
				case "damagedpartsdetails":
					txtDamagedPartsDetails.sendKeys(hmap.get(key));
					break;
				default:
					System.out.println("Implementation for the field '" + key + "' is not found");
				}
			}

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void enterClaimsDetails(String details) throws Exception {
		library.waitforDigitCareLoading();
		HashMap<String, String> hmap = library.getFieldMap(details);
		try {
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "typeofclaim":
					library.selectDropDownValue(drpTypeofClaim, hmap.get(key));
					break;
				case "typeofcover":
					library.selectDropDownValue(drpTypeofCover, hmap.get(key));
					break;
				case "typeofloss":
					library.selectDropDownValue(drpTypeofLoss, hmap.get(key));
					break;
				case "causeofloss":
					library.selectDropDownValue(drpCauseofLoss, hmap.get(key));
					break;
				case "callid":
					txtCallerId.sendKeys(hmap.get(key));
					break;
				case "damagedetails":
				case "damagedpartsdetails":
					txtDamagedPartsDetails.sendKeys(hmap.get(key));
					break;
				case "thirdpartydamage":
				case "third-partydamage":
					library.selectDropDownValue(drpThirdPartyDamage, hmap.get(key));
					break;
				case "whowasdriving":
					library.selectDropDownValue(drpWhoWasDriving, hmap.get(key));
					break;
				case "drivername":
					txtDriverName.sendKeys(hmap.get(key));
					break;
				case "drivercontactnumber":
					txtDriverContactNumber.sendKeys(hmap.get(key));
					break;
				case "driverlicensenumber":
					txtDriverLicenseNumber.sendKeys(hmap.get(key));
					break;
				default:
					System.out.println("Implementation for the field '" + key + "' is not found");
				}
			}

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void enterEnquiryDetails(String details) throws Exception {
		library.waitforDigitCareLoading();
		HashMap<String, String> hmap = library.getFieldMap(details);
		try {
			for (String key : hmap.keySet()) {
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "policehasbeenintimated":
				case "policeintimated":
					if (hmap.get(key).trim().equalsIgnoreCase("yes"))
						radPoliceIntimatedYes.click();
					else
						radPoliceIntimatedNo.click();
					break;
				case "policestationname":
				case "policestation":
					txtStationName.sendKeys(hmap.get(key));
					break;
				case "firnumber":
					txtFIRNumber.sendKeys(hmap.get(key));
					break;
				case "dateoffir":
				case "firdate":
					txtDateofFIR.sendKeys(hmap.get(key));
					break;
				case "whypolicewasn'tintimated":
					txtReasonforPoliceNotIntimated.sendKeys(hmap.get(key));
					break;
				default:
					System.out.println("Implementation for the field '" + key + "' is not found");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public void performAction(String action) throws Exception {
		try {
			switch (action.toLowerCase().replaceAll(" ", "")) {
			case "registerclaim":
			case "register":
				btnRegisterClaim.click();
				break;
			default:
				throw new Exception("The specified action '" + action
						+ "' is not recognised or not a valid action for the page 'Inspection details'");
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void closeClaimCreationPopup() throws Exception {
		try {
			library.waitforDigitCareLoading();
			//library.waitForElementVisible(lblClaimNumber, 20, 200);
			if(lblClaimNumber.isDisplayed())
			{
				System.out.println("Claim Number size : "+lblClaimNumber.getSize());
				String claimNumber = lblClaimNumber.getText().split(":")[1].trim();
				System.out.println("Claim Number : "+claimNumber);
				Library.staticMap.put("Claim Number", claimNumber);
			}
			else
				throw new Exception("Confirmation popup with the Claim number is not displayed");
			if(btnClaimNumberDone.isDisplayed())
				btnClaimNumberDone.click();
			else
				throw new Exception("Button 'Done' is not displayed in the Confirmation popup");
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}
