package motor_insurance_api;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

import com.utilities.PropertyFileReader;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RenewalTwoWheelerComprehensive {

	public static void main(String[] args) throws IOException {
		
		RenewalTwoWheelerComprehensive obj = new RenewalTwoWheelerComprehensive();
		obj.agentFloat_1yrOD_1yrTP();
	}
	
	public void agentFloat_1yrOD_1yrTP() throws IOException {
		
		com.utilities.Library lib = new com.utilities.Library();

		PropertyFileReader read = new PropertyFileReader();
		
		RequestSpecification baseURI = given()
				.auth().preemptive().basic(read.getMotorAPIUsername(), read.getMotorAPIPassword())
				.baseUri("https://preprod-digitplusservice.godigit.com/digitplusservices");
		
//		String policyForRenewal = "D500013436";
		String policyForRenewal = "D500007010";
		Response searchContract = baseURI
				.contentType("Application/json").accept("application/json")
				.get("/searchContract?policyNumber="+policyForRenewal);
		
		Response renewalData = baseURI
				.contentType("Application/json").accept("application/json")
				.get("/renewal/data?policyNumber="+policyForRenewal);
		System.out.println(renewalData.asPrettyString());
		Random random = new Random();
		
		String enquiryId = "DP-20201-Id-"+random.nextInt(1000);
		String vehicleIdentificationNumber = "VINDIGITSBIHDFCICICI"+(10+random.nextInt(89))+"X"+(10+random.nextInt(89));
		String engineNumber = (10+random.nextInt(89))+"AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89))+"ASD";
		String applicationId = searchContract.jsonPath().getString("applicationId");
		
		Object pinCode = getValue(searchContract, "persons[0].addresses[0].pincode");
		Object insuranceProductCode = getValue(searchContract, "contract.insuranceProductCode");
		Object subInsuranceProductCode = getValue(searchContract, "contract.subInsuranceProductCode");
		Object startDate = getValue(searchContract, "contract.startDate");
		Object endDate = getValue(searchContract, "contract.endDate");
		Object policyTerm = getValue(searchContract, "contract.policyTerm");
		Object currentNoClaimBonus = getValue(searchContract, "contract.currentNoClaimBonus");
		Object policyNumber = getValue(searchContract, "contract.policyNumber");
		Object coverages = getValue(searchContract, "contract.coverages");
		Object previousInsurerCode = getValue(searchContract, "previousInsurer.previousInsurerCode");
		Object vehicleMaincode = getValue(searchContract, "vehicle.vehicleMaincode");
		Object licensePlateNumber = getValue(searchContract, "vehicle.licensePlateNumber");
		Object registrationDate = getValue(searchContract, "vehicle.registrationDate");
		Object vehicleType = getValue(searchContract, "vehicle.vehicleType");
		Object usageType = getValue(searchContract, "vehicle.usageType");
		Object make = getValue(searchContract, "vehicle.make");
		Object model = getValue(searchContract, "vehicle.model");
		Object defaultIdv = getValue(searchContract, "vehicle.vehicleIDV.defaultIdv");
		Object minimumIdv = getValue(searchContract, "vehicle.vehicleIDV.minimumIdv");
		Object maximumIdv = getValue(searchContract, "vehicle.vehicleIDV.maximumIdv");

		String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/QQ_2WC_Renewal.json";
		
		File payloadFromFile = new File(payloadPath);
		
		JsonPath pathToPayload = JsonPath.given(payloadFromFile);
		
		lib.alterPayload("enquiryId", enquiryId, pathToPayload);
		lib.alterPayload("pinCode", pinCode, pathToPayload);
		lib.alterPayload("contract.insuranceProductCode", insuranceProductCode, pathToPayload);
		lib.alterPayload("contract.subInsuranceProductCode", subInsuranceProductCode, pathToPayload);
		lib.alterPayload("contract.startDate", startDate, pathToPayload);
		lib.alterPayload("contract.endDate", endDate, pathToPayload);
		lib.alterPayload("contract.policyTerm", policyTerm, pathToPayload);
		lib.alterPayload("contract.currentNoClaimBonus", currentNoClaimBonus, pathToPayload);
		lib.alterPayload("contract.policyNumber", policyNumber, pathToPayload);
		lib.alterPayload("contract.coverages", coverages, pathToPayload);
		lib.alterPayload("previousInsurer.previousInsurerCode", previousInsurerCode, pathToPayload);
		lib.alterPayload("previousInsurer.previousPolicyNumber", policyNumber, pathToPayload);
		lib.alterPayload("previousInsurer.previousPolicyExpiryDate", endDate, pathToPayload);
		lib.alterPayload("previousInsurer.previousNoClaimBonus", currentNoClaimBonus, pathToPayload);
		lib.alterPayload("previousInsurer.originalPreviousPolicyType", "1OD_1TP", pathToPayload);
		lib.alterPayload("vehicle.vehicleMaincode", vehicleMaincode, pathToPayload);
		lib.alterPayload("vehicle.licensePlateNumber", licensePlateNumber, pathToPayload);
		lib.alterPayload("vehicle.vehicleIdentificationNumber", vehicleIdentificationNumber, pathToPayload);
		lib.alterPayload("vehicle.engineNumber", engineNumber, pathToPayload);
		lib.alterPayload("vehicle.manufactureDate", "0001-01-01", pathToPayload);
		lib.alterPayload("vehicle.registrationDate", registrationDate, pathToPayload);
		lib.alterPayload("vehicle.vehicleType", vehicleType, pathToPayload);
		lib.alterPayload("vehicle.usageType", usageType, pathToPayload);
		lib.alterPayload("vehicle.make", make, pathToPayload);
		lib.alterPayload("vehicle.model", model, pathToPayload);
		lib.alterPayload("vehicle.vehicleIDV.defaultIdv", defaultIdv, pathToPayload);
		lib.alterPayload("vehicle.vehicleIDV.minimumIdv", minimumIdv, pathToPayload);
		lib.alterPayload("vehicle.vehicleIDV.maximumIdv", maximumIdv, pathToPayload);
		
		Map<String,Object> payload = pathToPayload.getMap("$");

		Response getOldNCBValue = baseURI
				.contentType("Application/json").accept("application/json")
				.get("/getValidNCBValue?ncb=0&expiryDate="+endDate.toString()+"&previousClaim=false&newVehicle=false");
		
		System.out.println(getOldNCBValue.asPrettyString());
		String ncbValue = getOldNCBValue.jsonPath().getString("ncb");
		System.out.println(ncbValue);
		System.out.println(endDate);
		
		Response getNewNCBValue = baseURI
				.contentType("Application/json").accept("application/json")
				.get("/getValidNCBValue?ncb="+ncbValue+"&expiryDate="+endDate.toString()+"&previousClaim=false&newVehicle=false");
		
		System.out.println(getNewNCBValue.asPrettyString());
//		ncbValue = getNewNCBValue.jsonPath().getString("ncb");
//		System.out.println(ncbValue);
		
		Response quickQuote = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/quickQuote?isPremiumRecalculate=true&isUserSpecialDiscountOpted=false&userId="+read.getMotorAPIUsername()
						+"&sourceType=20201motor&channel=D01&policyNumber="+policyNumber.toString()+"&applicationId="+applicationId);

		System.out.println(quickQuote.asPrettyString());
	}
	
	private Object getValue(Response response,String path) {
		Object value = "";
		try {
			@SuppressWarnings("unchecked")
			ArrayList<Object> jsonvalue = (ArrayList<Object>)response.jsonPath().getJsonObject(path);
			value = jsonvalue.get(0).toString();System.out.println(value.toString());
		} catch (Exception e) {
			System.out.println("No parameter present as \""+path+"\"");
		}
		return value;
	}
}
