package motor_insurance_api;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.Random;

import org.json.simple.JSONArray;

import com.utilities.Library;
import com.utilities.PropertyFileReader;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class NewFourWheelerComprehensive {

	@SuppressWarnings("unchecked")
	public void agentFloat_1YrOD() throws IOException {

		try {
			com.utilities.Library lib = new com.utilities.Library();

			PropertyFileReader read = new PropertyFileReader();
			
			RequestSpecification baseURI = given()
					.auth().preemptive().basic(read.getMotorAPIUsername(), read.getMotorAPIPassword())
					.baseUri("https://preprod-digitplusservice.godigit.com/digitplusservices");

			Calendar cal = Calendar.getInstance(); 
			SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
			Random random = new Random();

			String previousPolicyExpiryDate = s.format(cal.getTime());
			String enquiryId = "DP-20101-Id-"+random.nextInt(1000);
			String licensePlateNumber = "KA01";
			String vehicleIdentificationNumber = "VINDIGITSBIHDFCICICI"+(10+random.nextInt(89))+"X"+(10+random.nextInt(89));
			String engineNumber = (10+random.nextInt(89))+"AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89))+"ASD";

			String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/QuickQuote_4W.json";

			File payloadFromFile = new File(payloadPath);

			JsonPath pathToPayload = JsonPath.given(payloadFromFile);
			
			lib.alterPayload("enquiryId", enquiryId, pathToPayload);
			lib.alterPayload("contract.insuranceProductCode", "20103", pathToPayload);
			lib.alterPayload("contract.subInsuranceProductCode", "PB", pathToPayload);
			lib.alterPayload("previousInsurer.previousPolicyExpiryDate", previousPolicyExpiryDate, pathToPayload);
			lib.alterPayload("previousInsurer.previousPolicyType", "", pathToPayload);
			lib.alterPayload("vehicle.licensePlateNumber", licensePlateNumber, pathToPayload);
			lib.alterPayload("vehicle.isVehicleNew", true, pathToPayload);
			cal.add(Calendar.DATE, -Integer.parseInt(previousPolicyExpiryDate.split("-")[2]));
			lib.alterPayload("vehicle.manufactureDate", s.format(cal.getTime()), pathToPayload);
			lib.alterPayload("vehicle.registrationDate", "0001-01-01", pathToPayload);
			lib.alterPayload("vehicle.vehicleIdentificationNumber", vehicleIdentificationNumber, pathToPayload);
			lib.alterPayload("vehicle.engineNumber", engineNumber, pathToPayload);
			lib.alterPayload("vehicle.seatingCapacity", 5, pathToPayload);
			
			pathToPayload.getMap("previousInsurer").remove("originalPreviousPolicyType");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/ThirdPartyPolicy.json";
			Map<String,Object> tpp = JsonPath.given(new File(payloadPath)).getMap("$"); 
			
			pathToPayload.getMap("previousInsurer").putAll(tpp);
			
			lib.alterPayload("previousInsurer.currentThirdPartyPolicy.currentThirdPartyPolicyNumber", "TPPNUM"+random.nextInt(), pathToPayload);
			lib.alterPayload("previousInsurer.currentThirdPartyPolicy.currentThirdPartyPolicyStartDateTime", previousPolicyExpiryDate+"T00:00:01", pathToPayload);
			lib.alterPayload("previousInsurer.currentThirdPartyPolicy.currentThirdPartyPolicyExpiryDateTime", previousPolicyExpiryDate+"T23:59:59", pathToPayload);
			
			Map<String,Object> payload = pathToPayload.getMap("$");		
			
			Response quickQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/quickQuote?isPremiumRecalculate=false&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");

			lib.captureResponseInFile(quickQuote, "/ReqPayload/MotorInsurance/Recalculate.json");

			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Recalculate.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
////		
//		
//		ArrayList<Object> coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//		for (int i = 0; i < coverages.size(); i++) {
//			
//			Map<String, Object> eachCoverage = (Map<String, Object>)coverages.get(i);
//			if (eachCoverage.get("coverAvailability").equals("AVAILABLE")) {
//				
//				obj.alterPayload("contract.coverages["+i+"].selection", true, pathToPayload);
//				
//				coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//			}
//		}
			payload = pathToPayload.getMap("$");

			
			Response recalculate = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/quickQuote?isPremiumRecalculate=true&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");
			
			lib.captureResponseInFile(recalculate, "/ReqPayload/MotorInsurance/CreateQuote.json");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/CreateQuote.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			payload = pathToPayload.getMap("$");
			
			String payloadPathPerson = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/PersonForCQ.json";
			payloadFromFile = new File(payloadPathPerson);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadPerson = pathToPayload.getMap("$");		
			
			payload.putAll(payloadPerson);
			
			String payloadPathDealer = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Dealer.json";
			payloadFromFile = new File(payloadPathDealer);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			lib.alterPayload("dealer.dealerName", "All Wheels", pathToPayload);
			
			Map<String,Object> payloadDealer = pathToPayload.getMap("$");		
			
			payload.putAll(payloadDealer);
			payload.put("nominee", null);
			payload.put("payment", null);
			
			Response createQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/createQuote?isUserSpecialDiscountOpted=false&isDownloadQuote=true&userId=35327650&sourceType=20201motor&channel=D01&agentcode=1000295");

			lib.captureResponseInFile(createQuote, "/ReqPayload/MotorInsurance/UpdateQuote.json");
			
			String applicationId = createQuote.jsonPath().getString("applicationId");

			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UpdateQuote.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			payload = pathToPayload.getMap("$");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UploadDoc.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadDocument = pathToPayload.getMap("$");
			
			JSONArray document = new JSONArray();
			document.add(payloadDocument);
			
			payload.put("documents", document);
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Payment.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadPayment = pathToPayload.getMap("$");
			
			payload.put("payment", payloadPayment);
			
			lib.capturePayloadInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
			Response updateQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/updateQuote/"+applicationId+"?isUserSpecialDiscountOpted=false&isDownloadQuote=false&agentcode=1000295");
			
			lib.captureResponseInFile(updateQuote, "/ReqPayload/MotorInsurance/IssueContract.json");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/IssueContract.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			payload = pathToPayload.getMap("$");
			payload.put("payment", null);
			payload.put("agentName", "Vineet Bharadwaj");
			payload.put("userId", "35327650");
			payload.put("surveyData", null);
			
			System.out.println("PASS1");
			Response issueContract = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/issueContract/"+applicationId);

			System.out.println(issueContract.asPrettyString());
			try {
				Library.staticMap.put("Tag", "@AgentFloat_1YrOD_N4WC,@NewFourWheelerComprehensive,@AllMotorAPI".replaceAll("@", ""));
				Library.staticMap.put("StatusCode", issueContract.statusCode()+"");
				Library.staticMap.put("Policy Number", issueContract.jsonPath().getString("policyNumber"));
				System.out.println("Policy Number Captured.");
			} catch (Exception e) {
				System.out.println("Unexpected Response");
			}
			System.out.println("PASS2");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void agentFloat_1YrOD_3YrTP() throws IOException {
		
		try {
			com.utilities.Library lib = new com.utilities.Library();
			
			PropertyFileReader read = new PropertyFileReader();
			
			RequestSpecification baseURI = given()
					.auth().preemptive().basic(read.getMotorAPIUsername(), read.getMotorAPIPassword())
					.baseUri("https://preprod-digitplusservice.godigit.com/digitplusservices");
			
			Calendar cal = Calendar.getInstance(); 
			SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
			Random random = new Random();
			
			String previousPolicyExpiryDate = s.format(cal.getTime());
			String enquiryId = "DP-20101-Id-"+random.nextInt(1000);
			String licensePlateNumber = "KA01";
			String vehicleIdentificationNumber = "VINDIGITSBIHDFCICICI"+(10+random.nextInt(89))+"X"+(10+random.nextInt(89));
			String engineNumber = (10+random.nextInt(89))+"AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89))+"ASD";
			
			String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/QuickQuote_4W.json";
			
			File payloadFromFile = new File(payloadPath);
			
			JsonPath pathToPayload = JsonPath.given(payloadFromFile);
			
			lib.alterPayload("enquiryId", enquiryId, pathToPayload);
			lib.alterPayload("contract.insuranceProductCode", "20101", pathToPayload);//20103
			lib.alterPayload("contract.subInsuranceProductCode", "31", pathToPayload);//PB
			lib.alterPayload("previousInsurer.previousPolicyExpiryDate", previousPolicyExpiryDate, pathToPayload);
			lib.alterPayload("previousInsurer.previousPolicyType", "", pathToPayload);
			lib.alterPayload("vehicle.licensePlateNumber", licensePlateNumber, pathToPayload);
			lib.alterPayload("vehicle.isVehicleNew", true, pathToPayload);
			cal.add(Calendar.DATE, -Integer.parseInt(previousPolicyExpiryDate.split("-")[2]));
			lib.alterPayload("vehicle.manufactureDate", s.format(cal.getTime()), pathToPayload);
			lib.alterPayload("vehicle.registrationDate", "0001-01-01", pathToPayload);
			lib.alterPayload("vehicle.vehicleIdentificationNumber", vehicleIdentificationNumber, pathToPayload);
			lib.alterPayload("vehicle.engineNumber", engineNumber, pathToPayload);
			lib.alterPayload("vehicle.seatingCapacity", 5, pathToPayload);
			
			pathToPayload.getMap("previousInsurer").remove("originalPreviousPolicyType");
			
			Map<String,Object> payload = pathToPayload.getMap("$");		
			
			Response quickQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/quickQuote?isPremiumRecalculate=false&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");
			
			lib.captureResponseInFile(quickQuote, "/ReqPayload/MotorInsurance/Recalculate.json");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Recalculate.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
////		
//		
//		ArrayList<Object> coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//		for (int i = 0; i < coverages.size(); i++) {
//			
//			Map<String, Object> eachCoverage = (Map<String, Object>)coverages.get(i);
//			if (eachCoverage.get("coverAvailability").equals("AVAILABLE")) {
//				
//				obj.alterPayload("contract.coverages["+i+"].selection", true, pathToPayload);
//				
//				coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//			}
//		}
			payload = pathToPayload.getMap("$");
			
			
			Response recalculate = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/quickQuote?isPremiumRecalculate=true&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");
			
			lib.captureResponseInFile(recalculate, "/ReqPayload/MotorInsurance/CreateQuote.json");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/CreateQuote.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			payload = pathToPayload.getMap("$");
			
			String payloadPathPerson = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/PersonForCQ.json";
			payloadFromFile = new File(payloadPathPerson);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadPerson = pathToPayload.getMap("$");		
			
			payload.putAll(payloadPerson);
			
			String payloadPathDealer = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Dealer.json";
			payloadFromFile = new File(payloadPathDealer);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			lib.alterPayload("dealer.dealerName", "All Wheels", pathToPayload);
			
			Map<String,Object> payloadDealer = pathToPayload.getMap("$");		
			
			payload.putAll(payloadDealer);
			payload.put("nominee", null);
			payload.put("payment", null);
			
			Response createQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/createQuote?isUserSpecialDiscountOpted=false&isDownloadQuote=true&userId=35327650&sourceType=20201motor&channel=D01&agentcode=1000295");
			
			lib.captureResponseInFile(createQuote, "/ReqPayload/MotorInsurance/UpdateQuote.json");
			
			String applicationId = createQuote.jsonPath().getString("applicationId");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UpdateQuote.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			payload = pathToPayload.getMap("$");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UploadDoc.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadDocument = pathToPayload.getMap("$");
			
			JSONArray document = new JSONArray();
			document.add(payloadDocument);
			
			payload.put("documents", document);
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Payment.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadPayment = pathToPayload.getMap("$");
			
			payload.put("payment", payloadPayment);
			
			lib.capturePayloadInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
			Response updateQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/updateQuote/"+applicationId+"?isUserSpecialDiscountOpted=false&isDownloadQuote=false&agentcode=1000295");
			
			lib.captureResponseInFile(updateQuote, "/ReqPayload/MotorInsurance/IssueContract.json");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/IssueContract.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			payload = pathToPayload.getMap("$");
			payload.put("payment", null);
			payload.put("agentName", "Vineet Bharadwaj");
			payload.put("userId", "35327650");
			payload.put("surveyData", null);
			
			System.out.println("PASS1");
			Response issueContract = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/issueContract/"+applicationId);
			
			System.out.println(issueContract.asPrettyString());
			try {
				Library.staticMap.put("Tag", "@AgentFloat_1YrOD_3YrTP_N4WC,@NewFourWheelerComprehensive,@AllMotorAPI".replaceAll("@", ""));
				Library.staticMap.put("StatusCode", issueContract.statusCode()+"");
				Library.staticMap.put("Policy Number", issueContract.jsonPath().getString("policyNumber"));
				System.out.println("Policy Number Captured.");
			} catch (Exception e) {
				System.out.println("Unexpected Response");
			}
			System.out.println("PASS2");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void cheque() throws IOException {
		
		try {
			com.utilities.Library lib = new com.utilities.Library();

			PropertyFileReader read = new PropertyFileReader();
			
			RequestSpecification baseURI = given()
					.auth().preemptive().basic(read.getMotorAPIUsername(), read.getMotorAPIPassword())
					.baseUri("https://preprod-digitplusservice.godigit.com/digitplusservices");

			Calendar cal = Calendar.getInstance(); 
			SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
			Random random = new Random();

			String previousPolicyExpiryDate = s.format(cal.getTime());
			String enquiryId = "DP-20101-Id-"+random.nextInt(1000);
			String licensePlateNumber = "KA01";
			String vehicleIdentificationNumber = "VINDIGITSBIHDFCICICI"+(10+random.nextInt(89))+"X"+(10+random.nextInt(89));
			String engineNumber = (10+random.nextInt(89))+"AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89))+"ASD";

			String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/QuickQuote_4W.json";

			File payloadFromFile = new File(payloadPath);

			JsonPath pathToPayload = JsonPath.given(payloadFromFile);
			
			lib.alterPayload("enquiryId", enquiryId, pathToPayload);
			lib.alterPayload("contract.insuranceProductCode", "20101", pathToPayload);
			lib.alterPayload("contract.subInsuranceProductCode", "31", pathToPayload);
			lib.alterPayload("previousInsurer.previousPolicyExpiryDate", previousPolicyExpiryDate, pathToPayload);
			lib.alterPayload("previousInsurer.previousPolicyType", "", pathToPayload);
			lib.alterPayload("vehicle.licensePlateNumber", licensePlateNumber, pathToPayload);
			lib.alterPayload("vehicle.isVehicleNew", true, pathToPayload);
			cal.add(Calendar.DATE, -Integer.parseInt(previousPolicyExpiryDate.split("-")[2]));
			lib.alterPayload("vehicle.manufactureDate", s.format(cal.getTime()), pathToPayload);
			lib.alterPayload("vehicle.registrationDate", "0001-01-01", pathToPayload);
			lib.alterPayload("vehicle.vehicleIdentificationNumber", vehicleIdentificationNumber, pathToPayload);
			lib.alterPayload("vehicle.engineNumber", engineNumber, pathToPayload);
			lib.alterPayload("vehicle.seatingCapacity", 5, pathToPayload);
			
			pathToPayload.getMap("previousInsurer").remove("originalPreviousPolicyType");
			
			Map<String,Object> payload = pathToPayload.getMap("$");		
			
			Response quickQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/quickQuote?isPremiumRecalculate=false&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");

			lib.captureResponseInFile(quickQuote, "/ReqPayload/MotorInsurance/Recalculate.json");

			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Recalculate.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
////		
//		
//		ArrayList<Object> coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//		for (int i = 0; i < coverages.size(); i++) {
//			
//			Map<String, Object> eachCoverage = (Map<String, Object>)coverages.get(i);
//			if (eachCoverage.get("coverAvailability").equals("AVAILABLE")) {
//				
//				obj.alterPayload("contract.coverages["+i+"].selection", true, pathToPayload);
//				
//				coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//			}
//		}
			payload = pathToPayload.getMap("$");

			
			Response recalculate = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/quickQuote?isPremiumRecalculate=true&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");
			
			lib.captureResponseInFile(recalculate, "/ReqPayload/MotorInsurance/CreateQuote.json");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/CreateQuote.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			payload = pathToPayload.getMap("$");
			
			String payloadPathPerson = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/PersonForCQ.json";
			payloadFromFile = new File(payloadPathPerson);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadPerson = pathToPayload.getMap("$");		
			
			payload.putAll(payloadPerson);
			
			String payloadPathDealer = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Dealer.json";
			payloadFromFile = new File(payloadPathDealer);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			lib.alterPayload("dealer.dealerName", "All Wheels", pathToPayload);
			
			Map<String,Object> payloadDealer = pathToPayload.getMap("$");		
			
			payload.putAll(payloadDealer);
			payload.put("nominee", null);
			payload.put("payment", null);
			
			Response createQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/createQuote?isUserSpecialDiscountOpted=false&isDownloadQuote=true&userId=35327650&sourceType=20201motor&channel=D01&agentcode=1000295");

			lib.captureResponseInFile(createQuote, "/ReqPayload/MotorInsurance/UpdateQuote.json");
			
			String applicationId = createQuote.jsonPath().getString("applicationId");

			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UpdateQuote.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			payload = pathToPayload.getMap("$");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UploadDoc.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadDocument = pathToPayload.getMap("$");
			
			JSONArray document = new JSONArray();
			document.add(payloadDocument);
			
			payload.put("documents", document);
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Payment.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadPayment = pathToPayload.getMap("$");
			
			payloadPayment.put("paymentType", "CHEQUE");
			payloadPayment.put("paymentDate", s.format(cal.getTime()));
			payloadPayment.put("instrumentNumber", "123234");
			payloadPayment.put("ifscCode", "SBIN0001024");
			payloadPayment.put("agentConsent", "N");
			payloadPayment.put("paymentSource", "CUSTOMER_CHEQUE");
			
			payload.put("payment", payloadPayment);
			
			lib.capturePayloadInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
			Response updateQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/updateQuote/"+applicationId+"?isUserSpecialDiscountOpted=false&isDownloadQuote=false&agentcode=1000295");
			
			lib.captureResponseInFile(updateQuote, "/ReqPayload/MotorInsurance/IssueContract.json");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/IssueContract.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			payload = pathToPayload.getMap("$");
			payload.put("payment", null);
			payload.put("agentName", "Vineet Bharadwaj");
			payload.put("userId", "35327650");
			payload.put("surveyData", null);
			
			System.out.println("PASS1");
			Response issueContract = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/issueContract/"+applicationId);

			System.out.println(issueContract.asPrettyString());
			try {
				Library.staticMap.put("Tag", "@Cheque_N4WC,@NewFourWheelerComprehensive,@AllMotorAPI".replaceAll("@", ""));
				Library.staticMap.put("StatusCode", issueContract.statusCode()+"");
				Library.staticMap.put("Policy Number", issueContract.jsonPath().getString("policyNumber"));
				System.out.println("Policy Number Captured.");
			} catch (Exception e) {
				System.out.println("Unexpected Response");
			}
			System.out.println("PASS2");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	@SuppressWarnings("unchecked")
	public void online() throws IOException {
		
		try {
			com.utilities.Library lib = new com.utilities.Library();

			PropertyFileReader read = new PropertyFileReader();
			
			RequestSpecification baseURI = given()
					.auth().preemptive().basic(read.getMotorAPIUsername(), read.getMotorAPIPassword())
					.baseUri("https://preprod-digitplusservice.godigit.com/digitplusservices");

			Calendar cal = Calendar.getInstance(); 
			SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
			Random random = new Random();

			String previousPolicyExpiryDate = s.format(cal.getTime());
			String enquiryId = "DP-20101-Id-"+random.nextInt(1000);
			String licensePlateNumber = "KA01";
			String vehicleIdentificationNumber = "VINDIGITSBIHDFCICICI"+(10+random.nextInt(89))+"X"+(10+random.nextInt(89));
			String engineNumber = (10+random.nextInt(89))+"AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89))+"ASD";

			String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/QuickQuote_4W.json";

			File payloadFromFile = new File(payloadPath);

			JsonPath pathToPayload = JsonPath.given(payloadFromFile);
			
			lib.alterPayload("enquiryId", enquiryId, pathToPayload);
			lib.alterPayload("contract.insuranceProductCode", "20101", pathToPayload);
			lib.alterPayload("contract.subInsuranceProductCode", "31", pathToPayload);
			lib.alterPayload("previousInsurer.previousPolicyExpiryDate", previousPolicyExpiryDate, pathToPayload);
			lib.alterPayload("previousInsurer.previousPolicyType", "", pathToPayload);
			lib.alterPayload("vehicle.licensePlateNumber", licensePlateNumber, pathToPayload);
			lib.alterPayload("vehicle.isVehicleNew", true, pathToPayload);
			cal.add(Calendar.DATE, -Integer.parseInt(previousPolicyExpiryDate.split("-")[2]));
			lib.alterPayload("vehicle.manufactureDate", s.format(cal.getTime()), pathToPayload);
			lib.alterPayload("vehicle.registrationDate", "0001-01-01", pathToPayload);
			lib.alterPayload("vehicle.vehicleIdentificationNumber", vehicleIdentificationNumber, pathToPayload);
			lib.alterPayload("vehicle.engineNumber", engineNumber, pathToPayload);
			lib.alterPayload("vehicle.seatingCapacity", 5, pathToPayload);
			
			pathToPayload.getMap("previousInsurer").remove("originalPreviousPolicyType");
			
			Map<String,Object> payload = pathToPayload.getMap("$");		
			
			Response quickQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/quickQuote?isPremiumRecalculate=false&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");

			lib.captureResponseInFile(quickQuote, "/ReqPayload/MotorInsurance/Recalculate.json");

			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Recalculate.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
////		
//		
//		ArrayList<Object> coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//		for (int i = 0; i < coverages.size(); i++) {
//			
//			Map<String, Object> eachCoverage = (Map<String, Object>)coverages.get(i);
//			if (eachCoverage.get("coverAvailability").equals("AVAILABLE")) {
//				
//				obj.alterPayload("contract.coverages["+i+"].selection", true, pathToPayload);
//				
//				coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//			}
//		}
			payload = pathToPayload.getMap("$");

			
			Response recalculate = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/quickQuote?isPremiumRecalculate=true&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");
			
			lib.captureResponseInFile(recalculate, "/ReqPayload/MotorInsurance/CreateQuote.json");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/CreateQuote.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			payload = pathToPayload.getMap("$");
			
			String payloadPathPerson = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/PersonForCQ.json";
			payloadFromFile = new File(payloadPathPerson);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadPerson = pathToPayload.getMap("$");		
			
			payload.putAll(payloadPerson);
			
			String payloadPathDealer = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Dealer.json";
			payloadFromFile = new File(payloadPathDealer);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			lib.alterPayload("dealer.dealerName", "All Wheels", pathToPayload);
			
			Map<String,Object> payloadDealer = pathToPayload.getMap("$");		
			
			payload.putAll(payloadDealer);
			payload.put("nominee", null);
			payload.put("payment", null);
			
			Response createQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/createQuote?isUserSpecialDiscountOpted=false&isDownloadQuote=true&userId=35327650&sourceType=20201motor&channel=D01&agentcode=1000295");

			lib.captureResponseInFile(createQuote, "/ReqPayload/MotorInsurance/UpdateQuote.json");
			
			String applicationId = createQuote.jsonPath().getString("applicationId");

			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UpdateQuote.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			payload = pathToPayload.getMap("$");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UploadDoc.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadDocument = pathToPayload.getMap("$");
			
			JSONArray document = new JSONArray();
			document.add(payloadDocument);
			
			payload.put("documents", document);
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Payment.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadPayment = pathToPayload.getMap("$");
			
			payloadPayment.put("paymentType", "ONLINE");
			
			payload.put("payment", payloadPayment);
			
			lib.capturePayloadInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
			Response updateQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/updateQuote/"+applicationId+"?isUserSpecialDiscountOpted=false&isDownloadQuote=false&agentcode=1000295");
			
			lib.captureResponseInFile(updateQuote, "/ReqPayload/MotorInsurance/IssueContract.json");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/IssueContract.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			payload = pathToPayload.getMap("$");
			payload.put("payment", null);
			payload.put("agentName", "Vineet Bharadwaj");
			payload.put("userId", "35327650");
			payload.put("surveyData", null);
			
			System.out.println("PASS1");
			Response issueContract = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/issueContract/"+applicationId);

			System.out.println(issueContract.asPrettyString());
			try {
				Library.staticMap.put("Tag", "@Online_N4WC,@NewFourWheelerComprehensive,@AllMotorAPI".replaceAll("@", ""));
				Library.staticMap.put("StatusCode", issueContract.statusCode()+"");
				Library.staticMap.put("Policy Number", issueContract.jsonPath().getString("policyNumber"));
				System.out.println("Policy Number Captured.");
			} catch (Exception e) {
				System.out.println("Unexpected Response");
			}
			System.out.println("PASS2");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	@SuppressWarnings("unchecked")
	public void generatePaymentLink() {
		
		try {
			com.utilities.Library lib = new com.utilities.Library();

			PropertyFileReader read = new PropertyFileReader();
			
			RequestSpecification baseURI = given()
					.auth().preemptive().basic(read.getMotorAPIUsername(), read.getMotorAPIPassword())
					.baseUri("https://preprod-digitplusservice.godigit.com/digitplusservices");

			Calendar cal = Calendar.getInstance(); 
			SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
			Random random = new Random();

			String previousPolicyExpiryDate = s.format(cal.getTime());
			String enquiryId = "DP-20101-Id-"+random.nextInt(1000);
			String licensePlateNumber = "KA01";
			String vehicleIdentificationNumber = "VINDIGITSBIHDFCICICI"+(10+random.nextInt(89))+"X"+(10+random.nextInt(89));
			String engineNumber = (10+random.nextInt(89))+"AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89))+"ASD";

			String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/QuickQuote_4W.json";

			File payloadFromFile = new File(payloadPath);

			JsonPath pathToPayload = JsonPath.given(payloadFromFile);
			
			lib.alterPayload("enquiryId", enquiryId, pathToPayload);
			lib.alterPayload("contract.insuranceProductCode", "20101", pathToPayload);
			lib.alterPayload("contract.subInsuranceProductCode", "31", pathToPayload);
			lib.alterPayload("previousInsurer.previousPolicyExpiryDate", previousPolicyExpiryDate, pathToPayload);
			lib.alterPayload("previousInsurer.previousPolicyType", "", pathToPayload);
			lib.alterPayload("vehicle.licensePlateNumber", licensePlateNumber, pathToPayload);
			lib.alterPayload("vehicle.isVehicleNew", true, pathToPayload);
			cal.add(Calendar.DATE, -Integer.parseInt(previousPolicyExpiryDate.split("-")[2]));
			lib.alterPayload("vehicle.manufactureDate", s.format(cal.getTime()), pathToPayload);
			lib.alterPayload("vehicle.registrationDate", "0001-01-01", pathToPayload);
			lib.alterPayload("vehicle.vehicleIdentificationNumber", vehicleIdentificationNumber, pathToPayload);
			lib.alterPayload("vehicle.engineNumber", engineNumber, pathToPayload);
			lib.alterPayload("vehicle.seatingCapacity", 5, pathToPayload);
			
			pathToPayload.getMap("previousInsurer").remove("originalPreviousPolicyType");
			
			Map<String,Object> payload = pathToPayload.getMap("$");		
			
			Response quickQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/quickQuote?isPremiumRecalculate=false&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");

			lib.captureResponseInFile(quickQuote, "/ReqPayload/MotorInsurance/Recalculate.json");

			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Recalculate.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
////		
//		
//		ArrayList<Object> coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//		for (int i = 0; i < coverages.size(); i++) {
//			
//			Map<String, Object> eachCoverage = (Map<String, Object>)coverages.get(i);
//			if (eachCoverage.get("coverAvailability").equals("AVAILABLE")) {
//				
//				obj.alterPayload("contract.coverages["+i+"].selection", true, pathToPayload);
//				
//				coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//			}
//		}
			payload = pathToPayload.getMap("$");

			
			Response recalculate = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/quickQuote?isPremiumRecalculate=true&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");
			
			lib.captureResponseInFile(recalculate, "/ReqPayload/MotorInsurance/CreateQuote.json");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/CreateQuote.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			payload = pathToPayload.getMap("$");
			
			String payloadPathPerson = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/PersonForCQ.json";
			payloadFromFile = new File(payloadPathPerson);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadPerson = pathToPayload.getMap("$");		
			
			payload.putAll(payloadPerson);
			
			String payloadPathDealer = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Dealer.json";
			payloadFromFile = new File(payloadPathDealer);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			lib.alterPayload("dealer.dealerName", "All Wheels", pathToPayload);
			
			Map<String,Object> payloadDealer = pathToPayload.getMap("$");		
			
			payload.putAll(payloadDealer);
			payload.put("nominee", null);
			payload.put("payment", null);
			
			Response createQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/createQuote?isUserSpecialDiscountOpted=false&isDownloadQuote=true&userId=35327650&sourceType=20201motor&channel=D01&agentcode=1000295");

			lib.captureResponseInFile(createQuote, "/ReqPayload/MotorInsurance/UpdateQuote.json");
			
			String applicationId = createQuote.jsonPath().getString("applicationId");
			String policyNumber = createQuote.jsonPath().getString("policyNumber");

			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UpdateQuote.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			payload = pathToPayload.getMap("$");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UploadDoc.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadDocument = pathToPayload.getMap("$");
			
			JSONArray document = new JSONArray();
			document.add(payloadDocument);
			
			payload.put("documents", document);
			payload.put("payment", null);
			
			Response updateQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/updateQuote/"+applicationId+"?isUserSpecialDiscountOpted=false&isDownloadQuote=false&agentcode=1000295");
			
			lib.captureResponseInFile(updateQuote, "/ReqPayload/MotorInsurance/IssueContract.json");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/IssueContract.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			payload = pathToPayload.getMap("$");
			payload.put("agentName", "Vineet Bharadwaj");
			payload.put("userId", "35327650");
			payload.put("surveyData", null);
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Payment.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			Map<String,Object> payloadPayment = pathToPayload.getMap("$");
			
			payloadPayment.put("paymentType", "GENERATE_PAYMENT_LINK");
			payloadPayment.put("successReturnUrl", "https://preprod-plusui.godigit.com/DigitPlus/#/motor-new?applicationId="+applicationId+"&policyNumber="+policyNumber);
			payloadPayment.put("cancelReturnUrl", "https://preprod-plusui.godigit.com/DigitPlus/#/motor-new");
			payloadPayment.put("expiryHours", 7);
			
			payload.put("payment", payloadPayment);
			
			System.out.println("PASS1");
			Response issueContract = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/issueContract/"+applicationId);

			System.out.println(issueContract.asPrettyString());
			try {
				Library.staticMap.put("Tag", "@GeneratePaymentLink_N4WC,@NewFourWheelerComprehensive,@AllMotorAPI".replaceAll("@", ""));
				Library.staticMap.put("StatusCode", issueContract.statusCode()+"");
				issueContract.then().assertThat().body("link", containsString("DigitPaymentGateway"));
				Library.staticMap.put("Policy Number", createQuote.jsonPath().getString("policyNumber")+" [PI Link Generated Successfully!!]");
				System.out.println("Policy Number Captured.");
			} catch (Exception e) {
				System.out.println("Unexpected Response");
			}
			System.out.println("PASS2");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
