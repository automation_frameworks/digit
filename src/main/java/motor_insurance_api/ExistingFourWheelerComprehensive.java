package motor_insurance_api;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.Random;

import org.json.simple.JSONArray;

import com.utilities.Library;
import com.utilities.PropertyFileReader;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ExistingFourWheelerComprehensive {

	@SuppressWarnings("unchecked")
	public void agentFloat_1yr_1TP() throws IOException {
		
		try {
			com.utilities.Library lib = new com.utilities.Library();

			PropertyFileReader read = new PropertyFileReader();
			
			RequestSpecification baseURI = given()
					.auth().preemptive().basic(read.getMotorAPIUsername(), read.getMotorAPIPassword())
					.baseUri("https://preprod-digitplusservice.godigit.com/digitplusservices");

			Calendar cal = Calendar.getInstance(); 
			SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
			Random random = new Random();

			String previousPolicyExpiryDate = s.format(cal.getTime());
			String enquiryId = "DP-20201-Id-"+random.nextInt(1000);
			String licensePlateNumber = "KA01QK"+(1000+random.nextInt(8999));
			String vehicleIdentificationNumber = "ASDASUD9AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89));
			String engineNumber = (10+random.nextInt(89))+"AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89))+"ASD";

			String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/QuickQuote_4W.json";

			File payloadFromFile = new File(payloadPath);

			JsonPath pathToPayload = JsonPath.given(payloadFromFile);
			
			lib.alterPayload("enquiryId", enquiryId, pathToPayload);
			lib.alterPayload("contract.insuranceProductCode", "20301", pathToPayload);
			lib.alterPayload("contract.subInsuranceProductCode", "PB", pathToPayload);
			lib.alterPayload("previousInsurer.previousPolicyExpiryDate", previousPolicyExpiryDate, pathToPayload);
			lib.alterPayload("previousInsurer.previousInsurerCode", "139", pathToPayload);
			lib.alterPayload("previousInsurer.previousPolicyNumber", "POL"+random.nextInt(), pathToPayload);
			lib.alterPayload("previousInsurer.isClaimInLastYear", false, pathToPayload);
			lib.alterPayload("previousInsurer.previousPolicyType", "Comprehensive policy with Zero Dep", pathToPayload);
			lib.alterPayload("vehicle.licensePlateNumber", licensePlateNumber, pathToPayload);
			
			lib.alterPayload("vehicle.seatingCapacity", 7, pathToPayload);

			Map<String,Object> payload = pathToPayload.getMap("$");		
			
			Response quickQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/quickQuote?isPremiumRecalculate=false&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");

			lib.captureResponseInFile(quickQuote, "/ReqPayload/MotorInsurance/Recalculate.json");

			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Recalculate.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			
//		ArrayList<Object> coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//		for (int i = 0; i < coverages.size(); i++) {
//			
//			Map<String, Object> eachCoverage = (Map<String, Object>)coverages.get(i);
//			if (eachCoverage.get("coverAvailability").equals("AVAILABLE")) {
//				
//				obj.alterPayload("contract.coverages["+i+"].selection", true, pathToPayload);
//				
//				coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//			}
//		}
			payload = pathToPayload.getMap("$");

			
			Response recalculate = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/quickQuote?isPremiumRecalculate=true&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");
			
			lib.captureResponseInFile(recalculate, "/ReqPayload/MotorInsurance/CreateQuote.json");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/CreateQuote.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			lib.alterPayload("vehicle.vehicleIdentificationNumber", vehicleIdentificationNumber, pathToPayload);
			lib.alterPayload("vehicle.engineNumber", engineNumber, pathToPayload);
			
//		lib.alterPayload("preInspection.isPreInspectionOpted", true, pathToPayload);
//		lib.alterPayload("preInspection.isPreInspectionRequired", true, pathToPayload);
//		lib.alterPayload("preInspection.isPreInspectionEligible", true, pathToPayload);
			
			payload = pathToPayload.getMap("$");
//		obj.captureResponseInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
			
			String payloadPathPerson = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/PersonForCQ.json";
			payloadFromFile = new File(payloadPathPerson);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			Map<String,Object> payloadPerson = pathToPayload.getMap("$");		
			
			payload.putAll(payloadPerson);
			

			Response createQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/createQuote?isUserSpecialDiscountOpted=false&isDownloadQuote=true&agentcode=1000295");

			lib.captureResponseInFile(createQuote, "/ReqPayload/MotorInsurance/UpdateQuote.json");
			
			String applicationId = createQuote.jsonPath().getString("applicationId");

			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UpdateQuote.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			payload = pathToPayload.getMap("$");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UploadDoc.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadDocument = pathToPayload.getMap("$");
			
			JSONArray jar = new JSONArray();
			jar.add(payloadDocument);
			
			payload.put("documents", jar);
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Payment.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadPayment = pathToPayload.getMap("$");
			
			payload.put("payment", payloadPayment);
			
			Response updateQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/updateQuote/"+applicationId+"?isUserSpecialDiscountOpted=false&isDownloadQuote=false&agentcode=1000295");
			
			lib.captureResponseInFile(updateQuote, "/ReqPayload/MotorInsurance/IssueContract.json");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/IssueContract.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
//		lib.alterPayload("motorQuestions.selfInspection", true, pathToPayload);
//		lib.alterPayload("motorBreakIn.isBreakin", true, pathToPayload);
//		lib.alterPayload("motorBreakIn.isDocumentUploaded", true, pathToPayload);

			payload = pathToPayload.getMap("$");
			
			
			payload.put("agentName", "Vineet Bharadwaj");
			payload.put("userId", "35327650");
			payload.put("payment", null);
			payload.put("surveyData", null);
			
			lib.capturePayloadInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
			
			System.out.println("PASS1");
			Response issueContract = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/issueContract/"+applicationId);//+"?&userId=35327650&sourceType=20201motor&channel=D01&policyNumber="+policyNumber+"&agentcode=1000295");

			System.out.println(issueContract.asPrettyString());
			try {

				Library.staticMap.put("Tag", "@AgentFloat_1YrOD_1YrTP_E4WC,@ExistingFourWheelerComprehensive,@AllMotorAPI".replaceAll("@", ""));
				Library.staticMap.put("StatusCode", issueContract.statusCode()+"");
				Library.staticMap.put("Policy Number", issueContract.jsonPath().getString("policyNumber"));
				System.out.println("Policy Number Captured.");
			} catch (Exception e) {
				System.out.println("Unexpected Response");
			}
			System.out.println("PASS2");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void agentFloat_1yrODWithTPP() throws IOException {
		
		try {
			com.utilities.Library lib = new com.utilities.Library();
			
			PropertyFileReader read = new PropertyFileReader();
			
			RequestSpecification baseURI = given()
					.auth().preemptive().basic(read.getMotorAPIUsername(), read.getMotorAPIPassword())
					.baseUri("https://preprod-digitplusservice.godigit.com/digitplusservices");
			
			Calendar cal = Calendar.getInstance(); 
			SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
			Random random = new Random();
			
			String previousPolicyExpiryDate = s.format(cal.getTime());
			String enquiryId = "DP-20201-Id-"+random.nextInt(1000);
			String licensePlateNumber = "KA01QK"+(1000+random.nextInt(8999));
			String vehicleIdentificationNumber = "ASDASUD9AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89));
			String engineNumber = (10+random.nextInt(89))+"AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89))+"ASD";
			
			String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/QuickQuote_4W.json";
			
			File payloadFromFile = new File(payloadPath);
			
			JsonPath pathToPayload = JsonPath.given(payloadFromFile);
			
			lib.alterPayload("enquiryId", enquiryId, pathToPayload);
			lib.alterPayload("contract.insuranceProductCode", "20103", pathToPayload);
			lib.alterPayload("contract.subInsuranceProductCode", "PB", pathToPayload);
			lib.alterPayload("previousInsurer.previousPolicyExpiryDate", previousPolicyExpiryDate, pathToPayload);
			lib.alterPayload("previousInsurer.previousInsurerCode", "139", pathToPayload);
			lib.alterPayload("previousInsurer.previousPolicyType", "Comprehensive policy without Zero Dep", pathToPayload);
			lib.alterPayload("previousInsurer.originalPreviousPolicyType", "1OD_1TP", pathToPayload);
			lib.alterPayload("previousInsurer.previousPolicyNumber", "POL"+random.nextInt(), pathToPayload);
			lib.alterPayload("previousInsurer.isClaimInLastYear", false, pathToPayload);
			lib.alterPayload("previousInsurer.originalPreviousPolicyType", "0OD_1TP", pathToPayload);
			lib.alterPayload("previousInsurer.previousPolicyType", "Comprehensive policy with Zero Dep", pathToPayload);
			lib.alterPayload("vehicle.licensePlateNumber", licensePlateNumber, pathToPayload);
			
			lib.alterPayload("vehicle.seatingCapacity", 7, pathToPayload);
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/ThirdPartyPolicy.json";
			Map<String,Object> tpp = JsonPath.given(new File(payloadPath)).getMap("$"); 
			
			pathToPayload.getMap("previousInsurer").putAll(tpp);
			
			
			lib.alterPayload("previousInsurer.currentThirdPartyPolicy.currentThirdPartyPolicyNumber", "TPPOLNUM"+random.nextInt(), pathToPayload);
			cal.add(Calendar.DATE, -2);
			lib.alterPayload("previousInsurer.currentThirdPartyPolicy.currentThirdPartyPolicyStartDateTime", s.format(cal.getTime())+"T00:00:01", pathToPayload);
			cal.add(Calendar.DATE, 5);
			lib.alterPayload("previousInsurer.currentThirdPartyPolicy.currentThirdPartyPolicyExpiryDateTime", s.format(cal.getTime())+"T23:59:59", pathToPayload);
			
			Map<String,Object> payload = pathToPayload.getMap("$");		
			
			Response quickQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/quickQuote?isPremiumRecalculate=false&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");
			
			lib.captureResponseInFile(quickQuote, "/ReqPayload/MotorInsurance/Recalculate.json");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Recalculate.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			
//		ArrayList<Object> coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//		for (int i = 0; i < coverages.size(); i++) {
//			
//			Map<String, Object> eachCoverage = (Map<String, Object>)coverages.get(i);
//			if (eachCoverage.get("coverAvailability").equals("AVAILABLE")) {
//				
//				obj.alterPayload("contract.coverages["+i+"].selection", true, pathToPayload);
//				
//				coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//			}
//		}
			payload = pathToPayload.getMap("$");
			
			
			Response recalculate = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/quickQuote?isPremiumRecalculate=true&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");
			
			lib.captureResponseInFile(recalculate, "/ReqPayload/MotorInsurance/CreateQuote.json");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/CreateQuote.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			lib.alterPayload("vehicle.vehicleIdentificationNumber", vehicleIdentificationNumber, pathToPayload);
			lib.alterPayload("vehicle.engineNumber", engineNumber, pathToPayload);
			
//		lib.alterPayload("preInspection.isPreInspectionOpted", true, pathToPayload);
//		lib.alterPayload("preInspection.isPreInspectionRequired", true, pathToPayload);
//		lib.alterPayload("preInspection.isPreInspectionEligible", true, pathToPayload);
			
			payload = pathToPayload.getMap("$");
//		obj.captureResponseInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
			
			String payloadPathPerson = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/PersonForCQ.json";
			payloadFromFile = new File(payloadPathPerson);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			Map<String,Object> payloadPerson = pathToPayload.getMap("$");		
			
			payload.putAll(payloadPerson);
			
			
			Response createQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/createQuote?isUserSpecialDiscountOpted=false&isDownloadQuote=true&agentcode=1000295");
			
			lib.captureResponseInFile(createQuote, "/ReqPayload/MotorInsurance/UpdateQuote.json");
			
			String applicationId = createQuote.jsonPath().getString("applicationId");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UpdateQuote.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
			payload = pathToPayload.getMap("$");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UploadDoc.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadDocument = pathToPayload.getMap("$");
			
			JSONArray jar = new JSONArray();
			jar.add(payloadDocument);
			
			payload.put("documents", jar);
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Payment.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			Map<String,Object> payloadPayment = pathToPayload.getMap("$");
			
			payload.put("payment", payloadPayment);
			
			Response updateQuote = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/updateQuote/"+applicationId+"?isUserSpecialDiscountOpted=false&isDownloadQuote=false&agentcode=1000295");
			
			lib.captureResponseInFile(updateQuote, "/ReqPayload/MotorInsurance/IssueContract.json");
			
			payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/IssueContract.json";
			payloadFromFile = new File(payloadPath);
			pathToPayload = JsonPath.given(payloadFromFile);
			
//		lib.alterPayload("motorQuestions.selfInspection", true, pathToPayload);
//		lib.alterPayload("motorBreakIn.isBreakin", true, pathToPayload);
//		lib.alterPayload("motorBreakIn.isDocumentUploaded", true, pathToPayload);
			
			payload = pathToPayload.getMap("$");
			
			
			payload.put("agentName", "Vineet Bharadwaj");
			payload.put("userId", "35327650");
			payload.put("payment", null);
			payload.put("surveyData", null);
			
			lib.capturePayloadInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
			
			System.out.println("PASS1");
			Response issueContract = baseURI
					.body(payload)
					.contentType("Application/json").accept("application/json")
					.post("/issueContract/"+applicationId);//+"?&userId=35327650&sourceType=20201motor&channel=D01&policyNumber="+policyNumber+"&agentcode=1000295");
			
			System.out.println(issueContract.asPrettyString());
			try {
				Library.staticMap.put("Tag", "@AgentFloat_1YrOD_with_prevailing_TPP_E4WC,@ExistingFourWheelerComprehensive,@AllMotorAPI".replaceAll("@", ""));
				Library.staticMap.put("StatusCode", issueContract.statusCode()+"");
				Library.staticMap.put("Policy Number", issueContract.jsonPath().getString("policyNumber"));
				System.out.println("Policy Number Captured.");
			} catch (Exception e) {
				System.out.println("Unexpected Response");
			}			
			System.out.println("PASS2");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	public void agentFloat_1yrODWithTPPExpired() throws IOException {
		
			try {
				com.utilities.Library lib = new com.utilities.Library();
				
				PropertyFileReader read = new PropertyFileReader();
				
				RequestSpecification baseURI = given()
						.auth().preemptive().basic(read.getMotorAPIUsername(), read.getMotorAPIPassword())
						.baseUri("https://preprod-digitplusservice.godigit.com/digitplusservices");
				
				Calendar cal = Calendar.getInstance(); 
				SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
				Random random = new Random();
				
				String previousPolicyExpiryDate = s.format(cal.getTime());
				String enquiryId = "DP-20201-Id-"+random.nextInt(1000);
				String licensePlateNumber = "KA01QK"+(1000+random.nextInt(8999));
				String vehicleIdentificationNumber = "ASDASUD9AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89));
				String engineNumber = (10+random.nextInt(89))+"AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89))+"ASD";
				
				String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/QuickQuote_4W.json";
				
				File payloadFromFile = new File(payloadPath);
				
				JsonPath pathToPayload = JsonPath.given(payloadFromFile);
				
				lib.alterPayload("enquiryId", enquiryId, pathToPayload);
				lib.alterPayload("contract.insuranceProductCode", "20103", pathToPayload);
				lib.alterPayload("contract.subInsuranceProductCode", "PB", pathToPayload);
				lib.alterPayload("previousInsurer.previousPolicyExpiryDate", previousPolicyExpiryDate, pathToPayload);
				lib.alterPayload("previousInsurer.previousInsurerCode", "139", pathToPayload);
				lib.alterPayload("previousInsurer.previousPolicyType", "Comprehensive policy without Zero Dep", pathToPayload);
				lib.alterPayload("previousInsurer.originalPreviousPolicyType", "1OD_1TP", pathToPayload);
				lib.alterPayload("previousInsurer.previousPolicyNumber", "POL"+random.nextInt(), pathToPayload);
				lib.alterPayload("previousInsurer.isClaimInLastYear", false, pathToPayload);
				lib.alterPayload("previousInsurer.originalPreviousPolicyType", "0OD_1TP", pathToPayload);
				lib.alterPayload("previousInsurer.previousPolicyType", "Comprehensive policy with Zero Dep", pathToPayload);
				lib.alterPayload("vehicle.licensePlateNumber", licensePlateNumber, pathToPayload);
				
				lib.alterPayload("vehicle.seatingCapacity", 7, pathToPayload);
				
				payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/ThirdPartyPolicy.json";
				Map<String,Object> tpp = JsonPath.given(new File(payloadPath)).getMap("$"); 
				
				pathToPayload.getMap("previousInsurer").putAll(tpp);
				
				
				lib.alterPayload("previousInsurer.currentThirdPartyPolicy.currentThirdPartyPolicyNumber", "TPPOLNUM"+random.nextInt(), pathToPayload);
				lib.alterPayload("previousInsurer.currentThirdPartyPolicy.currentThirdPartyPolicyStartDateTime", s.format(cal.getTime())+"T00:00:01", pathToPayload);
				lib.alterPayload("previousInsurer.currentThirdPartyPolicy.currentThirdPartyPolicyExpiryDateTime", s.format(cal.getTime())+"T23:59:59", pathToPayload);
				
				Map<String,Object> payload = pathToPayload.getMap("$");		
				
				Response quickQuote = baseURI
						.body(payload)
						.contentType("Application/json").accept("application/json")
						.post("/quickQuote?isPremiumRecalculate=false&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");
				
				lib.captureResponseInFile(quickQuote, "/ReqPayload/MotorInsurance/Recalculate.json");
				
				payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Recalculate.json";
				payloadFromFile = new File(payloadPath);
				pathToPayload = JsonPath.given(payloadFromFile);
				
				
//		ArrayList<Object> coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//		for (int i = 0; i < coverages.size(); i++) {
//			
//			Map<String, Object> eachCoverage = (Map<String, Object>)coverages.get(i);
//			if (eachCoverage.get("coverAvailability").equals("AVAILABLE")) {
//				
//				obj.alterPayload("contract.coverages["+i+"].selection", true, pathToPayload);
//				
//				coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//			}
//		}
				payload = pathToPayload.getMap("$");
				
				
				Response recalculate = baseURI
						.body(payload)
						.contentType("Application/json").accept("application/json")
						.post("/quickQuote?isPremiumRecalculate=true&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");
				
				lib.captureResponseInFile(recalculate, "/ReqPayload/MotorInsurance/CreateQuote.json");
				
				payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/CreateQuote.json";
				payloadFromFile = new File(payloadPath);
				pathToPayload = JsonPath.given(payloadFromFile);
				
				lib.alterPayload("vehicle.vehicleIdentificationNumber", vehicleIdentificationNumber, pathToPayload);
				lib.alterPayload("vehicle.engineNumber", engineNumber, pathToPayload);
				
//		lib.alterPayload("preInspection.isPreInspectionOpted", true, pathToPayload);
//		lib.alterPayload("preInspection.isPreInspectionRequired", true, pathToPayload);
//		lib.alterPayload("preInspection.isPreInspectionEligible", true, pathToPayload);
				
				payload = pathToPayload.getMap("$");
//		obj.captureResponseInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
				
				String payloadPathPerson = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/PersonForCQ.json";
				payloadFromFile = new File(payloadPathPerson);
				pathToPayload = JsonPath.given(payloadFromFile);
				
				Map<String,Object> payloadPerson = pathToPayload.getMap("$");		
				
				payload.putAll(payloadPerson);
				
				
				Response createQuote = baseURI
						.body(payload)
						.contentType("Application/json").accept("application/json")
						.post("/createQuote?isUserSpecialDiscountOpted=false&isDownloadQuote=true&agentcode=1000295");
				
				createQuote.then().assertThat().body("error.validationMessages[0]", containsString("TP is expired"));
				try {

					Library.staticMap.put("Tag", "@AgentFloat_1YrOD_with_TPP_Expired_E4WC,@ExistingFourWheelerComprehensive,@AllMotorAPI".replaceAll("@", ""));
					Library.staticMap.put("StatusCode", createQuote.statusCode()+"");
					Library.staticMap.put("Policy Number", createQuote.jsonPath().getJsonObject("error.validationMessages[0]").toString());
					System.out.println("Policy Number Captured.");
				} catch (Exception e) {
					System.out.println("Unexpected Response");
				}
				System.out.println("Policy issuance not allowed, since TP is expired");
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
	@SuppressWarnings("unchecked")
	public void cheque() throws IOException {
		
		com.utilities.Library lib = new com.utilities.Library();

		PropertyFileReader read = new PropertyFileReader();
		
		RequestSpecification baseURI = given()
				.auth().preemptive().basic(read.getMotorAPIUsername(), read.getMotorAPIPassword())
				.baseUri("https://preprod-digitplusservice.godigit.com/digitplusservices");

		Calendar cal = Calendar.getInstance(); 
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
		Random random = new Random();

		String previousPolicyExpiryDate = s.format(cal.getTime());
		String enquiryId = "DP-20201-Id-"+random.nextInt(1000);
		String licensePlateNumber = "KA01QK"+(1000+random.nextInt(8999));
		String vehicleIdentificationNumber = "ASDASUD9AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89));
		String engineNumber = (10+random.nextInt(89))+"AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89))+"ASD";

		String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/QuickQuote_4W.json";

		File payloadFromFile = new File(payloadPath);

		JsonPath pathToPayload = JsonPath.given(payloadFromFile);
		
		lib.alterPayload("enquiryId", enquiryId, pathToPayload);
		lib.alterPayload("contract.insuranceProductCode", "20201", pathToPayload);
		lib.alterPayload("previousInsurer.previousPolicyExpiryDate", previousPolicyExpiryDate, pathToPayload);
		lib.alterPayload("previousInsurer.previousInsurerCode", "139", pathToPayload);
		lib.alterPayload("previousInsurer.previousPolicyNumber", "POL"+random.nextInt(), pathToPayload);
		lib.alterPayload("previousInsurer.isClaimInLastYear", false, pathToPayload);
		lib.alterPayload("previousInsurer.previousPolicyType", "Comprehensive policy with Zero Dep", pathToPayload);
		lib.alterPayload("vehicle.licensePlateNumber", licensePlateNumber, pathToPayload);
		
		lib.alterPayload("vehicle.seatingCapacity", 7, pathToPayload);

		Map<String,Object> payload = pathToPayload.getMap("$");		
		
		Response quickQuote = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/quickQuote?isPremiumRecalculate=false&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");

		lib.captureResponseInFile(quickQuote, "/ReqPayload/MotorInsurance/Recalculate.json");

		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Recalculate.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		
//		ArrayList<Object> coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//		for (int i = 0; i < coverages.size(); i++) {
//			
//			Map<String, Object> eachCoverage = (Map<String, Object>)coverages.get(i);
//			if (eachCoverage.get("coverAvailability").equals("AVAILABLE")) {
//				
//				obj.alterPayload("contract.coverages["+i+"].selection", true, pathToPayload);
//				
//				coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//			}
//		}
		payload = pathToPayload.getMap("$");

		
		Response recalculate = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/quickQuote?isPremiumRecalculate=true&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");
		
		lib.captureResponseInFile(recalculate, "/ReqPayload/MotorInsurance/CreateQuote.json");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/CreateQuote.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		lib.alterPayload("vehicle.vehicleIdentificationNumber", vehicleIdentificationNumber, pathToPayload);
		lib.alterPayload("vehicle.engineNumber", engineNumber, pathToPayload);
		
//		lib.alterPayload("preInspection.isPreInspectionOpted", true, pathToPayload);
//		lib.alterPayload("preInspection.isPreInspectionRequired", true, pathToPayload);
//		lib.alterPayload("preInspection.isPreInspectionEligible", true, pathToPayload);
		
		payload = pathToPayload.getMap("$");
//		obj.captureResponseInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
		
		String payloadPathPerson = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/PersonForCQ.json";
		payloadFromFile = new File(payloadPathPerson);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		Map<String,Object> payloadPerson = pathToPayload.getMap("$");		
		
		payload.putAll(payloadPerson);
		

		Response createQuote = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/createQuote?isUserSpecialDiscountOpted=false&isDownloadQuote=true&agentcode=1000295");

		lib.captureResponseInFile(createQuote, "/ReqPayload/MotorInsurance/UpdateQuote.json");
		
		String applicationId = createQuote.jsonPath().getString("applicationId");

		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UpdateQuote.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		payload = pathToPayload.getMap("$");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UploadDoc.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		Map<String,Object> payloadDocument = pathToPayload.getMap("$");
		
		JSONArray jar = new JSONArray();
		jar.add(payloadDocument);
		
		payload.put("documents", jar);
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Payment.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		Map<String,Object> payloadPayment = pathToPayload.getMap("$");
		payloadPayment.put("paymentType", "CHEQUE");
		payloadPayment.put("paymentDate", s.format(cal.getTime()));
		payloadPayment.put("instrumentNumber", "123234");
		payloadPayment.put("ifscCode", "SBIN0001024");
		payloadPayment.put("agentConsent", "N");
		payloadPayment.put("paymentSource", "CUSTOMER_CHEQUE");
		
		payload.put("payment", payloadPayment);
		
		Response updateQuote = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/updateQuote/"+applicationId+"?isUserSpecialDiscountOpted=false&isDownloadQuote=false&agentcode=1000295");
		
		lib.captureResponseInFile(updateQuote, "/ReqPayload/MotorInsurance/IssueContract.json");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/IssueContract.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
//		lib.alterPayload("motorQuestions.selfInspection", true, pathToPayload);
//		lib.alterPayload("motorBreakIn.isBreakin", true, pathToPayload);
//		lib.alterPayload("motorBreakIn.isDocumentUploaded", true, pathToPayload);

		payload = pathToPayload.getMap("$");
		
		
		payload.put("agentName", "Vineet Bharadwaj");
		payload.put("userId", "35327650");
		payload.put("payment", null);
		payload.put("surveyData", null);
		
		lib.capturePayloadInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
		
		System.out.println("PASS1");
		Response issueContract = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/issueContract/"+applicationId);//+"?&userId=35327650&sourceType=20201motor&channel=D01&policyNumber="+policyNumber+"&agentcode=1000295");

		System.out.println(issueContract.asPrettyString());
		try {

			Library.staticMap.put("Tag", "@Cheque_E4WC,@ExistingFourWheelerComprehensive,@AllMotorAPI".replaceAll("@", ""));
			Library.staticMap.put("StatusCode", issueContract.statusCode()+"");
			Library.staticMap.put("Policy Number", issueContract.jsonPath().getString("policyNumber"));
			System.out.println("Policy Number Captured.");
		} catch (Exception e) {
			System.out.println("Unexpected Response");
		}		
		System.out.println("PASS2");

	}
	
	@SuppressWarnings("unchecked")
	public void online() throws IOException {
		
		com.utilities.Library lib = new com.utilities.Library();

		PropertyFileReader read = new PropertyFileReader();
		
		RequestSpecification baseURI = given()
				.auth().preemptive().basic(read.getMotorAPIUsername(), read.getMotorAPIPassword())
				.baseUri("https://preprod-digitplusservice.godigit.com/digitplusservices");

		Calendar cal = Calendar.getInstance(); 
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
		Random random = new Random();

		String previousPolicyExpiryDate = s.format(cal.getTime());
		String enquiryId = "DP-20201-Id-"+random.nextInt(1000);
		String licensePlateNumber = "KA01QK"+(1000+random.nextInt(8999));
		String vehicleIdentificationNumber = "ASDASUD9AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89));
		String engineNumber = (10+random.nextInt(89))+"AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89))+"ASD";

		String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/QuickQuote_4W.json";

		File payloadFromFile = new File(payloadPath);

		JsonPath pathToPayload = JsonPath.given(payloadFromFile);
		
		lib.alterPayload("enquiryId", enquiryId, pathToPayload);
		lib.alterPayload("contract.insuranceProductCode", "20201", pathToPayload);
		lib.alterPayload("previousInsurer.previousPolicyExpiryDate", previousPolicyExpiryDate, pathToPayload);
		lib.alterPayload("previousInsurer.previousInsurerCode", "139", pathToPayload);
		lib.alterPayload("previousInsurer.previousPolicyNumber", "POL"+random.nextInt(), pathToPayload);
		lib.alterPayload("previousInsurer.isClaimInLastYear", false, pathToPayload);
		lib.alterPayload("previousInsurer.previousPolicyType", "Comprehensive policy with Zero Dep", pathToPayload);
		lib.alterPayload("vehicle.licensePlateNumber", licensePlateNumber, pathToPayload);
		
		lib.alterPayload("vehicle.seatingCapacity", 7, pathToPayload);

		Map<String,Object> payload = pathToPayload.getMap("$");		
		
		Response quickQuote = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/quickQuote?isPremiumRecalculate=false&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");

		lib.captureResponseInFile(quickQuote, "/ReqPayload/MotorInsurance/Recalculate.json");

		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Recalculate.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		
//		ArrayList<Object> coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//		for (int i = 0; i < coverages.size(); i++) {
//			
//			Map<String, Object> eachCoverage = (Map<String, Object>)coverages.get(i);
//			if (eachCoverage.get("coverAvailability").equals("AVAILABLE")) {
//				
//				obj.alterPayload("contract.coverages["+i+"].selection", true, pathToPayload);
//				
//				coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//			}
//		}
		payload = pathToPayload.getMap("$");

		
		Response recalculate = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/quickQuote?isPremiumRecalculate=true&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");
		
		lib.captureResponseInFile(recalculate, "/ReqPayload/MotorInsurance/CreateQuote.json");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/CreateQuote.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		lib.alterPayload("vehicle.vehicleIdentificationNumber", vehicleIdentificationNumber, pathToPayload);
		lib.alterPayload("vehicle.engineNumber", engineNumber, pathToPayload);
		
//		lib.alterPayload("preInspection.isPreInspectionOpted", true, pathToPayload);
//		lib.alterPayload("preInspection.isPreInspectionRequired", true, pathToPayload);
//		lib.alterPayload("preInspection.isPreInspectionEligible", true, pathToPayload);
		
		payload = pathToPayload.getMap("$");
//		obj.captureResponseInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
		
		String payloadPathPerson = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/PersonForCQ.json";
		payloadFromFile = new File(payloadPathPerson);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		Map<String,Object> payloadPerson = pathToPayload.getMap("$");		
		
		payload.putAll(payloadPerson);
		

		Response createQuote = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/createQuote?isUserSpecialDiscountOpted=false&isDownloadQuote=true&agentcode=1000295");

		lib.captureResponseInFile(createQuote, "/ReqPayload/MotorInsurance/UpdateQuote.json");
		
		String applicationId = createQuote.jsonPath().getString("applicationId");

		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UpdateQuote.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		payload = pathToPayload.getMap("$");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UploadDoc.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		Map<String,Object> payloadDocument = pathToPayload.getMap("$");
		
		JSONArray jar = new JSONArray();
		jar.add(payloadDocument);
		
		payload.put("documents", jar);
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Payment.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		Map<String,Object> payloadPayment = pathToPayload.getMap("$");
		payloadPayment.put("paymentType", "ONLINE");
		
		payload.put("payment", payloadPayment);
		
		Response updateQuote = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/updateQuote/"+applicationId+"?isUserSpecialDiscountOpted=false&isDownloadQuote=false&agentcode=1000295");
		
		lib.captureResponseInFile(updateQuote, "/ReqPayload/MotorInsurance/IssueContract.json");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/IssueContract.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
//		lib.alterPayload("motorQuestions.selfInspection", true, pathToPayload);
//		lib.alterPayload("motorBreakIn.isBreakin", true, pathToPayload);
//		lib.alterPayload("motorBreakIn.isDocumentUploaded", true, pathToPayload);

		payload = pathToPayload.getMap("$");
		
		
		payload.put("agentName", "Vineet Bharadwaj");
		payload.put("userId", "35327650");
		payload.put("payment", null);
		payload.put("surveyData", null);
		
		lib.capturePayloadInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
		
		System.out.println("PASS1");
		Response issueContract = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/issueContract/"+applicationId);//+"?&userId=35327650&sourceType=20201motor&channel=D01&policyNumber="+policyNumber+"&agentcode=1000295");

		System.out.println(issueContract.asPrettyString());
		try {

			Library.staticMap.put("Tag", "@Online_E4WC,@ExistingFourWheelerComprehensive,@AllMotorAPI".replaceAll("@", ""));
			Library.staticMap.put("StatusCode", issueContract.statusCode()+"");
			Library.staticMap.put("Policy Number", issueContract.jsonPath().getString("policyNumber"));
			System.out.println("Policy Number Captured.");
		} catch (Exception e) {
			System.out.println("Unexpected Response");
		}		
		System.out.println("PASS2");
	}
	
	@SuppressWarnings("unchecked")
	public void generatePaymentLink() throws IOException {
		
		com.utilities.Library lib = new com.utilities.Library();
		
		PropertyFileReader read = new PropertyFileReader();
		
		RequestSpecification baseURI = given()
				.auth().preemptive().basic(read.getMotorAPIUsername(), read.getMotorAPIPassword())
				.baseUri("https://preprod-digitplusservice.godigit.com/digitplusservices");
		
		Calendar cal = Calendar.getInstance(); 
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
		Random random = new Random();
		
		String previousPolicyExpiryDate = s.format(cal.getTime());
		String enquiryId = "DP-20201-Id-"+random.nextInt(1000);
		String licensePlateNumber = "KA01QK"+(1000+random.nextInt(8999));
		String vehicleIdentificationNumber = "ASDASUD9AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89));
		String engineNumber = (10+random.nextInt(89))+"AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89))+"ASD";
		
		String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/QuickQuote_4W.json";
		
		File payloadFromFile = new File(payloadPath);
		
		JsonPath pathToPayload = JsonPath.given(payloadFromFile);
		
		lib.alterPayload("enquiryId", enquiryId, pathToPayload);
		lib.alterPayload("contract.insuranceProductCode", "20201", pathToPayload);
		lib.alterPayload("previousInsurer.previousPolicyExpiryDate", previousPolicyExpiryDate, pathToPayload);
		lib.alterPayload("previousInsurer.previousInsurerCode", "139", pathToPayload);
		lib.alterPayload("previousInsurer.previousPolicyNumber", "POL"+random.nextInt(), pathToPayload);
		lib.alterPayload("previousInsurer.isClaimInLastYear", false, pathToPayload);
		lib.alterPayload("previousInsurer.previousPolicyType", "Comprehensive policy with Zero Dep", pathToPayload);
		lib.alterPayload("vehicle.licensePlateNumber", licensePlateNumber, pathToPayload);
		
		lib.alterPayload("vehicle.seatingCapacity", 7, pathToPayload);
		
		Map<String,Object> payload = pathToPayload.getMap("$");		
		
		Response quickQuote = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/quickQuote?isPremiumRecalculate=false&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");
		
		lib.captureResponseInFile(quickQuote, "/ReqPayload/MotorInsurance/Recalculate.json");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Recalculate.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		
//		ArrayList<Object> coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//		for (int i = 0; i < coverages.size(); i++) {
//			
//			Map<String, Object> eachCoverage = (Map<String, Object>)coverages.get(i);
//			if (eachCoverage.get("coverAvailability").equals("AVAILABLE")) {
//				
//				obj.alterPayload("contract.coverages["+i+"].selection", true, pathToPayload);
//				
//				coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//			}
//		}
		payload = pathToPayload.getMap("$");
		
		
		Response recalculate = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/quickQuote?isPremiumRecalculate=true&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");
		
		lib.captureResponseInFile(recalculate, "/ReqPayload/MotorInsurance/CreateQuote.json");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/CreateQuote.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		lib.alterPayload("vehicle.vehicleIdentificationNumber", vehicleIdentificationNumber, pathToPayload);
		lib.alterPayload("vehicle.engineNumber", engineNumber, pathToPayload);
		
//		lib.alterPayload("preInspection.isPreInspectionOpted", true, pathToPayload);
//		lib.alterPayload("preInspection.isPreInspectionRequired", true, pathToPayload);
//		lib.alterPayload("preInspection.isPreInspectionEligible", true, pathToPayload);
		
		payload = pathToPayload.getMap("$");
//		obj.captureResponseInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
		
		String payloadPathPerson = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/PersonForCQ.json";
		payloadFromFile = new File(payloadPathPerson);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		Map<String,Object> payloadPerson = pathToPayload.getMap("$");		
		
		payload.putAll(payloadPerson);
		
		
		Response createQuote = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/createQuote?isUserSpecialDiscountOpted=false&isDownloadQuote=true&agentcode=1000295");
		
		lib.captureResponseInFile(createQuote, "/ReqPayload/MotorInsurance/UpdateQuote.json");
		
		String applicationId = createQuote.jsonPath().getString("applicationId");
		String policyNumber = createQuote.jsonPath().getString("policyNumber");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UpdateQuote.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		payload = pathToPayload.getMap("$");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UploadDoc.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		Map<String,Object> payloadDocument = pathToPayload.getMap("$");
		
		JSONArray jar = new JSONArray();
		jar.add(payloadDocument);
		
		payload.put("documents", jar);
		
		
		Response updateQuote = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/updateQuote/"+applicationId+"?isUserSpecialDiscountOpted=false&isDownloadQuote=false&agentcode=1000295");
		
		lib.captureResponseInFile(updateQuote, "/ReqPayload/MotorInsurance/IssueContract.json");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/IssueContract.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		
		payload = pathToPayload.getMap("$");
		
		
		payload.put("agentName", "Vineet Bharadwaj");
		payload.put("userId", "35327650");
		payload.put("surveyData", null);
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Payment.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		Map<String,Object> payloadPayment = pathToPayload.getMap("$");
		
		payloadPayment.put("paymentType", "GENERATE_PAYMENT_LINK");
		payloadPayment.put("successReturnUrl", "https://preprod-plusui.godigit.com/DigitPlus/#/motor-new?applicationId="+applicationId+"&policyNumber="+policyNumber);
		payloadPayment.put("cancelReturnUrl", "https://preprod-plusui.godigit.com/DigitPlus/#/motor-new");
		payloadPayment.put("expiryHours", 5);
		
		payload.put("payment", payloadPayment);
		
		lib.capturePayloadInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
		
		System.out.println("PASS1");
		Response issueContract = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/issueContract/"+applicationId);//+"?&userId=35327650&sourceType=20201motor&channel=D01&policyNumber="+policyNumber+"&agentcode=1000295");
		
		System.out.println(issueContract.asPrettyString());
		try {
			Library.staticMap.put("Tag", "@GeneratePaymentLink_E4WC,@ExistingFourWheelerComprehensive,@AllMotorAPI".replaceAll("@", ""));
			Library.staticMap.put("StatusCode", issueContract.statusCode()+"");
			issueContract.then().assertThat().body("link", containsString("DigitPaymentGateway"));
			Library.staticMap.put("Policy Number", createQuote.jsonPath().getString("policyNumber")+" [PI Link Generated Successfully!!]");
			System.out.println("Policy Number Captured.");
		} catch (Exception e) {
			System.out.println("Unexpected Response");
		}		
		System.out.println("PASS2");
	}
	
	@SuppressWarnings("unchecked")
	public void selfInspection() throws IOException {
		
		com.utilities.Library lib = new com.utilities.Library();

		PropertyFileReader read = new PropertyFileReader();
		
		RequestSpecification baseURI = given()
				.auth().preemptive().basic(read.getMotorAPIUsername(), read.getMotorAPIPassword())
				.baseUri("https://preprod-digitplusservice.godigit.com/digitplusservices");

		Calendar cal = Calendar.getInstance(); 
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
		Random random = new Random();

		cal.add(Calendar.DATE, 7);
		String previousPolicyExpiryDate = s.format(cal.getTime());
		String enquiryId = "DP-20201-Id-"+random.nextInt(1000);
		String licensePlateNumber = "KA01QK"+(1000+random.nextInt(8999));
		String vehicleIdentificationNumber = "ASDASUD9AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89));
		String engineNumber = (10+random.nextInt(89))+"AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89))+"ASD";

		String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/QuickQuote_4W.json";

		File payloadFromFile = new File(payloadPath);

		JsonPath pathToPayload = JsonPath.given(payloadFromFile);
		
		lib.alterPayload("enquiryId", enquiryId, pathToPayload);
		lib.alterPayload("contract.insuranceProductCode", "20201", pathToPayload);
		lib.alterPayload("previousInsurer.previousPolicyExpiryDate", previousPolicyExpiryDate, pathToPayload);
		lib.alterPayload("previousInsurer.previousInsurerCode", "139", pathToPayload);
		lib.alterPayload("previousInsurer.previousPolicyNumber", "POL"+random.nextInt(), pathToPayload);
		lib.alterPayload("previousInsurer.isClaimInLastYear", false, pathToPayload);
		lib.alterPayload("previousInsurer.previousPolicyType", "Comprehensive policy with Zero Dep", pathToPayload);
		lib.alterPayload("vehicle.licensePlateNumber", licensePlateNumber, pathToPayload);
		
		lib.alterPayload("vehicle.seatingCapacity", 5, pathToPayload);

		Map<String,Object> payload = pathToPayload.getMap("$");		
		
		Response quickQuote = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/quickQuote?isPremiumRecalculate=false&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");

		lib.captureResponseInFile(quickQuote, "/ReqPayload/MotorInsurance/Recalculate.json");

		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Recalculate.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		
//		ArrayList<Object> coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//		for (int i = 0; i < coverages.size(); i++) {
//			
//			Map<String, Object> eachCoverage = (Map<String, Object>)coverages.get(i);
//			if (eachCoverage.get("coverAvailability").equals("AVAILABLE")) {
//				
//				obj.alterPayload("contract.coverages["+i+"].selection", true, pathToPayload);
//				
//				coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//			}
//		}
		payload = pathToPayload.getMap("$");

		
		Response recalculate = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/quickQuote?isPremiumRecalculate=true&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");
		
		lib.captureResponseInFile(recalculate, "/ReqPayload/MotorInsurance/CreateQuote.json");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/CreateQuote.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		lib.alterPayload("vehicle.vehicleIdentificationNumber", vehicleIdentificationNumber, pathToPayload);
		lib.alterPayload("vehicle.engineNumber", engineNumber, pathToPayload);
		
		lib.alterPayload("preInspection.isPreInspectionOpted", true, pathToPayload);
		lib.alterPayload("preInspection.isPreInspectionRequired", true, pathToPayload);
		lib.alterPayload("preInspection.isPreInspectionEligible", true, pathToPayload);
		
		payload = pathToPayload.getMap("$");
//		obj.captureResponseInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
		
		String payloadPathPerson = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/PersonForCQ.json";
		payloadFromFile = new File(payloadPathPerson);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		Map<String,Object> payloadPerson = pathToPayload.getMap("$");		
		
		payload.putAll(payloadPerson);
		

		Response createQuote = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/createQuote?isUserSpecialDiscountOpted=false&isDownloadQuote=true&agentcode=1000295");

		lib.captureResponseInFile(createQuote, "/ReqPayload/MotorInsurance/UpdateQuote.json");
		
		String applicationId = createQuote.jsonPath().getString("applicationId");

		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UpdateQuote.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		payload = pathToPayload.getMap("$");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UploadDoc.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		Map<String,Object> payloadDocument = pathToPayload.getMap("$");
		
		JSONArray jar = new JSONArray();
		jar.add(payloadDocument);
		
		payload.put("documents", jar);
		payload.put("payment", null);
		
		Response updateQuote = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/updateQuote/"+applicationId+"?isUserSpecialDiscountOpted=false&isDownloadQuote=false&agentcode=1000295");
		
		lib.captureResponseInFile(updateQuote, "/ReqPayload/MotorInsurance/IssueContract.json");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/IssueContract.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
//		lib.alterPayload("motorQuestions.selfInspection", true, pathToPayload);
//		lib.alterPayload("motorBreakIn.isBreakin", true, pathToPayload);
//		lib.alterPayload("motorBreakIn.isDocumentUploaded", true, pathToPayload);

		payload = pathToPayload.getMap("$");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/SurveyData.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		
		Map<String,Object> payloadSurvey = pathToPayload.getMap("$");
		payloadSurvey.put("surveyType", "SELF_INSPECTION");
		
		payload.put("agentName", "Vineet Bharadwaj");
		payload.put("userId", "35327650");
		payload.putAll(payloadSurvey);
		payload.put("payment", null); 
		payload.put("policyStatus", "PENDING_DOCUMENTS");
		
		lib.capturePayloadInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
		
		System.out.println("PASS1");
		Response issueContract = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/issueContract/"+applicationId);

		System.out.println(issueContract.asPrettyString());
		try {

			Library.staticMap.put("Tag", "@SelfInspection_E4WC,@ExistingFourWheelerComprehensive,@AllMotorAPI".replaceAll("@", ""));
			Library.staticMap.put("StatusCode", issueContract.statusCode()+"");
			Library.staticMap.put("Policy Number", issueContract.jsonPath().getString("policyNumber"));
			System.out.println("Policy Number Captured.");
		} catch (Exception e) {
			System.out.println("Unexpected Response");
		}		
		System.out.println("PASS2");
	}
	
	@SuppressWarnings("unchecked")
	public void sendPreInspectionLink() throws IOException {
		
		com.utilities.Library lib = new com.utilities.Library();

		PropertyFileReader read = new PropertyFileReader();
		
		RequestSpecification baseURI = given()
				.auth().preemptive().basic(read.getMotorAPIUsername(), read.getMotorAPIPassword())
				.baseUri("https://preprod-digitplusservice.godigit.com/digitplusservices");

		Calendar cal = Calendar.getInstance(); 
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
		Random random = new Random();

		cal.add(Calendar.DATE, 7);
		String previousPolicyExpiryDate = s.format(cal.getTime());
		String enquiryId = "DP-20201-Id-"+random.nextInt(1000);
		String licensePlateNumber = "KA01QK"+(1000+random.nextInt(8999));
		String vehicleIdentificationNumber = "ASDASUD9AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89));
		String engineNumber = (10+random.nextInt(89))+"AS"+(10+random.nextInt(89))+"D"+(10+random.nextInt(89))+"ASD";

		String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/QuickQuote_4W.json";

		File payloadFromFile = new File(payloadPath);

		JsonPath pathToPayload = JsonPath.given(payloadFromFile);
		
		lib.alterPayload("enquiryId", enquiryId, pathToPayload);
		lib.alterPayload("contract.insuranceProductCode", "20201", pathToPayload);
		lib.alterPayload("previousInsurer.previousPolicyExpiryDate", previousPolicyExpiryDate, pathToPayload);
		lib.alterPayload("previousInsurer.previousInsurerCode", "139", pathToPayload);
		lib.alterPayload("previousInsurer.previousPolicyNumber", "POL"+random.nextInt(), pathToPayload);
		lib.alterPayload("previousInsurer.isClaimInLastYear", false, pathToPayload);
		lib.alterPayload("previousInsurer.previousPolicyType", "Comprehensive policy with Zero Dep", pathToPayload);
		lib.alterPayload("vehicle.licensePlateNumber", licensePlateNumber, pathToPayload);
		
		lib.alterPayload("vehicle.seatingCapacity", 5, pathToPayload);

		Map<String,Object> payload = pathToPayload.getMap("$");		
		
		Response quickQuote = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/quickQuote?isPremiumRecalculate=false&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");

		lib.captureResponseInFile(quickQuote, "/ReqPayload/MotorInsurance/Recalculate.json");

		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/Recalculate.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		
//		ArrayList<Object> coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//		for (int i = 0; i < coverages.size(); i++) {
//			
//			Map<String, Object> eachCoverage = (Map<String, Object>)coverages.get(i);
//			if (eachCoverage.get("coverAvailability").equals("AVAILABLE")) {
//				
//				obj.alterPayload("contract.coverages["+i+"].selection", true, pathToPayload);
//				
//				coverages  = quickQuote.jsonPath().getJsonObject("contract.coverages");
//			}
//		}
		payload = pathToPayload.getMap("$");

		
		Response recalculate = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/quickQuote?isPremiumRecalculate=true&isUserSpecialDiscountOpted=false&userId=35327650&sourceType=20201motor&channel=D01");
		
		lib.captureResponseInFile(recalculate, "/ReqPayload/MotorInsurance/CreateQuote.json");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/CreateQuote.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		lib.alterPayload("vehicle.vehicleIdentificationNumber", vehicleIdentificationNumber, pathToPayload);
		lib.alterPayload("vehicle.engineNumber", engineNumber, pathToPayload);
		
		lib.alterPayload("preInspection.isPreInspectionOpted", true, pathToPayload);
		lib.alterPayload("preInspection.isPreInspectionRequired", true, pathToPayload);
		lib.alterPayload("preInspection.isPreInspectionEligible", true, pathToPayload);
		
		payload = pathToPayload.getMap("$");
//		obj.captureResponseInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
		
		String payloadPathPerson = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/PersonForCQ.json";
		payloadFromFile = new File(payloadPathPerson);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		Map<String,Object> payloadPerson = pathToPayload.getMap("$");		
		
		payload.putAll(payloadPerson);
		

		Response createQuote = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/createQuote?isUserSpecialDiscountOpted=false&isDownloadQuote=true&agentcode=1000295");

		lib.captureResponseInFile(createQuote, "/ReqPayload/MotorInsurance/UpdateQuote.json");
		
		String applicationId = createQuote.jsonPath().getString("applicationId");

		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UpdateQuote.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		payload = pathToPayload.getMap("$");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/UploadDoc.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		Map<String,Object> payloadDocument = pathToPayload.getMap("$");
		
		JSONArray jar = new JSONArray();
		jar.add(payloadDocument);
		
		payload.put("documents", jar);
		payload.put("payment", null);
		
		Response updateQuote = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/updateQuote/"+applicationId+"?isUserSpecialDiscountOpted=false&isDownloadQuote=false&agentcode=1000295");
		
		lib.captureResponseInFile(updateQuote, "/ReqPayload/MotorInsurance/IssueContract.json");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/IssueContract.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
//		lib.alterPayload("motorQuestions.selfInspection", true, pathToPayload);
//		lib.alterPayload("motorBreakIn.isBreakin", true, pathToPayload);
//		lib.alterPayload("motorBreakIn.isDocumentUploaded", true, pathToPayload);

		payload = pathToPayload.getMap("$");
		
		payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/MotorInsurance/SurveyData.json";
		payloadFromFile = new File(payloadPath);
		pathToPayload = JsonPath.given(payloadFromFile);
		
		
		Map<String,Object> payloadSurvey = pathToPayload.getMap("$");
		payloadSurvey.put("surveyType", "SEND_INSPECTION_LINK");
		
		payload.put("agentName", "Vineet Bharadwaj");
		payload.put("userId", "35327650");
		payload.putAll(payloadSurvey);
		payload.put("payment", null); 
		payload.put("policyStatus", "PENDING_DOCUMENTS");
		
		lib.capturePayloadInFile(payload, "/ReqPayload/MotorInsurance/PayloadForUQ.txt");
		
		System.out.println("PASS1");
		Response issueContract = baseURI
				.body(payload)
				.contentType("Application/json").accept("application/json")
				.post("/issueContract/"+applicationId);

		System.out.println(issueContract.asPrettyString());
		try {

			Library.staticMap.put("Tag", "@SendPreInspectionLink_E4WC,@ExistingFourWheelerComprehensive,@AllMotorAPI".replaceAll("@", ""));
			Library.staticMap.put("StatusCode", issueContract.statusCode()+"");
			Library.staticMap.put("Policy Number", issueContract.jsonPath().getString("policyNumber"));
			System.out.println("Policy Number Captured.");
		} catch (Exception e) {
			System.out.println("Unexpected Response");
		}		
		System.out.println("PASS2");
	}
}
