package institutional_api;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.Random;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Yatra {


	
	public void testAPI() throws IOException {

		Yatra obj = new Yatra();
	
	RequestSpecification baseURI = given()
			.auth().preemptive().basic("43148245", "digit123")
			.baseUri("https://prod-abs-sanity.godigit.com/mobileinsurance/services");
	
	
	Calendar cal = Calendar.getInstance(); 
	SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
	String startDate = s.format(cal.getTime());
	cal.add(Calendar.YEAR, 1);
	cal.add(Calendar.DATE, -1);
	String expiryDate = s.format(cal.getTime());
	Random random = new Random();
	String orderId = "Honor"+random.nextInt(1000)+"Id"+random.nextInt(1000);
	
	String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/ManishaPukale/Yatra.json";
	
	File payloadFromFile = new File(payloadPath);
	
	JsonPath pathToPayload = JsonPath.given(payloadFromFile);
	
	obj.alterPayload("orderId", orderId, pathToPayload);
	obj.alterPayload("policyStartDate", startDate, pathToPayload);
	obj.alterPayload("policyEndDate", expiryDate, pathToPayload);
	
	Map<String,Object> payload = pathToPayload.getMap("$");
	
	Response create = baseURI
			.body(payload)			//The payload() returns the RequestPayload after changing a few values against their keys as per requirement. 
			.contentType("Application/json").accept("application/json")
			.post("/group1/quote");
	
	System.out.println(create.asPrettyString());
	
	obj.captureResponseInTextFile(create);			//As "the Response" is too big for the eclipse-console, it is being stored in a
}													//text-file so that we can look at it in its entirety and add validations accordingly.



private void alterPayload(String keyPath,Object value,JsonPath pathToPayload) {
	
	Map<String,Object> map = pathToPayload.get("$");
	
	String[] split = keyPath.split("\\.");
	for (String key : split) {
		if (!key.equals(split[split.length-1])) {
			map = pathToPayload.get(key);
		} else {
			map.put(key, value);
		}
	}
}

private void captureResponseInTextFile(Response response) throws IOException {
	
	Object entireResponse = response.jsonPath().get("$");
	File f = new File(Paths.get("").toAbsolutePath().toString()+"/AllResponse/ManishaPukale/Yatra.txt");
	if (!f.exists()) {
		f.createNewFile();
	}
	PrintWriter pw = new PrintWriter(f);
	pw.write(entireResponse.toString());
	pw.close();
}
}
