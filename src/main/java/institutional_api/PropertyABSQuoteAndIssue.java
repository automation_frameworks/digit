package institutional_api;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.Random;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PropertyABSQuoteAndIssue {

	
	public void testAPI() throws IOException {

		PropertyABSQuoteAndIssue obj = new PropertyABSQuoteAndIssue();
	
	RequestSpecification baseURI = given()
			.auth().preemptive().basic("43148245", "digit123")
			.baseUri("https://prod-abs-sanity.godigit.com/propertyinsurance/services");
	
	
	Calendar cal = Calendar.getInstance(); 
	SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
	String startDate = s.format(cal.getTime());
	cal.add(Calendar.YEAR, 1);
	cal.add(Calendar.DATE, -1);
	String endDate = s.format(cal.getTime());
	Random random = new Random();
	String enquiryId = "Prop"+random.nextInt(1000)+"Id"+random.nextInt(1000);
	String providerRefNo = "01Test"+(10000+random.nextInt(1000));
	
	String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/ManishaPukale/PropertyABSQuote.json";
	
	File payloadFromFile = new File(payloadPath);
	
	JsonPath pathToPayload = JsonPath.given(payloadFromFile);
	
	obj.alterPayload("enquiryId", enquiryId, pathToPayload);
	obj.alterPayload("packageName", "A7", pathToPayload);
	obj.alterPayload("providerRefNo", providerRefNo, pathToPayload);
	obj.alterPayload("startDate", startDate, pathToPayload);
	obj.alterPayload("endDate", endDate, pathToPayload);
	
	Map<String,Object> payload = pathToPayload.getMap("$");
	
	Response create = baseURI
			.body(payload)			 
			.contentType("Application/json").accept("application/json")
			.post("/group/policy/create");
	
	System.out.println(create.asPrettyString());
	
	String applicationId = create.jsonPath().getString("applicationId");
	enquiryId = "Prop"+random.nextInt(1000)+"Id"+random.nextInt(1000);
	
	payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/Institutional_API/PropertyABSIssue.json";
	payloadFromFile = new File(payloadPath);
	pathToPayload = JsonPath.given(payloadFromFile);
	obj.alterPayload("enquiryId", enquiryId, pathToPayload);
	obj.alterPayload("providerRefNo", providerRefNo, pathToPayload);
	obj.alterPayload("applicationId", applicationId, pathToPayload);
	payload = pathToPayload.get("$");
	
	
	Response issue = baseURI
			.body(payload)			
			.contentType("Application/json").accept("application/json")
			.post("/group/policy/issue");
	
	System.out.println(issue.asPrettyString());
//	obj.captureResponseInTextFile(create);			//As "the Response" sometimes is too big for the eclipse-console, it is being stored in a
}													//text-file so that we can look at it in its entirety and add validations accordingly.

public static void main(String[] args) {
PropertyABSQuoteAndIssue obj = new PropertyABSQuoteAndIssue();
	
	RequestSpecification baseURI = given()
//			.auth().preemptive().basic("43148245", "digit123")
			.baseUri("https://prod-abs-sanity.godigit.com/propertyinsurance/services");
	
	
	Calendar cal = Calendar.getInstance(); 
	SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
	String startDate = s.format(cal.getTime());
	cal.add(Calendar.YEAR, 1);
	cal.add(Calendar.DATE, -1);
	String endDate = s.format(cal.getTime());
	Random random = new Random();
	String enquiryId = "Prop"+random.nextInt(1000)+"Id"+random.nextInt(1000);
	String providerRefNo = "01Test"+(10000+random.nextInt(1000));
	
	String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/Institutional_API/PropertyABSQuote.json";
	
	File payloadFromFile = new File(payloadPath);
	
	JsonPath pathToPayload = JsonPath.given(payloadFromFile);
	
	obj.alterPayload("enquiryId", enquiryId, pathToPayload);
	obj.alterPayload("packageName", "A7", pathToPayload);
	obj.alterPayload("providerRefNo", providerRefNo, pathToPayload);
	obj.alterPayload("startDate", startDate, pathToPayload);
	obj.alterPayload("endDate", endDate, pathToPayload);
	
	Map<String,Object> payload = pathToPayload.getMap("$");
	
	Response create = baseURI
			.body(payload)			 
			.contentType("Application/json").accept("application/json")
			.post("/group/policy/create");
	
	System.out.println(create.asPrettyString());
	
	String applicationId = create.jsonPath().getString("applicationId");
	enquiryId = "Prop"+random.nextInt(1000)+"Id"+random.nextInt(1000);
	
	payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/Institutional_API/PropertyABSIssue.json";
	payloadFromFile = new File(payloadPath);
	pathToPayload = JsonPath.given(payloadFromFile);
	obj.alterPayload("enquiryId", enquiryId, pathToPayload);
	obj.alterPayload("providerRefNo", providerRefNo, pathToPayload);
	obj.alterPayload("applicationId", applicationId, pathToPayload);
	payload = pathToPayload.get("$");
	
	
	Response issue = baseURI
			.body(payload)			
			.contentType("Application/json").accept("application/json")
			.post("/group/policy/issue");
	
	System.out.println(issue.asPrettyString());
}

private void alterPayload(String keyPath,Object value,JsonPath pathToPayload) {
	
	Map<String,Object> map = pathToPayload.get("$");
	
	String[] split = keyPath.split("\\.");
	for (String key : split) {
		if (!key.equals(split[split.length-1])) {
			map = pathToPayload.get(key);
		} else {
			map.put(key, value);
		}
	}
}

private void captureResponseInTextFile(Response response) throws IOException {
	
	Object entireResponse = response.jsonPath().get("$");
	File f = new File(Paths.get("").toAbsolutePath().toString()+"/AllResponse/ManishaPukale/PropertyABSQuote.txt");
	if (!f.exists()) {
		f.createNewFile();
	}
	PrintWriter pw = new PrintWriter(f);
	pw.write(entireResponse.toString());
	pw.close();
}
}
