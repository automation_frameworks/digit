package institutional_api;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.Random;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class KS_CreateQuoteAndIssuepolicy {

	
	public void testAPI() throws IOException {

		KS_CreateQuoteAndIssuepolicy obj = new KS_CreateQuoteAndIssuepolicy();
	
	RequestSpecification baseURI = given()
			.auth().preemptive().basic("43148245", "digit123")
			.baseUri("https://prod-abs-sanity.godigit.com/mobileinsurance/services");
	
	
	Calendar cal = Calendar.getInstance(); 
	SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
	String startDate = s.format(cal.getTime());
	cal.add(Calendar.DATE, -1);
	String orderDate = s.format(cal.getTime());
	String deliveryDate = s.format(cal.getTime());
	cal.add(Calendar.YEAR, 1);
	String expiryDate = s.format(cal.getTime());
	Random random = new Random();
	String orderId = "Order"+random.nextInt(1000)+"Id"+random.nextInt(1000);
	String nonAbsId = "H"+random.nextInt(9)+""+random.nextInt(1000);
	String itemId = "Item"+random.nextInt(1000)+""+random.nextInt(1000);
	String identificationNumber = (random.nextInt(8)+1)+""+(1000+random.nextInt(999))+"0000000000";
	
	String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/ManishaPukale/KS_CreateQuoteCopy.json";
	
	File payloadFromFile = new File(payloadPath);
	
	JsonPath pathToPayload = JsonPath.given(payloadFromFile);
	
	obj.alterPayload("orderId", orderId, pathToPayload);
	obj.alterPayload("itemId", itemId, pathToPayload);
	obj.alterPayload("nonAbsId", nonAbsId, pathToPayload);
	obj.alterPayload("mobileDetails[0].identificationNumber", identificationNumber, pathToPayload);
	obj.alterPayload("mobileDetails[0].orderDate", orderDate, pathToPayload);
	obj.alterPayload("mobileDetails[0].deliveryDate", deliveryDate, pathToPayload);
	obj.alterPayload("policyStartDate", startDate, pathToPayload);
	obj.alterPayload("policyEndDate", expiryDate, pathToPayload);
	
	Map<String,Object> payload = pathToPayload.get("$");
	
	Response create = baseURI
			.body(payload)			//The payload() returns the RequestPayload after changing a few values against their keys as per requirement. 
			.contentType("Application/json").accept("application/json")
			.post("/group1/quote");
	
	System.out.println(create.asPrettyString());
	
	String policyId = create.jsonPath().getString("policyId");
	
	payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/Institutional_API/KS_IssuePolicy.json";
	payloadFromFile = new File(payloadPath);
	pathToPayload = JsonPath.given(payloadFromFile);
	obj.alterPayload("policyId", policyId, pathToPayload);
	payload = pathToPayload.get("$");
	
	Response issue = baseURI
			.body(payload)			//The payload() returns the RequestPayload after changing a few values against their keys as per requirement. 
			.contentType("Application/json").accept("application/json")
			.post("/group2/"+policyId+"/issue");
	
	System.out.println(issue.asPrettyString());
//	obj.captureResponseInTextFile(create);			//As "the Response" is too big for the eclipse-console, it is being stored in a
}													//text-file so that we can look at it in its entirety and add validations accordingly.

public static void main(String[] args) {
	
	KS_CreateQuoteAndIssuepolicy obj = new KS_CreateQuoteAndIssuepolicy();
	
	RequestSpecification baseURI = given()
			.auth().preemptive().basic("43148245", "digit123")
			.baseUri("https://prod-abs-sanity.godigit.com/mobileinsurance/services");
	
	
	Calendar cal = Calendar.getInstance(); 
	SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd"); 
	String startDate = s.format(cal.getTime());
	cal.add(Calendar.DATE, -1);
	String orderDate = s.format(cal.getTime());
	String deliveryDate = s.format(cal.getTime());
	cal.add(Calendar.YEAR, 1);
	String expiryDate = s.format(cal.getTime());
	Random random = new Random();
	String orderId = "Order"+random.nextInt(1000)+"Id"+random.nextInt(1000);
	String nonAbsId = "H"+random.nextInt(9)+""+random.nextInt(1000);
	String itemId = "Item"+random.nextInt(1000)+""+random.nextInt(1000);
	String identificationNumber = (random.nextInt(8)+1)+""+(1000+random.nextInt(999))+"0000000000";
	
	String payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/ManishaPukale/KS_CreateQuoteCopy.json";
	
	File payloadFromFile = new File(payloadPath);
	
	JsonPath pathToPayload = JsonPath.given(payloadFromFile);
	
	obj.alterPayload("orderId", orderId, pathToPayload);
	obj.alterPayload("itemId", itemId, pathToPayload);
	obj.alterPayload("nonAbsId", nonAbsId, pathToPayload);
	obj.alterPayload("mobileDetails[0].identificationNumber", identificationNumber, pathToPayload);
	obj.alterPayload("mobileDetails[0].orderDate", orderDate, pathToPayload);
	obj.alterPayload("mobileDetails[0].deliveryDate", deliveryDate, pathToPayload);
	obj.alterPayload("policyStartDate", startDate, pathToPayload);
	obj.alterPayload("policyEndDate", expiryDate, pathToPayload);
	
	Map<String,Object> payload = pathToPayload.get("$");
	
	Response create = baseURI
			.body(payload)			//The payload() returns the RequestPayload after changing a few values against their keys as per requirement. 
			.contentType("Application/json").accept("application/json")
			.post("/group1/quote");
	
	System.out.println(create.asPrettyString());
	
	String policyId = create.jsonPath().getString("policyId");
	
	payloadPath = Paths.get("").toAbsolutePath().toString()+"/ReqPayload/Institutional_API/KS_IssuePolicy.json";
	payloadFromFile = new File(payloadPath);
	pathToPayload = JsonPath.given(payloadFromFile);
	obj.alterPayload("policyId", policyId, pathToPayload);
	payload = pathToPayload.get("$");
	
	Response issue = baseURI
			.body(payload)			//The payload() returns the RequestPayload after changing a few values against their keys as per requirement. 
			.contentType("Application/json").accept("application/json")
			.post("/group2/"+policyId+"/issue");
	
	System.out.println(issue.asPrettyString());
}

private void alterPayload(String keyPath,Object value,JsonPath pathToPayload) {
	
	Map<String,Object> map = pathToPayload.get("$");
	
	String[] split = keyPath.split("\\.");
	for (String key : split) {
		if (!key.equals(split[split.length-1])) {
			map = pathToPayload.get(key);
		} else {
			map.put(key, value);
		}
	}
}

private void captureResponseInTextFile(Response response) throws IOException {
	
	Object entireResponse = response.jsonPath().get("$");
	File f = new File(Paths.get("").toAbsolutePath().toString()+"/AllResponse/ManishaPukale/KS_CreateQuoteCopy.txt");
	if (!f.exists()) {
		f.createNewFile();
	}
	PrintWriter pw = new PrintWriter(f);
	pw.write(entireResponse.toString());
	pw.close();
}
}
