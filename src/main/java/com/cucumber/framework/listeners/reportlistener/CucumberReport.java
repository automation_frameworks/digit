package com.cucumber.framework.listeners.reportlistener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.List;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipOutputStream;

import com.cucumber.framework.utility.ResourceHelper;
import com.utilities.PropertyFileReader;
import com.utilities.PropertyFileWriter;
import com.utilities.TaskExecutor;
import org.apache.log4j.Logger;
import org.testng.ISuite;
import org.testng.ISuiteListener;

import com.cucumber.framework.helper.Logger.LoggerHelper;

import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;

public class CucumberReport implements ISuiteListener {
	public final Logger oLog = LoggerHelper.getLogger(CucumberReport.class);
	PropertyFileWriter pfw = new PropertyFileWriter();
	PropertyFileReader prop = new PropertyFileReader();
	public static com.utilities.TaskExecutor te = new com.utilities.TaskExecutor();

	@Override
	public void onStart(ISuite suite) {

	}
	
	@Override
	public void onFinish(ISuite suite) {
		try {
			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat("Eyyyy.MM.dd'_'hh:mm:ssa");
			String timeStamp = ft.format(dNow).replaceAll(":", "_");

			File jsonfile = new File("target/");
			String pathname = "target/reports/cucumberreports/" + timeStamp;
			File reportOutputDirectory = new File(pathname);

			String[] fileNames = jsonfile.list(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					if (name.endsWith(".json"))
						return true;
					return false;
				}
			});

			for (int i = 0; i < fileNames.length; i++) {
				// System.out.println("File Name : "+fileNames[i]);
				fileNames[i] = jsonfile.getAbsolutePath() + "/" + fileNames[i];
			}

			List<String> jsonFiles = Arrays.asList(fileNames);

			Configuration configuration = new Configuration(reportOutputDirectory, suite.getName());
			configuration.setStatusFlags(true, true, true);

			ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
			reportBuilder.generateReports();
			oLog.info("Report Generated: " + configuration.getReportDirectory());

			if (prop.getCustomProperty("deliverReportsViaMail").equals("true")) {
				// Convert the cucumber reports dir into a zipped file
				String zipOut = pathname + ".zip";
				te.convertReportToZip(pathname, zipOut);
				Thread.sleep(4000);
				System.out.println("-------------------------------------------------------------------------------------");
				System.out.println("Compressed cucumber reports generated: " + zipOut);
				System.out.println("-------------------------------------------------------------------------------------");

				pfw.setCustomProperty(
						"cucumberReportsPath",
						"" + zipOut,
						"" + ResourceHelper.getReportsPropPath()
						);

				// Send reports to all recipients
				te.emailReports();
			}

			System.out.println("-------------------------------------------------------------------------------------");

			// Kill all the drivers running in background
			try {
				te.driverKill();
			} catch (Exception e) {
				
			}

			System.out.println("-------------------------------------------------------------------------------------");
		} catch (Exception e) {
			oLog.equals(e);
		}
	}

}
