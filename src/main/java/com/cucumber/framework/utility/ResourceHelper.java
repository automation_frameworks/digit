/**
 * @author rahul.rathore
 *	
 *	20-Jul-2016
 */
package com.cucumber.framework.utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author rahul.rathore
 *	
 *	20-Jul-2016
 *
 */
public class ResourceHelper {
	final static String REPORTS_PROP_PATH = "target/test-classes/configfile/reports.properties";

	public ResourceHelper() {
		try {
			// Delete the config file if it already exists (otherwise, we may have duplicate key and value pairs)
			Path path = Paths.get(REPORTS_PROP_PATH);
			Files.deleteIfExists(path);
			Files.createFile(path);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getResourcePath(String resource) {
		String path = getBaseResourcePath() + resource;
		return path;
	}
	
	public static String getBaseResourcePath() {
		String path = ResourceHelper.class.getResource("/").getPath();
		return path;
	}
	
	public static InputStream getResourcePathInputStream(String resource) throws FileNotFoundException {
		return new FileInputStream(ResourceHelper.getResourcePath(resource));
	}

	public static String getReportsPropPath() {
		return REPORTS_PROP_PATH;
	}
}
