package com.agent.motor.api;

public class CreateBreakinPolicy {

	public void createBreakinPolicy(String vehicleType) throws Exception
	{
		try {
			switch(vehicleType.toLowerCase().replaceAll(" ", ""))
			{
			case "privatecar" :
				PrivateCar.createPolicyAndInspect();
				
				break;
			case "gcv3w":
				GCV_3W.createPolicyAndInspect();
				break;
			case "gcv4w":
				GCV_4W.createPolicyAndInspect();
				break;
			case "pcv3w":
				PCV_3W.createPolicyAndInspect();
				break;
			case "pcv4w":
				PCV_4W.createPolicyAndInspect();
				break;
			case "pcv51seater":
				PCV_51Seater.createPolicyAndInspect();
				break;
			case "misc":
				GCV_4W.createPolicyAndInspect();
				break;
				default:
					System.out.println("The given Vehicle Type '"+vehicleType+"' is not recognised");
				
			}
		}
		catch (Exception e)
		{
			throw new Exception(e.getMessage());
		}
	}
}
