package com.agent.motor.api;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.cucumber.framework.utility.ResourceHelper;

import pom.mobile.preinspection.UploadPhotos;

public class CreateBreakinIntegrated {

	//Breakin, New, Renewal
	static String URL_UAT = "uat-qnb.godigit.com";
	static String URL_PREPROD = "preprod-plus.godigit.com";
	static String URL_PREPRODPLUS1 = "preprod-plus1.godigit.com";
	static String URL_PREPRODTEST = "preprodtest-plus.godigit.com";
	static String server,vehicleType,sourceType,accessToken, applicationId, policyNumber, link,regNumber,randomNumber;
	
	public static void main(String args[]) throws Exception
	{
		new CreateBreakinIntegrated().createPolicy("breakin", "GCV 3W", "uat",true);
	}
	
	public void createPolicy(String policyType,String vehicleType,String environment,boolean preInspection) throws Exception
	{
		switch(environment.toLowerCase().replaceAll(" ", ""))
		{
		case "uat":
			server = URL_UAT;
			break;
		case "preprod":
			server = URL_PREPROD;
			break;
		case "preprodtest":
			server = URL_PREPRODTEST;
			break;
		case "preprodplus1":
			server = URL_PREPRODPLUS1;
			break;
			default:
				throw new Exception("Please specify a valid environment (uat,preprod,preprodtest,preprodplus1)");
		}
		switch(vehicleType.toLowerCase().replaceAll(" ", ""))
		{
		case "privatecar":
			CreateBreakinIntegrated.vehicleType = "PrivateCar";
			break;
		case "twowheeler":
			CreateBreakinIntegrated.vehicleType = "TwoWheeler";
			break;
		case "gcv3w":
			CreateBreakinIntegrated.vehicleType = "GCV3W";
			break;
		case "gcv4w":
			CreateBreakinIntegrated.vehicleType = "GCV4W";
			break;
		case "pcv3w":
			CreateBreakinIntegrated.vehicleType = "PCV3W";
			break;
		case "pcv4w":
			CreateBreakinIntegrated.vehicleType = "PCV4W";
			break;
		case "pcv51seater":
			CreateBreakinIntegrated.vehicleType = "PCV51Seater";
			break;
		case "misc":
			CreateBreakinIntegrated.vehicleType = "Misc";
			break;
		default:
			throw new Exception("Please specify a valid Vehicle Type (two wheeler,private car,gcv 3w,gcv 4w,pcv 3w,pcv 4w,pcv 51 seater,misc)");
		}
		grant();
		createQuote();
		issuePolicy();
		if(preInspection)
		{
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
		getPreInspectionLink();
		
		UploadPhotos.uploadImages(link);
		}
	}
	
	
	private static void grant()
	{
		try {
			String url = "https://"+server+"/digitplusservices/oauth/grant";
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept", "application/json, text/plain, */*");
			con.setRequestProperty("content-type", "application/x-www-form-urlencoded");

			String urlParameters = "username=35327650&password=Digit@123$";

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
//			System.out.println("Post parameters : " + urlParameters);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println("Response : "+response.toString());

			Object jsonObj = new JSONParser().parse(response.toString());

			// typecasting obj to JSONObject
			JSONObject jo = (JSONObject) jsonObj;
			//System.out.println("Access Token : " + jo.get("accessToken"));
			accessToken = jo.get("accessToken").toString();
		} catch (MalformedURLException me) {
			me.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private static void createQuote() {
		try {
			String url = "http://"+server+"/digitplusservices/createQuote?isUserSpecialDiscountOpted=false&isDownloadQuote=true&userId=35327650&sourceType=20101motor";
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept", "application/json, text/plain, */*");
			con.setRequestProperty("content-type", "Application/json");
			con.setRequestProperty("Authorization", "Bearer " + accessToken);
			con.setRequestProperty("clientAddress", "192.168.50.35");
			

			String urlParameters;
			Object jsonObject = null;
			try (FileReader reader = new FileReader(
					ResourceHelper.getBaseResourcePath() + "json/CreateQuote_"+vehicleType+".json")) {
				// Read JSON file
				JSONParser jsonParser = new JSONParser();
				jsonObject = jsonParser.parse(reader);

				// JSONArray employeeList = (JSONArray) jsonObj;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			urlParameters = jsonObject.toString();
			Random rndm = new Random();
			int randomNumber1 = rndm.nextInt(9999) + 1;
			regNumber = "KA01AA" + String.format("%04d", randomNumber1);
			randomNumber =""+ (rndm.nextInt(999999999) + 1)+(rndm.nextInt(999999999) + 1);
			urlParameters = urlParameters.replaceAll("<licensePlateNumber>", regNumber);
			urlParameters = urlParameters.replaceAll("<vehicleIdentificationNumber>", randomNumber);
			urlParameters = urlParameters.replaceAll("<engineNumber>", randomNumber);
			
			
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + urlParameters);
			System.out.println("Response Code : " + responseCode);
			System.out.println("Create Quote Response : " +con.getResponseMessage());
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());

			Object jsonObj = new JSONParser().parse(response.toString());

			// typecasting obj to JSONObject
			JSONObject jo = (JSONObject) jsonObj;
			applicationId = jo.get("applicationId").toString();
			policyNumber = jo.get("policyNumber").toString();
			com.utilities.Library.staticMap.put("PolicyNumber", policyNumber);
			//System.out.println("Application Id : " + applicationId);
			System.out.println("Policy Number : " + policyNumber);

		} catch (MalformedURLException me) {
			me.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	private static void issuePolicy() {
		try {
			String url = "http://"+server+"/digitplusservices/issueContract/" + applicationId
					+ "?&userId=35327650&sourceType=20101motor&policyNumber=" + policyNumber;
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept", "application/json, text/plain, */*");
			con.setRequestProperty("content-type", "Application/json");
			con.setRequestProperty("Authorization", "Bearer " + accessToken);
			con.setRequestProperty("clientAddress", "192.168.50.35");
			
			String urlParameters = "";
			Object jsonObject = null;
			try (FileReader reader = new FileReader(
					ResourceHelper.getBaseResourcePath() + "json/IssuePolicy_"+vehicleType+".json")) {
				// Read JSON file
				JSONParser jsonParser = new JSONParser();
				jsonObject = jsonParser.parse(reader);

				// JSONArray employeeList = (JSONArray) jsonObj;
				System.out.println(jsonObject);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			urlParameters = jsonObject.toString();
			urlParameters = urlParameters.replaceAll("<applicationId>", applicationId);
			urlParameters = urlParameters.replaceAll("<policyNumber>", policyNumber);
			urlParameters = urlParameters.replaceAll("<licensePlateNumber>", regNumber);
			urlParameters = urlParameters.replaceAll("<vehicleIdentificationNumber>", randomNumber);
			urlParameters = urlParameters.replaceAll("<engineNumber>", randomNumber);
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			//System.out.println("Post parameters : " + urlParameters);
			System.out.println("Response Code : " + responseCode);
			//System.out.println("Response Message : " + con.getResponseMessage());

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			System.out.println("Response : "+response.toString());
		} catch (MalformedURLException me) {
			me.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private static void getPreInspectionLink() throws Exception {

		try {
			String url = "https://"+server+"/digitplusservices/getPreInspectionLinkActivityResponse?PolicyNumber="
					+ policyNumber;
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("GET");
			con.setRequestProperty("Accept", "application/json, text/plain, */*");
			con.setRequestProperty("content-type", "Application/json");
			con.setRequestProperty("Authorization", "Bearer " + accessToken);

			// con.setDoOutput(true);

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
			System.out.println("Response Message : " + con.getResponseMessage());

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());

			JSONObject jsonArray = (JSONObject)new JSONParser().parse(response.toString());
			JSONArray linkArray = (JSONArray) jsonArray.get("receivedActivities");
			JSONObject linkObj = (JSONObject) linkArray.get(0);
			link = (String) linkObj.get("value");
			System.out.println("Link : " + linkObj.get("value"));
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}

	}
}
