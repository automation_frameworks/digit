package com.agent.motor.api;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.cucumber.framework.utility.ResourceHelper;
import com.utilities.Library;
import com.utilities.PropertyFileReader;

public class ApproveInspection {

	static String URL_UAT = "uat-digitcarenew.godigit.com";
	static String URL_PREPROD = "preprod-digitcarenew-c2.godigit.com";
	static String server;
	static Long activityId;

	public static void main(String[] args) throws Exception {
		ApproveInspection app = new ApproveInspection();
		try {
			app.approvePreInspection();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void approvePreInspection() throws Exception {
		try {
			PropertyFileReader prop = new PropertyFileReader();
			if (prop.getEnvironment().contains("preprod"))
				server = URL_PREPROD;
			else if (prop.getEnvironment().contains("uat"))
				server = URL_UAT;
			else
				throw new Exception(
						"The environment defined in the config file is not recognised : " + prop.getEnvironment());
			getActivityId();
			updateSurveyComments();
		} catch (Exception e) {
			throw new Exception("Unable to complete the preinspection. " + e.getMessage());
		}
	}

	private static void getActivityId() throws Exception {
		try {
			String url = "https://" + server
					+ "/DigitCareNew/getpageactivites?ActivityTypeId=5,26,49,50,51,30&AgentId=&ClaimNumber=&CustomerEmail=&CustomerMobile=&CustomerName=&PolicyNumber="+Library.staticMap.get("Policy Number")+"&ProductId=&Status=&extRefNumber=&assetId=&pageNo=1&vinNumber=&memberId=&oemFlag=no";
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("GET");
			con.setRequestProperty("Accept", "application/json, text/plain, */*");
			con.setRequestProperty("content-type", "application/x-www-form-urlencoded");
			con.setRequestProperty("Authorization",
					"Basic 1 eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2Vycy9Uek1Vb2NNRjRwIiwiZXhwIjoxNjA5MzUzMDAwLCJuYW1lIjoidGVjaC5kaWdpdGNhcmVAZ29kaWdpdC5jb20iLCJwYXNzd29yZCI6IkE1NjVFMTA3MkU5MDQzQTg5QkU3QTg2RDNDQjE0MzE1Iiwic2NvcGUiOiJzZWxmLGdyb3Vwcy9hZG1pbnMiLCJsb2dnZWRJblRpbWUiOjE1ODMyMTczNjY0MjB9.573GVpjLB9iWTFXlXlV4-GTgFyd2owfLTUcTuY2Z01A");

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			System.out.println("Response : " + response.toString());
			if (200 != responseCode)
				throw new Exception("Failed to get the activity Id through service : " + response);
			// print result
			

			// typecasting obj to JSONObject
			JSONArray linkArray = (JSONArray) new JSONParser().parse(response.toString());
			JSONObject linkObj = (JSONObject) linkArray.get(0);
			activityId = (Long) linkObj.get("activityID");
			System.out.println("Access Token : " + activityId);
		} catch (MalformedURLException me) {
			me.printStackTrace();
			throw new Exception("Failed to get the activity Id through service : " + me.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception("Failed to get the activity Id through service : " + e.getMessage());
		} catch (ParseException e) {
			e.printStackTrace();
			throw new Exception("Failed to get the activity Id through service : " + e.getMessage());
		}
	}

	private void updateSurveyComments() throws Exception {

		try {
			String url = "https://" + server + "/DigitCareNew/save/surveycomments";
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept", "application/json, text/plain, */*");
			con.setRequestProperty("content-type", "Application/json");
			con.setRequestProperty("Authorization",
					"Basic 1 eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2Vycy9Uek1Vb2NNRjRwIiwiZXhwIjoxNjA5MzUzMDAwLCJuYW1lIjoidGVjaC5kaWdpdGNhcmVAZ29kaWdpdC5jb20iLCJwYXNzd29yZCI6IkE1NjVFMTA3MkU5MDQzQTg5QkU3QTg2RDNDQjE0MzE1Iiwic2NvcGUiOiJzZWxmLGdyb3Vwcy9hZG1pbnMiLCJsb2dnZWRJblRpbWUiOjE1ODMyMTczNjY0MjB9.573GVpjLB9iWTFXlXlV4-GTgFyd2owfLTUcTuY2Z01A");

			String urlParameters;
			Object jsonObject = null;
			try (FileReader reader = new FileReader(
					ResourceHelper.getBaseResourcePath() + "json/UpdateSurveyComments.json")) {
				// Read JSON file
				JSONParser jsonParser = new JSONParser();
				jsonObject = jsonParser.parse(reader);

				// JSONArray employeeList = (JSONArray) jsonObj;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			urlParameters = jsonObject.toString();
			urlParameters = urlParameters.replaceAll("<activityId>", "" + activityId);
			// urlParameters = urlParameters.replaceAll("<policyNumber>", "D300006656");

			urlParameters = urlParameters.replaceAll("<policyNumber>", Library.staticMap.get("Policy Number"));
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + urlParameters);
			System.out.println("Response Code : " + responseCode);
			System.out.println("Survey Comments Response : " + con.getResponseMessage());
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			System.out.println("Response : "+response.toString());
			if (200 != responseCode)
				throw new Exception("Failed to update the survey comments through service : " + response);
			// print result
			

		} catch (MalformedURLException me) {
			me.printStackTrace();
			throw new Exception("Failed to get the survey comments through service : " + me.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception("Failed to get the survey comments through service : " + e.getMessage());
		} catch (ParseException e) {
			e.printStackTrace();
			throw new Exception("Failed to get the survey comments through service : " + e.getMessage());
		}

	}

}
