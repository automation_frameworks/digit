package com.agent.motor.api;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.cucumber.framework.utility.ResourceHelper;

import pom.mobile.preinspection.UploadPhotos;

public class GCV_3W {

	static String accessToken, applicationId, policyNumber, link,regNumber,randomNumber;

	public static void main(String args[]) {
		grant();
		createQuote();
		issuePolicy();
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		getPreInspectionLink();

	}

	public static void createPolicyAndInspect() throws InterruptedException {
		grant();
		createQuote();
		issuePolicy();
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		getPreInspectionLink();
		UploadPhotos.uploadImages(link);

	}

	private static void grant() {
		try {
			String url = "http://uat-plus.godigit.com/digitplusservices/oauth/grant";
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept", "application/json, text/plain, */*");
			con.setRequestProperty("content-type", "application/x-www-form-urlencoded");

			String urlParameters = "username=35327650&password=digit123";

			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + urlParameters);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());

			Object jsonObj = new JSONParser().parse(response.toString());

			// typecasting obj to JSONObject
			JSONObject jo = (JSONObject) jsonObj;
			System.out.println("Access Token : " + jo.get("accessToken"));
			accessToken = jo.get("accessToken").toString();
		} catch (MalformedURLException me) {
			me.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void createQuote() {

		try {
			String url = "https://uat-plus.godigit.com/digitplusservices/createQuote?isUserSpecialDiscountOpted=false&isDownloadQuote=true&userId=35327650&sourceType=20101motor";
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept", "application/json, text/plain, */*");
			con.setRequestProperty("content-type", "Application/json");
			con.setRequestProperty("Authorization", "Bearer " + accessToken);

			String urlParameters = "username=35327650&password=digit123";
			System.out
					.println("Resource Path : " + ResourceHelper.getBaseResourcePath() + "json/CreateQuote_GCV3W.json");
			Object jsonObject = null;
			try (FileReader reader = new FileReader(
					ResourceHelper.getBaseResourcePath() + "json/CreateQuote_GCV3W.json")) {
				// Read JSON file
				JSONParser jsonParser = new JSONParser();
				jsonObject = jsonParser.parse(reader);

				// JSONArray employeeList = (JSONArray) jsonObj;
				System.out.println(jsonObject);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			urlParameters = jsonObject.toString();
			Random rndm = new Random();
			int randomNumber1 = rndm.nextInt(9999) + 1;
			regNumber = "KA01AA" + String.format("%04d", randomNumber1);
			randomNumber =""+ (rndm.nextInt(999999999) + 1)+(rndm.nextInt(999999999) + 1);
			urlParameters = urlParameters.replaceAll("<licensePlateNumber>", regNumber);
			urlParameters = urlParameters.replaceAll("<vehicleIdentificationNumber>", randomNumber);
			urlParameters = urlParameters.replaceAll("<engineNumber>", randomNumber);
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + urlParameters);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());

			Object jsonObj = new JSONParser().parse(response.toString());

			// typecasting obj to JSONObject
			JSONObject jo = (JSONObject) jsonObj;
			applicationId = jo.get("applicationId").toString();
			policyNumber = jo.get("policyNumber").toString();
			com.utilities.Library.staticMap.put("PolicyNumber", policyNumber);
			//System.out.println("Application Id : " + applicationId);
			System.out.println("Policy Number : " + policyNumber);

		} catch (MalformedURLException me) {
			me.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void updateQuote() {

		try {
			String url = "https://uat-plus.godigit.com/digitplusservices/updateQuote/" + applicationId
					+ "?isUserSpecialDiscountOpted=false&isDownloadQuote=false&userId=35327650&sourceType=20101motor&policyNumber="
					+ policyNumber;
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept", "application/json, text/plain, */*");
			con.setRequestProperty("content-type", "Application/json");
			con.setRequestProperty("Authorization", "Bearer " + accessToken);

			String urlParameters = "";
			System.out
					.println("Resource Path : " + ResourceHelper.getBaseResourcePath() + "json/UpdateQuote_GCV3W.json");
			Object jsonObject = null;
			try (FileReader reader = new FileReader(
					ResourceHelper.getBaseResourcePath() + "json/UpdateQuote_GCV3W.json")) {
				// Read JSON file
				JSONParser jsonParser = new JSONParser();
				jsonObject = jsonParser.parse(reader);

				// JSONArray employeeList = (JSONArray) jsonObj;
				System.out.println(jsonObject);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			urlParameters = jsonObject.toString();
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + urlParameters);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());

			Object jsonObj = new JSONParser().parse(response.toString());

			// typecasting obj to JSONObject
			JSONObject jo = (JSONObject) jsonObj;
			// System.out.println("Application Id : "+jo.get("applicationId"));
			// System.out.println("Policy Number : "+jo.get("policyNumber"));
		} catch (MalformedURLException me) {
			me.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void issuePolicy() {
		try {
			String url = "https://uat-plus.godigit.com/digitplusservices/issueContract/" + applicationId
					+ "?&userId=35327650&sourceType=20101motor&policyNumber=" + policyNumber;
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept", "application/json, text/plain, */*");
			con.setRequestProperty("content-type", "Application/json");
			con.setRequestProperty("Authorization", "Bearer " + accessToken);

			String urlParameters = "";
			System.out
					.println("Resource Path : " + ResourceHelper.getBaseResourcePath() + "json/IssuePolicy_GCV3W.json");
			Object jsonObject = null;
			try (FileReader reader = new FileReader(
					ResourceHelper.getBaseResourcePath() + "json/IssuePolicy_GCV3W.json")) {
				// Read JSON file
				JSONParser jsonParser = new JSONParser();
				jsonObject = jsonParser.parse(reader);

				// JSONArray employeeList = (JSONArray) jsonObj;
				System.out.println(jsonObject);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			urlParameters = jsonObject.toString();
			urlParameters = urlParameters.replaceAll("<applicationId>", applicationId);
			urlParameters = urlParameters.replaceAll("<policyNumber>", policyNumber);
			urlParameters = urlParameters.replaceAll("<licensePlateNumber>", regNumber);
			urlParameters = urlParameters.replaceAll("<vehicleIdentificationNumber>", randomNumber);
			urlParameters = urlParameters.replaceAll("<engineNumber>", randomNumber);
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + urlParameters);
			System.out.println("Response Code : " + responseCode);
			System.out.println("Response Message : " + con.getResponseMessage());

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());

			Object jsonObj = new JSONParser().parse(response.toString());

			// typecasting obj to JSONObject
			JSONObject jo = (JSONObject) jsonObj;
			// System.out.println("Application Id : "+jo.get("applicationId"));
			// System.out.println("Policy Number : "+jo.get("policyNumber"));
		} catch (MalformedURLException me) {
			me.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void getPreInspectionLink() {

		try {
			String url = "https://uat-plus.godigit.com/digitplusservices/getPreInspectionLinkResponse?ActivityTypeId=30&PolicyNumber="
					+ policyNumber;
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

			// add reuqest header
			con.setRequestMethod("GET");
			con.setRequestProperty("Accept", "application/json, text/plain, */*");
			con.setRequestProperty("content-type", "Application/json");
			con.setRequestProperty("Authorization", "Bearer " + accessToken);

			// con.setDoOutput(true);

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
			System.out.println("Response Message : " + con.getResponseMessage());

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());

			JSONArray jsonArray = (JSONArray) new JSONParser().parse(response.toString());
			JSONObject receivedActivities = (JSONObject) jsonArray.get(0);
			JSONArray linkArray = (JSONArray) receivedActivities.get("receivedActivities");
			JSONObject linkObj = (JSONObject) linkArray.get(0);
			link = (String) linkObj.get("value");
			System.out.println("Link : " + linkObj.get("value"));
		} catch (MalformedURLException me) {
			me.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
