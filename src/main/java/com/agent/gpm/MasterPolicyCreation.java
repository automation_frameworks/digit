package com.agent.gpm;

import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.cucumber.framework.utility.ResourceHelper;

public class MasterPolicyCreation {

	@FindBy(xpath = "//button[contains(text(),'Search')]")
	private WebElement btnSearch;

	@FindBy(xpath = "//button[contains(text(),'New')]")
	private WebElement btnNew;

	@FindBy(xpath = "//button[contains(text(),'Proceed')]")
	private WebElement btnProceed;

	@FindBy(xpath = "//label[contains(text(),'Search By')]/../following-sibling::div//mat-select")
	private WebElement drpSearchBy;

	@FindBy(xpath = "//label[contains(text(),'Search By')]/../following-sibling::div[2]//input")
	private WebElement txtSearchBy;

	@FindBy(xpath = "//label[contains(text(),'Type of Document')]/../following-sibling::div//mat-select")
	private WebElement drpTypeOfDocument;

	@FindBy(xpath = "//label[contains(text(),'Document No.')]/../following-sibling::div//input")
	private WebElement txtDocumentNo;

	@FindBy(xpath = "//label[contains(text(),'Company Name(MPH)')]/../following-sibling::div//input")
	private WebElement txtCompanyName;

	@FindBy(xpath = "//label[contains(text(),'IMD Code')]/../following-sibling::div//input")
	private WebElement txtIMDCode;

	@FindBy(xpath = "//label[contains(text(),'Address')]/../following-sibling::div//textarea")
	private WebElement txtAddress;

	@FindBy(xpath = "//label[contains(text(),'Pincode')]/../following-sibling::div//input")
	private WebElement txtPincode;

	@FindBy(xpath = "//label[contains(text(),'Mobile')]/../following-sibling::div//input")
	private WebElement txtMobile;

	@FindBy(xpath = "//label[contains(text(),'Email ID')]/../following-sibling::div//input")
	private WebElement txtEmailId;

	@FindBy(xpath = "//label[contains(text(),'RM Email ID')]/../following-sibling::div//input")
	private WebElement txtRMEmailId;

	@FindBy(xpath = "//label[contains(text(),'Who will pay the premium?')]/../following-sibling::div//mat-select")
	private WebElement drpWhoWillPayPremium;

	@FindBy(xpath = "//label[contains(text(),'Max Sum Insured (INR)')]/../following-sibling::div//mat-select")
	private WebElement drpmaxSumInsured;

	@FindBy(xpath = "//label[contains(text(),'Count of Lives')]/../following-sibling::div//input")
	private WebElement txtCountOfLives;

	@FindBy(xpath = "//label[contains(text(),'Held cover letter number')]/../following-sibling::div//input")
	private WebElement txtHeldCoverLetterNumber;

	@FindBy(xpath = "//label[contains(text(),'Remarks')]/../following-sibling::div//input")
	private WebElement txtRemarks;

	@FindBy(xpath = "//label[contains(text(),'DigitDesk Ticket Number')]/../following-sibling::div//input")
	private WebElement txtDigitDeskNumber;

	@FindBy(xpath = "//label[contains(text(),'Terms of the proposal')]/../following-sibling::div//mat-select")
	private WebElement drpTermsoftheProposal;

	@FindBy(xpath = "//label[contains(text(),'Premium loading')]/../following-sibling::div//input")
	private WebElement txtPremiumLoading;

	@FindBy(xpath = "//label[contains(text(),'Proposal Type')]/../following-sibling::div//mat-select")
	private WebElement drpProposalType;

	@FindBy(xpath = "//button[@routerlink='/masterpolicy']")
	private WebElement btnUploadDocument;

	@FindBy(xpath = "//input[@id='fileUploader']")
	private WebElement btnChooseFile;

	@FindBy(xpath = "//label[contains(text(),'Total Premium')]/../following-sibling::div//input")
	private WebElement txtTotalPremium;

	@FindBy(xpath = "//label[contains(text(),'Total number of lives')]/../following-sibling::div//input")
	private WebElement txtTotalNumberOfLives;

	public com.utilities.Library library = new com.utilities.Library();
	static HashMap<String, String> hmap = new LinkedHashMap<>();

	public void enterCompanyDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "typeofdocument":
					drpTypeOfDocument.click();
					Thread.sleep(1000);
					library.getDriver().findElement(By
							.xpath("//span[contains(text(),'" + hmap.get(key).trim() + "')][@class='mat-option-text']"))
							.click();

					// library.selectDropDownValue(drpTypeOfDocument, hmap.get(key));
					break;
				case "documentno":
					if (hmap.get("Type of Document").equals("PAN"))
						txtDocumentNo.sendKeys(library.getRandomValue("[A | 5]") + library.getRandomValue("[N | 4]")
								+ library.getRandomValue("[A | 1]"));
					else
						txtDocumentNo.sendKeys("29" + library.getRandomValue("[A | 5]")
								+ library.getRandomValue("[N | 4]") + library.getRandomValue("[A | 1]") + "1Z6");
					break;
				case "companyname":
					txtCompanyName.sendKeys(library.getRandomValue("[A | 10]"));
					break;
				case "address":
					txtAddress.sendKeys(library.getRandomValue("[A | 10]"));
					break;
				case "imdcode":
					txtIMDCode.sendKeys(hmap.get(key));
					break;
				case "pincode":
					txtPincode.sendKeys("560001");
					break;
				case "mobile":
					txtMobile.sendKeys(library.getRandomValue("[N | 10]"));
					break;
				case "emailid":
					txtEmailId.sendKeys("test@godigit.com");
					break;
				case "rmemailid":
					txtRMEmailId.sendKeys("test@godigit.com");
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the company details : " + e.getMessage());
		}
	}

	public void enterPolicyDetails(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "whowillpaythepremium":
					drpWhoWillPayPremium.click();
					Thread.sleep(1000);
					library.getDriver().findElement(By
							.xpath("//span[contains(text(),'" + hmap.get(key).trim() + "')][@class='mat-option-text']"))
							.click();
					break;
				case "maxsuminsured":
					drpmaxSumInsured.click();
					Thread.sleep(1000);
					library.getDriver().findElement(By
							.xpath("//span[contains(text(),'" + hmap.get(key).trim() + "')][@class='mat-option-text']"))
							.click();
					break;
				case "countoflives":
					txtCountOfLives.sendKeys(hmap.get(key));
					break;
				case "heldcoverletternumber":
					txtHeldCoverLetterNumber.sendKeys(library.getRandomValue("[AN | 10]"));
					break;
				case "remarks":
					txtRemarks.sendKeys(library.getRandomValue("[AN | 10]"));
					break;
				case "digitdesknumber":
					txtDigitDeskNumber.sendKeys(library.getRandomValue("[AN | 10]"));
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the policy details : " + e.getMessage());
		}
	}

	public void enterSpecialConditions(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "termsoftheproposal":
					drpTermsoftheProposal.click();
					Thread.sleep(1000);
					library.getDriver().findElement(By
							.xpath("//span[contains(text(),'" + hmap.get(key).trim() + "')][@class='mat-option-text']"))
							.click();
					break;
				case "proposaltype":
					drpProposalType.click();
					Thread.sleep(1000);
					library.getDriver().findElement(By
							.xpath("//span[contains(text(),'" + hmap.get(key).trim() + "')][@class='mat-option-text']"))
							.click();
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the special conditions : " + e.getMessage());
		}
	}

	public void performAction(String action) throws Exception {
		try {
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "new":
				btnNew.click();
				break;
			case "search":
				btnSearch.click();
				break;
			case "proceed":
				Thread.sleep(1000);
				getFieldValues();
				library.scrollElementIntoView(btnProceed, library.getDriver());
				btnProceed.click();
				break;
			default:
				throw new Exception("Implementation for the action '" + action + "' is not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while performing action : " + e.getMessage());
		}

	}

	public void uploadMemberDataFile() throws Exception {
		try {
			System.out.println(Paths.get("").toAbsolutePath().toString() + "/src/main/resources/exceldata/COVID.xlsm");
			btnUploadDocument.click();
			Thread.sleep(10000);
			Actions action = new Actions(library.getDriver());
			action.sendKeys(Keys.TAB).perform();
			System.out.println("Entered TAB");
			Thread.sleep(1000);
			action.sendKeys(Keys.ENTER).perform();

			String filename = Paths.get("").toAbsolutePath().toString()
					+ "\\src\\main\\resources\\exceldata\\COVID.xlsm".replace("/", "\\");
			Runtime.getRuntime()
					.exec(ResourceHelper.getResourcePath("driver/ParameterisedFileUpload.exe") + " " + filename);
			Thread.sleep(5000);
			Alert alert = library.getDriver().switchTo().alert();
			alert.accept();
			System.out.println("Document is uploaded");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while uploading document : " + e.getMessage());
		}
	}

//	public void verifyErrorMessage(String message) throws Exception {
//		try {
//			library.verifyErrorMessageDisplayed(message);
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new Exception("Error while verifying the error message : " + e.getMessage());
//		}
//	}
//
//	public void verifySuccessMessage(String message) throws Exception {
//		try {
//			library.verifySuccessMessageDisplayed(message);
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new Exception("Error while verifying the success message : " + e.getMessage());
//		}
//	}

	public void verifyDropDownValues(String details) throws Exception {
		try {
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "typeofdocument":
					drpTypeOfDocument.click();
					Thread.sleep(1000);
					verifyDropDown(hmap.get(key));
					drpTypeOfDocument.click();
					break;
				case "whowillpaythepremium":
					drpWhoWillPayPremium.click();
					Thread.sleep(1000);
					verifyDropDown(hmap.get(key));
					Thread.sleep(1000);
					txtMobile.sendKeys("");
					break;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while verifying the drop down values : " + e.getMessage());
		}
	}

	private void getFieldValues() {
		try {

			hmap.put("Type of Document", library.getDriver().findElement(By.xpath(
					"//label[contains(text(),'Type of Document')]/../following-sibling::div//mat-select//span/span"))
					.getText());
			hmap.put("Document Number", txtDocumentNo.getAttribute("value"));
			hmap.put("Company Name", txtCompanyName.getAttribute("value"));
			hmap.put("IMD Code", txtIMDCode.getAttribute("value"));
			hmap.put("Address", txtAddress.getAttribute("value"));
			hmap.put("Pincode", txtPincode.getAttribute("value"));
			hmap.put("City",
					library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'City')]/../following-sibling::div//input"))
							.getAttribute("value"));
			hmap.put("State",
					library.getDriver()
							.findElement(By.xpath("//label[contains(text(),'State')]/../following-sibling::div//input"))
							.getAttribute("value"));
			hmap.put("Mobile", txtMobile.getAttribute("value"));
			hmap.put("Email ID", txtEmailId.getAttribute("value"));
			hmap.put("RM Email ID", txtRMEmailId.getAttribute("value"));
			hmap.put("Premium Payer", library.getDriver().findElement(By.xpath(
					"//label[contains(text(),'Who will pay the premium?')]/../following-sibling::div//mat-select//span/span"))
					.getText());
			hmap.put("Start Date",
					library.getDriver().findElement(By.xpath(
							"//label[contains(text(),'Period of Insurance - From')]/../following-sibling::div//input"))
							.getAttribute("value"));
			hmap.put("End Date",
					library.getDriver().findElement(By.xpath(
							"//label[contains(text(),'Period of Insurance - To')]/../following-sibling::div//input"))
							.getAttribute("value"));
			hmap.put("Maximum Sum Insured", library.getDriver().findElement(By.xpath(
					"//label[contains(text(),'Max Sum Insured (INR)')]/../following-sibling::div//mat-select//span/span"))
					.getText());
			hmap.put("Count of Lives", txtCountOfLives.getAttribute("value"));
			hmap.put("Held Cover Letter Number", txtHeldCoverLetterNumber.getAttribute("value"));
			hmap.put("Remarks", txtRemarks.getAttribute("value"));
			try {
				hmap.put("Digit Desk Number", txtDigitDeskNumber.getAttribute("value"));
			} catch (NoSuchElementException ne) {
				hmap.put("Digit Desk Number", "");
			}
			hmap.put("Terms of Proposal", library.getDriver().findElement(By.xpath(
					"//label[contains(text(),'Terms of the proposal')]/../following-sibling::div//mat-select//span/span"))
					.getText());
			hmap.put("Proposal Type", library.getDriver().findElement(By.xpath(
					"//label[contains(text(),'Proposal Type')]/../following-sibling::div//mat-select//span/span"))
					.getText());
			hmap.put("Premium Loading", library.getDriver()
					.findElement(
							By.xpath("//label[contains(text(),'Premium loading')]/../following-sibling::div//input"))
					.getAttribute("value"));
			for (String temp : hmap.keySet()) {
				System.out.println(temp + "==> " + hmap.get(temp));
			}
		} catch (Exception e) {

		}
	}

	private void verifyDropDown(String values) throws Exception {
		try {
			List<WebElement> lstWebElements = library.getDriver().findElements(By
					.xpath("//div[@class='mat-select-content ng-trigger ng-trigger-fadeInContent']//mat-option/span"));
			List<String> options = new LinkedList<>();
			List<String> expected = Arrays.asList(values.split(","));

			for (WebElement element : lstWebElements)
				options.add(element.getText().trim());

			if (!expected.equals(options))
				throw new Exception("Expected : " + values + " . Actual : " + options);

			library.getDriver()
					.findElement(By.xpath(
							"//div[@class='mat-select-content ng-trigger ng-trigger-fadeInContent']//mat-option/span"))
					.click();

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public void verifyPrincipleValues() throws Exception {
		try {
			List<String> lstError = new LinkedList<>();
			Connection connection = null;
			// Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(
					"jdbc:postgresql://digit-uat01.cpmhro02yml1.ap-south-1.rds.amazonaws.com:5432/digit_uat",
					"uat_insuract", "KTMHPA2Gab");
			String query = "select * from digit_bots.t_covid_principal_Details where document_no = '"
					+ hmap.get("Document Number") + "'";

			Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet resultSet = statement.executeQuery(query);

			HashMap<String, String> dbMap = new LinkedHashMap<>();
			ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

			int columnCount = resultSetMetaData.getColumnCount();

			while (resultSet.next()) {
				for (int i = 1; i <= columnCount; i++) {
					String columnValue = resultSet.getString(i);
					dbMap.put(resultSetMetaData.getColumnName(i), columnValue);
				}
			}

			for (String uiValue : hmap.keySet()) {
				switch (uiValue.toLowerCase().replaceAll(" ", "")) {
				case "typeofdocument":
					if (!hmap.get("Type of Document").equals(dbMap.get("document_type")))
						lstError.add("Type of Document : UI ==> " + hmap.get("Type of Document") + " | DB ==> "
								+ dbMap.get("document_type"));
					break;
				case "documentnumber":
					if (!hmap.get("Document Number").equals(dbMap.get("document_no")))
						lstError.add("Document Number : UI ==> " + hmap.get("Document Number") + " | DB ==> "
								+ dbMap.get("document_no"));
					break;
				case "companyname":
					if (!hmap.get("Company Name").equals(dbMap.get("group_manager_name")))
						lstError.add("Company Name : UI ==> " + hmap.get("Company Name") + " | DB ==> "
								+ dbMap.get("group_manager_name"));
					break;
				case "imdcode":
					if (!hmap.get("IMD Code").equals(dbMap.get("imd_code")))
						lstError.add(
								"IMD Code : UI ==> " + hmap.get("IMD Code") + " | DB ==> " + dbMap.get("imd_code"));
					break;
				case "address":
					if (!hmap.get("Address").equals(dbMap.get("address")))
						lstError.add("Address : UI ==> " + hmap.get("Address") + " | DB ==> " + dbMap.get("address"));
					break;
				case "pincode":
					if (!hmap.get("Pincode").equals(dbMap.get("pincode")))
						lstError.add("Pincode : UI ==> " + hmap.get("Pincode") + " | DB ==> " + dbMap.get("pincode"));
					break;
				case "mobile":
					if (!hmap.get("Mobile").equals(dbMap.get("mobile")))
						lstError.add("Mobile : UI ==> " + hmap.get("Mobile") + " | DB ==> " + dbMap.get("mobile"));
					break;
				case "emailid":
					if (!hmap.get("Email ID").equals(dbMap.get("emailid")))
						lstError.add("Email ID : UI ==> " + hmap.get("Email ID") + " | DB ==> " + dbMap.get("emailid"));
					break;
				case "rmemailid":
					if (!hmap.get("RM Email ID").equals(dbMap.get("rm_email")))
						lstError.add("RM Email ID : UI ==> " + hmap.get("RM Email ID") + " | DB ==> "
								+ dbMap.get("rm_email"));
					break;
				case "premiumpayer":
					String expectingPrem = "";
					if (hmap.get("Premium Payer").equals("Send payment link to MPH"))
						expectingPrem = "O";
					else
						expectingPrem = "A";
					if (!expectingPrem.equals(dbMap.get("payment_mode")))
						lstError.add(
								"Premium Payer : UI ==> " + expectingPrem + " | DB ==> " + dbMap.get("payment_mode"));
					break;
				case "startdate":
					SimpleDateFormat inFormat = new SimpleDateFormat("dd/MM/yyyy");
					SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd");
					String expectingStart = outFormat.format(inFormat.parse(hmap.get("Start Date")));
					if (!expectingStart.equals(dbMap.get("period_from")))
						lstError.add("Start Date : UI ==> " + expectingStart + " | DB ==> " + dbMap.get("period_from"));
					break;
				case "enddate":
					SimpleDateFormat inFormat2 = new SimpleDateFormat("dd/MM/yyyy");
					SimpleDateFormat outFormat2 = new SimpleDateFormat("yyyy-MM-dd");
					String expectingEnd = outFormat2.format(inFormat2.parse(hmap.get("End Date")));
					if (!expectingEnd.equals(dbMap.get("period_to")))
						lstError.add("End Date : UI ==> " + expectingEnd + " | DB ==> " + dbMap.get("period_to"));
					break;
				case "maximumsuminsured":
					if (!hmap.get("Maximum Sum Insured").equals(dbMap.get("si_options")))
						lstError.add("Maximum Sum Insured : UI ==> " + hmap.get("Maximum Sum Insured") + " | DB ==> "
								+ dbMap.get("si_options"));
					break;
				case "countoflives":
					if (!hmap.get("Count of Lives").equals(dbMap.get("no_of_lives")))
						lstError.add("Count of Lives : UI ==> " + hmap.get("Count of Lives") + " | DB ==> "
								+ dbMap.get("no_of_lives"));
					break;
				case "heldcoverletternumber":
					if (!hmap.get("Held Cover Letter Number").equals(dbMap.get("rhl_number")))
						lstError.add("Held Cover Letter Number : UI ==> " + hmap.get("Held Cover Letter Number")
								+ " | DB ==> " + dbMap.get("rhl_number"));
					break;
				case "remarks":
					if (!hmap.get("Remarks").equals(dbMap.get("remarks")))
						lstError.add("Remarks : UI ==> " + hmap.get("Remarks") + " | DB ==> " + dbMap.get("remarks"));
					break;
				case "termsofproposal":
					String expectingProposal;
					if (hmap.get("Terms of Proposal").equals("Standard"))
						expectingProposal = "S";
					else
						expectingProposal = "A";
					if (!expectingProposal.equals(dbMap.get("terms_of_proposal")))
						lstError.add("Terms of Proposal : UI ==> " + expectingProposal + " | DB ==> "
								+ dbMap.get("terms_of_proposal"));
					break;
				case "proposaltype":
					String expectingProposalType;
					if (hmap.get("Proposal Type").equals("Employer-Employee"))
						expectingProposalType = "Y";
					else
						expectingProposalType = "N";
					if (!expectingProposalType.equals(dbMap.get("emp_employer_relationship")))
						lstError.add("Proposal Type : UI ==> " + expectingProposalType + " | DB ==> "
								+ dbMap.get("emp_employer_relationship"));
					break;
				case "digitdesknumber":
					if (!hmap.get("Digit Desk Number").equals(dbMap.get("digit_desk_no")))
						lstError.add("Digit Desk Number : UI ==> " + hmap.get("Digit Desk Number") + " | DB ==> "
								+ dbMap.get("digit_desk_no"));
					break;
				case "loading":
					if (!hmap.get("Loading").equals(dbMap.get("loading")))
						lstError.add("Loading : UI ==> " + hmap.get("Loading") + " | DB ==> " + dbMap.get("loading"));
					break;
				case "state":
					if (!hmap.get("State").equals(dbMap.get("state")))
						lstError.add("State : UI ==> " + hmap.get("State") + " | DB ==> " + dbMap.get("state"));
					break;
				case "city":
					if (!hmap.get("City").equals(dbMap.get("city")))
						lstError.add("City : UI ==> " + hmap.get("City") + " | DB ==> " + dbMap.get("city"));
					break;

				}
			}
			if (!lstError.isEmpty())
				throw new Exception("There are mismatches between UI and DB : " + lstError);
			System.out.println(lstError);
			statement.close();
			resultSet.close();
			System.out.println(dbMap);
			verifyDistributorValues();

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public void verifyDistributorValues() throws Exception {
		try {
			List<String> lstError = new LinkedList<>();
			Connection connection = null;
			// Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(
					"jdbc:postgresql://digit-uat01.cpmhro02yml1.ap-south-1.rds.amazonaws.com:5432/digit_uat",
					"uat_insuract", "KTMHPA2Gab");
			String query = "select * from digit_bots.t_covid_distributers where parent_distributorid in (select cast (group_manager_id as varchar) from digit_bots.t_covid_principal_Details where document_no = '"
					+ hmap.get("Document Number") + "')";

			Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet resultSet = statement.executeQuery(query);

			HashMap<String, String> dbMap = new LinkedHashMap<>();
			ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

			int columnCount = resultSetMetaData.getColumnCount();

			while (resultSet.next()) {
				for (int i = 1; i <= columnCount; i++) {
					String columnValue = resultSet.getString(i);
					dbMap.put(resultSetMetaData.getColumnName(i), columnValue);
				}
			}

			for (String uiValue : hmap.keySet()) {
			}
			if (!lstError.isEmpty())
				throw new Exception("There are mismatches between UI and DB : " + lstError);
			System.out.println(lstError);
			statement.close();
			resultSet.close();
			System.out.println(dbMap);

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

}
