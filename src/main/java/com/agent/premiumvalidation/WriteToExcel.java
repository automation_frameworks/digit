package com.agent.premiumvalidation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteToExcel {

	static String filePath;

	public static void main(String args[]) throws IOException {
		WriteToExcel write = new WriteToExcel();
		write.createResultSheet();
		HashMap<String, String> excelMap = new LinkedHashMap<String, String>();
		excelMap.put("S.No.", "1");
		excelMap.put("Breakddown Assistance", "₹131");
		excelMap.put("Tyre Protect", "₹207");
		excelMap.put("Parts Depreciation Protect", "₹4,230");
		excelMap.put("Consumable Cover", "₹277");
		excelMap.put("Engine and Gear Box Protect", "₹691");
		excelMap.put("Net Premium", "14,028");
		excelMap.put("Total Premium", "16,553");
		excelMap.put("Quote Number", "D400438364");
		write.writeExcelData(excelMap);
		excelMap.put("S.No.", "2");
		excelMap.put("Breakddown Assistance", "₹231");
		excelMap.put("Tyre Protect", "₹307");
		excelMap.put("Parts Depreciation Protect", "₹5,230");
		excelMap.put("Consumable Cover", "₹377");
		excelMap.put("Engine and Gear Box Protect", "₹791");
		excelMap.put("Net Premium", "24,028");
		excelMap.put("Total Premium", "26,553");
		excelMap.put("Quote Number", "D400438365");
		write.writeExcelData(excelMap);
	}

	public void writeExcelData(HashMap<String, String> excelMap) throws IOException {
		// Create blank workbook
		FileInputStream fis = new FileInputStream(filePath);
		XSSFWorkbook inputBook = new XSSFWorkbook(fis);
		XSSFSheet inputsheet = inputBook.getSheetAt(0);
		int lastRow = inputsheet.getLastRowNum();
		System.out.println(lastRow);
		// Create row object
		XSSFRow row;
		int rowid = lastRow+1;
		int cellid = 0;
		row = inputsheet.createRow(rowid++);
		for (String key : excelMap.keySet()) {
			Cell cell = row.createCell(cellid++);
			cell.setCellValue((String) excelMap.get(key));
		}
		fis.close();
		// Write the workbook in file system
		FileOutputStream out = new FileOutputStream(new File(filePath));

		inputBook.write(out);
		out.close();
		System.out.println("Writesheet.xlsx written successfully");
	}

	public void createResultSheet() {
		try {
			XSSFFont headerFont;
			CellStyle headerCellStyle;

			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat("yyyy_MMM_dd HH-mm-ss");

			// File excel = new File(reportOutputDirectory + "/Report" + ft.format(dNow) +
			// ".xlsx");
			filePath = Paths.get("").toAbsolutePath().toString()
					+ "/src/main/resources/exceldata/preprod/4W Test Cases_V4_Results_" + ft.format(dNow) + ".xlsx";
			XSSFWorkbook workBook = new XSSFWorkbook();
			FileOutputStream out = new FileOutputStream(new File(filePath));
			// XSSFWorkbook workBook = new XSSFWorkbook();
			ft = new SimpleDateFormat("yyyy_MMM_dd");
			XSSFSheet sheet = workBook.getSheet(ft.format(dNow));
			if (sheet == null)
				sheet = workBook.createSheet(ft.format(dNow));

			headerFont = workBook.createFont();
			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short) 12);
			headerFont.setColor(IndexedColors.ORANGE.getIndex());

			headerCellStyle = workBook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setFillBackgroundColor(IndexedColors.AQUA.getIndex());

			Row row = sheet.createRow(0);
			Cell cell = row.createCell(0);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("S No.");
			
			cell = row.createCell(1);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Quote Number");
			
			cell = row.createCell(2);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Motor Own Damage Premium");
			
			cell = row.createCell(3);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Net Premium");

			cell = row.createCell(4);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Total Premium");
			
			cell = row.createCell(5);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Digit Current Discount");

			cell = row.createCell(6);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Breakddown Assistance");

			cell = row.createCell(7);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Tyre Protect");

			cell = row.createCell(8);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Parts Depreciation Protect");

			cell = row.createCell(9);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Consumable Cover");

			cell = row.createCell(10);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Engine and Gear Box Protect");

			workBook.write(out);
			out.close();
			System.out.println("Writesheet.xlsx written successfully");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void createResultSheet2W() {
		try {
			XSSFFont headerFont;
			CellStyle headerCellStyle;

			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat("yyyy_MMM_dd HH-mm-ss");

			// File excel = new File(reportOutputDirectory + "/Report" + ft.format(dNow) +
			// ".xlsx");
			filePath = Paths.get("").toAbsolutePath().toString()
					+ "/src/main/resources/exceldata/2W Test Cases_V4_Results_" + ft.format(dNow) + ".xlsx";
			XSSFWorkbook workBook = new XSSFWorkbook();
			FileOutputStream out = new FileOutputStream(new File(filePath));
			// XSSFWorkbook workBook = new XSSFWorkbook();
			ft = new SimpleDateFormat("yyyy_MMM_dd");
			XSSFSheet sheet = workBook.getSheet(ft.format(dNow));
			if (sheet == null)
				sheet = workBook.createSheet(ft.format(dNow));

			headerFont = workBook.createFont();
			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short) 12);
			headerFont.setColor(IndexedColors.ORANGE.getIndex());

			headerCellStyle = workBook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setFillBackgroundColor(IndexedColors.AQUA.getIndex());

			Row row = sheet.createRow(0);
			Cell cell = row.createCell(0);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("S No.");
			
			cell = row.createCell(1);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("IDV");
			
			cell = row.createCell(2);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Quick Quote Motor Own Damage Premium");
			
			cell = row.createCell(3);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Quick Quote Net Premium");

			cell = row.createCell(4);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Quick Quote Total Premium");
			
			cell = row.createCell(5);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Digit Default Discount");
			
			cell = row.createCell(6);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Quote Number");
			
			cell = row.createCell(7);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Add On Net Premium");

			cell = row.createCell(8);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Add On Total Premium");

			cell = row.createCell(9);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Break Down Assistance");

			cell = row.createCell(10);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Tyre Protect");

			cell = row.createCell(11);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Parts Depreciation Protect");
			
			cell = row.createCell(12);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Consumable Cover");
			
			cell = row.createCell(13);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Engine and Gear Box Protect");
			
			cell = row.createCell(14);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Return To Invoice");

			workBook.write(out);
			out.close();
			System.out.println("Writesheet.xlsx written successfully");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void createLimitsResultSheet() {
		try {
			XSSFFont headerFont;
			CellStyle headerCellStyle;

			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat("yyyy_MMM_dd HH-mm-ss");

			// File excel = new File(reportOutputDirectory + "/Report" + ft.format(dNow) +
			// ".xlsx");
			filePath = Paths.get("").toAbsolutePath().toString()
					+ "/src/main/resources/exceldata/4W Test Cases_V4_Results_" + ft.format(dNow) + ".xlsx";
			XSSFWorkbook workBook = new XSSFWorkbook();
			FileOutputStream out = new FileOutputStream(new File(filePath));
			// XSSFWorkbook workBook = new XSSFWorkbook();
			ft = new SimpleDateFormat("yyyy_MMM_dd");
			XSSFSheet sheet = workBook.getSheet(ft.format(dNow));
			if (sheet == null)
				sheet = workBook.createSheet(ft.format(dNow));

			headerFont = workBook.createFont();
			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short) 12);
			headerFont.setColor(IndexedColors.ORANGE.getIndex());

			headerCellStyle = workBook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setFillBackgroundColor(IndexedColors.AQUA.getIndex());

			Row row = sheet.createRow(0);
			Cell cell = row.createCell(0);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("S No.");
			
			cell = row.createCell(1);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("CNG Min");
			
			cell = row.createCell(2);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("CNG Max");

			cell = row.createCell(3);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Electrical Min");
			
			cell = row.createCell(4);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Electrical Max");

			cell = row.createCell(5);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Non Electrical Min");

			cell = row.createCell(6);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Non Electrical Max");

			cell = row.createCell(7);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("IDV Min");

			cell = row.createCell(8);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("IDV Max");

			cell = row.createCell(9);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Quote Number");
			
			workBook.write(out);
			out.close();
			System.out.println("Writesheet.xlsx written successfully");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void createLimitsResultSheet2W() {
		try {
			XSSFFont headerFont;
			CellStyle headerCellStyle;

			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat("yyyy_MMM_dd HH-mm-ss");

			// File excel = new File(reportOutputDirectory + "/Report" + ft.format(dNow) +
			// ".xlsx");
			filePath = Paths.get("").toAbsolutePath().toString()
					+ "/src/main/resources/exceldata/2W Test Cases_V4_Results_" + ft.format(dNow) + ".xlsx";
			XSSFWorkbook workBook = new XSSFWorkbook();
			FileOutputStream out = new FileOutputStream(new File(filePath));
			// XSSFWorkbook workBook = new XSSFWorkbook();
			ft = new SimpleDateFormat("yyyy_MMM_dd");
			XSSFSheet sheet = workBook.getSheet(ft.format(dNow));
			if (sheet == null)
				sheet = workBook.createSheet(ft.format(dNow));

			headerFont = workBook.createFont();
			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short) 12);
			headerFont.setColor(IndexedColors.ORANGE.getIndex());

			headerCellStyle = workBook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setFillBackgroundColor(IndexedColors.AQUA.getIndex());

			Row row = sheet.createRow(0);
			Cell cell = row.createCell(0);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("S No.");
			
			cell = row.createCell(1);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Quote Number");
			
			cell = row.createCell(2);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Electrical Min");
			
			cell = row.createCell(3);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Electrical Max");

			cell = row.createCell(4);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Non Electrical Min");

			cell = row.createCell(5);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Non Electrical Max");

			cell = row.createCell(6);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("IDV Min");

			cell = row.createCell(7);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("IDV Max");


			workBook.write(out);
			out.close();
			System.out.println("Writesheet.xlsx written successfully");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
