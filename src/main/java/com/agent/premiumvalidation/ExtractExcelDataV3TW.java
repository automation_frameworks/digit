package com.agent.premiumvalidation;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.utilities.Library;

import pom.agent.home.Home;
import pom.agent.home.LoginPage;
import com.agent.motorv4.DetailsPage;

public class ExtractExcelDataV3TW {

	@SuppressWarnings("deprecation")
	public static void main(String args[]) throws Exception {

		FileInputStream fis = new FileInputStream(new File(Paths.get("").toAbsolutePath().toString()
				+ "/src/main/resources/exceldata/TW Test cases_For V4 Updated.xlsx"));
		WriteToExcel write = new WriteToExcel();
		write.createResultSheet2W();
		 //write.createResultSheet();
		// write.createLimitsResultSheet();
		XSSFWorkbook workBook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workBook.getSheetAt(1);
		// System.out.println(workBook);
		Row row = sheet.getRow(1);
		// int numberofRows = sheet.getLastRowNum();
		int numberOfColumns = row.getLastCellNum();
		// System.out.println(numberofRows);
		// System.out.println(numberOfColumns);
		// for()
		FormulaEvaluator formulaEvaluator = workBook.getCreationHelper().createFormulaEvaluator();
		HashMap<String, String> map = new LinkedHashMap<String, String>();
		for (int i = 20; i < 21; i++) {
			try {
				for (int j = 0; j < numberOfColumns; j++) {
					// System.out.println(sheet.getRow(i).getCell(j));
					try {
						switch (formulaEvaluator.evaluateInCell(sheet.getRow(i).getCell(j)).getCellType()) {
						case Cell.CELL_TYPE_NUMERIC:
							map.put(sheet.getRow(1).getCell(j).getStringCellValue(),
									"" + sheet.getRow(i).getCell(j).getNumericCellValue());
							break;
						case Cell.CELL_TYPE_STRING:
							map.put(sheet.getRow(1).getCell(j).getStringCellValue(),
									sheet.getRow(i).getCell(j).getStringCellValue());
							break;
						}
					} catch (NullPointerException ne) {
						map.put(sheet.getRow(1).getCell(j).getStringCellValue(), "");
					}

				}
				System.out.println(map);
				login();
				Library library = new Library();
				Home hp = PageFactory.initElements(library.getDriver(), Home.class);
				hp.selectModule("Motor Insurance");
				dispatcher(map);
				System.out.println("=============================================");
				library.getDriver().close();
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Failed for row : " + map.get("Sl no"));
			}
		}
	}

	public static void dispatcher(HashMap<String, String> hmap) throws Exception {
		// vehicleDetails()
		// previouspolicydetials()
		// Addons premium fetch
		// Addons selection
		// Premium fetch
		// Quote creation
		Library library = new Library();
		DetailsPage detailsPage = PageFactory.initElements(library.getDriver(), DetailsPage.class);
		String details = "";
		details = details + "Vehicle Type := TWO WHEELER COMPREHENSIVE";
		switch (hmap.get("Long Term Package")) {
		case "1OD_5TP":
			details = details + " # 1 Year OD + 5 Year TP";
			break;
		case "1OD_1TP":
			details = details + " # 1 Year OD + 1 Year TP";
			break;
		case "1OD_0TP":
			details = details + " # 1 Year OD Only";
			break;
		case "3OD_3TP":
			details = details + " # 3 Year OD + 3 Year TP";
			break;
		case "2OD_2TP":
			details = details + " # 2 Year OD + 2 Year TP";
			break;
		}
		if (hmap.get("Registration Type").equals("Non-New")) {
			details = details + " # Registration Number := " + hmap.get("RTO Code");
			// + "AA"
			// + library.getRandomValue("N | 4");
			details = details + " # Registration Year := " + hmap.get("Vehicle Registration Date").split("\\/")[2];
			details = details + " # Registration Month :=  December";
			System.out.println("Registration month : " + hmap.get("Vehicle Registration Date").split("\\/")[0]);
			/*switch (hmap.get("Vehicle Registration Date").split("\\/")[0]) {
			case "01":
			case "1":
				details = details + " # Registration Month :=  January";
				break;
			case "02":
			case "2":
				details = details + " # Registration Month :=  February";
				break;
			case "03":
			case "3":
				details = details + " # Registration Month :=  March";
				break;
			case "04":
			case "4":
				details = details + " # Registration Month :=  April";
				break;
			case "05":
			case "5":
				details = details + " # Registration Month :=  May";
				break;
			case "06":
			case "6":
				details = details + " # Registration Month :=  June";
				break;
			case "07":
			case "7":
				details = details + " # Registration Month :=  July";
				break;
			case "08":
			case "8":
				details = details + " # Registration Month :=  August";
				break;
			case "09":
			case "9":
				details = details + " # Registration Month :=  September";
				break;
			case "10":
				details = details + " # Registration Month :=  October";
				break;
			case "11":
				details = details + " # Registration Month :=  November";
				break;
			case "12":
				details = details + " # Registration Month :=  December";
				break;
			}*/
		} else {
			details = details + " # Brand New Vehicle := true # Registration Number := " + hmap.get("RTO Code");
			System.out.println("REg Data : "+hmap.get("Vehicle Registration Date").split("\\/")[2]);
			details = details + " # Manufacturing Year := " + hmap.get("Vehicle Registration Date").split("\\/")[2];
		}
		if (hmap.get("Vehicle Code").substring(0, 2).equals("13"))
			details = details + " # Two Wheeler Type := SCOOTER";
		details = details + " # Vehicle Make := " + hmap.get("Make").trim() + " # Vehicle Model :="
				+ hmap.get("Model").trim();
		// details = details + " # Fuel Type := "+hmap.get("Fuel");
		details = details + " # Vehicle Variant := " + hmap.get("Variant").trim() + " (" + hmap.get("CC");
		details = details + " # Residence City := " + hmap.get("Residence City");
		details = details + " # Pincode := " + hmap.get("Pincode");
		System.out.println(details);
		Thread.sleep(5000);
		detailsPage.enterVehicleDetails(details);
		/* Previous poliy details */
		if (hmap.get("Registration Type").equals("Non-New")) {
			details = "";
			details = details + "Existing Policy NCB := " + hmap.get("Previous NCB%").replace("%", "");
			// details = details + " # Previous Policy Type := Comprehensive Policy with
			// Zero Dep and EP";
			if (hmap.get("Claim Made").equals("Yes") || hmap.get("Claim Made").equals("All"))
				details = details + " # Claim := Yes";
			else
				details = details + " # Claim := No";
//			if (hmap.get("PreviousInsurer").equals("Digit"))
//				details = details + " # Previous Insurer Name := Godigit";
//			else
//				details = details + " # Previous Insurer Name := Acko";
			details = details + " # Previous Policy Expiry Date := " + hmap.get("Previous Expiry Date");
			details = details + " # Previous Policy Number := " + library.getRandomValue("AN | 10");

			details = details + " # Previous Insurer Name := Acko";
			System.out.println(details);

			detailsPage.enterPreviousPolicyDetails(details);
		}
		else
		{
			
		}
		detailsPage.performAction("Get Quote");
		HashMap<String, String> finalPremiumMap = new LinkedHashMap<>();
		//finalPremiumMap.putAll(detailsPage.scrapGQPremium2W(hmap.get("Sl no")));
		/* Motor Own Damage Coverages */
		//detailsPage.enterIDV(hmap.get("IDV"));
		details = "";
		if (!hmap.get("Elec Acc IDV IMT25").equals("0.0"))
			details = details + " Electrical Accessories := " + hmap.get("Elec Acc IDV IMT25");
		if (!hmap.get("Non Elec Acc IDV IMT25").equals("0.0"))
			details = details + " # Non Electrical Accessories := " + hmap.get("Non Elec Acc IDV IMT25");
		// if (!hmap.get("CNG Kit IDV").equals("0"))
		// details = details + " # CNG Kit := " + hmap.get("CNG Kit IDV");
		detailsPage.selectOwnDamageCoverages(details);
		/* Motor Own Damage Add Ons Coverages */
		details = "";
		if (hmap.get("Dep Cap").equalsIgnoreCase("Yes"))
			details = details + " Parts Depreciation Protect := " + hmap.get("Selected Zero Dep. (Yes/No)");
		if (hmap.get("Consumbales").equalsIgnoreCase("Yes"))
			details = details + " # Consumable Cover := " + hmap.get("Consumable cover");
		if (hmap.get("Engine Protector").equalsIgnoreCase("Yes"))
			details = details + " # Engine and Gear Box Protect := " + hmap.get("Engine Protector");
		// if(hmap.get("Rim Protect Cover").equalsIgnoreCase("Yes"))
		// details = details + " # Rim Protect Cover := "+hmap.get("Rim Protect Cover");
		if (hmap.get("RTI ").equalsIgnoreCase("Yes"))
			details = details + " # Return to Invoice := " + hmap.get("Return to Invoice");
		// if (hmap.get("Tyre Protect Cover").equalsIgnoreCase("Yes"))
		// details = details + " # Tyre Protect := " + hmap.get("Tyre Protect Cover");
		if (hmap.get("RSA").equalsIgnoreCase("Yes"))
			details = details + " # Breakdown Assistance := " + hmap.get("Road Side Assistance");
		detailsPage.selectOwnDamageAddOnsCoverages(details);
		try {
			detailsPage.performAction("Recalculate");
		} catch (Exception e) {

		}
		Thread.sleep(5000);
		details = "Full Name := " + library.getRandomValue("A | 10") + " # Mobile Number := 1234567890";
		detailsPage.enterCustomerDetails(details);
		detailsPage.performAction("Save Quote");
		System.out.println("After Quick Quote : " + finalPremiumMap);
		WriteToExcel write = new WriteToExcel();
		finalPremiumMap.putAll(detailsPage.scrapPremium(hmap.get("Sl no")));
		System.out.println("After Add On : " + finalPremiumMap);
		write.writeExcelData(finalPremiumMap);
		//write.writeExcelData(detailsPage.scrapLimits(hmap.get("Sl no")));

	}

	private static void login() throws Exception {
		com.utilities.Library library = new com.utilities.Library();
		WebDriver driver = library.DriverSetup();
		library.setEnvironment();
		System.out.println(Library.staticMap);
		System.out.println("URL : " + Library.staticMap.get("server") + "/DigitPlus/#/login");
		driver.get(Library.staticMap.get("server"));
		// + "/DigitPlus/#/login"
		//Thread.sleep(3000);
		LoginPage loginobj = PageFactory.initElements(driver, LoginPage.class);
		loginobj.enterLoginCredentials();
	}
}
