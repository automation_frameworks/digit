package com.agent.premiumvalidation;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.agent.motorv4.DetailsPage;
import com.utilities.Library;

import pom.agent.home.Home;
import pom.agent.home.LoginPage;

public class ExtractExcelData {

	public static void main(String args[]) throws Exception {

		FileInputStream fis = new FileInputStream(new File(
				Paths.get("").toAbsolutePath().toString() + "/src/main/resources/exceldata/4W Test Cases_V4.xlsx"));
		WriteToExcel write = new WriteToExcel();
		write.createResultSheet();
		//write.createLimitsResultSheet();
		XSSFWorkbook workBook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workBook.getSheetAt(0);
		// System.out.println(workBook);
		Row row = sheet.getRow(1);
		int numberofRows = sheet.getLastRowNum();
		int numberOfColumns = row.getLastCellNum();
		// System.out.println(numberofRows);
		// System.out.println(numberOfColumns);
		// for()
		FormulaEvaluator formulaEvaluator = workBook.getCreationHelper().createFormulaEvaluator();
		HashMap<String, String> map = new LinkedHashMap<String, String>();
		for (int i = 2; i < 3; i++) {
			try {
				for (int j = 0; j < numberOfColumns; j++) {
					// System.out.println(sheet.getRow(i).getCell(j));
					try {
						switch (formulaEvaluator.evaluateInCell(sheet.getRow(i).getCell(j)).getCellType()) {
						case Cell.CELL_TYPE_NUMERIC:
							map.put(sheet.getRow(1).getCell(j).getStringCellValue(),
									"" + sheet.getRow(i).getCell(j).getNumericCellValue());
							break;
						case Cell.CELL_TYPE_STRING:
							map.put(sheet.getRow(1).getCell(j).getStringCellValue(),
									sheet.getRow(i).getCell(j).getStringCellValue());
							break;
						}
					} catch (NullPointerException ne) {
						map.put(sheet.getRow(1).getCell(j).getStringCellValue(), "");
					}

				}
				System.out.println(map);
				login();
				Library library = new Library();
				Home hp = PageFactory.initElements(library.getDriver(), Home.class);
				hp.selectModule("Motor Insurance - Beta");
				//library.getDriver().get("https://plus.godigit.com/DigitPlus/#/motor-new");
				dispatcher(map);
				System.out.println("=============================================");
				//library.getDriver().close();
			} catch (Exception e) {
				System.out.println("Failed for row : " + map.get("Scr. No."));
			}
		}
	}

	public static void dispatcher(HashMap<String, String> hmap) throws Exception {
		// vehicleDetails()
		// previouspolicydetials()
		// Addons premium fetch
		// Addons selection
		// Premium fetch
		// Quote creation
		Library library = new Library();
		DetailsPage detailsPage = PageFactory.initElements(library.getDriver(), DetailsPage.class);
		String details = "";
		details = details + "Vehicle Type := FOUR WHEELER COMPREHENSIVE";
		if (hmap.get("Business Type").equals("Rollover")) {
			details = details + " # Registration Number := " + hmap.get("RTO Code") + "ZZ"
					+ library.getRandomValue("N | 4");
			details = details + " # Registration Year := " + hmap.get("Vehicle Registration Date").split("\\/")[2];
			switch (hmap.get("Vehicle Registration Date").split("\\/")[0]) {
			case "01":
			case "1":
				details = details + " # Registration Month :=  January";
				break;
			case "02":
			case "2":
				details = details + " # Registration Month :=  February";
				break;
			case "03":
			case "3":
				details = details + " # Registration Month :=  March";
				break;
			case "04":
			case "4":
				details = details + " # Registration Month :=  April";
				break;
			case "05":
			case "5":
				details = details + " # Registration Month :=  May";
				break;
			case "06":
			case "6":
				details = details + " # Registration Month :=  June";
				break;
			case "07":
			case "7":
				details = details + " # Registration Month :=  July";
				break;
			case "08":
			case "8":
				details = details + " # Registration Month :=  August";
				break;
			case "09":
			case "9":
				details = details + " # Registration Month :=  September";
				break;
			case "10":
				details = details + " # Registration Month :=  October";
				break;
			case "11":
				details = details + " # Registration Month :=  November";
				break;
			case "12":
				details = details + " # Registration Month :=  December";
				break;
			}
		} else {
			details = details + " # Brand New Vehicle := true # Registration Number := " + hmap.get("RTO Code");
			details = details + " # Manufacturing Year := " + hmap.get("Vehicle Registration Date").split("\\/")[2];
		}
		details = details + " # Vehicle Make := " + hmap.get("Make") + " " + hmap.get("Model");
		details = details + " # Fuel Type := " + hmap.get("Fuel");
		details = details + " # Vehicle Variant := " + hmap.get("Variant");
		details = details + " # City := " + hmap.get("City/District");
		details = details + " # Pincode := " + hmap.get("Pincode");
		System.out.println(details);
		Thread.sleep(5000);
		detailsPage.enterVehicleDetails(details);
		/* Previous poliy details */
		details = "";
		details = details + "Existing Policy NCB := " + hmap.get("Previous year NCB").replace("%", "");
		details = details + " # Previous Policy Type :=  Comprehensive Policy with Zero Dep and EP";
		if (hmap.get("Tbr_NCB%").equals("0.0"))
			details = details + " # Claim := Yes";
		else
			details = details + " # Claim := No";
		details = details + " # Previous Insurer Name := Others";
		details = details + " # Previous Policy Expiry Date := "+hmap.get("Previous Expiry Date");
		details = details + " # Previous Policy Number := " + library.getRandomValue("AN | 10");
		System.out.println(details);
		if (hmap.get("Business Type").equals("Rollover"))
			detailsPage.enterPreviousPolicyDetails(details);
		detailsPage.performAction("Get Quote");
		/* Motor Own Damage Coverages */
		details = "";
		if (!hmap.get("Elec Acc IDV").equals("0"))
			details = details + " Electrical Accessories := " + hmap.get("Elec Acc IDV");
		if (!hmap.get("Non Elec Acc IDV").equals("0"))
			details = details + " # Non Electrical Accessories := " + hmap.get("Non Elec Acc IDV");
		if (!hmap.get("CNG Kit IDV").equals("0"))
			details = details + " # CNG Kit := " + hmap.get("CNG Kit IDV");
		detailsPage.selectOwnDamageCoverages(details);
		/* Motor Own Damage Add Ons Coverages */
		details = "";
		if (hmap.get("Selected Zero Dep. (Yes/No)").equalsIgnoreCase("Yes"))
			details = details + " Parts Depreciation Protect := " + hmap.get("Selected Zero Dep. (Yes/No)");
		if (hmap.get("Consumable cover ").equalsIgnoreCase("Yes"))
			details = details + " # Consumable Cover := " + hmap.get("Consumable cover");
		if (hmap.get("Engine Protector").equalsIgnoreCase("Yes"))
			details = details + " # Engine and Gear Box Protect := " + hmap.get("Engine Protector");
		// if(hmap.get("Rim Protect Cover").equalsIgnoreCase("Yes"))
		// details = details + " # Rim Protect Cover := "+hmap.get("Rim Protect Cover");
		// if(hmap.get("Return to Invoice").equalsIgnoreCase("Yes"))
		// details = details + " # Return to Invoice := "+hmap.get("Return to Invoice");
		if (hmap.get("Tyre Protect Cover").equalsIgnoreCase("Yes"))
			details = details + " # Tyre Protect := " + hmap.get("Tyre Protect Cover");
		if (hmap.get("Road Side Assistance").equalsIgnoreCase("Yes"))
			details = details + " # Breakdown Assistance := " + hmap.get("Road Side Assistance");
		detailsPage.selectOwnDamageAddOnsCoverages(details);
		detailsPage.enterIDV(hmap.get("IDV"));
		//detailsPage.performAction("Recalculate");
		Thread.sleep(5000);
		details = "Full Name := " + library.getRandomValue("A | 10");
		detailsPage.enterCustomerDetails(details);
		detailsPage.performAction("Save Quote");
		WriteToExcel write = new WriteToExcel();
		write.writeExcelData(detailsPage.scrapPremium(hmap.get("Scr. No.")));
		//write.writeExcelData(detailsPage.scrapLimits(hmap.get("Scr. No.")));
	}

	private static void login() throws Exception {
		com.utilities.Library library = new com.utilities.Library();
		WebDriver driver = library.DriverSetup();
		String environMentName = library.setEnvironment();
		System.out.println(Library.staticMap);
		System.out.println("URL : " + Library.staticMap.get("server") + "/DigitPlus/#/login");
		driver.get(Library.staticMap.get("server"));
		// + "/DigitPlus/#/login"
		Thread.sleep(3000);
		LoginPage loginobj = PageFactory.initElements(driver, LoginPage.class);
		loginobj.enterLoginCredentials();
	}
}
