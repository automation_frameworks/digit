package com.agent.gpa_sme;

import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import com.itextpdf.text.log.SysoCounter;
import com.utilities.DBUtility;
import com.utilities.PropertyFileReader;

public class GPA_Master_Policy_Creation {

	@FindBy(id = "new")
	private WebElement btnNewPolicy;

	@FindBy(xpath = "//label[text()='Policy Holder Name']/..//textarea")
	private WebElement txtPolicyHolderName;

	@FindBy(xpath = "//label[text()='Document Type']/..//mat-select")
	private WebElement listDocumentType;

	@FindBy(xpath = "//label[text()='Document Number']/..//input")
	private WebElement txtDocumentNumber;

	@FindBy(xpath = "//label[text()='Pincode']/..//input")
	private WebElement txtPincode;
	
	@FindBy(xpath = "//label[text()='IMD Code']/..//input")
	private WebElement txtIMDcode;

	@FindBy(xpath = "//label[text()='Address']/..//textarea")
	private WebElement txtAddress;

	@FindBy(xpath = "//label[text()='Policy Type']/..//mat-select")
	private WebElement listPolicyType;

	@FindBy(xpath = "//label[text()='Policy Inception date']/..//button")
	private WebElement iconPolicyInceptionDate;

	@FindBy(xpath = "//label[text()='Group Business Type']/..//mat-select")
	private WebElement listGroupBusinessType;

	@FindBy(xpath = "//label[text()='Family Definition']/..//mat-select")
	private WebElement listFamilyDefinition;

	@FindBy(xpath = "//label[text()='Number Of Members']/..//input")
	private WebElement txtNumberOfMembers;

	@FindBy(xpath = "//label[text()='Occupancy']/..//mat-select")
	private WebElement listOccupancy;

	@FindBy(xpath = "//label[text()='Named / Unnamed']/..//mat-select")
	private WebElement listNamedUnnamed;

	@FindBy(xpath = "//label[text()='Highest Individual SI']/..//input")
	private WebElement txtHighestIndividualSI;

	@FindBy(xpath = "//label[text()='Claims in Last 3 Years']/..//mat-select")
	private WebElement listClaimsinLast3Years;

	@FindBy(xpath = "//label[text()='Expiring Policy Claims Amount']/..//input")
	private WebElement txtExpiringPolicyClaimsAmount;

	@FindBy(xpath = "//label[text()='Expiring Policy Claims Count']/..//input")
	private WebElement txtExpiringPolicyClaimsCount;

	@FindBy(xpath = "//label[text()='Aggregate Sum Insured']/..//input")
	private WebElement txtAggregateSumInsured;

	@FindBy(xpath = "//label[text()='Premium at Inception Excl Tax']/..//input")
	private WebElement txtPremiumAtInceptionExclTax;

	@FindBy(xpath = "//label[text()='Total Members']/..//input")
	private WebElement txtTotalMembers;

	@FindBy(xpath = "//label[text()='Aggregate Sum Insured (in INR)']/..//input")
	private WebElement txtAggregateSumInsuredInINR;

	@FindBy(xpath = "//label[text()='Terrorism']/..//mat-select")
	private WebElement listTerrorism;

	@FindBy(xpath = "//label[text()='Geographical Limit']/..//mat-select")
	private WebElement listGeographicalLimit;

	@FindBy(xpath = "//label[text()='Coverage']/..//mat-select")
	private WebElement listCoverage;

	@FindBy(xpath = "//span[contains(text(),'Accidental Death')]/..//input")
	private WebElement chkAccidentalDeath;

	@FindBy(xpath = "//span[contains(text(),'Permanent Total Disablement')]/..//input")
	private WebElement chkPermanentTotalDisablement;

	@FindBy(xpath = "//span[contains(text(),'PermanentPartialDisablement')]/..//input")
	private WebElement chkPermanentPartialDisablement;

	@FindBy(xpath = "//span[contains(text(),'Loss of Income Benefit')]/..//input")
	private WebElement chkLossOfIncomeBenefit;

	@FindBy(xpath = "//span[contains(text(),'Accidental Hospitalization')]/..//input")
	private WebElement chkAccidentalHospitalization;

	@FindBy(xpath = "//span[contains(text(),'Child Education Benefit')]/..//input")
	private WebElement chkChildEducationBenefit;

	@FindBy(xpath = "//span[contains(text(),'Funeral Expenses')]/..//input")
	private WebElement chkFuneralExpenses;

	@FindBy(xpath = "//span[contains(text(),'Transportation Cost')]/..//input")
	private WebElement chkTransportationCost;

	@FindBy(xpath = "//span[contains(text(),'Burns Cover')]/..//input")
	private WebElement chkBurnsCover;

	@FindBy(xpath = "//span[contains(text(),'Lifestyle Modification')]/..//input")
	private WebElement chkLifestyleModification;

	@FindBy(xpath = "//span[contains(text(),'Expense for External Aids')]/..//input")
	private WebElement chkExpenseForExternalAids;

	@FindBy(xpath = "//span[contains(text(),'Compassionate Visit')]/..//input")
	private WebElement chkCompassionateVisit;

	@FindBy(xpath = "//span[contains(text(),'Out Patient (OPD) Benefit')]/..//input")
	private WebElement chkOutPatientBenefit;

	@FindBy(xpath = "//label[text()='Brokerage %']/..//mat-select")
	private WebElement listBrokerage;
	
	@FindBy(xpath = "//label[text()='Brokerage %']/..//input")
	private WebElement txtBrokerage;

	@FindBy(xpath = "//label[text()='GST Exemption']/..//mat-select")
	private WebElement listGSTExemption;

	@FindBy(xpath = "//th[contains(text(),'Age Band')]/../..//td[2]//mat-select")
	private WebElement listAgeBand;

	@FindBys({@FindBy(xpath = "//span[@class='mat-option-text']")})
	private List<WebElement> valuesInListbox;

	@FindBy(xpath = "//label[text()='Member data collection file']/../..//input")
	private WebElement btnUploadMemberDataCollectionFile;

	@FindBy(xpath = "//label[text()='Proposal Document']/../..//input")
	private WebElement btnUploadProposalDocument;
	
	@FindBy(xpath = "//label[text()='Proposal Document']")
	private WebElement labelProposalDocument;

	@FindBy(xpath = "//label[contains(text(),'No. of lives uploaded')]/..//input")
	private WebElement txtNumberOfLivesUploaded;

	@FindBy(xpath = "//label[contains(text(),'Total Premium uploaded')]/..//input")
	private WebElement txtTotalPremiumUploaded;

	@FindBy(xpath = "//label[text()='Child Policy Required']/..//mat-select")
	private WebElement listChildPolicyRequired;

	@FindBy(xpath = "//label[text()='Who will pay the premium']/..//mat-select")
	private WebElement listWhoWillPayThePremium;

	@FindBy(xpath = "//label[contains(text(),'RM Email ID')]/..//input")
	private WebElement txtRMEmailID;

	@FindBy(xpath = "//label[contains(text(),'Customer Mobile No.')]/..//input")
	private WebElement txtCustomerMobileNumber;

	@FindBy(xpath = "//label[contains(text(),'Customer Email ID')]/..//input")
	private WebElement txtCustomerEmailId;

	@FindBy(xpath = "//label[text()='Master policy SMS to be sent to']/..//mat-select")
	private WebElement listMasterPolicySMS;

	@FindBy(xpath = "//label[text()='Master policy Email to be sent to']/..//mat-select")
	private WebElement listMasterPolicyEmail;

	@FindBy(xpath = "//label[text()='Child policy SMS to be sent to']/..//mat-select")
	private WebElement listChildPolicySMS;

	@FindBy(xpath = "//label[text()='Child policy Email to be sent to']/..//mat-select")
	private WebElement listChildPolicyEmail;

	@FindBy(xpath = "//label[contains(text(),'Customer Account Number')]/..//input")
	private WebElement txtCustomerAccountNumber;
	
	@FindBy(xpath = "//span[text()='Declaration']")
	private WebElement labelDeclaration;

	@FindBy(xpath = "//p[contains(text(),'I accept')]/..//input")
	private WebElement chkIAcceptTnC;

	@FindBy(xpath = "//label[text() = 'Quote No. :']/../label[2]")
	private WebElement labelQuoteNumber;




	private static com.utilities.Library library = new com.utilities.Library();

	public void fillInsuredDetails_GPA_SME(String gpa_sme_id) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.get_GPA_SME_DataFromDB(gpa_sme_id);

		enterText(txtPolicyHolderName, dbData[2]);
		clickDesiredValueInListbox(listDocumentType, dbData[3]);
		enterText(txtDocumentNumber, dbData[4]);
		enterText(txtPincode, dbData[5]);
		txtPincode.sendKeys(Keys.TAB);
		try {
			enterText(txtIMDcode, "1000295");
			txtIMDcode.sendKeys(Keys.TAB);
		} catch (Exception e) {
			System.out.println("IMD code is disabled!!");
		}
		enterText(txtAddress, dbData[6]);
	}

	public void fillProspectiveDetails_GPA_SME(String gpa_sme_id) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.get_GPA_SME_DataFromDB(gpa_sme_id);

		clickDesiredValueInListbox(listPolicyType, dbData[7]);
		clickOnDesiredDateInCalendar(iconPolicyInceptionDate, dbData[8]);
		library.focus(library.getDriver(), txtAddress);
		clickDesiredValueInListbox(listGroupBusinessType, dbData[9]);
		clickDesiredValueInListbox(listFamilyDefinition, dbData[10]);
		enterText(txtNumberOfMembers, dbData[11]);
		clickDesiredValueInListbox(listOccupancy, dbData[12]);
		clickDesiredValueInListbox(listNamedUnnamed, dbData[13]);
		enterText(txtHighestIndividualSI, dbData[14]);
		library.focus(library.getDriver(), txtHighestIndividualSI);

		//		****************Previous Policy Details*************************
		if (dbData[7].replaceAll(" ", "").toLowerCase().equals("rollover")) {
			clickDesiredValueInListbox(listClaimsinLast3Years, dbData[15]);
			if (dbData[15].replaceAll(" ", "").toLowerCase().equals("yes")) {
				enterText(txtExpiringPolicyClaimsAmount, dbData[16]);
				enterText(txtExpiringPolicyClaimsCount, dbData[17]);
			}
			enterText(txtAggregateSumInsured, dbData[18]);
			enterText(txtPremiumAtInceptionExclTax, dbData[19]);
			enterText(txtTotalMembers, dbData[20]);
			library.focus(library.getDriver(), txtTotalMembers);
		}
	}

	public void fillCoverageDetails_GPA_SME(String gpa_sme_id) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.get_GPA_SME_DataFromDB(gpa_sme_id);

		enterText(txtAggregateSumInsuredInINR, dbData[21]);
		clickDesiredValueInListbox(listTerrorism, dbData[22]);
		clickDesiredValueInListbox(listGeographicalLimit, dbData[23]);
		clickDesiredValueInListbox(listCoverage, dbData[24]);
		library.focus(library.getDriver(), listCoverage);
		selectCheckbox(dbData[25]);
	}

	public void fillPremiumDetails_GPA_SME(String gpa_sme_id) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.get_GPA_SME_DataFromDB(gpa_sme_id);

		try {
			clickDesiredValueInListbox(listBrokerage, dbData[26]);
		} catch (Exception e) {
			enterText(txtBrokerage, dbData[26]);
		}
		clickDesiredValueInListbox(listGSTExemption, dbData[27]);
		library.focus(library.getDriver(), listGSTExemption);
		library.focus(library.getDriver(), listAgeBand);
	}

	public void fillPaymentDetails_GPA_SME(String gpa_sme_id) throws Exception {

		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String[] dbData = db.get_GPA_SME_DataFromDB(gpa_sme_id);

		btnUploadMemberDataCollectionFile.sendKeys(Paths.get("").toAbsolutePath().toString()
				+ "/src/main/resources/documents/GPA_SME_TEMPLATE.xlsx");
		library.waitForDigitLoad();
		btnUploadProposalDocument.sendKeys(Paths.get("").toAbsolutePath().toString()
				+ "/src/main/resources/documents/GPA_SME_Proposal_Form.docx");
		library.waitForDigitLoad();
		library.focus(library.getDriver(), labelProposalDocument);

		enterText(txtNumberOfLivesUploaded, dbData[28]);
		enterText(txtTotalPremiumUploaded, dbData[29]);
		clickDesiredValueInListbox(listChildPolicyRequired, dbData[30]);
		clickDesiredValueInListbox(listWhoWillPayThePremium, dbData[31]);
		listWhoWillPayThePremium.sendKeys(Keys.TAB);
		enterText(txtRMEmailID, dbData[32]);
		enterText(txtCustomerMobileNumber, dbData[33]);
		enterText(txtCustomerEmailId, dbData[34]);
		clickDesiredValueInListbox(listMasterPolicySMS, dbData[35]);
		clickDesiredValueInListbox(listMasterPolicyEmail, dbData[36]);
		clickDesiredValueInListbox(listChildPolicySMS, dbData[37]);
		clickDesiredValueInListbox(listChildPolicyEmail, dbData[38]);
		if (dbData[31].replaceAll(" ", "").toLowerCase().equals("customerfloat")) {
			enterText(txtCustomerAccountNumber, dbData[39]);
		}
		library.focus(library.getDriver(), listChildPolicyEmail);
		library.focus(library.getDriver(), labelDeclaration);
		if (!chkIAcceptTnC.isSelected()) {
			chkIAcceptTnC.click();
		}
	}
	
	public void storeDataForGPA_SMEInDB() {
		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		String formattedDate = myDateObj.format(myFormatObj);
		DBUtility db = PageFactory.initElements(library.getDriver(), DBUtility.class);
		String quoteNum = getInsId(labelQuoteNumber.getText());
		db.createNewRowInGMCMasterPolicyTableInDB(quoteNum, formattedDate, "10302");
	}
	private String getInsId(String quoteNum) {
		String[] digit = quoteNum.split("");
		quoteNum = "";
		boolean flag = false;
		for (String num : digit) {
			if (!num.equals("0") && !flag) {
				flag = true;
			}
			if (flag) {
				quoteNum = quoteNum + num;
			}
		}
		return quoteNum;
	}

	private void enterText(WebElement textField, String data) throws Exception {
		library.waitForDigitLoad();
		if (data!=null) {
			textField.clear();
			textField.sendKeys(data);
		}
		library.waitForDigitLoad();
	}

	private void clickDesiredValueInListbox(WebElement listbox,String value) throws Exception {
		library.waitForDigitLoad();
		if (value!=null) {
			library.waitForElementVisible(listbox, 10, 1000);
			listbox.click();
			Thread.sleep(1000);
			for (WebElement element : valuesInListbox) {
				if (element.getText().replaceAll(" ", "").replaceAll(",", "").toLowerCase()
						.contains(value.replaceAll(" ", "").replaceAll(",", "").toLowerCase())) {
					element.click();
					break;
				}
			} 
			Thread.sleep(1000);
		}
		library.waitForDigitLoad();
	}

	private void selectCheckbox(String value) throws InterruptedException {

		if (value!=null) {

			String[] listOfCovers = value.split(",");
			WebElement element = null;
			for (String cover : listOfCovers) {
				element = library.getDriver().findElement(By.xpath("//span[contains(text(),'"+cover.trim()+"')]/..//input"));
				System.out.println(element.isSelected());
				if (!element.isSelected()) {
					element.click();
				}
				Thread.sleep(2000);
			}
			library.focus(library.getDriver(), element);
		}
	}

	private void clickOnDesiredDateInCalendar(WebElement iconCalendar,String date) throws InterruptedException {

		iconCalendar.click();
		String dateTxt = date;
		Calendar cal = Calendar.getInstance(); 
		SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy"); 
		switch(date.toLowerCase().trim()) {
		case "today":
			date = s.format(cal.getTime());
			library.getDriver().findElement(By.xpath("//div[text()='"+date.split("-")[0]+"']")).click();
			break;
		case "tomorrow":
			cal.add(Calendar.DATE, 1);
			date = s.format(cal.getTime());
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//button[contains(@class , 'mat-calendar-period-button')]")).click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[text()='"+date.split("-")[2]+"']")).click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[text()='"+monthFlag(date)+"']")).click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[text()='"+Integer.parseInt(date.split("-")[0])+"']")).click();
			break;
		default:
			date = date.trim().toLowerCase().split(" ")[0];
			if (dateTxt.toLowerCase().contains("from")) {
				cal.add(Calendar.DATE, Integer.parseInt(date));
			} else {
				cal.add(Calendar.DATE, (-Integer.parseInt(date)-1));
			}
			date = s.format(cal.getTime());
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//button[contains(@class , 'mat-calendar-period-button')]"))
			.click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[text()='" + date.split("-")[2] + "']")).click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[text()='" + monthFlag(date) + "']")).click();
			Thread.sleep(2000);
			library.getDriver().findElement(By.xpath("//div[@class='mat-calendar-body-cell-content' and text()='"
					+ Integer.parseInt(date.split("-")[0]) + "']")).click();
			break;
		}
	}

	private String monthFlag(String date) {
		String month = "";
		switch(date.split("-")[1]) {
		case "01":
			month = "JAN";
			break;
		case "02":
			month = "FEB";
			break;
		case "03":
			month = "MAR";
			break;
		case "04":
			month = "APR";
			break;
		case "05":
			month = "MAY";
			break;
		case "06":
			month = "JUN";
			break;
		case "07":
			month = "JUL";
			break;
		case "08":
			month = "AUG";
			break;
		case "09":
			month = "SEPT";
			break;
		case "10":
			month = "OCT";
			break;
		case "11":
			month = "NOV";
			break;
		case "12":
			month = "DEC";
			break;
		}
		return month;
	}
}
