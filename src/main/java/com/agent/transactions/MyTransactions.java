package com.agent.transactions;

import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MyTransactions {

	@FindBy(id = "policyNumberSearch")
	private WebElement txtPolicyNumberSearch;

	@FindBy(xpath = "//i[text()='search']")
	private WebElement btnSearch;
	
	@FindBy(xpath="//table[@class='record_table tablewidthformobile table-striped']//td[4]")
	private WebElement lblFirstRow;
	
	@FindBy(xpath = "//nav[@class='navbar']//input")
	private WebElement globalSearchBox; 
	
	@FindBy(xpath="//li[text()='Policy / Quote Number']")
	private WebElement lstPolicyQuoteNumber;

	private com.utilities.Library library = new com.utilities.Library();

	public void openPolicy(String details) throws Exception {
		System.out.println("Details : " + details);
		library.waitForDigitLoad();
		HashMap<String, String> hmap = library.getFieldMap(details);
		try {
			for (String key : hmap.keySet()) {
				System.out.println("Key is : " + key);
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "policynumber":
					globalSearchBox.click();
					lstPolicyQuoteNumber.click();
					globalSearchBox.sendKeys(hmap.get(key));
					break;
				default:
					throw new Exception("There is no such field '" + key + "'");
				}
			}
			btnSearch.click();
			library.waitForDigitLoad();
			lblFirstRow.click();
			library.waitForDigitLoad();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while trying to open the policy : " + e.getMessage());
		}

	}

}
