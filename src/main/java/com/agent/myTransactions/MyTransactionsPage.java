package com.agent.myTransactions;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.utilities.Library;

import pom.agent.motor.DetailsPage;

public class MyTransactionsPage {

	static Library library = new Library();
	static WebDriver driver = library.getDriver();
	int colNumber = 0;
	String insuredItemName;
	static String preprodplusData;
	static String uatData;
	boolean flag;
	List<WebElement> policyQuoteNumberCol;

	@FindBy(xpath = "//div[contains(@class,'search')]/div[1]/input[contains(@placeholder,'Search')]")
	public WebElement searchBox;

	@FindBy(xpath = "//input[@placeholder='Search']/following-sibling::div//li")
	public List<WebElement> searchDropDownMenuOptions;

	@FindBy(xpath = "//div[contains(@class,'searchbar')]//i[text()='search']")
	public WebElement searchIcon;

	@FindBy(xpath = "//span[text()='Insured Item']/ancestor::thead/following-sibling::tbody/tr/td[4]")
	public List<WebElement> insuredItem;

	@FindBy(xpath = "(//table[@class='record_table tablewidthformobile table-striped'])[2]/tbody//div[@class='mat-checkbox-inner-container mat-checkbox-inner-container-no-side-margin']")
	public List<WebElement> allCheckBoxes;

	@FindBy(xpath = "(//table[@class='record_table tablewidthformobile table-striped'])[2]//td[8]/img")
	public List<WebElement> actionDowloadimg;

	@FindBy(xpath = "//div[@class='col-md-9']/a[contains(.,'Selected')]")
	public WebElement downloadSelectedImg;

	@FindBy(xpath = "//a[text()='>']")
	public WebElement nextButton;

	@FindBy(xpath = "//div[text()='Quotes']")
	public WebElement quotes;

	@FindBy(xpath = "//img[@alt='DigitPlus']")
	public WebElement digitPlusHomeNavigator;

	@FindBy(xpath = "//nav[@class='navbar']//input")
	private WebElement globalSearchBox;

	@FindBy(xpath = "//nav[@class='navbar']//span[1]/i")
	private WebElement globalSearchIcon;

	@FindBy(xpath = "//table/tbody/tr//td[2]")
	private WebElement searchedPolicyNum;

	@FindBy(xpath = "//table/tbody/tr//td[6]")
	private WebElement polStatus;

	@FindBy(xpath = "//input[@placeholder = 'Global Search']")
	private WebElement globalsearchTxtBox;

	@FindBy(xpath = "//div[@class='for-div-search-options']//li[text()='Policy / Quote Number']")
	private WebElement PolNumberSearchBox;

	@FindBy(name = "selectfromDate")
	private WebElement selectFromDate;

	@FindBy(name = "selectuptoDate")
	private WebElement selectUptoDate;

	@FindBy(xpath = "//div[@id='toast-container' and contains(@style,'position: fixed;')]//span")
	public WebElement toasterMessage;

	public void clickOnQuotes() throws Exception {
		try {
			library.waitForDigitLoad();
			quotes.click();
			library.waitForDigitLoad();
		} catch (Exception e) {
			throw new Exception("Quote option is not available");
		}
	}

	public void getSearchBox(String data) throws Exception {
		try {
			System.out.println("Data:" + data);
			library.waitForDigitLoad();
			searchBox.sendKeys(data);
			Thread.sleep(2000);
			searchIcon.click();
			Thread.sleep(2000);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public void VerifyPolicyQuoteNumber(String expPolicyQuoteNumber, String text, WebDriver driver) throws Exception {
		policyQuoteNumberCol = driver.findElements(
				By.xpath("//span[text()='" + text + "']/ancestor::thead/following-sibling::tbody//td[2]"));
		for (int i = 0; i < policyQuoteNumberCol.size(); i++) {
			System.out.println("ActualpolicyNumber:   " + policyQuoteNumberCol.get(i).getText());
			if (policyQuoteNumberCol.get(i).getText().trim().equalsIgnoreCase(expPolicyQuoteNumber.trim())) {
				flag = true;
				break;
			}
		}

		if (!flag)
			throw new Exception(
					"The expected Quote/Policy Number is not populated in my transaction page" + expPolicyQuoteNumber);

	}

	public void selectCheckBox() throws Throwable {
		try {
			library.waitForDigitLoad();
			allCheckBoxes.get(colNumber).click();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public void clickOnDownloadOption() throws Exception {
		try {
			Thread.sleep(2000);
			actionDowloadimg.get(colNumber).click();
			library.waitForDigitLoad();
			downloadSelectedImg.click();
			library.waitForDigitLoad();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public void clickOnFirstPolicyNumber(String text) throws Exception {
		try {
			com.utilities.Library library = new com.utilities.Library();
			WebDriver driver = library.getDriver();
			Library.waitForVisibilityOfTheElement(driver.findElement(
					By.xpath("//span[text()='" + text + "']/ancestor::thead/following-sibling::tbody//td[2]")));
			policyQuoteNumberCol = driver.findElements(
					By.xpath("//span[text()='" + text + "']/ancestor::thead/following-sibling::tbody//td[2]"));
			policyQuoteNumberCol.get(colNumber).click();
			policyQuoteNumberCol.clear();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public String getInsuredItem() throws Exception {
		try {
			insuredItemName = insuredItem.get(colNumber).getText();
			System.out.println("InsuredItemName");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return insuredItemName;

	}

	public void verifyRegistrationNumber(String environMentName, String details, WebDriver driver) throws Exception {
		try {
			System.out.println("InsuredName" + insuredItem.get(colNumber).getText());
			switch (environMentName.toLowerCase()) {
			case "preprodplus1":
			case "pre-prodplus1":
			case "uat":
			case "pre-prodplus":
			case "preprod":
			case "preprodplus":
				System.out.println("ExpectedpreprodplusRegNo: " + MyTransactionsPage.preprodplusData);
				String registrationNumber[] = insuredItem.get(colNumber).getText().split(",");
				System.out.println("Act REG No: " + registrationNumber[0].replaceAll(" ", ""));
				Assert.assertEquals(registrationNumber[0].replaceAll(" ", ""), MyTransactionsPage.preprodplusData);
				// Assert.assertTrue(insuredItemName.contains(MyTransactionsPage.preprodplusData));
				flag = true;
				break;
			case "preprodtest":
				// String registrationNumber[] = insuredItemName.split(",");
				// System.out.println("Act REG No: "+registrationNumber[0].replaceAll(" ", ""));
				// Assert.assertEquals(registrationNumber[0].replaceAll(" ", ""),
				// MyTransactionsPage.preprodplusData);
				flag = true;
				break;

			default:
				throw new Exception("Expected case  is not available" + environMentName);
			}

			if (!flag) {
				throw new Exception("Expected registration number is not available");

			}

		}

		catch (Exception e) {
			throw new Exception("Expected registration number is not available" + e.getMessage());
		}

	}

	public void enterPolicyQuotedetails(String environmentName, String expData, WebDriver driver, String text)
			throws Exception

	{
		com.utilities.Library library = new com.utilities.Library();
		HashMap<String, String> hmap = library.getFieldMap(expData);
		switch (environmentName.toLowerCase()) {
		case "preprodplus1":
		case "pre-prodplus1":
		case "preprodtest":
		case "pre-prodplus":
		case "preprod":
		case "preprodplus":
			VerifyPolicyQuoteNumber(MyTransactionsPage.preprodplusData, text, driver);
			break;
		case "uat":
			VerifyPolicyQuoteNumber(MyTransactionsPage.uatData, text, driver);
			break;
		default:
			throw new Exception("Implementation for the field '" + expData + "' is not found");
		}
	}

	public void searchDataInMyTransactions(String environMentName, String details) throws Exception {

		HashMap<String, String> hmap = library.getFieldMap(details);
		for (String key : hmap.keySet()) {
			switch (key.replaceAll(" ", "")) {
			case "preprodplusQuoteNo":
				MyTransactionsPage.preprodplusData = hmap.get(key);
				break;
			case "uatQuoteNo":
				MyTransactionsPage.uatData = hmap.get(key);
				break;
			case "preprodplusPolicyNo":
				MyTransactionsPage.preprodplusData = hmap.get(key);
				break;
			case "uatPolicyNo":
				MyTransactionsPage.uatData = hmap.get(key);
				break;
			case "preprodplusMobileNo":
				MyTransactionsPage.preprodplusData = hmap.get(key);
				break;
			case "uatMobileNo":
				MyTransactionsPage.uatData = hmap.get(key);
				break;
			case "preprodplusRegistrationNo":
				MyTransactionsPage.preprodplusData = hmap.get(key);
				break;
			case "uatRegistrationNo":
				MyTransactionsPage.uatData = hmap.get(key);
				break;
			case "preprodplusEngineNo":
				System.out.println("EngineNo: " + hmap.get(key));
				System.out.println("Environment:" + environMentName);
				MyTransactionsPage.preprodplusData = hmap.get(key);
				break;
			case "uatEngineNo":
				MyTransactionsPage.uatData = hmap.get(key);
				break;
			case "preprodplusChassisNo":
				MyTransactionsPage.preprodplusData = hmap.get(key);
				break;
			case "uatChassisNo":
				MyTransactionsPage.uatData = hmap.get(key);
				break;
			default:
				throw new Exception("Implementation for the field '" + key + "' is not found");
			}
		}
		switch (environMentName.toLowerCase()) {
		case "preprodplus1":
		case "pre-prodplus1":
		case "preprodtest":
		case "pre-prodplus":
		case "preprod":
		case "preprodplus":
			getSearchBox(MyTransactionsPage.preprodplusData);
			break;
		case "uat":
			System.out.println("uat:" + MyTransactionsPage.uatData);
			getSearchBox(MyTransactionsPage.uatData);
			break;

		default:
			throw new Exception("Implementation for the field '" + details + "' is not found");
		}

	}

	public void verifyMobileNumber(String details, String environMentName, WebDriver driver) throws Exception {
		com.utilities.Library library = new com.utilities.Library();
		DetailsPage dp = PageFactory.initElements(driver, DetailsPage.class);
		library.waitForDigitLoad();
		library.scrollElementIntoView(dp.txtMobileNumber, driver);
		switch (environMentName.toLowerCase()) {
		case "preprodplus1":
		case "pre-prodplus1":
		case "preprodtest":
		case "pre-prodplus":
		case "preprod":
		case "preprodplus":
			System.out.println("preprodplusDataMN:" + MyTransactionsPage.preprodplusData);
			Assert.assertTrue(preprodplusData.equalsIgnoreCase(dp.getMobileNumber()));
			break;
		case "uat":
			System.out.println("preprodtestDataMN:" + MyTransactionsPage.uatData);
			Assert.assertTrue(uatData.equalsIgnoreCase(dp.getMobileNumber()));
			break;
		default:
			throw new Exception("Environment is not available here, kindly implement :" + environMentName);
		}
	}

	public void getPolicyStatus() throws Exception {
		com.utilities.Library library = new com.utilities.Library();
		library.getDriver();
		Thread.sleep(3000);
		digitPlusHomeNavigator.click();
		library.waitForDigitLoad();

		String generatedPoluNum = library.staticMap.get("Policy Number");
		Actions act = new Actions(library.getDriver());
		act.moveToElement(globalsearchTxtBox);
		act.moveToElement(PolNumberSearchBox);
		act.click();
		act.perform();
		globalSearchBox.sendKeys(generatedPoluNum);

		globalSearchIcon.click();
		library.waitForDigitLoad();
		String displayedPolNum = searchedPolicyNum.getText();
		System.out.println("Displayed policy Number is" + displayedPolNum);

		library.waitForDigitLoad();
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(generatedPoluNum, displayedPolNum);

		String policyStatus = polStatus.getText().trim();
		library.staticMap.put("Policy Status", policyStatus);
	}

	public void selectDate(String details, WebDriver driver) throws Exception {

		/*
		 * com.utilities.Library library = new com.utilities.Library();
		 * library.getDriver(); library.enterDate(selectFromDate,details); //Actions act
		 * = new Actions(driver);
		 */
		com.utilities.Library library = new com.utilities.Library();
		library.getDriver();
		HashMap<String, String> hmap = library.getFieldMap(details);

		for (String key : hmap.keySet()) {
			switch (key.toLowerCase()) {
			case "startdate":
				searchBox.click();
				searchBox.sendKeys(Keys.TAB);
				System.out.println("StartDateKey=" + hmap.get(key));
				library.enterDate(selectFromDate, hmap.get(key));
				Thread.sleep(2000);
				break;
			case "enddate":
				searchBox.sendKeys(Keys.TAB);
				System.out.println("EndDateKey=" + hmap.get(key));
				library.enterDate(selectUptoDate, hmap.get(key));
				break;
			}
		}

	}

	public void mouseOverOnSearchBox(WebDriver driver) {

		Actions action = new Actions(driver);
		action.moveToElement(searchBox).perform();

	}

	public void selectDropDownMenuOptionsFromSerachBox(WebDriver driver, String element) throws Exception {
		for (int i = 0; i < searchDropDownMenuOptions.size(); i++) {
			if (searchDropDownMenuOptions.get(i).getText().equalsIgnoreCase(element)) {
				searchDropDownMenuOptions.get(i).click();
				flag = true;
			}
		}

		if (!flag)
			throw new Exception("The expected data is not available in searchDropdownMenu:" + element);
	}

	public void verifyEngineNumber(String environMentName, String details, WebDriver driver2) throws Exception {
		String engineNumber = null;
		try {
			switch (environMentName.toLowerCase()) {
			case "preprodplus1":
			case "pre-prodplus1":
			case "preprodtest":
			case "pre-prodplus":
			case "preprod":
			case "preprodplus":
				System.out.println("ExpectedpreprodplusRegNo: " + MyTransactionsPage.preprodplusData);
				DetailsPage dp = PageFactory.initElements(driver, DetailsPage.class);
				engineNumber = dp.getTxtEngineNumber();
				System.out.println("Act REG No: " + engineNumber);
				Assert.assertEquals(engineNumber, MyTransactionsPage.preprodplusData);
				// Assert.assertTrue(insuredItemName.contains(MyTransactionsPage.preprodplusData));
				flag = true;
				break;
			case "uat":
				Assert.assertEquals(engineNumber, MyTransactionsPage.uatData);
				flag = true;
				break;

			default:
				throw new Exception("Expected case  is not available" + environMentName);
			}

			if (!flag) {
				throw new Exception("Expected registration number is not available");

			}

		}

		catch (Exception e) {
			throw new Exception("Expected registration number is not available" + e.getMessage());
		}

	}

	public void verifyChassisNumber(String environMentName, String details, WebDriver driver2) throws Exception {
		String chassisNumber = null;
		try {
			switch (environMentName.toLowerCase()) {
			case "preprodplus1":
			case "pre-prodplus1":
			case "preprodtest":
			case "pre-prodplus":
			case "preprod":
			case "preprodplus":
				System.out.println("ExpectedpreprodplusRegNo: " + MyTransactionsPage.preprodplusData);
				DetailsPage dp = PageFactory.initElements(driver, DetailsPage.class);
				chassisNumber = dp.getTxtChassisNumber();
				System.out.println("Act REG No: " + chassisNumber);
				Assert.assertEquals(chassisNumber, MyTransactionsPage.preprodplusData);
				// Assert.assertTrue(insuredItemName.contains(MyTransactionsPage.preprodplusData));
				flag = true;
				break;
			case "uat":
				Assert.assertEquals(chassisNumber, MyTransactionsPage.preprodplusData);
				flag = true;
				break;
			default:
				throw new Exception("Expected case  is not available" + environMentName);
			}

			if (!flag) {
				throw new Exception("Expected registration number is not available");

			}

		}

		catch (Exception e) {
			throw new Exception("Expected registration number is not available" + e.getMessage());
		}

	}

	public void verifyPolicyStatus(String status) throws Exception {
		com.utilities.Library library = new com.utilities.Library();
		library.getDriver();
		Thread.sleep(3000);
		digitPlusHomeNavigator.click();
		library.waitForDigitLoad();

		String generatedPoluNum = Library.staticMap.get("Policy Number");
		Actions act = new Actions(library.getDriver());
		act.moveToElement(globalsearchTxtBox);
		act.moveToElement(PolNumberSearchBox);
		act.click();
		act.perform();
		globalSearchBox.sendKeys(generatedPoluNum);
		globalSearchIcon.click();
		library.waitForDigitLoad();
		Thread.sleep(2000);
		String displayedPolNum = searchedPolicyNum.getText();
		System.out.println("Displayed policy Number is" + displayedPolNum);

		library.waitForDigitLoad();
		String policyStatus = polStatus.getText().trim();
		Library.staticMap.put("Policy Status", policyStatus);
		if (!policyStatus.equals(status))
			throw new Exception("The policy is expected to be in [" + status + "]. But it is in [" + policyStatus + "].");
	}
}
