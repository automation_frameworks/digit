package com.agent.jewellery;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.utilities.Library;

public class SuccessPage {

	static Library library = new Library();
	
	@FindBy(xpath="//label[text()='Policy Number: ']//following-sibling::span/b")
	public WebElement policyNumber;
	
	@FindBy(xpath="//button[text()='Download Policy']")
	public WebElement downloadPolicyBtn;
	
	@FindBy(xpath="//h1[contains(text(),'Jewellery is now insured!')]")
	public WebElement successMsg;

	
public String getsuccessMsg() throws Exception {
		try {
		return successMsg.getText();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("Success message is not populated:"+e.getMessage());

		}
	}
	

	public String getPolicyNumber() throws Exception {
		try {
		return policyNumber.getText();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("Policy Number  is not available:"+e.getMessage());

		}
	}
	
public void clickOnDownloadPolicyButton() throws Exception {
		try {
	downloadPolicyBtn.click();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception("Download Policy Button  is not available:"+e.getMessage());
		}
	
	}
	
	
}
