	package com.agent.jewellery;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.testng.Assert;

import com.utilities.Library;

public class JewelleryDetailaPage {

	
	static Library library = new Library();
	
	@FindBy(xpath="//div[contains(text(),'SELECT ')]")
	private WebElement selectDropDown;
	
	@FindBy(xpath="//div[@id='ddItems']/div")
	private List<WebElement> jwelleryDropDownOptions;
	
	@FindBy(xpath="//input[@placeholder='Enter Description']")
	private  WebElement productDescription;
	
	@FindBy(xpath="//input[@placeholder='Enter Value']")
	private WebElement jwelleryValue;
	
	@FindBy(xpath="//button[text()='Add Item']")
	private WebElement addItemBtn;
	
	@FindBy(xpath="//button[text()='Get Quote']")
	private WebElement getQuoteBtn;
	
	@FindBy(xpath="//button[text()='Buy Now']")
	private WebElement buyNowBtn;

	@FindBy(xpath="//span[text()='Confirm Payment']")
	public static WebElement confirmPaymentPopupHeader;
	
	
			public void	enterAllThedetails(String details) throws InterruptedException
			{
				try {
					library.waitForDigitLoad();
				boolean flag = false;
				System.out.println(details);
				HashMap<String, String> hmap = library.getFieldMap(details);
		        for (String key : hmap.keySet())
		        {
		            switch (key.replaceAll(" ", "").toLowerCase()) {

		            case "selectjwellerytype":
		            	selectDropDown.click();
		            	String jwelleryType = hmap.get(key);
		                for(int i=0;i<jwelleryDropDownOptions.size();i++)
		                {
		                	if(jwelleryDropDownOptions.get(i).getText().equalsIgnoreCase(jwelleryType))
		                	{
		                		jwelleryDropDownOptions.get(i).click();
		                		flag=true;
		                	}
		                }
		                if(!flag)
		                	System.out.println("expected jwellery type is not available"+ jwelleryType);
		                
		            break;
		            case "productdescription":
		            	 System.out.println("productDescription : " + hmap.get(key));
		            	 productDescription.sendKeys(hmap.get(key).trim());
		            break;
		            case "jwelleryvalue":
		            	 System.out.println("jwelleryValue : " + hmap.get(key));
		            	 jwelleryValue.sendKeys(hmap.get(key).trim());
		            break;
		            default:
	                    throw new Exception("Implementation for the field '" + key + "' is not found");
			}
		        }
				}
					
		        catch(Exception e)
				{
		        	e.printStackTrace();
		    	}
			
			}	
			
			
			
			public void getAddItemButton() throws Exception
			{
				try 
				{
				addItemBtn.click();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
					throw new Exception("AddItem Button  is not available:"+e.getMessage());

				}
			}
			
			public void getQuoteButton() throws Exception {
				try {
				getQuoteBtn.click();
				}
				catch (Exception e)
				{
					e.printStackTrace();
					throw new Exception("Get Quote Button  is not available:"+e.getMessage());

				}
				
			}
			
			
			public String getconfirmPaymentPopupHeader() throws Exception
			{
				try {
				return confirmPaymentPopupHeader.getText();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
					throw new Exception("Confirm payment popup  is not populated:"+e.getMessage());
				}
			}
			
			public void clickOnBuyNowBtn() throws Exception {
				try
				{
				buyNowBtn.click();
				}
				catch (Exception e) 
				{
					e.printStackTrace();
					throw new Exception("proceed Button  is not available:"+e.getMessage());

				}
				
			}
			
			
}
