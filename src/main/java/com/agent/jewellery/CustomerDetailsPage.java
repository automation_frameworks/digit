package com.agent.jewellery;

import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.utilities.Library;

public class CustomerDetailsPage {

	static Library library = new Library();
	
	@FindBy(xpath="//h4[text()='Customer Details']")
	public static WebElement customerDEtailsFrameTitle;
	
	@FindBy(xpath="//input[@name='firstName']")
	public WebElement firstNameTextBox;
	
	@FindBy(xpath="//input[@name='lastName']")
	public WebElement lastNameTextBox;
	
	@FindBy(xpath="//input[@name='emailAddress']")
	public WebElement emailAddressTextBox;
	
	@FindBy(xpath="//input[@name='mobileNumber']")
	public WebElement mobileNumberTextBox;

	@FindBy(xpath="//input[@name='city']")
	public WebElement cityTextBox;
	
	@FindBy(xpath="//input[@name='pincode']")
	public WebElement pincodeTextBox;
	
	@FindBy(xpath="//label[@class='file-box']/following-sibling::input")
	public List<WebElement> fileUploadBox;
	
	@FindBy(xpath="//button[text()='Proceed']")
	public WebElement proceedBtn;
	
	@FindBy(xpath="//input[@id='productimg']")
	public WebElement productimg;
	
	@FindBy(xpath="//div[@class='row invoiceAndProductsTbl']/descendant::tr/td[2]")
	public List<WebElement> UploadDocTypeColumun;
	//div[@class='row invoiceAndProductsTbl']//tr/td[2]
	
	//div[@class='row invoiceAndProductsTbl']/descendant::tr/td[2]


public String customerDEtailsFrameTitle() throws Exception {
	try 
	{
	return customerDEtailsFrameTitle.getText();
	}
	catch (Exception e) 
	{
		e.printStackTrace();
		throw new Exception("Customer details frame title is not available:"+e.getMessage());
	}
}

public void clickOnproceedBtn() throws Exception
{
	try {
	proceedBtn.click();
	}
	catch (Exception e)
	{
		e.printStackTrace();
		throw new Exception("proceed Button  is not available:"+e.getMessage());
	}
}



public void enterCustomerDetails(String details) {
	
	try {
		
		System.out.println(details);
		HashMap<String, String> hmap = library.getFieldMap(details);
        for (String key : hmap.keySet())
        {
            switch (key.replaceAll(" ", "").toLowerCase()) {

            case "firstname":
            	firstNameTextBox.sendKeys(hmap.get(key));
            break;
            
            case "lastname":
            	lastNameTextBox.sendKeys(hmap.get(key));
            break;
            
            case "emailaddress":
            	emailAddressTextBox.sendKeys(hmap.get(key));
            break;
            
            case "mobilenumber":
            	mobileNumberTextBox.sendKeys(hmap.get(key));
            break;
            
            case "city":
            	cityTextBox.sendKeys(hmap.get(key));
            	
            break;
            
            case "pincode":
            	pincodeTextBox.sendKeys(hmap.get(key));
            	Thread.sleep(1000);
            	pincodeTextBox.sendKeys(Keys.DOWN);
            	Thread.sleep(1000);
            	pincodeTextBox.sendKeys(Keys.ENTER);
            	
            break;
            
            default:
                throw new Exception("Implementation for the field '" + key + "' is not found");
            	}
        }
		}
			
        catch(Exception e){
        	e.printStackTrace();
        	
        }
	
	
	
	
	
	
}


public void uploadJwelleryDocuments() throws Exception {
	try
	{
	for(int i=0;i<fileUploadBox.size();i++)
	{
		String fileuploadBoxName =fileUploadBox.get(i).getAttribute("id");
		System.out.println("Data from UI"+fileuploadBoxName);
		String path="D:\\JewelleryImg.jfif";
		fileUploadBox.get(i).sendKeys(Paths.get("").toAbsolutePath().toString()+"/src/main/resources/documents/download.jpeg");
		Thread.sleep(3000);
   }
	}
	catch (Exception e) {
		e.printStackTrace();
		throw new Exception("File is not uploaded:"+e.getMessage());
	}
}


public void verifyTheUploadedImages(String details) throws Exception {
	try
	{
	HashMap<String, String> hmap = library.getFieldMap(details);
	JavascriptExecutor js = (JavascriptExecutor) library.getDriver();
	   js.executeScript("window.scrollBy(0,1000)");
	
	for(int i=0;i<UploadDocTypeColumun.size();i++)
	{
			if(UploadDocTypeColumun.get(i).getText().equalsIgnoreCase(hmap.get(UploadDocTypeColumun.get(i).getText())))
			{
				System.out.println("Image uploaded successfully"+hmap.get(UploadDocTypeColumun.get(i).getText()));
			}
			else
			{
				throw new Exception("Image is not uploaded");
			}
	}
	}
	catch (Exception e) 
	{
		e.printStackTrace();
		throw new Exception("file upload verification is failed :"+e.getMessage());

	}
	
	
	
	}
	

}