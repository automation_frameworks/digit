package com.agent.motorv4;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections.set.SynchronizedSortedSet;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DetailsPage {

	@FindBy(xpath = "//span[text()='Vehicle with Registration']")
	private WebElement radVehicleWithRegistration;

	@FindBy(xpath = "//span[text()='Brand new vehicle']")
	private WebElement radBrandNewVehicle;

	@FindBy(xpath = "//span[text()='Renewal']")
	private WebElement radRenewals;

	@FindBy(name = "vehilce_type")
	private WebElement drpVehicleType;

	@FindBy(name = "policy_cover")
	private WebElement drpPolicyCover;

	@FindBy(name = "registrationNumber")
	private WebElement txtRegistrationNumber;

	@FindBy(name = "registrationYear")
	private WebElement txtRegistrationYear;

	@FindBy(name = "registrationMonth")
	private WebElement txtRegistrationMonth;

	@FindBy(xpath = "//select[@formcontrolname='manufacutreYear']")
	private WebElement drpManufacturingYear;

	@FindBy(name = "VehicleCategory")
	private WebElement txtVehicleCategory;

	@FindBy(name = "vehicleUsage")
	private WebElement txtVehicleUsage;

	@FindBy(xpath = "//input[@name='partOfFleetBusiness']/following-sibling::div[text()='Yes']")
	private WebElement radFleetYes;

	@FindBy(xpath = "//input[@name='partOfFleetBusiness']/following-sibling::div[text()='No']")
	private WebElement radFleetNo;

	@FindBy(name = "vehicleBrand")
	private WebElement txtVehicleBrand;

	@FindBy(name = "fuelType")
	private WebElement drpFuelType;

	@FindBy(name = "vehicleVariant")
	private WebElement txtVehicleVariant;

	@FindBy(name = "city")
	private WebElement txtCity;

	@FindBy(name = "pincode")
	private WebElement txtPincode;

	@FindBy(name = "chassisg")
	private WebElement txtChassisNumber;

	@FindBy(name = "enginef")
	private WebElement txtEngineNumber;

	@FindBy(name = "unknownPolicy")
	private WebElement chkUnknownPolicyDetails;

	@FindBy(name = "previousPolicyType")
	private WebElement drpPreviousPolicyType;

	@FindBy(name = "policyExpiryDate")
	private WebElement txtPreviousPolicyExpiryDate;

	@FindBy(name = "ncb")
	private WebElement drpPreviousPolicyNCB;

	@FindBy(xpath = "//input[@name='claimLastYear']/following-sibling::div[text()='Yes']")
	private WebElement radClaimYes;

	@FindBy(xpath = "//input[@name='claimLastYear']/following-sibling::div[text()='No']")
	private WebElement radClaimNo;

	@FindBy(name = "prevInsurerName")
	private WebElement txtPreviousInsurerName;
	
	@FindBy(name="policyExpiryDate")
	private WebElement txtpolicyExpiryDate;

	@FindBy(name = "previousPolicyNumber")
	private WebElement txtPreviousPolicyNumber;

	@FindBy(xpath = "//span[text()='Get Quote']")
	private WebElement btnGetQuote;

	@FindBy(xpath = "//span[text()='Recalculate']")
	private WebElement btnRecalculate;

	@FindBy(xpath = "//div[contains(., 'CNG Kit - IMT 25') and contains(@class, 'addonTitle')]//mat-checkbox")
	private WebElement radCNGKit;

	@FindBy(xpath = "//div[contains(., 'CNG Kit - IMT 25') and contains(@class, 'addonTitle')]/following-sibling::div/input")
	private WebElement txtCNGKit;

	@FindBy(xpath = "//div[contains(., 'CNG Kit - IMT 25') and contains(@class, 'addonTitle')]/following-sibling::div/label")
	private WebElement lblCNGKitLimits;

	@FindBy(xpath = "//div[contains(., 'Electrical Accessories') and contains(@class, 'addonTitle')]//mat-checkbox")
	private WebElement radElectricalAccessories;

	@FindBy(xpath = "//div[contains(., 'Electrical Accessories') and contains(@class, 'addonTitle')]/following-sibling::div/input")
	private WebElement txtElectricalAccessories;

	@FindBy(xpath = "//div[contains(., 'Electrical Accessories') and contains(@class, 'addonTitle')]/following-sibling::div/label")
	private WebElement lblElectricalAccessoriesLimits;

	@FindBy(xpath = "//div[contains(., 'Non Electrical Accessories') and contains(@class, 'addonTitle')]//mat-checkbox")
	private WebElement radNonElectricalAccessories;

	@FindBy(xpath = "//div[contains(., 'Non Electrical Accessories') and contains(@class, 'addonTitle')]/following-sibling::div/input")
	private WebElement txtNonElectricalAccessories;

	@FindBy(xpath = "//div[contains(., 'Non Electrical Accessories') and contains(@class, 'addonTitle')]/following-sibling::div/label")
	private WebElement lblNonElectricalAccessoriesLimits;
	
	@FindBy(xpath = "//a[@id='PA Owner Driver']/../mat-checkbox")
	private WebElement chkPAOwnerDriver;

	@FindBy(xpath = "//a[@id='Motor Own Damage -  Add Ons']/../mat-checkbox")
	private WebElement chkMotorOwnDamageAddOns;

	@FindBy(xpath = "//div[contains(., 'Assistance') and contains(@class, 'addonTitle')]//mat-checkbox")
	private WebElement chkBreakdownAssistance;

	@FindBy(xpath = "//div[contains(., 'Assistance') and contains(@class, 'addonTitle')]//following-sibling::div/span")
	private WebElement lblBreakdownAssistance;

	@FindBy(xpath = "//div[contains(., 'Tyre Protect') and contains(@class, 'addonTitle')]//mat-checkbox")
	private WebElement chkTyreProtect;

	@FindBy(xpath = "//div[contains(., 'Tyre Protect') and contains(@class, 'addonTitle')]//following-sibling::div/span")
	private WebElement lblTyreProtect;

	@FindBy(xpath = "//div[contains(., 'Parts Depreciation Protect') and contains(@class, 'addonTitle')]//mat-checkbox")
	private WebElement chkPartsDepreciationProtect;

	@FindBy(xpath = "//div[contains(., 'Parts Depreciation Protect') and contains(@class, 'addonTitle')]//following-sibling::div/select")
	private WebElement drpPartsDepreciationProtect;

	@FindBy(xpath = "//div[contains(., 'Parts Depreciation Protect') and contains(@class, 'addonTitle')]//following-sibling::div/span")
	private WebElement lblPartsDepreciationProtect;

	@FindBy(xpath = "//div[contains(., 'Consumable cover') and contains(@class, 'addonTitle')]//mat-checkbox")
	private WebElement chkConsumableCover;

	@FindBy(xpath = "//div[contains(., 'Consumable cover') and contains(@class, 'addonTitle')]//following-sibling::div/span")
	private WebElement lblConsumableCover;

	@FindBy(xpath = "//div[contains(., 'Engine and Gear Box Protect') and contains(@class, 'addonTitle')]//mat-checkbox")
	private WebElement chkEngineandGearBoxProtect;

	@FindBy(xpath = "//div[contains(., 'Engine and Gear Box Protect') and contains(@class, 'addonTitle')]//following-sibling::div/span")
	private WebElement lblEngineandGearBoxProtect;

	@FindBy(xpath = "//div[contains(., 'Return to Invoice') and contains(@class, 'addonTitle')]//mat-checkbox")
	private WebElement chkReturnToInvoice;

	@FindBy(xpath = "//div[contains(., 'Return to Invoice') and contains(@class, 'addonTitle')]//following-sibling::div/span")
	private WebElement lblReturnToInvoice;

	@FindBy(xpath = "//input[@formcontrolname='idv']")
	private WebElement txtIDVApplied;

	@FindBy(name = "idvButton")
	private WebElement btnIDVUpdate;

	@FindBy(xpath = "//input[@formcontrolname='idv']/following-sibling::span")
	private WebElement lblIDVLimits;

	@FindBy(xpath = "//img[@src='https://d2h44aw7l5xdvz.cloudfront.net/assets/gif/dot-animation-transparent-2.gif']")
	private WebElement imgDotAnimation;

	@FindBy(xpath = "//div[text()='Motor Own Damage']/../div[2]/span[2]")
	private WebElement lblMotorOwnDamagePremium;

	@FindBy(xpath = "//div[text()='Net Premium']/../div[2]/span[2]")
	private WebElement lblNetPremium;

	@FindBy(xpath = "//div[text()='Total Premium']/../div[2]/span[2]")
	private WebElement lblTotalPremium;

	@FindBy(xpath = "//div[@class='discountDIV']")
	private WebElement lblDigitCurrentDiscount;

	@FindBy(xpath = "//button[contains(text(),'Save Quote')]")
	private WebElement btnSaveQuote;

	@FindBy(xpath = "//button[contains(text(),'Generate Policy') or contains(text(),'Submit')]")
	private WebElement btnGeneratePolicy;

	@FindBy(id = "firstName")
	private WebElement txtFirstName;

	@FindBy(xpath = "//img[@alt='DigitPlus']")
	private WebElement imgDigitPlus;

	@FindBy(xpath = "//label[contains(text(),'is in edit mode. Would you like to close this Quote and continue ?')]")
	private WebElement lblQuoteNumber;

	@FindBy(xpath = "//button[contains(text(),'No')]")
	private WebElement btnQuoteNumberNo;

	public com.utilities.Library library = new com.utilities.Library();

	public void enterVehicleDetails(String details) throws Exception {
		try {
			library.waitForElementVisible(drpVehicleType, 20, 500);
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "vehicletype":
					System.out.println("Vehicle type : " + hmap.get(key));
					library.selectDropDownValue(drpVehicleType, hmap.get(key));
					break;
				case "policytype":
					System.out.println("Vehicle type : " + hmap.get(key));
					library.selectDropDownValue(drpPolicyCover, hmap.get(key));
					break;
				case "vehiclewithregistration":
					if (hmap.get(key).equalsIgnoreCase("true"))
						radVehicleWithRegistration.click();
					break;
				case "brandnewvehicle":
					if (hmap.get(key).equalsIgnoreCase("true"))
						radBrandNewVehicle.click();
					break;
				case "registrationnumber":
				case "rtocode":
					txtRegistrationNumber.sendKeys(hmap.get(key));
					break;
				case "vehiclecategory":
					library.selectDropDownValue(txtVehicleCategory, hmap.get(key));
					break;
				// case "permittype":
				// library.selectDropDownValue(drpPermitType, hmap.get(key));
				// break;
				case "usagetype":
					library.selectDropDownValue(txtVehicleUsage, hmap.get(key));
					break;
				case "vehiclemake":
					library.selectDropDownValue(txtVehicleBrand, hmap.get(key));
					break;
				case "fueltype":
					library.selectDropDownValue(drpFuelType, hmap.get(key));
					break;
				case "vehiclevariant":
					library.waitForDigitLoad();
					library.selectDropDownValue(txtVehicleVariant, hmap.get(key));
					break;
				case "registrationyear":
					library.selectDropDownValue(txtRegistrationYear, hmap.get(key));
					break;
				case "registrationmonth":
					library.selectDropDownValue(txtRegistrationMonth, hmap.get(key));
					break;
				case "manufacturingyear":
					library.selectDropDownValue(drpManufacturingYear, hmap.get(key));
					break;
				case "city":
					library.selectDropDownValue(txtCity, hmap.get(key));
					break;
				case "pincode":
					Thread.sleep(3000);
					library.waitForElementToBeClickable(txtPincode, 10, 500);
					library.selectDropDownValue(txtPincode, hmap.get(key));
					break;
				case "chassisnumber":
					library.selectDropDownValue(txtChassisNumber, hmap.get(key));
					txtChassisNumber.sendKeys(Keys.TAB);
					library.waitForDigitLoad();
					break;
				case "enginenumber":
					library.selectDropDownValue(txtEngineNumber, hmap.get(key));
					txtEngineNumber.sendKeys(Keys.TAB);
					library.waitForDigitLoad();
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}
			// enterMissingVehicleDetails(details);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the vehicle details : " + e.getMessage());
		}
	}

	public void enterPreviousPolicyDetails(String details) throws Exception {

		try {
			library.waitForElementVisible(drpVehicleType, 15, 500);
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "previouspolicytype":
					library.selectDropDownValue(drpPreviousPolicyType, hmap.get(key));
					break;
				case "previousinsurername":
					library.selectDropDownValue(txtPreviousInsurerName, hmap.get(key));
					break;
				case "previouspolicyexpirydate":
					//library.enterDateV4(txtpolicyExpiryDate, hmap.get(key));
					break;
				case "previouspolicynumber":
					txtPreviousPolicyNumber.sendKeys(library.getRandomValue("AN | 10"));
					break;
				case "existingpolicyncb":
					library.selectDropDownValue(drpPreviousPolicyNCB, hmap.get(key));
					break;
				case "claim":
					if (hmap.get(key).toLowerCase().equals("yes")) {
						library.scrollElementIntoView(radClaimNo, library.getDriver());
						radClaimYes.click();
					} else {
						library.scrollElementIntoView(radClaimNo, library.getDriver());
						radClaimNo.click();
					}
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the previous policy details : " + e.getMessage());
		}

	}

	public void performAction(String action) throws Exception {
		try {
			switch (action.replaceAll(" ", "").toLowerCase()) {
			case "getquote":
				btnGetQuote.click();
				break;
			case "recalculate":
				System.out.println("Recalculate is clicked");
				btnRecalculate.click();
				break;
			case "savequote":
				Thread.sleep(1000);
				btnSaveQuote.click();
				library.waitForElementVisible(btnGeneratePolicy, 60, 1000);
				break;
			default:
				throw new Exception("Implementation for the action '" + action + "' is not found");

			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while clicking the button : " + e.getMessage());
		}
	}

	public void selectOwnDamageCoverages(String details) throws Exception {
		try {
			library.waitForElementVisible(radElectricalAccessories, 20, 500);
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "electricalaccessories":
					txtElectricalAccessories.clear();
					txtElectricalAccessories.sendKeys(hmap.get(key));
					radElectricalAccessories.click();
					break;
				case "nonelectricalaccessories":
					txtNonElectricalAccessories.clear();
					txtNonElectricalAccessories.sendKeys(hmap.get(key));
					radNonElectricalAccessories.click();
					break;
				case "cngkit":
					txtCNGKit.clear();
					txtCNGKit.sendKeys(hmap.get(key));
					radCNGKit.click();
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while selecting the Own Damage Coverages : " + e.getMessage());
		}
	}
	
	public void uncheckPAOwnerDriver() throws Exception
	{
		try {
			library.scrollElementIntoView(chkPAOwnerDriver, library.getDriver());
			chkPAOwnerDriver.click();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception("Error while selecting the unchecking PA Owner driver : " + e.getMessage());
		}
	}

	public void selectOwnDamageAddOnsCoverages(String details) throws Exception {
		try {
			library.waitForElementVisible(radElectricalAccessories, 20, 500);
			HashMap<String, String> hmap = library.getFieldMap(details);
			System.out.println(details);
			// boolean flag = false;
			System.out.println(chkMotorOwnDamageAddOns.getAttribute("class"));

			List<WebElement> list = library.getDriver().findElements(By.xpath("//div[@id='ADDONS']/div/div[1]"));
			for (WebElement temp : list) {
				temp.getText();
			}
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				System.out.println(key);
				try {
					switch (key.replaceAll(" ", "").toLowerCase()) {
					case "partsdepreciationprotect":
						if (chkMotorOwnDamageAddOns.getAttribute("class")
								.contains("mat-checkbox-anim-checked-unchecked")
								|| chkMotorOwnDamageAddOns.getAttribute("class").contains("ng-untouched")) {
							library.scrollElementIntoView(chkMotorOwnDamageAddOns, library.getDriver());
							chkMotorOwnDamageAddOns.click();
						}
						library.scrollElementIntoView(chkPartsDepreciationProtect, library.getDriver());
						chkPartsDepreciationProtect.click();
						library.waitForDigitLoad();
						library.selectDropDownValue(drpPartsDepreciationProtect, "Cover only 2 claims per year");
						break;
					case "breakdownassistance":
						if (chkMotorOwnDamageAddOns.getAttribute("class")
								.contains("mat-checkbox-anim-checked-unchecked")
								|| chkMotorOwnDamageAddOns.getAttribute("class").contains("ng-untouched")) {
							library.scrollElementIntoView(chkMotorOwnDamageAddOns, library.getDriver());
							chkMotorOwnDamageAddOns.click();
						}
						library.scrollElementIntoView(chkBreakdownAssistance, library.getDriver());
						chkBreakdownAssistance.click();
						System.out.println("Break down assistace os clicked");
						break;
					case "tyreprotect":
						if (chkMotorOwnDamageAddOns.getAttribute("class")
								.contains("mat-checkbox-anim-checked-unchecked")
								|| chkMotorOwnDamageAddOns.getAttribute("class").contains("ng-untouched")) {
							library.scrollElementIntoView(chkMotorOwnDamageAddOns, library.getDriver());
							chkMotorOwnDamageAddOns.click();
						}
						library.scrollElementIntoView(chkTyreProtect, library.getDriver());
						chkTyreProtect.click();
						break;
					case "consumablecover":
						if (chkMotorOwnDamageAddOns.getAttribute("class")
								.contains("mat-checkbox-anim-checked-unchecked")
								|| chkMotorOwnDamageAddOns.getAttribute("class").contains("ng-untouched")) {
							library.scrollElementIntoView(chkMotorOwnDamageAddOns, library.getDriver());
							chkMotorOwnDamageAddOns.click();
						}
						library.scrollElementIntoView(chkConsumableCover, library.getDriver());
						chkConsumableCover.click();
						break;
					case "engineandgearboxprotect":
						if (chkMotorOwnDamageAddOns.getAttribute("class")
								.contains("mat-checkbox-anim-checked-unchecked")
								|| chkMotorOwnDamageAddOns.getAttribute("class").contains("ng-untouched")) {
							library.scrollElementIntoView(chkMotorOwnDamageAddOns, library.getDriver());
							chkMotorOwnDamageAddOns.click();
						}
						library.scrollElementIntoView(chkEngineandGearBoxProtect, library.getDriver());
						chkEngineandGearBoxProtect.click();
						break;
					case "returntoinvoice":
						if (chkMotorOwnDamageAddOns.getAttribute("class")
								.contains("mat-checkbox-anim-checked-unchecked")
								|| chkMotorOwnDamageAddOns.getAttribute("class").contains("ng-untouched")) {
							library.scrollElementIntoView(chkMotorOwnDamageAddOns, library.getDriver());
							chkMotorOwnDamageAddOns.click();
						}
						library.scrollElementIntoView(chkReturnToInvoice, library.getDriver());
						chkReturnToInvoice.click();
						break;
					default:
						throw new Exception("Implementation for the field '" + key + "' is not found");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while selecting the Own Damage Coverages : " + e.getMessage());
		}
	}

	public void enterIDV(String idv) throws Exception {
		try {
			library.waitForElementVisible(radElectricalAccessories, 20, 500);
			System.out.println("Entering IDV");
			library.scrollElementIntoView(txtIDVApplied, library.getDriver());
			txtIDVApplied.clear();
			txtIDVApplied.click();
			Thread.sleep(5000);
			txtIDVApplied.sendKeys(idv);
			// JavascriptExecutor js = (JavascriptExecutor)library.getDriver();
			// js.executeScript("arguments[0].setAttribute('value', '" + idv +"')",
			// txtIDVApplied);
			// Actions actions = new Actions(library.getDriver());
			// actions.keyUp(Keys.TAB);
			// txtIDVApplied.sendKeys(idv.trim());
			Robot robot = new Robot();
			System.out.println(idv);
			// for (char id : idv.toCharArray()) {
			// System.out.println(id);
			// switch (id) {
			// case '0':
			// robot.keyPress(KeyEvent.VK_0);
			// robot.keyRelease(KeyEvent.VK_0);
			// break;
			// case '1':
			// robot.keyPress(KeyEvent.VK_1);
			// robot.keyRelease(KeyEvent.VK_1);
			// break;
			// case '2':
			// robot.keyPress(KeyEvent.VK_2);
			// robot.keyRelease(KeyEvent.VK_2);
			// break;
			// case '3':
			// robot.keyPress(KeyEvent.VK_3);
			// robot.keyRelease(KeyEvent.VK_3);
			// break;
			// case '4':
			// robot.keyPress(KeyEvent.VK_4);
			// robot.keyRelease(KeyEvent.VK_4);
			// break;
			// case '5':
			// robot.keyPress(KeyEvent.VK_5);
			// robot.keyRelease(KeyEvent.VK_5);
			// break;
			// case '6':
			// robot.keyPress(KeyEvent.VK_6);
			// robot.keyRelease(KeyEvent.VK_6);
			// break;
			// case '7':
			// robot.keyPress(KeyEvent.VK_7);
			// robot.keyRelease(KeyEvent.VK_7);
			// break;
			// case '8':
			// robot.keyPress(KeyEvent.VK_8);
			// robot.keyRelease(KeyEvent.VK_8);
			// break;
			// case '9':
			// robot.keyPress(KeyEvent.VK_9);
			// robot.keyRelease(KeyEvent.VK_9);
			// break;
			// }
			// }
			// Release SHIFT key to release upper case effect
			// robot.keyRelease(KeyEvent.VK_SHIFT);
			// robot.keyPress(KeyEvent.VK_1);
			btnIDVUpdate.click();
			//library.waitForVisibilityOfTheElement(btnRecalculate);
			// library.waitForInVisibilityOfTheElement("//img[@src='https://d2h44aw7l5xdvz.cloudfront.net/assets/gif/dot-animation-transparent-2.gif']");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the IDV : " + e.getMessage());
		}
	}

	public HashMap<String, String> scrapPremium(String serialNumber) throws Exception {
		try {
			HashMap<String, String> premiumMap = new LinkedHashMap<String, String>();
			premiumMap.put("S.No.", serialNumber);
			premiumMap.put("Quote Number", getQuoteNumber());
			premiumMap.put("Motor Own Damage Premium", lblMotorOwnDamagePremium.getText());
			premiumMap.put("Net Premium", lblNetPremium.getText());
			premiumMap.put("Total Premium", lblTotalPremium.getText());
			premiumMap.put("Digit Current Discount", lblDigitCurrentDiscount.getText().split("%")[0]);
			Thread.sleep(2000);
			library.scrollElementIntoView(chkMotorOwnDamageAddOns, library.getDriver());
			chkMotorOwnDamageAddOns.click();
			chkMotorOwnDamageAddOns.click();
			if (chkMotorOwnDamageAddOns.getAttribute("class").contains("mat-checkbox-anim-checked-unchecked")
					|| chkMotorOwnDamageAddOns.getAttribute("class").contains("ng-untouched")) {
				library.scrollElementIntoView(chkMotorOwnDamageAddOns, library.getDriver());
				chkMotorOwnDamageAddOns.click();
			}
			List<WebElement> list = new LinkedList<WebElement>();
			list = library.getDriver().findElements(By.xpath("//div[@id='ADDONS']/div//mat-checkbox"));
			System.out.println("List of add on size : " + list.size());
			for (WebElement temp : list) {
				System.out.println("List " + temp.getText());
			}
			//btnRecalculate.click();
			//library.waitForElementVisible(btnRecalculate, 15, 1000);
			try {
				premiumMap.put("Breakddown Assistance", lblBreakdownAssistance.getText());
			} catch (NoSuchElementException ne) {
				premiumMap.put("Breakddown Assistance", "0");
			}
			try {
				premiumMap.put("Tyre Protect", lblTyreProtect.getText());
			} catch (NoSuchElementException ne) {
				premiumMap.put("Tyre Protect", "0");
			}
			try {
				premiumMap.put("Parts Depreciation Protect", lblPartsDepreciationProtect.getText());
			} catch (NoSuchElementException ne) {
				premiumMap.put("Parts Depreciation Protect", "0");
			}
			try {
				premiumMap.put("Consumable Cover", lblConsumableCover.getText());
			} catch (NoSuchElementException ne) {
				premiumMap.put("Consumable Cover", "0");
			}
			try {
				premiumMap.put("Engine and Gear Box Protect", lblEngineandGearBoxProtect.getText());
			} catch (NoSuchElementException ne) {
				premiumMap.put("Engine and Gear Box Protect", "0");
			}

			return premiumMap;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while getting the premium details : " + e.getMessage());
		}
	}

	public void enterCustomerDetails(String details) throws Exception {
		try {
			library.waitForElementVisible(txtFirstName, 15, 500);
			HashMap<String, String> hmap = library.getFieldMap(details);
			for (String key : hmap.keySet()) {
				library.waitForDigitLoad();
				switch (key.replaceAll(" ", "").toLowerCase()) {
				case "fullname":
					txtFirstName.sendKeys(hmap.get(key));
					break;
				default:
					throw new Exception("Implementation for the field '" + key + "' is not found");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the customer details : " + e.getMessage());
		}
	}

	public HashMap<String, String> scrapLimits(String serialNumber) throws Exception {
		try {
			HashMap<String, String> premiumMap = new LinkedHashMap<String, String>();
			premiumMap.put("S.No.", serialNumber);
			PageFactory.initElements(library.getDriver(), DetailsPage.class);
			Thread.sleep(1000);
//			library.scrollElementIntoView(radCNGKit, library.getDriver());
//			radCNGKit.click();
//			performAction("Recalculate");
//			String limit = lblCNGKitLimits.getText();
//			premiumMap.put("CNG Min", limit.split("to")[0].replaceAll("₹", "").replaceAll("\\(", "")
//					.replaceAll("\\)", "").replaceAll("\\*", "").trim());
//			premiumMap.put("CNG Max",
//					limit.split("to")[1].replaceAll("₹", "").replaceAll("\\(", "").replaceAll("\\)", "").trim());
//			library.waitForElementVisible(btnRecalculate, 20, 100);
			library.scrollElementIntoView(radElectricalAccessories, library.getDriver());
			radElectricalAccessories.click();
			performAction("Recalculate");
			Thread.sleep(5000);
			library.waitForElementVisible(lblElectricalAccessoriesLimits, 30, 500);
			String limit = lblElectricalAccessoriesLimits.getText();
			premiumMap.put("Electrical Min", limit.split("to")[0].replaceAll("₹", "").replaceAll("\\(", "")
					.replaceAll("\\)", "").replaceAll("\\*", "").trim());
			premiumMap.put("Electrical Max",
					limit.split("to")[1].replaceAll("₹", "").replaceAll("\\(", "").replaceAll("\\)", "").trim());
			//library.waitForElementVisible(btnRecalculate, 20, 100);
			library.scrollElementIntoView(radNonElectricalAccessories, library.getDriver());
			radNonElectricalAccessories.click();
			performAction("Recalculate");
			library.waitForElementVisible(lblElectricalAccessoriesLimits, 30, 500);
			limit = lblNonElectricalAccessoriesLimits.getText();
			premiumMap.put("Non Electrical Min", limit.split("to")[0].replaceAll("₹", "").replaceAll("\\(", "")
					.replaceAll("\\)", "").replaceAll("\\*", "").trim());
			premiumMap.put("Non Electrical Max",
					limit.split("to")[1].replaceAll("₹", "").replaceAll("\\(", "").replaceAll("\\)", "").trim());
			//library.waitForElementVisible(btnRecalculate, 20, 100);
			library.scrollElementIntoView(txtIDVApplied, library.getDriver());
			limit = lblIDVLimits.getText();
			premiumMap.put("IDV Min", limit.split("to")[0].split(":")[1].replaceAll("₹", "").replaceAll("\\(", "")
					.replaceAll("\\)", "").trim());
			premiumMap.put("IDV Max",
					limit.split("to")[1].replaceAll("₹", "").replaceAll("\\(", "").replaceAll("\\)", "").trim());
			premiumMap.put("Quote Number", getQuoteNumber());
			return premiumMap;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while getting the premium details : " + e.getMessage());
		}
	}

	private String getQuoteNumber() throws Exception {
		try {
			imgDigitPlus.click();
			library.waitForElementVisible(lblQuoteNumber, 30, 1000);
			String quoteNumber = (lblQuoteNumber.getText()).split("Quote - ")[1].split(" ")[0];
			btnQuoteNumberNo.click();
			return quoteNumber;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error while entering the customer details : " + e.getMessage());
		}
	}
}
