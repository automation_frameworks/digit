/**
 * rsr 
 *
 *Aug 6, 2016
 */
package com.utilities;

import java.io.FileInputStream;
import java.io.FileReader;
import java.util.Properties;

import org.apache.log4j.Level;

import com.cucumber.framework.utility.ResourceHelper;

/**
 * @author rsr
 *
 *         Aug 6, 2016
 */
public class PropertyFileReader {

	private static Properties prop = null;

	public PropertyFileReader() {
		prop = new Properties();
		try {
			/*
			 * prop.load(ResourceHelper .getResourcePathInputStream("configfile/" +
			 * "config.properties"));
			 * 
			 * configfile/config.properties
			 */
			//prop.load(new FileInputStream(".\\src\\main\\resources\\configfile\\config.properties"));
			prop.load(ResourceHelper.getResourcePathInputStream("configfile/" + "config.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Searches for the key and returns its value if found in the default file location. If the key is not not found, it returns null.
	 *
	 * @param key Key to be searched
	 * @return Value of the given key
	 * @author Deepjyoti Barman
	 * @since January 20, 2021
	 */
	public String getCustomProperty(String key) {
		try {
			return prop.getProperty(key).trim();
		} catch (NullPointerException npe) {
			return null;
		}
	}

	/**
	 * Searches for the key and returns its value if found in the given file location. If the key is not not found, it returns null.
	 *
	 * @param key Key to be searched
	 * @param fileLoc Absolute or relative path of the .properties file disk drive
	 * @return Value of the given key
	 * @author Deepjyoti Barman
	 * @since January 20, 2021
	 */
	public String getCustomProperty(String key, String fileLoc) {
		try {
			prop.load(new FileReader(fileLoc));
			return prop.getProperty(key).trim();
		} catch (NullPointerException npe) {
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getUserName() {
		return prop.getProperty("Username");
	}
	
	public String getPassword() {
		return prop.getProperty("Password");
	}

	public String getHealthUserName() {
		return prop.getProperty("Health_Username");
	}
	
	public String getHealthPassword() {
		return prop.getProperty("Health_Password");
	}
	
	public String getGMCSalesPreprodHealthPlusUserName() {
		return prop.getProperty("GMC_Sales_Preprod_HealthPlus_Username");
	}
	
	public String getGMCSalesPreprodHealthPlusPassword() {
		return prop.getProperty("GMC_Sales_Preprod_HealthPlus_Password");
	}
	
	public String getGMCOpsPreprodDigitHealthUserName() {
		return prop.getProperty("GMC_Ops_Preprod_DigitHealth_Username");
	}
	
	public String getGMCOpsPreprodDigitHealthPassword() {
		return prop.getProperty("GMC_Ops_Preprod_DigitHealth_Password");
	}
	
	public String getGPASMEPreprodHealthPlusUserName() {
		return prop.getProperty("GPA_SME_Preprod_HealthPlus_Username");
	}
	
	public String getGPASMEPreprodHealthPlusPassword() {
		return prop.getProperty("GPA_SME_Preprod_HealthPlus_Password");
	}
	
	public String getIMD_Code_For_GMC() {
		return prop.getProperty("IMD_For_GMC");
	}

	public String getWebsite() {
		return prop.getProperty("Website");
	}

	public int getPageLoadTimeOut() {
		return Integer.parseInt(prop.getProperty("PageLoadTimeOut"));
	}

	public int getImplicitWait() {
		return Integer.parseInt(prop.getProperty("ImplcitWait"));
	}

	public int getExplicitWait() {
		return Integer.parseInt(prop.getProperty("ExplicitWait"));
	}

	public String getDbType() {
		return prop.getProperty("DataBase.Type");
	}

	public String getDbConnStr() {
		return prop.getProperty("DtaBase.ConnectionStr");
	}

	public String getBrowser() {
		return prop.getProperty("Browser").toString();
	}

	public String getEnvironment() {
		return prop.getProperty("Environment").toString();
	}
	
	public String getEnvironment_For_GMC_Large() {
		return prop.getProperty("Environment_For_GMC_Large").toString();
	}
	
	public String getEnvironment_For_GMC() {
		return prop.getProperty("Environment_For_GMC").toString();
	}

	public String getPaymentType() {
		return prop.getProperty("PaymentType").toString();
	}

	public String getHeadLess() {
		try {
			return prop.getProperty("Headless").toString();
		} catch (NullPointerException ne) {
			return "null";
		}
	}

	public String getFailThreshold() {
		try {
			return prop.getProperty("FailThreshold").toString();
		} catch (NullPointerException ne) {
			return "null";
		}
	}

	public String getBrowserClose() {
		try {
			return prop.getProperty("BrowserClose").toString();
		} catch (NullPointerException ne) {
			return "null";
		}
	}

	public String getDigitCareUserName() {
		return prop.getProperty("DigitCare_Username");
	}

	public String getDigitCarePassword() {
		return prop.getProperty("DigitCare_Password");
	}

	public String getDBName() {
		return prop.getProperty("DB_Name").trim().toString().toLowerCase();
	}

	public String getNonAbsUatUrl() {
		return prop.getProperty("NonABS_UAT_Url").trim().toString();
	}

	public String getNonAbsPreprodUrl() {
		return prop.getProperty("NonABS_Preprod_Url").trim().toString();
	}

	public String getNonAbsProdUrl() {
		return prop.getProperty("NonABS_Prod_Url").trim().toString();
	}
	
	public String getAbsPreprodRAPUrl() {
		return prop.getProperty("ABS_Preprod_RAP_URL").trim().toString();
	}

	public String getAbsPreprodRAPUsername() {
		return prop.getProperty("ABS_Preprod_RAP_Username").trim().toString();
	}
	
	public String getAbsPreprodRAPPassword() {
		return prop.getProperty("ABS_Preprod_RAP_Password").trim().toString();
	}
	
	public String getMotorAPIUsername() {
		return prop.getProperty("MotorAPI_Username").trim().toString();
	}
	
	public String getMotorAPIPassword() {
		return prop.getProperty("MotorAPI_Password").trim().toString();
	}
	
	public String getNonAbsUatUsername() {
		return prop.getProperty("NonABS_UAT_Username").trim().toString();
	}

	public String getNonAbsUatPassword() {
		return prop.getProperty("NonABS_UAT_Password").trim().toString();
	}
	
	public String getNonAbsProdUsername() {
		return prop.getProperty("NonABS_Prod_Username").trim().toString();
	}

	public String getNonAbsPreprodUsername() {
		return prop.getProperty("NonABS_Preprod_Username").trim().toString();
	}

	public String getNonAbsProdPassword() {
		return prop.getProperty("NonABS_Prod_Password").trim().toString();
	}

	public String getNonAbsPreprodPassword() {
		return prop.getProperty("NonABS_Preprod_Password").trim().toString();
	}

	public Level getLoggerLevel() {

		switch (prop.getProperty("Logger.Level")) {

		case "DEBUG":
			return Level.DEBUG;
		case "INFO":
			return Level.INFO;
		case "WARN":
			return Level.WARN;
		case "ERROR":
			return Level.ERROR;
		case "FATAL":
			return Level.FATAL;
		}
		return Level.ALL;
	}

	public static String getReportConfigPath() {
		String reportConfigPath = prop.getProperty("reportConfigPath");
		if (reportConfigPath != null)
			return reportConfigPath;
		else
			throw new RuntimeException(
					"Report Config Path not specified in the Configuration.properties file for the Key:reportConfigPath");
	}
}
