package com.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.cucumber.framework.utility.ResourceHelper;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Library {

	static WebDriver driver;
	@FindBy(xpath = "//span[@class='customLoader']/img")
	private WebElement spnDigitCareLoader;
	PropertyFileReader prop;
	String environment;

	public static HashMap<String, String> staticMap = new HashMap<>();
	public static SoftAssert softassert = new SoftAssert();
	// public static List<String> loaderList;

	public HashMap<String, String> getFieldMap(String details) {
		HashMap<String, String> dataMap = new LinkedHashMap<>();
		details = details.trim();
		if (details.length() == 0)
			return dataMap;
		String[] dataSet = details.split("#");
		for (String temp : dataSet) {
			dataMap.put(temp.split(":=")[0].trim(), temp.split(":=")[1].trim());
		}
		return dataMap;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriverNull() {
		driver = null;
	}

	public WebDriver DriverSetup() throws Exception {
		try {
			prop = new PropertyFileReader();
			switch (prop.getBrowser().toString().toUpperCase()) {
			case "FIREFOX":
				// System.out.println("Firefox is opened");
				System.setProperty("webdriver.gecko.driver", ".\\src\\main\\resources\\driver\\geckodriver.exe");
				DesiredCapabilities capabilities = DesiredCapabilities.firefox();
				capabilities.setCapability("marionette", true);
				driver = new FirefoxDriver();// new FirefoxDriver(capabilities);
				break;

			case "CHROME":
				HashMap<String, String> chromePrefs = new HashMap<String, String>();
				chromePrefs.put("profile.default_content_settings.popups", "0");
				chromePrefs.put("safebrowsing.enabled", "false");
				System.out.println("Execution Environment:" + System.getProperty("os.name"));
				System.setProperty("webdriver.chrome.silentOutput","true");
				if(System.getProperty("os.name").contains("Windows"))
					System.setProperty("webdriver.chrome.driver","./src/main/resources/driver/chromedriver.exe");
				else
					System.setProperty("webdriver.chrome.driver",
							ResourceHelper.getResourcePath(".\\src\\main\\resources\\driver\\chromedriver_linux.exe").substring(1));
				String downloadFilepath = Paths.get("").toAbsolutePath().toString()+"\\DownloadFiles\\";
				System.setProperty("webdriver.chrome.driver",
						ResourceHelper.getResourcePath("resources/driver/chromedriver.exe").substring(1));
				chromePrefs.put("download.default_directory", downloadFilepath);
				if(System.getProperty("os.name").contains("Windows"))
					System.setProperty("webdriver.chrome.driver",
							Paths.get("").toAbsolutePath().toString()+"/target/test-classes/driver/chromedriver.exe");
				else
					System.setProperty("webdriver.chrome.driver",
							Paths.get("").toAbsolutePath().toString()+"/chromedriver_linux.exe");
				System.out.println(Paths.get("").toAbsolutePath().toString()+"/chromedriver_linux.exe");
				ChromeOptions options = new ChromeOptions();
				options.setExperimentalOption("prefs", chromePrefs); 
				options.addArguments("--start-maximized");
				options.addArguments("--disable-web-security");
				if(!System.getProperty("os.name").contains("Windows") || prop.getHeadLess().trim().equals("true"))
				{
					options.addArguments("--headless");
					options.addArguments("--disable-gpu");
					options.addArguments("--window-size=1280,800");
				}
				//					 options.addArguments("--auto-open-devtools-for-tabs"); // uncommand to run
				//					 options.addArguments("")
				// chrome with Dev tool option;
				options.setExperimentalOption("prefs", chromePrefs);
				DesiredCapabilities cap = DesiredCapabilities.chrome();
				cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				cap.setCapability(ChromeOptions.CAPABILITY, options);
				driver = new ChromeDriver(options);
				break;
			case "IE":
				System.setProperty("webdriver.ie.driver", "C:\\ARTEMIS\\references\\IEDriverServer.exe");
				DesiredCapabilities iecapabilities = DesiredCapabilities.internetExplorer();
				iecapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
				driver = new InternetExplorerDriver(iecapabilities);
				break;

			case "EDGE":
				System.setProperty("webdriver.edge.driver", "C:\\ARTEMIS\\references\\MicrosoftWebDriver.exe");
				DesiredCapabilities capabilities1 = DesiredCapabilities.edge();
				driver = new EdgeDriver(capabilities1);
				break;
			case "SAFARI":
				driver = new SafariDriver();
				break;
			}
			return driver;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to initialise the driver");
		}
	}


	public WebDriver DriverSetupForRAP() throws Exception {
		try {
			PropertyFileReader prop = new PropertyFileReader();
			switch (prop.getBrowser().toString().toUpperCase()) {
			case "FIREFOX":
				// System.out.println("Firefox is opened");
				System.setProperty("webdriver.gecko.driver", "C:\\ARTEMIS\\references\\geckodriver.exe");
				DesiredCapabilities capabilities = DesiredCapabilities.firefox();
				capabilities.setCapability("marionette", true);
				driver = new FirefoxDriver();// new FirefoxDriver(capabilities);
				break;

			case "CHROME":
				HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
				chromePrefs.put("profile.default_content_settings.popups", 0);
				System.setProperty("webdriver.chrome.driver",
						ResourceHelper.getResourcePath("driver/chromedriver.exe").substring(1));
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--start-maximized");
				options.addArguments("--start-fullscreen");
				if (!System.getProperty("os.name").contains("Windows"))
					System.setProperty("webdriver.chrome.driver",
							Paths.get("").toAbsolutePath().toString() + "/chromedriver_linux.exe");
				if (!System.getProperty("os.name").contains("Windows") || prop.getHeadLess().equals("true")) {
					options.addArguments("--headless");
					options.addArguments("--disable-gpu");
					options.addArguments("--window-size=1280,800");
				}
				// options.addArguments("--auto-open-devtools-for-tabs"); // uncommand to run
				// chrome with Dev tool option;
				options.setExperimentalOption("prefs", chromePrefs);
				DesiredCapabilities cap = DesiredCapabilities.chrome();
				cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				cap.setCapability(ChromeOptions.CAPABILITY, options);
				driver = new ChromeDriver(options);
				break;
			case "IE":
				System.setProperty("webdriver.ie.driver", "C:\\ARTEMIS\\references\\IEDriverServer.exe");
				DesiredCapabilities iecapabilities = DesiredCapabilities.internetExplorer();
				iecapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
						true);
				driver = new InternetExplorerDriver(iecapabilities);
				break;

			case "EDGE":
				System.setProperty("webdriver.edge.driver", "C:\\ARTEMIS\\references\\MicrosoftWebDriver.exe");
				DesiredCapabilities capabilities1 = DesiredCapabilities.edge();
				driver = new EdgeDriver(capabilities1);
				break;

			case "SAFARI":
				driver = new SafariDriver();
				break;
			}
			return driver;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Unable to initialise the driver");
		}
	}

	public String setEnvironment() throws Exception {

		try {
			prop = new PropertyFileReader();
			switch (prop.getEnvironment().toString().toLowerCase()) {
			case "uat":
			case "uat-plus":
				Library.staticMap.put("server", "https://uat-plus.godigit.com");
				break;
			case "preprod-plus":
			case "preprodplus":
			case "preprod":
				Library.staticMap.put("server", "https://preprod-plusui.godigit.com");
				break;
			case "preprodhealth":
				Library.staticMap.put("server", "https://preprod-health-plus.godigit.com");
				break;
			case "uat-qnb":
			case "uatqnb":
				Library.staticMap.put("server", "https://uat-qnb.godigit.com");
				break;
			case "preprodtest":
			case "preprodtest-plus":
				Library.staticMap.put("server", "https://preprodtest-plus.godigit.com");
				break;
			case "plus":
				Library.staticMap.put("server", "https://plus.godigit.com");
				break;
			default:
				throw new Exception("The environment mentioned '" + prop.getEnvironment() + "' is not valid");
			}
		} catch (Exception e) {
			throw new Exception("Unable to decide the server URL because of the error : " + e.getMessage());
		}
		return prop.getEnvironment().toString().toLowerCase();
	}

	public String setExecutionEnvironment(String environment) throws Exception {

		try {
			prop = new PropertyFileReader();
			switch (environment.toLowerCase()) {
			case "uat":
			case "uat-plus":
				Library.staticMap.put("server", "https://uat-plus.godigit.com");
				break;
			case "preprod-plus":
			case "preprodplus":
			case "preprod":
				Library.staticMap.put("server", "https://preprod-plusui.godigit.com");
				break;
			case "preprodhealth":
				Library.staticMap.put("server", "https://preprod-health-plus.godigit.com");
				break;
			case "preprod-digithealth":
				Library.staticMap.put("server", "https://preprod-digithealth.godigit.com");
				break;
			case "uat-qnb":
			case "uatqnb":
				Library.staticMap.put("server", "https://uat-qnb.godigit.com");
				break;
			case "preprodtest":
			case "preprodtest-plus":
				Library.staticMap.put("server", "https://preprodtest-plus.godigit.com");
				break;
			case "plus":
				Library.staticMap.put("server", "https://plus.godigit.com");
				break;
			default:
				throw new Exception("The environment mentioned '" + prop.getEnvironment() + "' is not valid");
			}
		} catch (Exception e) {
			throw new Exception("Unable to decide the server URL because of the error : " + e.getMessage());
		}
		return prop.getEnvironment().toString().toLowerCase();
	}

	public String setEnvironmentForGMC() throws Exception {
		try {
			prop = new PropertyFileReader();
			//				switch (prop.getEnvironment().toString().toLowerCase()) {
			System.out.println(System.getProperty("environment"));
			if(System.getProperty("environment") == null ||System.getProperty("environment").equals(""))
			{
				PropertyFileReader prop = new PropertyFileReader();
				environment = prop.getEnvironment().toString();
			}
			else
				environment = System.getProperty("environment");
			switch (environment.toLowerCase()) {
			case "uat":
			case "uatplus":
			case "uat-plus":
				Library.staticMap.put("server", "https://uat-plus.godigit.com");
				break;
			case "uat-plus1":
			case "uatplus1":
				Library.staticMap.put("server", "https://uat-plus1.godigit.com");
				break;
			case "preprod-plus":
			case "preprodplus":
			case "preprod":
				Library.staticMap.put("server", "http://preprod-plusui.godigit.com");
				break;
			case "preprodhealth":
				Library.staticMap.put("server", "http://preprod-plusui.godigit.com");
				break;
			case "uat-qnb":
			case "uatqnb":
				Library.staticMap.put("server", "http://uat-qnb.godigit.com");
				break;
			case "preprodtest":
			case "preprodtest-plus":
				Library.staticMap.put("server", "http://preprodtest-plus.godigit.com");
			case "preprodplusui":
				Library.staticMap.put("server", "https://preprod-plusui.godigit.com");
				break;
			case "plus":
			case "prod":
				Library.staticMap.put("server", "https://plus.godigit.com");
				break;
			default:
				throw new Exception("The environment mentioned '" + environment + "' is not valid");
			}
		}

		catch (Exception e) {
			throw new Exception("Unable to decide the server URL because of the error : " + e.getMessage());
		}
		return prop.getEnvironment().toString().toLowerCase();
		//				return environment.toLowerCase();	
	}

	public void waitForElementVisible(By locator, int timeOutInSeconds, int pollingEveryInMiliSec) {
		setImplicitWait(1, TimeUnit.SECONDS);
		WebDriverWait wait = getWait(timeOutInSeconds, pollingEveryInMiliSec);
		wait.until(ExpectedConditions.visibilityOf(driver.findElement(locator)));
	}

	public void waitForElementVisible(WebElement element, int timeOutInSeconds, int pollingEveryInMiliSec) {
		setImplicitWait(2, TimeUnit.SECONDS);
		WebDriverWait wait = getWait(timeOutInSeconds, pollingEveryInMiliSec);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public void waitForElementToBeClickable(WebElement element, int timeOutInSeconds, int pollingEveryInMiliSec) {
		setImplicitWait(2, TimeUnit.SECONDS);
		WebDriverWait wait = getWait(timeOutInSeconds, pollingEveryInMiliSec);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void setImplicitWait(long timeout, TimeUnit unit) {
		driver.manage().timeouts().implicitlyWait(timeout, unit == null ? TimeUnit.SECONDS : unit);
	}

	private WebDriverWait getWait(int timeOutInSeconds, int pollingEveryInMiliSec) {
		WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
		wait.pollingEvery(pollingEveryInMiliSec, TimeUnit.MILLISECONDS);
		wait.ignoring(NoSuchElementException.class);
		wait.ignoring(ElementNotVisibleException.class);
		wait.ignoring(StaleElementReferenceException.class);
		wait.ignoring(NoSuchFrameException.class);
		return wait;
	}

	public String getTestDataProperties(String key) throws FileNotFoundException, IOException {
		Properties prop = new Properties();
		prop.load(ResourceHelper.getResourcePathInputStream("configfile/" + "testdata.properties"));
		return prop.getProperty(key);
	}

	public String getDropDownValue(WebElement webElement)
	{
		Select select = new Select(webElement);
		String selectedOptionFromDropdown = select.getFirstSelectedOption().getText();
		return selectedOptionFromDropdown;
	}

	public void selectDropDownValue(WebElement webElement, String value) throws Exception {
		try {
			if (webElement.getTagName().equals("select")) {
				Select select = new Select(webElement);
				boolean found = false;
				List<WebElement> items = select.getOptions();
				int currentindex = 0;
				for (WebElement element : items) {
					String currenttext = element.getText().trim();
					if (currenttext.equalsIgnoreCase(value.trim()) && !found) {
						select.selectByIndex(currentindex);
						found = true;
						break;
					}
					currentindex++;
				}
			} else if (webElement.getTagName().equals("input")) {
				if (webElement.getAttribute("class").contains("ng-autocomplete-input")) {
					webElement.clear();
					webElement.sendKeys(value.trim());
					String retrivedData = value;
					String generatedXPath = "//div[";
					for (String temp : retrivedData.split(" ")) {
						generatedXPath = generatedXPath + "span[contains(text(),'" + temp + "')] and ";
					}
					generatedXPath = generatedXPath + "span[@class='dropdown-item-highlight']]";
					System.out.println("The generated xpath is " + generatedXPath);
					Thread.sleep(3000);
					driver.findElement(By.xpath(generatedXPath)).click();

					// TestAttributes.driver.findElement(By.xpath(TestAttributes.LocatorValue+"/../datalist/option[text()='"+TestAttributes.Data+"']")).click();
				}

				else if (!webElement.getAttribute("list").isEmpty()) {
					webElement.clear();
					webElement.sendKeys(value.trim());
					String xpathDataList = "//datalist/option[contains(text(),'" + value + "')]";
					// driver.findElement(By.xpath(xpathDataList)).click();
					System.out.println("Generated xpath is" + xpathDataList);
					Actions act = new Actions(driver);
					act.sendKeys(Keys.DOWN, Keys.ENTER).perform();
					// act.moveToElement(driver.findElement(By.xpath(xpathDataList))).perform();
					// act.click().perform();

				}

				else {
					waitForElementVisible(webElement, 5, 100);
					webElement.clear();
					System.out.println("Attribute value: " + webElement.getAttribute("placeholder"));
				} }

			/*	else if(!webElement.getAttribute("list").isEmpty())	{
					webElement.clear();
					webElement.sendKeys(value.trim());
					String xpathDataList = "//datalist/option[contains(text(),'"+ value +"')]";
					driver.findElement(By.xpath(xpathDataList)).click();
					System.out.println("Generated xpath is" +xpathDataList);


				}*/

			else {
				waitForElementVisible(webElement, 5, 100);
				webElement.clear();
				webElement.sendKeys(value);
				System.out.println("Value entered in the text : "+value);
				waitForDigitLoad();
				String retrivedData = value;
				String generatedXPath = "//div[";
				for (String temp : retrivedData.split(" ")) {
					generatedXPath = generatedXPath + "span[contains(text(),'" + temp + "')] and ";
				}
				generatedXPath = generatedXPath + "span[@class='dropdown-item-highlight']]";
				System.out.println("The generated xpath is " + generatedXPath);
				Thread.sleep(2000);
				driver.findElement(By.xpath(generatedXPath)).click();
			}
		}

		catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public String getParamValue(String details) throws Exception {
		String returnValue = "";
		try {
			System.out.println(staticMap);
			HashMap<String, String> hmap = getFieldMap(details);
			for (String key : hmap.keySet()) {
				if (hmap.get(key).contains("[RANDOM || "))
					returnValue = returnValue + "#" + key + " := " + getRandomValue(hmap.get(key));
				else if (hmap.get(key).contains("[TODAY"))
					returnValue = returnValue + "#" + key + " := " + getDateValue(hmap.get(key));
				else if (hmap.get(key).contains("[HMAP")) {
					String staticKey = hmap.get(key).replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("HMAP", "")
							.replaceAll("\\|\\|", "");
					System.out.println("Key : " + staticKey);
					returnValue = returnValue + "#" + key + " := " + staticMap.get(staticKey.trim());
				} else
					returnValue = returnValue + "#" + key + " := " + hmap.get(key);
			}
			returnValue = returnValue.trim();
			if (returnValue.startsWith("#"))
				returnValue = returnValue.substring(1);
			return returnValue;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	public void waitforDigitCareLoading() {
		try {
			WebDriver driver = Library.driver;
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			while (driver.findElements(By.xpath("//span[@class='customLoader']/img")).size() > 0) {
				Thread.sleep(200L);
			}
			// Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
			// .withTimeout(30, TimeUnit.SECONDS)
			// .pollingEvery(1, TimeUnit.SECONDS)
			// .ignoring(NoSuchElementException.class);
			// wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//span[@class='customLoader']/img")));
		} catch (Exception e) {

		}
	}

	public void waitForDigitLoad() throws Exception {
		try {
			if (IsErrorDisplayed())
				return;
			boolean flag = false;
			WebDriver _driver = this.getDriver();
			_driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
			if (_driver.findElements(By.xpath(
					"//*[contains(@class,'loading') or (@id = 'loading' and @style = 'margin: auto;') or (@id='overlay-bg-img' and @style = 'display: block;') or (@id='preloader' and @style = 'display: block;')]"))
					.size() == 0) {
				// System.out.println("The loading icon is not present");
				return;
			}
			for (int i = 0; i <= 1200; i++) {
				if (_driver.findElements(By.xpath(
						"//*[contains(@class,'loading') or (@id = 'loading' and @style = 'margin: auto;') or (@id='overlay-bg-img' and @style = 'display: block;') or (@id='preloader' and @style = 'display: block;')]"))
						.size() > 0) {
					if (!flag) {
						System.out.println("GoDigit is loading.....");
						flag = true;
					}
					try {
						Thread.sleep(100L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else {
					System.out.println("GoDigit has loaded for " + i / 10 + " seconds");
					flag = true;
					break;
				}
			}
			Thread.sleep(1000);
			if (IsErrorDisplayed())
				return;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public void waitForABSLoading() throws Exception {
		try {
			Thread.sleep(1000L);
			boolean flag = false;
			WebDriver _driver = this.getDriver();
			_driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
			for (int i = 0; i <= 1200; i++) {
				if (_driver.findElements(By.xpath("//div[contains(@style,'175c809f')]")).size() > 0) {
					if (!flag) {
						System.out.println("ABS is loading.....");
						flag = true;
					}
					try {
						Thread.sleep(100L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else {
					System.out.println("ABS has loaded for " + i / 10 + " seconds");
					flag = true;
					break;
				}
			}
			Thread.sleep(1000L);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public void waitForGMCLoading() throws Exception {
		try {
			Thread.sleep(1000L);
			boolean flag = false;
			WebDriver _driver = this.getDriver();
			_driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
			for (int i = 0; i <= 1200; i++) {
				if (_driver.findElements(By.xpath("//div[@class='overlay ng-star-inserted' and contains(@style,'absolute')]")).size() > 0) {
					if (!flag) {
						System.out.println("GMC is loading.....");
						flag = true;
					}
					try {
						Thread.sleep(100L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else {
					System.out.println("GMC has loaded for " + i / 10 + " seconds");
					flag = true;
					break;
				}
			}
			Thread.sleep(1000L);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public boolean IsErrorDisplayed() throws Exception {
		WebDriver tempDriver = getDriver();
		tempDriver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
		if (tempDriver.findElements(By.xpath("//div[@id='errorReportModal' and contains(@style,'display: block')]"))
				.size() > 0) {
			if (tempDriver
					.findElement(By.xpath("//div[@id='errorReportModal']//span[text()='URL:']/following-sibling::div"))
					.getText().contains("PDFGeneration/rest/digit/generatePolicy?")) {
				// System.out.println("Inside if block");
				tempDriver.findElement(By.xpath("//*[@id=\"errorReportModal\"]//button[text()='Close']")).click();
				return false;
			}

			else if(tempDriver
					.findElement(
							By.xpath("//div[@id='errorReportModal']//span[text()='URL:']/following-sibling::div"))
					.getText().contains("PDFGeneration/rest/digit/downloadQuotePDF?"))
			{
				tempDriver.findElement(By.xpath("//*[@id=\"errorReportModal\"]//button[text()='Close']")).click();
				return false;
			}
			else if(tempDriver
					.findElement(
							By.xpath("//div[@id='errorReportModal']//span[text()='URL:']/following-sibling::div"))
					.getText().contains("inspection/status?registrationNumber"))
			{
				tempDriver.findElement(By.xpath("//*[@id=\"errorReportModal\"]//button[text()='Close']")).click();
				return false;
			}
			else if(tempDriver
					.findElement(
							By.xpath("//div[@id='errorReportModal']//span[text()='URL:']/following-sibling::div"))
					.getText().contains("getPreInspectionLinkResponse?"))
			{
				tempDriver.findElement(By.xpath("//*[@id=\"errorReportModal\"]//button[text()='Close']")).click();
				return false;
			}


			if (tempDriver
					.findElement(
							By.xpath("//div[@id='errorReportModal']//span[text()='Error:']/following-sibling::div"))
					.getText().contains("NoSuchMethodError")) {
				// System.out.println("Inside if block");
				tempDriver.findElement(By.xpath("//*[@id=\"errorReportModal\"]//button[text()='Close']")).click();
				return false;
			} else {
				if (tempDriver
						.findElement(
								By.xpath("//div[@id='errorReportModal']//span[text()='URL:']/following-sibling::div"))
						.getText().length() > 0) {
					// System.out.println("Inside second if");
					// System.out.println("URL : " + tempDriver
					// .findElement(By
					// .xpath("//div[@id='errorReportModal']//span[text()='URL:']/following-sibling::div"))
					// .getText());
					// System.out.println(
					// tempDriver.findElement(By.xpath("//div[@id='errorReportModal']//span[text()='Status:']/.."))
					// .getText());
					// System.out.println("Error : " + tempDriver
					// .findElement(By.xpath(
					// "//div[@id='errorReportModal']//span[text()='Error:']/following-sibling::div"))
					// .getText());
					throw new Exception("Service Error is thrown with details : \nURL : "
							+ tempDriver.findElement(By
									.xpath("//div[@id='errorReportModal']//span[text()='URL:']/following-sibling::div"))
							.getText()
							+ "\n"
							+ tempDriver.findElement(
									By.xpath("//div[@id='errorReportModal']//span[text()='Status:']/..")).getText()
							+ "\nError : "
							+ tempDriver.findElement(By.xpath(
									"//div[@id='errorReportModal']//span[text()='Error:']/following-sibling::div"))
							.getText());
				}
			}

		}
		//		if(tempDriver.findElements(By.xpath("//div[@class='toast toast-warning ng-star-inserted']")).size()>0)
		//			throw new Exception("Toaster popup is displayed with the error : "+tempDriver.findElement(By.xpath("//span[@class='toast-message ng-star-inserted']")).getText());
		return false;
	}

	public void enterDate(WebElement element, String data) {
		try {
			String date = data.trim();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-YYYY");
			if (date.contains("[")) {
				date = date.replaceAll("\\[", "").replaceAll("\\]", "");
				date = date.replaceAll("TODAY", "");
				System.out.println("Date is " + date);
				if (date.contains("+")) {
					LocalDate date1 = LocalDate.now().plusDays(Integer.parseInt(date.substring(1)));
					date = formatter.format(date1);
				} else if (date.length() == 0) {
					LocalDate date3 = LocalDate.now();
					date = formatter.format(date3);
				} else if (date.substring(0, 1).equals("-")) {
					LocalDate date2 = LocalDate.now().minusDays(Integer.parseInt(date.substring(1)));
					date = formatter.format(date2);
					System.out.println("Date:" + date);

				}
				// System.out.println("Date " + date + "is aboutt to be entered");
			}
			if (element.getAttribute("type").equals("text")) {
				// element.clear();
				// Thread.sleep(1000);
				// element.clear();
				element.sendKeys(
						Keys.chord(Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE,
								Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE, Keys.BACK_SPACE));
			} else if (element.getAttribute("type").equals("date")) {
				element.sendKeys(Keys.TAB);
				String press = Keys.chord(Keys.SHIFT, Keys.TAB);
				element.sendKeys(press);
			}

			for (int i = 0; i < 3; i++) {
				System.out.println("Entering date for " + i + " time");

				//						for (int i = 0; i < 5; i++) {
				//							System.out.println("Entering date for " + (i+1) + " time");
				element.clear();
				for (String temp : date.split("-")) {
					if (element.getAttribute("value").contains("aN-aN-NaN")) {
						element.clear();
					}
					System.out.println("Enter Date " + temp);
					element.sendKeys(temp);
					Thread.sleep(2000L);

				}
				if (element.getAttribute("value").equals(date))
					break;
				else
					element.clear();
			}

			//					}
		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getDate(String data, String dateFormat) {
		String date = data.trim();
		try {

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
			if (date.contains("[")) {
				date = date.replaceAll("\\[", "").replaceAll("\\]", "");
				date = date.replaceAll("TODAY", "");
				// System.out.println("Date is " + date);
				if (date.contains("+"))

				{
					LocalDate date1 = LocalDate.now().plusDays(Integer.parseInt(date.substring(1)));
					System.out.println(date1);
					date = formatter.format(date1);
				} else if (date.length() == 0) {
					LocalDate date3 = LocalDate.now();
					date = formatter.format(date3);
				} else if (date.substring(0, 1).equals("-")) {
					LocalDate date2 = LocalDate.now().minusDays(Integer.parseInt(date.substring(1)));
					date = formatter.format(date2);

				}
				System.out.println("Date " + date + " is about to be entered");

			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	public String getDateValue(String data) {
		String date = data.trim();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-YYYY");
		if (date.contains("[")) {
			date = date.replaceAll("\\[", "").replaceAll("\\]", "");
			date = date.replaceAll("TODAY", "");
			// System.out.println("Date is " + date);
			if (date.contains("+")) {
				LocalDate date1 = LocalDate.now().plusDays(Integer.parseInt(date.trim().substring(1).trim()));
				date = formatter.format(date1);
			} else if (date.length() == 0) {
				LocalDate date3 = LocalDate.now();
				date = formatter.format(date3);
			} else if (date.substring(0, 1).equals("-")) {
				LocalDate date2 = LocalDate.now().minusDays(Integer.parseInt(date.trim().substring(1).trim()));
				date = formatter.format(date2);

			}
		}
		return date;
		// System.out.println("Date " + date + "is aboutt to be entered");
	}


	public String getRandomValue(String data) throws Exception {
		try {
			data = data.replaceAll(" ", "").toUpperCase();
			data = data.replaceAll("\\[", "").replaceAll("\\]", "");
			String formed;
			switch (data.split("\\|")[0]) {
			case "AN":
				formed = RandomStringUtils.random(Integer.parseInt(data.split("\\|")[1]), true, true);
				System.out.println(formed.toUpperCase());
				break;
			case "N":
				formed = RandomStringUtils.random(Integer.parseInt(data.split("\\|")[1]), false, true);
				System.out.println(formed);
				break;
			case "A":
				formed = RandomStringUtils.random(Integer.parseInt(data.split("\\|")[1]), true, false);
				System.out.println(formed.toUpperCase());
				break;
			case "RANGE":
				Random random = new Random();
				String min = data.split("\\|")[1].trim();
				String max = data.split("\\|")[2].trim();
				int randomNumber = 0;
				try {
					randomNumber = random.nextInt(Integer.parseInt(max)) + Integer.parseInt(min);
					formed = "" + randomNumber;
				} catch (NumberFormatException e) {
					// System.out.println(max.substring(0, max.length()-9));
					// System.out.println(max.substring(max.length()-9, max.length()));
					String random1 = "" + random.nextInt(Integer.parseInt(max.substring(0, max.length() - 9)));
					String random2 = "" + random.nextInt(
							Integer.parseInt(max.substring(max.length() - 9, max.length())) + Integer.parseInt(min));
					// System.out.println(random1);
					// System.out.println(random2);
					formed = random1 + random2;
				}
				// System.out.println(formed);
				break;
			case "REGNUM":
				Random rndm = new Random();
				int randomNumber1 = rndm.nextInt(9999) + 1;
				formed = "KA01AA" + String.format("%04d", randomNumber1);
				System.out.println(formed);
				break;
			default:
				throw new Exception(
						"The given random functionality '" + data + "'is not recognised/implemented in the script");
			}
			return formed;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public void deletetheFilesInDownloadFiles() throws Exception {
		try {
			File dir = new File("./DownloadFiles");
			File[] dirContents = dir.listFiles();
			for (int i = 0; i < dirContents.length; i++) {
				dirContents[i].delete();
			}
		} catch (Exception e) {
			throw new Exception("DownloadsFiles folder is not found ");
		}

	}

	public void verifyTheDownloadedFiles() throws Exception {

		int count = 0;
		File dir = new File("./DownloadFiles");
		File[] dirContents = dir.listFiles();
		for (int i = 0; i < dirContents.length; i++) {
			if (dirContents[i].getName().contains("pdf")) {
				count++;
			}
		}
		System.out.println("Count :" + count);
		if (count != 2) {

			throw new Exception("The policy has not downloaded and the count of file in the download folder :" + count);
		}
		count = 0;
	}

	public static void waitForVisibilityOfTheElement(WebElement element) {
		PropertyFileReader prop = new PropertyFileReader();
		try {
			WebDriverWait wait = new WebDriverWait(driver, prop.getExplicitWait());
			wait.until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {
			e.printStackTrace();
			e.getMessage();
			throw new NoSuchElementException("Could not found the desired element");
		}
	}

	public void scrollElementIntoView(WebElement element, WebDriver driver) {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView();", element);

	}

	public void performOnlinePayment() {
		try {
			Thread.sleep(5000L);
		} catch (InterruptedException e) {
		}
		WebDriverWait wait = new WebDriverWait(driver, 30);
		String frameLastPart = driver.findElement(By.xpath("//iframe[@class='card_number_iframe']"))
				.getAttribute("name");
		frameLastPart = frameLastPart.split("_")[3];
		// try {Thread.sleep(15000L);} catch (InterruptedException e) {}
		String cardNumberFrame = "card_number_iframe_" + frameLastPart;
		String cardExpiryMonthFrame = "card_exp_month_iframe_" + frameLastPart;
		String cardExpiryYearFrame = "card_exp_year_iframe_" + frameLastPart;
		String cardCVVFrame = "security_code_iframe_" + frameLastPart;
		String cardHolderNameFrame = "name_on_card_iframe_" + frameLastPart;

		driver.switchTo().defaultContent();
		driver.switchTo().frame(cardNumberFrame);
		driver.findElement(By.id("card_number")).sendKeys("5123456789012346");

		driver.switchTo().defaultContent();
		driver.switchTo().frame(cardHolderNameFrame);
		driver.findElement(By.id("name_on_card")).sendKeys("Test Name");

		driver.switchTo().defaultContent();
		driver.switchTo().frame(cardExpiryMonthFrame);
		driver.findElement(By.id("card_exp_month")).sendKeys("05");

		driver.switchTo().defaultContent();
		driver.switchTo().frame(cardExpiryYearFrame);
		driver.findElement(By.id("card_exp_year")).sendKeys("20");

		driver.switchTo().defaultContent();
		driver.switchTo().frame(cardCVVFrame);
		driver.findElement(By.id("security_code")).sendKeys("123");

		driver.switchTo().defaultContent();
		WebElement btnPayNow = driver.findElement(By.id("common_pay_btn"));

		btnPayNow.click();
		//driver.switchTo().defaultContent();
		System.out.println("Started waiting for visibility");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("juspay-logo")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("juspay-logo")));
		System.out.println("Loading is completed");
		try {
			Thread.sleep(30000L);
		} catch (InterruptedException e) {
		}
	}

	public void selectDropDownValue_ABS(WebElement element, String value) throws Exception {
		try {
			Thread.sleep(500);
			element.clear();
			Thread.sleep(500);
			char[] charArray = value.toCharArray();
			for (char temp : charArray) {
				element.sendKeys("" + temp);
				if (element.getAttribute("value").equals(value))
					break;
				Thread.sleep(200);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public void waitForLoaderLoading() throws Exception {
		try {
			boolean flag=false;
			WebDriver _driver = this.getDriver();
			_driver.manage().timeouts().implicitlyWait(500, TimeUnit.MILLISECONDS);
			if (_driver.findElements(By.xpath("//mat-spinner")).size() == 0) {
				System.out.println("The loading icon is not present in loader");
				return;
			}
			for (int i = 0; i <= 1200; i++) {
				if (_driver.findElements(By.xpath("//mat-spinner")).size() > 0) {
					if (!flag) {
						System.out.println("GoDigit is loading in Loader page.....");
						flag = true;
					}
					try {
						Thread.sleep(100L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else {
					System.out.println("GoDigit Loader has loaded for " + i / 10 + " seconds in loader page");
					flag = true;
					break;
				}
			}

			if (IsErrorDisplayed())
				return;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Used to kill the all the 'chomedriver' processes running in background. Multi-support provided for Windows, Linux and Mac operating systems.
	 *
	 * @throws Exception
	 * @author Deepjyoti Barman
	 * @since September 09, 2020
	 */
	public void killChromeDriver() {
		final String OS                = System.getProperty("os.name").toLowerCase();
		final String FILE_PATH_WINDOWS = "process_kill_windows.bat";
		final String FILE_PATH_UNIX    = "process_kill_unix.sh";

		String[] cmd = OS.contains("windows") ? new String[] { "cmd", "/c", FILE_PATH_WINDOWS } : new String[] { "sh", "-p", FILE_PATH_UNIX };

		try {
			File file = new File(cmd[2]);

			if (file.exists() && file.isFile()) {
				// If we are running in Linux or Mac OS and the file does not have execute permission then provide the same
				if (!OS.contains("windows") && !file.canExecute())
					Runtime.getRuntime().exec(new String[] { "chmod", "+x", FILE_PATH_UNIX });
			} else {
				throw new FileNotFoundException("File does not exist or file path is incorrect");
			}

			Process process       = Runtime.getRuntime().exec(cmd);
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			StringBuilder output  = new StringBuilder();

			String line;
			while ((line = reader.readLine()) != null) {
				output.append(line + "\n");
			}

			// Once the process terminates normally, displays the output
			int exitVal = process.waitFor();
			if (exitVal == 0) {
				System.out.println(output);
			} else {
				System.out.println("Abnormal termination of process, could not print the output");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Shift the focus to the given element
	 *
	 * @param driver the current and active driver instance
	 * @param element element on the webpage to focus
	 * @author Deepjyoti Barman
	 * @since December 26, 2020
	 */
	public void focus(WebDriver driver, WebElement element) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}

	/**
	 * Given a string it returns the numeric (integer) value of it, replacing all empty spaces and special characters
	 *
	 * @param str string to convert into a number
	 * @return the numeric value of the given string
	 * @author Deepjyoti Barman
	 * @since December 26, 2020
	 */
	public int strToNum(String str) {
		str = str.replaceAll("[^0-9]", "");
		return Integer.parseInt(str);
	}

	/**
	 * Given a string this function splits it into multiple different parts depending on the position the delimiter given as input. Then returns a String array containing all the parts of the actual string generated during this process.
	 *
	 * @param str String to split
	 * @param delimiter Character or symbol (e.g. , / | -) to split the given string
	 * @return Returns a string array containing all the split parts of the actual string
	 * @author Deepjyoti Barman
	 * @since December 26, 2020
	 */
	public String[] splitter(String str, String delimiter) {
		String[] strArr = str.split("[" + delimiter + "]");

		for (int i = 0; i < strArr.length; i++)
			strArr[i] = strArr[i].trim();

		return strArr;
	}

	/**
	 * It converts the current timestamp into a String in the following format:
	 *  format: DD-MM-YYYY hh:mm:ss a
	 *
	 * @return A String where the current timestamp is in 'DD-MM-YYYY hh:mm:ss a' format
	 * @author Deepjyoti Barman
	 * @since Jan 26, 2020
	 */
	public String getCurrentTimestamp()
	{
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("EEEE DD-MM-YYYY hh:mm:ss a");

		return sdf.format(new Timestamp(date.getTime()));
	}

	public void clickOnElementJS(WebElement element, WebDriver driver) {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", element);

	}
	
	public void alterPayload(String keyPath,Object value,JsonPath pathToPayload) {
		
		Map<String, Object> map = pathToPayload.get("$");
		
		String[] split = keyPath.split("\\.");
		String path = "";
		for (String key : split) {
			
			if (!key.equals(keyPath)) {
				if (!key.equals(split[split.length - 1])) {

					path = path + "." + key;
				} else {
					path = path.replaceFirst("\\.", "");
					map = pathToPayload.getJsonObject(path);	
					map.put(key, value);
				} 
			} else {
				
				map.put(key, value);
			}
		}
	}
	
	public void captureResponseInFile(Response response,String path) throws IOException {

		File f = new File(Paths.get("").toAbsolutePath().toString()+path);
		if (!f.exists()) {
			f.createNewFile();
		}
		PrintWriter pw = new PrintWriter(f);
		pw.write(response.asPrettyString());
		pw.close();
	}
	
	public void capturePayloadInFile(Map payload,String path) throws IOException {
		
		File f = new File(Paths.get("").toAbsolutePath().toString()+path);
		if (!f.exists()) {
			f.createNewFile();
		}
		PrintWriter pw = new PrintWriter(f);
		pw.write(payload.toString());
		pw.close();
	}
	
	public void captureStringInFile(String value,String path) throws IOException {
		
		File f = new File(Paths.get("").toAbsolutePath().toString()+path);
		FileWriter fw = new FileWriter(f,true);
		if (!f.exists()) {
			f.createNewFile();
		}
		PrintWriter pw = new PrintWriter(fw);
		pw.println(value);
		pw.close();
	}
}