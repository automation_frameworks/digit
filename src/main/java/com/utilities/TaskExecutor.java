package com.utilities;

import com.cucumber.framework.utility.ResourceHelper;

import java.io.*;
import java.util.Arrays;
import java.util.zip.ZipOutputStream;

public class TaskExecutor {
    Library library = new Library();
    PropertyFileReader prop = new PropertyFileReader();
    ZipHandler zipHandler   = new ZipHandler();

    public void driverKill() {
        library.killChromeDriver();
    }

    public void emailReports() {
        try {
            MailSender ms        = new MailSender();
            String[] to          = library.splitter(prop.getCustomProperty("mailTo"), "|");
            String subject       = "[" + library.getCurrentTimestamp() + "] Selenium Test Results (Local)";
            String msg           = "Kindly find the locally generated selenium test results through the attachment.<br><br>" +
                    "Note: The .js files could not be added in the 'cucumber-html-reports' directory because of security issues. " +
                    "Hence it is requested to download the required .js files from the given like down below to read the reports properly.<br><br>" +
                    "Link: " +
                    "<a href='https://iiii-my.sharepoint.com/:u:/g/personal/deepjyoti_barman_godigit_com/EbE6iO12al5CvN5pEcU437IB-BRcTXdczdO4yCuGBNGNCg'>" +
                    "cucumber-html-reports.zip" +
                    "</a>";
            String[] attachments = {
                    prop.getCustomProperty("excelReportsPath", ResourceHelper.getReportsPropPath()),
                    prop.getCustomProperty("cucumberReportsPath", ResourceHelper.getReportsPropPath())
            };

            ms.sendMail(to, subject, msg, attachments);
            System.out.println("Mail has been sent successfully...");
            System.out.println("Recipients: " + Arrays.toString(to));
        }
        catch (Exception e) {
            System.out.println("Sorry, could not deliver the mail");
            e.printStackTrace();
        }
    }

    public void convertReportToZip(String sourceDir, String zipOut) {
        try {
            File fileToZip = new File(sourceDir);
            FileOutputStream fos = new FileOutputStream(zipOut);
            ZipOutputStream zos = new ZipOutputStream(fos);

            zipHandler.zipDir(fileToZip, fileToZip.getName(), zos);

            // Important lines if we don't close the streams then files may get corrupted
            zos.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
