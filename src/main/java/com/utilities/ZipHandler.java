package com.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipHandler {
    public void zipDir(File fileToZip, String fileName, ZipOutputStream zipOut) {
        try {
            // If the given dir is hidden then do nothing
            if (fileToZip.isHidden()) {
                return;
            }

            // Check if the given path is of a directory
            if (fileToZip.isDirectory()) {
                if (fileName.endsWith("/")) {
                    zipOut.putNextEntry(new ZipEntry(fileName));
                    zipOut.closeEntry();
                } else {
                    zipOut.putNextEntry(new ZipEntry(fileName + "/"));
                    zipOut.closeEntry();
                }

                // Reading the files and directories in the given path recursively
                File[] children = fileToZip.listFiles();
                for (File childFile : children) {
                    // Skip the .js files which will cause security issues while uploading over the mail
                    if (!childFile.getName().endsWith("js"))
                        zipDir(childFile, fileName + "/" + childFile.getName(), zipOut);
                }
                return;
            }

            // Writing buffer set as 1024 bytes
            byte[] bytes        = new byte[1024];
            FileInputStream fis = new FileInputStream(fileToZip);
            ZipEntry zipEntry   = new ZipEntry(fileName);
            int length;

            zipOut.putNextEntry(zipEntry);

            // Writing into the zip file byte by byte
            while ((length = fis.read(bytes)) >= 0) {
                zipOut.write(bytes, 0, length);
            }

            fis.close();
        } catch (IOException ie) {
            ie.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
