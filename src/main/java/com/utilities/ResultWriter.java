package com.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import com.cucumber.framework.utility.ResourceHelper;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import cucumber.api.Scenario;
import pom.agent.motor.LoaderPage;

public class ResultWriter {
	static XSSFWorkbook workBook;
	static int counter = 0, skipped = 0;
	static XSSFSheet sheet;
	static FileOutputStream fis;
	XSSFFont dataFont, passFont, failFont, skippedFont;
	CellStyle dataCellStyle, passCellStyle, failCellStyle, skippedCellStyle;
	PropertyFileWriter pfw = new PropertyFileWriter();

	public String createResultSheet() {
		try {
			XSSFFont headerFont;
			CellStyle headerCellStyle;

			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat("yyyy_MMM_dd HH-mm-ss");
			File reportOutputDirectory = new File("target/reports/excelreports");
			if (!reportOutputDirectory.isDirectory()) {
				System.out.println("Folder is created");
				System.out.println(reportOutputDirectory.mkdirs());
			} else
				System.out.println("Folder is already present");

			File reportOutputDirectory2 = new File("target/reports/cucumberreports");
			if (!reportOutputDirectory2.isDirectory()) {
				System.out.println("Folder is created");
				System.out.println(reportOutputDirectory2.mkdirs());
			}
			// File excel = new File(reportOutputDirectory + "/Report" + ft.format(dNow) +
			// ".xlsx");
			workBook = new XSSFWorkbook();
			String excelReportsPath = "target/reports/excelreports/Report" + ft.format(dNow) + ".xlsx";
			fis = new FileOutputStream(new File(excelReportsPath));
			// XSSFWorkbook workBook = new XSSFWorkbook();
			ft = new SimpleDateFormat("yyyy_MMM_dd");
			sheet = workBook.getSheet(ft.format(dNow));
			if (sheet == null)
				sheet = workBook.createSheet(ft.format(dNow));

			headerFont = workBook.createFont();
			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short) 12);
			headerFont.setColor(IndexedColors.ORANGE.getIndex());

			headerCellStyle = workBook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setFillBackgroundColor(IndexedColors.AQUA.getIndex());

			Row row = sheet.createRow(0);
			Cell cell = row.createCell(0);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("S No.");

			cell = row.createCell(1);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Scenario");

			cell = row.createCell(2);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Pass/Fail");

			cell = row.createCell(3);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Policy Number");

			cell = row.createCell(4);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("StatusCode");
			
			cell = row.createCell(5);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Tag");

			cell = row.createCell(6);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Recalculated Premium");

			cell = row.createCell(7);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Save Quote Premium");
			
			cell = row.createCell(8);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Error Message");
			
			cell = row.createCell(9);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Actual");
			
			cell = row.createCell(10);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Expected");
			
			cell = row.createCell(11);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Time Executed");

			pfw.setCustomProperty(
					"excelReportsPath",
					"" + excelReportsPath,
					"" + ResourceHelper.getReportsPropPath()
			);

			return excelReportsPath;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void writeTestResults(Scenario scenario, boolean isSkipped) {
		try {

			dataFont = workBook.createFont();
			dataFont.setBold(true);
			dataFont.setFontHeightInPoints((short) 12);
			dataFont.setColor(IndexedColors.BLACK.getIndex());

			passFont = workBook.createFont();
			passFont.setBold(true);
			passFont.setFontHeightInPoints((short) 12);
			passFont.setColor(IndexedColors.GREEN.getIndex());
			// passFont.setFillBackgroundColor(IndexedColors.GREEN.getIndex());

			failFont = workBook.createFont();
			failFont.setBold(true);
			failFont.setFontHeightInPoints((short) 12);
			failFont.setColor(IndexedColors.RED.getIndex());

			skippedFont = workBook.createFont();
			skippedFont.setBold(true);
			skippedFont.setFontHeightInPoints((short) 12);
			skippedFont.setColor(IndexedColors.BLUE.getIndex());

			dataCellStyle = workBook.createCellStyle();
			dataCellStyle.setFont(dataFont);

			passCellStyle = workBook.createCellStyle();
			passCellStyle.setFont(passFont);

			failCellStyle = workBook.createCellStyle();
			failCellStyle.setFont(failFont);

			skippedCellStyle = workBook.createCellStyle();
			skippedCellStyle.setFont(skippedFont);

			int rowsPresent = sheet.getLastRowNum();
			System.out.println("Rows present is : " + rowsPresent);
			counter = rowsPresent;

			if (scenario.getName().toLowerCase().contains("loader")) {
				writeLoaderTestResult();
				return;
			}

			Row row = sheet.createRow(++counter);
			Cell cell = row.createCell(0);
			cell.setCellStyle(dataCellStyle);
			cell.setCellValue(counter);

			cell = row.createCell(1);
			cell.setCellStyle(dataCellStyle);
			cell.setCellValue(scenario.getName());

			cell = row.createCell(2);
			if (isSkipped) {
				skipped++;
				cell.setCellStyle(skippedCellStyle);
				cell.setCellValue("Skipped");
			} else if (scenario.isFailed()) {
				cell.setCellStyle(failCellStyle);
				cell.setCellValue("Failed");
			} else {
				cell.setCellStyle(passCellStyle);
				cell.setCellValue("Passed");
			}

			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat("yyyy_MMM_dd hh:mm:ss");
			cell = row.createCell(11);
			cell.setCellStyle(dataCellStyle);
			cell.setCellValue(ft.format(dNow));

			if (Library.staticMap.containsKey("Policy Number")) {
				cell = row.createCell(3);
				cell.setCellStyle(passCellStyle);
				cell.setCellValue(Library.staticMap.get("Policy Number"));
			}
			
			if (Library.staticMap.containsKey("StatusCode")) {
				cell = row.createCell(4);
				if (Library.staticMap.get("StatusCode").startsWith("2")) {
					cell.setCellStyle(passCellStyle);
					cell.setCellValue(Library.staticMap.get("StatusCode"));
				} else {
					dataCellStyle.setBottomBorderColor(IndexedColors.RED.getIndex());
					cell.setCellStyle(dataCellStyle);
					cell.setCellValue(Library.staticMap.get("StatusCode"));
					dataCellStyle.setBottomBorderColor(IndexedColors.WHITE.getIndex());
				}
			}
			
			if (Library.staticMap.containsKey("Tag")) {
				cell = row.createCell(5);
				cell.setCellStyle(passCellStyle);
				cell.setCellValue(Library.staticMap.get("Tag"));
			}
			// else {
			// cell = row.createCell(4);
			// cell.setCellStyle(failCellStyle);
			// cell.setCellValue("Policy Number not generated");
			// }

			if (Library.staticMap.containsKey("Policy Status")) {
				cell = row.createCell(5);
				cell.setCellStyle(dataCellStyle);
				cell.setCellValue(Library.staticMap.get("Policy Status"));
			}
			cell = row.createCell(6);
			cell.setCellStyle(dataCellStyle);
			cell.setCellValue(Library.staticMap.get("Recalculated premium"));

			cell = row.createCell(7);
			cell.setCellStyle(dataCellStyle);
			cell.setCellValue(Library.staticMap.get("Save Quote premium"));
			
			if (Library.staticMap.containsKey("Actual")) {
				cell = row.createCell(9);
				cell.setCellStyle(dataCellStyle);
				cell.setCellValue(Library.staticMap.get("Actual"));
			}
			
			if (Library.staticMap.containsKey("Expected")) {
				cell = row.createCell(10);
				cell.setCellStyle(dataCellStyle);
				cell.setCellValue(Library.staticMap.get("Expected"));
			}

			// workBook.write(fis);
			// fis.close();
			// workBook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void writeLoaderTestResult() {
		LoaderPage loader = new LoaderPage();
		HashMap<String, String> loaderMap = loader.getLoaderMap();
		System.out.println("Number of policies in loader : " + loaderMap.get("LoaderCount"));
		int loaderCount = Integer.parseInt(loaderMap.get("LoaderCount"));
		for (int i = 1; i <= loaderCount; i++) {
			System.out.println("Writing result for loader : " + i);
			Row row = sheet.createRow(++counter);
			Cell cell = row.createCell(0);
			cell.setCellStyle(dataCellStyle);
			cell.setCellValue(counter);

			cell = row.createCell(1);
			cell.setCellStyle(dataCellStyle);
			cell.setCellValue(loaderMap.get("Scenario_" + i));
			
			if(loaderMap.get("Error_"+i)!=null && loaderMap.get("Error_"+i).equals("-"))
			{
			cell = row.createCell(2);
			cell.setCellStyle(passCellStyle);
			cell.setCellValue("Passed");
			}
			else
			{
				cell = row.createCell(2);
				cell.setCellStyle(failCellStyle);
				cell.setCellValue("Failed");
				}	
			

			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat("yyyy_MMM_dd hh:mm:ss");
			cell = row.createCell(3);
			cell.setCellStyle(dataCellStyle);
			cell.setCellValue(ft.format(dNow));

			cell = row.createCell(4);
			cell.setCellStyle(passCellStyle);
			cell.setCellValue(loaderMap.get("PolicyNumber_" + i));
			// else {
			// cell = row.createCell(4);
			// cell.setCellStyle(failCellStyle);
			// cell.setCellValue("Policy Number not generated");
			// }

			cell = row.createCell(5);
			cell.setCellStyle(dataCellStyle);
			cell.setCellValue(loaderMap.get("Comment_" + i));

			cell = row.createCell(6);
			cell.setCellStyle(dataCellStyle);
			cell.setCellValue(loaderMap.get("QuickQuotePremium_" + i));

			cell = row.createCell(7);
			cell.setCellStyle(dataCellStyle);
			cell.setCellValue(loaderMap.get("GetQuotePremium_" + i));
			
			cell = row.createCell(8);
			cell.setCellStyle(failCellStyle);
			cell.setCellValue(loaderMap.get("Error_" + i));
		}

	}

	public void closeExcel() {
		try {
			XSSFFont headerFont, dataFont;
			CellStyle headerCellStyle, dataCellStyle;

			headerFont = workBook.createFont();
			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short) 12);
			headerFont.setColor(IndexedColors.ORANGE.getIndex());

			headerCellStyle = workBook.createCellStyle();
			headerCellStyle.setFont(headerFont);

			dataFont = workBook.createFont();
			dataFont.setBold(true);
			dataFont.setFontHeightInPoints((short) 12);
			dataFont.setColor(IndexedColors.BLACK.getIndex());

			dataCellStyle = workBook.createCellStyle();
			dataCellStyle.setFont(dataFont);

			counter += 3;
			Row row = sheet.createRow(counter);
			PropertyFileReader prop = new PropertyFileReader();

			Cell cell = row.createCell(1);
			cell.setCellStyle(headerCellStyle);
			if (skipped > 0)
				cell.setCellValue(skipped + " test cases has been skipped as there were " + prop.getFailThreshold()
						+ " continuous failures");

			row = sheet.createRow(++counter);
			cell = row.createCell(1);
			cell.setCellStyle(headerCellStyle);
			cell.setCellValue("Environment");

			cell = row.createCell(2);
			cell.setCellStyle(dataCellStyle);
			cell.setCellValue(prop.getEnvironment());

			workBook.write(fis);
			fis.close();
			workBook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
