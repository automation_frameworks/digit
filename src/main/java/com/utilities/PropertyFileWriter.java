package com.utilities;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class PropertyFileWriter {
    Properties prop = null;
    OutputStream out = null;

    public PropertyFileWriter() {
        prop = new Properties();
    }

    /**
     * Sets the key and the value of the property in the given .properties file location. Creates the .properties file if the file does not exist. If the key and value pair already exists then updates the value of the same.
     *
     * @param key Name of the property
     * @param value Value to be set against the name of the property
     * @param fileLoc Location of the .properties file in disk drive
     */
    public void setCustomProperty(String key, String value, String fileLoc) {
        try {
            out = new FileOutputStream(fileLoc, true);

            // Set the property value
            prop.setProperty(key, value);

            // Save the property file
            prop.store(out, null);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
