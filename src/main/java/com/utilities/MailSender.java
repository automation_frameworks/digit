package com.utilities;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

public class MailSender {
    PropertyFileReader prop = new PropertyFileReader();

    // SMTP info
    String host     = "smtp.office365.com";
    String port     = "587";
    String from     = prop.getCustomProperty("mailFrom");
    String password = prop.getCustomProperty("mailPass");

    public void sendMail(String[] to, String subject, String message, String[] attachments) throws MessagingException {
        // Set SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.user", from);
        properties.put("mail.password", password);

        // Creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(from, password);
            }
        };
        Session session = Session.getInstance(properties, auth);

        // Create a new e-mail message
        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(from));
        InternetAddress[] toAddresses = new InternetAddress[to.length];
        for (int i = 0; i < toAddresses.length; i++)
            toAddresses[i] = new InternetAddress(to[i]);
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject);
        msg.setSentDate(new Date());

        // Create message part
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(message, "text/html");

        // Create multi-part
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);

        // Add attachments
        if (attachments != null && attachments.length > 0) {
            for (String filePath : attachments) {
                MimeBodyPart attachPart = new MimeBodyPart();

                try {
                    attachPart.attachFile(filePath);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                multipart.addBodyPart(attachPart);
            }
        }

        // Set the multi-part as e-mail's content
        msg.setContent(multipart);

        // Send the e-mail
        Transport.send(msg);
    }
}
