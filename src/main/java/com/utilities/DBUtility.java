package com.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;

import pom.agent.health.OptionsRecomendedByDoctor;
import pom.agent.health.PlanDetails;

public class DBUtility {

	PropertyFileReader read = new PropertyFileReader();
	final String dbURL_UAT = "jdbc:postgresql://" + read.getNonAbsUatUrl();
	final String usernameUAT = read.getNonAbsUatUsername();
	final String passwordUAT = read.getNonAbsUatPassword();
	final String dbURL_Preprod = "jdbc:postgresql://" + read.getNonAbsPreprodUrl();
	final String usernamePreprod = read.getNonAbsPreprodUsername();
	final String passwordPreprod = read.getNonAbsPreprodPassword();
	public static String modeOfPayment;
	public static String cityZoneFlag;

	public String getPersonDataFromDB(String planId, String value) throws SQLException {

		String query = "SELECT * FROM digit_health.health_testdata_plan where id = '" + planId + "'";
		;

		Connection connection = null;
		try {
			// Creates Connection.
			connection = DriverManager.getConnection(dbURL_UAT, usernameUAT, passwordUAT);

			if (connection != null)
				System.out.println(" Successfully connected to PostgreSQL database!\n");
			else
				System.out.println("Failed to make connection with database!\n");

		} catch (Exception e) {
			e.printStackTrace();
		}

		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);

		while (resultSet.next()) {
			switch (value.replaceAll(" ", "").toLowerCase()) {

			case "dob":
				value = resultSet.getString("dob").trim();
				break;

			case "height":
				value = resultSet.getString("height").trim();
				break;

			case "weight":
				value = resultSet.getString("weight").trim();
				break;

			case "pincode":
				value = resultSet.getString("pincode").trim();
				break;

			}
		}

		connection.close();
		return value;
	}

	public String getPlanDataFromDB(String element, String planId) throws SQLException {

		final String query = "SELECT * FROM digit_health.health_testdata_plan where id = '" + planId + "'";

		Connection connection = null;
		try {

			connection = DriverManager.getConnection(dbURL_UAT, usernameUAT, passwordUAT);

			if (connection != null)
				System.out.println(" Successfully connected to PostgreSQL database!\n");
			else
				System.out.println("Failed to make connection with database!\n");

		} catch (Exception e) {
			e.printStackTrace();
		}

		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);

		while (resultSet.next()) {
			switch (element.replaceAll(" ", "").toLowerCase()) {

			case "plan":
				element = resultSet.getString("plan").trim();
				break;

			case "sitype":
				element = resultSet.getString("sitype").trim();
				break;

			case "suminsured":
				element = resultSet.getString("suminsured").trim();
				break;

			case "policyterm":
				element = resultSet.getString("policyterm").trim();
				break;

			case "paymentmode":
				element = resultSet.getString("paymentmode").trim();
				break;

			}
		}

		connection.close();
		return element;
	}

	public void planToBeSelected(WebDriver driver, String planId) throws Exception {

		final String query = "SELECT plan FROM digit_health.health_testdata_plan where id = '" + planId + "'";

		Connection connection = null;
		try {

			connection = DriverManager.getConnection(dbURL_UAT, usernameUAT, passwordUAT);

			if (connection != null)
				System.out.println(" Successfully connected to PostgreSQL database!\n");
			else
				System.out.println("Failed to make connection with database!\n");

		} catch (Exception e) {
			e.printStackTrace();
		}

		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);

		while (resultSet.next()) {

			com.utilities.Library library = new com.utilities.Library();
			OptionsRecomendedByDoctor orbd = PageFactory.initElements(driver, OptionsRecomendedByDoctor.class);

			String plan = resultSet.getString("plan").trim();
			orbd.selectPlan(plan, planId);
			library.setImplicitWait(20, TimeUnit.SECONDS);
		}
	}

	public void selectPlanDetailsForHealthFromDB(WebDriver driver, String planId) throws Exception {

		final String query = "SELECT * FROM digit_health.health_testdata_plan where id = '" + planId + "'";

		Connection connection = null;
		try {

			connection = DriverManager.getConnection(dbURL_UAT, usernameUAT, passwordUAT);

			if (connection != null)
				System.out.println(" Successfully connected to PostgreSQL database!\n");
			else
				System.out.println("Failed to make connection with database!\n");

		} catch (Exception e) {
			e.printStackTrace();
		}

		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);

		while (resultSet.next()) {

			PlanDetails pd = PageFactory.initElements(driver, PlanDetails.class);

			String siType = resultSet.getString("sitype").trim();
			siType = "SI Type := " + siType.replace(" ", "").toLowerCase();
			pd.enterPlanDetails(siType, planId);
			Thread.sleep(2000);

			String si = resultSet.getString("suminsured").trim();
			si = "Sum Insured := " + si.replace(" ", "").toLowerCase();
			pd.enterPlanDetails(si, planId);
			Thread.sleep(2000);

			String term = resultSet.getString("policyterm").trim();
			term = "Policy Term := " + term.replace(" ", "").toLowerCase();
			pd.enterPlanDetails(term, planId);
			Thread.sleep(2000);

			modeOfPayment = resultSet.getString("paymentmode");
		}
	}

	public String getMedicDataFromDB(String value, String planId) throws SQLException {

		final String query = "SELECT * FROM digit_health.health_testdata_plan where id = '" + planId + "'";

		Connection connection = null;
		try {

			connection = DriverManager.getConnection(dbURL_UAT, usernameUAT, passwordUAT);

			if (connection != null)
				System.out.println(" Successfully connected to PostgreSQL database!\n");
			else
				System.out.println("Failed to make connection with database!\n");

		} catch (Exception e) {
			e.printStackTrace();
		}

		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);

		while (resultSet.next()) {
			switch (value.replaceAll(" ", "").toLowerCase()) {

			case "undiagnosedsymptoms":
				value = resultSet.getString("undiagnosedsymptoms").trim();
				break;

			case "preexistingconditions":
				value = resultSet.getString("preexistingconditions").trim();
				break;

			default:
				Reporter.log(value + " is not present in the database!!", true);
				break;
			}
		}

		connection.close();
		return value;
	}

	public String getDiseaseDataFromDB(String disease) throws SQLException {

		String query = "SELECT * FROM digit_health.health_testdata_disease where disease = '" + disease + "'";

		Connection connection = null;
		try {

			connection = DriverManager.getConnection(dbURL_UAT, usernameUAT, passwordUAT);

			if (connection != null)
				System.out.println(" Successfully connected to PostgreSQL database!\n");
			else
				System.out.println("Failed to make connection with database!\n");

		} catch (Exception e) {
			e.printStackTrace();
		}

		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		String value = "";
		int i = 1;
		while (resultSet.next()) {
			value = resultSet.getString("option" + (i) + "");
			try {
				while (!resultSet.getString("option" + (++i) + "").equals("-----NoData-----")) {

					value = value + " # " + resultSet.getString("option" + (i) + "");

				}
			} catch (Exception e) {
			}
		}

		connection.close();
		return value;
	}

	public void storeResultInNonABS_UAT_DB(String detail, String id, String flag, String table) throws Exception {

		try {
			String query = "";

			switch (flag) {
			case "declined":
				query = "UPDATE " + table + " SET \"result\" ='" + detail + "||Declined' WHERE id = '" + id + "'";
				break;
			case "referred":
				query = "UPDATE " + table + " SET \"result\" ='" + detail + "||Referred' WHERE id = '" + id + "'";
				break;
			case "fail":
				query = "UPDATE " + table + " SET \"comments\" ='" + detail + "||Referred' WHERE id = '" + id + "'";
				break;
			default:
				query = "UPDATE " + table + " SET \"result\" ='" + detail + "||Accepted' WHERE id = '" + id + "'";
				break;
			}

			Connection connection = null;
			try {

				connection = DriverManager.getConnection(dbURL_UAT, usernameUAT, passwordUAT);

				if (connection != null)
					System.out.println(" Successfully connected to PostgreSQL database!\n");
				else
					System.out.println("Failed to make connection with database!\n");

			} catch (Exception e) {
				e.printStackTrace();
			}

			Statement statement = connection.createStatement();
			statement.executeUpdate(query);
			System.out.println("Result Stored");
			Thread.sleep(2000);
			connection.close();
		} catch (Exception e) {
			System.out.println("Result could  not be stored.");
		}

	}
	public void storeDataInNonABS_Preprod_DB(String value, String id, String column, String table) throws Exception {
		
		try {
			String query = "UPDATE " + table + " SET "+column+" ='" + value + "' WHERE id = '" + id + "'";
			
			Connection connection = null;
			try {
				
				connection = DriverManager.getConnection(dbURL_Preprod, usernamePreprod, passwordPreprod);
				
				if (connection != null)
					System.out.println(" Successfully connected to PostgreSQL database!\n");
				else
					System.out.println("Failed to make connection with database!\n");
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			Statement statement = connection.createStatement();
			statement.executeUpdate(query);
			System.out.println("Value Stored");
			Thread.sleep(2000);
			connection.close();
		} catch (Exception e) {
			System.out.println("Value could  not be stored.");
			e.printStackTrace();
		}
		
	}

	public void storeScriptFailCommentInDB(String msg, String id, String table) throws Exception {

		try {
			String query = "UPDATE " + table + " SET comments ='Script Failed||" + msg + "' WHERE id = '" + id + "'";

			Connection connection = null;
			try {

				connection = DriverManager.getConnection(dbURL_UAT, usernameUAT, passwordUAT);

				if (connection != null)
					System.out.println(" Successfully connected to PostgreSQL database!\n");
				else
					System.out.println("Failed to make connection with database!\n");

			} catch (Exception e) {
				e.printStackTrace();
			}

			Statement statement = connection.createStatement();
			statement.executeUpdate(query);
			System.out.println("Comment Stored");
			Thread.sleep(2000);
			connection.close();
		} catch (Exception e) {
			System.out.println("Comment could  not be stored.");
		}

	}
	public void storeCommentsInDB(String msg, String id,String column, String table) throws Exception {

		try {
			String query = "UPDATE " + table + " SET "+column+" ='" + msg + "' WHERE id = '" + id + "'";

			Connection connection = null;
			try {

				connection = DriverManager.getConnection(dbURL_UAT, usernameUAT, passwordUAT);

				if (connection != null)
					System.out.println(" Successfully connected to PostgreSQL database!\n");
				else
					System.out.println("Failed to make connection with database!\n");

			} catch (Exception e) {
				e.printStackTrace();
			}

			Statement statement = connection.createStatement();
			statement.executeUpdate(query);
			System.out.println("Comment Stored");
			Thread.sleep(2000);
			connection.close();
		} catch (Exception e) {
			System.out.println("Comment could  not be stored.");
		}

	}

	public String getPortabilityDataFromDB(String column, String portId) throws SQLException {

		final String query = "SELECT " + column + " FROM digit_health.health_testdata_portability where id = '" + portId
				+ "'";

		Connection connection = null;
		try {

			connection = DriverManager.getConnection(dbURL_UAT, usernameUAT, passwordUAT);

		} catch (Exception e) {
			e.printStackTrace();
		}

		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);

		String value = "";

		while (resultSet.next()) {

			value = resultSet.getString(column);
		}

		connection.close();
		return value;
	}

	@SuppressWarnings("resource")
	public String getMinSI_BasedOnCityZoneFromDB(String planId) throws SQLException {

		String query = "SELECT zone_mapped FROM digit_common.t_pincode_zone_mapping where pincode = '"
				+ getPersonDataFromDB(planId, "pincode").trim() + "'";

		Connection connection = null;
		try {

			connection = DriverManager.getConnection("jdbc:postgresql://" + read.getNonAbsPreprodUrl(),
					read.getNonAbsPreprodUsername(), read.getNonAbsPreprodPassword());

		} catch (Exception e) {
			e.printStackTrace();
		}

		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);

		String value = "";

		while (resultSet.next()) {

			value = resultSet.getString("zone_mapped");
		}
		cityZoneFlag = null;
		switch (value) {
		case "A":
			cityZoneFlag = "A";
			query = "SELECT min_suminsured FROM digit_common.t_pincode_zone_mapping where id = '2'";
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {

				value = resultSet.getString("min_suminsured");
			}
			break;
		case "B":
			cityZoneFlag = "B";
			query = "SELECT min_suminsured FROM digit_common.t_pincode_zone_mapping where id = '3'";
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {

				value = resultSet.getString("min_suminsured");
			}
			break;
		case "C":
			cityZoneFlag = "C";
			query = "SELECT min_suminsured FROM digit_common.t_pincode_zone_mapping where id = '4'";
			resultSet = statement.executeQuery(query);
			while (resultSet.next()) {

				value = resultSet.getString("min_suminsured");
			}
			break;
		}
		resultSet.close();
		connection.close();
		return value;
	}

	public String getRetailDataFromDB(String column, String retailId) throws SQLException {

		final String query = "SELECT " + column + " FROM digit_retail_pa.retail_pa_testdata where id = '" + retailId
				+ "'";

		Connection connection = null;
		try {
			connection = DriverManager.getConnection(dbURL_UAT, "digitplus", "7pRqhErFPd");
		} catch (Exception e) {
			e.printStackTrace();
		}

		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);
		String value = "";
		while (resultSet.next()) {

			value = resultSet.getString(column);
		}
		connection.close();
		return value;
	}


	public String[] getGMCLargeDataFromDB(String gmcSalesId) throws SQLException {

		final String query = "SELECT * FROM digit_health.t_health_testdata_gmc_large where id = '" + gmcSalesId + "'";

		Connection connection = null;
		try {
			connection = DriverManager.getConnection(dbURL_UAT, usernameUAT, passwordUAT);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);

		String[] dbData = new String[80];

		while (resultSet.next()) {

			dbData[0]  = resultSet.getString("searchby");
			dbData[1]  = resultSet.getString("enterdocnum");
			dbData[2]  = resultSet.getString("doctype");
			dbData[3]  = resultSet.getString("docnum");
			dbData[4]  = resultSet.getString("policyholdername");
			dbData[5]  = resultSet.getString("pincode");
			dbData[6]  = resultSet.getString("industry");
			dbData[7]  = resultSet.getString("address");
			dbData[8]  = resultSet.getString("policytype");
			dbData[9]  = resultSet.getString("familydef");
			dbData[10] = resultSet.getString("policysibasis");
			dbData[11] = resultSet.getString("policyincpdate");
			dbData[12] = resultSet.getString("groupbusinesstype");
			dbData[13] = resultSet.getString("numoflives");
			dbData[14] = resultSet.getString("parentprempaidby");
			dbData[15] = resultSet.getString("prematstart");
			dbData[16] = resultSet.getString("prematexp");
			dbData[17] = resultSet.getString("livesatinception");
			dbData[18] = resultSet.getString("livesatexpiry");
			dbData[19] = resultSet.getString("asofdateclaims");
			dbData[20] = resultSet.getString("claimspaid");
			dbData[21] = resultSet.getString("claimsoutstanding");
			dbData[22] = resultSet.getString("expinsurername");
			dbData[23] = resultSet.getString("expbroker");
			dbData[24] = resultSet.getString("exppolicynum");
			dbData[25] = resultSet.getString("coverageonexpterms");
			dbData[26] = resultSet.getString("pedwp");
			dbData[27] = resultSet.getString("initwp");
			dbData[28] = resultSet.getString("specillnesswp");
			dbData[29] = resultSet.getString("roomrentrestriction");
			dbData[30] = resultSet.getString("propredonroomrent");
			dbData[31] = resultSet.getString("ambulance");
			dbData[32] = resultSet.getString("hosptlexpdays");
			dbData[33] = resultSet.getString("parentalcopay");
			dbData[34] = resultSet.getString("copayment");
			dbData[35] = resultSet.getString("maternity");
			dbData[36] = resultSet.getString("normaldelvrycap");
			dbData[37] = resultSet.getString("csecdelvrycap");
			dbData[38] = resultSet.getString("maternitycoverage");
			dbData[39] = resultSet.getString("prepostnatalcap");
			dbData[40] = resultSet.getString("maternitywaitperiod");
			dbData[41] = resultSet.getString("babydayonecover");
			dbData[42] = resultSet.getString("corpbuffer");
			dbData[43] = resultSet.getString("cbusage");
			dbData[44] = resultSet.getString("cbfamilycap");
			dbData[45] = resultSet.getString("nonntwrkcopay");
			dbData[46] = resultSet.getString("domhosptl");
			dbData[47] = resultSet.getString("aayushtreatment");
			dbData[48] = resultSet.getString("organdonorexp");
			dbData[49] = resultSet.getString("psycillsublimit");
			dbData[50] = resultSet.getString("terrorism");
			dbData[51] = resultSet.getString("externalcongcond");
			dbData[52] = resultSet.getString("lasiksurgery");
			dbData[53] = resultSet.getString("ntwrkhosptlreimbcopay");
			dbData[54] = resultSet.getString("roboticsurgery");
			dbData[55] = resultSet.getString("gammaknife");
			dbData[56] = resultSet.getString("cochimplant");
			dbData[57] = resultSet.getString("claimintclause");
			dbData[58] = resultSet.getString("docsubclause");
			dbData[59] = resultSet.getString("opdperfamily");
			dbData[60] = resultSet.getString("opdcopay");
			dbData[61] = resultSet.getString("cyberknife");
			dbData[62] = resultSet.getString("competitorqprem");
			dbData[63] = resultSet.getString("targetprem");
			dbData[64] = resultSet.getString("gstexemp");
			dbData[65] = resultSet.getString("payfrequency");
			dbData[66] = resultSet.getString("tpa");
			dbData[67] = resultSet.getString("suminsured");
			dbData[68] = resultSet.getString("ageband");
			dbData[69] = resultSet.getString("numofperson");
			dbData[70] = resultSet.getString("documentname");
			dbData[71] = resultSet.getString("otherdocdetails");
			dbData[72] = resultSet.getString("parentalsirest");
			dbData[73] = resultSet.getString("diseasecap");
			dbData[74] = resultSet.getString("brokerage");
			dbData[75] = resultSet.getString("ipd");
			dbData[76] = resultSet.getString("rmemail");
			dbData[77] = resultSet.getString("excel");
			dbData[78] = resultSet.getString("expectedrefreason");
			dbData[79] = resultSet.getString("comments");
		}
		connection.close();
		return dbData;
	}
	String[] dbData = null;
	public String[] getGMC_SMEDataFromDB(String gmcSalesId) throws SQLException {

		final String query = "SELECT * FROM digit_health.t_health_testdata_gmc_large where id = '" + gmcSalesId + "'";

		Connection connection = null;
		try {
			connection = DriverManager.getConnection(dbURL_UAT, usernameUAT, passwordUAT);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);

		String[] dbData = new String[43];

		while (resultSet.next()) {

			dbData[0]  = resultSet.getString("searchby");
			dbData[1]  = resultSet.getString("enterdocnum");
			dbData[2]  = resultSet.getString("doctype");
			dbData[3]  = resultSet.getString("docnum");
			dbData[4]  = resultSet.getString("policyholdername");
			dbData[5]  = resultSet.getString("pincode");
			dbData[6]  = resultSet.getString("industry");
			dbData[7]  = resultSet.getString("address");
			dbData[8]  = resultSet.getString("covidrenewal");
			dbData[9]  = resultSet.getString("policytype");
			dbData[10] = resultSet.getString("policyincpdate");
			dbData[11]  = resultSet.getString("familydef");
			dbData[12] = resultSet.getString("numofmembers");
			dbData[13] = resultSet.getString("groupbusinesstype");
			dbData[14] = resultSet.getString("prevmaspolnumber");
			dbData[15] = resultSet.getString("creditlink");
			dbData[16] = resultSet.getString("pedwaived");
			dbData[17] = resultSet.getString("roomrentlimit");
			dbData[18] = resultSet.getString("preposthospitalization");
			dbData[19] = resultSet.getString("copay");
			dbData[20] = resultSet.getString("initialwaitperiod");
			dbData[21] = resultSet.getString("specificillnesswaitperiod");
			dbData[22] = resultSet.getString("aayushcover");
			dbData[23] = resultSet.getString("organdonor");
			dbData[24] = resultSet.getString("diseasecap");
			dbData[25] = resultSet.getString("maternity");
			dbData[26] = resultSet.getString("additionalloading");
			dbData[27] = resultSet.getString("brokerage");
			dbData[28] = resultSet.getString("gstexemption");
			dbData[29] = resultSet.getString("tpa");
			dbData[30] = resultSet.getString("suminsured");
			dbData[31] = resultSet.getString("ageband");
			dbData[32] = resultSet.getString("numofpersons");
			dbData[33] = resultSet.getString("prempaidby");
			dbData[34] = resultSet.getString("childpolicyrequired");
			dbData[35] = resultSet.getString("rmemail");
			dbData[36] = resultSet.getString("customermobnum");
			dbData[37] = resultSet.getString("customeremail");
			dbData[38] = resultSet.getString("customeraccnum");
			dbData[39] = resultSet.getString("masterpolicysms");
			dbData[40] = resultSet.getString("masterpolicyemail");
			dbData[41] = resultSet.getString("childpolicysms");
			dbData[42] = resultSet.getString("childpolicyemail");

		}
		connection.close();
		return dbData;
	}

	public String[] getGroupCovidDataFromDB(String covidId) throws SQLException {

		final String query = "SELECT * FROM digit_health.t_health_testdata_groupcovid where id = '" + covidId + "'";

		Connection connection = null;
		try {
			connection = DriverManager.getConnection(dbURL_UAT, usernameUAT, passwordUAT);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);

		String[] dbData = new String[28];

		while (resultSet.next()) {

			dbData[0]  = resultSet.getString("gstnum");
			dbData[1]  = resultSet.getString("pan");
			dbData[2]  = resultSet.getString("company");
			dbData[3]  = resultSet.getString("address");
			dbData[4]  = resultSet.getString("pincode");
			dbData[5]  = resultSet.getString("mobilenum");
			dbData[6]  = resultSet.getString("email");
			dbData[7]  = resultSet.getString("rmemail");
			dbData[8]  = resultSet.getString("imdcode");
			dbData[9]  = resultSet.getString("prempaidby");
			dbData[10] = resultSet.getString("from");
			dbData[11] = resultSet.getString("maxsi");
			dbData[12] = resultSet.getString("totalgst");
			dbData[13] = resultSet.getString("countoflives");
			dbData[14] = resultSet.getString("coverletternum");
			dbData[15] = resultSet.getString("digitdeskticketnum");
			dbData[16] = resultSet.getString("remarks");
			dbData[17] = resultSet.getString("proposalterm");
			dbData[18] = resultSet.getString("proposaltype");
			dbData[19] = resultSet.getString("gstexemption");
			dbData[20] = resultSet.getString("vectorbornedisease");
			dbData[21] = resultSet.getString("mpemail");
			dbData[22] = resultSet.getString("mpsms");
			dbData[23] = resultSet.getString("cpemail");
			dbData[24] = resultSet.getString("cpsms");
			dbData[25] = resultSet.getString("memberdoc");
			dbData[26] = resultSet.getString("proposaldoc");
			dbData[27] = resultSet.getString("result");

		}
		connection.close();
		return dbData;
	}

	public String[] get_GPA_SME_DataFromDB(String gpa_sme_id) throws SQLException {

		final String query = "SELECT * FROM digit_health.t_health_testdata_gpa_sme where id = '" + gpa_sme_id + "'";

		Connection connection = null;
		try {
			connection = DriverManager.getConnection(dbURL_UAT, usernameUAT, passwordUAT);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery(query);

		String[] dbData = new String[42];

		while (resultSet.next()) {

			dbData[0]  = resultSet.getString("searchby");
			dbData[1]  = resultSet.getString("enterdocnum");
			dbData[2]  = resultSet.getString("policyholdername");
			dbData[3]  = resultSet.getString("doctype");
			dbData[4]  = resultSet.getString("docnum");
			dbData[5]  = resultSet.getString("pincode");
			dbData[6]  = resultSet.getString("address");
			dbData[7]  = resultSet.getString("policytype");
			dbData[8]  = resultSet.getString("policyincpdate");
			dbData[9]  = resultSet.getString("groupbusinesstype");
			dbData[10] = resultSet.getString("familydef");
			dbData[11] = resultSet.getString("numofmembers");
			dbData[12] = resultSet.getString("occupancy");
			dbData[13] = resultSet.getString("namedunnamed");
			dbData[14] = resultSet.getString("highestindvsi");
			dbData[15] = resultSet.getString("claimsinlast3yrs");
			dbData[16] = resultSet.getString("exppolclaimsamount");
			dbData[17] = resultSet.getString("exppolclaimscount");
			dbData[18] = resultSet.getString("aggregatesi");
			dbData[19] = resultSet.getString("prematincpn");
			dbData[20] = resultSet.getString("totalmembers");
			dbData[21] = resultSet.getString("aggregatesiinr");
			dbData[22] = resultSet.getString("terrorism");
			dbData[23] = resultSet.getString("geolimit");
			dbData[24] = resultSet.getString("coverage");
			dbData[25] = resultSet.getString("policycoverageandlimits");
			dbData[26] = resultSet.getString("brokerage");
			dbData[27] = resultSet.getString("gstexemption");
			dbData[28] = resultSet.getString("numoflivesuploaded");
			dbData[29] = resultSet.getString("totalpremiumuploaded");
			dbData[30] = resultSet.getString("childpolicyrequired");
			dbData[31] = resultSet.getString("prempaidby");
			dbData[32] = resultSet.getString("rmemail");
			dbData[33] = resultSet.getString("customermobnum");
			dbData[34] = resultSet.getString("customeremail");
			dbData[35] = resultSet.getString("masterpolicysms");
			dbData[36] = resultSet.getString("masterpolicyemail");
			dbData[37] = resultSet.getString("childpolicysms");
			dbData[38] = resultSet.getString("childpolicyemail");
			dbData[39] = resultSet.getString("customeraccnum");
			dbData[40] = resultSet.getString("memberdatafile");
			dbData[41] = resultSet.getString("proposaldocfile");
		}
		connection.close();
		return dbData;
	}
	public String[] get_GMC_Ops_DataFromDB(String dataFor,String gmc_ops_Id) throws SQLException {

		Connection connection = null;
		try {
			connection = DriverManager.getConnection(dbURL_Preprod, usernamePreprod, passwordPreprod);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Statement statement = connection.createStatement();
		String[] dbData = new String[42];
		String query = "";
		
		switch(dataFor.replaceAll(" ", "").toLowerCase()) {
		
		case "newcustomer":
			query = "SELECT * FROM digit_health.t_testdata_gmc_ops_newcustomer where id = '" + gmc_ops_Id + "'";
			
			ResultSet resultSetNC = statement.executeQuery(query);
			
			while (resultSetNC.next()) {
				
				dbData[0]  = resultSetNC.getString("customertype");
				dbData[1]  = resultSetNC.getString("corporatename");
				dbData[2]  = resultSetNC.getString("industrytype");
				dbData[3]  = resultSetNC.getString("companyname");
				dbData[4]  = resultSetNC.getString("imd");
				dbData[5]  = resultSetNC.getString("mobnum");
				dbData[6]  = resultSetNC.getString("email");
				dbData[7]  = resultSetNC.getString("address");
				dbData[8]  = resultSetNC.getString("pincode");
				dbData[9]  = resultSetNC.getString("gstnum");
				dbData[10] = resultSetNC.getString("pan");
			}
			break;
		case "newquote":
			query = "SELECT * FROM digit_health.testdata_gmc_ops_newquote where id = '" + gmc_ops_Id + "'";
			
			ResultSet resultSetNQ = statement.executeQuery(query);
			
			while (resultSetNQ.next()) {
				
				dbData[0]  = resultSetNQ.getString("gcorcompanyname");
				dbData[1]  = resultSetNQ.getString("childcompany");
				dbData[2]  = resultSetNQ.getString("grouptype");
				dbData[3]  = resultSetNQ.getString("creditlink");
				dbData[4]  = resultSetNQ.getString("policytype");
				dbData[5]  = resultSetNQ.getString("product");
				dbData[6]  = resultSetNQ.getString("numofmembers");
				dbData[7]  = resultSetNQ.getString("gstpercent");
				dbData[8]  = resultSetNQ.getString("basepremium");
				dbData[9]  = resultSetNQ.getString("maxage");
				dbData[10] = resultSetNQ.getString("familydef");
			}
			break;
		case "masterpolicy":
			query = "SELECT * FROM digit_health.testdata_gmc_ops_newquote where id = '" + gmc_ops_Id + "'";
			
			ResultSet resultSetMP = statement.executeQuery(query);
			
			while (resultSetMP.next()) {
				
				dbData[0]  = resultSetMP.getString("gcorcompanyname");
				dbData[1]  = resultSetMP.getString("childcompany");
				dbData[2]  = resultSetMP.getString("grouptype");
				dbData[3]  = resultSetMP.getString("creditlink");
				dbData[4]  = resultSetMP.getString("policytype");
				dbData[5]  = resultSetMP.getString("product");
				dbData[6]  = resultSetMP.getString("numofmembers");
				dbData[7]  = resultSetMP.getString("gstpercent");
				dbData[8]  = resultSetMP.getString("basepremium");
				dbData[9]  = resultSetMP.getString("maxage");
				dbData[10] = resultSetMP.getString("familydef");
			}
			break;
		}

		connection.close();
		return dbData;
	}

	public void createNewRowInGMCMasterPolicyTableInDB(String ins_id,String status,String product_code) {

		String query = "INSERT INTO digit_health.t_gmc_master_policy(ins_id, status, is_active, product_code)  VALUES ('"+ins_id+"', '"+status+"', true, '"+product_code+"');";

		try {
			Connection connection = null;
			try {

				connection = DriverManager.getConnection(dbURL_Preprod, usernamePreprod, passwordPreprod);

				if (connection != null)
					System.out.println(" Successfully connected to PostgreSQL database!\n");
				else
					System.out.println("Failed to make connection with database!\n");

			} catch (Exception e) {
				e.printStackTrace();
			}

			Statement statement = connection.createStatement();
			statement.executeUpdate(query);
			System.out.println("Quote Number Stored in Database!!");
			Thread.sleep(2000);
			connection.close();
		} catch (Exception e) {
			System.out.println("Comment could  not be stored.");
		}
	}

	public static void main(String[] args) throws Exception {
	
		//		PropertyFileReader read = new PropertyFileReader();
		//		final String dbURL = "jdbc:postgresql://digit-uat01.cpmhro02yml1.ap-south-1.rds.amazonaws.com:5432/digit_uat";
		//		final String username = read.getNonAbsUatUsername();
		//		final String password = read.getNonAbsUatPassword();
		//
		//		XSSFWorkbook book = new XSSFWorkbook();
		//		XSSFSheet sheet = book.createSheet("MyTestSheet");
		//		XSSFRow row;
		//
		////					XSSFFont dataFont, passFont, failFont, skippedFont;
		//		XSSFFont headerFont;
		//		CellStyle headerCellStyle;
		//
		//		headerFont = book.createFont();
		//		headerFont.setBold(true);
		//		headerFont.setFontHeightInPoints((short) 15);
		//		headerFont.setColor(IndexedColors.ORANGE.getIndex());
		//
		//		headerCellStyle = book.createCellStyle();
		//		headerCellStyle.setFont(headerFont);
		//		headerCellStyle.setFillBackgroundColor(IndexedColors.AQUA.getIndex());
		//
		//		LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
		//
		//		final String query = "SELECT * FROM digit_retail_pa.retail_pa_testdata";
		//
		//		Connection connection = null;
		//		try {
		//			connection = DriverManager.getConnection(dbURL, username, password);
		//		} catch (Exception e) {
		//			e.printStackTrace();
		//		}
		//
		//		int rowId = 0, cellID = 0, colHead = 0;
		//		Cell cell;
		//		Statement statement = connection.createStatement();
		//		ResultSet resultSet = statement.executeQuery(query);
		//		while (resultSet.next()) {
		//
		//			row = sheet.createRow(rowId);
		//			data.put("id", resultSet.getString("id"));
		//			data.put("selfdob", resultSet.getString("selfdob"));
		//			data.put("spousedob", resultSet.getString("spousedob"));
		//			data.put("c1dob", resultSet.getString("c1dob"));
		//			data.put("c2dob", resultSet.getString("c2dob"));
		//			data.put("fatherdob", resultSet.getString("fatherdob"));
		//			data.put("motherdob", resultSet.getString("motherdob"));
		//			data.put("coverfor", resultSet.getString("coverfor"));
		//			data.put("planfor", resultSet.getString("planfor"));
		//			data.put("earning", resultSet.getString("earning"));
		//			data.put("basesi", resultSet.getString("basesi"));
		//			data.put("selfsi", resultSet.getString("selfsi"));
		//			data.put("spousesi", resultSet.getString("spousesi"));
		//			data.put("c1si", resultSet.getString("c1si"));
		//			data.put("c2si", resultSet.getString("c2si"));
		//			data.put("fathersi", resultSet.getString("fathersi"));
		//			data.put("mothersi", resultSet.getString("mothersi"));
		//			data.put("grossannualincome", resultSet.getString("grossannualincome"));
		//			data.put("selfrc", resultSet.getString("selfrc"));
		//			data.put("spouserc", resultSet.getString("spouserc"));
		//			data.put("c1rc", resultSet.getString("c1rc"));
		//			data.put("c2rc", resultSet.getString("c2rc"));
		//			data.put("fatherrc", resultSet.getString("fatherrc"));
		//			data.put("motherrc", resultSet.getString("motherrc"));
		//			data.put("familypolicyterm", resultSet.getString("familypolicyterm"));
		//			data.put("parentpolicyterm", resultSet.getString("parentspolicyterm"));
		//			data.put("planfamily", resultSet.getString("planfamily"));
		//			data.put("planparent", resultSet.getString("planparent"));
		//			data.put("pincode", resultSet.getString("pincode"));
		//			data.put("ped", resultSet.getString("ped"));
		//			data.put("coverage", resultSet.getString("coverage"));
		//			data.put("modeofpayment", resultSet.getString("modeofpayment"));
		//			data.put("imdcode", resultSet.getString("imdcode"));
		//			data.put("sec27", resultSet.getString("sec27"));
		//			data.put("cb", resultSet.getString("cb"));
		//			data.put("result", resultSet.getString("result"));
		//			data.put("comments", resultSet.getString("comments"));
		//			while (colHead++ < 1) {
		//				Set<String> allcolumns = data.keySet();
		//				for (String columnName : allcolumns) {
		//					cell = row.createCell(cellID++);
		//					cell.setCellStyle(headerCellStyle);
		//					cell.setCellValue(columnName);
		//				}
		//				row = sheet.createRow(++rowId);
		//			}
		//			cellID = 0;
		//			Collection<String> set = data.values();
		//			for (String value : set) {
		//				cell = row.createCell(cellID++);
		//				cell.setCellValue(value);
		//			}
		//			rowId++;
		//		}
		//		connection.close();
		//		Calendar cal = Calendar.getInstance(); 
		//		SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy");
		//		String timeStamp = java.time.LocalTime.now() +"";
		//		
		//		System.out.println(timeStamp);
		//		String dateNTime = s.format(cal.getTime())+"_Time_"+timeStamp.split(":")[0]+"-"+timeStamp.split(":")[1];
		//		System.out.println(dateNTime);
		//		File file = new File(Paths.get("").toAbsolutePath().toString() + "/target/ExcelReportRPA/TestData_Dt-"+dateNTime+"_RPA.xlsx");
		//		if (!file.exists()) {
		//			FileOutputStream out = new FileOutputStream(file);
		//			book.write(out);
		//			out.flush();
		//			out.close();
		//		}
		//		book.close();
		//		System.out.println("Pass");
	}
}
