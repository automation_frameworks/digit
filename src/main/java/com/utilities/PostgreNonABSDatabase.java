/**
 * 
 */
package com.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedHashMap;



/**
 * @author Abhishek.Shukla
 *
 */
public class PostgreNonABSDatabase {

	static Connection connection;
	static Statement statement;
	static ResultSet resultSet;

	/**
	 * 
	 * @param environment
	 */
	public void connectToPostgreNonABSDatabase(String databaseName, String environment) {
		PropertyFileReader prop = new PropertyFileReader();
		try {
			if (databaseName.equalsIgnoreCase("non abs")) {
				if (environment.contains("uat")) {
					connection = DriverManager.getConnection(prop.getNonAbsUatUrl(), prop.getNonAbsUatUsername(),
							prop.getNonAbsUatPassword());
				}

				else if (environment.contains("preprod")) {
					connection = DriverManager.getConnection(prop.getNonAbsPreprodUrl(),
							prop.getNonAbsPreprodUsername(), prop.getNonAbsPreprodPassword());
				}
				
				else if (environment.contains("prod")) {
					connection = DriverManager.getConnection(prop.getNonAbsProdUrl(),
							prop.getNonAbsProdUsername(), prop.getNonAbsProdPassword());
				}

				if (connection != null)
					System.out.println("Successfully connected to " + databaseName.toUpperCase() + " database "
							+ environment.toUpperCase() + " environment - PostgreSQL ! \n");
				else
					System.out.println("Failed to connect with " + databaseName.toUpperCase() + " in "
							+ environment.toUpperCase() + "! \n");

				statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			} else if (databaseName.equalsIgnoreCase("abs")) {
				System.out.println("Connected to ABS database");
			}
		} catch (SQLException e) {
			System.out.println("Connection failure. Check database connectivity");
			e.printStackTrace();
		}
	}

	public boolean closePostgreNonABSDatabaseConnection(String databaseName) {
		try {
			if (databaseName.equalsIgnoreCase("abs")) {
				statement.close();
				connection.close();
				System.out.println("ABS Database connection is closed");
				return true;
			} else if (databaseName.equalsIgnoreCase("nonabs")) {
				System.out.println("Non ABS database connection is closed");
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Failed to close the database connection");
			return false;
		}
		return false;
	}

	/**
	 * 
	 * @param environment
	 * @param query
	 * @return
	 */
	public HashMap<String, String> getValuesMapFromDataBase(String query) {
		HashMap<String, String> dbMap = null;
		try {
			resultSet = statement.executeQuery(query);
			dbMap = new LinkedHashMap<>();
			ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
			int columnCount = resultSetMetaData.getColumnCount();
			while (resultSet.next()) {
				for (int i = 1; i <= columnCount; i++) {
					String columnValue = resultSet.getString(i);
					dbMap.put(resultSetMetaData.getColumnName(i), columnValue);
				}
			}
			resultSet.close();
			/*
			 * connection.close(); System.out.println("Database connection is closed");
			 */
		}  catch (SQLException e) {
			System.out.println("Connection failure. Check database connectivity");
			e.printStackTrace();
		}
		return dbMap;
	}

	/**
	 * 
	 * @param environment
	 * @param query
	 * @param columnLabel
	 * @return
	 */
	public String getValueFromDataBase(String environment, String query, String columnLabel) {
		String result = null;
		PropertyFileReader prop = new PropertyFileReader();
		try {
			Connection connection = null;
			if (prop.getEnvironment().contains("uat")) {
				connection = DriverManager.getConnection(prop.getNonAbsUatUrl(), prop.getNonAbsUatUsername(),
						prop.getNonAbsUatPassword());
			}

			else if (prop.getEnvironment().equals("preprod")) {
				connection = DriverManager.getConnection(prop.getNonAbsPreprodUrl(), prop.getNonAbsPreprodUsername(),
						prop.getNonAbsPreprodPassword());
			}

			if (connection != null)
				System.out.println("Successfully connected to PostgreSQL database!");
			else
				System.out.println("Failed to make connection with database!");

			Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			ResultSet resultSet = statement.executeQuery(query);
			resultSet.next();
			result = resultSet.getString(columnLabel);
			statement.close();
			resultSet.close();
			connection.close();
			System.out.println("Database connection is closed");

		} catch (SQLException e) {
			System.out.println("Connection failure. Check database connectivity");
			e.printStackTrace();
		}
		return result;
	}

}
