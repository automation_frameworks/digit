Feature: To verify the functionalities of health


Scenario: To verify normal policy issuance

Given logged to the application with details "UserName :=  35327650 # Password := Digit@123$"
When module "Health Insurance" is selected
And select "Create Custom Plan" option in Agent Health home page
And enter age details as "Proposer := 45 # Spouse := 42" in Agent Health age details page