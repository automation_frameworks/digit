@FourWheeler @Motor @Agent_Sanity @Regression
Feature: To verify the functionalities of Four Wheeler is working fine

@FourWheeler_New
Scenario: 4W NEW
Vehicle Ownership : Corporate

Given user login to the application with details
#Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := FOUR WHEELER COMPREHENSIVE # Brand New Vehicle := true # RTO Code := KA01 # Vehicle Make := HONDA # Vehicle Model := ACCORD # Vehicle Variant := 1.6 (AT) (1596.0)(Petrol) # Manufacturing Year := 2019 # Vehicle Ownership := INDIVIDUAL" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And upload the previous policy document
And button "Generate Policy" is clicked
And verify policy issuance is successful for Four Wheeler
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded 


@FourWheelerTP_RollOver
Scenario: 4WTP ROLLOVER
Vehicle Ownership : Corporate

Given user login to the application with details
#Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := FOUR WHEELER THIRD PARTY # Vehicle With Registration := true # Vehicle Make := HONDA # Vehicle Model := ACCORD # Vehicle Variant := 1.6 (AT) (1596.0)(Petrol)# Registration Year := 2018 # Vehicle Ownership := INDIVIDUAL" are entered
And previous policy details "Previous Policy Expiry Date := [TODAY]" are entered
#And previous policy details "previouspolicyexpirydate := [Today+5] # previousinsurername := Bharti AXA General Insurance Company Limited # previouspolicynumber := 7757576" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And nominee details "Nominee Name := Sathesh # Nominee Relation := Brother" are entered
And enter payment details
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And button "Generate Policy" is clicked
And verify quote issuance is successful for Four Wheeler
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded 


@FourWheelerTP_New
Scenario: 4WTP NEW
Vehicle Ownership : Corporate

Given user login to the application with details
#Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Motor Insurance" is selected
#And vehicle details "Vehicle Type := FOUR WHEELER THIRD PARTY # Brand New Vehicle := true # registrationnumber := MH04RT54 # Vehicle Make := HONDA # Vehicle Model := AMAZE # Vehicle Variant := 1.2 E (1199.0)(Petrol) # Registration Year := 2018 # Vehicle Ownership := INDIVIDUAL" are entered
And vehicle details "Vehicle Type := FOUR WHEELER THIRD PARTY # Brand New Vehicle := true # RTO Code := KA01 # Vehicle Make := HONDA # Vehicle Model := ACCORD # Vehicle Variant := 1.6 (AT) (1596.0)(Petrol) # Manufacturing Year := 2019 # Vehicle Ownership := CORPORATE" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And button "Generate Policy" is clicked
And verify quote issuance is successful for Four Wheeler
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded 


@FourWheeler_Breakin @Breakin
Scenario: 4W BREAKIN

Given user login to the application with details
#Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := FOUR WHEELER COMPREHENSIVE # Vehicle with Registration := true # Vehicle Make := HONDA # Vehicle Model := ACCORD # Vehicle Variant := 1.6 (AT) (1596.0)(Petrol)(5 str) # Registration Year := 2018 # Vehicle Ownership := INDIVIDUAL" are entered
And previous policy details "Previous Policy Expiry Date := [TODAY-1] # Previous Policy type := Comprehensive Policy with Zero Dep and EP" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And nominee details "Nominee Name := Sathesh # Nominee Relation := Brother" are entered
And button "Save Quote" is clicked
And click on download buttun
And survey details "Survey Type := Self Inspection" are entered
And upload the previous policy document
And button "Submit" is clicked
And verify quote issuance is successful for Four Wheeler
And button "Download Quote" is clicked
And verify the policyStatus
Then verify whether file has downloaded 

Then update the survey comments for the quote created

And open the policy with "Policy Number := [Policy_Number]"
And button "Recalculate" is clicked
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And button "Generate policy" is clicked
And verify policy issuance is successful for Four Wheeler
And verify the policyStatus


@FourWheeler_Rollover
Scenario: 4W ROLLOVER

Given user login to the application with details
#Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := FOUR WHEELER COMPREHENSIVE # Vehicle with Registration := true # Vehicle Make := HONDA # Vehicle Model := ACCORD # Vehicle Variant := 1.6 (AT) (1596.0)(Petrol)(5 str) # Registration Month := January # Registration Year := 2018 # Vehicle Ownership := CORPORATE" are entered
And previous policy details "Previous Policy Expiry Date := [TODAY]" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And upload the previous policy document
And button "Generate Policy" is clicked
And verify policy issuance is successful for Four Wheeler
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded