@Loader @Agent_Sanity
Feature: To verify Loader Functionality is working fine

@2WLoader
Scenario: 2W Loader

Given user login to the application with details
When module "Motor Loader" is selected
And verify the page and select "Two Wheeler" from the dropdown
When upload "Two Wheeler" excel sheet
And edit changes for "2W LOADER_OD New"
And edit changes for "2W LOADER_OD RollOver"
And edit changes for "2W LOADER_OD Breakin"
And click on QuickQuote button
And click on GetQuote button
And click on IssuePolicy button
#Then log the results in the report

@4WLoader
Scenario: 4W Loader

Given user login to the application with details
When module "Motor Loader" is selected
And verify the page and select "Private Car" from the dropdown
When upload "Private Car" excel sheet
And edit changes for "4W LOADER_OD New"
And edit changes for "4W LOADER_OD RollOver"
And edit changes for "4W LOADER_OD Breakin"
And click on QuickQuote button
And click on GetQuote button
And click on IssuePolicy button
#Then log the results in the report


@CVLoader
Scenario: CV Loader

Given user login to the application with details
When module "Motor Loader" is selected
And verify the page and select "Commercial Vehicle Comprehensive" from the dropdown
When upload "Commercial Vehicle" excel sheet
And edit changes for "CV LOADER_OD New"
And edit changes for "CV LOADER_OD RollOver"
And edit changes for "CV LOADER_OD Breakin"
And click on QuickQuote button
And click on GetQuote button
And click on IssuePolicy button
#Then log the results in the report