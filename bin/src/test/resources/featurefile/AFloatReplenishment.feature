@FloatReplenishment @Agent_Sanity
Feature: To verify the functionalities of float replenishment is working fine

@FloatReplenishment_PayOnline
Scenario Outline: Float Replenishment

Given user login to the application with details
When module "Float Replenishment" is selected
And enter "<Amount>" in amount to be added
And choose payment type as "Credit / Debit Card" and enter details
And verify success Message from toast if it contains "1 Success, 0 Failed"


Examples:

|Amount|
|200000|