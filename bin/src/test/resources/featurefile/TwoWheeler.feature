@TwoWheeler @Motor @Agent_Sanity @Regression
Feature: To verify the functionalities of Two Wheeler is working fine

@TwoWheeler_Breakin @Breakin
Scenario: 2W BREAKIN

Given user login to the application with details
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := TWO WHEELER COMPREHENSIVE # Vehicle with Registration := true # Vehicle Type := MOTOR BIKE # Vehicle Make := HONDA # Vehicle Model := SHINE # Vehicle Variant := ELEC ST DISC BRAKE AWS (125.0)(Petrol) # Registration Year := 2018 # Vehicle Ownership := INDIVIDUAL" are entered
And previous policy details "Previous Policy Expiry Date := [TODAY-1]" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And nominee details "Nominee Name := Sathesh # Nominee Relation := Brother" are entered
And button "Save Quote" is clicked
And click on download buttun
And survey details "Survey Type := Self Inspection" are entered
And upload the previous policy document
And button "Submit" is clicked
And verify quote issuance is successful for Four Wheeler
And button "Download Quote" is clicked

Then update the survey comments for the quote created

And open the policy with "Policy Number := [Policy_Number]"
And button "Recalculate" is clicked
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And button "Generate policy" is clicked
And verify policy issuance is successful for Two Wheeler
And verify the policyStatus

@TwoWheeler_Rollover
Scenario: 2W ROLLOVER
Vehicle Ownership : Corporate

Given user login to the application with details
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := TWO WHEELER COMPREHENSIVE # Vehicle with Registration := true # Two Wheeler Type := MOTOR BIKE # Vehicle Make := HONDA # Vehicle Model := SHINE # Vehicle Variant := ELEC ST DISC BRAKE AWS (125.0)(Petrol) # Registration Year := 2018 # Vehicle Ownership := CORPORATE" are entered
And previous policy details "" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And upload the previous policy document
And button "Generate Policy" is clicked
And verify policy issuance is successful for Two Wheeler
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded 


@TwoWheeler_New
Scenario: 2W NEW
Vehicle Ownership : Corporate

Given user login to the application with details
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := TWO WHEELER COMPREHENSIVE # Brand New Vehicle := true # RTO Code := KA01 # Two Wheeler Type := SCOOTER # Vehicle Make := HONDA # Vehicle Model := ACTIVA # Vehicle Variant := 3G (110.0)(Petrol) # Manufacturing Year := 2019 # Vehicle Ownership := CORPORATE" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And upload the previous policy document
And button "Generate Policy" is clicked
And verify policy issuance is successful for Two Wheeler
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded 


@TwoWheelerTP_RollOver
Scenario: 2WTP ROLLOVER
Vehicle Ownership : Corporate

Given user login to the application with details
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := TWO WHEELER  THIRD PARTY # Vehicle With Registration := true # Two Wheeler Type := SCOOTER # Vehicle Make := HONDA # Vehicle Model := ACTIVA # Vehicle Variant := 3G (110.0)(Petrol) # Registration Year := 2018 # Vehicle Ownership := CORPORATE" are entered
And previous policy details "Previous Policy Expiry Date := [TODAY]" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And button "Generate Policy" is clicked
And verify policy issuance is successful for Two Wheeler
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded 

@TwoWheelerTP_New
Scenario: 2WTP NEW
Vehicle Ownership : Corporate

Given user login to the application with details
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := TWO WHEELER  THIRD PARTY # Brand New Vehicle := true # RTO Code := KA01 # Two Wheeler Type := MOTOR BIKE # Vehicle Make := HONDA # Vehicle Model := SHINE # Vehicle Variant := ELEC ST DISC BRAKE AWS (125.0)(Petrol) # Manufacturing Year := 2019 # Vehicle Ownership := INDIVIDUAL" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And button "Generate Policy" is clicked
And verify policy issuance is successful for Two Wheeler
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded 

