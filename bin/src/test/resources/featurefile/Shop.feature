@Property @Agent_Sanity @Regression
Feature: To verify the functionalities of Home and Shop Insurance 

@Shop
Scenario: SHOP 

Given user login to the application with details 
When module "Shop Insurance" is selected
And ownership type is selected as "Corporate" in Agent Shop Insurance page
And shop details "Shop Category := Art Dealers # Age of Property := 2 # Address of Risk Location := Digit Insurance # Pin code := 560001 # Square Feet Area := 1000 # Floor Number := 2 " are entered in Agent Shop Insurance page
And SFSP details "Content Stock := 300000 # Terrorism := Yes" are entered in Agent Shop Insurance page
And burglary details "" are entered in Agent Shop Insurance page
And asset care details "Plate Glass Cover := 1000 # Neon Sign Cover := 1000 # Equipment 1 := Television # Serial Number 1 := KLSDJS9039 # Age 1 := 2 " are entered in Agent Shop Insurance page
And employee compensation details "No Of Employees := 5 # Total Monthly Salary := 10000" are entered in Agent Shop Insurance page
And action "Show Premium" is performed in Shop Insurance page
And insurer details "Insured Name := Insured # Pincode := 560001 # Address := address # Mobile Number := 9876543210 # Email ID := test@godigit.com" are entered in Agent Shop Insurance page
And action "Save Quote" is performed in Shop Insurance page
And payment details "Payment By Customer Cheque" are entered in Agent Shop Insurance page
And action "Proceed To Pay" is performed in Shop Insurance page
And verify the success page for Shop Insurance
And verify the policyStatus

@Home
Scenario: HOME 

Given user login to the application with details 
When module "Home Insurance" is selected
And ownership type is selected as "Corporate" in Agent Home Insurance page
And home details "Age of Property := 2 # Address of Risk Location := Digit Insurance # Square Feet Area := 1000" are entered in Agent Home Insurance page
And SFSP details "Terrorism := Yes" are entered in Agent Home Insurance page
And burglary details "" are entered in Agent Home Insurance page
And asset care details "Frame Number := GHJJH78738 # Cycle Make := SCOTT # Cycle Model := Model # Sum Insured := 10000 # Equipment 1 := Washing Machine # Age 1 := 2 " are entered in Agent Home Insurance page
And action "Show Premium" is performed in Home Insurance page
And insurer details "Insured Name := Insured # Pincode := 560001 # Address := address # Mobile Number := 9876543210 # Email ID := test@godigit.com" are entered in Agent Shop Insurance page
And action "Save Quote" is performed in Home Insurance page
And payment details "Payment By Customer Cheque" are entered in Agent Home Insurance page
And action "Proceed To Pay" is performed in Home Insurance page
And verify the success page for Home Insurance
And verify the policyStatus