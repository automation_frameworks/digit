@Regression @Motor_Extra
Feature: To verify the policy issuance of various Two Wheeler policy types

@TwoWheeler_Rollover_2+2
Scenario: Two Wheeler RollOver 2+2

Given user login to the application with details
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := TWO WHEELER COMPREHENSIVE # Policy Type := 2 YEAR OD + 2 YEAR TP # Vehicle Type := MOTOR BIKE # Vehicle Make := HONDA # Vehicle Model := SHINE # Vehicle Variant := ELEC ST DISC BRAKE AWS (125.0)(Petrol) # Registration Year := 2018 # Vehicle Ownership := INDIVIDUAL" are entered
And previous policy details "Previous Policy Expiry Date := [TODAY]" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And enter payment details
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And upload the previous policy document
And button "Generate Policy" is clicked
And verify quote issuance is successful for Commercial Vehicle
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded

@TwoWheeler_Rollover_3+3
Scenario: Two Wheeler RollOver 3+3

Given user login to the application with details
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := TWO WHEELER COMPREHENSIVE # Policy Type := 3 YEAR OD + 3 YEAR TP # Vehicle Type := MOTOR BIKE # Vehicle Make := HONDA # Vehicle Model := SHINE # Vehicle Variant := ELEC ST DISC BRAKE AWS (125.0)(Petrol) # Registration Year := 2018 # Vehicle Ownership := INDIVIDUAL" are entered
And previous policy details "Previous Policy Expiry Date := [TODAY]" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And enter payment details
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And upload the previous policy document
And button "Generate Policy" is clicked
And verify quote issuance is successful for Commercial Vehicle
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded

@TwoWheeler_New_5+5
Scenario: Two Wheeler RollOver 3+3

Given user login to the application with details
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := TWO WHEELER COMPREHENSIVE # Brand New Vehicle := true # Policy Type := 5 YEAR OD + 5 YEAR TP # Vehicle Type := MOTOR BIKE # Vehicle Make := HONDA # Vehicle Model := SHINE # Vehicle Variant := ELEC ST DISC BRAKE AWS (125.0)(Petrol) # Manufacturing Year := 2019 # Vehicle Ownership := CORPORATE" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And enter payment details
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And upload the previous policy document
And button "Generate Policy" is clicked
And verify quote issuance is successful for Commercial Vehicle
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded

@TwoWheeler_New_1ODOnly
Scenario: Two Wheeler New 1 Year OD Only

Given user login to the application with details
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := TWO WHEELER COMPREHENSIVE # Brand New Vehicle := true # Policy Type := 1 YEAR OD ONLY # Two Wheeler Type := SCOOTER # Vehicle Make := APRILIA # Vehicle Model := SR # Vehicle Variant := 150 STD (155.0)(Petrol) # Manufacturing Year := 2019 # Vehicle Ownership := CORPORATE" are entered
And third party policy details "" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And enter payment details
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And upload the previous policy document
And button "Generate Policy" is clicked
And verify quote issuance is successful for Commercial Vehicle
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded