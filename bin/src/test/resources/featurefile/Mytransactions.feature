@MyTransactions @Agent1_Sanity
Feature: verify the functionality of My Transactions

@MyTransaction
Scenario: To verify whether user is able to filter using todays date
Given user login to the application with details
When user selects the "My Transactions" module
When button "Filter" is clicked
Then check if the policies are displayed





@MyTransactions_SearchByPolicyNumber
Scenario: To verify whether user able to search the policy using policy number
Given user login to the application with details
When user selects the "My Transactions" module
When user keep the cursor on my transaction search box
Then user select the "Policy Number" from drop down menu options
And user search the policy using policy number "preprodplusPolicyNo := D400082029 # uatPolicyNo := D300074612"
Then It should display the policy details related to the policy number "preprodplusPolicyNo := D400082029  # uatPolicyNo := D300074612"
And select the check box of that particular policy number
And click on download option under Actions for the particular policy number of and clicks on download selected image
Then verify whether file has downloaded 
When click on the "Policy Number"
Then it should display all the vehicle datails in single page  


@MyTransactions_SearchPolicyUsingMobileNumber
Scenario: To verify whether user able to search the policy using mobile number
Given user login to the application with details
When user selects the "My Transactions" module
When user keep the cursor on my transaction search box
Then user select the "Mobile Number" from drop down menu options
And user search the quote/policy using mobile number "preprodplusMobileNo := 8765432134 # uatMobileNo := 9864035777"
And select the check box of first policy number
And click on download option under Actions for first policy number and clicks on download selected image
Then verify whether file has downloaded 
When click on the "Policy Number"
Then it should display all the vehicle datails related to the mobile number in single page "preprodplusMobileNo := 8765432134 # uatMobileNo := 8765434567"

@MyTransactions_SearchPolicyUsingRegistrationNumber
Scenario: To verify whether user able to search the policy using registration number
Given user login to the application with details
When user selects the "My Transactions" module
When user keep the cursor on my transaction search box
Then user select the "Registration Number" from drop down menu options
And user search the policy using registration number "preprodplusRegistrationNo := KA01AA3617 # uatRegistrationNo := KA70AD1234"
Then it should display the policy details related to that registration number "preprodplusRegistrationNo := KA01AA3617 # uatRegistrationNo := KA01A8735"
And select the check box of first policy number
And click on download option under Actions for first policy number and clicks on download selected image
Then verify whether file has downloaded 
When click on the "Policy Number"
Then it should display all the vehicle datails in single page

@MyTransactions_SearchPolicyUsingEngineNumber
Scenario: To verify whether user able to search the policy using Engine number
Given user login to the application with details
When user selects the "My Transactions" module
When user keep the cursor on my transaction search box
Then user select the "Engine Number" from drop down menu options
And user search the quote/policy using Engine number "preprodplusEngineNo := 9GX3UBQVONDP0YB # uatEngineNo := DJWJDJWDNJC"
#Then it should display the policy details related to that Engine number "preprodplusEngineNo := 9GX3UBQVONDP0YB # preprodtestEngineNo := KA01A8735"
And select the check box of first policy number
And click on download option under Actions for first policy number and clicks on download selected image
Then verify whether file has downloaded 
When click on the "Policy Number"
Then it should display all the vehicle datails related to the Engine number "preprodplusEngineNo := 9GX3UBQVONDP0YB # uatEngineNo := KA01A8735"

@MyTransactions_SearchPolicyUsingChassisNumber
Scenario: To verify whether user able to search the policy using Chassis number
Given user login to the application with details
When user selects the "My Transactions" module
When user keep the cursor on my transaction search box
Then user select the "Chassis Number" from drop down menu options
And  user search the quote/policy using chassis number "preprodplusChassisNo := QBDDX0RHOJBWZFI # uatChassisNo := ZHXJJAWJA"
#Then it should display the policy details related to that registration number "preprodplusChassisNo := KA01AA3617 # preprodtestChassisNo := KA01A8735"
And select the check box of first policy number
And click on download option under Actions for first policy number and clicks on download selected image
Then verify whether file has downloaded 
When click on the "Policy Number"
Then it should display all the vehicle datails related to the chassis number "preprodplusChassisNo := QBDDX0RHOJBWZFI # uatChassisNo := QBDDX0RHOJBWZFI123"

@MyTransactions_SearchByQuotes
Scenario: To verify whether user able to search the policy using Quote number
Given user login to the application with details
When user selects the "My Transactions" module
And clicks on the Quotes
When user keep the cursor on my transaction search box
Then user select the "Quote Number" from drop down menu options
And user search the Quote using Quote number "preprodplusQuoteNo := D400153234 # uatQuoteNo := D300074622"
Then It should display the Quote details related to the Quote number "preprodplusQuoteNo := D400111733 # uatQuoteNo := D300074622"
And select the check box of that particular quote number
And click on download option under Actions for the first quote number and clicks on download selected image
Then verify whether file has downloaded 
When click on the "Quote Number"
Then it should display all the vehicle datails in single page


@MyTransactions_SearchQuoteUsingRegistrationNumber
Scenario: To verify whether user able to search the quote using registration number
Given user login to the application with details
When user selects the "My Transactions" module
And clicks on the Quotes
When user keep the cursor on my transaction search box
Then user select the "Quote Number" from drop down menu options
And user search the quote using registration number "preprodplusRegistrationNo := KA01FJ2952 # uatRegistrationNo := MH12AA1234"
Then it should display the policy details related to that registration number "preprodplusRegistrationNo := KA01FJ2952 # uatRegistrationNo := MH12AA1234"
And select the check box of that particular quote number
And click on download option under Actions for first policy number and clicks on download selected image
Then verify whether file has downloaded 
When click on the "Quote Number"
Then it should display all the vehicle datails in single page

@MyTransactions_SearchQuoteUsingMobileNumber
Scenario: Search Quote using mobile Number
Given user login to the application with details
When user selects the "My Transactions" module
And clicks on the Quotes
When user keep the cursor on my transaction search box
Then user select the "Quote Number" from drop down menu options
And user search the quote/policy using mobile number "preprodplusMobileNo := 8765432134 # uatMobileNo := 1234567876"
And select the check box of that particular quote number
And click on download option under Actions for first quote number and clicks on download selected image
Then verify whether file has downloaded 
When click on the "Quote Number"
Then it should display all the vehicle datails related to the mobile number in single page "preprodplusMobileNo := 8765432134 # uatMobileNo := 1234567876"

@MyTransactions_SearchQuoteUsingEngineNumber
Scenario: To verify whether user able to search the quote using Engine number
Given user login to the application with details
When user selects the "My Transactions" module
And clicks on the Quotes
When user keep the cursor on my transaction search box
Then user select the "Engine Number" from drop down menu options
And user search the quote/policy using Engine number "preprodplusEngineNo := 9GX3UBQVONDP0YB # uatEngineNo := SDFGHJHGFDGHJ"
#Then it should display the policy details related to that registration number "preprodplusEngineNo := KA01FJ2952 # uatEngineNo := SDFGHJHGFDGHJ"
And select the check box of that particular quote number
And click on download option under Actions for first policy number and clicks on download selected image
Then verify whether file has downloaded 
When click on the "Quote Number"
Then it should display all the vehicle datails related to the Engine number "preprodplusEngineNo := 9GX3UBQVONDP0YB # uatEngineNo := SDFGHJHGFDGHJ"

@MyTransactions_SearchQuoteUsingChassisNumber
Scenario: To verify whether user able to search the quote using chassis number
Given user login to the application with details
When user selects the "My Transactions" module
And clicks on the Quotes
When user keep the cursor on my transaction search box
Then user select the "Chassis Number" from drop down menu options
And  user search the quote/policy using chassis number "preprodplusChassisNo := QBDDX0RHOJBWZFI # uatChassisNo := ASDFGHJHGFDFGH"
#Then it should display the policy details related to that registration number "preprodplusChassisNo := QBDDX0RHOJBWZFI # uatChassisNo := ASDFGHJHGFDFGH"
And select the check box of that particular quote number
And click on download option under Actions for first policy number and clicks on download selected image
Then verify whether file has downloaded 
When click on the "Quote Number"
Then it should display all the vehicle datails related to the chassis number "preprodplusChassisNo := QBDDX0RHOJBWZFI # uatChassisNo := ASDFGHJHGFDFGH"


@MyTransactions_VerifyDAteFilterWithMaximumDays
Scenario: Verify whether user able to filter more than five days
Given user login to the application with details
When user selects the "My Transactions" module
And clicks on the Quotes
Then user select the date more than five days "startDate := [TODAY-8] # endDate:=[TODAY-1]"
When button "Filter" is clicked
Then it should populate the toaster as "Date range can be maximum of 5 days."



















