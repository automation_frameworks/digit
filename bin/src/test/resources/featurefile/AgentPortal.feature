Feature: To issue a policy

@First @SmokeTest @Sanity
Scenario Outline: To issue a policy wwith Four Wheeler

Given logged to the application with details "User Name := <username> # Password := <password>"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := Four Wheeler # Vehicle With Registration := true " are entered
And button "Get Quote" is clicked
#Then "Save Quote" button should be displayed
#And button "Recalculate" is clicked
#Then "Create Quote" button should be displayed

Examples:
|username | password|
|35327650 | Digit@123$|
#|35327651 | Digit@123$|

@Smoketest @Sample
Scenario: To issue a policy wwith Four Wheeler

Given logged to the application with details "User Name := 35327650 # Password := digit123"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := FOUR WHEELER COMPREHENSIVE # Vehicle with Registration := true # Registration Number := KA01aa1224 # Vehicle Make := CHEVROLET # Registration Year := 2009" are entered
And previous policy details "Don't know previous Policy details := true" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked

@Smoketest
Scenario: To issue a policy wwith Two Wheeler

Given logged to the application with details "User Name := 35327650 # Password := digit123"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := TWO WHEELER COMPREHENSIVE # Vehicle with Registration := true # Two Wheeler Type := Motor Bike #Vehicle Make := BAJAJ # Vehicle Model := AVENGER # Vehicle Variant := 180 STREET BSIV (180.0)(Petrol) # Registration Year := 2009" are entered
And previous policy details "Don't know previous Policy details := true" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And button "Save Quote" is clicked

@Smoketest
Scenario: To issue a policy wwith Two Wheeler

Given logged to the application with details "User Name := 35327650 # Password := digit123"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := COMMERCIAL VEHICLE COMPREHENSIVE # Vehicle with Registration := true # Registration Number := KA01a1233 #Vehicle Category := GOODS CARRYING # Usage Type := Others #Vehicle Make := ASHOK LEYLAND # Vehicle Model := 1611 # Vehicle Variant := TANKER (1611.0)(Diesel) # Registration Year := 2009 # Vehicle Ownership := INDIVIDUAL" are entered
And previous policy details "Don't know previous Policy details := true" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And nominee details "Nominee Name := Sathesh # Nominee Relation := Brother" are entered
And button "Save Quote" is clicked
And survey details "Survey Type := Physical Inspection" are entered
And button "Submit" is clicked
And button "Download Quote" is clicked