Feature: Rollover PI

@FourWheeler_PI_Rollover_Breakin
Scenario Outline: 4W Rollover Breakin

Given user login to the application with details
#Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := FOUR WHEELER COMPREHENSIVE # Registration Number:= <RegNo> # Vehicle with Registration := true # Vehicle Make := <Make> # Vehicle Model := <Model> # Vehicle Variant := <Variant> # Registration Year := <RegYear> # Registration Month := <RegMonth> # Vehicle Ownership := INDIVIDUAL" are entered
And previous policy details "Previous Policy Expiry Date := [TODAY]" are entered
And button "Get Quote" is clicked
#And select all the covers
#And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And nominee details "Nominee Name := Sathesh # Nominee Relation := Brother" are entered
And button "Save Quote" is clicked
And survey details "Survey Type := Self Survey" are entered
And upload the previous policy document
And button "Submit" is clicked
And verify quote issuance is successful for Four Wheeler
#And button "Download Quote" is clicked
#And verify the policyStatus

Examples:	
	|	RegNo		|	Make	|	Model	|	Variant							  					|	RegMonth	|	RegYear	|
	|	DL01VB6778	|	Hyundai	|	CRETA	|	1.6 CRDi VGT 6Speed S+ (AT) (1582.0)(Diesel)(5 str)	|	February	|	2018	|	
#	|	MH12ZX9593	|	Hyundai	|  CELERIO	|	LDi (O) (793.0)(Diesel)(5 str) 						|	February	|	2018	|   
#	|	DL01ZX9593	|	Hyundai	|	RITZ	|	Genus VDi (1248.0)(Diesel)(5 str) 					|	February	|	2017	|
#	|	KL01ZX9593	|	Hyundai	|BALENO NEW	|	Alpha 1.3 (1248.0)(Diesel)(5 str) 				|	February	|	2018	|
#	|	TN03ZX9593	|	Hyundai	|  S CROSS	|	1.3 Alpha (1248.0)(Diesel)(5 str) 					|	February	|	2016	|
	