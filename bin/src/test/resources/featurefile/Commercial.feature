@Commercial @Motor @Agent_Sanity @Regression
Feature: To verify the functionalities of Commercial Vehicle is working fine

@Commercial_Breakin @Breakin
Scenario: CVOD BREAKIN

Given user login to the application with details
#Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := COMMERCIAL VEHICLE COMPREHENSIVE # Vehicle with Registration := true #Vehicle Category := PASSENGER CARRYING # Usage Type := Contract Carriage Permit Deluxe Taxi # Vehicle Make := AADITYA EMOTORS # Vehicle Model := DIXN # Vehicle Variant := E RICKSHAW (200.0) # Registration Year := 2009 # Vehicle Ownership := CORPORATE" are entered
And previous policy details "Don't know previous Policy details := true" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And button "Save Quote" is clicked
And click on download buttun
And survey details "Survey Type := Self Inspection" are entered
And button "Submit" is clicked
And verify quote issuance is successful for Commercial Vehicle
And button "Download Quote" is clicked
Then verify whether file has downloaded 

Then update the survey comments for the quote created

And open the policy with "Policy Number := [Policy_Number]"
And button "Recalculate" is clicked
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And button "Generate policy" is clicked
And verify policy issuance is successful for Commercial Vehicle
And verify the policyStatus


@Commercial_Rollover @Comm_Excel
Scenario: CVOD ROLLOVER
Vehicle Ownership : Corporate

Given user login to the application with details
#Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := COMMERCIAL VEHICLE COMPREHENSIVE # Vehicle with Registration := true #Vehicle Category := GOODS CARRYING # Usage Type := Others #Vehicle Make := CLH GASEOUS FUEL PVT LTD # Vehicle Model := GREEN CART # Vehicle Variant := E LOADER (200.0) # Registration Year := 2009 # Vehicle Ownership := INDIVIDUAL" are entered
And previous policy details "" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And nominee details "Nominee Name := Sathesh # Nominee Relation := Brother" are entered
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And upload the previous policy document
And button "Generate Policy" is clicked
And verify policy issuance is successful for Commercial Vehicle
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded 


@Comm_Excel @Commercial_New
Scenario: CVOD NEW
Vehicle Ownership : Corporate

Given user login to the application with details
#Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := COMMERCIAL VEHICLE COMPREHENSIVE # Brand New Vehicle := true # RTO Code := KA01 #Vehicle Category := GOODS CARRYING # Usage Type := National Permit #Vehicle Make := AMBAJI AUTO # Vehicle Model := KHUSHBU # Vehicle Variant := DELIVERY VAN (395.0)(Diesel) # Manufacturing Year := 2019 # Vehicle Ownership := CORPORATE" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And upload the previous policy document
And button "Generate Policy" is clicked
And verify policy issuance is successful for Commercial Vehicle
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded 


@CVTP_Rollover
Scenario: CVTP ROLLOVER

Given user login to the application with details
#Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := COMMERCIAL VEHICLE THIRD PARTY # Vehicle with Registration := true # Vehicle Category := GOODS CARRYING # Usage Type := National Permit # Vehicle Make := PACE # Vehicle Model := CARGO STAR # Vehicle Variant := E LOADER (200.0)(ELECTRIC) # Registration Year := 2009 # Vehicle Ownership := INDIVIDUAL" are entered
#And previous policy details "" are entered

#And previous policy details "Previous Policy Expiry Date := [TODAY]" are entered
#And previous policy details "Don't know previous Policy details := true" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And nominee details "Nominee Name := Sathesh # Nominee Relation := Brother" are entered
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And button "Generate Policy" is clicked
And verify quote issuance is successful for Commercial Vehicle
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded 



@CVTP_New
Scenario: CVTP NEW

Given user login to the application with details
#Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := COMMERCIAL VEHICLE THIRD PARTY # Brand New Vehicle := true # Vehicle Category := MISCELLANEOUS # Usage Type := Excavators #Vehicle Make := APOLLO # Vehicle Model := AP 550 PAVER # Vehicle Variant := OTHER MISC (1.0)(Diesel) # Manufacturing Year := 2019 # Vehicle Ownership := CORPORATE" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And button "Generate Policy" is clicked
And verify quote issuance is successful for Commercial Vehicle
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded 
