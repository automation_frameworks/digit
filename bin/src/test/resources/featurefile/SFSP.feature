@SFSP @Property @Regression
Feature: To verify the features of Sfsp is working fine

@SFSP_Rollover @Agent_Sanity
Scenario: SFSP ROLLOVER INDIVIDUAL 

Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "SFSP" is selected
And policy details "Policy Type := Rollover # Ownership Type := Individual" are entered in Agent SFSP Insurance page
And building details "Occupancy Type:= Dwellings # Type of construction:= Pucca # Fire Fighting Arrangements:= Fire Extinguisher,Hydrants  # Is Basement Exposed := No # Floor Number:= 2 # Build up Year:= 2019 # Address := GM Palya # pincode := 560023" are entered in Agent SFSP Insurance page
And base coverage details "Building := 10000000 # Plinth & Foundation := 10000 # Machinery & Accessories := 10000 # Furniture & Fixtures := 10000 # Other Property := 10000" are entered in Agent SFSP Insurance page
And previous insurer details "Expiring Insurer := Others # Any Past Loss Experience := Yes # No Of Claims := 2" are entered in Agent SFSP Insurance page
And claim details "Amount Claimed := 1000 # Reason := jksjdksjf" for claim "1,2" are entered in Agent SFSP Insurance page
And action "Show Premium" is performed in SFSP Insurance page
And insurer details "Insured Name := Insured # Last Name := lastname # Pincode := 560001 # Address := address # Mobile Number := 9876543210 # Email ID := test@godigit.com" are entered in Agent SFSP Insurance page
And action "Save Quote" is performed in SFSP Insurance page
And payment details "Payment By Customer Cheque" are entered in Agent SFSP Insurance page
And action "Proceed To Pay" is performed in SFSP Insurance page
And verify the success page for SFSP Insurance 
And verify the policyStatus

@SFSP_Rollover_Corporate
Scenario: SFSP ROLLOVER CORPORATE 

Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "SFSP" is selected
And policy details "Policy Type := Rollover # Ownership Type := Corporate" are entered in Agent SFSP Insurance page
And building details "Occupancy Type:= Dwellings # Type of construction:= Pucca # Fire Fighting Arrangements:= Fire Extinguisher  # Is Basement Exposed := No # Floor Number:= 2 # Build up Year:= 2019 # Hypothecated To:= Indian Bank # Address := GM Palya # pincode := 560023" are entered in Agent SFSP Insurance page
And base coverage details "Building := 10000000 # Plinth & Foundation := 10000 # Machinery & Accessories := 10000 # Furniture & Fixtures := 10000 # Other Property := 10000" are entered in Agent SFSP Insurance page
And previous insurer details "Expiring Insurer := Others # Any Past Loss Experience := No" are entered in Agent SFSP Insurance page
And action "Show Premium" is performed in SFSP Insurance page
And insurer details "Insured Name := Insured # Pincode := 560001 # Address := address # Mobile Number := 9876543210 # Email ID := test@godigit.com" are entered in Agent SFSP Insurance page
And action "Save Quote" is performed in SFSP Insurance page
And payment details "Payment By Customer Cheque" are entered in Agent SFSP Insurance page
And action "Proceed To Pay" is performed in SFSP Insurance page
And verify the success page for SFSP Insurance
And verify the policyStatus

@SFSP_New_Individual
Scenario: SFSP ROLLOVER INDIVIDUAL 

Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "SFSP" is selected
And policy details "Policy Type := New # Ownership Type := Individual" are entered in Agent SFSP Insurance page
And building details "Occupancy Type:= Dwellings # Type of construction:= Pucca # Fire Fighting Arrangements:= Fire Extinguisher  # Is Basement Exposed := No # Floor Number:= 2 # Build up Year:= 2019 # Hypothecated To:= Indian Bank # Address := GM Palya # pincode := 560023" are entered in Agent SFSP Insurance page
And base coverage details "Building := 10000000 # Plinth & Foundation := 10000 # Machinery & Accessories := 10000 # Furniture & Fixtures := 10000 # Other Property := 10000" are entered in Agent SFSP Insurance page
And action "Show Premium" is performed in SFSP Insurance page
And insurer details "Insured Name := Insured # Last Name := lastname # Pincode := 560001 # Address := address # Mobile Number := 9876543210 # Email ID := test@godigit.com" are entered in Agent SFSP Insurance page
And action "Save Quote" is performed in SFSP Insurance page
And payment details "Payment By Customer Cheque" are entered in Agent SFSP Insurance page
And action "Proceed To Pay" is performed in SFSP Insurance page
And verify the success page for SFSP Insurance
And verify the policyStatus

@SFSP_New_Corporate
Scenario: SFSP ROLLOVER CORPORATE 

Given logged to the application with details "User Name := 35327650 # Password := Digit@123$"
When module "SFSP" is selected
And policy details "Policy Type := New # Ownership Type := Corporate" are entered in Agent SFSP Insurance page
And building details "Occupancy Type:= Dwellings # Type of construction:= Pucca # Fire Fighting Arrangements:= Fire Extinguisher  # Is Basement Exposed := No # Floor Number:= 2 # Build up Year:= 2019 # Hypothecated To:= Indian Bank # Address := GM Palya # pincode := 560023" are entered in Agent SFSP Insurance page
And base coverage details "Building := 10000000 # Plinth & Foundation := 10000 # Machinery & Accessories := 10000 # Furniture & Fixtures := 10000 # Other Property := 10000" are entered in Agent SFSP Insurance page
And action "Show Premium" is performed in SFSP Insurance page
And insurer details "Insured Name := Insured # Pincode := 560001 # Address := address # Mobile Number := 9876543210 # Email ID := test@godigit.com" are entered in Agent SFSP Insurance page
And action "Save Quote" is performed in SFSP Insurance page
And payment details "Payment By Customer Cheque" are entered in Agent SFSP Insurance page
And action "Proceed To Pay" is performed in SFSP Insurance page
And verify the success page for SFSP Insurance
And verify the policyStatus


