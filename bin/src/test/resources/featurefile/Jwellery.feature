@Jwellery
Feature: To issue a policy for jwellery
@Jwellery
Scenario: JWELLERY 

Given user login to the application with details
When user selects "Jewellery" module
And enters all the details "selectJwelleryType := Bracelets # productDescription := abcdef # jwelleryValue := 5000"
When user clicks on addItem button and getQuote button
Then "Confirm Payment" popup should be displayed
When user clicks on Buy Now option in confirm payment popup
Then "Customer Details" page should be displayed
And user enters all the details "FirstName := saranya # LastName := sundharrasu # EmailAddress :=sara@g.com # MobileNumber:= 3657465423 # city:=Bengaluru # pinCode:=560001"
And Upload Take picture and Upload Invoice Photo and Upload Acknowledgement Photo 
Then All the images to be uploaded "Product:=Product # Invoice:=Invoice # Acknowledgment:=Acknowledgment"
And clicks on proceed button
Then verify the success page with the success message "Jewellery is now insured!"
And click on download button