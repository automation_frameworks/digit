@Agent_Sanity @Motor_Extra
Feature: Verify the OD only policies

@FourWheeler_ODOnly @FourWheeler_Extra
Scenario: 4W OD ONLY

Given user login to the application with details
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := FOUR WHEELER COMPREHENSIVE # Policy Type := 1 YEAR OD ONLY # Vehicle With Registration := true # Vehicle Make := HONDA # Vehicle Model := AMAZE # Vehicle Variant := 1.2 E (1199.0)(Petrol) # Registration Year := 2018 # Vehicle Ownership := INDIVIDUAL" are entered
And previous policy details "Previous Policy Expiry Date := [TODAY]" are entered
And third party policy details "" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And enter payment details
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And upload the previous policy document
And button "Generate Policy" is clicked
And verify quote issuance is successful for Four Wheeler
And verify the policyStatus
Then verify whether file has downloaded

@TwoWheeler_ODOnly
Scenario: 2W OD ONLY

Given user login to the application with details
When module "Motor Insurance" is selected
And vehicle details "Vehicle Type := TWO WHEELER COMPREHENSIVE # Policy Type := 1 YEAR OD ONLY # Vehicle Type := MOTOR BIKE # Vehicle Make := HONDA # Vehicle Model := SHINE # Vehicle Variant := ELEC ST DISC BRAKE AWS (125.0)(Petrol) # Registration Year := 2018 # Vehicle Ownership := INDIVIDUAL" are entered
And previous policy details "Previous Policy Type := OD + 1 Year TP # Previous Policy Expiry Date := [TODAY]" are entered
And third party policy details "" are entered
And button "Get Quote" is clicked
And select all the covers
And button "Recalculate" is clicked
And customer details "Address Line 2 := jhdk" are entered
And enter payment details
And button "Save Quote" is clicked
And click on download buttun
And enter payment details
And upload the previous policy document
And button "Generate Policy" is clicked
And verify quote issuance is successful for Two Wheeler
And button "Download Policy For Email" is clicked
And verify the policyStatus
Then verify whether file has downloaded
